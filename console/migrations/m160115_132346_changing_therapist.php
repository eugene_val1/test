<?php

use yii\db\Schema;
use yii\db\Migration;

class m160115_132346_changing_therapist extends Migration
{
    public function up()
    {
        $this->dropTable('{{%specialty_lang}}');
        $this->dropTable('{{%specialty}}');
        $this->dropTable('{{%approach_lang}}');
        $this->dropTable('{{%approach}}');
        $this->dropTable('{{%format_lang}}');
        $this->dropTable('{{%format}}');
        
        $this->dropColumn('{{%therapist_lang}}', 'specialty');
        $this->dropColumn('{{%therapist_lang}}', 'approach');
        $this->dropColumn('{{%therapist_lang}}', 'format');
        
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {          
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%property}}', [
            'id' => Schema::TYPE_PK,
            'type' => Schema::TYPE_STRING . ' NOT NULL',
            'number' => Schema::TYPE_INTEGER . ' NOT NULL'
        ], $tableOptions);
        
        $this->createTable('{{%property_lang}}', [
            'id' => Schema::TYPE_PK,
            'property_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'language' => Schema::TYPE_STRING . '(6) NOT NULL',
            'name' => Schema::TYPE_STRING . ' NOT NULL'
        ], $tableOptions);
        
        $this->createIndex('property_lang', '{{%property_lang}}', 'language, property_id'); 
        
        $this->createTable('{{%therapist_property}}', [
            'therapist_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'property_id' => Schema::TYPE_INTEGER . ' NOT NULL',
        ], $tableOptions);
        
        $this->addPrimaryKey('', '{{%therapist_property}}', ['therapist_id', 'property_id']);
        $this->createIndex('therapist_properties', '{{%therapist_property}}', 'therapist_id');
    }

    public function down()
    {
        echo "m160115_132346_changing_therapist cannot be reverted.\n";

        return false;
    }
}
