<?php

use yii\db\Schema;
use yii\db\Migration;

class m151007_073443_link_in_blank_page extends Migration
{
    public function up()
    {
        $this->addColumn('{{%link}}', 'blank', Schema::TYPE_SMALLINT . ' NOT NULL AFTER `modal` ');
    }

    public function down()
    {
        $this->dropColumn('{{%link}}', 'blank');
    }
}
