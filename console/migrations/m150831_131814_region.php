<?php

use yii\db\Schema;
use yii\db\Migration;

class m150831_131814_region extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {          
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%region}}', [
            'id' => Schema::TYPE_PK,
            'number' => Schema::TYPE_INTEGER . ' NOT NULL'
        ], $tableOptions);
        
        $this->createTable('{{%region_lang}}', [
            'id' => Schema::TYPE_PK,
            'region_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'language' => Schema::TYPE_STRING . '(6) NOT NULL',
            'name' => Schema::TYPE_STRING . ' NOT NULL'
        ], $tableOptions);
        
        $this->createIndex('region_id', '{{%region_lang}}', 'region_id');
        $this->createIndex('language', '{{%region_lang}}', 'language');

        $this->addColumn('{{%country}}', 'region_id', Schema::TYPE_INTEGER . ' NOT NULL AFTER `id` ');
        
        $this->addForeignKey('fk_cl_region', 'region_lang', 'region_id', 'region', 'id','CASCADE','CASCADE');
    }

    public function down()
    {
        $this->dropTable('{{%region}}');
        $this->dropTable('{{%region_lang}}');
    }
}
