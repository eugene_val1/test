<?php

use yii\db\Schema;
use yii\db\Migration;

class m160427_184241_profile_order_number extends Migration
{
    public function up()
    {
        $this->addColumn('{{%profile}}', 'number', Schema::TYPE_INTEGER . ' NOT NULL AFTER `type` ');
    }

    public function down()
    {
        $this->dropColumn('{{%profile}}', 'number');
    }
}
