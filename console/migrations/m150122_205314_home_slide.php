<?php

use yii\db\Schema;
use yii\db\Migration;

class m150122_205314_home_slide extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {          
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%home_slide}}', [
            'id' => Schema::TYPE_PK,
            'number' => Schema::TYPE_INTEGER . ' NOT NULL',
            'type' => Schema::TYPE_STRING . ' NOT NULL',
            'data' => Schema::TYPE_TEXT . ' NOT NULL'
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%home_slide}}');
    }
}
