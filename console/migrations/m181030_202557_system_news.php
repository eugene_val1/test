<?php

use yii\db\Migration;

class m181030_202557_system_news extends Migration
{
    public function safeUp()
    {
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('system_news', [
            'id' => $this->primaryKey(),
            'target' => $this->smallInteger(),
            'text' => $this->text(),
            'title' => $this->string(1024),
            'status' => $this->smallInteger()->defaultValue(0),
            'created' => $this->timestamp()->notNull()->defaultExpression('NOW()'),
            'updated' => $this->timestamp()->notNull()->defaultExpression('NOW()'),
        ], $tableOptions);

        $this->createTable('system_news_read', [
            'id' => $this->primaryKey(),
            'system_news_id' => $this->integer()->notNull(),
            'user_id' => $this->integer()->notNull(),
        ]);

        $this->addForeignKey(
            'system_news_id_idx',
            'system_news_read',
            'system_news_id',
            'system_news',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropTable('system_news_read');
        $this->dropTable('system_news');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181030_202557_system_news cannot be reverted.\n";

        return false;
    }
    */
}
