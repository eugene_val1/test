<?php

use yii\db\Schema;
use yii\db\Migration;

class m151209_105046_article_featured extends Migration
{
    public function up()
    {
        $this->addColumn('{{%article}}', 'featured', Schema::TYPE_INTEGER . ' NOT NULL AFTER `published` ');
    }

    public function down()
    {
        $this->dropColumn('{{%article}}', 'featured');
    }
}
