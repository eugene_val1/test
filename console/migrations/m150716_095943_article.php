<?php

use yii\db\Schema;
use yii\db\Migration;

class m150716_095943_article extends Migration
{
    public function up() {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%article}}', [
            'id' => Schema::TYPE_PK,
            'user_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'type' => Schema::TYPE_STRING . ' NOT NULL',
            'title' => Schema::TYPE_STRING . '(500) NOT NULL',
            'short_description' => Schema::TYPE_TEXT . ' NOT NULL',
            'content' => Schema::TYPE_TEXT . ' NOT NULL',
            'image' => Schema::TYPE_STRING . ' NOT NULL',
            'video_source' => Schema::TYPE_STRING . ' NOT NULL',
            'video_source_id' => Schema::TYPE_STRING . ' NOT NULL',
            'created' => Schema::TYPE_INTEGER . ' NOT NULL',
            'updated' => Schema::TYPE_INTEGER . ' NOT NULL',
            'published' => Schema::TYPE_INTEGER . ' NOT NULL'
        ], $tableOptions);
        
        $this->createIndex('article_user_idx', '{{%article}}', 'user_id');
    }

    public function down() {
        $this->dropTable('{{%article}}');
    }
}
