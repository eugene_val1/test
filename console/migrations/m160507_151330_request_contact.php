<?php
use yii\db\Schema;
use yii\db\Migration;

class m160507_151330_request_contact extends Migration
{
    public function up()
    {
        $this->addColumn('{{%contact_theme}}', 'use_for_request', Schema::TYPE_INTEGER . ' NOT NULL AFTER `number` ');
    }

    public function down()
    {
        $this->dropColumn('{{%contact_theme}}', 'use_for_request');
    }
}
