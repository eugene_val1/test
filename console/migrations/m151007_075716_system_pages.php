<?php

use yii\db\Schema;
use yii\db\Migration;

class m151007_075716_system_pages extends Migration
{
    public function up()
    {
        $this->addColumn('{{%page}}', 'type', Schema::TYPE_STRING . ' NOT NULL AFTER `image` ');
        $this->addColumn('{{%page}}', 'section', Schema::TYPE_STRING . ' NOT NULL AFTER `type` ');
        
        $this->addColumn('{{%page_lang}}', 'keywords', Schema::TYPE_TEXT . ' NOT NULL AFTER `content` ');
        $this->addColumn('{{%page_lang}}', 'description', Schema::TYPE_TEXT . ' NOT NULL AFTER `keywords` ');

        $this->batchInsert('{{%page}}', ['published', 'type', 'section'], [
            ['1', 'system', 'account'],
            ['1', 'system', 'field'],
            ['1', 'system', 'therapists'],
            ['1', 'system', 'faq'],
            ['1', 'system', 'messages'],
        ]);
        
        $pages = (new \yii\db\Query())
            ->select(['id', 'section'])
            ->from('page')
            ->where(['type' => 'system'])
            ->indexBy('section')
            ->all();
        
        $this->batchInsert('{{%page_lang}}', ['page_id', 'language', 'title'], [
            [$pages['account']['id'], 'ru-RU', 'Мой аккаунт'],
            [$pages['account']['id'], 'en-US', 'My account'],
            [$pages['field']['id'], 'ru-RU', 'Поле'],
            [$pages['field']['id'], 'en-US', 'Field'],
            [$pages['therapists']['id'], 'ru-RU', 'Терапевты'],
            [$pages['therapists']['id'], 'en-US', 'Therapists'],
            [$pages['faq']['id'], 'ru-RU', 'F.A.Q.'],
            [$pages['faq']['id'], 'en-US', 'F.A.Q.'],
            [$pages['messages']['id'], 'ru-RU', 'Сообщения'],
            [$pages['messages']['id'], 'en-US', 'Messages'],
        ]);
        
    }

    public function down()
    {
        $this->dropColumn('{{%page}}', 'type');
        $this->dropColumn('{{%page}}', 'section');
        $this->dropColumn('{{%page_lang}}', 'keywords');
        $this->dropColumn('{{%page_lang}}', 'description');
    }
}
