<?php

use yii\db\Schema;
use yii\db\Migration;

class m160826_074339_appointment_overdue_informed extends Migration
{
    public function up()
    {
        $this->addColumn('{{%appointment}}', 'overdue_informed', Schema::TYPE_INTEGER . ' NOT NULL DEFAULT "0" AFTER `created`');
    }

    public function down()
    {
        $this->dropColumn('{{%appointment}}', 'overdue_informed');
    }
}
