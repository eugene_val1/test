<?php

use yii\db\Schema;
use yii\db\Migration;

class m150831_202020_therapist_additional_columns extends Migration
{
    public function up()
    {
        $this->addColumn('{{%profile}}', 'birth_date', Schema::TYPE_DATE . ' NOT NULL DEFAULT \'0000-00-00 00:00:00\' AFTER `alias` ');
        $this->addColumn('{{%profile}}', 'duration', Schema::TYPE_INTEGER . ' DEFAULT NULL AFTER `birth_date` ');
        
        $this->addColumn('{{%therapist_lang}}', 'education', Schema::TYPE_TEXT . ' DEFAULT NULL AFTER `short_description` ');
        $this->addColumn('{{%therapist_lang}}', 'experience', Schema::TYPE_TEXT . ' DEFAULT NULL AFTER `education` ');
    }

    public function down()
    {
        $this->dropColumn('{{%profile}}', 'birth_date');
        $this->dropColumn('{{%profile}}', 'duration');
        
        $this->dropColumn('{{%therapist_lang}}', 'education');
        $this->dropColumn('{{%therapist_lang}}', 'experience');
    }
}
