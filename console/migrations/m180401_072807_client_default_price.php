<?php

use common\models\Client;
use yii\db\Migration;

class m180401_072807_client_default_price extends Migration
{
    public function safeUp()
    {
        $clients = Client::find()
            ->alias('c')
            ->with(['therapist'])
            ->where(['price' => 0])
            ->all();

        foreach ($clients as $client) {
            /** @var Client $client */
            $client->price = $client->getTherapistPrice();
            $client->save(false);
        }
    }

    public function safeDown()
    {
        return true;
    }
}
