<?php

use yii\db\Migration;

/**
 * Class m190322_195133_therapist_business_card_alt
 */
class m190322_195133_therapist_business_card_alt extends Migration
{
    /**
     * @var int
     */
    private $imageAltLength = 1024;

    /**
     * Up
     */
    public function safeUp()
    {
        $this->addColumn(
            'therapist_additional',
            'business_card_image_alt',
            $this->string($this->imageAltLength)
        );
    }

    /**
     * Down
     */
    public function safeDown()
    {
        $this->dropColumn('therapist_additional', 'business_card_image_alt');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190322_195133_therapist_business_card_alt cannot be reverted.\n";

        return false;
    }
    */
}
