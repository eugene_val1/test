<?php

use yii\db\Schema;
use yii\db\Migration;

class m150812_102301_feedback extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%feedback}}', [
            'id' => Schema::TYPE_PK,
            'user_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'text' => Schema::TYPE_TEXT . ' NOT NULL',
            'published' => Schema::TYPE_INTEGER . '(2) NOT NULL',
            'created' => Schema::TYPE_INTEGER . ' NOT NULL',
        ], $tableOptions);
        
        $this->createIndex('feedback_idx', '{{%feedback}}', 'published');
    }

    public function down()
    {
        $this->dropTable('{{%feedback}}');
    }
}
