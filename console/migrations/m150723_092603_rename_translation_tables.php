<?php

use yii\db\Migration;

class m150723_092603_rename_translation_tables extends Migration
{
    public function up()
    {
        $this->renameTable('{{%source_message}}', '{{%source_translation_message}}');
        $this->renameTable('{{%message}}', '{{%translation_message}}');
    }

    public function down()
    {
        echo "m150723_092603_rename_translation_tables cannot be reverted.\n";

        return false;
    }
}
