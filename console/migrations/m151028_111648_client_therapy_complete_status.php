<?php

use yii\db\Schema;
use yii\db\Migration;

class m151028_111648_client_therapy_complete_status extends Migration
{
    public function up()
    {
        $this->addColumn('{{%client}}', 'complete_status', Schema::TYPE_INTEGER . ' NOT NULL AFTER `next_appointment_start` ');
    }

    public function down()
    {
        $this->dropColumn('{{%client}}', 'complete_status');
    }
}
