<?php

use yii\db\Schema;
use yii\db\Migration;

class m160706_191307_partner_code extends Migration
{
    public function up()
    {
        $this->addColumn('{{%partner}}', 'partner_code', Schema::TYPE_STRING . ' DEFAULT NULL AFTER `user_id`');
    }

    public function down()
    {
        $this->dropColumn('{{%partner}}', 'partner_code');
    }
}
