<?php

use yii\db\Schema;
use yii\db\Migration;

class m151211_204319_comments extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%comment}}', [
            'id' => Schema::TYPE_PK,
            'user_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'article_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'reply_to' => Schema::TYPE_INTEGER . ' NOT NULL DEFAULT 0',
            'level' => Schema::TYPE_INTEGER . ' NOT NULL DEFAULT 0',
            'text' => Schema::TYPE_TEXT . ' NOT NULL',
            'created' => Schema::TYPE_INTEGER . ' NOT NULL',
            'deleted' => Schema::TYPE_INTEGER . ' NOT NULL',
            'votes_count' => Schema::TYPE_INTEGER . ' NOT NULL DEFAULT 0',
            'replies_count' => Schema::TYPE_INTEGER . ' NOT NULL DEFAULT 0',
        ], $tableOptions);
        
        $this->addColumn('{{%article}}', 'comments_count', Schema::TYPE_INTEGER . ' NOT NULL AFTER `votes_count` ');
        $this->addForeignKey('user_comment_fk', 'comment', 'user_id', 'user', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('article_comment_fk', 'comment', 'article_id', 'article', 'id', 'CASCADE', 'CASCADE');
        
    }

    public function down()
    {
        $this->dropTable('{{%comment}}');
        $this->dropColumn('{{%article}}', 'comments_count');
    }
}
