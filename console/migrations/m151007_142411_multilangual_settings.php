<?php

use yii\db\Schema;
use yii\db\Migration;

class m151007_142411_multilangual_settings extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%setting_lang}}', [
            'id' => Schema::TYPE_PK,
            'setting_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'language' => Schema::TYPE_STRING . '(6) NOT NULL',
            'content' => Schema::TYPE_STRING . ' NOT NULL',
        ], $tableOptions);
        
        $this->createIndex('setting_id', '{{%setting_lang}}', 'setting_id');
        $this->createIndex('language', '{{%setting_lang}}', 'language');
        
        $this->addForeignKey('fk_cl_setting', 'setting_lang', 'setting_id', 'setting', 'id','CASCADE','CASCADE');
    }

    public function down()
    {
        $this->dropTable('{{%setting_lang}}');
    }
}
