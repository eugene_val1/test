<?php

use yii\db\Schema;
use yii\db\Migration;

class m160201_200458_wayforpay_merchant_settings extends Migration
{
    public function up()
    {
        $this->batchInsert('{{%setting}}', ['key', 'category', 'type', 'title', 'value'], [
            ['wayforpayMerchantAccount', 'payment', 'string', 'WayForPay merchant account', ''],
            ['wayforpayMerchantSecretKey', 'payment', 'string', 'WayForPay secret key', ''],
        ]);
    }

    public function down()
    {
        echo "m160201_200458_wayforpay_merchant_settings cannot be reverted.\n";

        return false;
    }
}
