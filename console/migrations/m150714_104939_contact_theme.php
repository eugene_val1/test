<?php

use yii\db\Schema;
use yii\db\Migration;

class m150714_104939_contact_theme extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {          
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%contact_theme}}', [
            'id' => Schema::TYPE_PK,
            'use_additional_field' => Schema::TYPE_INTEGER . ' NOT NULL',
            'number' => Schema::TYPE_INTEGER . ' NOT NULL'
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%contact_theme}}');
    }
}
