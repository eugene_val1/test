<?php

use yii\db\Migration;

class m170723_154330_add_mailing_agreement extends Migration
{
    public function safeUp()
    {
        $this->addColumn('profile', 'agree_mailing', $this->boolean()->defaultValue(true));
    }

    public function safeDown()
    {
        $this->dropColumn('profile', 'agree_mailing');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170723_154330_add_mailing_agreement cannot be reverted.\n";

        return false;
    }
    */
}
