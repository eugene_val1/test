<?php

use yii\db\Schema;
use yii\db\Migration;

class m150127_132539_home_slide_lang extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%home_slide_lang}}', [
            'id' => Schema::TYPE_PK,
            'home_slide_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'language' => Schema::TYPE_STRING . '(6) NOT NULL',
            'title' => Schema::TYPE_STRING . '(500) NOT NULL',
            'content' => Schema::TYPE_TEXT . ' NOT NULL'
        ], $tableOptions);
        
        $this->createIndex('home_slide_id', '{{%home_slide_lang}}', 'home_slide_id');
        $this->createIndex('language', '{{%home_slide_lang}}', 'language');
        
        $this->addForeignKey('fk_cl_home_slide', 'home_slide_lang', 'home_slide_id', 'home_slide', 'id','CASCADE','CASCADE');
    }

    public function down()
    {
        $this->dropTable('{{%home_slide_lang}}');
    }
}
