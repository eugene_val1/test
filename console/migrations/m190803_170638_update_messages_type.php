<?php

use yii\db\Migration;

/**
 * Class m190803_170638_update_messages_type
 */
class m190803_170638_update_messages_type extends Migration
{
    /**
     * Down
     */
    public function safeUp()
    {
        $sql = 'UPDATE conversation_message
                SET type="system"
                  WHERE type="standart" AND (
                  text LIKE "%сессия #%"
                   OR text LIKE "%сессия №%"
                   OR text LIKE "%Сессия No%"
                   OR text LIKE "[Это автоматическое сообщение]%"
                   OR text LIKE "%изменил(а) временной пояс на%"
                )';

        $this->execute($sql);

    }

    /**
     * Down
     */
    public function safeDown()
    {
        $sql = 'UPDATE conversation_message
                SET type="standart"
                WHERE type="system" AND (
                  text LIKE "%сессия #%"
                   OR text LIKE "%сессия №%"
                   OR text LIKE "[Это автоматическое сообщение]%"
                   OR text LIKE "%изменил(а) временной пояс на%"
                )';

        $this->execute($sql);
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190803_170638_update_messages_type cannot be reverted.\n";

        return false;
    }
    */
}
