<?php

use yii\db\Migration;

/**
 * Class m180830_202131_add_article_preview_image
 */
class m180830_202131_add_article_preview_image extends Migration
{
    /**
     * Safe up
     */
    public function safeUp()
    {
        $this->addColumn('article', 'preview_image', $this->string(255)->after('cover_image'));
    }

    /**
     * Down
     */
    public function safeDown()
    {
        $this->dropColumn('article', 'preview_image');
    }
}
