<?php

use yii\db\Schema;
use yii\db\Migration;

class m151007_144153_seo_settings extends Migration
{
    public function up()
    {
        $this->batchInsert('{{%setting}}', ['key', 'type', 'title'], [
            ['mainTitle', 'multilangual', 'Main site title'],
            ['mainKeywords', 'multilangual', 'Main site keywords'],
            ['mainDescription', 'multilangual', 'Main site description'],
        ]);
        
        $settings = (new \yii\db\Query())
            ->select(['id', 'key'])
            ->from('setting')
            ->where(['type' => 'multilangual'])
            ->indexBy('key')
            ->all();
        
        $this->batchInsert('{{%setting_lang}}', ['setting_id', 'language', 'content'], [
            [$settings['mainTitle']['id'], 'ru-RU', 'Тритфилд'],
            [$settings['mainTitle']['id'], 'en-US', 'Treatfield'],
            [$settings['mainKeywords']['id'], 'ru-RU', 'Ключевые слова, сайт, и т.д.'],
            [$settings['mainKeywords']['id'], 'en-US', 'Keywords, site, etc.'],
            [$settings['mainDescription']['id'], 'ru-RU', 'Короткое описание сайта'],
            [$settings['mainDescription']['id'], 'en-US', 'Short site description'],
        ]);
        
    }

    public function down()
    {
        echo "m151007_144153_seo_settings cannot be reverted.\n";

        return false;
    }
}
