<?php

use yii\db\Migration;

class m151230_131735_footer_content extends Migration
{
    public function up()
    {
        $this->insert('{{%setting}}', [
            'key' => 'footerContent',
            'type' => 'text',
            'title' => 'Footer content',
            'value'=> ''
        ]);
    }

    public function down()
    {
        echo "m151230_131735_footer_content cannot be reverted.\n";

        return false;
    }
}
