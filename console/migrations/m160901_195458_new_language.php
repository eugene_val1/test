<?php

use yii\db\Migration;
use common\models\Language;

class m160901_195458_new_language extends Migration
{
    public function up()
    {
        $this->insert('{{%language}}', [
            'url' => 'ua',
            'local' => 'uk-UA',
            'name' => 'Українська'
        ]);
        
        $models = $this->getModels();
        foreach ($models as $model){
            $modelName = "common\models\\" . $model;
            $records = $modelName::findAll(['language' => 'ru-RU']);
            foreach($records as $record){
                if($model != 'TranslationMessage'){
                    $record->id = null;
                }
                $record->isNewRecord = true;                
                $record->language = 'uk-UA';
                $record->save(false);
            }
        }
    }

    public function down()
    {
        Language::deleteAll(['local' => 'uk-UA']);
        $models = $this->getModels();
        foreach ($models as $model){
            $modelName = "common\models\\" . $model;
            $modelName::deleteAll(['language' => 'uk-UA']);
        }
    }
    
    public function getModels(){
        return ['CertificateLang', 'ContactThemeLang', 'CountryLang', 'FaqCategoryLang', 'FaqLang', 'FeatureLang', 'HomeSlideLang', 'LinkLang', 'MenuLang', 'PageLang', 'PropertyLang', 'RegionLang', 'SettingLang', 'TherapistLang', 'TranslationMessage', 'Timezone'];
    }
}
