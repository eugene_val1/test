<?php

use yii\db\Schema;
use yii\db\Migration;

class m150526_212539_feature_lang extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%feature_lang}}', [
            'id' => Schema::TYPE_PK,
            'feature_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'language' => Schema::TYPE_STRING . '(6) NOT NULL',
            'title' => Schema::TYPE_STRING . ' NOT NULL',
            'description' => Schema::TYPE_TEXT . ' NOT NULL'
        ], $tableOptions);
        
        $this->createIndex('feature_id', '{{%feature_lang}}', 'feature_id');
        $this->createIndex('language', '{{%feature_lang}}', 'language');
        
        $this->addForeignKey('fk_cl_feat', 'feature_lang', 'feature_id', 'feature', 'id','CASCADE','CASCADE');
    }

    public function down()
    {
        $this->dropTable('{{%feature_lang}}');
    }
}
