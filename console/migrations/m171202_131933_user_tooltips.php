<?php

use yii\db\Migration;

class m171202_131933_user_tooltips extends Migration
{
    public function safeUp()
    {
        $this->addColumn('profile', 'show_tooltips', $this->boolean()->defaultValue(true));
        \common\models\Profile::updateAll(['show_tooltips' => 0]);

        $this->insert('setting', [
            'key' => 'showTooltips',
            'category' => 'main',
            'type' => 'checkbox',
            'title' => 'Show tooltips to new users',
            'value' => 1
        ]);
    }

    public function safeDown()
    {
        $this->dropColumn('profile', 'show_tooltips');
        $this->delete('setting', ['key' => 'showTooltips']);
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171202_131933_user_tooltips cannot be reverted.\n";

        return false;
    }
    */
}
