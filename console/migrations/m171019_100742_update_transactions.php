<?php

use yii\db\Migration;

class m171019_100742_update_transactions extends Migration
{
    public function safeUp()
    {
        $this->addColumn('transaction', 'status_update_informed', $this->boolean()->defaultValue(1));
    }

    public function safeDown()
    {
        $this->dropColumn('transaction', 'status_update_informed');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171019_100742_update_transactions cannot be reverted.\n";

        return false;
    }
    */
}
