<?php

use yii\db\Migration;

class m170521_180917_update_messages_table extends Migration
{
    public function up()
    {
        $this->addColumn('conversation_message', 'receiver_id', $this->integer(11)->after('user_id'));
        $this->createIndex('receiver_id_idx', 'conversation_message', 'receiver_id');

        $messages = \common\models\Messages\ConversationMessage::find()
            ->with(['conversation'])
            ->all();

        foreach ($messages as $message) {
            if (empty($message->conversation)) {
                continue;
            }
            if ($message->conversation->sender_id == $message->user_id) {
                $message->receiver_id = $message->conversation->receiver_id;
            } else {
                $message->receiver_id = $message->conversation->sender_id;
            }
            $message->save(false);
        }

    }

    public function down()
    {
        $this->dropIndex('receiver_id_idx', 'conversation_message');
        $this->dropColumn('conversation_message', 'receiver_id');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
