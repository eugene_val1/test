<?php

use yii\db\Migration;
use yii\db\Schema;

class m170521_084826_create_messages_notifications extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('messages_notifications', [
            'id' => $this->primaryKey(11),
            'message_id' => $this->integer(11)->comment('Conversation message ID'),
            'created' => Schema::TYPE_INTEGER . ' NOT NULL',
        ], $tableOptions);

        $this->createIndex('message_id_idx', 'messages_notifications', 'message_id');
    }

    public function down()
    {
        $this->dropTable('messages_notifications');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
