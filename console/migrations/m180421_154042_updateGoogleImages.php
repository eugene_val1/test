<?php

use common\models\Profile;
use yii\db\Migration;

/**
 * Class m180421_154042_updateGoogleImages
 */
class m180421_154042_updateGoogleImages extends Migration
{
    /**
     * Up
     */
    public function safeUp()
    {
        $models = Profile::find()->all();

        foreach ($models as $model) {
            if (strpos($model->image, '?sz=50') !== false) {
                $model->image = str_replace('sz=50', 'sz=200', $model->image);
                $model->save(false);
            }
        }
    }
}
