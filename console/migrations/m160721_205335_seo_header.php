<?php

use yii\db\Migration;

class m160721_205335_seo_header extends Migration
{
    public function up()
    {
        $this->insert('{{%setting}}', [
            'key' => 'headContent',
            'category' => 'seo',
            'type' => 'text',
            'title' => 'Head section content',
            'value'=> ''
        ]);
    }

    public function down()
    {
        echo "m160721_205335_seo_header cannot be reverted.\n";

        return false;
    }
}
