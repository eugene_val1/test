<?php

use yii\db\Schema;
use yii\db\Migration;

class m150528_130457_therapist extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {          
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%therapist}}', [
            'id' => Schema::TYPE_PK,
            'user_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'alias' => Schema::TYPE_STRING . ' NOT NULL',
            'number' => Schema::TYPE_INTEGER . ' NOT NULL',
            'image' => Schema::TYPE_STRING . ' NOT NULL',
            'catalog_image' => Schema::TYPE_STRING . ' NOT NULL',
            'catalog_image_hover' => Schema::TYPE_STRING . ' NOT NULL',
            'published' => Schema::TYPE_INTEGER . ' NOT NULL',
        ], $tableOptions);
        
        $this->createIndex('therapist_user_idx', '{{%therapist}}', 'user_id');
        $this->createIndex('therapist_alias_idx', '{{%therapist}}', 'alias', true);
    }

    public function down()
    {
        $this->dropTable('{{%therapist}}');
    }
}
