<?php

use yii\db\Migration;

class m170527_115838_add_admin_settings extends Migration
{
    public function up()
    {
        $this->insert('setting', [
            'key' => 'showUserComments',
            'category' => 'main',
            'type' => 'checkbox',
            'title' => 'Show user comments slider',
            'value' => 0
        ]);
    }

    public function down()
    {
        $this->delete('setting', ['key' => 'showUserComments']);
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
