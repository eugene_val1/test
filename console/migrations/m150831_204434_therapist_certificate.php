<?php

use yii\db\Schema;
use yii\db\Migration;

class m150831_204434_therapist_certificate extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {          
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%certificate}}', [
            'id' => Schema::TYPE_PK,
            'therapist_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'image' => Schema::TYPE_STRING . ' NOT NULL'
        ], $tableOptions);
        
        $this->createTable('{{%certificate_lang}}', [
            'id' => Schema::TYPE_PK,
            'certificate_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'language' => Schema::TYPE_STRING . '(6) NOT NULL',
            'description' => Schema::TYPE_TEXT . ' NOT NULL'
        ], $tableOptions);
        
        $this->createIndex('certificate_id', '{{%certificate_lang}}', 'certificate_id');
        $this->createIndex('language', '{{%certificate_lang}}', 'language');

        $this->addForeignKey('fk_cl_certificate', 'certificate_lang', 'certificate_id', 'certificate', 'id','CASCADE','CASCADE');
    }

    public function down()
    {
        $this->dropTable('{{%certificate}}');
        $this->dropTable('{{%certificate_lang}}');
    }
}
