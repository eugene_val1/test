<?php

use yii\db\Schema;
use yii\db\Migration;

class m150817_202949_client_name_column extends Migration
{
    public function up()
    {
        $this->addColumn('{{%client}}', 'user_name', Schema::TYPE_STRING . ' NOT NULL AFTER `user_id` ');
    }

    public function down()
    {
        echo "m150817_202949_client_name_column cannot be reverted.\n";

        return false;
    }
}
