<?php

use yii\db\Schema;
use yii\db\Migration;

class m141113_174210_profile extends Migration {

    public function up() {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%profile}}', [
            'id' => Schema::TYPE_PK,
            'user_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'name' => Schema::TYPE_STRING . '(255) NOT NULL',
            'image' => Schema::TYPE_STRING . '(100) NOT NULL'
        ], $tableOptions);
        
        $this->insert('{{%profile}}', [
            'id' => 1,
            'user_id' => 1,
            'name' => 'Admin',
            'image' => ''
        ]);
        
        $this->createIndex('profile_user_id', '{{%profile}}', 'user_id');
    }

    public function down() {
        $this->dropTable('{{%profile}}');
    }

}
