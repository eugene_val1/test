<?php
use yii\db\Schema;
use yii\db\Migration;

class m160619_162343_therapist_partner extends Migration
{
   public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {          
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%partner}}', [
            'id' => Schema::TYPE_PK,
            'user_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'phone' => Schema::TYPE_STRING . ' DEFAULT NULL',
            'card_holder' => Schema::TYPE_STRING . ' DEFAULT NULL',
            'card_number' => Schema::TYPE_STRING . ' DEFAULT NULL',
            'expired' => Schema::TYPE_INTEGER . ' NOT NULL'
        ], $tableOptions);
        
        $this->createIndex('partner_therapist_id_idx', '{{%partner}}', 'user_id');
    }

    public function down()
    {
        $this->dropTable('{{%partner}}');
    }
}
