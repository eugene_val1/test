<?php
use common\models\Profile;
use yii\db\Migration;

class m170312_212341_user_alias extends Migration
{
    public function up()
    {
        $profiles = Profile::findAll(['type' => Profile::TYPE]);
        foreach($profiles as $profile){                
            $profile->alias = Profile::getUniqueAlias();
            $profile->save(false);
        }
    }

    public function down()
    {
        echo "m170312_212341_user_alias cannot be reverted.\n";
        return false;
    }
}
