<?php

use yii\db\Schema;
use yii\db\Migration;

class m151002_133802_faq extends Migration {

    public function up() {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%faq_category}}', [
            'id' => Schema::TYPE_PK,
            'parent_id' => Schema::TYPE_INTEGER . ' NOT NULL DEFAULT 0',
            'number' => Schema::TYPE_INTEGER . ' NOT NULL',
            'alias' => Schema::TYPE_STRING . ' NOT NULL',
        ], $tableOptions);

        $this->createTable('{{%faq_category_lang}}', [
            'id' => Schema::TYPE_PK,
            'faq_category_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'language' => Schema::TYPE_STRING . '(6) NOT NULL',
            'name' => Schema::TYPE_STRING . ' NOT NULL'
        ], $tableOptions);
        
        $this->createIndex('faq_category_alias_idx', '{{%faq_category}}', 'alias', true);

        $this->createIndex('faq_category_id', '{{%faq_category_lang}}', 'faq_category_id');
        $this->createIndex('language', '{{%faq_category_lang}}', 'language');
        $this->addForeignKey('fk_cl_faq_category', 'faq_category_lang', 'faq_category_id', 'faq_category', 'id', 'CASCADE', 'CASCADE');
    
        $this->createTable('{{%faq}}', [
            'id' => Schema::TYPE_PK,
            'category_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'number' => Schema::TYPE_INTEGER . ' NOT NULL',
            'featured' => Schema::TYPE_SMALLINT . ' NOT NULL',
        ], $tableOptions);
        
        $this->createIndex('faq_category_id', '{{%faq}}', 'category_id');
        $this->createIndex('faq_featured', '{{%faq}}', 'featured, number');
        
        $this->createTable('{{%faq_lang}}', [
            'id' => Schema::TYPE_PK,
            'faq_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'language' => Schema::TYPE_STRING . '(6) NOT NULL',
            'question' => Schema::TYPE_TEXT . ' NOT NULL',
            'answer' => Schema::TYPE_TEXT . ' NOT NULL',
        ], $tableOptions);
        
        $this->createIndex('faq_id', '{{%faq_lang}}', 'faq_id');
        $this->createIndex('language', '{{%faq_lang}}', 'language');
        
        $this->addForeignKey('fk_cl_faq', 'faq_lang', 'faq_id', 'faq', 'id','CASCADE','CASCADE');
        
    }

    public function down() {        
        $this->dropTable('{{%faq_category_lang}}');
        $this->dropTable('{{%faq_category}}');
        $this->dropTable('{{%faq_lang}}');
        $this->dropTable('{{%faq}}');
    }
}
