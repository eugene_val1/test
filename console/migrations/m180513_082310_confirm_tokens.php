<?php

use yii\db\Migration;

class m180513_082310_confirm_tokens extends Migration
{
    public function safeUp()
    {
        $this->createTable('confirm_tokens', [
            'id' => $this->bigPrimaryKey(11),
            'user_id' => $this->integer(11)->notNull(),
            'type' => $this->string(255)->notNull(),
            'token' => $this->string(30)->notNull(),
            'data' => $this->string(2000),
            'expires' => $this->integer(11)->notNull(),
        ]);

        $this->createIndex('token_idx', 'confirm_tokens', ['token']);
    }

    public function safeDown()
    {
        $this->dropTable('confirm_tokens');
    }
}
