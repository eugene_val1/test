<?php

use yii\db\Schema;
use yii\db\Migration;

class m150714_104949_contact_theme_lang extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%contact_theme_lang}}', [
            'id' => Schema::TYPE_PK,
            'contact_theme_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'language' => Schema::TYPE_STRING . '(6) NOT NULL',
            'title' => Schema::TYPE_STRING . ' NOT NULL',
            'additional_title' => Schema::TYPE_STRING . ' NOT NULL',
        ], $tableOptions);
        
        $this->createIndex('contact_theme_id', '{{%contact_theme_lang}}', 'contact_theme_id');
        $this->createIndex('language', '{{%contact_theme_lang}}', 'language');
        
        $this->addForeignKey('fk_cl_contact_theme', 'contact_theme_lang', 'contact_theme_id', 'contact_theme', 'id','CASCADE','CASCADE');
    }

    public function down()
    {
        $this->dropTable('{{%contact_theme_lang}}');
    }
}
