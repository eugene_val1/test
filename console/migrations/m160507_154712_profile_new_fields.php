<?php
use yii\db\Schema;
use yii\db\Migration;

class m160507_154712_profile_new_fields extends Migration
{
    public function up()
    {
        $this->addColumn('{{%profile}}', 'hide_age', Schema::TYPE_INTEGER . ' NOT NULL AFTER `gender` ');
    }

    public function down()
    {
        $this->dropColumn('{{%profile}}', 'hide_age');
    }
}
