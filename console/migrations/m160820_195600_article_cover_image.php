<?php

use yii\db\Schema;
use yii\db\Migration;

class m160820_195600_article_cover_image extends Migration
{
    public function up()
    {
        $this->addColumn('{{%article}}', 'cover_image', Schema::TYPE_STRING . ' NOT NULL AFTER `image`');
    }

    public function down()
    {
        $this->dropColumn('{{%article}}', 'cover_image');
    }
}
