<?php

use yii\db\Schema;
use yii\db\Migration;

class m150713_181505_page_lang extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%page_lang}}', [
            'id' => Schema::TYPE_PK,
            'page_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'language' => Schema::TYPE_STRING . '(6) NOT NULL',
            'title' => Schema::TYPE_STRING . '(32) NOT NULL',
            'content' => Schema::TYPE_TEXT . ' NOT NULL',
        ], $tableOptions);
        
        $this->createIndex('page_id', '{{%page_lang}}', 'page_id');
        $this->createIndex('language', '{{%page_lang}}', 'language');
        
        $this->addForeignKey('fk_cl_page', 'page_lang', 'page_id', 'page', 'id','CASCADE','CASCADE');
    }

    public function down()
    {
        $this->dropTable('{{%page_lang}}');
    }
}
