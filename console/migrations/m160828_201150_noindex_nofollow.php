<?php

use yii\db\Schema;
use yii\db\Migration;

class m160828_201150_noindex_nofollow extends Migration
{
    public function up()
    {
        $this->addColumn('{{%page}}', 'noindex', Schema::TYPE_INTEGER . ' NOT NULL DEFAULT "0" AFTER `published`');
        $this->addColumn('{{%page}}', 'nofollow', Schema::TYPE_INTEGER . ' NOT NULL DEFAULT "0" AFTER `noindex`');
    }

    public function down()
    {
        $this->dropColumn('{{%page}}', 'noindex');
        $this->dropColumn('{{%page}}', 'nofollow');
    }
}
