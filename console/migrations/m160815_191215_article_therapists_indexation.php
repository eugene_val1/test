<?php

use yii\db\Schema;
use yii\db\Migration;

class m160815_191215_article_therapists_indexation extends Migration
{
    public function up()
    {
        $this->addColumn('{{%article}}', 'index_status', Schema::TYPE_INTEGER . ' NULL DEFAULT "0" AFTER `views_count`');
        $this->addColumn('{{%therapist_lang}}', 'index_status', Schema::TYPE_INTEGER . ' NULL DEFAULT "0" AFTER `terms`');
    }

    public function down()
    {
        $this->dropColumn('{{%article}}', 'index_status');
        $this->dropColumn('{{%therapist_lang}}', 'index_status');
    }
}
