<?php

use yii\db\Schema;
use yii\db\Migration;

class m141128_165624_setting extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%setting}}', [
            'id' => Schema::TYPE_PK,
            'key' => Schema::TYPE_STRING . '(100) NOT NULL',
            'type' => Schema::TYPE_STRING . '(100) NOT NULL',
            'title' => Schema::TYPE_STRING . '(100) NOT NULL',
            'value' => Schema::TYPE_TEXT . ' NOT NULL',
        ], $tableOptions);
        
        $this->createIndex('key_idx', '{{%setting}}', 'key');
        
        $this->insert('{{%setting}}', [
            'key' => 'adminEmail',
            'type' => 'string',
            'title' => 'Emails to send notifications',
            'value'=> 'admin@treatfield.com'
        ]);
    }

    public function down()
    {
        $this->dropTable('{{%setting}}');
    }
}
