<?php

use yii\db\Schema;
use yii\db\Migration;

class m160125_135114_therapist_new_fields extends Migration
{
    public function up()
    {
        $this->addColumn('{{%profile}}', 'gender', Schema::TYPE_STRING . ' DEFAULT NULL AFTER `alias` ');
        $this->update('{{%profile}}', ['gender' => 'male'], 'type = "therapist"');
        
        $this->addColumn('{{%therapist_lang}}', 'spoken_languages', Schema::TYPE_STRING . ' DEFAULT NULL AFTER `full_description` ');
        $this->addColumn('{{%therapist_lang}}', 'terms', Schema::TYPE_TEXT . ' DEFAULT NULL AFTER `spoken_languages` ');
    }

    public function down()
    {
        $this->dropColumn('{{%profile}}', 'gender');
        $this->dropColumn('{{%therapist_lang}}', 'spoken_languages');
        $this->dropColumn('{{%therapist_lang}}', 'terms');
    }
}
