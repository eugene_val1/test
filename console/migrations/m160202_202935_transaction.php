<?php

use yii\db\Schema;
use yii\db\Migration;

class m160202_202935_transaction extends Migration
{
   public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {          
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%transaction}}', [
            'id' => Schema::TYPE_PK,
            'type' => " enum('debit', 'credit')",
            'payment_id' => Schema::TYPE_STRING . ' DEFAULT NULL',
            'user_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'appointment_id' => Schema::TYPE_INTEGER . ' DEFAULT NULL',
            'amount' => Schema::TYPE_INTEGER . ' NOT NULL',
            'reason' => Schema::TYPE_TEXT . ' DEFAULT NULL',
            'status' => " enum('open', 'pending', 'canceled', 'success')",
            'created' => Schema::TYPE_INTEGER . ' NOT NULL'
        ], $tableOptions);
        
        $this->createIndex('transaction_user_id_idx', '{{%transaction}}', 'user_id');
        $this->createIndex('transaction_appointment_id_idx', '{{%transaction}}', 'appointment_id');
        $this->createIndex('transaction_payment_id_idx', '{{%transaction}}', 'payment_id');
    }

    public function down()
    {
        $this->dropTable('{{%transaction}}');
    }
}
