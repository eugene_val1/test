<?php

use yii\db\Schema;
use yii\db\Migration;

class m160125_191024_therapist_language_property extends Migration
{
    public function up()
    {
        $this->dropColumn('{{%therapist_lang}}', 'spoken_languages');
    }

    public function down()
    {
        $this->addColumn('{{%therapist_lang}}', 'spoken_languages', Schema::TYPE_STRING . ' NOT NULL AFTER `full_description` ');
    }
}
