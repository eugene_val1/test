<?php

use yii\db\Migration;

class m170614_213139_update_therapist_table extends Migration
{
    public function up()
    {
        $this->addColumn('profile', 'business_card_url', $this->string(1024)->after('personal_fee'));
    }

    public function down()
    {
        $this->dropColumn('profile', 'business_card_url');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
