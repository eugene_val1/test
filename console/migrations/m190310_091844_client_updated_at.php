<?php

use yii\db\Migration;

/**
 * Class m190310_091844_client_updated_at
 */
class m190310_091844_client_updated_at extends Migration
{
    public function safeUp()
    {
        $this->addColumn(
            'client',
            'last_message_created',
                $this->timestamp()->notNull()->defaultExpression('NOW()')->after('created')
        );

        $this->execute('UPDATE `client` SET `last_message_created` = FROM_UNIXTIME(`created`)');
    }

    public function safeDown()
    {
        $this->dropColumn('client', 'last_message_created');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190310_091844_client_updated_at cannot be reverted.\n";

        return false;
    }
    */
}
