<?php

use yii\db\Schema;
use yii\db\Migration;

class m160227_143900_room_for_appointment extends Migration
{
    public function up()
    {
        $this->addColumn('{{%appointment}}', 'room_id', Schema::TYPE_INTEGER . ' NOT NULL AFTER `user_id` ');
    }

    public function down()
    {
        $this->dropColumn('{{%appointment}}', 'room_id');
    }
}
