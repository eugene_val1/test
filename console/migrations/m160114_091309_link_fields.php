<?php

use yii\db\Schema;
use yii\db\Migration;

class m160114_091309_link_fields extends Migration
{
    public function up()
    {
        $this->addColumn('{{%link}}', 'icon', Schema::TYPE_STRING . ' NOT NULL AFTER `modal` ');
        $this->addColumn('{{%link}}', 'template', Schema::TYPE_INTEGER . ' NOT NULL AFTER `icon` ');
    }

    public function down()
    {
        $this->dropColumn('{{%link}}', 'icon');
        $this->dropColumn('{{%link}}', 'template');
    }
}
