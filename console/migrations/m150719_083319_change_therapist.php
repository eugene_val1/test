<?php

use yii\db\Schema;
use yii\db\Migration;

class m150719_083319_change_therapist extends Migration
{
    public function up()
    {
        $this->addColumn('{{%profile}}', 'alias', Schema::TYPE_STRING . ' NOT NULL AFTER `name` ');
        $this->addColumn('{{%profile}}', 'type', Schema::TYPE_STRING . ' DEFAULT "user" AFTER `name` ');
        $this->addColumn('{{%profile}}', 'catalog_image', Schema::TYPE_STRING . ' NOT NULL AFTER `image` ');
        $this->addColumn('{{%profile}}', 'catalog_image_hover', Schema::TYPE_STRING . ' NOT NULL AFTER `catalog_image` ');
        $this->addColumn('{{%profile}}', 'published', Schema::TYPE_INTEGER . ' NOT NULL AFTER `catalog_image_hover` ');

        $this->dropForeignKey('fk_cl_therapist', '{{%therapist_lang}}');
        $this->dropTable('{{%therapist}}');
        
        $this->createIndex('therapist_alias_idx', '{{%profile}}', 'alias');
        
        $this->dropIndex('therapist_id', '{{%therapist_lang}}');
        $this->renameColumn('{{%therapist_lang}}', 'therapist_id', 'user_id');
        $this->createIndex('therapist_user_id', '{{%therapist_lang}}', 'user_id');
        $this->createIndex('therapist_user_language', '{{%therapist_lang}}', 'user_id, language');
    }

    public function down()
    {
        echo "m150719_083319_change_therapist cannot be reverted.\n";

        return false;
    }
}
