<?php

use common\models\Profile;
use common\models\Therapist;
use yii\db\Migration;

/**
 * Class m190203_134334_therapist_article_description
 */
class m190203_134334_therapist_article_description extends Migration
{
    /**
     * @var int
     */
    private $imageAltLength = 1024;

    /**
     * Up
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('therapist_additional', [
            'id' => $this->bigPrimaryKey(11),
            'user_id' => $this->integer(11)
                ->comment('Therapist profile ID')
                ->notNull(),
            'article_description' => $this
                ->text()
                ->comment('Therapist description for his articles'),
            'image_alt' => $this->string($this->imageAltLength),
            'main_image_alt' => $this->string($this->imageAltLength),
            'catalog_image_alt' => $this->string($this->imageAltLength),

        ], $tableOptions);

        $this->addForeignKey(
            'therapist_additional_user_id_fk',
            'therapist_additional',
            'user_id',
            'profile',
            'user_id',
            'CASCADE',
            'CASCADE'
        );

        $this->addColumn(
            'article',
            'preview_image_alt',
            $this->string($this->imageAltLength)->after('preview_image')
        );
        $this->addColumn(
            'article',
            'cover_image_alt',
            $this->string($this->imageAltLength)->after('preview_image')
        );
        $this->addColumn(
            'article',
            'image_alt',
            $this->string($this->imageAltLength)->after('preview_image')
        );

        $therapists = Profile::find()->all();
        $therapistsAdditionalData = [];
        foreach ($therapists as $therapist) {
            /** @var Therapist $therapist */
            $therapistsAdditionalData[] = [$therapist->user_id];
        }

        Yii::$app
            ->db
            ->createCommand()
            ->batchInsert('therapist_additional', ['user_id'],$therapistsAdditionalData)
            ->execute();
    }

    /**
     * Down
     */
    public function safeDown()
    {
        $this->dropTable('therapist_additional');
        $this->dropColumn('article', 'preview_image_alt');
        $this->dropColumn('article', 'image_alt');
        $this->dropColumn('article', 'cover_image_alt');
    }
}
