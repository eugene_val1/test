<?php

use yii\db\Schema;
use yii\db\Migration;

class m150804_205033_appointment extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%appointment}}', [
            'id' => Schema::TYPE_PK,
            'therapist_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'user_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'number' => Schema::TYPE_INTEGER . ' NOT NULL',
            'start' => Schema::TYPE_INTEGER . ' NOT NULL',
            'end' => Schema::TYPE_INTEGER . ' NOT NULL',
            'comment' => Schema::TYPE_TEXT . ' NOT NULL',
            'rating' => Schema::TYPE_INTEGER . ' NOT NULL',
            'status' => Schema::TYPE_INTEGER . '(2) NOT NULL'
        ], $tableOptions);
        
        $this->createIndex('appointment_idx', '{{%appointment}}', 'therapist_id, user_id');
        $this->createIndex('appointment_status_idx', '{{%appointment}}', 'therapist_id, status');
        $this->createIndex('appointment_start_idx', '{{%appointment}}', 'therapist_id, start');
    }

    public function down()
    {
        $this->dropTable('{{%appointment}}');
    }
}
