<?php

use yii\db\Schema;
use yii\db\Migration;

class m151207_144005_articles_columns extends Migration
{
    public function up()
    {
        $this->addColumn('{{%article}}', 'alias', Schema::TYPE_STRING . ' NOT NULL AFTER `title` ');
        $this->alterColumn('{{%article}}', 'published', Schema::TYPE_STRING);
        $this->update('{{%article}}', ['published' => 'pending']);
    }

    public function down()
    {
        $this->alterColumn('{{%article}}', 'published', Schema::TYPE_INTEGER);
        $this->dropColumn('{{%article}}', 'alias');
    }
}
