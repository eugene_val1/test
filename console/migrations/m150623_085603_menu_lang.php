<?php

use yii\db\Schema;
use yii\db\Migration;

class m150623_085603_menu_lang extends Migration
{
public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%menu_lang}}', [
            'id' => Schema::TYPE_PK,
            'menu_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'language' => Schema::TYPE_STRING . '(6) NOT NULL',
            'title' => Schema::TYPE_STRING . '(32) NOT NULL'
        ], $tableOptions);
        
        $this->createIndex('menu_id', '{{%menu_lang}}', 'menu_id');
        $this->createIndex('language', '{{%menu_lang}}', 'language');
        
        $this->addForeignKey('fk_cl_menu', 'menu_lang', 'menu_id', 'menu', 'id','CASCADE','CASCADE');
    }

    public function down()
    {
        $this->dropTable('{{%menu_lang}}');
    }
}
