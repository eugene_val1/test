<?php

use yii\db\Schema;
use yii\db\Migration;

class m160227_143840_room_lock extends Migration
{
    public function up()
    {
        $this->addColumn('{{%room}}', 'type', Schema::TYPE_STRING . ' DEFAULT NULL AFTER `alias` ');
        $this->addColumn('{{%room}}', 'password', Schema::TYPE_STRING . ' DEFAULT NULL AFTER `type` ');      
    }

    public function down()
    {
        $this->dropColumn('{{%room}}', 'password');
        $this->dropColumn('{{%room}}', 'type');
    }
}
