<?php

use yii\db\Schema;
use yii\db\Migration;

class m150623_090349_link_lang extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%link_lang}}', [
            'id' => Schema::TYPE_PK,
            'link_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'language' => Schema::TYPE_STRING . '(6) NOT NULL',
            'title' => Schema::TYPE_STRING . '(500) NOT NULL'
        ], $tableOptions);
        
        $this->createIndex('link_id', '{{%link_lang}}', 'link_id');
        $this->createIndex('language', '{{%link_lang}}', 'language');
        
        $this->addForeignKey('fk_cl_link', 'link_lang', 'link_id', 'link', 'id','CASCADE','CASCADE');
    }

    public function down()
    {
        $this->dropTable('{{%link_lang}}');
    }
}
