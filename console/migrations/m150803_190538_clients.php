<?php

use yii\db\Schema;
use yii\db\Migration;

class m150803_190538_clients extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%client}}', [
            'id' => Schema::TYPE_PK,
            'therapist_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'user_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'conversation_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'status' => Schema::TYPE_INTEGER . '(2) NOT NULL',
            'created' => Schema::TYPE_INTEGER . ' NOT NULL',
            'next_appointment_start' => Schema::TYPE_INTEGER . ' DEFAULT NULL'
        ], $tableOptions);
        
        $this->createIndex('client_idx', '{{%client}}', 'therapist_id, user_id');
        $this->createIndex('client_status_idx', '{{%client}}', 'therapist_id, status');
    }

    public function down()
    {
        $this->dropTable('{{%client}}');
    }
}
