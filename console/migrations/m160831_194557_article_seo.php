<?php

use yii\db\Schema;
use yii\db\Migration;
use common\models\Article;
use yii\db\Expression;

class m160831_194557_article_seo extends Migration
{
    public function up()
    {
        $this->addColumn('{{%article}}', 'page_title', Schema::TYPE_TEXT . ' NULL AFTER `views_count` ');
        $this->addColumn('{{%article}}', 'page_keywords', Schema::TYPE_TEXT . ' NULL AFTER `page_title` ');
        $this->addColumn('{{%article}}', 'page_description', Schema::TYPE_TEXT . ' NULL AFTER `page_keywords` ');
    
        Article::updateAll(['page_title' => new Expression('title')]);
    }

    public function down()
    {
        $this->dropColumn('{{%article}}', 'page_title');
        $this->dropColumn('{{%article}}', 'page_keywords');
        $this->dropColumn('{{%article}}', 'page_description');
    }
}
