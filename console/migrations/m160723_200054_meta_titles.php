<?php

use yii\db\Schema;
use yii\db\Migration;
use common\models\PageLang;
use yii\db\Expression;

class m160723_200054_meta_titles extends Migration
{
    public function up()
    {
        $this->addColumn('{{%page_lang}}', 'name', Schema::TYPE_STRING . ' DEFAULT NULL AFTER `language`');
        
        $this->insert('{{%page}}', [
            'type' => 'system',
            'section' => 'home',
            'published' => '1'
        ]);
        
        $page = (new \yii\db\Query())
            ->select(['id'])
            ->from('page')
            ->where(['type' => 'system', 'section' => 'home'])
            ->one();
        
        $this->batchInsert('{{%page_lang}}', ['page_id', 'language', 'title'], [
            [$page['id'], 'ru-RU', 'Домашняя страница'],
            [$page['id'], 'en-US', 'Home page']
        ]);
        
        PageLang::updateAll(['name' => new Expression('title')]);
    }

    public function down()
    {
        $this->dropColumn('{{%page_lang}}', 'name');
    }
}
