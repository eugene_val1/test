<?php

use yii\db\Schema;
use yii\db\Migration;

class m151208_091126_like_system extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->addColumn('{{%article}}', 'votes_count', Schema::TYPE_INTEGER . ' NOT NULL AFTER `published` ');
        
        $this->createTable('{{%vote}}', [
            'user_id' => Schema::TYPE_INTEGER . ' NOT NULL',            
            'target' => Schema::TYPE_STRING . ' NOT NULL',
            'target_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'value' => Schema::TYPE_INTEGER . ' NOT NULL'
        ], $tableOptions);
        
        $this->createIndex('vote_target_idx', '{{%vote}}', 'target, target_id');
        $this->addForeignKey('user_vote_fk', 'vote', 'user_id', 'user', 'id', 'CASCADE', 'CASCADE');
        
        $this->addPrimaryKey('vote_user_target_pk', 'vote', ['user_id', 'target_id', 'target']);
    }

    public function down()
    {
        $this->dropColumn('{{%article}}', 'votes_count');
        $this->dropTable('{{%vote}}');
    }
}
