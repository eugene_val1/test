<?php

use yii\db\Migration;

class m170619_113722_add_convert_amounts_to_transactions extends Migration
{
    public function safeUp()
    {
        $this->addColumn('transaction', 'converted_amounts', $this->string(512)->after('ratio'));

        $transactions = \common\models\Transaction::find()->where('ratio IS NOT NULL')->all();
        //$ratio = \common\components\payments\WayForPay::getConvertRatio();
        foreach ($transactions as $transaction) {
            $transaction->converted_amounts = json_encode([
                'amount' => $transaction->convertAmount($transaction->amount),
                'fee' => $transaction->convertAmount($transaction->fee),
                'payment_fee' => $transaction->convertAmount($transaction->payment_fee)
            ]);
            $transaction->save(false);
        }
    }

    public function safeDown()
    {
        $this->dropColumn('transaction', 'converted_amounts');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170619_113722_add_convert_amounts_to_transactions cannot be reverted.\n";

        return false;
    }
    */
}
