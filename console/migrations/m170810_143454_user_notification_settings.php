<?php

use yii\db\Migration;

class m170810_143454_user_notification_settings extends Migration
{
    public function safeUp()
    {
        $this->execute("
            CREATE TABLE `notification_settings` (
              `user_id` int(11) NOT NULL,
              `messages` tinyint(2) DEFAULT '1',
              `appointment_new` tinyint(2) DEFAULT '1',
              `appointment_rescheduled` tinyint(2) DEFAULT '1',
              `appointment_canceled` tinyint(2) DEFAULT '1',
              `appointment_completed` tinyint(2) DEFAULT '1',
              `appointment_missed` tinyint(2) DEFAULT '1',
              `appointment_paid` tinyint(2) DEFAULT '1',
              `appointment_not_paid` tinyint(2) DEFAULT '1',
              PRIMARY KEY (`user_id`),
              UNIQUE KEY `user_id_UNIQUE` (`user_id`),
              CONSTRAINT `user_id-user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        ");

        $userIDs = \yii\helpers\ArrayHelper::getColumn(\common\models\User::find()->select('id')->all(), 'id');

        $records = [];
        foreach ($userIDs as $userID) {
            $records[] = [$userID];
        }

        Yii::$app->db->createCommand()->batchInsert('notification_settings', ['user_id'], $records)->execute();
    }

    public function safeDown()
    {
        $this->delete('notification_settings');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170810_143454_user_notification_settings cannot be reverted.\n";

        return false;
    }
    */
}
