<?php

use yii\db\Migration;

class m160704_195333_payment_currency extends Migration
{
    public function up()
    {
        $this->insert('{{%setting}}', [
            'key' => 'currency',
            'category' => 'payment',
            'type' => 'string',
            'title' => 'Payment currency',
            'value'=> 'USD'
        ]);
    }

    public function down()
    {
        echo "m160704_195333_payment_currency cannot be reverted.\n";
        return false;
    }
}
