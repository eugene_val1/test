<?php

use yii\db\Migration;

class m170616_104554_update_transaction_table extends Migration
{
    public function up()
    {
        $this->addColumn('transaction', 'ratio', $this->double()->after('fee')->comment('Ratio for exchange'));
    }

    public function down()
    {
        $this->dropColumn('transaction', 'ratio');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
