<?php
use yii\db\Schema;
use yii\db\Migration;

class m160510_062412_features_url extends Migration
{
    public function up()
    {
        $this->addColumn('{{%feature}}', 'url', Schema::TYPE_STRING . ' DEFAULT NULL AFTER `image` ');
    }

    public function down()
    {
        $this->dropColumn('{{%feature}}', 'url');
    }
}
