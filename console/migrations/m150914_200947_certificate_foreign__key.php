<?php

use yii\db\Migration;

class m150914_200947_certificate_foreign__key extends Migration
{
    public function up()
    {
        $this->addForeignKey('fk_cl_therapist_certificate', 'certificate', 'therapist_id', 'user', 'id','CASCADE','CASCADE');
    }

    public function down()
    {
        echo "m150914_200947_certificate_foreign__key cannot be reverted.\n";

        return false;
    }
}
