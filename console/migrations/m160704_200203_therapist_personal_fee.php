<?php

use yii\db\Schema;
use yii\db\Migration;

class m160704_200203_therapist_personal_fee extends Migration
{
    public function up()
    {
        $this->addColumn('{{%profile}}', 'personal_fee', Schema::TYPE_STRING . ' DEFAULT NULL AFTER `balance`');
    }

    public function down()
    {
        $this->dropColumn('{{%profile}}', 'personal_fee');
    }
}
