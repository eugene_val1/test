<?php

use yii\db\Schema;
use yii\db\Migration;

class m160711_194150_payment_changes extends Migration
{
    public function up()
    {
        $this->dropColumn('{{%profile}}', 'balance');
        $this->addColumn('{{%transaction}}', 'currency', Schema::TYPE_STRING . ' DEFAULT NULL AFTER `amount`');
        $this->addColumn('{{%transaction}}', 'settled', Schema::TYPE_INTEGER . '(1) NOT NULL DEFAULT 0 AFTER `reason`');
        $this->insert('{{%setting}}', [
            'key' => 'wayforpayApiVersion',
            'category' => 'payment',
            'type' => 'string',
            'title' => 'WayForPay api version',
            'value'=> '1'
        ]);
    }

    public function down()
    {
        echo "m160711_194150_payment_changes cannot be reverted.\n";

        return false;
    }
}
