<?php

use yii\db\Schema;
use yii\db\Migration;

class m160125_092827_therapist_duration extends Migration
{
    public function up()
    {
        $this->update('{{%profile}}', ['duration' => 60], '`duration` = 0 AND type = "therapist"');
        
        $this->batchInsert('{{%setting}}', ['key', 'category', 'type', 'title', 'value'], [
            ['defaultDuration', 'therapist', 'string', 'Default therapist session duration (min)', 60],
            ['minDuration', 'therapist', 'string', 'Minimal therapist session duration (min)', 15],
            ['maxDuration', 'therapist', 'string', 'Maximal therapist session duration (min)', 480],
        ]);
    }

    public function down()
    {
        echo "m160121_124701_new_settings cannot be reverted.\n";
        return false;
    }
}
