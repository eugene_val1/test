<?php

use yii\db\Schema;
use yii\db\Migration;

class m151209_154637_event_inform extends Migration
{
    public function up()
    {
        $this->addColumn('{{%event}}', 'informed', Schema::TYPE_INTEGER . ' NOT NULL AFTER `created` ');
    }

    public function down()
    {
        $this->dropColumn('{{%event}}', 'informed');
    }
}
