<?php
use yii\db\Schema;
use yii\db\Migration;

class m170312_165627_lang_enable extends Migration
{
    public function up()
    {
        $this->addColumn('{{%language}}', 'active', Schema::TYPE_INTEGER . ' NULL DEFAULT "1" AFTER `image`');
    }

    public function down()
    {
        $this->dropColumn('{{%language}}', 'active');
    }
}