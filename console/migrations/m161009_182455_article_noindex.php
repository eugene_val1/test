<?php
use yii\db\Schema;
use yii\db\Migration;

class m161009_182455_article_noindex extends Migration
{
    public function up()
    {
        $this->addColumn('{{%article}}', 'noindex', Schema::TYPE_INTEGER . ' NULL DEFAULT "0" AFTER `page_description`');
        $this->addColumn('{{%article}}', 'nofollow', Schema::TYPE_INTEGER . ' NULL DEFAULT "0" AFTER `noindex`');
    }

    public function down()
    {
        $this->dropColumn('{{%article}}', 'noindex');
        $this->dropColumn('{{%article}}', 'nofollow');
    }
}
