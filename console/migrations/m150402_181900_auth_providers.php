<?php

use yii\db\Schema;
use yii\db\Migration;

class m150402_181900_auth_providers extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%auth_provider}}', [
            'user_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'name' => Schema::TYPE_STRING . '(100) NOT NULL',
            'provider_id' => Schema::TYPE_STRING . '(100) NOT NULL',
        ], $tableOptions);
        
        $this->addPrimaryKey('id', 'auth_provider', ['name', 'provider_id']);
    }

    public function down()
    {
        $this->dropTable('{{%auth_provider}}');
    }
}
