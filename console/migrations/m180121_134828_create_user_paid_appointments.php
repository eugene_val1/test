<?php

use yii\db\Migration;

class m180121_134828_create_user_paid_appointments extends Migration
{
    public function safeUp()
    {
        $this->createTable('user_paid_appointments', [
            'id' => $this->primaryKey(11),
            'user_id' => $this->integer(11)->notNull(),
            'amount' => $this->smallInteger()->notNull()->defaultValue(1)
        ]);

        $this->addForeignKey('user_id_idx', 'user_paid_appointments', 'user_id', 'user', 'id', 'CASCADE', 'CASCADE');

        $this->addColumn('transaction', 'appointmentAmount', $this->smallInteger()->defaultValue(1));
    }

    public function safeDown()
    {
        $this->dropTable('user_paid_appointments');
        $this->dropColumn('transaction', 'appointmentAmount');
    }
}
