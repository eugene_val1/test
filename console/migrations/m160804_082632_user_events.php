<?php

use yii\db\Schema;
use yii\db\Migration;

class m160804_082632_user_events extends Migration
{
    public function up()
    {
        $this->addColumn('{{%event}}', 'target', Schema::TYPE_INTEGER . ' NOT NULL DEFAULT "0" AFTER `type`');
    }

    public function down()
    {
        $this->dropColumn('{{%event}}', 'target');
    }
}
