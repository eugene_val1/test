<?php

use yii\db\Schema;
use yii\db\Migration;

class m160705_203435_transaction_fields extends Migration
{
    public function up()
    {
        $this->dropColumn('{{%transaction}}', 'type');
        $this->addColumn('{{%transaction}}', 'therapist_id', Schema::TYPE_INTEGER . ' NOT NULL AFTER `user_id`');
        $this->addColumn('{{%transaction}}', 'fee', Schema::TYPE_FLOAT . '(10,2) AFTER `amount`');
        $this->addColumn('{{%transaction}}', 'payment_fee', Schema::TYPE_FLOAT . '(10,2) AFTER `amount`');
        $this->alterColumn('{{%transaction}}', 'amount', Schema::TYPE_FLOAT . '(10,2)');
        
        $this->insert('{{%setting}}', [
            'key' => 'wayforpayFee',
            'category' => 'payment',
            'type' => 'string',
            'title' => 'Payment fee',
            'value'=> '2.5'
        ]);
    }

    public function down()
    {
        echo "m160705_203435_transaction_fields cannot be reverted.\n";
        return false;
    }
}
