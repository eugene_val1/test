<?php

use yii\db\Schema;
use yii\db\Migration;

class m151215_204946_comment_status extends Migration
{
    public function up()
    {
        $this->renameColumn('{{%comment}}', 'deleted', 'status');
    }

    public function down()
    {
        $this->renameColumn('{{%comment}}', 'status', 'deleted');
    }
}
