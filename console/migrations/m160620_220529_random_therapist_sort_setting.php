<?php

use yii\db\Migration;

class m160620_220529_random_therapist_sort_setting extends Migration
{
    public function up()
    {
        $this->insert('{{%setting}}', [
            'key' => 'therapistRandomSort',
            'category' => 'therapist',
            'type' => 'checkbox',
            'title' => 'Use random sort',
            'value'=> 0
        ]);
    }

    public function down()
    {
        echo "m160620_220529_random_therapist_sort_setting cannot be reverted.\n";

        return false;
    }
}
