<?php

use yii\db\Schema;
use yii\db\Migration;

class m150713_142955_country_lang extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%country_lang}}', [
            'id' => Schema::TYPE_PK,
            'country_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'language' => Schema::TYPE_STRING . '(6) NOT NULL',
            'name' => Schema::TYPE_STRING . ' NOT NULL'
        ], $tableOptions);
        
        $this->createIndex('country_id', '{{%country_lang}}', 'country_id');
        $this->createIndex('language', '{{%country_lang}}', 'language');
        
        $this->addForeignKey('fk_cl_country', 'country_lang', 'country_id', 'country', 'id','CASCADE','CASCADE');
    }

    public function down()
    {
        $this->dropTable('{{%country_lang}}');
    }
}
