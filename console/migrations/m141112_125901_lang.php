<?php

use yii\db\Schema;
use yii\db\Migration;

class m141112_125901_lang extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%language}}', [
            'id' => Schema::TYPE_PK,
            'url' => Schema::TYPE_STRING . '(255) NOT NULL',
            'local' => Schema::TYPE_STRING . '(255) NOT NULL',
            'name' => Schema::TYPE_STRING . '(255) NOT NULL',
            'image' => Schema::TYPE_STRING . " NOT NULL DEFAULT '' ",
        ], $tableOptions);

    $this->batchInsert('language', ['url', 'local', 'name'], [
        ['en', 'en-US', 'English'],
        ['ru', 'ru-RU', 'Русский'],
    ]);
    }

    public function down()
    {
        $this->dropTable('{{%language}}');
    }
}
