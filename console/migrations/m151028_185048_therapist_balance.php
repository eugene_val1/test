<?php

use yii\db\Schema;
use yii\db\Migration;

class m151028_185048_therapist_balance extends Migration
{
    public function up()
    {
        $this->addColumn('{{%appointment}}', 'amount', Schema::TYPE_INTEGER . ' NOT NULL AFTER `rating` ');
        $this->addColumn('{{%profile}}', 'balance', Schema::TYPE_INTEGER . ' NOT NULL AFTER `duration` ');
    }

    public function down()
    {
        $this->dropColumn('{{%appointment}}', 'amount');
        $this->dropColumn('{{%profile}}', 'balance');
    }
}
