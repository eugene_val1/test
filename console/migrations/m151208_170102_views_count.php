<?php

use yii\db\Schema;
use yii\db\Migration;

class m151208_170102_views_count extends Migration
{
    public function up()
    {
        $this->addColumn('{{%article}}', 'views_count', Schema::TYPE_INTEGER . ' NOT NULL AFTER `votes_count` ');
    }

    public function down()
    {
        $this->dropColumn('{{%article}}', 'views_count');
    }
}
