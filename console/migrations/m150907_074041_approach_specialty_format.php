<?php

use yii\db\Schema;
use yii\db\Migration;

class m150907_074041_approach_specialty_format extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {          
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%specialty}}', [
            'id' => Schema::TYPE_PK,
            'number' => Schema::TYPE_INTEGER . ' NOT NULL',
            'default' => Schema::TYPE_INTEGER . ' NOT NULL'
        ], $tableOptions);
        
        $this->createTable('{{%specialty_lang}}', [
            'id' => Schema::TYPE_PK,
            'specialty_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'language' => Schema::TYPE_STRING . '(6) NOT NULL',
            'name' => Schema::TYPE_STRING . ' NOT NULL'
        ], $tableOptions);
        
        $this->createIndex('specialty_id', '{{%specialty_lang}}', 'specialty_id');
        $this->createIndex('language', '{{%specialty_lang}}', 'language');
        
        $this->createTable('{{%approach}}', [
            'id' => Schema::TYPE_PK,
            'number' => Schema::TYPE_INTEGER . ' NOT NULL',
            'default' => Schema::TYPE_INTEGER . ' NOT NULL'
        ], $tableOptions);
        
        $this->createTable('{{%approach_lang}}', [
            'id' => Schema::TYPE_PK,
            'approach_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'language' => Schema::TYPE_STRING . '(6) NOT NULL',
            'name' => Schema::TYPE_STRING . ' NOT NULL'
        ], $tableOptions);
        
        $this->createIndex('approach_id', '{{%approach_lang}}', 'approach_id');
        $this->createIndex('language', '{{%approach_lang}}', 'language');
        
        $this->createTable('{{%format}}', [
            'id' => Schema::TYPE_PK,
            'number' => Schema::TYPE_INTEGER . ' NOT NULL',
            'default' => Schema::TYPE_INTEGER . ' NOT NULL'
        ], $tableOptions);
        
        $this->createTable('{{%format_lang}}', [
            'id' => Schema::TYPE_PK,
            'format_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'language' => Schema::TYPE_STRING . '(6) NOT NULL',
            'name' => Schema::TYPE_STRING . ' NOT NULL'
        ], $tableOptions);
        
        $this->createIndex('format_id', '{{%format_lang}}', 'format_id');
        $this->createIndex('language', '{{%format_lang}}', 'language');

        $this->addColumn('{{%therapist_lang}}', 'specialty', Schema::TYPE_TEXT . ' NOT NULL AFTER `experience` ');
        $this->addColumn('{{%therapist_lang}}', 'approach', Schema::TYPE_TEXT . ' NOT NULL AFTER `specialty` ');
        $this->addColumn('{{%therapist_lang}}', 'format', Schema::TYPE_TEXT . ' NOT NULL AFTER `approach` ');
    }

    public function down()
    {
        $this->dropTable('{{%specialty}}');
        $this->dropTable('{{%specialty_lang}}');
        $this->dropTable('{{%approach}}');
        $this->dropTable('{{%approach_lang}}');
        $this->dropTable('{{%format}}');
        $this->dropTable('{{%format_lang}}');
    }
}
