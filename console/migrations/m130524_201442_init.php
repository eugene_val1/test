<?php

use yii\db\Schema;
use yii\db\Migration;

class m130524_201442_init extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%user}}', [
            'id' => Schema::TYPE_PK,
            'username' => Schema::TYPE_STRING . ' NOT NULL',
            'auth_key' => Schema::TYPE_STRING . '(32) NOT NULL',
            'password_hash' => Schema::TYPE_STRING . ' NOT NULL',
            'token' => Schema::TYPE_STRING,
            'email' => Schema::TYPE_STRING . ' NOT NULL',
            'country_id' => Schema::TYPE_INTEGER . ' NOT NULL DEFAULT 0',
            'role' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 1',
            'status' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 10',
            'created' => Schema::TYPE_INTEGER . ' NOT NULL',
            'updated' => Schema::TYPE_INTEGER . ' NOT NULL',
        ], $tableOptions);
        
        $this->insert('{{%user}}', [
            'id'=>1,
            'username'=>'admin',
            'email'=>'admin@example.com',
            'password_hash'=>Yii::$app->getSecurity()->generatePasswordHash('admin'),
            'auth_key'=>Yii::$app->getSecurity()->generateRandomString(),
            'role'=>\common\models\User::ROLE_ADMIN,
            'status'=>\common\models\User::STATUS_ACTIVE,
            'created'=>time(),
            'updated'=>time()
        ]);
    }

    public function down()
    {
        $this->dropTable('{{%user}}');
    }
}
