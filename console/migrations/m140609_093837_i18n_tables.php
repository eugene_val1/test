<?php

use yii\base\InvalidConfigException;
use yii\db\Schema;
use yii\db\Migration;

class m140609_093837_i18n_tables extends Migration
{
    /**
     * @return bool|void
     * @throws InvalidConfigException
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%source_message}}', [
            'id' => Schema::TYPE_PK,
            'category' => 'varchar(32) null',
            'type' => "enum('string','text') NOT NULL DEFAULT 'string'",
            'message' => 'text null'
        ], $tableOptions);
        
        $this->createTable('{{%message}}', [
            'id' => Schema::TYPE_INTEGER . ' not null default 0',
            'language' => 'varchar(16) not null default ""',
            'translation' => 'text null'
        ], $tableOptions);
        
        $this->addPrimaryKey('id', 'message', ['id', 'language']);
        $this->addForeignKey('fk_source_message_message', 'message', 'id', 'source_message', 'id', 'cascade');
    }
    public function down()
    {
        echo 'm140609_093837_i18n_tables cannot be reverted.' . PHP_EOL;
        return false;
    }
}
