<?php

use yii\db\Schema;
use yii\db\Migration;

class m160121_124701_new_settings extends Migration
{
    public function up()
    {
        $this->addColumn('{{%setting}}', 'category', Schema::TYPE_STRING . ' NOT NULL AFTER `key` ');
        
        $this->update('{{%setting}}', ['category' => 'main'], 'type = :type', [':type' => 'multilangual']);
        $this->update('{{%setting}}', ['category' => 'notification'], '`key` = :key', [':key' => 'adminEmail']);
        $this->update('{{%setting}}', ['category' => 'seo'], '`key` = :key', [':key' => 'footerContent']);
        
        $this->insert('{{%setting}}', [
            'key' => 'therapistChoosePage',
            'category' => 'therapist',
            'type' => 'string',
            'title' => 'Link to how to choose therapist page',
            'value'=> ''
        ]);
    }

    public function down()
    {
        echo "m160121_124701_new_settings cannot be reverted.\n";
        return false;
    }
}
