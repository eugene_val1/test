<?php

use yii\db\Schema;
use yii\db\Migration;

class m150908_054142_review extends Migration
{
    public function up()
    {
        $this->addForeignKey('fk_cl_specialty', 'specialty_lang', 'specialty_id', 'specialty', 'id','CASCADE','CASCADE');
        $this->addForeignKey('fk_cl_approach', 'approach_lang', 'approach_id', 'approach', 'id','CASCADE','CASCADE');
        $this->addForeignKey('fk_cl_format', 'format_lang', 'format_id', 'format', 'id','CASCADE','CASCADE');
        
        $this->renameColumn('{{%profile}}', 'published', 'publish_status');
    }

    public function down()
    {
        echo "m150908_054142_review cannot be reverted.\n";

        return false;
    }
}
