<?php

use yii\db\Schema;
use yii\db\Migration;

class m150528_130506_therapist_lang extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%therapist_lang}}', [
            'id' => Schema::TYPE_PK,
            'therapist_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'language' => Schema::TYPE_STRING . '(6) NOT NULL',
            'name' => Schema::TYPE_STRING . '(32) NOT NULL',
            'short_description' => Schema::TYPE_TEXT . ' NOT NULL',
            'full_description' => Schema::TYPE_TEXT . ' NOT NULL'
        ], $tableOptions);
        
        $this->createIndex('therapist_id', '{{%therapist_lang}}', 'therapist_id');
        $this->createIndex('language', '{{%therapist_lang}}', 'language');
        
        $this->addForeignKey('fk_cl_therapist', 'therapist_lang', 'therapist_id', 'therapist', 'id','CASCADE','CASCADE');
    }

    public function down()
    {
        $this->dropTable('{{%therapist_lang}}');
    }
}
