<?php

use yii\db\Schema;
use yii\db\Migration;

class m150623_090341_link extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {          
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%link}}', [
            'id' => Schema::TYPE_PK,
            'menu_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'number' => Schema::TYPE_INTEGER . ' NOT NULL',
            'url' => Schema::TYPE_STRING . ' NOT NULL',
            'modal' => Schema::TYPE_INTEGER . ' NOT NULL'
        ], $tableOptions);
        
        $this->createIndex('link_menu_id', '{{%link}}', 'menu_id');
        $this->createIndex('menu_number', '{{%link}}', 'menu_id, number');
    }

    public function down()
    {
        $this->dropTable('{{%link}}');
    }
}
