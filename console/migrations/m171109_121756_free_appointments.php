<?php

use yii\db\Migration;

class m171109_121756_free_appointments extends Migration
{
    public function safeUp()
    {
        $this->addColumn('appointment', 'free', $this->boolean()->defaultValue(false)->after('payment_status'));
        $this->addColumn('profile', 'free_appointment', $this->boolean()->defaultValue(false));

        $this->insert('setting', [
            'key' => 'allowFreeAppointment',
            'category' => 'main',
            'type' => 'checkbox',
            'title' => 'Allow free appointments',
            'value' => 1
        ]);
    }

    public function safeDown()
    {
        $this->dropColumn('appointment', 'free');
        $this->dropColumn('profile', 'free_appointment');
        $this->delete('setting', ['key' => 'allowFreeAppointment']);
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171109_121756_free_appointments cannot be reverted.\n";

        return false;
    }
    */
}
