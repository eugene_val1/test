<?php

use yii\db\Schema;
use yii\db\Migration;

class m160205_121715_video_rooms extends Migration
{
   public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {          
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%room}}', [
            'id' => Schema::TYPE_PK,
            'user_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'name' => Schema::TYPE_STRING . ' DEFAULT NULL',
            'alias' => Schema::TYPE_STRING . ' DEFAULT NULL',
            'status' => Schema::TYPE_STRING . ' DEFAULT NULL',
            'created' => Schema::TYPE_INTEGER . ' NOT NULL'
        ], $tableOptions);
        
        $this->createIndex('room_user_id_idx', '{{%room}}', 'user_id');
        $this->createIndex('room_alias_idx', '{{%room}}', 'alias');
    }

    public function down()
    {
        $this->dropTable('{{%room}}');
    }
}
