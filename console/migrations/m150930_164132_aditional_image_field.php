<?php

use yii\db\Schema;
use yii\db\Migration;

class m150930_164132_aditional_image_field extends Migration
{
    public function up()
    {
        $this->addColumn('{{%profile}}', 'main_image', Schema::TYPE_STRING . ' NOT NULL AFTER `image` ');
    }

    public function down()
    {
        $this->dropColumn('{{%profile}}', 'main_image');
    }
}
