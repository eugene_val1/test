<?php

use yii\db\Schema;
use yii\db\Migration;

class m150714_122827_contact extends Migration
{
    public function up() {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%contact}}', [
            'id' => Schema::TYPE_PK,
            'email' => Schema::TYPE_STRING . '(255) NOT NULL',
            'name' => Schema::TYPE_STRING . '(255) NOT NULL',
            'country_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'theme_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'theme' => Schema::TYPE_STRING . '(500) NOT NULL',
            'message' => Schema::TYPE_TEXT . ' NOT NULL',
            'created' => Schema::TYPE_INTEGER . ' NOT NULL',
            'read' => Schema::TYPE_INTEGER . ' NOT NULL'
        ], $tableOptions);
    }

    public function down() {
        $this->dropTable('{{%contact}}');
    }
}
