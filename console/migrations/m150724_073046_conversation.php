<?php

use yii\db\Schema;
use yii\db\Migration;

class m150724_073046_conversation extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%conversation}}', [
            'id' => Schema::TYPE_PK,
            'sender_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'receiver_id' => Schema::TYPE_INTEGER . ' NOT NULL',
        ], $tableOptions);
        
        $this->createTable('{{%conversation_message}}', [
            'id' => Schema::TYPE_PK,
            'conversation_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'user_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'text' => Schema::TYPE_TEXT . ' NOT NULL',
            'status' => Schema::TYPE_INTEGER . '(2) NOT NULL',
            'created' => Schema::TYPE_INTEGER . ' NOT NULL',
        ], $tableOptions);
        
        $this->createIndex('sender_receiver_idx', '{{%conversation}}', 'sender_id, receiver_id');
        $this->createIndex('conversation_idx', '{{%conversation_message}}', 'conversation_id');
    }

    public function down()
    {
        $this->dropTable('{{%conversation}}');
        $this->dropTable('{{%conversation_message}}');
    }
}
