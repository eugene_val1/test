<?php

use common\components\UploadedFileBehavior;
use common\models\Profile;
use yii\db\Migration;

/**
 * Class m180909_102433_fix_social_images
 */
class m180909_102433_fix_social_images extends Migration
{
    /**
     * Up
     */
    public function safeUp()
    {
        $profiles = Profile::find()
            ->where(['like', 'image', 'http://'])
            ->orWhere(['like', 'image', 'https://'])
            ->all();

        foreach ($profiles as $profile) {
            $profile->resizeType = UploadedFileBehavior::RESIZE_NONE;
            $result = $profile->saveRemoteImage('image', $profile->image);
            if (!$result) {
                unset($profile->image);
            }

            $profile->save(false);
        }
    }

    /**
     * Down
     *
     * @return bool
     */
    public function safeDown()
    {
        echo "m180909_102433_fix_social_images cannot be reverted.\n";
    }
}
