<?php

use yii\db\Migration;

class m171016_112653_add_user_rectoken extends Migration
{
    public function safeUp()
    {
        $this->insert('setting', [
            'key' => 'useRecToken',
            'category' => 'payment',
            'type' => 'checkbox',
            'title' => 'Use one-click payment',
            'value' => 1
        ]);

        $this->addColumn('profile', 'rec_token', $this->string(255));
    }

    public function safeDown()
    {
        $this->delete('setting', ['key' => 'useRecToken']);
        $this->dropColumn('user', 'rec_token');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171016_112653_add_user_rectoken cannot be reverted.\n";

        return false;
    }
    */
}
