<?php

use common\models\NotificationSettings;
use yii\db\Migration;

/**
 * Class m180224_213607_user_bots
 */
class m180224_213607_user_bots extends Migration
{
    /**
     * Up
     */
    public function safeUp()
    {
        $this->createTable('user_bots', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(11),
            'type' => $this->string(30),
            'target_token' => $this->string(255),
            'created' => $this->timestamp()->notNull()->defaultExpression('NOW()')
        ]);

        $this->addForeignKey('user_id_id_idx', 'user_bots', 'user_id', 'user', 'id', 'CASCADE', 'CASCADE');
        $this->createIndex('user-id_type_idx', 'user_bots', ['user_id', 'type'], true);

        NotificationSettings::updateAll(['appointment_canceled' => 0]);

        $this->addColumn(
            'notification_settings',
            'appointment_reminder',
            $this->boolean()->defaultValue(true)
        );
        $this->addColumn(
            'notification_settings',
            'target_email',
            $this->boolean()->defaultValue(true)
        );
        $this->addColumn(
            'notification_settings',
            'target_bots',
            $this->boolean()->defaultValue(true)
        );
    }

    /**
     * Down
     */
    public function safeDown()
    {
        $this->dropTable('user_bots');
        $this->dropColumn('notification_settings', 'appointment_reminder');
        $this->dropColumn('notification_settings', 'target_email');
        $this->dropColumn('notification_settings', 'target_bots');
    }
}
