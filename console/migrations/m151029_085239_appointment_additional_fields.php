<?php

use yii\db\Schema;
use yii\db\Migration;

class m151029_085239_appointment_additional_fields extends Migration
{
    public function up()
    {
        $this->addColumn('{{%appointment}}', 'duration', Schema::TYPE_INTEGER . ' NOT NULL AFTER `end` ');
        $this->addColumn('{{%appointment}}', 'created', Schema::TYPE_INTEGER . ' NOT NULL AFTER `status` ');
    }

    public function down()
    {
        $this->dropColumn('{{%appointment}}', 'duration');
        $this->dropColumn('{{%appointment}}', 'created');
    }
}
