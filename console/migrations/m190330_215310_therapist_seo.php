<?php

use yii\db\Migration;

/**
 * Class m190330_215310_therapist_seo
 */
class m190330_215310_therapist_seo extends Migration
{
    public function safeUp()
    {
        $this->addColumn('therapist_additional', 'page_title', $this->string(1024));
        $this->addColumn('therapist_additional', 'page_description', $this->text());
    }

    public function safeDown()
    {
        $this->dropColumn('therapist_additional', 'page_description');
        $this->dropColumn('therapist_additional', 'page_title');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190330_215310_therapist_seo cannot be reverted.\n";

        return false;
    }
    */
}
