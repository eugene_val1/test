<?php

use yii\db\Migration;

class m171029_124305_set_conversations_visibility extends Migration
{
    public function safeUp()
    {
        $this->addColumn('conversation', 'hidden', $this->boolean()->defaultValue(false));
    }

    public function safeDown()
    {
        $this->dropColumn('conversation', 'hidden');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171029_124305_set_conversations_visibility cannot be reverted.\n";

        return false;
    }
    */
}
