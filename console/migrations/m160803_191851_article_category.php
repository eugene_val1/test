<?php

use yii\db\Schema;
use yii\db\Migration;

class m160803_191851_article_category extends Migration
{
    public function up()
    {
        $this->addColumn('{{%article}}', 'category_id', Schema::TYPE_INTEGER . ' NOT NULL DEFAULT "0" AFTER `image`');
    }

    public function down()
    {
        $this->dropColumn('{{%article}}', 'category_id');
    }
}
