<?php

use yii\db\Schema;
use yii\db\Migration;

class m151027_151030_appointment_payment_status extends Migration
{
    public function up()
    {
        $this->addColumn('{{%appointment}}', 'payment_status', Schema::TYPE_INTEGER . ' NOT NULL AFTER `rating` ');
    }

    public function down()
    {
        $this->dropColumn('{{%appointment}}', 'payment_status');
    }
}
