<?php

use yii\db\Migration;

class m160622_211232_service_fee extends Migration
{
    public function up()
    {
        $this->batchInsert('{{%setting}}', ['key', 'type', 'category', 'title', 'value'], [
            ['serviceFee', 'string', 'payment', 'Service fee', 0],
            ['serviceFeePercent', 'checkbox', 'payment', 'Percent service fee', 0]
        ]);
    }

    public function down()
    {
        echo "m160622_211232_service_fee cannot be reverted.\n";

        return false;
    }
}
