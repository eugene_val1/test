<?php

use yii\db\Schema;
use yii\db\Migration;

class m150730_085148_user_timezone extends Migration
{
    public function up()
    {
       $this->addColumn('{{%user}}', 'timezone', Schema::TYPE_STRING . ' NOT NULL AFTER `country_id` ');
        
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%timezone}}', [
            'id' => Schema::TYPE_PK,
            'timezone_id' => Schema::TYPE_STRING . ' NOT NULL',
            'language' => Schema::TYPE_STRING . '(6) NOT NULL',
            'name' => Schema::TYPE_STRING . ' NOT NULL'
        ], $tableOptions);
        
        $this->createIndex('timezone_language', '{{%timezone}}', 'language');
        $this->createIndex('timezone_id_language', '{{%timezone}}', 'timezone_id, language');
    }

    public function down()
    {
        echo "m150730_085148_user_timezone cannot be reverted.\n";

        return false;
    }
}
