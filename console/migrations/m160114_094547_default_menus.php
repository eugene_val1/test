<?php

use yii\db\Schema;
use yii\db\Migration;

class m160114_094547_default_menus extends Migration
{
    public function up()
    {
        $this->batchInsert('{{%menu}}', ['position'], [['top'], ['help'], ['about'], ['contact'], ['social']]);
        
        $menu = (new \yii\db\Query())
            ->select(['id', 'position'])
            ->from('menu')
            ->indexBy('position')
            ->all();
        
        $this->batchInsert('{{%menu_lang}}', ['menu_id', 'language', 'title'], [
            [$menu['top']['id'], 'ru-RU', 'Верхнее меню'],
            [$menu['top']['id'], 'en-US', 'Top menu'],
            [$menu['help']['id'], 'ru-RU', 'Помощь'],
            [$menu['help']['id'], 'en-US', 'Help'],
            [$menu['about']['id'], 'ru-RU', 'Страницы о проекте'],
            [$menu['about']['id'], 'en-US', 'About project'],
            [$menu['contact']['id'], 'ru-RU', 'Контакты'],
            [$menu['contact']['id'], 'en-US', 'Contacts'],
            [$menu['social']['id'], 'ru-RU', 'Социальные сети'],
            [$menu['social']['id'], 'en-US', 'Social'],
        ]);
        
    }

    public function down()
    {
        echo "m160114_094547_default_menus cannot be reverted.\n";

        return false;
    }
}
