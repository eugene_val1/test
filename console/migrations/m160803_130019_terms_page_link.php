<?php

use yii\db\Migration;

class m160803_130019_terms_page_link extends Migration
{
    public function up()
    {
        $this->insert('{{%setting}}', [
            'key' => 'termsPage',
            'category' => 'main',
            'type' => 'string',
            'title' => 'Link to terms and conditions page',
            'value'=> ''
        ]);
    }

    public function down()
    {
        echo "m160803_130019_terms_page_link cannot be reverted.\n";

        return false;
    }
}
