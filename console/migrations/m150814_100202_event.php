<?php

use yii\db\Schema;
use yii\db\Migration;

class m150814_100202_event extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%event}}', [
            'id' => Schema::TYPE_PK,
            'type' => Schema::TYPE_INTEGER . ' NOT NULL',
            'importance' => Schema::TYPE_INTEGER . ' NOT NULL',
            'text' => Schema::TYPE_TEXT . ' NOT NULL',
            'data' => Schema::TYPE_TEXT . ' NOT NULL',
            'created' => Schema::TYPE_INTEGER . ' NOT NULL'
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%event}}');
    }
}
