<?php

use yii\db\Schema;
use yii\db\Migration;

class m150623_085330_menu extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {          
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%menu}}', [
            'id' => Schema::TYPE_PK,
            'position' => Schema::TYPE_STRING . '(32) NOT NULL',
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%menu}}');
    }
}
