<?php

use yii\db\Schema;
use yii\db\Migration;

class m160812_204915_dispatch extends Migration
{
   public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {          
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%dispatch}}', [
            'id' => Schema::TYPE_PK,
            'user_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'type' => Schema::TYPE_STRING . ' DEFAULT NULL',
            'message' => Schema::TYPE_TEXT . ' NOT NULL',
            'last_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'count' => Schema::TYPE_INTEGER . ' NOT NULL',
            'status' => Schema::TYPE_STRING . ' DEFAULT NULL',
            'sent' => Schema::TYPE_INTEGER . ' NOT NULL'
        ], $tableOptions);

    }

    public function down()
    {
        $this->dropTable('{{%dispatch}}');
    }
}
