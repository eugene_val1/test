<?php

use yii\db\Schema;
use yii\db\Migration;

class m160814_122535_client_price extends Migration
{
    public function up()
    {
        $this->addColumn('{{%client}}', 'price', Schema::TYPE_INTEGER . ' NOT NULL DEFAULT "0" AFTER `user_name`');
    }

    public function down()
    {
        $this->dropColumn('{{%client}}', 'price');
    }
}
