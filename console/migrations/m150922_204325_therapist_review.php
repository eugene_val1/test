<?php

use yii\db\Schema;
use yii\db\Migration;

class m150922_204325_therapist_review extends Migration
{
    public function up() {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%therapist_review}}', [
            'id' => Schema::TYPE_PK,
            'therapist_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'name' => Schema::TYPE_STRING . '(255) NOT NULL',
            'text' => Schema::TYPE_TEXT . ' NOT NULL',
            'published' => Schema::TYPE_INTEGER . '(2) NOT NULL',
            'created' => Schema::TYPE_INTEGER . ' NOT NULL',
        ], $tableOptions);
        
        $this->addForeignKey('fk_cl_therapist_review', 'therapist_review', 'therapist_id', 'user', 'id','CASCADE','CASCADE');
    }

    public function down() {
        $this->dropTable('{{%therapist_review}}');
    }
}
