<?php

use yii\db\Schema;
use yii\db\Migration;

class m150831_151103_region_price extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {          
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%region_price}}', [
            'id' => Schema::TYPE_PK,
            'region_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'therapist_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'amount' => Schema::TYPE_INTEGER . ' NOT NULL'
        ], $tableOptions);
        
        $this->createIndex('region_therapist_idx', '{{%region_price}}', 'region_id, therapist_id');
        $this->createIndex('price_therapist_idx', '{{%region_price}}', 'therapist_id');

        $this->addForeignKey('fk_cl_region_price', 'region_price', 'region_id', 'region', 'id','CASCADE','CASCADE');
        $this->addForeignKey('fk_cl_therapist_price', 'region_price', 'therapist_id', 'user', 'id','CASCADE','CASCADE');
    }

    public function down()
    {
        $this->dropTable('{{%region_price}}');
    }
}
