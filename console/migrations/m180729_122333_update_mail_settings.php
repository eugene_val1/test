<?php

use yii\db\Migration;

/**
 * Class m180729_122333_update_mail_settings
 */
class m180729_122333_update_mail_settings extends Migration
{
    public function safeUp()
    {
        $this->dropColumn('profile', 'agree_mailing');
        $this->addColumn('profile', 'agree_mailing', $this->boolean()->defaultValue(false));
    }

    public function safeDown()
    {
        $this->dropColumn('profile', 'agree_mailing');
        $this->addColumn('profile', 'agree_mailing', $this->boolean()->defaultValue(true));
    }
}
