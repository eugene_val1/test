<?php

use yii\db\Schema;
use yii\db\Migration;

class m150920_163422_message_with_attachment extends Migration
{
    public function up()
    {
        $this->addColumn('{{%conversation_message}}', 'type', Schema::TYPE_STRING . ' NOT NULL AFTER `text` ');
        $this->addColumn('{{%conversation_message}}', 'data', Schema::TYPE_TEXT . ' DEFAULT NULL AFTER `type` ');
    }

    public function down()
    {
        $this->dropColumn('{{%conversation_message}}', 'type');
        $this->dropColumn('{{%conversation_message}}', 'data');
    }
}
