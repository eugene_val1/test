<?php

use yii\db\Schema;
use yii\db\Migration;

class m151210_071727_article_tags extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%tag}}', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . ' NOT NULL',
            'frequency' => Schema::TYPE_INTEGER . ' NOT NULL DEFAULT 0',
        ], $tableOptions);
        
        $this->createTable('{{%article_tag}}', [
            'article_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'tag_id' => Schema::TYPE_INTEGER . ' NOT NULL',
        ], $tableOptions);
        
        $this->addPrimaryKey('', '{{%article_tag}}', ['article_id', 'tag_id']);        
    }

    public function down()
    {
        $this->dropTable('{{%tag}}');
        $this->dropTable('{{%article_tag}}');
    }
}
