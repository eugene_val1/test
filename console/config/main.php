<?php
use yii\swiftmailer\Mailer;
use common\components\ConsoleFormatter;
use common\models\User;
use common\components\ConsoleUser;
use yii\log\FileTarget;

$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-console',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'console\controllers',
    'modules' => [],
    'components' => [
        'log' => [
            'targets' => [
                [
                    'class' => FileTarget::class,
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'user' => [
            'class' => ConsoleUser::class,
            'identityClass' => User::class,
            'enableAutoLogin' => false,
        ],
        'urlManager' => [
            'hostInfo' => 'https://www.treatfield.com',
            'rules' => [
                'field/<alias:[0-9a-zA-Z-]+>' => 'field/view',
                'therapist/<alias:[0-9a-zA-Z-]+>' => 'therapists/view'
            ],
        ],
        'formatter' => [
            'class' => ConsoleFormatter::class,
        ],
        'mailer' => [
            'class' => Mailer::class,
            'viewPath' => '@common/mail',
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp.gmail.com',
                'username' => 'team@treatfield.com',
                'password' => 'IAVAtreatfield2017@ua!',
                'port' => '587',
                'encryption' => 'tls',
            ],
        ]
    ],
    'params' => $params,
];
