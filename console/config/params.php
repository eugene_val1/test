<?php
return [
    'supportEmail' => 'contact@treatfield.com',
    'baseUrl' => 'https://www.treatfield.com'
];
