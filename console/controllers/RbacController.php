<?php
namespace console\controllers;

use yii\console\Controller;
use common\components\rbac\UserRoleRule;
 
class RbacController extends Controller
{
    public function actionInit()
    {
        $auth = \Yii::$app->authManager;
        $auth->removeAll();
        
        $rule = new UserRoleRule();
        $auth->add($rule);

        $user = $auth->createRole('user');
        $user->ruleName = $rule->name;
        $editor = $auth->createRole('editor');
        $editor->ruleName = $rule->name;
        $admin = $auth->createRole('admin');
        $admin->ruleName = $rule->name;
        $therapist = $auth->createRole('therapist');
        $therapist->ruleName = $rule->name;
        $auth->add($user);
        $auth->add($editor);
        $auth->add($admin);
        $auth->add($therapist);
        $auth->addChild($admin, $editor);
        $auth->addChild($admin, $user);
    }
}