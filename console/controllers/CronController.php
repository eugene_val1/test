<?php

namespace console\controllers;

use console\controllers\actions\DispatchAction;
use console\controllers\actions\EventAction;
use console\controllers\actions\IndexingAction;
use console\controllers\actions\NewMessagesAction;
use console\controllers\actions\NotifyAction;
use console\controllers\actions\OncomingAppointmentAction;
use Yii;
use yii\base\Action;
use yii\console\Controller;
use yii\mutex\FileMutex;
use yii\mutex\Mutex;

/**
 * Class CronController
 *
 * @package console\controllers
 */
class CronController extends Controller
{
    /**
     * @var Mutex
     */
    public $mutex;

    /**
     * @return array
     */
    public function actions()
    {
        return [
            'event' => [
                //events for admin
                'class' => EventAction::class,
            ],
            'notify' => [
                'class' => NotifyAction::class,
            ],
            'notify-oncoming-appointment' => [
                //send notifications to bots/email about oncoming appointments in 24 hours
                'class' => OncomingAppointmentAction::class,
            ],
            'notify-messages' => [
                //send notifications to users that have unread messages
                'class' => NewMessagesAction::class,
            ],
            'dispatch' => [
                'class' => DispatchAction::class,
            ],
            'indexing' => [
                'class' => IndexingAction::class,
            ],
        ];
    }

    /**
     * Before action
     * @param Action $action
     * @return bool
     */
    public function beforeAction($action)
    {
        if (!parent::beforeAction($action)) {
            return false;
        }

        Yii::$app->urlManager->setBaseUrl(Yii::$app->params['siteUrl']);

        if (YII_ENV_PROD) {
            \Sentry\init(['dsn' => Yii::$app->params['sentry']['console-dns']]);
        }

        if (YII_ENV_PROD) {
            $this->mutex = new FileMutex();

            if (!$this->mutex->acquire('cron-' . $action->id)) {
                return false;
            }
        }

        return true;
    }

    /**
     * @param Action $action
     * @param mixed $result
     *
     * @return mixed
     */
    public function afterAction($action, $result)
    {
        if (YII_ENV_PROD) {
            $this->mutex->release('cron-' . $action->id);
        }

        return parent::afterAction($action, $result);
    }
}