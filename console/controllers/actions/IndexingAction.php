<?php

namespace console\controllers\actions;

use common\models\Article;
use common\models\PropertyLang;
use common\models\TherapistLang;
use console\models\ArticleElasticsearch;
use console\models\TherapistElasticsearch;
use yii\base\Action;

/**
 * Class IndexingAction
 * @package console\controllers\actions
 */
class IndexingAction extends Action
{
    /**
     * Run
     */
    public function run()
    {
        return;

        $articles = Article::find()
            ->where('index_status NOT IN (:new,:indexed)', [
                ':indexed' => Article::IN_INDEX,
                ':new' => Article::NOT_INDEX
            ])
            ->with('tags')
            ->all();

        foreach ($articles as $article) {
            $tags = [];
            foreach ($article->tags as $tag) {
                $tags[] = $tag->name;
            }
            $model = false;
            if ($article->index_status == Article::NEED_REINDEX) {
                $model = ArticleElasticsearch::findOne('article-' . $article->id);
            }
            if (!$model) {
                $model = new ArticleElasticsearch();
            }
            $model->setAttributes([
                'id' => 'article-' . $article->id,
                'title' => $article->title,
                'short_description' => $article->short_description,
                'content' => $article->content,
                'tags' => $tags,
                'url' => Url::toRoute(['field/view', 'alias' => $article->alias])
            ]);
            try {
                if ($model->save()) {
                    $article->index_status = Article::IN_INDEX;
                    $article->save(false);
                }
            } catch (\Exception $exception) {
            }
        }

        $therapists = TherapistLang::find()
            ->where('index_status NOT IN (:new,:indexed)', [
                    ':indexed' => TherapistLang::IN_INDEX,
                    ':new' => TherapistLang::NOT_INDEX]
            )->with([
                'relatedModel',
                'relatedModel.propertyIds',
                'languageModel',
            ])
            ->all();

        foreach ($therapists as $therapist) {
            $tags = [];
            $properties = PropertyLang::find()->where([
                'property_id' => $therapist->relatedModel->propertyIds,
                'language' => $therapist->language,
            ])->all();
            foreach ($properties as $property) {
                $tags[] = $property->name;
            }

            $model = false;
            if ($therapist->index_status == Article::NEED_REINDEX) {
                $model = TherapistElasticsearch::findOne('therapist-' . $therapist->languageModel->url . '-' . $therapist->user_id);
            }
            if (!$model) {
                $model = new TherapistElasticsearch();
            }
            $model->setAttributes([
                'id' => 'therapist-' . $therapist->languageModel->url . '-' . $therapist->user_id,
                'title' => $therapist->name,
                'content' => $therapist->short_description,
                'education' => $therapist->education,
                'experience' => $therapist->experience,
                'full_description' => $therapist->full_description,
                'tags' => $tags,
                'url' => '/' . $therapist->languageModel->url . '/therapist/' . $therapist->relatedModel->alias,
            ]);

            try {
                if ($model->save()) {
                    $therapist->index_status = Article::IN_INDEX;
                    $therapist->save(false);
                }
            } catch (\Exception $exception) {
            }
        }
    }
}