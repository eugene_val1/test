<?php

namespace console\controllers\actions;

use common\components\bots\BotMessageHelper;
use common\components\MailHelper;
use common\models\Appointment;
use common\models\NotificationSettings;
use common\models\Timezone;
use Yii;
use yii\base\Action;
use yii\db\Expression;

/**
 * Class OncomingAppointmentAction
 * @package console\controllers\actions
 */
class OncomingAppointmentAction extends Action
{
    /**
     * Run
     */
    public function run()
    {
        $now = time();
        $day = 24 * 3600;

        $appointments = Appointment::find()
            ->alias('a')
            ->joinWith([
                'userIdentity',
                'userIdentity.bots',
                'userIdentity.notificationSettings',
                'therapist',
            ])
            ->where([
                'a.status' => [
                    Appointment::STATUS_NEW,
                    Appointment::STATUS_RESCHEDULE,
                ]
            ])
            ->where(['>', 'start', $now - 2 * $day])
            ->groupBy('a.id')
            ->having(new Expression('MAX(a.number)'))
            ->all();

        foreach ($appointments as $appointment) {

            /** @var Appointment $appointment */

            if (!$appointment->userIdentity || !$appointment->therapist || empty($appointment->start)) {
                continue;
            }

            //to client
            $allowReminder = $appointment->userIdentity->isNoticeAllowed(NotificationSettings::TYPE_APPOINTMENT_REMINDER);
            $clientNow = Timezone::convertTimestamp($now, $appointment->userIdentity->timezone);
            $clientStart = Timezone::convertTimestamp($appointment->start, $appointment->userIdentity->timezone);
            $clientStartShort = Timezone::convert($appointment->start, $appointment->userIdentity->timezone, 'H:i');

            if (
                abs($clientStart - $day - $clientNow) > 5 * 60
                || ($clientStart - $day) > $clientNow
                || !$allowReminder
            ) {
                continue;
            }

            //send to bots
            if ($appointment->userIdentity->notificationSettings->target_bots) {
                foreach ($appointment->userIdentity->bots as $bot) {
                    BotMessageHelper::sendAppointmentReminder(
                        $bot,
                        $appointment->therapist->name,
                        $clientStartShort
                    );
                }
            }

            //send by email
            if ($appointment->userIdentity->notificationSettings->target_email) {
                try {
                    MailHelper::appointmentReminder($appointment, $appointment->userIdentity, $appointment->therapist);
                } catch (\Exception $e) {
                    Yii::error('Email SMTP error: ' . $e->getMessage());
                }
            }
        }
    }
}