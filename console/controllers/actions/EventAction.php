<?php

namespace console\controllers\actions;

use common\components\MailHelper;
use common\models\Event;
use Yii;
use yii\base\Action;

/**
 * Class EventAction
 * @package console\controllers\actions
 */
class EventAction extends Action
{
    /**
     * Run
     */
    public function run()
    {
        $events = Event::find()
            ->where([
                'informed' => Event::STATUS_NOT_INFORMED,
                'target' => Event::RECEIVER_ADMIN,
            ])
            ->all();

        $receivers = explode(',', Yii::$app->settingManager->get('adminEmail'));
        $receivers = array_map(function ($value) {
            return trim($value);
        }, $receivers);
        $receivers = array_filter($receivers, function ($value) {
            return !empty($value);
        });

        if (empty($events) || empty($receivers)) {
            return;
        }

        $message = Yii::$app->mailer->compose('reportEvents', ['events' => $events])
            ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name])
            ->setTo($receivers)
            ->setSubject(Yii::t('backend', 'New events on') . ' ' . Yii::$app->name);

        //sign message
        MailHelper::signMessage($message);

        try {
            if (!$message->send()) {
                Yii::error('Email SMTP error');
            }
        } catch (\Exception $e) {
            Yii::error('Email SMTP error: ' . $e->getMessage());
        }

        foreach ($events as $event) {
            /** @var Event $event */
            $event->setInformed();
        }
    }
}