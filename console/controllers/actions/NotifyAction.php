<?php

namespace console\controllers\actions;

use common\components\bots\BotMessageHelper;
use common\components\MailHelper;
use common\models\Appointment;
use common\models\AppointmentEvent;
use common\models\ChangeTimezoneEvent;
use common\models\Messages\ConversationMessage;
use common\models\Event;
use common\models\Messages\EventMessage;
use common\models\User;
use Yii;
use yii\base\Action;

/**
 * Class NotifyAction
 *
 * @package console\controllers\actions
 */
class NotifyAction extends Action
{
    /**
     * Run
     */
    public function run()
    {
        $events = Event::find()
            ->where([
                'informed' => Event::STATUS_NOT_INFORMED,
                'target' => Event::RECEIVER_USER,
            ])
            ->all();

        foreach ($events as $event) {
            try {
                /** @var Event $event */

                //1. Inform about `changed timezone` event as a message
                if ($event instanceof ChangeTimezoneEvent) {
                    /** @var ChangeTimezoneEvent $event */
                    if ($event->sendMessage()) {
                        $event->setInformed();
                    }
                    continue;
                }

                //2. Create message about this event
                if ($event->senderId && $event->conversationId) {
                    $conversationMessage = new EventMessage([
                        'user_id' => $event->senderId,
                        'text' => $event->message,
                        'conversation_id' => $event->conversationId
                    ]);
                    $conversationMessage->processEventMessage($event);
                    $conversationMessage->save(false);
                    if ($conversationMessage->client) {
                        $conversationMessage->client->updateLastMessageDate();
                    }
                }

                //3. send by email/bots
                /** @var AppointmentEvent $event */
                foreach ($event->receivers as $receiver) {

                    //send to bots: AppointmentEvent
                    if (
                        $event instanceof AppointmentEvent
                        && $receiver['user']->notificationSettings->target_bots
                    ) {
                        if ($receiver['targetRole'] === User::ROLE_THERAPIST) {
                            $start = $event->getData()['appointment_start'];
                            foreach ($event->getUser()->bots as $bot) {
                                if (empty($start)) {
                                    break;
                                }
                                BotMessageHelper::sendAppointmentEvent($bot, $receiver, $start);
                            }
                        }
                        if (
                            $receiver['targetRole'] === User::ROLE_USER
                            && $event->appointmentStatus == Appointment::PAID
                        ) {
                            foreach ($event->getTherapist()->bots as $bot) {
                                BotMessageHelper::sendToBots($bot, $receiver['text']);
                            }
                        }
                    }

                    //send by email
                    if ($receiver['user']->notificationSettings->target_email) {
                        $message = Yii::$app->mailer->compose($event->template, ['event' => $event, 'receiver' => $receiver])
                            ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name])
                            ->setTo($receiver['email'])
                            ->setSubject($event->subject);

                        //sign message
                        MailHelper::signMessage($message);

                        try {
                            if (!$message->send()) {
                                Yii::error('Email SMTP error');
                            }
                        } catch (\Exception $e) {
                            Yii::error('Email SMTP error: ' . $e->getMessage());
                        }
                    }
                }
            } catch (\Exception $e) {
                Yii::error('Email error: ' . $e->getMessage());
            } finally {
                if ($event) {
                    $event->setInformed();
                }
            }
        }
    }
}