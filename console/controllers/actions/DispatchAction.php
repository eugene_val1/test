<?php

namespace console\controllers\actions;

use common\models\Conversation;
use common\models\Messages\ConversationMessage;
use common\models\Dispatch;
use common\models\User;
use Yii;
use yii\base\Action;

/**
 * Class DispatchAction
 * @package console\controllers\actions
 */
class DispatchAction extends Action
{
    /**
     * Run
     */
    public function run()
    {
        /**
         * @var Dispatch $dispatch
         */
        $dispatch = Dispatch::find()
            ->where([
                'sent' => false,
                'status' => Dispatch::STATUS_RUNNING
            ])
            ->one();

        if (!$dispatch) {
            return;
        }

        $limit = 100;
        $query = User::find()
            ->where('id > :id', [':id' => $dispatch->last_id])
            ->limit($limit);

        if ($dispatch->type == Dispatch::TYPE_ALL) {
            $query->andFilterWhere(['in', 'role', [User::ROLE_THERAPIST, User::ROLE_USER]]);
        } else {
            $query->andFilterWhere([
                'role' => ($dispatch->type == Dispatch::TYPE_THERAPISTS) ? User::ROLE_THERAPIST : User::ROLE_USER
            ]);
        }

        $users = $query->all();

        foreach ($users as $i => $user) {
            $conversation = Conversation::find()
                ->where('(sender_id = :user_id AND receiver_id = :contact_id) OR (sender_id = :contact_id AND receiver_id = :user_id)', [
                    'user_id' => $dispatch->user_id,
                    'contact_id' => $user->id
                ])
                ->one();

            if ($conversation === null) {
                $conversation = new Conversation([
                    'sender_id' => $dispatch->user_id,
                    'receiver_id' => $user->id
                ]);
                $conversation->save();
            }

            $conversationMessage = new ConversationMessage([
                'user_id' => $dispatch->user_id,
                'text' => $dispatch->message,
                'conversation_id' => $conversation->id
            ]);
            $conversationMessage->save(false);

            if ($i + 1 == count($users)) {
                $dispatch->last_id = $user->id;
            }
        }

        $dispatch->count += count($users);
        $status = (count($users) < $limit) ? Dispatch::STATUS_COMPLETED : Dispatch::STATUS_RUNNING;
        $dispatch->setStatus($status);
    }
}