<?php

namespace console\controllers\actions;

use common\components\bots\BotMessageHelper;
use common\components\MailHelper;
use common\models\Messages\ConversationMessage;
use common\models\MessagesNotifications;
use common\models\NotificationSettings;
use Yii;
use yii\base\Action;

/**
 * Class NewMessagesAction
 * @package console\controllers\actions
 */
class NewMessagesAction extends Action
{
    /**
     * Run
     *
     * @throws \yii\base\InvalidParamException
     * @throws \yii\db\Exception
     */
    public function run()
    {
        $messages = ConversationMessage::find()
            ->alias('cm')
            ->with(['user.user', 'receiver.user', 'botReceivers'])
            ->joinWith('notification')
            ->where([
                'status' => ConversationMessage::STATUS_NEW,
                'messages_notifications.id' => null,
            ])
            ->orderBy('created')
            ->groupBy('cm.id')
            ->all();

        $receivers = [];
        foreach ($messages as $message) {
            $receivers[$message->receiver_id][] = $message;
        }

        $notifiedMessages = [];

        foreach ($receivers as $receiver => $receiverMessages) {

            $now = time();
            foreach ($receiverMessages as $receiverMessage) {
                $notifiedMessages[] = [$receiverMessage->id, $now];
            }

            if (
                empty($receiverMessages) ||
                empty($receiverMessages[0]->receiver) ||
                empty($receiverMessages[0]->receiver->user) ||
                empty($receiverMessages[0]->receiver->user->email) ||
                !$receiverMessages[0]->receiver->user->isNoticeAllowed(NotificationSettings::TYPE_MESSAGES)
            ) {
                continue;
            }

            $messageSubject = count($receiverMessages) > 0 ? 'messages' : 'message';

            //send to bots
            if (
                !empty($receiverMessages[0]->botReceivers) &&
                $receiverMessages[0]->receiver->user->notificationSettings->target_bots
            ) {
                foreach ($receiverMessages[0]->botReceivers as $botReceiver) {
                    BotMessageHelper::sendNewMessages($botReceiver, count($receiverMessages));
                }
            }

            //send by email
            if ($receiverMessages[0]->receiver->user->notificationSettings->target_email) {
                $this->sendEmail($receiverMessages, $messageSubject);
            }


        }

        //mark notified messages
        Yii::$app->db
            ->createCommand()
            ->batchInsert(
                MessagesNotifications::tableName(),
                ['message_id', 'created'],
                $notifiedMessages
            )
            ->execute();
    }

    /**
     * @param array $receiverMessages
     * @param string $messageSubject
     */
    protected function sendEmail($receiverMessages, $messageSubject)
    {
        try {
            $to = $receiverMessages[0]->receiver->user->email;

            $email = Yii::$app->mailer
                ->compose('messagesNotifications', [
                    'messages' => $receiverMessages,
                    'receiver' => $receiverMessages[0]->receiver
                ])
                ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name])
                ->setTo($to)
                ->setSubject('You have new ' . $messageSubject);

            //sign message
            MailHelper::signMessage($email);

            if (!$email->send()) {
                throw new \Exception('can not send email about new messages');
            }
        } catch (\Exception $e) {
            Yii::error([
                'Email SMTP error: ' . $e->getMessage(),
                'to' => $to,
                'receiverId' => isset($receiverMessages[0]) ? $receiverMessages[0]->receiver->user->id : 'empty'
            ]);
        }
    }
}