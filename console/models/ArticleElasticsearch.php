<?php

namespace console\models;

use yii\elasticsearch\ActiveRecord;

/**
 * Class ArticleElasticsearch
 * @package console\models
 */
class ArticleElasticsearch extends ActiveRecord
{
    /**
     * @return string
     */
    public static function index()
    {
        return 'treatfield';
    }

    /**
     * @return string
     */
    public static function type()
    {
        return 'article';
    }

    /**
     * @return array
     */
    public static function primaryKey()
    {
        return ['id'];
    }

    /**
     * @return array
     */
    public function attributes()
    {
        return [
            "id",
            "title",
            "short_description",
            "content",
            "tags",
            "url"
        ];
    }

    public function rules()
    {
        return [
            [$this->attributes(), 'safe']
        ];
    }
}
