<?php

namespace console\models;

use yii\elasticsearch\ActiveRecord;

/**
 * Class TherapistElasticsearch
 * @package console\models
 */
class TherapistElasticsearch extends ActiveRecord
{

    public static function index()
    {
        return 'treatfield';
    }

    public static function type()
    {
        return 'therapist';
    }

    public static function primaryKey()
    {
        return ['id'];
    }

    public function attributes()
    {
        return [
            "id",
            "title",
            "short_description",
            "education",
            "experience",
            "content",
            "tags",
            "url"
        ];
    }

    public function rules()
    {
        return [
            [$this->attributes(), 'safe']
        ];
    }
}
