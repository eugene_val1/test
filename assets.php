<?php

use yii\web\AssetBundle;
use yii\bootstrap\BootstrapPluginAsset;
use yii\web\JqueryAsset;
use yii\web\YiiAsset;
use frontend\assets\ConversationAsset;
use frontend\assets\AccountAsset;
use frontend\assets\AppAsset;
use frontend\assets\HomeAsset;

/**
 * Configuration file for the "yii asset" console command.
 */

// In the console environment, some path aliases may not exist. Please define these:
Yii::setAlias('@webroot', __DIR__ . '/frontend/web');
Yii::setAlias('@web', '/');

return [
    // Adjust command/callback for JavaScript files compressing:
    'jsCompressor' => 'java -jar compiler.jar --js {from} --js_output_file {to}',
    // Adjust command/callback for CSS files compressing:
    'cssCompressor' => 'java -jar yuicompressor.jar --type css {from} -o {to}',
    // The list of asset bundles to compress:
    'bundles' => [
        HomeAsset::class,
        AppAsset::class,
        AccountAsset::class,
        ConversationAsset::class,
        YiiAsset::class,
        JqueryAsset::class,
        BootstrapPluginAsset::class
    ],
    // Asset bundle for compression output:
    'targets' => [
        'all' => [
            'class' => AssetBundle::class,
            'basePath' => '@webroot/assets',
            'baseUrl' => '@web/assets',
            'js' => 'all-{hash}.js',
            'css' => 'all-{hash}.css',
        ],
    ],
    // Asset manager configuration:
    'assetManager' => [
        'basePath' => '@webroot/assets',
        'baseUrl' => '@web/assets',
    ],
];