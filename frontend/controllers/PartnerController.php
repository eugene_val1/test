<?php

namespace frontend\controllers;

use common\models\User;
use Yii;
use frontend\components\FrontendController;
use yii\filters\AccessControl;
use common\models\Partner;
use yii\filters\ContentNegotiator;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\filters\VerbFilter;
use common\components\payments\WayForPay;

/**
 * Class PartnerController
 * @package frontend\controllers
 */
class PartnerController extends FrontendController
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['therapist'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'remove' => ['post'],
                ],
            ],
            [
                'class' => ContentNegotiator::className(),
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }

    /**
     * @return array
     */
    public function actionPayment()
    {
        $partner = Partner::findOne(['user_id' => Yii::$app->user->id]);

        return [
            'result' => true,
            'html' => $this->renderAjax('_payment', [
                'partner' => $partner
            ])
        ];
    }

    /**
     * @return array
     */
    public function actionCreate()
    {
        /** @var User $user */
        $user = Yii::$app->user->identity;
        $model = new Partner([
            'user_id' => $user->id,
            'email' => trim($user->email),
            'partner_code' => trim($user->username),
        ]);

        $message = false;

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $payment = new WayForPay();
            $result = $payment->request('addPartnerRequest', $this->getData($user, $model));

            if ($result['status']) {
                $model->save(false);
                $message = Yii::t('main', 'Billing information saved');
            } else {
                $message = Yii::t('main', 'Errors during partner billing information saving');
            }
        }

        return [
            'result' => true,
            'errors' => $model->hasErrors(),
            'message' => $message,
            'html' => $this->renderAjax('_form', [
                'model' => $model
            ])
        ];
    }

    /**
     * @return array
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionUpdate()
    {
        /** @var User $user */
        $user = Yii::$app->user->identity;

        /** @var Partner $model */
        $model = $this->getModel();
        $model->email = $user->email;
        $model->clearAttributes(['card_number', 'card_holder', 'cvv', 'month', 'year']);
        $message = false;

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $payment = new WayForPay();
            $result = $payment->request('editPartnerRequest', $this->getData($user, $model));

            if ($result['status']) {
                $model->save(false);
                $message = Yii::t('main', 'Billing information updated');
            } else {
                $message = Yii::t('main', 'Errors during partner billing information saving');
            }
        }

        return [
            'result' => true,
            'errors' => $model->hasErrors(),
            'message' => $message,
            'html' => $this->renderAjax('_form', [
                'model' => $model
            ])
        ];
    }

    /**
     * @return array
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionRemoveConfirmation()
    {
        /** @var Partner $model */
        $model = $this->getModel();

        return [
            'result' => true,
            'html' => $this->renderAjax('_remove_confirmation', [
                'model' => $model
            ])
        ];
    }

    /**
     * @return array
     * @throws \Exception
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionRemove()
    {
        /** @var Partner $model */
        $model = $this->getModel();
        $model->delete();
        return [
            'result' => true
        ];
    }

    /**
     * @return Partner
     * @throws NotFoundHttpException
     */
    public function getModel()
    {
        $model = Partner::findOne(['user_id' => Yii::$app->user->id]);

        if (empty($model)) {
            throw new NotFoundHttpException('Can not create Partner');
        }

        return $model;
    }

    /**
     * @param $user
     * @param $model
     * @return array
     */
    public function getData($user, $model)
    {
        return [
            'partnerCode' => $model->partner_code,
            'phone' => $model->phone,
            'email' => $model->email,
            'compensationCardNumber' => $model->card_number,
            'compensationCardHolder' => $model->card_holder,
            'compensationCardExpYear' => $model->year,
            'compensationCardExpMonth' => $model->month,
            'compensationCardCvv' => $model->cvv,
            'description' => Yii::t('main', 'Therapist') . ' ' . $user->id . ', ' . Yii::t('main', 'username') . ' - ' . $user->username
        ];
    }
}
