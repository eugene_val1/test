<?php

namespace frontend\controllers;

use common\components\GeoService;
use common\components\UserManager;
use common\models\Room;
use frontend\components\Browser;
use frontend\components\DeviceManager;
use frontend\components\FrontendController;
use frontend\models\EnterForm;
use Yii;
use yii\base\Action;
use yii\filters\AccessControl;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;

/**
 * Class RoomController
 *
 * @package frontend\controllers
 */
class RoomController extends FrontendController
{
    public $layout = 'room';

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @param Action $event
     *
     * @return bool
     */
    public function beforeAction($event)
    {
        $this->enableCsrfValidation = false;

        return parent::beforeAction($event);
    }

    /**
     * @param $alias
     * @return string
     */
    public function actionIndex($alias = null)
    {
        $room = $this->findRoom($alias);

        //ios
        if (DeviceManager::isIOS()) {
            return $this->render('index-ios', [
                'room' => $room,
            ]);
        }

        if ($room->password && !$room->checkRememberedPassword()) {
            $model = new EnterForm(['room' => $room]);
            $model->scenario = 'check';
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $room->rememberPassword();
            } else {
                return $this->render('lock', [
                    'model' => $model,
                ]);
            }
        }

        $appointment = $room->type === Room::TYPE_APPOINTMENT ? $room->appointment : null;
        $role = 'guest';

        if ($appointment && Yii::$app->user->id == $appointment->therapist_id) {
            $role = 'therapist';
        } else if ($appointment && Yii::$app->user->id == $appointment->user_id) {
            $role = 'client';
        }

        $view = 'index';

        //desktop safari
        if (!$this->isSupportedBrowser()) {
            $view = 'index-wrong-browser';
        }

        $geoService = new GeoService(UserManager::getUserIp());
        $isRussia = $geoService->isRussia();
        if ($isRussia) {
            $videoServerUrl = Yii::$app->params['auxiliaryVideoServerUrl'];
        } else {
            $videoServerUrl = Yii::$app->params['videoServerUrl'];
        }

        return $this->render($view, [
            'room' => $room,
            'appointment' => $appointment,
            'role' => $role,
            'videoServerUrl' => $videoServerUrl,
            'isRussia' => $isRussia,
        ]);
    }

    /**
     * @param $alias
     * @return array
     */
    public function actionInvite($alias = null)
    {
        Yii::$app->response->format = 'json';
        $room = $this->findRoom($alias);

        return [
            'result' => true,
            'html' => $this->renderAjax('_invite', [
                'room' => $room,
            ])
        ];
    }

    /**
     * @param $alias
     * @return array
     */
    public function actionLock($alias = null)
    {
        Yii::$app->response->format = 'json';
        $room = $this->findRoom($alias);
        $model = new EnterForm(['room' => $room]);
        $message = false;

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $room->setPassword($model->password);
            $room->rememberPassword();
            $message = Yii::t('main', 'Room now locked by password');
        }

        return [
            'result' => true,
            'errors' => $model->hasErrors(),
            'message' => $message,
            'html' => $this->renderAjax('_password', [
                'model' => $model,
            ])
        ];
    }

    /**
     * @param $alias
     * @return array
     */
    public function actionUnlock($alias = null)
    {
        Yii::$app->response->format = 'json';
        $room = $this->findRoom($alias);
        $room->removePassword();

        return [
            'result' => true,
            'message' => Yii::t('main', 'Room now not protected by password'),
        ];
    }

    /**
     * @param $alias
     * @return array
     */
    public function actionDuration($alias = null)
    {
        Yii::$app->response->format = 'json';
        $room = $this->findRoom($alias);
        $appointment = $room->type == Room::TYPE_APPOINTMENT ? $room->appointment : null;
        $duration = (int)Yii::$app->request->post('duration');

        if ($appointment && Yii::$app->user->id == $appointment->therapist_id && $duration > $appointment->duration) {
            $appointment->duration = $duration;
            $appointment->save();

            return [
                'result' => true,
            ];
        }
    }

    /**
     * TODO: add security check
     *
     * @return array|\yii\web\Response
     */
    public function actionLog()
    {
        if (!Yii::$app->request->isAjax) {
            return $this->goHome();
        }

        Yii::$app->response->format = 'json';

        $message = Yii::$app->request->post('message');
        if (!empty($message)) {
            Yii::info($message, 'room');
        }

        return [
            'result' => true,
        ];
    }

    /**
     * @param $alias
     * @return Room
     * @throws HttpException
     */
    public function findRoom($alias)
    {
        if (($model = Room::findByAlias($alias)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException();
    }

    /**
     * @return bool
     */
    private function isSupportedBrowser()
    {
        $browser = new Browser();

        return \in_array($browser->getBrowser(), [
            Browser::BROWSER_CHROME,
            Browser::BROWSER_FIREFOX,
            Browser::BROWSER_OPERA,
        ], true);
    }
}
