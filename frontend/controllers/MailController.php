<?php
namespace frontend\controllers;

use Yii;
use yii\filters\Cors;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Class MailController
 * @package frontend\controllers
 */
class MailController extends Controller
{
    const KEY = 'FugjvxT05M';

    /**
     * @param \yii\base\Action $action
     * @return bool
     */
    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'corsFilter' => [
                'class' => Cors::className(),
                'cors' => [
                    // restrict access to
                    'Origin' => ['*'],
                    'Access-Control-Request-Method' => ['POST', 'GET'],
                    // Allow only POST and PUT methods
                ],
                'only' => ['send']
            ],
        ];
    }

    /**
     * Mail worker
     * @return array
     * @throws NotFoundHttpException
     */
    public function actionSend()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $data = Yii::$app->request->post();

        if (empty($data['key']) || $data['key'] !== self::KEY) {
            throw new NotFoundHttpException();
        }

        if (
            !empty($data['email']) &&
            !empty($data['name']) &&
            !empty($data['message'])
        ) {
            $result = Yii::$app->mailerWorker->compose()
                ->setTo('family.massage.cabinet@gmail.com')
                ->setFrom([$data['email'] => $data['name']])
                ->setSubject('Обратная связь')
                ->setHtmlBody($data['message'])
                ->send();

            return [
                'result' => $result
            ];
        }

        throw new NotFoundHttpException();
    }
}
