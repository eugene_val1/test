<?php

namespace frontend\controllers;

use common\models\Article;
use common\models\Comment;
use common\models\Vote;
use frontend\components\FrontendController;
use Yii;
use yii\filters\AccessControl;
use yii\filters\ContentNegotiator;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 *  Comment controller
 */
class CommentController extends FrontendController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['vote', 'replies'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            [
                'class' => ContentNegotiator::className(),
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @param $id
     * @param int $reply_to
     * @return array
     * @throws NotFoundHttpException
     */
    public function actionSend($id, $reply_to = 0)
    {
        $level = 0;
        $article = Article::findOne($id);

        if (!$article) {
            throw new NotFoundHttpException;
        }

        if ($reply_to > 0) {
            $replyToComment = Comment::findOne($reply_to);

            if (!$replyToComment) {
                throw new NotFoundHttpException;
            }

            $level = $replyToComment->level + 1;
        }

        $comment = new Comment([
            'user_id' => Yii::$app->user->id,
            'article_id' => $article->id,
            'reply_to' => $reply_to,
            'level' => $level
        ]);

        if ($comment->load(Yii::$app->request->post()) && $comment->validate()) {
            $comment->save(false);

            $article->updateCounter('comments_count', 1);

            if ($reply_to > 0 && $replyToComment) {
                $replyToComment->updateCounter('replies_count', 1);
            }
        }

        return [
            'result' => true,
            'errors' => $comment->hasErrors(),
            'comment' => (!$comment->hasErrors()) ? $this->renderPartial('_comment', ['comment' => $comment]) : '',
            'html' => $this->renderPartial('_send', ['comment' => $comment])
        ];
    }

    /**
     * @param $id
     * @return array
     */
    public function actionReplies($id)
    {
        $comment = $this->getComment($id);
        $comments = Comment::find()
            ->where('reply_to = :id', [':id' => $comment->id])
            ->joinWith(['profile', 'therapistLang'])
            ->with(['vote'])
            ->orderBy('created DESC')
            ->all();

        return [
            'result' => true,
            'message' => false,
            'html' => $this->renderPartial('_replies', ['replies' => $comments, 'article' => $comment->article])
        ];
    }

    /**
     * @param $id
     * @return array
     */
    public function actionReport($id)
    {
        $comment = $this->getComment($id);

        if ($comment->status === Comment::STATUS_PUBLISHED) {
            $comment->setStatus(Comment::STATUS_REPORTED);
        }

        return [
            'result' => true,
            'message' => Yii::t('main', 'Thank you for your report. We moderate this comment as soon as posible.'),
        ];
    }

    /**
     * @param $id
     * @return array
     */
    public function actionVote($id)
    {
        $comment = $this->getComment($id);

        if (Yii::$app->user->isGuest) {
            return [
                'result' => true,
                'message' => Yii::t('main', 'You need to login for voting'),
                'callback' => false
            ];
        }

        if ($comment->vote) {
            $comment->vote->delete();
            $comment->updateCounter('votes_count', Vote::VOTE_DOWN);
        } else {
            $vote = new Vote([
                'user_id' => Yii::$app->user->id,
                'target' => Comment::tableName(),
                'target_id' => $comment->id,
                'value' => Vote::VOTE_UP
            ]);
            $vote->save(false);

            $comment->updateCounter('votes_count', Vote::VOTE_UP);
        }

        return [
            'result' => true,
            'message' => false,
            'callback' => true,
            'count' => $comment->votes_count,
        ];
    }

    /**
     * @param $id
     * @return array
     */
    public function actionDeleteConfirmation($id)
    {
        $comment = $this->getComment($id);

        return [
            'result' => true,
            'html' => $this->renderPartial('_remove_confirmation', ['comment' => $comment])
        ];
    }

    /**
     * @param $id
     * @return array
     */
    public function actionDelete($id)
    {
        $comment = $this->getComment($id);

        if (Yii::$app->user->id !== $comment->user_id && Yii::$app->user->id !== $comment->article->user_id) {
            return [
                'result' => true,
                'message' => Yii::t('main', 'You have no permissions to delete this comment'),
                'callback' => false
            ];
        }

        if ($comment->replies_count > 0) {
            $comment->setStatus(Comment::STATUS_DELETED);
        } else {
            $comment->delete();

            if ($comment->article) {
                $comment->article->updateCounter('comments_count', -1);
            }
        }

        return [
            'result' => true,
            'message' => false,
            'id' => $comment->id,
            'replies' => $comment->replies_count,
            'html' => Yii::t('main', 'Comment deleted'),
        ];
    }

    /**
     * @param $id
     * @return static
     * @throws NotFoundHttpException
     */
    public function getComment($id)
    {
        $comment = Comment::findOne($id);

        if (!$comment) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        return $comment;
    }
}
