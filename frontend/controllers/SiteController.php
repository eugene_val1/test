<?php

namespace frontend\controllers;

use common\components\SiteManager;
use common\models\Therapist;
use common\models\TherapistReview;
use frontend\components\FrontendController;
use frontend\components\PageBehavior;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use yii\elasticsearch\Query;
use yii\web\Cookie;

/**
 * Class SiteController
 * @package frontend\controllers
 */
class SiteController extends FrontendController
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => PageBehavior::className(),
                'section' => 'home'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
            ],
        ];
    }

    /**
     * @param \yii\base\Action $event
     * @return bool
     */
    public function beforeAction($event)
    {
        if ($this->action->id === 'subscribe-success') {
            \Yii::$app->controller->enableCsrfValidation = false;
        }
        return parent::beforeAction($event);
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $businessCardTherapists = Therapist::find()
            ->where(['is not', 'business_card_url', null])
            ->andWhere(['!=', 'business_card_url', ''])
            ->all();

        foreach ($businessCardTherapists as $therapist) {
            /** @var Therapist $therapist */
            $therapist->business_card_url = str_replace('?autoplay=1', '', $therapist->business_card_url);
        }

        $therapistReviews = TherapistReview::find()
            ->with('therapist')
            ->where(['published' => TherapistReview::STATUS_PUBLISHED])
            ->orderBy(new Expression('RAND()'))
            ->limit(20)
            ->all();

        return $this->render('index', [
            'businessCardTherapists' => $businessCardTherapists,
            'therapistReviews' => $therapistReviews,
        ]);
    }

    /**
     * @param $q
     * @return string
     */
    public function actionSearch($q)
    {
        $query = new Query(['options' => ['q' => $q]]);
        $query->from('treatfield');

        $provider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 20,
            ]
        ]);

        return $this->render('search', [
            'provider' => $provider,
            'q' => $q,
        ]);
    }

    /**
     * Set cookies that hide mailchimp popup
     */
    public function actionSubscribeSuccess()
    {
        if (\Yii::$app->request->isPost) {
            $hideTime = 259200; //3 days
        } else {
            //user has already subscribed
            $hideTime = 15552000; //180 days
        }
        Yii::$app->response->cookies->add(new Cookie([
            'name' => 'hideMailchimp',
            'value' => true,
            'expire' => time() + $hideTime,
            'domain' => 'www.treatfield.com' //https://stackoverflow.com/a/2345183
        ]));

        if (\Yii::$app->request->isGet) {
            \Yii::$app->response->redirect('/');
        }
    }

    /**
     * @return string
     */
    public function actionError()
    {
        $exception = Yii::$app->errorHandler->exception;

        if ($exception !== null) {
            return $this->render('error', [
                'exception' => $exception,
            ]);
        }
    }
}
