<?php

namespace frontend\controllers;

use common\models\Profile;
use common\models\Therapist;
use Yii;
use frontend\components\FrontendController;
use yii\filters\AccessControl;
use common\models\Appointment;
use yii\filters\ContentNegotiator;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\HttpException;
use common\components\payments\WayForPay;
use yii\helpers\Json;
use common\models\Transaction;
use common\models\Partner;
use yii\base\InvalidParamException;

/**
 * Class PaymentController
 *
 * @package frontend\controllers
 */
class PaymentController extends FrontendController
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['callback'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['user'],
                    ],
                ],
            ],
            [
                'class' => ContentNegotiator::className(),
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {
        if ($action->id == 'callback') {
            $this->enableCsrfValidation = false;
        }

        return parent::beforeAction($action);
    }

    /**
     * Create modal with ability to select appointment amount
     * @param $id
     * @return array
     */
    public function actionCreate($id)
    {
        $appointment = $this->findAppointment($id);
        return [
            'result' => true,
            'html' => $this->renderAjax('appointment_count', [
                'appointment' => $appointment,
                'profile' => Yii::$app->user->identity->profile,
                'url' => Url::toRoute(['payment/payment-widget', 'id' => $id, 'appointmentAmount' => 1]),
                'urlTemplate' => Url::toRoute(['payment/payment-widget', 'id' => $id]),
            ])
        ];
    }

    /**
     * Render Wayforpay payment widget
     *
     * @param $id
     * @param $appointmentAmount
     * @return array|Response
     *
     * @throws HttpException
     */
    public function actionPaymentWidget($id, $appointmentAmount = 1)
    {
        if (!Yii::$app->request->isAjax) {
            return $this->redirect(['/account']);
        }

        $message = false;
        $appointment = $this->findAppointment($id);
        $appointmentAmount = \in_array((int)$appointmentAmount, range(1, Yii::$app->params['appointments']['maxAmountPayment']), true)
            ? (int)$appointmentAmount
            : 1;

        if ($appointment->payment_status != Appointment::NOT_PAID || $appointment->user_id !== Yii::$app->user->id) {
            throw new NotFoundHttpException();
        }

        $partner = Partner::findOne(['user_id' => $appointment->therapist_id]);
        if (!$partner || $partner->expired < time()) {
            throw new NotFoundHttpException();
        }

        if ($appointment->transaction) {
            $transaction = $appointment->transaction;
        } else {
            $transaction = new Transaction();
        }

        /** @var Therapist $therapist */
        $therapist = $appointment->therapist;
        $ratio = WayForPay::getExchangeParser()->getRatio();

        $fee = $therapist->getFee($appointment->amount, $appointmentAmount);
        $price = $appointmentAmount * $appointment->amount + $fee;
        $paymentFee = ($price * Yii::$app->settingManager->get('wayforpayFee')) / 100;

        //converted
        $convertedFee = WayForPay::convert($fee, $ratio);
        $convertedPrice = WayForPay::convert($price, $ratio);
        $convertedPaymentFee = WayForPay::convert($paymentFee, $ratio);

        if ($appointment->number == 1) {
            $serviceFee = $price / 2 - $paymentFee;
            $amount = $price / 2;

            //converted
            $convertedServiceFee = round($convertedPrice / 2 - $convertedPaymentFee, 2);
            $convertedAmount = round($convertedPrice / 2, 2);
        } else {
            $amount = $appointmentAmount * $appointment->amount;
            $serviceFee = $fee - $paymentFee;

            //converted
            $convertedServiceFee = round($convertedFee - $convertedPaymentFee, 2);
            $convertedAmount = WayForPay::convert($appointmentAmount * $appointment->amount, $ratio);
        }

        if ($serviceFee < 0) {
            $serviceFee = 0;
            $amount = $price - $paymentFee;

            //converted
            $convertedServiceFee = 0;
            $convertedAmount = round($convertedPrice - $convertedPaymentFee, 2);
        }

        $transaction->setAttributes([
            'user_id' => $appointment->user_id,
            'therapist_id' => $appointment->therapist_id,
            'appointment_id' => $appointment->id,
            'amount' => $amount,
            'currency' => Yii::$app->settingManager->get('currency'),
            'fee' => $serviceFee,
            'payment_fee' => $paymentFee,
            'status' => Transaction::STATUS_OPEN,
            'reason' => Transaction::REASON_PAY,
            'ratio' => $ratio,
            'appointmentAmount' => $appointmentAmount,
            'status_update_informed' => false,
        ]);

        //convert all amounts to self::TRANSACTION_CURRENCY
        $transaction->converted_amounts = json_encode([
            'amount' => $convertedAmount,
            'fee' => $convertedServiceFee,
            'payment_fee' => $convertedPaymentFee,
        ]);

        $transaction->payment_id = $this->randomTransactionId($appointment->id, 7);
        $transaction->created = time();
        $transaction->save(false);

        $payment = new WayForPay();

        return [
            'result' => true,
            'message' => $message,
            'tid' => $transaction->id,
            'options' => $payment->getPaymentOptions($transaction, $appointment),
        ];
    }

    /**
     * Wayforpay Callback
     * @param $id
     * @return array
     */
    public function actionCallback($id)
    {
        $transaction = $this->findTransaction($id);
        $data = Json::decode(Yii::$app->request->rawBody);

        if (!is_array($data)) {
            Yii::info('Invalid request data', 'payments');
            Yii::info(Yii::$app->request->rawBody, 'payments');
            throw new InvalidParamException('Invalid request data');
        }

        $payment = new WayForPay();
        if ($payment->isPaymentValid($data, 'widgetResponse') === true) {
            switch ($data['transactionStatus']) {
                case WayForPay::STATUS_APPROVED:
                case WayForPay::STATUS_AUTH_COMPLETE:
                    $transaction->setStatus(Transaction::STATUS_SUCCESS);
                    break;
                case WayForPay::STATUS_PROCESSING:
                case WayForPay::STATUS_PENDING:
                    $transaction->setStatus(Transaction::STATUS_PENDING);
                    break;
                case WayForPay::STATUS_DECLINED:
                case WayForPay::STATUS_REFUNDED:
                case WayForPay::STATUS_VOIDED:
                    $transaction->setStatus(Transaction::STATUS_CANCELED);
                    break;
                case WayForPay::STATUS_EXPIRED:
                    if ($data['orderReference'] == $transaction->payment_id) {
                        $transaction->setStatus(Transaction::STATUS_CANCELED);
                    }
                    break;
                default:
                    Yii::error('Invalid status', 'payments');
                    Yii::info($data, 'payments');
            }

            if (!empty($data['recToken'])) {
                Profile::updateAll(['rec_token' => $data['recToken']], ['user_id' => $transaction->user_id]);
            }
        } else {
            Yii::error('Invalid payment', 'payments');
            Yii::info(Yii::$app->request->rawBody, 'payments');
            return null;
        }

        return $payment->getResponse($data);
    }

    /**
     * @param $id
     *
     * @return Appointment
     *
     * @throws NotFoundHttpException
     */
    public function findAppointment($id)
    {
        /** @var Appointment $model */
        $model = Appointment::find()
            ->where(['id' => (int)$id])
            ->with(['user', 'therapist'])
            ->one();

        if (!$model) {
            throw new NotFoundHttpException();
        }

        return $model;
    }

    /**
     * @param $id
     *
     * @return Transaction
     *
     * @throws NotFoundHttpException
     */
    public function findTransaction($id)
    {
        if (($model = Transaction::findOne($id)) !== null) {
            return $model;
        }

        Yii::error('Transaction not found ' . $id, 'payments');
        throw new NotFoundHttpException();
    }

    /**
     * @param $id
     * @param $length
     * @return string
     */
    public function randomTransactionId($id, $length)
    {
        $random = '';
        for ($i = 0; $i < $length; $i++) {
            $random .= mt_rand(0, 9);
        }

        return 'TR' . $id . '-' . $random;
    }
}
