<?php

namespace frontend\controllers;

use Yii;
use frontend\components\FrontendController;
use yii\filters\AccessControl;
use common\models\Appointment;
use yii\filters\ContentNegotiator;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\HttpException;

/**
 * Class AppointmentController
 * @package frontend\controllers
 */
class AppointmentController extends FrontendController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['mark', 'set-status', 'update', 'overdue', 'later'],
                        'allow' => true,
                        'roles' => ['therapist'],
                    ],
                    [
                        'actions' => ['video'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            [
                'class' => ContentNegotiator::className(),
                'except' => ['video'],
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }

    /**
     * @param $id
     * @return array
     */
    public function actionMark($id)
    {
        $appointment = $this->getModel($id);

        return [
            'result' => true,
            'html' => $this->renderAjax('_mark', [
                'appointment' => $appointment
            ])
        ];
    }

    /**
     * @param $id
     * @param $status
     * @return array
     */
    public function actionSetStatus($id, $status)
    {
        $appointment = $this->getModel($id);
        $view = '_set_status';

        if ($status == Appointment::STATUS_COMPLETED || $status == Appointment::STATUS_MISSED) {
            $view = '_confirm_set_status';
        }

        return [
            'result' => true,
            'html' => $this->renderAjax($view, [
                'appointment' => $appointment,
                'status' => $status,
            ])
        ];
    }

    /**
     * @param $id
     * @return array
     */
    public function actionUpdate($id)
    {
        $model = $this->getModel($id);
        $message = false;

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->save(false);
            $message = Yii::t('main', 'Appointment information successfully updated');
        }

        return [
            'result' => true,
            'errors' => $model->hasErrors(),
            'message' => $message,
            'html' => $this->renderAjax('_update', [
                'model' => $model,
            ])
        ];
    }

    /**
     * @param $id
     * @return array
     */
    public function actionOverdue($id)
    {
        $appointment = $this->getModel($id);
        return [
            'result' => true,
            'html' => $this->renderAjax('_overdue', [
                'appointment' => $appointment,
            ])
        ];
    }

    /**
     * @param $id
     * @return array
     */
    public function actionLater($id)
    {
        $appointment = $this->getModel($id);
        $appointment->overdue_informed = Appointment::OVERDUE_INFORMED;
        $appointment->save(false);

        return [
            'result' => true,
            'message' => false,
        ];
    }

    /**
     * @param $id
     * @return Response
     * @throws HttpException
     */
    public function actionVideo($id)
    {
        /** @var Appointment $appointment */
        $appointment = $this->getModel($id);

        if (
            !$appointment->room ||
            ($appointment->payment_status !== Appointment::PAID && !$appointment->free) ||
            !($appointment->therapist_id == Yii::$app->user->id || $appointment->user_id == Yii::$app->user->id)
        ) {
            return $this->redirect(['account']);
        }

        return $this->redirect($appointment->room->getUrl());
    }

    /**
     * @param $id
     * @return Appointment
     * @throws HttpException
     */
    public function getModel($id)
    {
        if (($model = Appointment::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException();
    }
}
