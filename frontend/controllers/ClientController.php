<?php

namespace frontend\controllers;

use common\models\Messages\DefaultConversationMessage;
use common\models\PaidAppointments;
use Yii;
use frontend\components\FrontendController;
use yii\data\Pagination;
use yii\filters\AccessControl;
use common\models\Client;
use common\models\Appointment;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\filters\ContentNegotiator;
use yii\web\HttpException;

/**
 *  Client controller
 */
class ClientController extends FrontendController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['therapist'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'remove-client' => ['post'],
                ],
            ],
            [
                'class' => ContentNegotiator::className(),
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }

    /**
     * @return array|Response
     */
    public function actionActiveList()
    {
        $query = Client::find()
            ->with(['user', 'newMessages', 'nextAppointment', 'completedAppointments'])
            ->andWhere(['client.status' => Client::STATUS_ACTIVE])
            ->andWhere(['therapist_id' => Yii::$app->user->id])
            ->orderBy('next_appointment_start')
            ->groupBy('client.id');

        $countQuery = clone $query;
        $count = $countQuery->count();
        $pages = new Pagination([
            'totalCount' => $count,
            'pageSize' => 5,
        ]);
        $models = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        return [
            'result' => true,
            'html' => $this->renderAjax('_active_list', [
                'models' => $models,
                'pages' => $pages,
            ])
        ];
    }

    /**
     * @return array|Response
     */
    public function actionPendingList()
    {
        $query = Client::find()
            ->with(['user', 'newMessages', 'nextAppointment', 'completedAppointments'])
            ->andWhere(['status' => Client::STATUS_PENDING])
            ->andWhere(['therapist_id' => Yii::$app->user->id])
            ->orderBy('last_message_created DESC');

        $countQuery = clone $query;
        $count = $countQuery->count();
        $pages = new Pagination([
            'totalCount' => $count,
            'pageSize' => 5,
        ]);
        $models = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        return [
            'result' => true,
            'html' => $this->renderAjax('_pending_list', [
                'models' => $models,
                'pages' => $pages,
            ])
        ];
    }

    /**
     * @return array|Response
     */
    public function actionNewRequestsList()
    {
        $query = Client::find()
            ->where([
                'therapist_id' => Yii::$app->user->id,
                'status' => Client::STATUS_NEW,
            ])
            ->with(['user', 'innerUser', 'newMessages'])
            ->orderBy('last_message_created DESC');

        $countQuery = clone $query;
        $count = $countQuery->count();
        $pages = new Pagination(['totalCount' => $count, 'pageSize' => 5]);
        $models = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        return [
            'result' => true,
            'html' => $this->renderAjax('_new_requests_new', [
                'models' => $models,
                'pages' => $pages,
            ])
        ];
    }

    /**
     * @return array|Response
     */
    public function actionNonActiveList()
    {
        $query = Client::find()
            ->with(['user', 'newMessages', 'nextAppointment', 'completedAppointments'])
            ->andWhere(['status' => Client::STATUS_INACTIVE])
            ->andWhere(['therapist_id' => Yii::$app->user->id])
            ->orderBy('last_message_created DESC');

        $countQuery = clone $query;
        $count = $countQuery->count();
        $pages = new Pagination([
            'totalCount' => $count,
            'pageSize' => 3,
        ]);
        $models = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        return [
            'result' => true,
            'html' => $this->renderAjax('_non_active_list', [
                'models' => $models,
                'pages' => $pages,
            ])
        ];
    }

    /**
     * @param $id
     * @return array
     */
    public function actionRemoveClientConfirmation($id)
    {
        $client = $this->getClient($id);

        return [
            'result' => true,
            'html' => $this->renderAjax('_remove_confirmation', [
                'client' => $client
            ])
        ];
    }

    /**
     * @param $id
     * @return array
     */
    public function actionRemoveClient($id)
    {
        $client = $this->getClient($id);
        $client->delete();
        return [
            'result' => true
        ];
    }

    /**
     * Complete appointment
     * @param $id
     * @param $status
     * @param $apId
     * @return Response
     * @throws HttpException
     */
    public function actionCompleteAppointment($id, $status, $apId)
    {
        $client = $this->getClient((int)$id);
        $appointment = Appointment::findOne(['id' => (int)$apId]);

        if (!in_array($status, [Appointment::STATUS_COMPLETED, Appointment::STATUS_MISSED])) {
            throw new NotFoundHttpException();
        }

        if ($appointment->payment_status != Appointment::PAID) {
            throw new HttpException(500, 'System error');
        }

        if ($appointment->status == $status) {
            return $this->redirect('/account');
        }

        $appointment->setStatus($status);

        $nextAppointment = $this->newAppointment($client, true);
        if ($nextAppointment->save(false)) {
            PaidAppointments::decrease($client->user_id);
        }

        $client->next_appointment_start = Client::NO_NEXT_APPPOINTMENT;
        $client->setStatus(Client::STATUS_PENDING);

        return $this->redirect('/account');
    }

    /**
     * @param $id
     * @param bool $reschedule
     * @return array
     */
    public function actionAppointment($id, $reschedule = false)
    {
        $client = $this->getClient($id);
        $message = false;
        $status = Appointment::STATUS_NEW;
        $isNewAppointment = false;

        if ($reschedule && $client->nextAppointment !== null) {
            $appointment = $client->nextAppointment;

            if ($appointment->payment_status == Appointment::NOT_PAID) {
                $appointment->amount = $client->getTherapistPrice();
            }
            $status = Appointment::STATUS_RESCHEDULE;
        } else {
            $appointment = $this->newAppointment($client);
            $isNewAppointment = true;
        }

        $appointment->scenario = 'schedule';

        if ($appointment->load(Yii::$app->request->post()) && $appointment->validate()) {
            $appointment->start = Appointment::convertLocalToGMT($appointment->start);
            $appointment->end = Appointment::convertLocalToGMT($appointment->end);
            $appointment->setStatus($status);

            $message = Yii::t('main', 'Appointment set successfully');
            $client->next_appointment_start = $appointment->start;

            if ($client->status !== Client::STATUS_ACTIVE) {
                $client->complete_status = Client::UNCOMPLETED;
                $client->setStatus(Client::STATUS_ACTIVE);
                $message .= '<br>' . Yii::t('main', 'Client now has active status');
            } else {
                $client->save(false);
            }

            //send default message
            if ($isNewAppointment && $appointment->isFirst())  {
                $newMessage = new DefaultConversationMessage();
                $newMessage->user_id = $client->therapist_id;
                $newMessage->receiver_id = $client->user_id;
                $newMessage->conversation_id = $client->conversation_id;
                $newMessage->save(false);
            }
        }

        return [
            'result' => true,
            'errors' => $appointment->hasErrors(),
            'message' => $message,
            'html' => $this->renderAjax('_appointment', ['client' => $client, 'appointment' => $appointment])
        ];
    }


    /**
     * Create new appointment
     * @param Client $client
     * @param bool $usePaid
     * @return Appointment
     */
    public function newAppointment($client, $usePaid = false)
    {
        $paidAmount = $usePaid ? PaidAppointments::getForUser($client->user_id) : 0;
        $appointment = new Appointment([
            'therapist_id' => $client->therapist_id,
            'user_id' => $client->user_id,
            'amount' => $client->getTherapistPrice(),
            'status' => Appointment::STATUS_NEW
        ]);

        if ($paidAmount > 0) {
            $appointment->setPaymentStatus(Appointment::PAID);
        }

        return $appointment;
    }

    /**
     * @param $id
     * @return array
     * @throws HttpException
     */
    public function actionSetInactiveConfirmation($id)
    {
        $client = $this->getClient($id);

        if ($client->nextAppointment === null) {
            throw new HttpException(500, 'System error');
        }

        return [
            'result' => true,
            'html' => $this->renderAjax('_inactive_confirmation', ['client' => $client])
        ];
    }

    /**
     * @param $id
     * @param $status
     * @return array
     */
    public function actionSetPending($id, $status)
    {
        $client = $this->getClient($id);

        if ($client->nextAppointment === null) {
            return ['result' => false, 'message' => Yii::t('main', 'System error occured during set status')];
        }

        if ($status == Appointment::STATUS_COMPLETED || $status == Appointment::STATUS_MISSED) {
            if ($client->nextAppointment->payment_status != Appointment::PAID) {
                return ['result' => false, 'message' => Yii::t('main', 'System error occured during set status')];
            }

            $client->nextAppointment->setStatus($status);
            $appointment = $this->newAppointment($client);
            $appointment->save(false);
        } else if ($status == Appointment::STATUS_RESCHEDULE) {
            $client->nextAppointment->removeDate();
        }

        $client->next_appointment_start = Client::NO_NEXT_APPPOINTMENT;
        $client->setStatus(Client::STATUS_PENDING);

        return ['result' => true];
    }

    /**
     * @param $id
     * @param bool $status
     * @return array
     */
    public function actionSetInactive($id, $status = false)
    {
        $client = $this->getClient($id);

        if ($client->nextAppointment === null) {
            return ['result' => false, 'message' => Yii::t('main', 'System error occured during set status')];
        }

        if ($status === Appointment::STATUS_COMPLETED || $status === Appointment::STATUS_MISSED) {
            $client->nextAppointment->setStatus($status);
        } else {
            $client->nextAppointment->delete();
        }

        $client->next_appointment_start = Client::NO_NEXT_APPPOINTMENT;
        $client->complete_status = Client::COMPLETED;
        $client->setStatus(Client::STATUS_INACTIVE);

        return [
            'result' => true
        ];
    }

    /**
     * @param $id
     * @param $status
     * @param int $appointment_status
     * @return array
     * @throws HttpException
     */
    public function actionSetStatus($id, $status, $appointment_status = Appointment::STATUS_NEW)
    {
        if (!in_array($status, [Client::STATUS_PENDING, Client::STATUS_INACTIVE])) {
            throw new NotFoundHttpException();
        }

        $client = $this->getClient($id);

        if ($client->nextAppointment !== null) {
            if ($appointment_status == Appointment::STATUS_COMPLETED || $appointment_status == Appointment::STATUS_MISSED) {
                $client->nextAppointment->setStatus($appointment_status);
            } else {
                $client->nextAppointment->delete();
            }
        }

        if ($status == Client::STATUS_PENDING) {
            if ($appointment_status == Appointment::STATUS_RESCHEDULE && $client->nextAppointment !== null) {
                $client->nextAppointment->removeDate();
            } else {
                $appointment = $this->newAppointment($client);
                $appointment->save(false);
            }
        }

        $client->next_appointment_start = Client::NO_NEXT_APPPOINTMENT;
        $client->setStatus($status);

        return [
            'result' => true
        ];
    }

    /**
     * @param $id
     * @return array
     */
    public function actionCustomize($id)
    {
        $model = $this->getClient($id);
        $message = false;

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->save(false);
            $message = Yii::t('main', 'Client data successfully updated');
        }

        return [
            'result' => true,
            'errors' => $model->hasErrors(),
            'message' => $message,
            'html' => $this->renderAjax('_customize', [
                'model' => $model
            ])
        ];
    }

    /**
     * @param $id
     * @return array|Client|\yii\db\ActiveRecord
     * @throws HttpException
     */
    public function getClient($id)
    {
        $model = Client::find()
            ->where('therapist_id = :therapist_id AND id = :id', [
                ':therapist_id' => Yii::$app->user->id,
                ':id' => $id]
            )
            ->one();

        if (empty($model)) {
            throw new NotFoundHttpException();
        }

        return $model;
    }
}
