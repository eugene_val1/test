<?php

namespace frontend\controllers;

use common\models\VideoArticle;
use common\models\TreatArticle;
use Yii;
use frontend\components\FrontendController;
use common\models\Article;
use frontend\models\ArticleFilter;
use frontend\components\PageBehavior;
use yii\caching\TagDependency;
use yii\web\Cookie;
use yii\web\NotFoundHttpException;
use common\models\CategoryPage;

/**
 * Class FieldController
 *
 * @package frontend\controllers
 */
class FieldController extends FrontendController
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => PageBehavior::className(),
                'section' => $this->id
            ],
        ];
    }

    /**
     * @param string $type
     * @return string
     */
    public function actionIndex($type = ArticleFilter::TYPE_ALL)
    {
        $filterModel = new ArticleFilter(['type' => $type]);
        $dataProvider = $filterModel->filter(Yii::$app->request->queryParams);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'filterModel' => $filterModel
        ]);
    }

    /**
     * @param string $alias
     * @return string
     *
     * @throws NotFoundHttpException
     */
    public function actionView($alias)
    {
        $cacheKey = Article::getCacheKey($alias);
        $article = Yii::$app->frontendCache->get($cacheKey);

        if (!$article) {
            /** @var Article $article */
            $article = Article::find()
                ->where(['article.alias' => $alias])
                ->joinWith(['profile', 'therapistLang'])
                ->one();

            Yii::$app->frontendCache->set(
                $cacheKey,
                $article,
                Article::CACHE_DURATION,
                new TagDependency(['tags' => $cacheKey])
            );
        }

        $isAvailable = $article
            ? $article->published == Article::STATUS_PUBLISHED
            : false;

        if (!$isAvailable) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        $this->updateArticleViews($article);

        $filterModel = new ArticleFilter();
        $filterModel->type = $filterModel->filterTypeReverse($article->type);

        if ($article instanceof VideoArticle) {
            return $this->render('view_video', [
                'article' => $article,
                'articlesProvider' => $filterModel->get(4, [$article->id]),
            ]);
        }

        if ($article instanceof TreatArticle) {
            return $this->render('view_treat', [
                'article' => $article,
            ]);
        }

        return $this->render('view_text', [
            'article' => $article,
            'articlesProvider' => $filterModel->get(7, [$article->id]),
        ]);
    }

    /**
     * @param string $alias
     * @return array|string
     *
     * @throws NotFoundHttpException
     */
    public function actionCategory($alias)
    {
        $page = CategoryPage::findByAlias($alias);
        if (!$page) {
            throw new NotFoundHttpException;
        }

        $filterModel = new ArticleFilter([
            'category' => $page->id,
            'type' => ArticleFilter::TYPE_POSTS
        ]);
        $dataProvider = $filterModel->filter(Yii::$app->request->queryParams);

        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = 'json';
            return [
                'result' => true,
                'html' => $page->content
            ];
        }

        return $this->render('category', [
            'page' => $page,
            'dataProvider' => $dataProvider,
            'filterModel' => $filterModel
        ]);
    }

    /**
     * @param Article $article
     * @throws \yii\base\InvalidCallException
     */
    protected function updateArticleViews($article)
    {
        $viewsData = Yii::$app->request->cookies->getValue(Article::VIEW_COOKIES_NAME);
        $views = \json_decode($viewsData);
        $views = \is_array($views) ? $views : [];

        if (\in_array($article->id, $views)) {
            return;
        }

        $views[] = $article->id;

        Yii::$app->response->cookies->add(new Cookie([
            'value' => \json_encode($views),
            'name' => Article::VIEW_COOKIES_NAME,
            'expire' => time() + Article::VIEW_COOKIES_EXPIRE,
        ]));

        $article->updateCounter('views_count', 1);
    }
}
