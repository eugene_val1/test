<?php

namespace frontend\controllers;

use frontend\components\FrontendController;
use common\models\Faq;
use common\models\FaqCategory;
use yii\base\InvalidParamException;
use yii\web\NotFoundHttpException;
use frontend\models\FaqSearch;
use frontend\components\PageBehavior;

/**
 * Class FaqController
 * @package frontend\controllers
 */
class FaqController extends FrontendController
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => PageBehavior::className(),
                'section' => $this->id
            ],
        ];
    }

    /**
     * @return string
     *
     * @throws InvalidParamException
     */
    public function actionIndex()
    {
        $featured = Faq::getDb()->cache(function ($db) {
            return Faq::find()->with('translation')
                ->where(['featured' => Faq::FEATURED])
                ->orderBy('number')
                ->all();
        }, 3600);

        return $this->render('index', [
            'current' => false,
            'page' => $this->page,
            'search' => $this->getSearchModel(),
            'categories' => $this->getCategories(),
            'questions' => $featured,
        ]);
    }

    /**
     * @param $alias
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionCategory($alias)
    {
        /** @var FaqCategory $category */
        $category = FaqCategory::find()
            ->with(['translation', 'children'])
            ->where(['alias' => $alias])
            ->one();

        if (!$category) {
            throw new NotFoundHttpException;
        }

        $questions = Faq::find()
            ->with('translation')
            ->where(['category_id' => $category->withChildren()])
            ->orderBy('number')
            ->all();

        return $this->render('index', [
            'current' => $category,
            'page' => $this->page,
            'search' => $this->getSearchModel(),
            'categories' => $this->getCategories(),
            'questions' => $questions
        ]);
    }

    /**
     * @param $key
     * @return string
     */
    public function actionSearch($key = null)
    {
        if (empty($key)) {
            return $this->redirect('/faq');
        }

        $model = $this->getSearchModel($key);
        $dataProvider = $model->search();

        return $this->render('index', [
            'current' => false,
            'page' => $this->page,
            'search' => $model,
            'categories' => $this->getCategories(),
            'questions' => $dataProvider->models
        ]);
    }

    /**
     * @return array|\yii\db\ActiveRecord[]
     */
    public function getCategories()
    {
        $root = 0;
        return FaqCategory::find()
            ->with(['translation', 'children'])
            ->where(['parent_id' => $root])
            ->orderBy('number')
            ->all();
    }

    /**
     * @param bool $key
     * @return FaqSearch
     */
    public function getSearchModel($key = false)
    {
        return new FaqSearch(['key' => $key]);
    }
}
