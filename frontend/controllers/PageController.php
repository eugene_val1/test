<?php

namespace frontend\controllers;

use Yii;
use frontend\components\FrontendController;
use common\models\Page;
use yii\caching\TagDependency;
use yii\web\NotFoundHttpException;

/**
 * Class PageController
 *
 * @package frontend\controllers
 */
class PageController extends FrontendController
{
    const PAGE_HOW_IT_WORKS = 'how-it-works';
    const PAGE_ABOUT_US = 'about-project';

    /**
     * @param $alias
     *
     * @return array|string
     *
     * @throws NotFoundHttpException
     */
    public function actionIndex($alias = null)
    {
        if (empty($alias)) {
            return $this->goHome();
        }

        $cacheKey = Page::getCacheKey($alias);
        $page = Yii::$app->frontendCache->get($cacheKey);

        if (!$page) {
            $page = Page::findByAlias($alias);
            Yii::$app->frontendCache->set(
                $cacheKey,
                $page,
                Page::CACHE_DURATION,
                new TagDependency(['tags' => $cacheKey])
            );
        }

        if (!$page) {
            throw new NotFoundHttpException;
        }

        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = 'json';
            return [
                'result' => true,
                'html' => $this->renderAjax('_modal', [
                    'page' => $page,
                ])
            ];
        }

        //customize page
        return $this->getPageView($alias, $page);
    }

    /**
     * @param string $alias
     * @param Page $page
     *
     * @return string
     */
    protected function getPageView($alias, $page)
    {
        if ($alias === self::PAGE_HOW_IT_WORKS) {
            return $this->render('how-it-works', [
                'page' => $page,
            ]);
        }

        if ($alias === self::PAGE_ABOUT_US) {
            return $this->render('about-project', [
                'page' => $page,
            ]);
        }

        return $this->render('index', [
            'page' => $page,
        ]);
    }
}
