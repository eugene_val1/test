<?php

namespace frontend\controllers;

use common\models\Contact;
use common\models\ContactTheme;
use frontend\components\FrontendController;
use Yii;

/**
 * Class ContactController
 * @package frontend\controllers
 */
class ContactController extends FrontendController
{
    /**
     * @param bool $request
     * @return array|string
     */
    public function actionIndex($request = false)
    {
        $model = new Contact();
        $message = false;

        if (Yii::$app->user->isGuest) {
            $model->scenario = 'guest';
        } else {
            $user = Yii::$app->user->identity;
            $model->name = $user->profile->name;
            $model->email = $user->email;
            $model->country_id = $user->country_id;
        }

        $title = $request
            ? Yii::t('main', 'Describe your request, and we will recommend you the appropriate specialist')
            : Yii::t('main', 'How can we help you?');
        if ($request) {
            $theme = ContactTheme::findOne(['use_for_request' => 1]);
            if ($theme) {
                $model->theme_id = $theme->id;
            }
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->save(false);
            $message = Yii::t('main', 'Thank you for request. We will contact you as soon as possible');
        }

        $formName = '_contactForm';

        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = 'json';
            return [
                'result' => true,
                'errors' => $model->hasErrors(),
                'message' => $message,
                'html' => $this->renderAjax($formName, ['model' => $model, 'title' => $title])
            ];
        }

        return $this->render('index', [
            'model' => $model,
            'message' => $message,
            'title' => $title,
            'formName' => $formName,
        ]);
    }

    /**
     * @return array|string
     */
    public function actionFreeConsultation()
    {
        $model = new Contact();
        $model->theme_id = Contact::TYPE_CONSULTATION;
        $message = false;

        if (Yii::$app->user->isGuest) {
            $model->scenario = 'guest';
        } else {
            $user = Yii::$app->user->identity;
            $model->name = $user->profile->name;
            $model->email = $user->email;
            $model->country_id = $user->country_id;
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->save(false);
            $message = Yii::t('main', 'Thank you for request. We will contact you as soon as possible');
        }

        $formName = '_consultationForm';
        $title = Yii::t('main', 'Request a free consultation for choosing a therapist');

        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = 'json';
            return [
                'result' => true,
                'errors' => $model->hasErrors(),
                'message' => $message,
                'html' => $this->renderAjax($formName, ['model' => $model, 'title' => $title])
            ];
        }

        return $this->render('index', [
            'model' => $model,
            'message' => $message,
            'title' => $title,
            'formName' => $formName,
        ]);
    }
}
