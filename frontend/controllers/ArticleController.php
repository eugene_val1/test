<?php

namespace frontend\controllers;

use common\models\Article;
use common\models\Tag;
use common\models\TextArticle;
use common\models\TreatArticle;
use common\models\Vote;
use frontend\components\FrontendController;
use frontend\models\CropImageForm;
use Yii;
use yii\base\Action;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;

/**
 * Class ArticleController
 *
 * @package frontend\controllers
 */
class ArticleController extends FrontendController
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['vote'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['index', 'treat', 'update', 'tags', 'remove', 'remove-confirmation', 'vote'],
                        'allow' => true,
                        'roles' => ['user'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['therapist'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @param Action $event
     *
     * @return bool
     */
    public function beforeAction($event)
    {
        if (Yii::$app->controller->action->id === 'vote') {
            $this->enableCsrfValidation = false;
        }

        return parent::beforeAction($event);
    }

    /**
     * @param bool $status
     * @return string
     */
    public function actionIndex($status = false)
    {
        $query = Article::find()->where([
            'user_id' => Yii::$app->user->id,
            'type' => [TextArticle::TYPE, TreatArticle::TYPE]
        ])->orderBy('created DESC');

        $count['all'] = 0;

        foreach (Article::getPublishStatuses() as $publishedStatus => $statusName) {
            ${$publishedStatus} = clone $query;
            $queryCount = ${$publishedStatus}->andWhere(['published' => $publishedStatus])->count();
            $count[$publishedStatus] = $queryCount;
            $count['all'] += $queryCount;
        }

        if ($status && in_array($status, array_keys(Article::getPublishStatuses()))) {
            $query->andFilterWhere([
                'published' => $status,
            ]);
        }

        $provider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 20,
            ]
        ]);

        return $this->render('index', [
            'provider' => $provider,
            'count' => $count
        ]);
    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionTreat()
    {
        $model = new TreatArticle(
            [
                'user_id' => Yii::$app->user->id,
                'type' => TreatArticle::TYPE,
            ]
        );

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->setStatus(TreatArticle::STATUS_PENDING);
            return $this->redirect(['index']);
        }

        return $this->render('form', [
            'model' => $model
        ]);
    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $article = new TextArticle([
            'scenario' => 'insert',
            'type' => TextArticle::TYPE,
            'user_id' => Yii::$app->user->id
        ]);
        if ($article->load(Yii::$app->request->post()) && $article->validate()) {
            $article->setStatus(Article::STATUS_PENDING);
            return $this->redirect(['index']);
        }

        return $this->render('form', [
            'model' => $article
        ]);
    }

    /**
     * @param $id
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id = null)
    {
        $article = Article::findOne($id);

        if (!$article || $article->user_id !== Yii::$app->user->id) {
            throw new NotFoundHttpException();
        }

        if ($article->load(Yii::$app->request->post()) && $article->validate()) {
            $article->setStatus(Article::STATUS_PENDING);
            return $this->redirect(['index']);
        }

        return $this->render('form', [
            'model' => $article
        ]);
    }

    /**
     * @return array
     */
    public function actionUploadImage()
    {
        Yii::$app->response->format = 'json';

        $model = new TextArticle();
        if ($model->validate(['cover_imageFile'])) {
            return [
                'status' => true,
                'html' => $this->renderAjax('_crop', [
                    'url' => $model->getFileUrl('cover_image'),
                    'model' => new CropImageForm(['image' => $model->cover_image])
                ])
            ];
        } else {
            return [
                'status' => false,
                'error' => $model->getFirstError('cover_imageFile')
            ];
        }
    }

    /**
     * @return array
     */
    public function actionCropImage()
    {
        Yii::$app->response->format = 'json';
        $crop = new CropImageForm();
        if ($crop->load(Yii::$app->request->post()) && $crop->validate()) {
            $model = new TextArticle([
                'cover_image' => $crop->image
            ]);
            $model->crop('cover_image', 'image', $crop->width, $crop->height, [$crop->x, $crop->y]);
            return [
                'status' => true,
                'cover_image' => $model->cover_image,
                'image' => $model->image,
                'url' => $model->getFileUrl('image')
            ];
        } else {
            return [
                'status' => false,
                'error' => Yii::t('main', 'Select crop region')
            ];
        }
    }

    /**
     * @param $id
     * @return array
     * @throws NotFoundHttpException
     */
    public function actionVote($id = null)
    {
        Yii::$app->response->format = 'json';
        $article = Article::findOne($id);

        if (!$article) {
            throw new NotFoundHttpException();
        }

        if (Yii::$app->user->isGuest) {
            $message = '<p class="text-center">' . Yii::t('main', 'You need to login for voting') . '</p>';
            return [
                'result' => true,
                'message' => $message,
                'callback' => false
            ];
        }

        if ($article->vote) {
            $article->vote->delete();
            $article->updateCounter('votes_count', Vote::VOTE_DOWN);
        } else {
            $vote = new Vote([
                'user_id' => Yii::$app->user->id,
                'target' => Article::tableName(),
                'target_id' => $article->id,
                'value' => Vote::VOTE_UP,
            ]);
            $vote->save(false);

            $article->updateCounter('votes_count', Vote::VOTE_UP);
        }

        return [
            'result' => true,
            'message' => false,
            'callback' => true,
            'count' => $article->votes_count
        ];
    }

    /**
     * @param null $q
     * @param null $id
     * @return array
     */
    public function actionTags($q = null, $id = null)
    {
        Yii::$app->response->format = 'json';

        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $query = new Query;
            $query->select('name as id, name as text')
                ->from('tag')
                ->where(['like', 'name', $q])
                ->limit(20);
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        } elseif ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => Tag::find($id)->name];
        }
        return $out;
    }

    /**
     * @param $id
     * @return array
     */
    public function actionRemoveConfirmation($id)
    {
        Yii::$app->response->format = 'json';
        $model = $this->getModel($id);

        return [
            'result' => true,
            'html' => $this->renderAjax('_remove_confirmation', [
                'model' => $model
            ])
        ];
    }

    /**
     * @param $id
     * @return array
     */
    public function actionRemove($id)
    {
        Yii::$app->response->format = 'json';
        $model = $this->getModel($id);
        $model->delete();
        return [
            'result' => true,
            'message' => false,
            'id' => $id
        ];
    }

    /**
     * @param $id
     * @return Article
     * @throws NotFoundHttpException
     */
    public function getModel($id)
    {
        $article = Article::findOne($id);

        if (!$article || $article->user_id !== Yii::$app->user->id) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        return $article;
    }
}
