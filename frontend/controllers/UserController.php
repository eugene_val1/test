<?php

namespace frontend\controllers;

use common\components\MailHelper;
use common\components\UserManager;
use common\models\ConfirmTokens;
use frontend\components\ModalHelper;
use frontend\components\TooltipHelper;
use frontend\models\FinishSignupForm;
use Yii;
use frontend\components\FrontendController;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use yii\authclient\OAuth2;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\AuthProvider;
use frontend\models\AuthSignupForm;
use common\models\User;
use common\models\Profile;
use common\models\Country;
use common\models\Timezone;
use yii\base\Model;
use common\models\Therapist;
use yii\helpers\Json;
use yii\web\Cookie;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\ServerErrorHttpException;
use yii\authclient\AuthAction;
use yii\captcha\CaptchaAction;

/**
 * Class UserController
 * @package frontend\controllers
 */
class UserController extends FrontendController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => [
                            'logout',
                            'name-list',
                            'auth',
                            'profile',
                            'autologin',
                            'activate-tooltips',
                            'stop-tooltips',
                            'delete',
                            'confirm-new-email',
                        ],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @param \yii\base\Action $event
     * @return bool
     */
    public function beforeAction($event)
    {
        if ($this->action->id === 'logout' && Yii::$app->request->isPost) {
            \Yii::$app->controller->enableCsrfValidation = false;
        }
        return parent::beforeAction($event);
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'auth' => [
                'class' => AuthAction::class,
                'successCallback' => [$this, 'successCallback'],
            ],
            'captcha' => [
                'class' => CaptchaAction::class,
            ],
        ];
    }

    /**
     * @return array|bool|string|Response
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return false;
        }

        $model = new LoginForm();

        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            if ($model->getUser()->profile->show_tooltips
                && $model->getUser()->role === User::ROLE_USER
            ) {
                Yii::$app->tooltip->updateStep(TooltipHelper::STEP_EMPTY);
                return $this->redirect('/');
            }

            return $this->redirect('/ru/account');
        }

        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = 'json';
            return [
                'result' => true,
                'errors' => $model->hasErrors(),
                'message' => false,
                'html' => $this->renderAjax('_loginForm', ['model' => $model])
            ];
        }

        return $this->render('login', [
            'model' => $model
        ]);
    }

    /**
     * Oauth Callback
     *
     * @param OAuth2 $client
     * @return Response
     *
     * @throws ServerErrorHttpException
     */
    public function successCallback($client)
    {
        if (!Yii::$app->user->isGuest) {
            return $this->redirect('/ru/account');
        }

        $attributes = $client->getUserAttributes();
        $provider = $client->getName();
        $email = isset($attributes['email'])
            ? strtolower(trim($attributes['email']))
            : '';

        /**
         * @var $authProvider AuthProvider
         */
        $authProvider = AuthProvider::findOne([
            'name' => $provider,
            'provider_id' => $attributes['id'],
        ]);

        $userByEmail = User::findByEmail($email);

        if ($userByEmail) {
            $user = $userByEmail;
        } elseif ($authProvider) {
            $user = $authProvider->user;
        } else {
            $transaction = Yii::$app->db->beginTransaction();

            $user = new User();
            $country = Country::getCurrentByIP();
            $timezone = Timezone::getCurrentByIP();
            $user->attributes = [
                'username' => $provider . '_' . $attributes['id'],
                'email' => isset($attributes['email']) ? $attributes['email'] : '',
                'role' => User::ROLE_USER,
                'status' => User::STATUS_CREATED,
                'country_id' => $country ? $country->id : 0,
                'timezone' => $timezone ?: null,
            ];
            $user->generateAuthKey();
            $user->generateToken();

            if (!$user->save(false)) {
                $transaction->rollBack();
                throw new ServerErrorHttpException('Internal error');
            }

            Yii::info([
                'user_id' => $user->id,
                'ip' => UserManager::getUserIp(),
            ], 'geo');

            $profile = new Profile();
            $profile->attributes = [
                'user_id' => $user->id,
                'type' => Profile::TYPE,
                'name' => (isset($attributes['name']))
                    ? $attributes['name']
                    : 'TreatField User',
                'image' => isset($attributes['photo'])
                    ? $attributes['photo']
                    : '',
            ];

            if (!$profile->save(false)) {
                $transaction->rollBack();
                throw new ServerErrorHttpException('Internal error');
            }

            $authProvider = new AuthProvider();
            $authProvider->attributes = [
                'user_id' => $user->id,
                'name' => $provider,
                'provider_id' => $attributes['id'],
            ];
            if (!$authProvider->save()) {
                Yii::$app->getSession()->setFlash('error', Yii::t('main', 'Internal error. Can not login'));
                throw new ServerErrorHttpException('Internal error. Can not save auth provider');
            }

            //success
            $transaction->commit();
        }

        if (Yii::$app->user->isGuest) {
            if ((int)$user->status === User::STATUS_ACTIVE && !empty($user->email)) {
                if (!empty($attributes['photo']) && (empty($user->profile->image) || $user->profile->isProfileImageSocial())) {
                    $user->profile->saveRemoteImage('image', $attributes['photo']);
                    $user->profile->save(false);
                }
                Yii::$app->user->login($user);
                if ($user->profile->show_tooltips && $user->role == User::ROLE_USER) {
                    return $this->redirect(Yii::$app->request->referrer);
                }

                return $this->redirect('/ru/account');
            }

            if ((int)$user->status === User::STATUS_CREATED && !empty($user->email)) {
                $user->status = User::STATUS_ACTIVE;
                $user->save(false);
            }

            Yii::$app->response->cookies->add(new Cookie([
                'value' => md5($user->id . $provider),
                'name' => 'sauth',
                'expire' => time() + 3600,
            ]));
            return $this->redirect([
                'user/auth-signup',
                'provider' => $provider,
                'id' => $user->id,
            ]);
        }

        return $this->redirect(['account/profile']);
    }

    /**
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Signs user up
     * @return mixed
     * @throws ServerErrorHttpException
     */
    public function actionSignup()
    {
        $transaction = Yii::$app->db->beginTransaction();

        $user = new User(['role' => User::ROLE_USER]);
        $user->scenario = 'create';

        $profile = new Profile(['type' => Profile::TYPE]);
        $message = false;

        if (
            $user->load(Yii::$app->request->post())
            && $profile->load(Yii::$app->request->post())
            && Model::validateMultiple([$user, $profile])
        ) {
            $country = Country::getCurrentByIP();
            $timezone = Timezone::getCurrentByIP();
            $user->country_id = $country ? $country->id : null;
            $user->timezone = $timezone ?: null;

            $user->setPassword($user->user_password);
            $user->generateAuthKey();
            $user->generateToken();
            if (!$user->save(false)) {
                $transaction->rollBack();
                $message = Yii::t('main', 'Can not sign up. Server error. Try again later');
                $profile->addError('agree_mailing', $message);
                return $this->signupResponse($user, $profile, $message);
            }

            Yii::info([
                'user_id' => $user->id,
                'ip' => UserManager::getUserIp(),
            ], 'geo');

            $profile->user_id = $user->id;
            $profile->agree_mailing = $user->agree_mailing;
            if (!$profile->save(false)) {
                $transaction->rollBack();
                $message = Yii::t('main', 'Can not sign up. Server error. Try again later');
                $profile->addError('agree_mailing', $message);
                return $this->signupResponse($user, $profile, $message);
            }

            try {
                MailHelper::sendActivationEmail($user);
            } catch (\Exception $e) {
                $transaction->rollBack();
                Yii::error('Can not send activation email to user ' . $user->id);
                $profile->addError('agree_mailing', 'Can not send activation email');
                $message = Yii::t('main', 'Can not send activation email');

                return $this->signupResponse($user, $profile, $message);
            }

            //success
            $transaction->commit();

            $message = Yii::t('main', 'Account created.');
            $message .= '<br/><p><a href="#" onclick="resendActivationLink(this,' . $user->id . ')"> ' . Yii::t('main', 'Resend activation link') . '</a></p>';

            if ($user->agree_mailing) {
                MailHelper::addToMailchimp($user->email);
            }
        }

        return $this->signupResponse($user, $profile, $message);
    }

    /**
     * @param string $token
     *
     * @return string
     *
     * @throws InvalidParamException
     * @throws NotFoundHttpException
     */
    public function actionActivate($token = null)
    {
        if (empty($token) || !\is_string($token)) {
            return $this->redirect(['user/login']);
        }

        /** @var User $user */
        $user = User::find()
            ->with(['profile'])
            ->where([
                'token' => $token,
            ])
            ->one();

        if (!$user) {
            throw new NotFoundHttpException();
        }

        $model = new FinishSignupForm($user);
        if ($user->status == User::STATUS_CREATED) {
            $model->activate();
        }

        $loaded = $model->load(Yii::$app->request->post());
        $validated = $loaded && $model->validate();
        if ($validated && $model->getUser() && $model->updateUser()) {
            return $this->redirect(['user/login']);
        }

        return $this->render('finishSignup', [
            'model' => $model,
        ]);
    }

    /**
     * Continue auth sign up
     *
     * @param $provider
     * @param $id
     *
     * @return string|Response
     *
     * @throws NotFoundHttpException
     */
    public function actionAuthSignup($provider = null, $id = null)
    {
        $model = new AuthSignupForm([
            'user_id' => (int)$id,
            'provider_name' => $provider,
        ]);

        $model->timezone = Timezone::getCurrentByIP();
        $model->country_id = Country::getCurrentByIP()->id;

        $secureHash = Yii::$app->request->cookies->getValue('sauth');
        if ($secureHash !== md5($id . $provider) || !$model->getUser()) {
            throw new NotFoundHttpException();
        }

        $email = $model->getUser()->email;

        $loaded = $model->load(Yii::$app->request->post());
        if ($email !== $model->email) {
            $model->scenario = 'update-email';
        }

        $validated = $loaded && $model->validate();

        if ($validated && $model->user && $model->updateUser()) {
            if (empty($email) && $model->agree_mailing) {
                MailHelper::addToMailchimp($model->getUser()->email);
            }
            Yii::$app->user->login($model->user);
            Yii::$app->response->cookies->remove('sauth');
            Yii::$app->tooltip->updateStep(TooltipHelper::STEP_EMPTY);

            return $this->redirect('/');
        }

        return $this->render('authSignup', [
            'model' => $model,
        ]);
    }

    /**
     * @param User $user
     * @param Profile $profile
     * @param string $message
     *
     * @return array|string
     *
     * @throws InvalidParamException
     */
    private function signupResponse($user, $profile, $message)
    {
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = 'json';
            return [
                'result' => true,
                'errors' => $user->hasErrors() || $profile->hasErrors(),
                'message' => $message,
                'html' => $this->renderAjax('_signupForm', [
                    'user' => $user,
                    'profile' => $profile,
                ]),
            ];
        }

        return $this->render('signup', [
            'user' => $user,
            'profile' => $profile,
            'message' => $message,
        ]);
    }

    /**
     * @param null $userID
     * @return bool
     * @throws ServerErrorHttpException
     */
    public function actionResendActivation($userID = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $user = User::findOne(['id' => $userID]);
        if (empty($user)) {
            return false;
        }

        try {
            MailHelper::sendActivationEmail($user);
        } catch (\Exception $e) {
            Yii::error('Can not resend activation email to user ' . $user->id);
            return false;
        }

        return true;
    }

    /**
     * @return array|string
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        $message = false;

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                $message = Yii::t('main', 'Check your email for further instructions.');
            } else {
                $message = Yii::t('main', 'Sorry, we are unable to reset password for email provided.');
            }
        }

        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = 'json';
            return [
                'result' => true,
                'errors' => $model->hasErrors(),
                'message' => $message,
                'html' => $this->renderAjax('_requestPasswordResetForm', ['model' => $model]),
            ];
        }

        return $this->render('requestPasswordReset', [
            'model' => $model,
            'message' => $message,
        ]);
    }

    /**
     * @param $token
     * @return string|Response
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if (
            $model->load(Yii::$app->request->post())
            && $model->validate()
            && $model->resetPassword()
        ) {
            Yii::$app->getSession()->setFlash('success', Yii::t('main', 'New password saved.'));

            return $this->redirect(['user/login']);
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    /**
     * @param null $q
     */
    public function actionNameList($q = null)
    {
        $therapists = Therapist::find()->where('therapist_lang.name LIKE "%' . $q . '%" AND publish_status = ' . Therapist::STATUS_PUBLISHED)->joinWith(['translation'])->all();
        $out = [];
        foreach ($therapists as $therapist) {
            $out[] = ['value' => $therapist->name, 'id' => $therapist->user_id];
        }
        echo Json::encode($out);
    }

    /**
     * @param null $alias
     * @param null $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionProfile($alias = null, $id = null)
    {
        $profile = Profile::find()
            ->where(['alias' => $alias])
            ->orWhere(['user_id' => (int)$id])
            ->one();
        if (!$profile) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        return $this->render('profile', [
            'profile' => $profile,
        ]);
    }

    /**
     * @return Response|array
     */
    public function actionGuest()
    {
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = 'json';
            return [
                'result' => true,
                'html' => $this->renderAjax('_guest')
            ];
        }

        return $this->redirect('/therapists');
    }

    /**
     * Login as user for 1 hour
     *
     * @param $h string private hash
     * @param $id integer user id
     * @return Response
     */
    public function actionAutologin($h = null, $id = null)
    {
        $user = User::findOne(['id' => (int)$id]);

        if ($h !== Yii::$app->params['autologinHash'] || empty($user)) {
            return $this->redirect('/');
        }

        if (!Yii::$app->user->isGuest) {
            Yii::$app->user->logout();
        }
        Yii::$app->user->login($user, 3600);

        return $this->redirect('/account');
    }

    /**
     * Activate user tooltips
     */
    public function actionActivateTooltips()
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect('/');
        }
        Yii::$app->tooltip->updateStep(TooltipHelper::STEP_EMPTY);

        /**
         * @var Profile $profile
         */
        $profile = Yii::$app->user->identity->profile;
        $profile->show_tooltips = true;
        $profile->save(false);

        return $this->redirect('/');
    }

    /**
     * Stop user tooltips
     */
    public function actionStopTooltips()
    {
        if (Yii::$app->user->isGuest) {
            if (Yii::$app->request->isPost) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ['result' => false];
            }
            return $this->redirect('/');
        }

        Yii::$app->tooltip->updateStep(TooltipHelper::STEP_5);

        /**
         * @var Profile $profile
         */
        $profile = Yii::$app->user->identity->profile;
        $profile->show_tooltips = false;
        $profile->save(false);

        if (Yii::$app->request->isPost) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['result' => true];
        }

        if (Yii::$app->request->get('finished')) {
            return $this->redirect('/therapists');
        }

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * Delete user
     * @return Response
     */
    public function actionDelete()
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect('/');
        }

        /**
         * @var User $user
         */
        $user = Yii::$app->user->identity;
        $user->status = User::STATUS_DELETED;
        $user->email = User::DELETED_PREFIX . $user->email;
        $user->username = User::DELETED_PREFIX . $user->username;
        $user->save(false);

        Yii::$app->user->logout();

        return $this->redirect('/');
    }

    /**
     * @param string|null $token
     *
     * @return Response
     *
     * @throws \Exception
     * @throws NotFoundHttpException
     * @throws ServerErrorHttpException
     */
    public function actionConfirmNewEmail($token = null)
    {
        /** @var ConfirmTokens $confirmToken */
        $confirmToken = ConfirmTokens::find()
            ->with('user')
            ->where([
                'token' => $token,
                'type' => ConfirmTokens::TYPE_UPDATE_EMAIL
            ])
            ->orderBy('expires DESC')
            ->one();

        if (!$confirmToken || $confirmToken->isExpired() || !$confirmToken->user) {
            throw new NotFoundHttpException();
        }

        $confirmToken->user->email = $confirmToken->data;
        if (!$confirmToken->user->save()) {
            throw new ServerErrorHttpException('Can not update user email');
        }

        $confirmToken->delete();

        if (Yii::$app->user->isGuest) {
            Yii::$app->user->login($confirmToken->user, LoginForm::SESSION_DURATION);
        }

        (new ModalHelper())->setMessage(Yii::t('main', 'Email {0} has been successfully updated', [
            $confirmToken->user->email,
        ]));

        return $this->redirect('/account');
    }
}