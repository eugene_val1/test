<?php

namespace frontend\controllers;

use Yii;
use common\models\Language;
use yii\web\Controller;
use common\models\Article;
use common\models\Therapist;
use common\models\Page;
use yii\web\NotFoundHttpException;

/**
 * Class SitemapController
 * @package frontend\controllers
 */
class SitemapController extends Controller
{
    public $cacheExpire = 86400;
    public $itemsPerMap = 300;
    public $models;

    /**
     * Init
     */
    public function init()
    {

        $this->models = [
            'articles' => Article::className(),
            'therapists' => Therapist::className(),
            'pages' => Page::className(),
        ];

        return parent::init();
    }

    /**
     * @return mixed|string
     */
    public function actionIndex()
    {
        if (!$sitemapIndex = Yii::$app->cache->get('sitemapIndex')) {
            $sitemapIndex = $this->generateSitemapIndex();
        }

        $this->setHeaders();
        return $sitemapIndex;
    }

    /**
     * @param $type
     * @param $page
     * @return mixed|string
     * @throws NotFoundHttpException
     */
    public function actionUrlset($type, $page)
    {
        if (!isset($this->models[$type])) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        $modelName = $this->models[$type];
        $count = $modelName::countAll();

        if ($count < ($page - 1) * $this->itemsPerMap) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        $language = Language::getCurrent();

        if (!$sitemap = Yii::$app->cache->get('sitemap-' . $language->url . '-' . $type . '-' . $page)) {
            $sitemap = $this->generateSitemap($language, $type, $page);
        }

        $this->setHeaders();
        return $sitemap;
    }

    /**
     * @return string
     */
    public function generateSitemapIndex()
    {
        $languages = Language::find()->all();
        $maps = [];

        foreach ($this->models as $name => $modelName) {
            $count = $modelName::countAll();

            if ($count > $this->itemsPerMap) {
                $mapsCount = ceil($count / $this->itemsPerMap);
                for ($i = 1; $i <= $mapsCount; $i++) {
                    $maps[] = $name . '-' . $i;
                }
            } else if ($count > 0) {
                $maps[] = $name . '-1';
            }
        }

        $sitemapIndex = $this->renderPartial('index', [
            'languages' => $languages,
            'maps' => $maps
        ]);

        Yii::$app->cache->set('sitemapIndex', $sitemapIndex, $this->cacheExpire);
        return $sitemapIndex;
    }

    /**
     * @param $language
     * @param $type
     * @param $page
     * @return string
     */
    public function generateSitemap($language, $type, $page)
    {
        $modelName = $this->models[$type];

        $offset = ($page - 1) * $this->itemsPerMap;
        $urls = $modelName::sitemapUrls($this->itemsPerMap, $offset);

        $sitemap = $this->renderPartial('urlset', [
            'urls' => $urls
        ]);

        Yii::$app->cache->set('sitemap-' . $language->url . '-' . $type . '-' . $page, $sitemap, $this->cacheExpire);

        return $sitemap;
    }

    /**
     * Set headers
     */
    public function setHeaders()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
        $headers = Yii::$app->response->headers;
        $headers->add('Content-Type', 'application/xml');
    }
}
