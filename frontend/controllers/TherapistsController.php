<?php

namespace frontend\controllers;

use common\components\GeoService;
use common\models\Country;
use Yii;
use frontend\components\FrontendController;
use common\models\Therapist;
use common\models\User;
use yii\caching\TagDependency;
use yii\data\Pagination;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use common\models\Client;
use common\models\Certificate;
use yii\data\ActiveDataProvider;
use common\models\Appointment;
use frontend\models\CompleteTherapyForm;
use common\models\TherapistReview;
use frontend\components\PageBehavior;
use frontend\models\TherapistFilter;
use frontend\models\ArticleFilter;
use yii\db\Query;
use yii\helpers\Json;
use yii\web\Response;
use yii\web\ServerErrorHttpException;

/**
 * Class TherapistsController
 * @package frontend\controllers
 */
class TherapistsController extends FrontendController
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => PageBehavior::className(),
                'section' => $this->id,
            ],
        ];
    }

    /**
     * @param null $alias
     * @return string|Response
     */
    public function actionIndex($alias = null)
    {
        if (!empty($alias)) {
            return $this->redirect('/therapist/' . $alias);
        }

        $filterModel = new TherapistFilter();
        $dataProvider = $filterModel->filter(Yii::$app->request->queryParams);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'filterModel' => $filterModel
        ]);
    }

    /**
     * @return array|Response
     */
    public function actionFilter()
    {
        Yii::$app->response->format = 'json';
        $filterModel = new TherapistFilter();

        if ($filterModel->load(Yii::$app->request->post()) && $filterModel->validate()) {
            return $this->redirect($filterModel->createUrl('therapists/index'));
        }

        $filterModel->parseUrl(Yii::$app->request->queryParams);

        return [
            'result' => true,
            'errors' => $filterModel->hasErrors(),
            'html' => $this->renderAjax('_filter', [
                'filterModel' => $filterModel,
            ])
        ];
    }

    /**
     * @param $alias
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView($alias = null)
    {
        $cacheKey = Therapist::getCacheKey($alias);
        $therapist = Yii::$app->frontendCache->get($cacheKey);

        if (!$therapist) {
            /** @var Therapist $therapist */
            $therapist = Therapist::findByAlias($alias);

            Yii::$app->frontendCache->set(
                $cacheKey,
                $therapist,
                Therapist::CACHE_DURATION,
                new TagDependency(['tags' => $cacheKey])
            );
        }

        if (!$therapist) {
            throw new NotFoundHttpException;
        }

        //Therapist certificates
        $certificatesCacheKey = Certificate::getCacheKey($therapist->user_id);
        $certificates = Yii::$app->frontendCache->get($certificatesCacheKey);

        if (!$certificates) {
            /** @var Certificate[] $certificates */
            $certificates = Certificate::find()
                ->where(['therapist_id' => $therapist->user_id])
                ->joinWith('translation')
                ->all();

            Yii::$app->frontendCache->set(
                $certificatesCacheKey,
                $certificates,
                Certificate::CACHE_DURATION,
                new TagDependency(['tags' => $certificatesCacheKey])
            );
        }

        //Therapist reviews
        $reviewsCacheKey = TherapistReview::getCacheKey($therapist->user_id);
        $reviews = Yii::$app->frontendCache->get($reviewsCacheKey);

        if (!$reviews) {
            /** @var TherapistReview[] $reviews */
            $reviews = TherapistReview::find()
                ->where([
                    'therapist_id' => $therapist->user_id,
                    'published' => TherapistReview::STATUS_PUBLISHED
                ])
                ->all();

            Yii::$app->frontendCache->set(
                $reviewsCacheKey,
                $reviews,
                TherapistReview::CACHE_DURATION,
                new TagDependency(['tags' => $reviewsCacheKey])
            );
        }

        //Therapist articles
        $articleFilter = new ArticleFilter();

        if (!Yii::$app->user->isGuest && Yii::$app->user->identity->role !== User::ROLE_THERAPIST) {
            $client = Client::find()
                ->where('`client`.`user_id` = :user_id AND `client`.`therapist_id` = :therapist_id AND `client`.`status` = :active', [
                    ':user_id' => Yii::$app->user->id,
                    ':therapist_id' => $therapist->user_id,
                    ':active' => Client::STATUS_ACTIVE
                ])
                ->with('nextAppointment')
                ->one();
        } else {
            $client = false;
        }

        $reviewForm = new TherapistReview([
            'scenario' => Yii::$app->user->isGuest ? 'therapist-page' : '',
        ]);

        if (Yii::$app->user->isGuest) {
            $therapistPrice = $therapist->getPriceByCountry(Country::getCurrentByIP(false));
        } else {
            $therapistPrice = $therapist->getPrice(Yii::$app->user->id);
        }

        return $this->render('view', [
            'therapist' => $therapist,
            'therapistPrice' => $therapistPrice,
            'certificates' => $certificates,
            'review' => $reviewForm,
            'reviews' => $reviews,
            'articleDataProvider' => $articleFilter->filter(['user_id' => $therapist->user_id]),
            'client' => $client,
        ]);
    }

    /**
     * @param $alias
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionTerms($alias = null)
    {
        $therapist = Therapist::findByAlias($alias);

        if (!$therapist) {
            throw new NotFoundHttpException;
        }

        return $this->render('terms', [
            'therapist' => $therapist
        ]);
    }

    /**
     * @param $id
     * @return array
     * @throws NotFoundHttpException
     */
    public function actionCertificate($id = null)
    {
        Yii::$app->response->format = 'json';
        $certificate = Certificate::find()
            ->where(['`certificate`.`id`' => $id])
            ->joinWith('translation')
            ->one();

        if (!$certificate) {
            throw new NotFoundHttpException;
        }

        return [
            'result' => true,
            'html' => $this->renderPartial('_certificate', [
                'model' => $certificate,
            ])
        ];
    }

    /**
     * @return array
     */
    public function actionTherapy()
    {
        Yii::$app->response->format = 'json';
        $query = Client::find()
            ->andWhere(['user_id' => Yii::$app->user->id])
            ->andWhere(['status' => [
                Client::STATUS_ACTIVE,
                Client::STATUS_PENDING,
                Client::STATUS_INACTIVE,
            ]])
            ->with([
                'therapist',
                'newTherapistMessages',
                'nextAppointment',
                'completedAppointments'
            ])
            ->orderBy('status ASC, next_appointment_start ASC');

        $countQuery = clone $query;
        $pages = new Pagination([
            'totalCount' => $countQuery->count(),
            'pageSize' => 3,
        ]);

        $models = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        return [
            'result' => true,
            'html' => $this->renderAjax('_therapy_new', [
                'models' => $models,
                'pages' => $pages,
            ])
        ];
    }

    /**
     * @param $id
     * @return array
     */
    public function actionCompleteTherapy($id = null)
    {
        Yii::$app->response->format = 'json';
        $client = Client::find()
            ->where('id = :id', [':id' => $id])
            ->with('nextAppointment')
            ->one();

        if (!$client) {
            return [
                'result' => false,
            ];
        }

        $message = false;
        $model = new CompleteTherapyForm(['client' => $client]);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->send();

            if ($client->nextAppointment->payment_status == Appointment::NOT_PAID) {
                $client->nextAppointment->delete();
                $client->complete_status = Client::COMPLETED;
                $client->setStatus(Client::STATUS_INACTIVE);
            } else {
                $client->nextAppointment->removeDate();
                $client->next_appointment_start = Client::NO_NEXT_APPPOINTMENT;
                $client->complete_status = Client::COMPLETED;
                $client->setStatus(Client::STATUS_PENDING);
            }

            $message = Yii::t('main', 'You have completed therapy with this therapist');
        }

        return [
            'result' => true,
            'errors' => $model->hasErrors(),
            'message' => $message,
            'html' => $this->renderAjax('_completeForm', [
                'model' => $model,
            ]),
        ];
    }

    /**
     * @param $id
     * @return array
     * @throws NotFoundHttpException
     */
    public function actionReview($id = null)
    {
        Yii::$app->response->format = 'json';

        $therapist = Therapist::find()
            ->where('user_id = :id', [':id' => $id])
            ->one();

        if ($therapist === null) {
            throw new NotFoundHttpException;
        }

        $model = new TherapistReview([
            'therapist_id' => $therapist->user_id,
        ]);

        if (Yii::$app->user->isGuest) {
            $model->scenario = 'guest';
        } else {
            $user = Yii::$app->user->identity;
            $model->name = $user->profile->name;
        }

        $message = false;

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->save(false);
            $message = Yii::t('main', 'Thank you for review. It will be published here after moderation');
        }

        return [
            'result' => true,
            'errors' => $model->hasErrors(),
            'message' => $message,
            'html' => $this->renderAjax('_reviewForm', [
                'model' => $model,
                'therapist' => $therapist,
            ]),
        ];
    }

    /**
     * @param bool $id
     * @return array
     */
    public function actionRandom($id = false)
    {
        Yii::$app->response->format = 'json';
        $query = Therapist::find()
            ->where(['publish_status' => Therapist::STATUS_PUBLISHED])
            ->orderBy('RAND()');

        if ($id && is_numeric($id)) {
            $query->andWhere(['<>', 'id', $id]);
        }

        return [
            'result' => true,
            'html' => $this->renderAjax('_random', [
                'therapist' => $query->one(),
            ])
        ];
    }

    /**
     * @return array
     */
    public function actionFeedback()
    {
        Yii::$app->response->format = 'json';
        $user = Yii::$app->user->identity;
        $model = new TherapistReview(['name' => $user->profile->name]);

        $message = false;

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->save(false);
            $message = Yii::t('main', 'Thank you for feedback, it will published on therapist page after moderation');
        }

        return [
            'result' => true,
            'errors' => $model->hasErrors(),
            'message' => $message,
            'html' => $this->renderAjax('_feedback', [
                'model' => $model,
            ]),
        ];
    }

    /**
     * @param null $q
     */
    public function actionNameList($q = null)
    {
        $therapists = new Query;
        $therapists->select('name, user_id')
            ->from('therapist_lang')
            ->where('language="' . Yii::$app->language . '" AND name LIKE "%' . $q . '%"')
            ->orderBy('name');
        $command = $therapists->createCommand();
        $data = $command->queryAll();

        $out = [];
        foreach ($data as $d) {
            $out[] = ['value' => $d['name'], 'id' => $d['user_id']];
        }

        echo Json::encode($out);
    }

    /**
     * Get therapist catalog image
     * @param null $id
     * @throws ServerErrorHttpException
     */
    public function actionCatalogImage($id = null)
    {
        $therapist = Therapist::findOne(['user_id' => $id]);
        if (empty($therapist)) {
            return;
        }

        $response = Yii::$app->getResponse();
        $response->headers->set('Content-Type', 'image/jpeg');
        $response->format = Response::FORMAT_RAW;
        if (!is_resource($response->stream = fopen($therapist->getFileUrl('catalog_image'), 'r'))) {
            throw new ServerErrorHttpException('file access failed: permission deny');
        }

        return $response->send();
    }

    /**
     * @return string
     */
    public function actionReviews()
    {
        $therapistReviews = TherapistReview::find()
            ->with('therapist')
            ->where(['published' => TherapistReview::STATUS_PUBLISHED])
            ->orderBy('created DESC')
            ->limit(4)
            ->all();

        $therapistReviews = ArrayHelper::index($therapistReviews, null, 'therapist_id');

        return $this->render('reviews', [
            'therapistReviews' => $therapistReviews,
        ]);
    }
}