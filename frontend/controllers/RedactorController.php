<?php

namespace frontend\controllers;

use yii\web\Controller;
use yii\filters\AccessControl;
use common\components\actions\UploadAction;
use common\components\actions\GetAction;

class RedactorController extends Controller
{
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['therapist'],
                    ],
                ],
            ],
        ];
    }
    
    public function actions()
    {
        return [
            'image-upload' => [
                'class' => UploadAction::className(),
            ],
            'images-get' => [
                'class' => GetAction::className(),
            ]
        ];
    }
}
