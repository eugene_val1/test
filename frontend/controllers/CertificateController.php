<?php
namespace frontend\controllers;

use Yii;
use frontend\components\FrontendController;
use yii\filters\AccessControl;
use common\models\Certificate;
use common\models\CertificateLang;
use yii\filters\ContentNegotiator;
use yii\web\Response;
use common\models\Language;
use frontend\models\UploadImage;
use yii\helpers\Html;
use yii\web\NotFoundHttpException;
use yii\helpers\FileHelper;
use yii\base\Model;
use yii\filters\VerbFilter;
/**
 *  Certificate controller
 */
class CertificateController extends FrontendController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['therapist'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'remove' => ['post'],
                ],
            ],
            [
                'class' => ContentNegotiator::className(),
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }
    
    public function actionList()
    {
        $therapist = Yii::$app->user->identity;
        $certificates = Certificate::find()->where(['therapist_id' => $therapist->id])->joinWith('translation')->all();

        return [
            'result' => true,
            'html' => $this->renderAjax('_list', [
                'certificates' => $certificates
            ])
        ];
    }
    
    public function actionCreate()
    {
        $therapist = Yii::$app->user->identity;
        $model = new Certificate([
            'therapist_id' => $therapist->id
        ]);
        $languages = Language::find()->indexBy('local')->all();
        $translations = [];
        $message = false;
        $errors = false;

        foreach ($languages as $language) {
            $translations[$language->local] = new CertificateLang([
                'language' => $language->local
            ]);
        }

        if ($model->load(Yii::$app->request->post()) && Model::loadMultiple($translations, Yii::$app->request->post())) {
            if($model->validate() && Model::validateMultiple($translations)){                
                $model->save(false);                
                foreach ($translations as $translation) {
                    $translation->certificate_id = $model->id;
                    $translation->save(false);
                }                
                $message = Yii::t('main', 'Certificate added successfully');
            } else {
                $errors = true;
            }
        }
        
        return [
                'result' => true,
                'errors' => $errors,
                'message' => $message,
                'html' => $this->renderAjax('_form', [
                    'model' => $model,
                    'translations' => $translations,
                    'languages' => $languages,
                    'upload' => new UploadImage()
                ])
            ];
    }
    
    public function actionUpdate($id)
    {
        $model = $this->getModel($id);        
        $languages = Language::find()->indexBy('local')->all();
        $translations = $model->translations;
        $message = false;
        $errors = false;

        if ($model->load(Yii::$app->request->post()) && Model::loadMultiple($translations, Yii::$app->request->post())) {
            if($model->validate() && Model::validateMultiple($translations)){
                $model->save(false);                
                foreach ($translations as $translation) {
                    $translation->save(false);
                }                
                $message = Yii::t('main', 'Certificate updated successfully');
            } else {
                $errors = true;
            }
        }
        
        return [
                'result' => true,
                'errors' => $errors,
                'message' => $message,
                'html' => $this->renderAjax('_form', [
                    'model' => $model,
                    'translations' => $translations,
                    'languages' => $languages,
                    'upload' => new UploadImage()
                ])
            ];
    }
    
    public function actionUploadImage($id = false) {
        $model = new UploadImage();
        if ($model->validate(['image'])) {
                return [
                    'status' => true,
                    'img' => Html::img($model->getFileUrl('name'), ['class' => 'img-responsive']),
                    'name' => $model->name,
                ];
        } else {
            return [
                'status' => false,
                'error' => $model->getFirstError('image')
            ];
        }
    }

    public function actionRemoveConfirmation($id)
    {
        $model = $this->getModel($id);
        
        return [
                'result' => true,
                'html' => $this->renderAjax('_remove_confirmation', [
                    'model' => $model
                ])
            ];
    }
    
    public function actionRemove($id) {
        $model = $this->getModel($id);
        $model->delete();
        return [
            'result' => true
        ];
    }
    
    public function getModel($id){
        if (($model = Certificate::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
