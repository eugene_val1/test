<?php

namespace frontend\controllers;

use common\components\bots\FacebookBotService;
use common\components\bots\TelegramBotService;
use common\components\MailHelper;
use common\models\ConfirmTokens;
use common\models\Event;
use common\models\NotificationSettings;
use common\models\SystemNews;
use common\models\SystemNewsRead;
use common\models\User;
use Yii;
use frontend\components\FrontendController;
use yii\data\Pagination;
use yii\filters\AccessControl;
use common\models\Profile;
use common\models\Therapist;
use common\models\TherapistProperty;
use yii\base\Model;
use yii\helpers\Json;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use common\models\RegionLang;
use common\models\RegionPrice;
use common\models\Transaction;
use frontend\components\PageBehavior;
use yii\helpers\ArrayHelper;
use yii\web\Response;

/**
 * Class AccountController
 * @package frontend\controllers
 */
class AccountController extends FrontendController
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            [
                'class' => PageBehavior::className(),
                'section' => $this->id,
            ],
        ];
    }

    /**
     * @param \yii\base\Action $event
     * @return bool
     */
    public function beforeAction($event)
    {
        if ($this->action->id === 'index' && Yii::$app->request->isPost) {
            \Yii::$app->controller->enableCsrfValidation = false;
        }
        return parent::beforeAction($event);
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $user = Yii::$app->user->identity;
        $profile = $user->profile;
        $notifications = $user->notificationSettings;

        $tid = Yii::$app->request->get('tid');
        $transaction = Transaction::findOne([
            'id' => (int)$tid,
            'user_id' => Yii::$app->user->identity->id,
        ]);

        if (!empty($transaction) && $transaction->status_update_informed) {
            $transaction = null;
            $tid = null;
        } /*elseif (!empty($transaction) && $transaction->status === Transaction::STATUS_SUCCESS) {
            $transactionsCount = Transaction::find()
                ->where(['user_id' => Yii::$app->user->identity->id])
                ->count();

            if ($transactionsCount < 2) {
                return $this->render('after_first_payment');
            }
        }*/

        return $this->render($profile->type, [
            'user' => $user,
            'profile' => $profile,
            'notifications' => $notifications,
            'tid' => $tid,
            'transaction' => $transaction,
        ]);
    }

    /**
     * @param User $user
     *
     * @return array
     */
    protected function getBots($user)
    {
        $bots = [];
        if (!TelegramBotService::getInstance()->isConnected($user->getId())) {
            $bots['telegram'] = TelegramBotService::getInstance()->buildConnectLink($user->getId());
        }
        if (!FacebookBotService::getInstance()->isConnected($user->getId())) {
            $bots['facebook'] = FacebookBotService::getInstance()->buildConnectLink($user->getId());
        }

        return $bots;
    }

    /**
     * User profile
     * @return string
     */
    public function actionProfile()
    {
        /**
         * @var User $user
         */
        $user = Yii::$app->user->identity;

        if ($user->role === User::ROLE_THERAPIST) {
            return $this->actionUpdateTherapist();
        }

        return $this->actionUpdateUser();
    }

    /**
     * Update user
     *
     * @return string
     */
    public function actionUpdateUser()
    {
        //Yii::$app->response->format = 'json';

        /** @var User $user */
        $user = Yii::$app->user->identity;
        /** @var Profile $profile */
        $profile = $user->profile;
        /** @var NotificationSettings $notifications */
        $notifications = $user->notificationSettings;

        $email = $user->email;
        $timezone = $user->timezone;

        $post = Yii::$app->request->post();
        $loaded = $user->load($post) && $profile->load($post) && $notifications->load($post);

        if ($email !== $user->email) {
            $user->scenario = 'update-email';
        }

        $validated = $loaded && Model::validateMultiple([$user, $profile, $notifications]);

        if ($validated) {
            $transaction = Yii::$app->db->beginTransaction();

            if ($user->user_password) {
                $user->setPassword($user->user_password);
                $user->generateAuthKey();
            }

            $user->save(false);
            $profile->save(false);
            $notifications->save(false);

            //verify email
            if ($user->email !== $email) {
                $confirmToken = new ConfirmTokens([
                    'user_id' => $user->id,
                    'type' => ConfirmTokens::TYPE_UPDATE_EMAIL,
                    'data' => $user->email,
                ]);
                if ($confirmToken->save()) {
                    MailHelper::sendConfirmNewEmail($user, $user->email, $confirmToken->token);
                    Yii::$app->getSession()->setFlash('info', Yii::t('main', 'Email will be updated after confirmation. Check your new email to confirm it.'));
                    $user->email = $email;
                    $user->save(false);
                }
            }

            $transaction->commit();
            Yii::$app->getSession()->setFlash('success', Yii::t('main', 'Data saved successfully'));

            if ($user->timezone !== $timezone) {
                $event = new Event([
                    'type' => Event::TYPE_CHANGE_TIMEZONE,
                    'text' => 'timezone',
                    'importance' => Event::IMPORTANCE_NORMAL,
                    'target' => Event::RECEIVER_USER,
                    'data' => Json::encode([
                        'user_id' => $user->id,
                        'timezone' => $user->timezone,
                    ])
                ]);
                $event->save(false);
            }
        }

        return $this->render('edit_' . $profile->type, [
            'user' => $user,
            'profile' => $profile,
            'notifications' => $notifications,
            'bots' => $this->getBots($user),
        ]);
    }

    /**
     * Update therapist
     *
     * @return string
     */
    public function actionUpdateTherapist()
    {
        //Yii::$app->response->format = 'json';

        /** @var User $user */
        $user = Yii::$app->user->identity;
        $notifications = $user->notificationSettings;
        $profile = $user->profile;

        $timezone = $user->timezone;
        $email = $user->email;

        $loaded = $user->load(Yii::$app->request->post()) && $notifications->load(Yii::$app->request->post());
        if ($email !== $user->email) {
            $user->scenario = 'update-email';
        }

        $validated = $loaded && $user->validate() && $notifications->validate();

        if ($validated) {
            $transaction = Yii::$app->db->beginTransaction();

            if ($user->user_password) {
                $user->setPassword($user->user_password);
                $user->generateAuthKey();
            }

            $user->save(false);
            $notifications->save(false);

            //verify email
            if ($user->email !== $email) {
                $confirmToken = new ConfirmTokens([
                    'user_id' => $user->id,
                    'type' => ConfirmTokens::TYPE_UPDATE_EMAIL,
                    'data' => $user->email,
                ]);
                if ($confirmToken->save()) {
                    MailHelper::sendConfirmNewEmail($user, $user->email, $confirmToken->token);
                    Yii::$app->getSession()->setFlash('info', Yii::t('main', 'Email will be updated after confirmation. Check your new email to confirm it.'));
                    $user->email = $email;
                    $user->save(false);
                }
            }

            $transaction->commit();
            Yii::$app->getSession()->setFlash('success', Yii::t('main', 'Data saved successfully'));

            if ($user->timezone !== $timezone) {
                $event = new Event([
                    'type' => Event::TYPE_CHANGE_TIMEZONE,
                    'text' => 'timezone',
                    'importance' => Event::IMPORTANCE_NORMAL,
                    'target' => Event::RECEIVER_USER,
                    'data' => Json::encode([
                        'user_id' => $user->id,
                        'timezone' => $user->timezone,
                    ])
                ]);
                $event->save(false);
            }
        }

        return $this->render('edit_' . $profile->type, [
            'user' => $user,
            'profile' => $profile,
            'notifications' => $notifications,
            'bots' => $this->getBots($user),
        ]);
    }

    /**
     * @return array
     */
    public function actionTherapistData()
    {
        Yii::$app->response->format = 'json';

        /** @var Profile $profile */
        $profile = Yii::$app->user->identity->profile;
        $translation = $profile->translation;
        $profile->scenario = $translation->scenario = 'update';
        $profile->setProperties();

        if ($translation->load(Yii::$app->request->post()) && $profile->load(Yii::$app->request->post()) &&
            Model::validateMultiple([$translation, $profile])
        ) {
            $profile->setStatus(Therapist::STATUS_WAITING);
            $translation->save(false);

            TherapistProperty::deleteAll('therapist_id = :id', [':id' => $profile->user_id]);

            foreach ($profile->properties as $id) {
                $property = new TherapistProperty();
                $property->setAttributes(['therapist_id' => $profile->user_id, 'property_id' => $id]);
                $property->save();
            }

        }

        return [
            'result' => true,
            'errors' => $profile->hasErrors() || $translation->hasErrors(),
            'html' => $this->renderAjax('_therapist_data', [
                'translation' => $translation,
                'profile' => $profile
            ])
        ];
    }

    /**
     * @param $language
     * @return array
     */
    public function actionTranslate($language)
    {
        Yii::$app->response->format = 'json';
        Yii::$app->language = $language;
        $message = false;
        $profile = Yii::$app->user->identity->profile;
        $translation = $profile->translations[$language];
        $translation->scenario = 'update';

        if ($translation->load(Yii::$app->request->post()) && $translation->validate()) {
            $translation->save(false);
            $profile->setStatus(Therapist::STATUS_WAITING);
            $message = Yii::t('main', 'Translation saved successfully');
        }

        return [
            'result' => true,
            'errors' => $translation->hasErrors(),
            'message' => $message,
            'html' => $this->renderAjax('_translate_therapist_data', [
                'translation' => $translation
            ])
        ];
    }

    /**
     * @return array
     */
    public function actionTherapistPrices()
    {
        Yii::$app->response->format = 'json';
        $therapist = Yii::$app->user->identity->profile;
        $regions = RegionLang::find()->where(['language' => Yii::$app->language])->all();
        $prices = RegionPrice::find()->where(['therapist_id' => $therapist->user_id])
            ->joinWith('region')
            ->indexBy('region_id')
            ->all();
        $errors = false;

        foreach ($regions as $region) {
            if (!isset($prices[$region->region_id])) {
                $prices[$region->region_id] = new RegionPrice([
                    'therapist_id' => $therapist->user_id,
                    'region_id' => $region->region_id,
                    'regionName' => $region->name
                ]);
            }
        }

        if (Model::loadMultiple($prices, Yii::$app->request->post())) {
            if (Model::validateMultiple($prices)) {
                foreach ($prices as $price) {
                    $price->save(false);
                }
            } else {
                $errors = true;
            }
        }

        return [
            'result' => true,
            'errors' => $errors,
            'html' => $this->renderAjax('_therapist_prices', [
                'prices' => $prices,
                'therapist' => $therapist
            ])
        ];
    }

    /**
     * @param $id
     * @param string $attribute
     *
     * @return string
     *
     * @throws \yii\web\BadRequestHttpException
     * @throws NotFoundHttpException
     */
    public function actionUploadImage($id, $attribute = 'image')
    {
        $profile = Profile::findOne($id);
        if (!$profile) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        $attributes = $profile->getImageAttributes();
        if (!key_exists($attribute, $attributes)) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        $fileAttribute = ArrayHelper::getValue($attributes[$attribute], 'fileAttribute', false);

        if ($fileAttribute && $profile->validate([$fileAttribute])) {
            if ($profile::TYPE == 'therapist') {
                $profile->setStatus(Therapist::STATUS_WAITING);
            } else {
                $profile->save(false);
            }
        } else {
            throw new BadRequestHttpException('Invalid image');
        }

        return $profile->getFileUrl($attribute);
    }

    /**
     * @return array
     * @throws NotFoundHttpException
     */
    public function actionSubmit()
    {
        Yii::$app->response->format = 'json';
        $profile = Yii::$app->user->identity->profile;

        if (!$profile || $profile->type != Therapist::TYPE) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        $profile->setStatus(Therapist::STATUS_WAITING);

        return [
            'result' => true,
            'html' => $this->renderPartial('_status', [
                'profile' => $profile
            ])
        ];
    }

    /**
     * @return array|Response
     */
    public function actionFinances()
    {
        if (!Yii::$app->request->isAjax) {
            return $this->redirect('/account');
        }

        Yii::$app->response->format = 'json';

        $isTherapist = Yii::$app->user->identity->isTherapist();
        $field = $isTherapist ? 'therapist_id' : 'user_id';

        $query = Transaction::find()
            ->where($field . '=:user_id', [':user_id' => Yii::$app->user->id])
            ->with(['therapistLang', 'profile', 'appointment', 'therapist'])
            ->orderBy('created DESC');

        $countQuery = clone $query;
        $pages = new Pagination([
            'totalCount' => $countQuery->count(),
            'pageSize' => $isTherapist ? 5 : 3,
        ]);
        $models = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        return [
            'result' => true,
            'html' => $this->renderAjax('_finances_new', [
                'models' => $models,
                'pages' => $pages,
            ])
        ];
    }

    /**
     * @return array|Response
     */
    public function actionSystemNews()
    {
        if (!Yii::$app->request->isAjax) {
            return $this->redirect('/account');
        }

        Yii::$app->response->format = Response::FORMAT_JSON;

        $isTherapist = Yii::$app->user->identity->isTherapist();
        $target = $isTherapist
            ? SystemNews::TARGET_THERAPISTS
            : SystemNews::TARGET_CLIENTS;

        $readNewsIds = ArrayHelper::getColumn(Yii::$app->user->identity->readSystemNews, 'system_news_id');

        $query = SystemNews::find()
            ->where(['status' => SystemNews::STATUS_PUBLISHED])
            ->andWhere(['target' => [SystemNews::TARGET_ALL, $target]])
            ->andWhere(['>=', 'created', Yii::$app->user->identity->created])
            ->orderBy('created DESC');

        $countQuery = clone $query;
        $pages = new Pagination([
            'totalCount' => $countQuery->count(),
            'pageSize' => 3,
        ]);
        $models = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        //mark read
        foreach ($models as $model) {
            if (in_array($model->id, $readNewsIds)) {
                continue;
            }

            $readModel = new SystemNewsRead();
            $readModel->user_id = Yii::$app->user->identity->getId();
            $readModel->system_news_id = $model->id;
            $readModel->save();
        }

        return [
            'result' => true,
            'html' => $this->renderAjax('_system_news', [
                'models' => $models,
                'pages' => $pages,
                'readNewsIds' => $readNewsIds,
            ])
        ];
    }

    /**
     * @return array
     */
    public function actionFeedback()
    {
        Yii::$app->response->format = 'json';
        return [
            'result' => true,
            'html' => $this->renderPartial('_feedback')
        ];
    }

    /**
     * @param int $tid
     * @return array|string
     * @throws NotFoundHttpException
     */
    public function actionPayment($tid = null)
    {
        $transaction = Transaction::findOne([
            'id' => (int)$tid,
            'user_id' => Yii::$app->user->identity->id,
        ]);

        if (null === $transaction) {
            throw new NotFoundHttpException('Transaction not found');
        }

        $transaction->status_update_informed = true;
        $transaction->save();

        Yii::$app->response->format = 'json';

        return [
            'result' => true,
            'html' => $this->renderAjax('_after_payment_modal', [
                'status' => $transaction->status
            ])
        ];
    }
}
