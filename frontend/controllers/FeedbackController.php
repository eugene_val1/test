<?php

namespace frontend\controllers;

use Yii;
use frontend\components\FrontendController;
use yii\filters\AccessControl;
use yii\data\ActiveDataProvider;
use common\models\Feedback;
use yii\web\NotFoundHttpException;
use common\models\User;

/**
 *  Feedback controller
 */
class FeedbackController extends FrontendController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['post', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $query = Feedback::find()
            ->where(['published' => Feedback::STATUS_PUBLISHED])
            ->orderBy('created DESC');

        $provider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 20,
            ]
        ]);

        return $this->render('index', [
            'provider' => $provider
        ]);
    }

    /**
     * @param bool $id
     * @return array
     * @throws NotFoundHttpException
     */
    public function actionPost($id = false)
    {
        Yii::$app->response->format = 'json';
        $message = false;

        if (!$id) {
            $model = new Feedback(['user_id' => Yii::$app->user->id,]);
        } else {
            $model = Feedback::findOne(['id' => $id]);
            if ($model->user_id != Yii::$app->user->id && Yii::$app->user->identity->role != User::ROLE_ADMIN) {
                throw new NotFoundHttpException('The requested page does not exist.');
            }
            if ($model->user_id == Yii::$app->user->id) {
                $model->published = Feedback::STATUS_DRAFT;
            }
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->save(false);
            $message = ($model->user_id == Yii::$app->user->id) ? Yii::t('main', 'Thank you for your feedback. It will be published after moderation') : Yii::t('main', 'Feedback text changed');
        }

        return [
            'result' => true,
            'errors' => $model->hasErrors(),
            'message' => $message,
            'html' => $this->renderAjax('_form', [
                'model' => $model
            ]),
            'text' => Yii::$app->formatter->asNtext($model->text)
        ];
    }
}
