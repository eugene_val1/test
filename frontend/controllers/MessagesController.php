<?php

namespace frontend\controllers;

use common\models\Appointment;
use common\models\Client;
use common\models\Conversation;
use common\models\Messages\ConversationAppointmentMessage;
use common\models\Messages\ConversationMessage;
use common\models\Therapist;
use common\models\User;
use frontend\components\FrontendController;
use frontend\components\PageBehavior;
use Yii;
use yii\base\Action;
use yii\base\Exception;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\ServerErrorHttpException;

/**
 * Class MessagesController
 * @package frontend\controllers
 */
class MessagesController extends FrontendController
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            [
                'class' => PageBehavior::className(),
                'section' => $this->id,
            ],
        ];
    }

    /**
     * @param Action $event
     * @return bool
     */
    public function beforeAction($event)
    {
        if ($this->action->id === 'send') {
            $this->enableCsrfValidation = false;
        }
        return parent::beforeAction($event);
    }

    /**
     * @return string
     * @throws ServerErrorHttpException
     */
    public function actionIndex()
    {
        try {
            $conversations = Conversation::find()
                ->alias('conv')
                ->where('conv.sender_id = :user_id OR conv.receiver_id = :user_id', [
                    ':user_id' => Yii::$app->user->id,
                ])
                ->andWhere(['hidden' => false])
                ->with(['sender', 'receiver', 'newMessages'])
                ->joinWith(['lastMessage lm'])
                ->orderBy('lm.created DESC')
                ->all();
        } catch (Exception $e) {
            Yii::error($e->getMessage());
            throw new ServerErrorHttpException('Server error');
        }

        $contacts = [];
        $messages = [];

        foreach ($conversations as $conversation) {
            if ($conversation->sender && $conversation->receiver) {
                $contact = $conversation->sender->user_id == Yii::$app->user->id
                    ? $conversation->receiver
                    : $conversation->sender;

                if ($conversation->lastMessage) {
                    $conversation->lastMessage->contact = $contact;
                    $messages[] = $conversation->lastMessage;
                }

                $contacts[] = [
                    'conversationId' => $conversation->id,
                    'user' => $contact,
                    'newMessagesCount' => count($conversation->newMessages),
                ];
            }
        }

        return $this->render('index', [
            'messages' => $messages,
            'contacts' => $contacts,
        ]);
    }

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionConversation($id)
    {
        $conversation = Conversation::findOne($id);

        if ($conversation === null
            || !($conversation->sender_id == Yii::$app->user->id
            || $conversation->receiver_id == Yii::$app->user->id)
        ) {
            throw new NotFoundHttpException;
        }

        $limit = 20;
        $query = ConversationMessage::find()->where(['conversation_id' => $id]);
        $count = $query->count();
        $offset = (($count - $limit) > 0) ? $count - $limit : 0;

        $messages = $query->with('user')->offset($offset)->limit($limit)->all();
        $contact = $conversation->sender_id == Yii::$app->user->id
            ? $conversation->receiver
            : $conversation->sender;

        //get client appointments statuses
        $appointments = [];
        if (Yii::$app->user->identity->role == User::ROLE_USER) {
            $appointments = Appointment::find()
                ->select(['id', 'payment_status'])
                ->where(['user_id' => Yii::$app->user->getId()])
                ->all();
            $appointments = ArrayHelper::map($appointments, 'id', 'payment_status');
        }

        if ($contact === null) {
            throw new NotFoundHttpException;
        }

        $this->markRead($conversation->id);

        $newMessage = new ConversationMessage();

        return $this->render('conversation', [
            'offset' => $offset,
            'conversation' => $conversation,
            'contact' => $contact,
            'messages' => $messages,
            'newMessage' => $newMessage,
            'contacts' => $this->getContacts(),
            'appointments' => $appointments
        ]);
    }

    /**
     * @param $id
     * @return array|Response
     * @throws NotFoundHttpException
     */
    public function actionSend($id)
    {
        Yii::$app->response->format = 'json';

        $contact = User::findOne($id);
        if ($contact === null) {
            throw new NotFoundHttpException;
        }

        $message = false;
        $newMessage = new ConversationMessage();
        $newMessage->scenario = 'contact';
        $newMessage->contact_id = $id;

        if ($newMessage->load(Yii::$app->request->post()) && $newMessage->validate()) {
            $conversation = $this->getConversation($id);
            $newMessage->user_id = Yii::$app->user->id;
            $newMessage->receiver_id = $conversation->sender_id == Yii::$app->user->id
                ? $conversation->receiver_id
                : $conversation->sender_id;
            $newMessage->conversation_id = $conversation->id;
            $newMessage->save(false);

            if ($newMessage->client) {
                $newMessage->client->updateLastMessageDate();
            }

            $message = Yii::t('main', 'Your message sent');
        }

        return [
            'result' => true,
            'errors' => $newMessage->hasErrors(),
            'message' => $message,
            'html' => $this->renderAjax('_sendForm', ['newMessage' => $newMessage, 'id' => $id, 'contact' => $contact])
        ];
    }

    /**
     * @param $id
     * @return array
     * @throws NotFoundHttpException
     */
    public function actionRequestAppointment($id)
    {
        Yii::$app->response->format = 'json';

        $therapist = Therapist::find()->where('user_id = :id', [':id' => $id])->one();

        if ($therapist === null) {
            throw new NotFoundHttpException;
        }

        $message = false;
        $model = new ConversationAppointmentMessage();
        $model->scenario = 'contact';
        $model->contact_id = $id;

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $conversation = $this->getConversation($id);
            $model->user_id = Yii::$app->user->id;
            $model->conversation_id = $conversation->id;
            $model->receiver_id = $id;
            $model->save(false);

            $message = Yii::t('main', 'Your message sent');
        }

        return [
            'result' => true,
            'errors' => $model->hasErrors(),
            'message' => $message,
            'html' => $this->renderAjax('_appointment', [
                'model' => $model,
                'therapist' => $therapist,
                'id' => $id,
            ])
        ];
    }

    /**
     * @param $conversationId
     * @param $lastId
     * @param $lastUserId
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionList($conversationId, $lastId, $lastUserId)
    {
        $conversation = Conversation::findOne($conversationId);

        if ($conversation === null
            || !($conversation->sender_id == Yii::$app->user->id || $conversation->receiver_id == Yii::$app->user->id)
        ) {
            throw new NotFoundHttpException;
        }

        $messages = ConversationMessage::find()
            ->where('id > :lastId AND conversation_id = :conversation_id', [
                ':lastId' => $lastId,
                ':conversation_id' => $conversationId
            ])
            ->all();

        $this->markRead($conversationId);

        if (empty($messages)) {
            return '';
        }

        return $this->renderAjax('_messages', [
            'messages' => $messages,
            'lastUserId' => $lastUserId,
        ]);
    }

    /**
     * @param $conversationId
     * @param $startId
     * @return array
     */
    public function actionPrevious($conversationId, $startId)
    {
        Yii::$app->response->format = 'json';
        $conversation = Conversation::findOne($conversationId);

        if ($conversation === null
            || !($conversation->sender_id == Yii::$app->user->id
                || $conversation->receiver_id == Yii::$app->user->id)
        ) {
            return [
                'result' => false
            ];
        }

        $limit = 20;
        $query = ConversationMessage::find()
            ->where('id < :lastId AND conversation_id = :conversation_id', [
                ':lastId' => $startId,
                ':conversation_id' => $conversationId
            ]);
        $count = $query->count();
        $offset = (($count - $limit) > 0) ? $count - $limit : 0;

        $messages = $query->with('user')->offset($offset)->limit($limit)->all();

        return [
            'result' => true,
            'offset' => $offset,
            'html' => $this->renderAjax('_messages', [
                'messages' => $messages,
                'lastUserId' => false,
            ])
        ];
    }

    /**
     * @param $id
     */
    public function markRead($id)
    {
        ConversationMessage::updateAll(['status' => ConversationMessage::STATUS_READ],
            'conversation_id = :conversation_id AND user_id != :user_id', [
                ':conversation_id' => $id,
                ':user_id' => Yii::$app->user->id,
            ]);
    }

    /**
     * @return array
     */
    public function getContacts()
    {
        $conversations = Conversation::find()
            ->alias('conv')
            ->where('conv.sender_id = :user_id OR conv.receiver_id = :user_id', [':user_id' => Yii::$app->user->id])
            ->andWhere(['hidden' => false])
            ->with(['sender', 'receiver', 'newMessages'])
            ->joinWith(['lastMessage lm'])
            ->orderBy('lm.created DESC')
            ->all();

        $contacts = [];

        foreach ($conversations as $conversation) {
            if ($conversation->sender && $conversation->receiver) {
                $contact = $conversation->sender->user_id == Yii::$app->user->id
                    ? $conversation->receiver
                    : $conversation->sender;
                $contacts[] = [
                    'conversationId' => $conversation->id,
                    'user' => $contact,
                    'newMessagesCount' => count($conversation->newMessages)
                ];
            }
        }

        return $contacts;
    }

    /**
     * @param $id
     * @return array|Conversation|null|\yii\db\ActiveRecord
     */
    public function getConversation($id)
    {
        $conversation = Conversation::find()
            ->where('(sender_id = :user_id AND receiver_id = :contact_id) OR (sender_id = :contact_id AND receiver_id = :user_id)', [
                'user_id' => Yii::$app->user->id,
                'contact_id' => $id
            ])
            ->one();

        if ($conversation === null) {
            $conversation = new Conversation([
                'sender_id' => Yii::$app->user->id,
                'receiver_id' => $id
            ]);
            $conversation->save();
        }

        if ($this->isNewClient($id)) {
            $attributes = [
                'therapist_id' => $id,
                'user_id' => Yii::$app->user->id,
                'conversation_id' => $conversation->id
            ];
            $client = Client::findOne($attributes);
            $therapist = Therapist::findOne(['user_id' => $id]);
            if (!$client) {
                $client = new Client($attributes);
                //set default region price
                $client->price = $therapist->getPrice(Yii::$app->user->id, false);
                $client->setStatus(Client::STATUS_NEW);
            }
        }

        return $conversation;
    }

    /**
     * @param $id
     * @return bool
     */
    public function isNewClient($id)
    {
        $user = User::findOne($id);

        if ($user->role != User::ROLE_THERAPIST || Yii::$app->user->identity->role != User::ROLE_USER) {
            return false;
        }

        return true;
    }

    /**
     * @param $conversationId
     * @return string
     */
    public function actionConversations($conversationId)
    {
        return $this->renderPartial('_conversations', [
            'contacts' => $this->getContacts(),
            'conversationId' => $conversationId
        ]);
    }

    /**
     * @return string|Response
     */
    public function actionStart()
    {
        $newMessage = new ConversationMessage();
        $newMessage->scenario = 'contact';

        if ($newMessage->load(Yii::$app->request->post()) && $newMessage->validate()) {

            $conversation = $this->getConversation($newMessage->contact_id);

            $newMessage->user_id = Yii::$app->user->id;
            $newMessage->conversation_id = $conversation->id;
            $newMessage->save(false);

            if ($newMessage->client) {
                $newMessage->client->updateLastMessageDate();
            }

            return $this->redirect(['conversation', 'id' => $conversation->id]);
        }

        return $this->render('start', [
            'newMessage' => $newMessage,
            'contacts' => $this->getContacts()
        ]);
    }

    /**
     * @return array
     */
    public function actionNewConversations()
    {
        Yii::$app->response->format = 'json';

        if (Yii::$app->user->isGuest) {
            return [
                'result' => false
            ];
        }

        $query = Conversation::find()
            ->distinct()
            ->where('`sender_id` = :user_id OR `conversation`.`receiver_id` = :user_id', [':user_id' => Yii::$app->user->id])
            ->rightJoin(
                '{{%conversation_message}}',
                '`conversation_message`.`conversation_id` = `conversation`.`id` AND `conversation_message`.`user_id` != :user_id AND status = :status',
                [
                    ':user_id' => Yii::$app->user->id,
                    ':status' => ConversationMessage::STATUS_NEW
                ]
            );

        return [
            'result' => true,
            'count' => $query->count()
        ];
    }

    /**
     * @param null $id
     * @return Response
     */
    public function actionHideConversation($id = null)
    {
        if (Yii::$app->user->identity->role == User::ROLE_THERAPIST) {
            Conversation::updateAll(['hidden' => true], ['id' => (int)$id]);
        }

        return $this->redirect('/messages/index');
    }
}
