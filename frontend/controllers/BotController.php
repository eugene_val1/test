<?php

namespace frontend\controllers;

use common\components\bots\AbstractBot;
use common\components\bots\BotMessageHelper;
use common\components\bots\FacebookBotService;
use common\components\bots\TelegramBotService;
use common\models\UserBots;
use frontend\components\ModalHelper;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use Yii;
use yii\web\Response;
use yii\web\ServerErrorHttpException;

/**
 * Class BotController
 * @package frontend\controllers
 */
class BotController extends Controller
{
    /**
     * @var TelegramBotService
     */
    protected $telegram;

    /**
     * @var FacebookBotService
     */
    protected $facebook;

    /**
     * Init
     */
    public function init()
    {
        parent::init();

        $this->telegram = TelegramBotService::getInstance();
        $this->facebook = FacebookBotService::getInstance();
    }

    /**
     * @param \yii\base\Action $action
     * @return bool
     */
    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    /**
     * Telegram webhook
     *
     * @param string $h secret hash
     *
     * @throws ServerErrorHttpException
     */
    public function actionTelegram($h = null)
    {
        try {
            $input = json_decode(file_get_contents('php://input'), true);

            if (!$this->telegram->validateSecretHash($h)) {
                throw new ServerErrorHttpException('Invalid request');
            }

            $text = ArrayHelper::getValue($input, 'message.text');
            $chatId = ArrayHelper::getValue($input, 'message.chat.id');
            $name = ArrayHelper::getValue($input, 'message.chat.first_name');

            $isStart = strpos($text, '/start') === 0;

            if (empty($chatId)) {
                Yii::info('Empty telegram chatId', AbstractBot::LOG_CATEGORY);
                throw new \Exception('Empty chatId. ' . json_encode($input));
            }

            if ($isStart) {
                $payload = trim(str_replace('/start', '', $text));
                $userId = Yii::$app->cache->get($payload);
                if (empty($userId)) {
                    return;
                }

                $this->telegram->addUser($userId, $chatId);
                Yii::$app->cache->delete($payload);

                $helloMessage = Yii::t('bots', 'Hi {0}! Welcome on TreatField. You will receive all important updates here', [$name]);
                $this->telegram->sendMessage($chatId, $helloMessage);
                (new ModalHelper())->setMessage(Yii::t('bots', 'Bot subscribed successfully'));
            }

            $this->telegram->sendMessage($chatId, BotMessageHelper::getDefaultMessage());

        } catch (\Exception $e) {
            Yii::error($e->getMessage(), AbstractBot::LOG_CATEGORY);
        }
    }

    /**
     * Facebook webhook
     *
     * @param string $h secret hash
     *
     * @throws ServerErrorHttpException
     */
    public function actionFacebook($h = null)
    {
        //verify bot
        if ($hubChallenge = $this->validateFacebook()) {
            echo $hubChallenge;
            return;
        }

        $input = json_decode(file_get_contents('php://input'), true);

        if (!$this->facebook->validateSecretHash($h)) {
            Yii::error('Invalid facebook hash', AbstractBot::LOG_CATEGORY);
        }

        $helloMessage = Yii::t('bots', 'Hi! Welcome on TreatField. You will receive all important updates here');

        $message = ArrayHelper::getValue($input, 'entry.0.messaging.0', []);
        $ref = ArrayHelper::getValue($message, 'postback.referral.ref');
        $senderId = ArrayHelper::getValue($message, 'sender.id');

        if (empty($senderId)) {
            return;
        }

        //first connection
        if (!empty($ref)) {
            $userId = Yii::$app->cache->get($ref);
            if (empty($userId)) {
                return;
            }
            $this->facebook->addUser($userId, $senderId);
            Yii::$app->cache->delete($ref);

            $this->facebook->sendMessage($senderId, $helloMessage);
            (new ModalHelper())->setMessage(Yii::t('bots', 'Bot subscribed successfully'));

            return;
        }

        //any message here
        $this->facebook->sendMessage($senderId, BotMessageHelper::getDefaultMessage());
    }

    /**
     * @return string|null
     */
    protected function validateFacebook()
    {
        if (
            !empty($_REQUEST['hub_mode'])
            && !empty($_REQUEST['hub_challenge'])
            && !empty($_REQUEST['hub_verify_token'])
            && $_REQUEST['hub_mode'] === 'subscribe'
            && $this->facebook->validateSecretHash($_REQUEST['hub_verify_token'])
        ) {
            return $_REQUEST['hub_challenge'];;
        }
    }

    /**
     * Delete a bot by type
     *
     * @param string $type
     *
     * @return Response
     *
     * @throws \Exception
     */
    public function actionDelete($type)
    {
        if (!in_array($type, [TelegramBotService::TYPE, FacebookBotService::TYPE])) {
            return $this->redirect('/account');
        }

        $bot = UserBots::find()
            ->where([
                'type' => $type,
                'user_id' => Yii::$app->user->identity->getId()
            ])
            ->one();

        if (empty($type) || empty($bot)) {
            return $this->redirect('/account');
        }

        $bot->delete();
        Yii::$app->getSession()->setFlash('info', Yii::t('bots', 'Bot subscription deleted'));

        return $this->redirect(Yii::$app->request->referrer);
    }
}