<?php

namespace app\modules\api\controllers;

use app\modules\api\ApiModule;
use common\components\MailHelper;
use common\models\AuthProvider;
use common\models\Country;
use common\models\Profile;
use common\models\Timezone;
use frontend\components\auth\Facebook;
use frontend\components\auth\GoogleOAuth;
use Yii;
use yii\authclient\clients\Twitter;
use yii\authclient\OAuth1;
use yii\authclient\OAuth2;
use yii\authclient\OAuthToken;
use yii\web\Controller;
use \Firebase\JWT\JWT;
use common\models\User;

/**
 * Class UserController
 * @package app\modules\api\controllers
 */
class UserController extends Controller
{
    public $enableCsrfValidation = false;

    /**
     * @var array auth providers
     */
    static $providers = [
        'facebook' => Facebook::class,
        'twitter' => Twitter::class,
        'google' => GoogleOAuth::class,
    ];

    /**
     * Login
     * @return array
     */
    public function actionLogin()
    {
        $data = Yii::$app->request->bodyParams;
        $login = isset($data['login']) ? $data['login'] : '';
        $password = isset($data['password']) ? $data['password'] : '';

        if (empty($login) || empty($password)) {
            return ApiModule::errorResponse(406, 'Login or password mismatch');
        }

        $user = User::findByEmailOrUsername($login);

        if (!$user || !$user->validatePassword($password)) {
            return ApiModule::errorResponse(406, 'Login or password mismatch');
        }

        $token = $this->generateToken($user->id);

        return ApiModule::response(['token' => $token]);
    }

    /**
     * Get token by social auth token
     * @return array
     */
    public function actionAuthenticate()
    {
        $data = Yii::$app->request->bodyParams;

        $token = isset($data['token']) ? $data['token'] : '';
        $provider = isset($data['provider']) ? $data['provider'] : '';
        $email = isset($data['email']) ? $data['email'] : '';
        $register = isset($data['register']) ? (boolean)$data['register'] : false;

        if (empty($token) || !in_array($provider, array_keys(self::$providers))) {
            return ApiModule::errorResponse(400, 'Bad request');
        }

        /** @var OAuth2|OAuth1 $oauthClient */
        $oauthClient = new self::$providers[$provider]();
        $accessToken = new OAuthToken();
        $accessToken->setToken($token);
        $oauthClient->setAccessToken($accessToken);

        //fetch user attributes
        try {
            $attributes = $oauthClient->getUserAttributes();
            if (empty($attributes['id'])) {
                throw new \Exception();
            }
        } catch (\Exception $e) {
            return ApiModule::errorResponse(400, 'Invalid token');
        }

        $authProvider = AuthProvider::findOne([
            'name' => $provider,
            'provider_id' => $attributes['id'],
        ]);

        if (empty($authProvider)) {
            if (!$register) {
                return ApiModule::errorResponse(404, 'User not found');
            }

            //register user
            if (empty($attributes['email']) && empty($email)) {
                return ApiModule::errorResponse(400, 'Empty email');
            }
            $email = empty($attributes['email']) ? $email : $attributes['email'];

            $transaction = Yii::$app->db->beginTransaction();

            $user = new User();
            $country = Country::getCurrentByIP();
            $timezone = Timezone::getCurrentByIP();
            $user->attributes = [
                'username' => $provider . '_' . $attributes['id'],
                'email' => $email,
                'role' => User::ROLE_USER,
                'status' => User::STATUS_ACTIVE,
                'country_id' => $country ? $country->id : 0,
                'timezone' => $timezone ?: null,
            ];
            $user->generateAuthKey();
            $user->generateToken();
            if (!$user->save(false)) {
                $transaction->rollBack();
                return ApiModule::errorResponse(500, 'Internal error');
            }

            //save email to Mailchimp
            MailHelper::addToMailchimp($user->email);

            $profile = new Profile();
            $profile->attributes = [
                'user_id' => $user->id,
                'type' => Profile::TYPE,
                'name' => isset($attributes['name']) ? $attributes['name'] : 'TreatField User',
                'image' => isset($attributes['photo']) ? $attributes['photo'] : '',
            ];
            if (!$profile->save(false)) {
                $transaction->rollBack();
                return ApiModule::errorResponse(500, 'Internal error');
            }

            $authProvider = new AuthProvider();
            $authProvider->attributes = [
                'user_id' => $user->id,
                'name' => $provider,
                'provider_id' => $attributes['id'],
            ];
            if (!$authProvider->save()) {
                $transaction->rollBack();
                return ApiModule::errorResponse(500, 'Internal error');
            }

            //success
            $transaction->commit();
        }

        $providerUser = $authProvider->user;
        if (empty($providerUser)) {
            return ApiModule::errorResponse(404, 'User not found');
        }

        return [
            'status' => 200,
            'data' => [
                'token' => $this->generateToken($providerUser->id),
                //'user' => self::userInfo($user)
            ]
        ];
    }

    /**
     * Generate user access token
     * @param $userId
     * @return string
     */
    private function generateToken($userId)
    {
        $payload = [
            'data' => [
                'id' => $userId,
            ],
            'uid' => uniqid('api', true),
        ];

        return JWT::encode($payload, Yii::$app->params['apiSecret'], 'HS256');
    }

    /**
     * @return array
     */
    public function actionView()
    {
        $this->module->checkLogin();

        /** @var $user User */
        $user = $this->module->user;

        return [
            'status' => 200,
            'data' => self::userInfo($user)
        ];
    }

    /**
     * Create session
     * @return array
     */
    public function actionSessionLogin()
    {
        $this->module->checkLogin();

        /** @var $user User */
        $user = $this->module->user;
        Yii::$app->user->login($user);

        return [
            'status' => 200,
            'data' => self::userInfo($user),
        ];
    }

    /**
     * Destroy session
     * @return array
     */
    public function actionSessionLogout()
    {
        $this->module->checkLogin();
        Yii::$app->user->logout();

        return [
            'status' => 200,
        ];
    }

    /**
     * @param $user User
     * @return array
     */
    public static function userInfo($user)
    {
        return [
            'id' => $user->getId(),
            'username' => $user->username,
            'email' => $user->email,
            'name' => $user->getName(),
            'role' => $user->role === User::ROLE_THERAPIST ? 1 : 0,
        ];
    }
}