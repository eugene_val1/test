<?php

namespace app\modules\api\controllers;

use common\models\Appointment;
use yii\web\Controller;
use yii\filters\ContentNegotiator;
use yii\web\Response;
use yii\filters\AccessControl;
use yii\filters\auth\HttpBearerAuth;

/**
 * Class RoomController
 * @package app\modules\api\controllers
 */
class RoomController extends Controller
{
    public $enableCsrfValidation = false;

    /**
     * @return array
     */
    public function actionSettings()
    {
        return [
            'status' => '',
            'data' => [
                'username' => 'treatfield',
                'password' => 'tUi97Kmp',
                'uris' => [
                    'turn:treatfield.com:3478?transport=udp',
                    'turn:treatfield.com:3478?transport=tcp'
                ]
            ]
        ];
    }

    /**
     * @return string
     */
    public function actionList()
    {
        $this->module->checkLogin();

        $appointments = Appointment::find()
            ->with(['room', 'therapist', 'user'])
            ->where([
                'or',
                ['user_id' => $this->module->user->getId()],
                ['therapist_id' => $this->module->user->getId()],
            ])
            ->all();

        $list = [];

        foreach ($appointments as $appointment) {
            /** @var $appointment Appointment */
            $list[] = [
                'therapist' => $appointment->therapist ? $appointment->therapist->name : null,
                'user' => $appointment->user ? $appointment->user->name : null,
                'room_id' => $appointment->room_id,
                'room_alias' => $appointment->room ? $appointment->room->alias : null,
                'room_url' => \Yii::$app->params['baseUrl'] . '/appointment/video?id=' . $appointment->id ,
                'start' => $appointment->start,
                'end' => $appointment->end,
                'duration' => $appointment->duration,
                'complete_status' => $appointment->status,
                'payment_status' => $appointment->payment_status,
                'created' => $appointment->created
            ];
        }

        return [
            'status' => 200,
            'data' => $list
        ];
    }
}