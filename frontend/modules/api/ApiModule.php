<?php

namespace app\modules\api;

use common\models\User;
use yii\base\Module;
use yii\web\ForbiddenHttpException;
use yii\web\Response;
use Yii;

/**
 * Class Module
 * @package app\modules\api
 */
class ApiModule extends Module
{
    public $controllerNamespace = 'app\modules\api\controllers';
    /**
     * @var User
     */
    public $user;

    /**
     * Init
     */
    public function init()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $token = Yii::$app->request->get('token');
        if (empty($token)) {
            $token = Yii::$app->request->post('token');
        }
        if (empty($token)) {
            $token = Yii::$app->request->headers->get('token');
        }

        $this->user = User::findIdentityByAccessToken($token);

        parent::init();
    }

    /**
     * Before action
     * @param \yii\base\Action $action
     * @return bool
     */
    public function beforeAction($action)
    {
        return parent::beforeAction($action);
    }

    /**
     * Check login
     * @throws ForbiddenHttpException
     */
    public function checkLogin()
    {
        if (empty($this->user)) {
            throw new ForbiddenHttpException('User is not authorized');
        }
    }

    /**
     * Success response
     * @param $data
     * @return array
     */
    public static function response($data)
    {
        return [
            'status' => 200,
            'data' => $data
        ];
    }

    /**
     * Success response
     * @param $status
     * @param $message
     * @return array
     */
    public static function errorResponse($status, $message)
    {
        return [
            'status' => $status,
            'message' => $message
        ];
    }
}