<?php

use frontend\components\TooltipHelper;
use common\components\LanguageUrlManager;
use frontend\components\LanguageRequest;
use frontend\components\auth\Twitter;
use frontend\components\auth\GoogleOAuth;
use frontend\components\auth\Facebook;
use common\models\User;
use app\modules\api\ApiModule;

$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'modules' => [
        'api' => [
            'class' => ApiModule::class,
        ],
    ],
    'components' => [
        'user' => [
            'identityClass' => User::class,
            'loginUrl' => ['user/login'],
            'enableAutoLogin' => true,
        ],
        'authClientCollection' => [
            'class' => 'yii\authclient\Collection',
            'clients' => [
                'facebook' => [
                    'class' => Facebook::class,
                    'clientId' => '884705478238219',
                    'clientSecret' => '18be71ba8472e21ea4db45bba957dcca',
                    'attributeNames' => [
                        'id',
                        'name',
                        'email',
                    ],
                ],
                'google' => [
                    'class' => GoogleOAuth::class,
                    'clientId' => '688707626384-engiv6ss917hanrs87qrr1di2m7untei.apps.googleusercontent.com',
                    'clientSecret' => 'LI8i_brF_GfFgyKPb1J8gsNw',
                ],
                'twitter' => [
                    'class' => Twitter::class,
                    'consumerKey' => 'De8i0HuX3mthTG3hgEoSqnr3y',
                    'consumerSecret' => '5ZWS6B3E1ZVesY0dVnaeCDQgQsweW5TyGNSflvpGu7GeUxEDt8',
                ],
                /*'odnoklassniki' => [
                    'class' => 'frontend\components\auth\Odnoklassniki',
                    'clientId' => '1156043520',
                    'publicKey' => 'CBAENIOFEBABABABA',
                    'clientSecret' => '60BDE9FBD248613481A69D19',
                ],
                'vkontakte' => [
                    'class' => 'frontend\components\auth\VKontakte',
                    'clientId' => '5081524',
                    'clientSecret' => 'f3ShUtFtK3WlM8Dk850v',
                ],*/
            ],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'trace'],
                    'except' => [
                        'yii\web\HttpException:403',
                        'yii\web\HttpException:404',
                        'yii\web\HttpException:415',
                        'yii\base\InvalidRouteException:*',
                        'yii\web\ForbiddenHttpException',
                    ],
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'info'],
                    'categories' => ['payments'],
                    'logFile' => '@frontend/runtime/logs/payments.log',
                    'maxFileSize' => 1024 * 2,
                    'maxLogFiles' => 50,
                    'logVars' => [],
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['info'],
                    'categories' => ['geo'],
                    'logFile' => '@frontend/runtime/logs/geo.log',
                    'maxFileSize' => 1024 * 2,
                    'maxLogFiles' => 50,
                    'logVars' => [],
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['info', 'error'],
                    'categories' => ['bots'],
                    'logFile' => '@frontend/runtime/logs/bots.log',
                    'maxFileSize' => 1024 * 2,
                    'maxLogFiles' => 10,
                    'logVars' => [],
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['info', 'error'],
                    'categories' => ['room'],
                    'logFile' => '@frontend/runtime/logs/room.log',
                    'maxFileSize' => 1024 * 2,
                    'maxLogFiles' => 10,
                ],
            ],
        ],
        'reCaptcha' => [
            'name' => 'reCaptcha',
            'class' => 'himiklab\yii2\recaptcha\ReCaptcha',
            'siteKey' => '6LdZxloUAAAAALZ9NrT8TzzqatoqF3ubv1Kt2Ghg',
            'secret' => '6LdZxloUAAAAAOm4pN_mC3pHoL8X12LhnyyFBFCP',
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'request' => [
            'class' => LanguageRequest::class,
        ],
        'urlManager' => [
            'class' => LanguageUrlManager::class,
            'rules' => [
                '<controller:(account|field|therapists|article|feedback)>' => '<controller>/index',
                'page/<alias:[0-9a-zA-Z-]+>' => 'page/index',
                'room/id/<alias:[0-9a-zA-Z-]+>' => 'room/index',
                'category/<alias:[0-9a-zA-Z-]+>' => 'field/category',
                'field/<type:(posts|videos|treats)>' => 'field/index',
                'field/<alias:[0-9a-zA-Z-]+>' => 'field/view',
                'therapist/<alias:[0-9a-zA-Z-]+>' => 'therapists/view',
                'therapist/<alias:[0-9a-zA-Z-]+>/terms' => 'therapists/terms',
                'faq/category/<alias:[0-9a-zA-Z-]+>' => 'faq/category',
                'user/profile/<alias:[0-9a-zA-Z-_]+>' => 'user/profile',
                'sitemap.xml' => 'sitemap/index',
                'search' => 'site/search',
                '<type:[a-zA-Z-]+>-<page:[0-9]+>.xml' => 'sitemap/urlset',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
            ],
        ],
        'assetManager' => [
            'appendTimestamp' => true,
        ],
        'tooltip' => [
            'class' => TooltipHelper::class,
        ],
    ],
    'params' => $params,
];
