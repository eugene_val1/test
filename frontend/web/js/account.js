$(function () {
    $(".load-content").each(function (i) {
        var url = $(this).attr('data-url');
        if (url) {
            loadSection($(this), url);
        }
    });
    $('.scroll-nav .hash-scroll').click(function (e) {
        e.preventDefault();
        $(window).stop(true).scrollTo(this.hash, {duration: 1000, interrupt: true});
        window.location.hash = this.hash;
    });
    var hash = document.location.hash;
    if (hash && $('.user-side-nav').length) {
        $(window).stop(true).scrollTo(hash, {duration: 1000, interrupt: true});
    }
});

function loadSection(container, url) {
    $.ajax({
        url: url,
        success: function (response) {
            if (response.result) {
                container.find('.section-inner').html(response.html);
            }
            setTimeout(function () {
                if (url === "therapists/therapy" && isMobile) {
                    var payBtn = $(document).find('*[data-callback="pay"]').first();
                    if (payBtn) {
                        payBtn.parents('tr').first().addClass('expanded');
                        payBtn.parents('tr').first().css('width', '100%');
                    }
                }
            }, 1500);
        }
    });
}

$(document).on("click", ".account-section .section-inner:not(.sync) .btn-submit", function (ev) {
    ev.preventDefault();
    $('.button-addon').hide();
    var form = $(this).closest("form");
    sendAccountForm(form);
});

$(document).on("click", ".remove-client", function (ev) {
    ev.preventDefault();
    $.ajax({
        url: $(this).attr('href'),
        method: 'post',
        success: function (response) {
            if (response.result) {
                $('#modal').modal('hide');
                //$('#grid-new-requests').yiiGridView('applyFilter');
                updateAllPjaxContainers();
            }
        }
    });
});

$(document).on("keyup", ".input-price", function (ev) {
    var price = parseInt($(this).val()) || 0,
        container = $(this).closest('.price-wrapper'),
        isPercent = parseInt(container.attr('data-percent')),
        fee = (price !== 0) ? parseInt(container.attr('data-fee')) : 0,
        finalPrice = container.find('.final-price'),
        serviceFee = (isPercent) ? Math.ceil((price * fee) / 100) : fee;

    finalPrice.html(price + serviceFee);
});

$(document).on("click", ".remove-certificate", function (ev) {
    ev.preventDefault();
    $.ajax({
        url: $(this).attr('href'),
        method: 'post',
        success: function (response) {
            if (response.result) {
                $('#modal').modal('hide');
                var container = $("#certificates-container");
                var url = container.attr('data-url');
                if (url) {
                    loadSection(container, url);
                }
            }
        }
    });
});

$(document).on("click", ".remove-partner", function (ev) {
    ev.preventDefault();
    $.ajax({
        url: $(this).attr('href'),
        method: 'post',
        success: function (response) {
            if (response.result) {
                $('#modal').modal('hide');
                var container = $("#partner-container");
                var url = container.attr('data-url');
                if (url) {
                    loadSection(container, url);
                }
            }
        }
    });
});

$(document).on("click", ".inactive-client", function (ev) {
    ev.preventDefault();
    var reloadGrid = $(this).attr('data-reload-grid');

    $.ajax({
        url: $(this).attr('href'),
        method: 'post',
        success: function (response) {
            if (response.result) {
                $('#modal').modal('hide');
                if (reloadGrid) {
                    $('#inactive-pjax-clients').attr('data-reload-grid', reloadGrid + '-clients-grid');
                }
                //$('#inactive-clients-grid').yiiGridView('applyFilter');
                updateAllPjaxContainers();
            } else {
                $("#modal .modal-body").html('<div class="alert alert-danger">' + response.message + '</div>');
            }
        }
    });
});

$(document).on("click", ".pending-client", function (ev) {
    ev.preventDefault();
    $.ajax({
        url: $(this).attr('href'),
        method: 'post',
        success: function (response) {
            if (response.result) {
                $('#modal').modal('hide');
                $('#active-pjax-clients').attr('data-reload-grid', 'pending-clients-grid');
                //$('#active-clients-grid').yiiGridView('applyFilter');
                updateAllPjaxContainers();
            } else {
                $("#modal .modal-body").html('<div class="alert alert-danger">' + response.message + '</div>');
            }
        }
    });
});

$(document).on("click", ".reshedule-client", function (ev) {
    ev.preventDefault();
    var url = $(this).attr('data-target');
    $.ajax({
        url: $(this).attr('href'),
        method: 'post',
        success: function (response) {
            if (response.result) {
                openModal(url);
            }
        }
    });
});

$(document).on("click", "#account-status .btn-submit", function (ev) {
    ev.preventDefault();
    $.ajax({
        url: '/account/submit',
        method: 'post',
        success: function (response) {
            if (response.result) {
                $('#account-status').html(response.html);
            }
        }
    });
});

function sendAccountForm(form) {

    var container = form.closest(".section-inner");

    $.ajax({
        url: form.attr('action'),
        type: 'post',
        data: form.serialize(),
        success: function (response) {
            if (response.result) {
                container.html(response.html);
                var addon = container.find('.button-addon');
                if (response.errors) {
                    addon.html('<i class="fa fa-close"></i> ' + addon.attr('data-error-title')).addClass('error').show();
                } else {
                    addon.html('<i class="fa fa-check"></i> ' + addon.attr('data-success-title')).removeClass('error').show();
                }

            }
        }
    });
}

function afterAppointment(form) {
    var reloadGrid = form.attr('data-reload-grid');

    if (!reloadGrid) {
        $('#active-pjax-clients').attr('data-reload-grid', 'grid-new-requests');
    } else if (reloadGrid && reloadGrid !== 'active') {
        $('#active-pjax-clients').attr('data-reload-grid', reloadGrid + '-clients-grid');
    }

    //$('#active-clients-grid').yiiGridView('applyFilter');
    updateAllPjaxContainers();
}

function afterClientNameChange(form) {
    var reloadGrid = form.attr('data-reload-grid');

    if (!reloadGrid) {
        //$('#grid-new-requests').yiiGridView('applyFilter');
    } else {
        //$('#' + reloadGrid + '-clients-grid').yiiGridView('applyFilter');
    }
    updateAllPjaxContainers();
}

function afterCompleteTherapy(form) {
    updateTherapyGrid();
}

function afterUpdateCertificate(form) {
    var container = $("#certificates-container");
    var url = container.attr('data-url');
    if (url) {
        loadSection(container, url);
    }
}

function afterUpdatePartner(form) {
    var container = $("#partner-container");
    var url = container.attr('data-url');
    if (url) {
        loadSection(container, url);
    }
}

function updateTherapyGrid() {
    $('#pjax-therapy').attr('data-reload-grid', 'grid-finances');
    //$('#grid-therapy').yiiGridView('applyFilter');
    updateAllPjaxContainers();
}

function checkOverdue() {
    var overdue = $('.not-read[data-informed="0"]').first();
    if (overdue.length) {
        var url = overdue.attr('data-target');
        openModal(url);
    }
}

function afterOverdueInformed() {
    $('#modal').modal('hide');
    //$('#active-clients-grid').yiiGridView('applyFilter');
    updateAllPjaxContainers();

}

$(document).on("pjax:end", function (event) {
    var reloadGrid = $(event.target).attr('data-reload-grid');
    if (reloadGrid) {
        //$('#' + reloadGrid).yiiGridView('applyFilter');
        updateAllPjaxContainers();
    }
    $(event.target).removeAttr('data-reload-grid');

    if ($('.not-read[data-informed="0"]').length) {
        if ($('#modal').hasClass('in')) {
            $('#modal').addClass('check-overdue');
        } else {
            checkOverdue();
        }
    }

    $('[data-toggle="popover"]').popover({trigger: 'hover'});
});

$(document).on('hidden.bs.modal', '#modal', function (event) {
    if ($(event.target).hasClass('check-overdue')) {
        $(event.target).removeClass('check-overdue');
        checkOverdue();
    }
});

$(document).on("pjax:timeout", function (event) {
    event.preventDefault();
});

function setSelectedTherapist(item) {
    $('#selected-therapist').val(item.id);
}

$(document).on('click', ".user_appoint", function () {
    var lol = $(this).closest('tr')
    if (lol.hasClass('expanded')) {
        lol.animate({
            height: '65px'
        }).removeClass('expanded');
    } else {
        lol.animate({
            height: '100%'
        }).addClass('expanded');
    }
});

/**
 * Update all containers
 */
function updateAllPjaxContainers() {
    $('.account-page .load-content').each(function () {
        var pjaxContainerId = $(this).attr('data-container-id');
        var pjaxUrl = $(this).attr('data-url');
        if (pjaxContainerId && pjaxUrl) {
            $.pjax({
                url: pjaxUrl,
                container: '#' + pjaxContainerId,
                push: false,
                timeout: 2000,
                async: false
            });
        }
    });
}

/**
 * Appointment amount before payment: create payment url
 * @param target
 */
function updatePaymentUrl(target) {
    var urlElem = $('#appointment-count-form #payment-link');
    if (!urlElem.length) {
        return;
    }
    var link = urlElem.attr('href');
    var linkTemplate = urlElem.attr('data-href-template');
    var amount = $(target).val();
    urlElem.attr('href', linkTemplate + '&appointmentAmount=' + amount);
}

/**
 * Hide payment modal after setting counter
 */
$('body').on('click', '#appointment-count-form #payment-link', function () {
    $('#modal').modal('hide');
});

//Personal cabinet
$('.account-page').on('click', '.cabinet-table .h-head i', function(){
    $(this).closest('.client-row').find('.history-content').slideToggle();
    $(this).toggleClass('opened');
    if($(this).closest('.non-active-table')){
        $('.non-active-table .table-body').toggleClass('history-opened');
    }
});

$('.account-page').on('click', '.news-table .table-body .new-row', function(){
    $(this).toggleClass('opened');
    $(this).removeClass('not-read');
});

$('.account-page').on('click', '.cabinet-table .mob-head', function(e) {
    if (e.target != this) {
        var target = $(e.target);
        if (target.closest('a').length > 0) {
            return;
        }
    }
    $(this).parent().find('.row-content').slideToggle();
});
//Personal cabinet
