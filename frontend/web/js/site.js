var isMobile = (function (a) {
    return /(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4))
})(navigator.userAgent || navigator.vendor || window.opera);

var isIOS = ['iPad', 'iPhone', 'iPod'].indexOf(navigator.platform) >= 0;

//extensions START
$.fn.isInViewport = function () {
    var elementTop = $(this).offset().top;
    var elementBottom = elementTop + $(this).outerHeight();
    var viewportTop = $(window).scrollTop();
    var viewportBottom = viewportTop + $(window).height();
    return elementBottom > viewportTop && elementTop < viewportBottom;
};

$.fn.scrollEnd = function (callback, timeout) {
    $(this).scroll(function () {
        var $this = $(this);
        if ($this.data('scrollTimeout')) {
            clearTimeout($this.data('scrollTimeout'));
        }
        $this.data('scrollTimeout', setTimeout(callback, timeout));
    });
};
//extensions END

/**
 * Set cookies
 * @param name string
 * @param value string
 * @param options object | expires: seconds
 */
function setCookie(name, value, options) {
    options = options || {};

    var expires = options.expires;
    options.path = '/';

    if (typeof expires == "number" && expires) {
        var d = new Date();
        d.setTime(d.getTime() + expires * 1000);
        expires = options.expires = d;
    }
    if (expires && expires.toUTCString) {
        options.expires = expires.toUTCString();
    }

    value = encodeURIComponent(value);

    var updatedCookie = name + "=" + value;

    for (var propName in options) {
        updatedCookie += "; " + propName;
        var propValue = options[propName];
        if (propValue !== true) {
            updatedCookie += "=" + propValue;
        }
    }

    document.cookie = updatedCookie;
}

/**
 * возвращает cookie с именем name, если есть, если нет, то undefined
 * @param name
 * @returns {*}
 */
function getCookie(name) {
    var matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
}

$(document).on("click", "a[data-target='#modal']", function (ev) {
    ev.preventDefault();
    ev.stopPropagation();
    var target = $(this).attr("href");
    var reload = $(this).attr("data-reload-on-hide");
    openModal(target, reload);
});

/**
 * @param url string
 * @param reload bool
 */
function openModal(url, reload) {
    $.ajax({
        url: url,
        success: function (response) {
            var text = response.html ? response.html : '<h3>Error</h3>';
            renderModal(text, reload);
        }
    });
}

/**
 * @param content string
 * @param reload bool
 */
function renderModal(content, reload) {
    if ($('#modal').size() > 0 && $('#modal').is('.standart-modal.in')) {
        $("#modal .modal-body").html(content);
        if (reload == 1) {
            $("#modal").addClass('on-close-reload-modal');
        }
    } else {
        appendModal('standart-modal', reload);
        $("#modal .modal-body").html(content);
        $("#modal").modal("show");
    }
}

/**
 * Flash message modal
 */
function openFlashModal() {
    var message = getCookie('flash-message');
    if (message) {
        message = JSON.parse(message);
        message.text = message.text.replace(/\+/g, ' ');
        renderModal(message.text);
        setCookie('flash-message', null, {expires: -1})
    }
}

/**
 * Append modal
 * @param type
 * @param reload
 */
function appendModal(type, reload) {
    if ($('#modal').size() !== 0) {
        $('#modal').remove();
    }

    var modal = '<div class="modal fade ' + type + (reload == 1 ? 'on-close-reload-modal' : '') + '" id="modal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">' +
        '<div class="modal-dialog">' +
        '<div class="modal-content">' +
        '<div class="modal-header">' +
        '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
        '</div>' +
        '<div class="modal-body"></div>' +
        '</div>' +
        '</div>' +
        '</div>';

    $('body').append(modal);
}

/**
 * @returns {boolean}
 */
function checkHumanMath() {
    var x = Math.floor(Math.random() * 10) + 1;
    var y = Math.floor(Math.random() * 10) + 1;

    var exp = x + '+' + y;
    var check = prompt('Сколько будет: ' + exp + '? Нам просто нужно убедиться, что вы не робот', '');
    if (check != eval(exp)) {
        alert('Попробуйте еще');
        return false;
    }

    return true;
}

/**
 * Submit form handler
 */
$(document).on("click", "#modal .btn-submit", function (ev) {
    ev.preventDefault();

    if ($(this).hasClass('check-human-math') && !checkHumanMath()) {
        return;
    }

    var form = $(this).closest("form");
    $(this).prop("disabled", true);
    sendForm(form);
});

/**
 * Send form on submit
 * @param form
 */
function sendForm(form) {
    var reload = true;
    var callback = form.attr('data-callback');

    $.ajax({
        url: form.attr('action'),
        type: 'post',
        data: form.serialize(),
        success: function (response) {

            if (response.errors) {
                $("#modal .modal-body").html(response.html);
                reload = false;
                callback = false;
            } else if (response.message !== false) {
                $("#modal .modal-body .form-wrapper").html('<div class="alert alert-success">' + response.message + '</div>');
                reload = false;
            }

            if (typeof window[callback] === 'function') {
                window[callback](form, response);
            } else if (reload) {
                location.reload();
            }
        }
    });
}

$(document).ready(function () {
    compactMenu();
    if ($('.account-body .user-side-menu').length > 0 && $(window).width() > 980) {
        $('#nav-collapse').collapse('hide');
        closeLeftMenu();
        openUserMenu();
    }
    if ($('.side-menu.auto-open').length > 0 && $(window).width() >= 768) {
        $('#nav-collapse').collapse('hide');
        closeUserMenu();
        openLeftMenu();
    }
    $('#main-arrow-down').click(function (e) {
        var container = $('section.advantages');
        $("html, body").animate({
            scrollTop: container.offset().top
        }, 1000);
    });

    //MailChimp popup
    setTimeout(function () {
        if ($(window).width() > 768) {
            $('#mc_embed_signup.wait-for-popup').fadeIn(2000);
        } else {
            $('#mobile_mc_embed_signup.wait-for-popup').fadeIn(2000);
        }
    }, SUBSCRIBE_POPUP_DELAY);

    //Hide mailchimp popup
    $('.wait-for-popup .close').click(function (event) {
        event.preventDefault();
        event.stopPropagation();
        $('.wait-for-popup').hide();
        $.post("/site/subscribe-success", {}, function (data, status) {});
    });

    var userNav = $('body > aside.user-nav');
    var sideNav = $('body > aside.side-nav');
    var userNavPadding = userNav.innerHeight() - userNav.height();
    var cookiesInfoBlock = $('.main-nav.compact-nav #cookies-info:not(.accepted)');

    if (cookiesInfoBlock.is(':visible')) {
        //set new padding
        $('body > div').css('padding-top', cookiesInfoBlock.height() + 'px');
        sideNav.css('padding-top', cookiesInfoBlock.height() + 'px');
        userNav.css('padding-top', userNav.innerHeight() - userNav.height() + cookiesInfoBlock.height() + 'px');
    }

    $('#cookies-info .page-btn').click(function (e) {
        e.preventDefault();
        setCookie('accept-cookies', 1, {expires: 157680000});
        $('#cookies-info').slideToggle();

        //set previous padding
        $('body > div').css('padding-top', '0px');
        sideNav.css('padding-top', '0px');
        userNav.css('padding-top', userNavPadding);
    });
});

$(window).scroll(function () {
    compactMenu();
    if ($(window).scrollTop() > 1000) {
        $('.scroll-to-top').fadeIn();
    } else {
        $('.scroll-to-top').fadeOut();
    }
});

$('body').on({
    'touchmove': function (e) {
        compactMenu();
    }
});

/**
 * Compact menu
 */
function compactMenu() {
    if (!$('.main-nav').hasClass('full-nav') && !$('.navbar-collapse').hasClass('in') && !$('body').hasClass('right-menu')) {
        if ($(window).scrollTop() <= 0) {
            $('.main-nav').removeClass('compact-nav');
        } else {
            $('.main-nav').addClass('compact-nav');
        }
    }
}

$(document).on('show.bs.collapse', function (event) {
    if ($(event.target).hasClass('navbar-collapse')) {
        $('.main-nav').addClass('compact-nav');
    } else if ($(event.target).hasClass('appointment-list-collapse')) {
        var container = $(event.target).closest('.appointment-list-scroll');
        container.slimScroll({
            height: '130px',
            distance: '15px'
        });
        container.mouseover();
    } else if ($(event.target).hasClass('calendar-attachment-collapse')) {
        var id = $(event.target).attr('data-calendar');
        $(event.target).css({width: '100%'});
        var button = $(event.target).closest('.full-width-message').find('.calendar-button');
        button.html(button.attr('data-hide'));
        setTimeout(function () {
            $('#' + id).fullCalendar('render');
        }, 200);
    }
});

$(document).on('hidden.bs.collapse', function (event) {
    compactMenu();
});

$(document).on('hide.bs.collapse', function (event) {
    if ($(event.target).hasClass('appointment-list-collapse')) {
        var container = $(event.target).closest('.appointment-list-scroll');
        container.slimScroll({destroy: true});
        setTimeout(function () {
            container.removeAttr('style');
        }, 100);
    } else if ($(event.target).hasClass('calendar-attachment-collapse')) {
        var button = $(event.target).closest('.full-width-message').find('.calendar-button');
        button.html(button.attr('data-view'));
    }
});

$('.languages select').on('change', function () {
    window.location.replace($(this).val());
});

/**
 * Open left menu
 */
function openLeftMenu() {
    $('#nav-collapse').collapse('hide');
    closeUserMenu();

    var windowWidth = $(window).width();
    $('.side-nav').css({left: '0'});
    if ($(window).width() >= 768) {
        if ($(window).width() >= 768) {
            $('body').addClass('side-slide left-menu').css({marginLeft: '240px'});
        } else {
            $('body').addClass('side-push left-menu').css({left: '240px'});
        }
    } else {
        $('.side-nav').width(windowWidth);
    }
    //$('.menu-btn,#context-menu-btn').hide();
}

/**
 * Close left menu
 */
function closeLeftMenu() {
    if ($(window).width() >= 768) {
        $('.side-nav').css({left: '-250px'});
    } else {
        $('.side-nav').css({left: '-' + $('.side-nav').width() + 'px'});
    }
    if ($(window).width() >= 768) {
        $('body').css({marginLeft: '0'});
    } else {
        $('body').css({left: '0'});
    }
    setTimeout(function () {
        $('body').removeClass('left-menu');
        refreshBody();
    }, 0);
    //$('.menu-btn,#context-menu-btn').show();
}

$('.menu-btn, #context-menu-btn').click(function () {
    if (parseInt($('.side-nav').css('left')) < 0) {
        openLeftMenu();
        $('#nav-collapse').removeClass('in');
    } else {
        closeLeftMenu();
    }
});

$('.menu-btn,#context-menu-btn').click(function () {
    if (parseInt($('.side-nav').css('left')) < 0) {
        openLeftMenu();
        $('#nav-collapse').removeClass('in');
    } else {
        closeLeftMenu();
    }
});

$('.menu-close-btn').click(function () {
    closeLeftMenu();
});

//block window scrolling if any menu is opened
$(function () {
    var scrollTop = $(window).scrollTop();
    $(window).scroll(function () {
        // this bit stops the page from being scrolled
        if (
            isMobile &&
            (parseInt($('.side-nav').css('left')) === 0 || $('#nav-collapse').hasClass('in'))
        ) {
            $(window).scrollTop(scrollTop);
        }
        // update the new y value
        scrollTop = $(window).scrollTop();
    });
});

/**
 * Hide left menu on toggle main
 */
$('#nav #main-menu-btn').click(function () {
    $('.user-nav').css({right: '-250px'});
    $('#nav-collapse').collapse('hide');
    closeLeftMenu();
});

$(function () {
    $('#nav-collapse').on('show.bs.collapse', function () {
        var $fadeout = $('<div/>')
            .addClass('fadeout-overlay fade')
            .on('click', function () {
                $('#nav-collapse').collapse('hide');
            })
            .appendTo('body');

        setTimeout(function () {
            $fadeout.addClass('in');
        }, 0);

        $('#nav-collapse').one('hide.bs.collapse', function () {
            $fadeout.remove();
        });
    });
});

$(document).on("click", ".filter-item:not(.active-type) .filter-type", function (e) {
    if (!$(this).hasClass('open')) {
        $('.filter-type.open').toggleClass('open').next(".sub-filter").toggle('fast');
    }

    $(this).toggleClass('open').next(".sub-filter").toggle('fast');
});

$(document).on("click", ".launch-sidebar", function (e) {
    e.preventDefault();
    if ($('body').hasClass('right-menu')) {
        closeUserMenu();
    } else {
        $('#nav-collapse').collapse('hide');
        closeLeftMenu();
        openUserMenu();
    }
});

/**
 * Open user menu
 */
function openUserMenu() {
    $('.main-nav').addClass('compact-nav');
    $('.user-nav').css({right: '0'});
    if ($(window).width() > 980) {
        $('body').addClass('side-slide right-menu').css({marginRight: '250px'});
    } else {
        $('body').addClass('side-push right-menu').css({right: '250px'});
    }

    if (isMobile) {
        var $fadeout = $('<div/>')
            .addClass('fadeout-overlay fade')
            .on('click', function () {
                closeUserMenu();
                $('body').removeClass('lock-scroll');
            })
            .appendTo('body');

        $('body').addClass('lock-scroll');

        setTimeout(function () {
            $fadeout.addClass('in');
        }, 0);
    }
}

/**
 * Close user menu
 */
function closeUserMenu() {
    $('.fadeout-overlay').remove();

    $('.user-nav').css({right: '-250px'});
    if ($(window).width() > 980) {
        $('body').css({marginRight: '0'});
    } else {
        $('body').css({right: '0'});
        $('body').removeClass('lock-scroll');
    }
    setTimeout(function () {
        $('body').removeClass('right-menu');
        refreshBody();
        compactMenu();
    }, 300);
}

function refreshBody() {
    if (!$('body').hasClass('right-menu') && !$('body').hasClass('left-menu')) {
        $('body').removeClass('side-slide side-push');
    }
}

(function ($) {
    "use strict";

    $.fn.tree = function () {

        return this.each(function () {
            var btn = $(this).children("a").first();
            var menu = $(this).children(".treeview-menu").first();
            var isActive = $(this).hasClass('active');

            //initialize already active menus
            if (isActive) {
                menu.show();
                btn.children(".fa-angle-right").first().removeClass("fa-angle-right").addClass("fa-angle-down");
            }
            //Slide open or close the menu on link click
            btn.click(function (e) {
                e.preventDefault();
                if (isActive) {
                    //Slide up to close menu
                    menu.slideUp();
                    isActive = false;
                    btn.children(".fa-angle-down").first().removeClass("fa-angle-down").addClass("fa-angle-right");
                    btn.parent("li").removeClass("active");
                } else {
                    //Slide down to open menu
                    menu.slideDown();
                    isActive = true;
                    btn.children(".fa-angle-right").first().removeClass("fa-angle-right").addClass("fa-angle-down");
                    btn.parent("li").addClass("active");
                }
            });

        });

    };


}(jQuery));

function setThemeField(select) {
    var option = $("option:selected", select);
    if ($(option).hasClass('use-additional-field')) {
        var placeholder = $(option).attr('data-field-title');
        $('#additional-theme-field').attr('placeholder', placeholder).show();
    } else {
        $('#additional-theme-field').attr('placeholder', '').val('').hide();
    }
}

$(".sidebar .treeview").tree();

$(function () {
    if ($('body').hasClass('logged-in')) {
        updateConversationCounter();
        setInterval(updateConversationCounter, 10000);
    }
});

/**
 * Update conversation counter
 */
function updateConversationCounter() {
    $.ajax({
        url: '/messages/new-conversations',
        type: 'get',
        success: function (data) {
            if (data.result) {
                if (parseInt(data.count) > 0) {
                    updateCounter(data.count);
                } else {
                    $('.conversation-counter').html('');
                }
            }
        }
    });
}

/**
 * Update counter
 * @param count
 */
function updateCounter(count) {
    var topCounter = $('.account-block .conversation-counter');
    var sideCounter = $('.user-side-menu .conversation-counter');

    if (topCounter.length > 0) {
        var topHtml = '<span class="notify-label badge">' + count + '</span>';
        topCounter.html(topHtml);
    }

    if (sideCounter.length > 0) {
        var sideHtml = '<span class="notify-label">' + count + '</span>';
        sideCounter.html(sideHtml);
    }
}

$(document).on('shown.bs.modal', '#modal', function (event) {
    if ($('#calendar').length) {
        setTimeout(function () {
            $("#calendar").fullCalendar('render');
        }, 100);
    }
});

$(function () {
    if (document.location.hash) {
        var hash = document.location.hash.replace("/", "");
    }
    if (hash && $('.nav-tabs').length) {
        $('.nav-tabs a[href="' + hash + '"]').tab('show');
        $('html, body').animate({scrollTop: $(hash).offset().top - 120}, 800);
    }
    if (hash && $('.collapse-items').length) {
        $(".collapse").collapse('hide');
        $(hash).collapse('show');
        $('html, body').animate({scrollTop: $(hash).offset().top - 120}, 800);
    }
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        window.location.hash = e.target.hash.replace("#", "/");
    });
});

$(document).on("click", ".review-form .btn-submit", function (ev) {
    ev.preventDefault();
    var form = $(this).closest("form");
    $.ajax({
        url: form.attr('action'),
        type: 'post',
        data: form.serialize(),
        success: function (response) {
            if (response.errors) {
                $(".review-form").html(response.html);
            } else if (response.message !== false) {
                $(".review-form").html('<div class="alert alert-success">' + response.message + '</div>');
            }
        }
    });
});

$(document).on('show.bs.collapse', '.collapse', function (event) {
    $(this).closest('.collapse-item').find(".fa-angle-down").removeClass("fa-angle-down").addClass("fa-angle-up");
    if ($(event.target).hasClass('collapse-hash-change')) {
        window.location.hash = "/" + event.target.id;
    }
});

$(document).on('hide.bs.collapse', '.collapse', function (event) {
    $(this).closest('.collapse-item').find(".fa-angle-up").removeClass("fa-angle-up").addClass("fa-angle-down");
});

$(document).on("click", "a[data-target='#ajax']", function (ev) {
    ev.preventDefault();
    requestUrl($(this));
});

/**
 * Request Url
 * @param link
 */
function requestUrl(link) {
    var url = link.attr("href");
    var callback = link.attr('data-callback');

    $.ajax({
        url: url,
        type: 'post',
        success: function (response) {
            if (response.result) {
                if (response.message !== false) {
                    appendModal('standart-modal');
                    $("#modal .modal-body").html(response.message);
                    $("#modal").modal("show");
                }

                if (response.callback !== false && typeof window[callback] === 'function') {
                    window[callback](link, response);
                }
            }
        }
    });
}

function updateVoteCounter(link, data) {
    link.toggleClass('btn-primary');
    var counter = (data.count > 0) ? data.count : '';
    link.parent().find('.vote-counter').html(counter);
}

$(document).on("click", ".comment-form .btn-submit", function (ev) {
    ev.preventDefault();
    var form = $(this).closest("form");
    sendComment(form);
});

/**
 * Send comment
 * @param form
 */
function sendComment(form) {
    var reply_to = parseInt($(form).attr('data-reply'));
    var isReply = (reply_to > 0);
    var wrapper = $(form).closest(".comment-form");
    $(form).find('.btn-submit').attr('disabled', 'disabled');

    $.ajax({
        url: form.attr('action'),
        type: 'post',
        data: form.serialize(),
        success: function (response) {
            if (response.errors) {
                wrapper.html(response.html);
            } else {
                if (isReply) {
                    $('#answers-' + reply_to + '> .comment-list').prepend(response.comment);
                } else {
                    $('#comments').prepend(response.comment);
                    $('#comments .empty').remove();
                }
                clearCommentForm(form, isReply);
            }
        }
    });
}

function clearCommentForm(form, isReply) {
    if (isReply) {
        $(form).closest(".reply-form").html('');
    } else {
        $(form).find('.btn-submit').removeAttr('disabled');
        $(form).find('.has-error').removeClass('has-error');
        $(form).find('.comment-textarea').val('');
        $(form).find('.help-block').html('');
    }
}

$(document).on("click", ".reply-to", function (ev) {
    ev.preventDefault();
    var id = $(this).closest('.comment').attr('data-id');
    var url = $(this).attr("href");

    $.ajax({
        url: url,
        success: function (response) {
            if (response.result) {
                $('#answers-' + id + '> .reply-form').html(response.html);
            }
        }
    });
});

function loadReplies(link, data) {
    link.closest('.comment-list').html(data.html);
}

function deleteComment(link, data) {
    $('#modal').modal('hide');

    if (data.replies > 0) {
        $('#comment-' + data.id + '> .comment-inner').html(data.html);
    } else {
        $('#comment-' + data.id).remove();
    }
}

$(document).on("click", ".close-modal", function (ev) {
    ev.preventDefault();
    $('#modal').modal('hide');
});

$(document).on("click", "a[href='#top']", function (ev) {
    ev.preventDefault();
    $("html, body").animate({scrollTop: 0}, 1000);
});

function pay(link, response) {
    var wayforpay = new Wayforpay();
    var tid = response.tid;
    wayforpay.run(
        response.options,
        function (response) {
        },
        function (response) {
        },
        function (response) {
        }
    );

    window.addEventListener("message", function (event) {
        if (event.data === "WfpWidgetEventClose") {
            paymentCallback(tid);
        }
    });
}

function paymentCallback(tid) {
    window.location.href = "/account?tid=" + tid;
}

$(".main-search").keypress(function (e) {
    if (e.keyCode == 13) {
        var q = $(this).val();
        if (q !== '') {
            window.location.href = "/search?q=" + q;
        }
    }
});

function afterUpdateReview(form, response) {
    var id = form.attr('data-id');
    var editor = form.attr('data-editor');
    if (id) {
        if (editor == 'owner') {
            $("#review-item-" + id).remove();
        } else {
            $("#review-item-" + id + " .review-item-text").html(response.text);
        }
    }
}

function afterArticleRemove(link, response) {
    $("#article-item-" + response.id).remove();
    $('#modal').modal('hide');
}

if (isMobile) {
    $('.page-inner').css({padding: "10px"});
    $('.page-body .page-inner img').css({width: "100%", height: "auto"});
}

/**
 * Send activation link once more
 * @param target
 * @param userID
 */
function resendActivationLink(target, userID) {
    var btn = $(target);
    $.get("/user/resend-activation?userID=" + userID, function (data, status) {
        btn.hide();
    });
    return false;
}

function setVideoSection() {
    $('#video-wrap').height($(window).height());
}

var player;
var url;
var section;

$(document).on("click", "a[data-target='#video-modal']", function (ev) {
    ev.preventDefault();
    var url = $(this).data("video-url");
    appendModal('video-modal');
    var html = '<iframe id="video-frame" src="' + url + '" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
    $("#modal .modal-body").html(html);
    $("#modal").modal("show");
    section = $(this).closest('section');
});

$(document).ready(function () {
    if ($(window).width > 992) {
        var wideTileHeight;
        wideTileHeight = $('.text-item.wide').height();
        $('.treat-item.wide').css('height', wideTileHeight);
        $('.video-item:not(.wide), .text-item:not(.wide)').css('height', wideTileHeight);
    }
});

$(window).resize(function () {
    setVideoSection();
    if (!isMobile) {
        $('.parallax').each(function () {
            $(this).css({backgroundPosition: '50% ' + 0 + 'px'});
        });
    }

    if ($(window).width > 992) {
        wideTileHeight = $('.text-item.wide').height();
        $('.treat-item').css('height', wideTileHeight);
        $('.video-item:not(.wide), .text-item:not(.wide)').css('height', wideTileHeight);
    }
});

$(window).scroll(function () {
    if (!isMobile && $(window).width() > 1028) {
        $('.parallax').each(function () {
            var yPos = -(($(window).scrollTop() - $(this).offset().top) / $(this).data('speed'));
            $(this).css({backgroundPosition: '50% ' + yPos + 'px'});
        });
    }
});

$(document).on('hidden.bs.modal', '#modal', function (event) {
    if ($(event.target).hasClass('video-modal')) {
        $("#modal .modal-body").html('');
        number = parseInt(section.data('number')) + 2;
        var target = $('section[data-number=' + number + ']');

        if (target && !isNaN(number) && section.data('number') != 2) {
            $('html,body').animate({
                scrollTop: target.offset().top - 50
            }, 1000);
        }
    }
    if ($(event.target).hasClass('on-close-reload-modal')) {
        window.location.reload();
    }
});

$(document).ready(function () {
    openFlashModal();
});

//set timezone from user browser
if (Intl !== 'undefined') {
    setCookie('timezone', Intl.DateTimeFormat().resolvedOptions().timeZone);
}