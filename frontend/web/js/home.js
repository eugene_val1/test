$(document).ready(function () {

    //remove landing preloader
    setTimeout(function () {
        $('.landing-loader').remove();
    }, 500);

    compactMenu();
    homeVideoHandler();

    var manualPlayVideos = $('video.manual-autoplay');
    videoManualAutoplay(manualPlayVideos);

    if (!isMobile) {
        var videoWrap = $('#video-wrap');

        var file = videoWrap.data("file");
        var video = '<video id="video" preload="metadata" autoplay loop>' +
            '<source src="' + file + '" type="video/mp4">' +
            '</video>';

        videoWrap.prepend(video);

        $('#video').backgroundVideo({
            parallaxOptions: {
                effect: 1.9
            }
        });
    }
});

/**
 * Play video
 * @param video
 */
function playVideo(video) {
    if (video.paused) {
        video.play();
    } else {
        video.pause();
    }
}

/**
 * Reviews read more
 */
$('#reviews .read-more').click(function () {

    var readMore = $(this);
    var closedText = 'читать весь';
    var openedText = 'скрыть';

    readMore.parent()
        .find('.full-text')
        .slideToggle(200, function () {
            if (readMore.hasClass('opened')) {
                readMore.text(openedText);
                $('#reviews').carousel('pause');
            } else {
                readMore.text(closedText);
                $('#reviews').carousel({
                    interval: 5000,
                    cycle: true
                });
            }
        })
        .css('display', 'inline');

    readMore.toggleClass('opened');
});

/**
 * Reviews carousel
 */
$(document).ready(function () {
    $("#reviews").swiperight(function () {
        $(this).carousel('prev');
    });
    $("#reviews").swipeleft(function () {
        $(this).carousel('next');
    });
});

/**
 * Home Video
 */
function homeVideoHandler() {
	var muteBtn = document.querySelector('.mute-btn');
	var homeVideo = document.getElementById("home-video");
    var isSafari = !!navigator.userAgent.match(/Version\/[\d\.]+.*Safari/);

	if(isSafari) {
		if (homeVideo)  {
			var listener = function () {
				playVideo(homeVideo);
			}

			if($(window).width() > 980){
				console.log($(window).width())
				homeVideo.addEventListener("click", listener);
			}
			else{
				homeVideo.setAttribute('controls', '');
				muteBtn.style.display = 'none';
			}
			$(window).on('resize', function(){
				if($(window).width() > 980){
					homeVideo.addEventListener("click", listener);
					homeVideo.removeAttribute('controls');
					muteBtn.style.display = 'block';
				}
				else{
					homeVideo.removeEventListener('click', listener);
					homeVideo.setAttribute('controls', '');
					muteBtn.style.display = 'none';
				}
			});

		}
	}
	else{
		if (homeVideo) {
			homeVideo.addEventListener("click", function () {
				playVideo(homeVideo);
			});
		}
    }


    var mute = document.querySelector('.mute-btn');
    if (mute) {
        mute.addEventListener('click', function () {
            if (!homeVideo.muted) {
                homeVideo.muted = true;
                this.classList.add('muted');
            } else {
                homeVideo.muted = false;
                this.classList.remove('muted');
            }
        });
    }
}

/**
 * Add handlers for videos that should plays on viewport and click
 */
function videoManualAutoplay(videos) {

    videos.on('ended', function () {
        var video = $(this);
        var playedTimes = video.attr('data-attr-played');
        var maxPlayedTimes = video.attr('data-attr-max-played');

        if (++playedTimes >= maxPlayedTimes) {
            video.addClass('played');
            return;
        }

        video.attr('data-attr-played', playedTimes);
        video.get(0).play();
    });

    //set handlers for playing videos being in viewport
    $(window).on('resize scroll', function () {
        videos.each(function () {
            var video = $(this);
            if (video.isInViewport() && video.get(0).paused && !video.hasClass('played')) {
                video.get(0).play();
            }
        })
    });

    //set click handlers
    videos.click(function () {
        playVideo($(this).get(0));
    });

    //set mouseover handlers
    videos.mouseover(function () {
        playVideo($(this).get(0));
    });
}

/**
 * business cards carousel - set 4 items per view
 */
$('#carousel-business-generic .owl-carousel').owlCarousel({
    loop: false,
    margin: 10,
    nav: true,
    dots: false,
    responsive: {
        0: {
            items: 1
        },
        800: {
            items: 2
        },
        1100: {
            items: 3
        },
        1200: {
            items: 4
        }
    }
});
