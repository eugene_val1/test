function cropImage(result) {
    appendModal("standart-modal");
    $("#modal .modal-body").html(result.html);
    $("#modal").modal("show");
}

$(document).on('shown.bs.modal', '#modal', function (event) {
    if ($('#need-crop-image').length) {
        var img = $('#need-crop-image');
        var ratio = img.attr('data-ratio');
        var iw = img.width();
        var ih = img.height();
        var bw, bh;

        if (iw > ih) {
            bw = $('.crop-article-image').width();
            bh = (bw * ih) / iw;
        } else {
            bh = $(window).height() * 0.5;
            bw = (bh * iw) / ih;
        }

        img.Jcrop({
            aspectRatio: ratio,
            boxWidth: bw,
            boxHeight: bh,
            onSelect: updateCoords,
            onChange: updateCoords,
            setSelect: getCropSelected(iw, ih, ratio)
        });
    }
});

function getCropSelected(width, height, ratio)
{
    var selectedWidth, selectedHeight;

    if (width > height) {
        selectedHeight = height;
        selectedWidth = Math.round(height * ratio);
    } else {
        selectedWidth = width;
        selectedHeight = Math.round(width / ratio);
    }

    return [
        width / 2 - selectedWidth / 2,
        height / 2 - selectedHeight / 2,
        width / 2 + selectedWidth / 2,
        height / 2 + selectedHeight / 2
    ];
}

function updateCoords(c)
{
    $('#crop-x').val(c.x);
    $('#crop-y').val(c.y);
    $('#crop-w').val(c.w);
    $('#crop-h').val(c.h);
}

function afterCrop(form, response){
    if(response.status){
        $("#image-error").html("").hide();
        $("#article-cover-image-input").val(response.cover_image);
        $("#article-image-input").val(response.image);
        $("#article-image").attr("src", response.url);
        $('#modal').modal('hide');
    } else {
        $("#crop-image-error").html(response.error).show();
        $("#modal .btn-submit").prop("disabled",false);
    }    
}