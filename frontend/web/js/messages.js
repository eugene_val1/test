var updateInProgress = false;

$(document).ready(function () {
    $('#conversations').css('max-height', ($('.nav-inner').height() - $('#nav').height()) + 'px');
});

$(function () {
    if ($('#conversation').length) {
        $('body').addClass('message-scroll');
        setScroll();
        setInterval(updateMessagesList, 5000);
    }
    if ($('#conversations').length) {
        setInterval(updateConversations, 5000);
    }
});

$(window).scroll(function () {
    if ($('#conversation').length) {
        var container = $('body');
        var scrollTop = container.scrollTop();
        var position = scrollToPosition() - 50;

        if (scrollTop < position) {
            container.removeClass('message-scroll');
        } else {
            container.addClass('message-scroll');
        }
    }
});

function scrollToPosition() {
    var documentHeight = $(document).height();
    var windowHeight = $(window).height();
    var messagesHeight = $(".messages-list").outerHeight();
    var footerHeight = $("footer").outerHeight();

    if (messagesHeight > windowHeight) {
        return documentHeight - windowHeight - footerHeight - 100;
    } else {
        return 0;
    }
}

function setScroll() {
    if ($('body').hasClass('message-scroll')) {
        var scroll = scrollToPosition();
        if (scroll > 0) {
            $("html, body").stop().animate({scrollTop: scroll}, '500', 'swing');
        }
    }
}

function sendMessage(form) {
    $('#newMessage button').attr('disabled', 'disabled');
    if ($(form).find('.has-error').length) {
        $('#newMessage button').removeAttr('disabled');
        return false;
    }
    $.ajax({
        url: form.attr('action'),
        type: 'post',
        data: form.serialize(),
        success: function (data) {
            $('#newMessage button').removeAttr('disabled');
            updateMessagesList();
            $('#message-text').val('');
        }
    });
}

$('#message-text').keypress(function (event) {
    if (isMobile) {
        return;
    }
    if (event.keyCode == 13 && event.shiftKey) {
        event.stopPropagation();
    } else if (event.keyCode == 13) {
        $('#newMessage').submit();
        return false;
    }
});

$('body').on('beforeSubmit', 'form#newMessage', function () {
    sendMessage($(this));
    return false;
});

$('body').on('click', '.load-previous button', function () {
    previousMessages();
});

function previousMessages() {
    var button = $('.load-previous button');
    var container = $('#conversation');
    var conversationId = container.data("conversation");
    var startId = $("#conversation .message-item").first().data("id");

    button.attr('disabled', 'disabled');

    $.ajax({
        url: '/messages/previous',
        type: 'get',
        async: false,
        data: {conversationId: conversationId, startId: startId},
        success: function (data) {
            if (data.result) {
                container.prepend(data.html);
                button.removeAttr('disabled');

                if (parseInt(data.offset) < 1) {
                    button.hide();
                }
            }
        }
    });
}

function updateMessagesList() {
    var container = $('#conversation');
    var conversationId = container.data("conversation");
    var lastId = $("#conversation .message-item").last().data("id");
    var lastUserId = $("#conversation .message-item").last().data("user-id");

    if (!lastId) {
        lastId = 0;
    }

    if (!updateInProgress) {
        updateInProgress = true;
        $.ajax({
            url: '/messages/list',
            type: 'get',
            async: false,
            data: {conversationId: conversationId, lastId: lastId, lastUserId: lastUserId},
            success: function (data) {
                if (data.length) {
                    container.append(data);
                    setScroll();
                }
                updateInProgress = false;
            }
        });
    }
}

function updateConversations() {
    var container = $('#conversation');
    var conversationId = 0;

    if (container.length) {
        conversationId = container.data("conversation");
    }

    $.ajax({
        url: '/messages/conversations',
        type: 'get',
        data: {conversationId: conversationId},
        success: function (data) {
            if (data.length) {
                $('#conversations').html(data);
            }
        }
    });
}

function setSelectedUser(item) {
    $('#selected-user').val(item.id);
}