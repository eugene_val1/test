$(document).ready(function () {

    var reset = true;

    var anno = new Anno([
        {
            target: isMobile ? '.mobile-fixed-btn.hide-desktop.orange' : '.get-therapist > .page-btn.orange',
            position: 'top',
            content: tooltipLabels.step3.appointment,
            buttons: [
                {
                    text: tooltipLabels.cancel,
                    className: 'anno-btn-low-importance',
                    click: function(anno, evt) {
                        window.location.replace('/user/stop-tooltips');
                    }
                },
                {
                    'text': tooltipLabels.next
                }
            ],
            onShow: function (anno, $target, $annoElem) {
                $target.css('z-index', 1001);
                $('.anno .anno-arrow').css('left', '10%');
                $('.anno').css('left', '0px');
                if (isMobile) {
                    $('.anno').css('top', ($(window).height()-280) +'px');
                }
            }
        },
        {
            target: '.main-nav.compact-nav .account-block',
            position: {
                'right': '0px',
                'top': '75px'
            },
            content: tooltipLabels.step3.profile,
            buttons: [
                {
                    text: tooltipLabels.cancel,
                    className: 'anno-btn-low-importance',
                    click: function(anno, evt) {
                        window.location.replace('/user/stop-tooltips');
                    }
                },
                {
                    text: tooltipLabels.next,
                    click: function (anno, evt) {
                        if ($('.account-block').length) {
                            reset = false;
                            window.location.reload();
                        }
                        anno.hide();
                        $('.login-button a').trigger('click');
                    }
                }
            ],
            onShow: function (anno, $target, $annoElem) {
                $('.anno .anno-arrow').css('left', '80%');
            }
        }
    ]);

    setTimeout(function () {
        anno.show();
    }, 200);

    window.onbeforeunload = function(e) {
        if (reset) {
            $.ajax({
                type: 'POST',
                url: '/user/stop-tooltips',
                async: false
            });
        }
    };

});