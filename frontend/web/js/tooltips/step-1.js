$(document).ready(function () {
    if (!$('.nav.navbar-nav.navbar-left li:first').length) {
        return;
    }

    if (isMobile) {
        $('#main-menu-btn').trigger('click');
    }

    var reset = true;

    setTimeout(function () {
        var anno = new Anno({
            target: '.nav.navbar-nav.navbar-left li:first',
            position: 'bottom',
            content: tooltipLabels.step1.menu,
            autoFocusLastButton: false,
            buttons: [
                {
                    text: tooltipLabels.cancel,
                    className: 'anno-btn-low-importance',
                    click: function(anno, evt) {
                        window.location.replace('/user/stop-tooltips');
                    }
                },
                {
                    text: tooltipLabels.next,
                    click: function (anno, evt) {
                        anno.hide();
                        reset = false;
                        window.location.replace('/therapists');
                    }
                }
            ],
            onShow: function (anno, $target, $annoElem) {
                $('.anno').css('left', 'auto');
                $target.find('a').css('color', '#56565a');
                $target.find('a').css('font-weight', '900');
            }
        });

        anno.show();
    }, 1000);

    window.onbeforeunload = function(e) {
        if (reset) {
            $.ajax({
                type: 'POST',
                url: '/user/stop-tooltips',
                async: false
            });
        }
    };
});