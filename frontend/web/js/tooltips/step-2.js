$(document).ready(function () {

    var reset = true;

    window.onbeforeunload = function(e) {
        if (reset) {
            $.ajax({
                type: 'POST',
                url: '/user/stop-tooltips',
                async: false
            });
        }
    };

    if (!$('#therapists-container .person-wrapper:first .content').length) {
        return;
    }

    var anno = new Anno({
        target: '#therapists-container .person-wrapper:first .content',
        position: {
            'right': '-30px'
        },
        content: tooltipLabels.step2.therapist,
        autoFocusLastButton: false,
        buttons: [
            {
                text: tooltipLabels.cancel,
                className: 'anno-btn-low-importance',
                click: function(anno, evt) {
                    window.location.replace('/user/stop-tooltips');
                }
            },
            {
                text: tooltipLabels.next,
                click: function (anno, evt) {
                    anno.hide();
                    var href = $('#therapists-container .person-wrapper:first .content > a').attr('href');
                    reset = false;
                    window.location.replace(href);
                }
            }
        ],
        onShow: function (anno, $target, $annoElem) {
            $target.css('z-index', 1001);
        }
    });

    setTimeout(function () {
        anno.show();
    }, 200);

});