$(document).ready(function () {

    var reset = true;

    var wait = setInterval(function () {
        if ($('#section-therapy .section-inner').length) {
            clearInterval(wait);
            setTimeout(function () {
                var target = '#section-therapy .section-inner';
                var anno = new Anno([
                    {
                        target: target,
                        position: 'bottom',
                        content: tooltipLabels.step5.therapist,
                        buttons: [
                            {
                                text: tooltipLabels.cancel,
                                className: 'anno-btn-low-importance',
                                click: function(anno, evt) {
                                    window.location.replace('/user/stop-tooltips');
                                }
                            },
                            {
                                'text': tooltipLabels.next
                            }
                        ],
                        onShow: function (anno, $target, $annoElem) {
                            $target.css('z-index', 1001);
                            if(!isMobile){
                                $target.css('left', '15px')
                            }
	                        window.location.replace('#section-therapy');
                        }
                    },
                    {
                        target: '.payment > a',
                        position: isMobile ? 'left' : 'bottom',
                        content: tooltipLabels.step5.time,
                        buttons: [
                            {
                                text: tooltipLabels.cancel,
                                className: 'anno-btn-low-importance',
                                click: function(anno, evt) {
                                    window.location.replace('/user/stop-tooltips');
                                }
                            },
                            {
                                'text': tooltipLabels.next
                            }
                        ],
                        onShow: function (anno, $target, $annoElem) {
                            $target.css('z-index', 1001);
                            if (isMobile) {
                                var tooltip = $('.anno');
                                tooltip.css('width', '250px');
                                tooltip.css('left', '-25px');
                                tooltip.css('top', (parseInt(tooltip.css('top'), 10) - 20) + 'px');
                            }
                        }
                    },
                    {
                        target: '.room > a',
                        position: isMobile ? 'left' : 'bottom',
                        content: tooltipLabels.step5.room,
                        buttons: [
                            {
                                text: tooltipLabels.finish,
                                click: function (anno, evt) {
                                    anno.hide();
                                    window.location.replace('/user/stop-tooltips?finished=1');
                                }
                            }
                        ],
                        onShow: function (anno, $target, $annoElem) {
                            $target.css('z-index', 1001);
                        }
                    }
                ]);

                anno.show();

            }, isMobile ? 1500 : 1000);
        }
    }, 500);

    window.onbeforeunload = function(e) {
        if (reset) {
            $.ajax({
                type: 'POST',
                url: '/user/stop-tooltips',
                async: false
            });
        }
    };

});