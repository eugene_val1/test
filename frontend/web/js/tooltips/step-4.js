$(document).ready(function () {

    var reset = true;

    var anno = new Anno([
        {
            target: '.user-side-nav li:nth-child(1)',
            position: 'bottom',
            content: tooltipLabels.step4.therapy,
            buttons: [
                {
                    text: tooltipLabels.cancel,
                    className: 'anno-btn-low-importance',
                    click: function(anno, evt) {
                        window.location.replace('/user/stop-tooltips');
                    }
                },
                {
                    text: tooltipLabels.next,
                    click: function (anno, evt) {
                        reset = false;
                        window.location.replace('/account#section-therapy');
                    }
                }
            ],
            onShow: function (anno, $target, $annoElem) {
                $('.anno').css('width', '250px');
                $('.user-nav.user-side-menu').css('z-index', 1001);
                var href = $('.user-side-nav li:nth-child(1)');
                href.addClass('hover');
            }
        }
    ]);

    openUserMenu();

    setTimeout(function () {
        anno.show();
    }, 200);

    window.onbeforeunload = function(e) {
        if (reset) {
            $.ajax({
                type: 'POST',
                url: '/user/stop-tooltips',
                async: false
            });
        }
    };

});