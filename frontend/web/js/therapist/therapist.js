$(window).on('resize scroll', function () {
    var requestButton = $('.therapist-info .get-therapist .small-page-btn.request-btn:not(.mobile-fixed-btn)');
    var requestMobileButton = $('.therapist-info .get-therapist .mobile-fixed-btn');
    if (requestButton.isInViewport()) {
        requestMobileButton.addClass('hidden');
    } else {
        requestMobileButton.removeClass('hidden');
    }
});