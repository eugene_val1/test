var room;
var myId;
var myRole;
var videoEnabled = true;
var audioEnabled = true;
var viewSpeaker = false;
var timer;
var selfStream;

/**
 * Log
 * @param message
 */
function log(message) {
    console.log(message);
    $.post('/room/log', {
        message: message,
        userId: myId,
        role: myRole
    });
}

/**
 * Init room
 * @param url
 * @param id
 * @param username
 * @param role
 */
function init(url, id, username, role) {
    myRole = role;

    var connectSuccess = function (id) {
        myId = id;
    };
    var connectFailure = function (errorCode, errorText) {
        errorCode = errorCode || "EMPTY CODE";
        log(errorCode + ': ' + errorText + ' - on ' + url);
        easyrtc.showError("LOST-CONNECTION", "Looks like something went wrong");
    };

    if (typeof easyrtc === 'undefined') {
        log('easyrtc could not be initialized'  + ' - on ' + url);
        easyrtc.showError("LOST-CONNECTION", "Looks like something went wrong");
        return;
    }

    easyrtc.setSocketUrl(url);
    easyrtc.dontAddCloseButtons(true);
    easyrtc.setUsername(username);
    easyrtc.setRoomEntryListener(entryListener);
    easyrtc.setRoomOccupantListener(roomListener);
    easyrtc.setPeerListener(peerListener);

    easyrtc.initMediaSource(
        function () {
            var selfVideo = document.getElementById("self");
            selfStream = easyrtc.getLocalStream();
            easyrtc.setVideoObjectSrc(selfVideo, selfStream);
            easyrtc.connect("room-" + id, connectSuccess, connectFailure);

            var speech = hark(selfStream);
            speech.on('speaking', function () {
                setSpeaking();
            });
        },
        connectFailure
    );

    easyrtc.setStreamAcceptor(function (callerEasyrtcid, stream) {
        if (easyrtc.getConnectionCount() > 0) {
            $('.waiting-alert').hide();
        }

        addNewStream(callerEasyrtcid, stream);
    });

    easyrtc.setOnStreamClosed(function (callerEasyrtcid) {
        var connectionCount = easyrtc.getConnectionCount();

        if (connectionCount === 0) {
            $('.waiting-alert').show();
            pauseSession();
        }

        removeStream(callerEasyrtcid);
    });

    easyrtc.setDisconnectListener(function () {
        easyrtc.showError("LOST-CONNECTION", "Lost connection to signaling server");
        log("Lost connection to signaling server"  + ' - on ' + url);
    });

    setStreamsWrapper();
}

/**
 *
 * @param id
 * @returns {*|jQuery}
 */
function getStreamContainer(id) {
    return $('#user-' + id).closest('.user-stream');
}

/**
 * Wrapper
 */
function setStreamsWrapper() {
    var padding = 90;
    if ($('.room-page-inner .page-container').hasClass('streams-fullscreen')) {
        padding = 0;
    }
    var height = $(window).height() - padding;
    var width = '100%';
    var userStreams = $('.streams .user-stream');

    $('.streams').css({width: width, height: height});

    userStreams.each(function (index) {
        var video = $(this).find('video');
        if (!$(this).hasClass('hero-stream')) {
            video.css({height: $(this).width() * 0.8});
        } else {
            video.css({height: height});
        }
    });
}

/**
 * Auto resizing for streams
 */
setInterval(function () {
    setStreamsWrapper();
}, 800);

/**
 * Entry listener
 * @param entry
 * @param roomName
 */
function entryListener(entry, roomName) {
    if (!entry) {
        return;
    }

    room = roomName;
    easyrtc.setRoomApiField(room, 'videoEnabled', videoEnabled);
    easyrtc.setRoomApiField(room, 'role', myRole);
    if (myRole === 'client') {
        easyrtc.sendPeerMessage({targetRoom: room}, 'role', {role: myRole});
    }
}

/**
 * Room listener
 * @param roomName
 * @param users
 */
function roomListener(roomName, users) {
    if (myRole === 'therapist') {
        easyrtc.setRoomOccupantListener(roleListener);
    } else {
        easyrtc.setRoomOccupantListener(null);
    }

    for (var id in users) {
        if (!users[id].apiField) {
            continue;
        }
        var userVideoEnabled = users[id].apiField.videoEnabled;
        var container = getStreamContainer(id);
        if (userVideoEnabled.fieldValue) {
            container.removeClass('muted-video');
        } else {
            container.addClass('muted-video');
        }

        var userRole = users[id].apiField.role;
        if (userRole.fieldValue === 'client' && myRole === 'therapist') {
            if ($('.timer').data('state') !== 'running') {
                startSession();
            }
        }

        easyrtc.call(id,
            function (otherCaller, mediaType) {
                console.log("Successful call to" + easyrtc.idToName(id));
            },
            function (errorCode, errorMessage) {
                log("Failure to connect. Code: " + (errorCode || "EMPTY CODE") + ", " + errorMessage);
            }
        );
    }
}

/**
 * Role listener
 * @param roomName
 * @param users
 */
function roleListener(roomName, users) {
    for (var id in users) {
        var userRole = users[id].apiField.role;
        if (userRole.fieldValue === 'client') {
            if ($('.timer').data('state') !== 'running') {
                startSession();
            }
            return;
        }
    }
    pauseSession();
}

/**
 * Peer listener
 * @param id
 * @param messageType
 * @param data
 */
function peerListener(id, messageType, data) {
    if (messageType === 'videoEnabled') {
        var container = getStreamContainer(id);
        if (data.enable) {
            container.removeClass('muted-video');
        } else {
            container.addClass('muted-video');
        }
    } else if (messageType === 'lockStatus') {
        toggleLockButton(data.locked);
    } else if (messageType === 'chat') {
        insertChatMessage(id, data.message);
    } else if (messageType === 'speaking' && viewSpeaker) {
        var container = getStreamContainer(id);
        if (container.length && !container.hasClass('.hero-stream')) {
            viewStream(container);
        }
    } else if (messageType === 'role' && data.role === 'client' && myRole === 'therapist') {
        startSession();
    }
}

/**
 * Start session
 */
function startSession() {
    var timerDiv = $('.timer');
    var seconds = timerDiv.attr('data-seconds') || 0;

    if (timerDiv.data('state') === 'paused') {
        timerDiv.timer('resume');
    } else {
        timerDiv.timer({
            seconds: seconds,
            format: '%H:%M:%S'
        });
    }

    timer = setTimeout(updateDuration, 60000);
}

/**
 * Update duration
 */
function updateDuration() {
    var timerDiv = $('.timer');
    var seconds = timerDiv.data('seconds');
    var url = timerDiv.attr('data-update');

    $.ajax({
        url: url,
        type: 'post',
        data: {duration: seconds},
        success: function (response) {
            if (response.result) {
                timer = setTimeout(updateDuration, 60000);
            }
        }
    });
}

/**
 * Pause
 */
function pauseSession() {
    $(".timer").timer('pause');
    clearTimeout(timer);
}

/**
 * Enable video
 * @param enable
 */
function enableVideo(enable) {
    easyrtc.setRoomApiField(room, 'videoEnabled', enable);
    easyrtc.enableCamera(enable);
    videoEnabled = enable;
    $('.self-video').toggleClass('muted-video');
    easyrtc.sendPeerMessage({'targetRoom': room}, 'videoEnabled', {enable: enable});
}

/**
 * Enable audio
 * @param enable
 */
function enableAudio(enable) {
    easyrtc.enableMicrophone(enable);
    audioEnabled = enable;
}

/**
 * After password
 * @param form
 * @param data
 */
function afterPasswordSet(form, data) {
    easyrtc.sendPeerMessage({'targetRoom': room}, 'lockStatus', {locked: true});
    toggleLockButton(true);
}

/**
 * Set speaking
 */
function setSpeaking() {
    easyrtc.sendPeerMessage({'targetRoom': room}, 'speaking');
}

/**
 * Send chat message
 * @param message
 */
function sendChatMessage(message) {
    insertChatMessage(myId, message);
    easyrtc.sendPeerMessage({'targetRoom': room}, 'chat', {message: message});
    $("#chat-text").val('');
}

/**
 * Inser chat message
 * @param id
 * @param message
 */
function insertChatMessage(id, message) {
    var name = easyrtc.idToName(id);
    var html = '<div class="media"><div class="media-body"><div class="media-heading">' + name + '</div>' + message + '</div></div>';
    var container = $('.chat-messages');
    container.append(html);

    if (container.hasClass('enable-scroll')) {
        container.scrollTop(container[0].scrollHeight);
    }
}

/**
 * Lock
 * @param locked
 */
function toggleLockButton(locked) {
    if (locked) {
        $('.lock-btn').removeClass('is-unlocked').addClass('is-locked');
    } else {
        $('.lock-btn').removeClass('is-locked').addClass('is-unlocked');
    }
}

/**
 * New stream
 * @param callerEasyrtcid
 * @param stream
 */
function addNewStream(callerEasyrtcid, stream) {
    var wrapperClass = ($('.hero-stream').length) ? 'user-stream' : 'user-stream hero-stream';
    var html = '<div class="' + wrapperClass + '">' +
        '<div class="stream-inner">' +
        '<video autoplay="autoplay" id="user-' + callerEasyrtcid + '"></video>' +
        '<a href="#" class="mute-stream btn btn-default"><i class="fa fa-microphone"></i></a>' +
        '</div>' +
        '</div>';
    $('.streams').append(html);
    easyrtc.setVideoObjectSrc(document.getElementById('user-' + callerEasyrtcid), stream);
    rearrangeStreams();
}

/**
 * Remove stream
 * @param callerEasyrtcid
 */
function removeStream(callerEasyrtcid) {
    var container = getStreamContainer(callerEasyrtcid);

    if (container.hasClass('hero-stream')) {
        $('.streams .user-stream:not(.hero-stream)').first().addClass('hero-stream').removeAttr('style');
    }

    container.remove();
    rearrangeStreams();
}

/**
 * Rearrange stream
 */
function rearrangeStreams() {
    var containerWidth = $('.streams').width();
    var streams = $('.streams .user-stream');
    var streamsCount = streams.length - 1;
    var streamWidth = (streamsCount > 6) ? containerWidth / streamsCount : containerWidth / 6;
    var left = 0;
    streams.each(function (index) {
        if (!$(this).hasClass('hero-stream')) {
            $(this).animate({left: left, width: streamWidth, height: streamWidth * 0.8}, {
                duration: 500,
                queue: false
            });
            left = left + streamWidth;
        }
    });
}

/**
 * View stream
 * @param container
 */
function viewStream(container) {
    $('.hero-stream').removeClass('hero-stream');
    $(container).addClass('hero-stream').removeAttr('style');
    rearrangeStreams();
}

/**
 * Toogle full screen
 */
function toggleFullscreen() {
    var selfVideo = document.getElementById("fullscreen-self");
    $('.room-page-inner .page-container').toggleClass('streams-fullscreen');
    rearrangeStreams();

    //Browser fullscreen
    if (!document.fullscreenElement && !document.mozFullScreenElement && !document.webkitFullscreenElement) {
        if (document.documentElement.requestFullscreen) {
            document.documentElement.requestFullscreen();
        } else if (document.documentElement.mozRequestFullScreen) {
            document.documentElement.mozRequestFullScreen();
        } else if (document.documentElement.webkitRequestFullscreen) {
            document.documentElement.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT);
        }
        easyrtc.setVideoObjectSrc(selfVideo, selfStream);
        $('.fullscreen-self-video').draggable();
    } else {
        if (document.cancelFullScreen) {
            document.cancelFullScreen();
        } else if (document.mozCancelFullScreen) {
            document.mozCancelFullScreen();
        } else if (document.webkitCancelFullScreen) {
            document.webkitCancelFullScreen();
        }
        easyrtc.setVideoObjectSrc(selfVideo, "");
    }
}

/**
 * Events
 */

$(document).on("click", ".toggle-video-btn", function (ev) {
    ev.preventDefault();
    $(this).toggleClass('is-on');
    enableVideo(!videoEnabled);
});

$(document).on("click", ".toggle-audio-btn", function (ev) {
    ev.preventDefault();
    $(this).toggleClass('is-on');
    enableAudio(!audioEnabled);
});

$(document).on("click", ".is-unlocked", function (ev) {
    ev.preventDefault();
    var url = $(this).find('.room-unlocked').attr('data-url');
    openModal(url);
});

$(document).on("click", ".is-locked", function (ev) {
    ev.preventDefault();
    var url = $(this).find('.room-locked').attr('data-url');

    $.ajax({
        url: url,
        type: 'post',
        success: function (response) {
            if (response.result) {
                if (response.message !== false) {
                    appendModal('standart-modal');
                    $("#modal .modal-body").html(response.message);
                    $("#modal").modal("show");
                }
                easyrtc.sendPeerMessage({'targetRoom': room}, 'lockStatus', {locked: false});
                toggleLockButton(false);
            }
        }
    });
});

$(document).on("click", "#chat-btn", function (ev) {
    ev.preventDefault();
    var message = $("#chat-text").val();
    if (message !== '') {
        sendChatMessage(message);
    }
});

$(document).on("click", ".user-stream:not(.hero-stream) video", function (ev) {
    viewStream($(this).closest('.user-stream'));
});

$(document).on("click", ".mute-stream", function (ev) {
    var mute = $(this).hasClass('muted') ? false : true;
    var video = $(this).closest('.stream-inner').find('video');
    var icon = $(this).find('i');
    easyrtc.muteVideoObject($(video).attr('id'), mute);
    $(this).toggleClass('muted');

    if (mute) {
        icon.removeClass('fa-microphone').addClass('fa-microphone-slash');
    } else {
        icon.removeClass('fa-microphone-slash').addClass('fa-microphone');
    }
});

$('#toggle-speaker').change(function () {
    viewSpeaker = $(this).prop('checked');
});

$('.chat-messages').on('scroll', function (event) {
    var scrollTop = $(this).scrollTop();
    var scrollHeight = $(this).prop('scrollHeight');
    var height = $(this).height();

    if (scrollTop < scrollHeight - height - 25) {
        $(this).removeClass('enable-scroll');
    }
    if (scrollTop > scrollHeight - height - 10) {
        $(this).addClass('enable-scroll');
    }
});

$(document).on("dblclick", ".hero-stream", function (ev) {
    toggleFullscreen();
});

$(document).on("click", ".fullscreen-close-btn", function (ev) {
    toggleFullscreen();
});

$(document).keyup(function (e) {
    e.preventDefault();
    if (e.keyCode == 27 && $('.streams-fullscreen').size()) {
        $('.room-page-inner .page-container').toggleClass('streams-fullscreen');
        rearrangeStreams();
    }
});

$("#chat-text").keypress(function (e) {
    if (e.keyCode == 13) {
        var message = $(this).val();
        if (message !== '') {
            sendChatMessage(message);
        }
    }
});