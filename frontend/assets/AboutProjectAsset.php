<?php
namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Class AboutProjectAsset
 * @package frontend\assets
 */
class AboutProjectAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $js = [];
    public $css = [
        '/css/about-project.css'
    ];
    public $depends = [
        'yii\web\JqueryAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        'frontend\assets\AppAsset'
    ];
}
