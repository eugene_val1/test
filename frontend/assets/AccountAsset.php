<?php
namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Class AccountAsset
 * @package frontend\assets
 */
class AccountAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $js = [
        'js/jquery.scrollTo.min.js',
        'js/jquery.slimscroll.min.js',
        'js/account.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        'frontend\assets\AppAsset',
    ];
}
