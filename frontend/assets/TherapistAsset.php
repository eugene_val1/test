<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Class TherapistAsset
 * @package frontend\assets
 */
class TherapistAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $js = [
        'js/therapist/therapist.js',
    ];
    public $depends = [
        'frontend\assets\AppAsset',
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
