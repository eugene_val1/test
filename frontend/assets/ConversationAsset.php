<?php
namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Class ConversationAsset
 * @package frontend\assets
 */
class ConversationAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $js = [
        'js/messages.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        'frontend\assets\AppAsset'
    ];
}
