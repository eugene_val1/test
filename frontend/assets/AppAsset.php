<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace frontend\assets;

use Yii;
use common\models\User;
use yii\web\AssetBundle;

/**
 * Class AppAsset
 * @package frontend\assets
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
    	'css/owl.carousel.min.css',
    	'css/owl.theme.default.min.css',
        'css/style.css',
        'css/mailchimp.css',
        'css/font-awesome/css/font-awesome.min.css'
        ];
    public $js = [
        'js/site.js',
        'js/bootstrap-show-password.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];

    /**
     * Init
     */
    public function init()
    {
        parent::init();

        if (empty(Yii::$app->tooltip)) {
            $this->js[] = '/js/anno.js';
            $this->css[] = '/css/anno.css';
            return;
        }

        //show tooltips
        if (
            Yii::$app->tooltip->addSteps($this)
        ) {
            $this->js[] = '/js/anno.js';
            $this->css[] = '/css/anno.css';
        }
    }
}
