<?php
namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Class RedactorAsset
 * @package frontend\assets
 */
class RedactorAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [];
    public $js = [
        'js/redactor/plugins/lineheight/lineheight.js',
    ];
    public $depends = [
        'vova07\imperavi\Asset'
    ];
}
