<?php

namespace frontend\assets;

use Yii;
use yii\web\AssetBundle;

/**
 * Class AbstractVideoRoomAsset
 * @package frontend\assets
 */
abstract class AbstractVideoRoomAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $depends = [
        'frontend\assets\AppAsset',
        'yii\bootstrap\BootstrapAsset',
    ];

    /**
     * Init
     */
    public function init()
    {
        $this->js = [
            $this->getServerUrl() . '/socket.io/socket.io.js',
            $this->getServerUrl() . '/easyrtc/easyrtc.js',
            'js/easyrtc/hark.bundle.js',
            'js/easyrtc/draggable.js',
            'js/timer.jquery.js',
            'js/bootstrap-toggle.min.js',
            'js/easyrtc/room.js',
        ];

        $this->css = [
            $this->getServerUrl() . '/easyrtc/easyrtc.css',
            'css/bootstrap-toggle.min.css',
        ];

        return parent::init();
    }

    /**
     * @return string
     */
    abstract public function getServerUrl();
}
