<?php

namespace frontend\assets;

use Yii;

/**
 * Class AuxiliaryVideoRoomAsset
 * @package frontend\assets
 */
class AuxiliaryVideoRoomAsset extends AbstractVideoRoomAsset
{
    /**
     * @return mixed
     */
    public function getServerUrl()
    {
        return Yii::$app->params['auxiliaryVideoServerUrl'];
    }
}
