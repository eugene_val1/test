<?php

namespace frontend\assets;

use Yii;

/**
 * Class VideoRoomAsset
 * @package frontend\assets
 */
class VideoRoomAsset extends AbstractVideoRoomAsset
{
    /**
     * @return mixed
     */
    public function getServerUrl()
    {
        return Yii::$app->params['videoServerUrl'];
    }
}
