<?php
namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Class ArticleAsset
 * @package frontend\assets
 */
class ArticleAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $js = [
        'js/jquery.jcrop.min.js',
        'js/crop.js',
    ];
    public $css = [
        'css/jcrop/jquery.jcrop.min.css'
    ];
    public $depends = [
        'yii\web\JqueryAsset',
        'frontend\assets\AppAsset',
    ];
}
