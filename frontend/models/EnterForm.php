<?php
namespace frontend\models;

use Yii;
use yii\base\Model;

/**
 * Enter form
 */
class EnterForm extends Model
{
    public $password;
    public $room;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['password'], 'required'],
            ['password', 'validatePassword', 'on' => 'check'],
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->room || !$this->room->validatePassword($this->password)) {
            $this->addError($attribute, Yii::t('main', 'Incorrect room password.'));
        }
    }
    
    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'password' => Yii::t('main', 'Password'),
        ];
    }
}
