<?php
namespace frontend\models;

use Yii;
use yii\base\Model;
use common\models\Messages\ConversationMessage;
use common\models\Event;
use yii\helpers\Json;

class CompleteTherapyForm extends Model
{
    public $reason;
    public $client;
    public $sendCopy;

    public function rules()
    {
        return [
            ['reason', 'required'],
            ['reason', 'string', 'max' => 1000],
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'reason' => Yii::t('main', 'The reason for completion of therapy'),
            'sendCopy' => Yii::t('main', 'Therapist will receive copy of this message'),
        ];
    }
    
    public function send() {
        
        $event = new Event([
            'type' => Event::TYPE_COMPLETE_THERAPY,
            'text' => $this->reason,
            'importance' => Event::IMPORTANCE_NORMAL,
            'data' => Json::encode([
                'client_id' => $this->client->id,
            ])
        ]);
        $event->save(false);
        
        if($this->sendCopy){
            $conversationMessage = new ConversationMessage([
                'user_id' => Yii::$app->user->id,
                'text' => $this->reason,
                'conversation_id' => $this->client->conversation_id
            ]);
            $conversationMessage->save(false);
        }        
    }
}
