<?php

namespace frontend\models;

use Yii;
use yii\data\ActiveDataProvider;
use common\models\Article;
use common\models\TextArticle;
use common\models\VideoArticle;
use common\models\TreatArticle;
use yii\data\ArrayDataProvider;

/**
 * Class ArticleFilter
 * @package frontend\models
 */
class ArticleFilter extends Article
{
    const SORT_RATING = 'rating';
    const SORT_RANDOM = 'random';

    const TYPE_ALL = 'all';
    const TYPE_POSTS = 'posts';
    const TYPE_VIDEOS = 'videos';

    /**
     * @deprecated
     */
    const TYPE_TREATS = 'treats';

    /**
     * @var string
     */
    public $sort;

    /**
     * @var string
     */
    public $type;

    /**
     * @var string
     */
    public $tag;

    /**
     * @var string
     */
    public $category;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['tag', 'string'],
            [['user_id', 'category'], 'integer'],
            ['sort', 'in', 'range' => [self::SORT_RATING, self::SORT_RANDOM]],
            ['type', 'in', 'range' => array_keys($this->getTypes())],
        ];
    }

    /**
     * @return string
     */
    public function formName()
    {
        return '';
    }

    /**
     * @param $params
     * @return ActiveDataProvider
     */
    public function filter($params)
    {
        $query = Article::find()->where(['published' => Article::STATUS_PUBLISHED])->distinct();
        $query->joinWith(['profile', 'therapistLang']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 21,
            ],
            'sort' => ['defaultOrder' => ['created' => SORT_DESC]]
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        if ($this->sort == self::SORT_RATING) {
            $query->orderBy(['votes_count' => SORT_DESC, 'created' => SORT_DESC]);
        } else if ($this->sort == self::SORT_RANDOM) {
            $query->orderBy('RAND()');
        }

        if (in_array($this->type, [self::TYPE_POSTS, self::TYPE_VIDEOS, self::TYPE_TREATS])) {
            $query->andFilterWhere([
                'article.type' => $this->filterType($this->type),
            ]);
        }

        if ($this->tag) {
            $query->joinWith('tags')->andWhere(['tag.name' => $this->tag]);
        }

        if ($this->user_id) {
            $query->andFilterWhere(['article.user_id' => $this->user_id]);
        }

        if ($this->category) {
            $query->andFilterWhere(['article.category_id' => $this->category]);
        }

        return $dataProvider;
    }

    /**
     * @param int $limit
     * @param int[] $excludeIds
     *
     * @return ArrayDataProvider
     */
    public function get($limit, $excludeIds = [])
    {
        $query = Article::find()
            ->joinWith(['profile', 'therapistLang'])
            ->where(['published' => Article::STATUS_PUBLISHED])
            ->limit($limit)
            ->orderBy('created DESC')
            ->distinct();

        if (in_array($this->type, [self::TYPE_POSTS, self::TYPE_VIDEOS, self::TYPE_TREATS])) {
            $query->andFilterWhere([
                'article.type' => $this->filterType($this->type),
            ]);
        }

        if (!empty($excludeIds)) {
            $query->andWhere(['not in', 'article.id', $excludeIds]);
        }

        $dataProvider = new ArrayDataProvider([
            'allModels' => $query->all(),
            'pagination' => [
                'pageSize' => $limit,
            ],
        ]);

        return $dataProvider;
    }

    /**
     * @return array
     */
    public static function getTypes()
    {
        return [
            self::TYPE_ALL => Yii::t('main', 'All field'),
            self::TYPE_POSTS => Yii::t('main', 'Posts'),
            self::TYPE_VIDEOS => Yii::t('main', 'Videos'),
        ];
    }

    /**
     * @param $type
     * @return mixed
     */
    public function filterType($type)
    {
        $types = [
            self::TYPE_POSTS => TextArticle::TYPE,
            self::TYPE_VIDEOS => VideoArticle::TYPE,
            self::TYPE_TREATS => TreatArticle::TYPE
        ];

        return $types[$type];
    }

    /**
     * @param $articleType
     * @return mixed|null
     */
    public function filterTypeReverse($articleType)
    {
        $types = [
            TextArticle::TYPE => self::TYPE_POSTS,
            VideoArticle::TYPE => self::TYPE_VIDEOS,
            TreatArticle::TYPE => self::TYPE_TREATS,
        ];

        return isset($types[$articleType]) ? $types[$articleType] : null;
    }
}
