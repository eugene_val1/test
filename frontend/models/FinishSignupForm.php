<?php

namespace frontend\models;

use common\components\MailHelper;
use Yii;
use yii\base\Model;
use common\models\User;
use yii\db\Exception;
use yii\web\NotFoundHttpException;
use yii\web\ServerErrorHttpException;

/**
 * Class FinishSignupForm
 *
 * @package frontend\models
 */
class FinishSignupForm extends Model
{
    public $user_id;
    public $agree_mailing;

    private $user;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['agree_mailing', 'boolean'],
        ];
    }

    /**
     * FinishSignupForm constructor.
     *
     * @param User $user
     */
    public function __construct($user)
    {
        parent::__construct([]);

        $this->user = $user;
        $this->agree_mailing = $user->profile->agree_mailing;
    }

    /**
     * @return bool|User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @throws ServerErrorHttpException
     */
    public function activate()
    {
        $this->getUser()->status = User::STATUS_ACTIVE;
        if (!$this->getUser()->save(false)) {
            Yii::error('Can not activate user with email: ' . $this->getUser()->email);
            throw new ServerErrorHttpException();
        }
    }

    /**
     * Update user model
     *
     * @return bool
     *
     * @throws NotFoundHttpException
     * @throws ServerErrorHttpException
     * @throws Exception
     */
    public function updateUser()
    {
        $transaction = Yii::$app->db->beginTransaction();

        if (!$this->getUser()) {
            throw new NotFoundHttpException();
        }

        //save user data
        $this->getUser()->removeToken();
        if (!$this->getUser()->save(false)) {
            $transaction->rollBack();
            throw new ServerErrorHttpException();
        }

        //save profile data
        if ($this->agree_mailing) {
            $this->getUser()->profile->agree_mailing = $this->agree_mailing;
            MailHelper::addToMailchimp($this->getUser()->email);
        }

        if (!$this->getUser()->profile->save(false)) {
            $transaction->rollBack();
            throw new ServerErrorHttpException();
        }

        $transaction->commit();

        return true;
    }
}
