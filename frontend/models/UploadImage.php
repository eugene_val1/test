<?php
namespace frontend\models;
use Yii;
use yii\base\Model;
use common\components\UploadedFileBehavior;

class UploadImage extends Model
{
    public $image;
    public $name;
    
    public function behaviors() {
        return [
            [
                'class' => UploadedFileBehavior::className(),
                'subdirectoryName' => 'temp/' . Yii::$app->user->id,
                'resizeType' => UploadedFileBehavior::RESIZE_NONE,
                'attributes' => [
                    'name' => ['fileAttribute' => 'image'],
                ]
            ],
        ];
    }

    public function rules()
    {
        return [
            [['image'], 'file', 'extensions'=>'jpg, gif, png'],
        ];
    }
}
