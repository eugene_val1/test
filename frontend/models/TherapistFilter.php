<?php

namespace frontend\models;

use Yii;
use yii\db\Expression;
use yii\data\ActiveDataProvider;
use common\models\Therapist;
use common\models\Property;
use DateTime;
use yii\web\Cookie;

/**
 * Class TherapistFilter
 * @package frontend\models
 */
class TherapistFilter extends Therapist
{
    const AGE_FROM = 18;
    const AGE_TO = 99;

    public $age;
    public $ageRange;
    private $propertyTypes;
    private $propertyAttributes = [];

    /**
     * Init
     */
    public function init()
    {
        parent::init();

        foreach ($this->getPropertyTypes() as $type) {
            $this->propertyAttributes[$type] = null;
            $this->propertyAttributes[$type . 'Values'] = null;
        }
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [array_merge($this->getPropertyTypes(), ['age', 'ageRange']), 'string'],
            [array_map(function ($type) {
                return $type . 'Values';
            }, $this->getPropertyTypes()), 'each', 'rule' => ['integer']],
            ['ageRange', 'delimiterSeparatedValues', 'params' => ['delimiter' => ',']],
            [array_merge($this->getPropertyTypes(), ['age']), 'delimiterSeparatedValues', 'params' => ['delimiter' => '-']],
            ['gender', 'in', 'range' => [self::MALE, self::FEMALE]],
        ];
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function delimiterSeparatedValues($attribute, $params)
    {
        if (!empty($this->$attribute)) {
            foreach (explode($params['delimiter'], $this->$attribute) as $value) {
                if (!is_numeric($value)) {
                    $this->addError($attribute, Yii::t('main', 'Wrong value'));
                    break;
                }
            }
        }
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'ageRange' => Yii::t('main', 'Age'),
            'gender' => Yii::t('main', 'Gender')
        ];
    }

    /**
     * @return string
     */
    public function formName()
    {
        return '';
    }

    /**
     * @param $params
     * @return ActiveDataProvider
     */
    public function filter($params)
    {
        $query = Therapist::find()->where(['publish_status' => Therapist::STATUS_PUBLISHED]);
        $query->joinWith(['translation']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => $this->getSort(),
            'pagination' => [
                'pageSize' => 30,
            ],
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        if ($this->age) {
            list($from, $to) = explode('-', $this->age);

            $from = new DateTime("-$from years");
            $toDate = $from->format('Y-m-d');

            $to = new DateTime("-$to years");
            $fromDate = $to->format('Y-m-d');

            $query->andWhere(['between', 'birth_date', $fromDate, $toDate]);
        }

        if ($this->gender) {
            $query->andFilterWhere(['gender' => $this->gender]);
        }

        foreach ($this->getPropertyTypes() as $type) {
            if ($this->$type) {
                $query->joinWith(['propertyIds' => function ($q) use ($type) {
                    $q->from('therapist_property ' . $type);
                }]);
                $query->andFilterWhere([$type . '.property_id' => explode('-', $this->$type)]);
            }
        }

        return $dataProvider;
    }

    /**
     * @param $params
     */
    public function parseUrl($params)
    {
        if ($this->load($params) && $this->validate()) {
            foreach ($this->getPropertyTypes() as $type) {
                if ($this->$type) {
                    $this->{$type . 'Values'} = explode('-', $this->$type);
                }
            }

            if ($this->age) {
                list($from, $to) = explode('-', $this->age);
                $this->ageRange = $from . ',' . $to;
            }
        }
    }

    /**
     * @param $route
     * @return array
     */
    public function createUrl($route)
    {
        $params = [];
        foreach ($this->getPropertyTypes() as $type) {
            if (count($this->{$type . 'Values'}) > 0) {
                $params[$type] = implode('-', $this->{$type . 'Values'});
            }
        }

        if ($this->ageRange) {
            list($from, $to) = explode(',', $this->ageRange);
            if (!($from == self::AGE_FROM && $to == self::AGE_TO)) {
                $params['age'] = str_replace(',', '-', $this->ageRange);
            }
        }

        if ($this->gender) {
            $params['gender'] = $this->gender;
        }

        array_unshift($params, $route);

        return $params;
    }

    /**
     * @param $route
     * @return array|mixed
     */
    public function getUrl($route)
    {
        $params = Yii::$app->request->queryParams;
        array_unshift($params, $route);

        return $params;
    }

    /**
     * @return array
     */
    public function getPropertyTypes()
    {
        if (empty($this->propertyTypes)) {
            $types = Property::getTypes();
            $this->propertyTypes = (is_array($types)) ? array_keys($types) : [];
        }

        return $this->propertyTypes;
    }

    /**
     * @return array
     * @throws \yii\base\InvalidCallException
     */
    public function getSort()
    {
        if (!Yii::$app->settingManager->get('therapistRandomSort')) {
            return ['defaultOrder' => ['number' => SORT_ASC]];
        }

        //select all therapists ids despite publish status
        if (Yii::$app->cache->exists('therapists-ids')) {
            $therapistIds = Yii::$app->cache->get('therapists-ids');
        } else {
            $therapistIds = Therapist::find()
                ->select('id')
                ->column();
            Yii::$app->cache->set('therapists-ids', $therapistIds, 3600);
        }

        $cookies = Yii::$app->request->cookies;

        //select pseudo therapist ids
        if (Yii::$app->cache->exists('pseudo-therapists-ids')) {
            $pseudoTherapists = Yii::$app->cache->get('pseudo-therapists-ids');
        } else {
            $pseudoTherapists = Therapist::find()
                ->select(['id'])
                ->where([
                    'in',
                    'alias',
                    array_merge(Therapist::$catBugentalAliases, Therapist::$treatfieldAliases)]
                )
                ->column();
            Yii::$app->cache->set('pseudo-therapists-ids', $pseudoTherapists, 3600);
        }

        if ($cookies->has('sort')) {
            $sort = explode(',', $cookies->getValue('sort'));
            if (count($sort) === count($therapistIds)) {
                $therapistIds = $sort;
            } else {
                $therapistIds = array_merge($sort, $therapistIds);
            }
        } else {
            shuffle($therapistIds);
        }

        $therapistCount = count($therapistIds);
        foreach ($therapistIds as $i => $therapistId) {
            if ($i === 0 && $therapistCount > 1 && in_array($therapistId, $pseudoTherapists)) {
                $next = $therapistIds[$i + 1];
                $therapistIds[1] = $therapistId;
                $therapistIds[0] = $next;
            }
        }

        $fields = implode(',', $therapistIds);

        if (!$cookies->has('sort')) {
            Yii::$app->response->cookies->add(new Cookie([
                'name' => 'sort',
                'value' => $fields,
                'expire' => strtotime('+7 days'),
            ]));
        }

        $fieldSort = '`profile`.id';
        if (!empty($fields)) {
            $fieldSort = 'FIELD (`profile`.id, ' . $fields . ')';
        }

        return [
            'attributes' => [
                'id' => [
                    'asc' => [new Expression($fieldSort)],
                ]
            ],
            'defaultOrder' => ['id' => SORT_ASC]
        ];
    }

    /**
     * @param string $attribute
     * @return mixed
     */
    public function __get($attribute)
    {
        if (key_exists($attribute, $this->propertyAttributes)) {
            return $this->propertyAttributes[$attribute];
        } else {
            return parent::__get($attribute);
        }
    }

    /**
     * @param string $attribute
     * @param mixed $value
     * @return mixed
     */
    public function __set($attribute, $value)
    {
        if (key_exists($attribute, $this->propertyAttributes)) {
            return $this->propertyAttributes[$attribute] = $value;
        } else {
            return parent::__set($attribute, $value);
        }
    }
}
