<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Faq;

/**
 * FaqSearch represents the model behind the search form about `common\models\Faq`.
 */
class FaqSearch extends Faq
{
    public $key;
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['key'], 'required'],
            [['key'], 'string', 'min' => 3],
        ];
    }
    
    public function attributeLabels()
    {
        return [
            'key' => Yii::t('main', 'Search keywords'),
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search()
    {
        $query = Faq::find();
        $query->joinWith(['translation']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['number'=>SORT_ASC]]
        ]);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'or',
            ['like', 'faq_lang.question', $this->key],
            ['like', 'faq_lang.answer', $this->key],
        ]);
        
        return $dataProvider;
    }
}
