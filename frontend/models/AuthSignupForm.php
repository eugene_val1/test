<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use common\models\AuthProvider;
use common\models\User;
use yii\web\NotFoundHttpException;

/**
 * Class AuthSignupForm
 *
 * @package frontend\models
 */
class AuthSignupForm extends Model
{
    public $user_id;
    public $provider_name;
    public $email;
    public $country_id;
    public $timezone;
    public $agree_mailing;

    /**
     * @var User
     */
    private $_user;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'provider_name', 'email', 'timezone'], 'required'],
            ['email', 'email'],
            ['email', 'unique', 'targetClass' => User::class, 'on' => 'update-email'],
            [['country_id', 'user_id'], 'integer'],
            [['provider_name'], 'string'],
            [['user_password', 'timezone'], 'safe'],
            ['agree_mailing', 'boolean'],
            ['user_id', 'validateProvider'],
        ];
    }

    /**
     * AuthSignupForm constructor
     *
     * @param array $config
     *
     * @throws NotFoundHttpException
     */
    public function __construct(array $config = [])
    {
        parent::__construct($config);

        $user = $this->getUser();
        if (!$user) {
            throw new NotFoundHttpException();
        }
        $this->timezone = $user->timezone;
        $this->country_id = $user->country_id;
        $this->email = $user->email;
        $this->agree_mailing = $user->profile->agree_mailing;
    }

    /**
     * @param $attribute
     * @param $params
     * @throws NotFoundHttpException
     */
    public function validateProvider($attribute, $params)
    {
        $authProvider = AuthProvider::findOne([
            'user_id' => $this->user_id,
            'name' => $this->provider_name,
        ]);
        if (!$authProvider) {
            throw new NotFoundHttpException();
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => Yii::t('main', 'User'),
            'timezone' => Yii::t('main', 'Timezone'),
            'country_id' => Yii::t('main', 'Country'),
        ];
    }

    /**
     * @return bool|User
     */
    public function getUser()
    {
        if ($this->_user === null) {
            /** @var User $user */
            $user = User::find()
                ->with(['profile'])
                ->where(['id' => $this->user_id])
                ->one();
            $this->_user = $user ?: false;
        }

        return $this->_user;
    }

    /**
     * Update user model
     * @return bool
     * @throws NotFoundHttpException
     */
    public function updateUser()
    {
        if (!$this->getUser()) {
            throw new NotFoundHttpException();
        }

        if (!empty($this->email) && empty($this->getUser()->email)) {
            //activate user on setting new email
            $this->getUser()->email = $this->email;
            $this->getUser()->status = User::STATUS_ACTIVE;
        }

        $this->getUser()->timezone = $this->timezone;
        $this->getUser()->country_id = $this->country_id;
        $this->getUser()->profile->agree_mailing = $this->agree_mailing;

        return $this->getUser()->save(false);
    }
}
