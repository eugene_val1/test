<?php
namespace frontend\models;

use yii\base\Model;

class CropImageForm extends Model
{
    public $x;
    public $y;
    public $width;
    public $height;
    public $image;

    public function rules()
    {
        return [
            [['x', 'y', 'width', 'height', 'image'], 'required'],
        ];
    }
}
