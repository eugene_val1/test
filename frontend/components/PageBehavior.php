<?php

namespace frontend\components;

use Yii;
use yii\base\Behavior;
use yii\web\Controller;
use common\models\Page;

/**
 * Class PageBehavior
 * @package frontend\components
 */
class PageBehavior extends Behavior
{
    public $section;
    public $page;

    /**
     * @return array
     */
    public function events()
    {
        return [Controller::EVENT_BEFORE_ACTION => 'beforeAction'];
    }

    /**
     * @param $event
     * @return bool
     */
    public function beforeAction($event)
    {
        $page = Page::find()->where(['section' => $this->section])->with('translation')->one();

        if ($page) {
            $this->page = $page;
            $page->setMetaTags();
        }

        return true;
    }
}
