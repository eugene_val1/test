<?php

namespace frontend\components;

use yii\base\Component;
use Yii;

/**
 * Class ContextMenuHandler
 * We may handle the visibility of context menu here
 *
 * @package frontend\components
 */
class ContextMenuHandler extends Component
{
    /**
     * @return bool
     */
    public static function isVisible()
    {
        $controller = Yii::$app->controller->id;
        $action = Yii::$app->controller->action->id;

        if ($action === 'index'
            && in_array($controller, ['therapists', 'field', 'article', 'room', 'messages'])
        ) {
            return true;
        }

        if ($controller === 'field' && $action === 'category') {
            return true;
        }

        if ($controller === 'messages' && in_array($action, ['start', 'conversation'])) {
            return true;
        }

        return false;
    }
}