<?php

namespace frontend\components;

/**
 * Class ShareLinks
 * @package frontend\components
 */
class ShareLinks extends \ijackua\sharelinks\ShareLinks
{
    const SOCIAL_TELEGRAM = 8;

    public $shareUrlMap = [
        self::SOCIAL_TWITTER => 'https://twitter.com/intent/tweet?url={url}',
        self::SOCIAL_FACEBOOK => 'https://www.facebook.com/sharer/sharer.php?u={url}',
        self::SOCIAL_VKONTAKTE => 'http://vk.com/share.php?url={url}',
        self::SOCIAL_GPLUS => 'https://plus.google.com/share?url={url}',
        self::SOCIAL_LINKEDIN => 'http://www.linkedin.com/shareArticle?url={url}',
        self::SOCIAL_KINDLE => 'http://fivefilters.org/kindle-it/send.php?url={url}',
        self::SOCIAL_XING => 'https://www.xing.com/spi/shares/new?url={url}',
        self::SOCIAL_TELEGRAM => 'https://telegram.me/share/url?url={url}'
    ];
}