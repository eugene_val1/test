<?php

namespace frontend\components;

use frontend\assets\AppAsset;
use Yii;
use yii\base\Component;
use yii\web\Cookie;
use yii\web\View;

/**
 * Class TooltipHelper
 *
 * @package frontend\components
 */
class TooltipHelper extends Component
{
    const COOKIE_STEP_NAME = 'tooltip-step';

    const STEP_EMPTY = 0;
    const STEP_1 = 1;
    const STEP_2 = 2;
    const STEP_3 = 3;
    const STEP_4 = 4;
    const STEP_5 = 5;

    private $controller;
    private $action;
    private $step;
    private $debugStep;

    /**
     * Init component
     */
    public function init()
    {
        parent::init();

        $this->controller = Yii::$app->controller->id;
        $this->action = Yii::$app->controller->action->id;

        $this->step = (int)Yii::$app->request->cookies->getValue(self::COOKIE_STEP_NAME);

        $this->debugStep = Yii::$app->request->get('step', null);
        if (!is_null($this->debugStep)) {
            $this->step = $this->debugStep;
        }
    }

    /**
     * Add steps
     * @param AppAsset $asset
     * @return bool
     */
    public function addSteps($asset)
    {
        if (is_null($this->debugStep)) {
            if (!Yii::$app->settingManager->get('showTooltips', false)) {
                return false;
            }

            if (!Yii::$app->request->cookies->has(self::COOKIE_STEP_NAME)) {
                return false;
            } elseif (!Yii::$app->user->isGuest && !Yii::$app->user->identity->profile->show_tooltips) {
                return false;
            }
        }

        $this->setTooltipsLabels();

        //1
        if ($this->controller !== 'therapists' && (empty($this->step) || $this->step == self::STEP_EMPTY)) {
            $asset->js[] = '/js/tooltips/step-1.js';
            $this->updateStep(self::STEP_1);
            return true;
        }

        //2
        if ($this->controller === 'therapists' && $this->action === 'index' && $this->step <= self::STEP_1) {
            $asset->js[] = '/js/tooltips/step-2.js';
            $this->updateStep(self::STEP_2);
            return true;
        }

        //3
        if ($this->controller === 'therapists' && $this->action !== 'index' && $this->step <= self::STEP_2) {
            $asset->js[] = '/js/tooltips/step-3.js';
            $this->updateStep(self::STEP_3);
            return true;
        }

        //4
        if ($this->step == self::STEP_3) {
            $asset->js[] = '/js/tooltips/step-4.js';
            $this->updateStep(self::STEP_4);
            return true;
        }

        //5
        if ($this->controller === 'account' && $this->action === 'index' && $this->step == self::STEP_4) {
            $asset->js[] = '/js/tooltips/step-5.js';
            $this->updateStep(self::STEP_5);
            return true;
        }

        return false;
    }

    /**
     * Update step
     * @param $step
     */
    public function updateStep($step)
    {
        Yii::$app->response->cookies->add(new Cookie([
            'name' => self::COOKIE_STEP_NAME,
            'value' => $step,
            'expire' => time() + 30758400, //year
        ]));
    }

    /**
     * Example appointment
     * @return bool
     */
    public function showExampleAppointment()
    {
        if (Yii::$app->user->isGuest) {
            return false;
        }

        return Yii::$app->user->identity->profile->show_tooltips;
    }

    /**
     * Generate tooltips labels into js var
     */
    private function setTooltipsLabels()
    {
        if (!Yii::$app->view) {
            return;
        }

        $labels = [
            'next' => Yii::t('tooltip', 'Next'),
            'cancel' => Yii::t('tooltip', 'Cancel'),
            'finish' => Yii::t('tooltip', 'Finish'),
            'step1' => [
                'menu' => Yii::t('tooltip', 'Go to the Therapists page'),
            ],
            'step2' => [
                'therapist' => Yii::t('tooltip', 'Choose a therapist'),
            ],
            'step3' => [
                'appointment' => Yii::t('tooltip', 'Sign up for a consultation. You can describe your request and a convenient time to meet'),
                'profile' => Yii::t('tooltip', 'Login to your account'),
            ],
            'step4' => [
                'therapy' => Yii::t('tooltip', 'Section of personal therapy'),
            ],
            'step5' => [
                'therapist' => Yii::t('tooltip', 'The therapist will appear when you agree with him about the time and he will assign you the first appointment'),
                'time' => Yii::t('tooltip', 'When the therapist assigns you an appointment, the date and time of will be displayed here, and you will be able to make a prepayment. Payment will be debited only after a successful appointment'),
                'room' => Yii::t('tooltip', 'After the appointment is paid for, the online room becomes active and you can enter it at the time agreed with the therapist'),
            ],
        ];

        Yii::$app->view->registerJs('var tooltipLabels = ' . json_encode($labels), View::POS_HEAD);
    }

    /**
     * @return mixed
     */
    public function getStep()
    {
        return $this->step;
    }
}