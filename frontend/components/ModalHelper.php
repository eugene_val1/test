<?php

namespace frontend\components;

use yii\base\Component;
use Yii;

/**
 * Class ModalHelper
 *
 * @package frontend\components
 */
class ModalHelper extends Component
{
    const COOKIE_NAME = 'flash-message';

    private $centeredText;

    /**
     * ModalHelper constructor
     *
     * @param bool $centeredText
     */
    public function __construct($centeredText = true)
    {
        parent::__construct([]);

        $this->centeredText = $centeredText;
    }

    /**
     * @param string $text
     */
    public function setMessage($text)
    {
        if ($this->centeredText) {
            $text = '<h4 class="text-center">' . $text . '</h4>';
        }

        $message = json_encode([
            'text' => utf8_encode($text),
        ], JSON_UNESCAPED_SLASHES);

        setcookie(self::COOKIE_NAME, $message, time() + 5 * 60, '/');
    }
}