<?php

namespace frontend\components\seo;

/**
 * Class ViewMetaHelper
 *
 * @package frontend\components\seo
 */
class FieldViewMetaHelper extends ViewMetaHelper
{
    /**
     * @var string
     */
    protected $tag;

    /**
     * FieldViewMetaHelper constructor
     *
     * @param string $tag
     */
    public function __construct($tag)
    {
        $this->tag = $tag;
        parent::__construct();
    }

    /**
     * @param string $title
     * @return $this
     */
    public function setTitle($title)
    {
        switch ($this->tag) {
            case 'visitka':
                $title = 'Видео психологов и психотерапевтов';
                break;
            case 'treatfield':
                $title = 'Видео платформы онлайн психотерапии';
                break;
        }

        if ($title) {
            parent::setTitle($title);
        }

        return $this;
    }

    /**
     * @param string $text
     * @return $this
     */
    public function registerDescription($text)
    {
        switch ($this->tag) {
            case 'visitka':
                $text = 'Видео визитки психологов, психотерапевтов. Выбрать психолога по видео. Тритфилд – онлайн-платформа консультаций с психологом и психотерапевтом.';
                break;
        }

        parent::registerDescription($text);

        return $this;
    }
}