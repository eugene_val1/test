<?php

namespace frontend\components\seo;

use yii\base\Component;
use Yii;
use yii\web\View;

/**
 * Class ViewMetaHelper
 *
 * @package frontend\components\seo
 */
class ViewMetaHelper extends Component
{
    /**
     * @var View
     */
    protected $view;

    /**
     * ViewMetaHelper constructor.
     * @param View $view
     */
    public function __construct($view = null)
    {
        $this->view = $view;
        if ($this->view === null) {
            $this->view = Yii::$app->controller->view;
        }

        parent::__construct([]);
    }

    /**
     * @param string $title
     * @return $this
     */
    public function setTitle($title)
    {
        $this->view->title = Yii::$app->view->title . ' | ' . $title;

        return $this;
    }

    /**
     * @param string $text
     * @return $this
     */
    public function registerDescription($text)
    {
        $this->view->registerMetaTag([
            'name' => 'description',
            'content' => $text,
        ], 'description');

        return $this;
    }
}