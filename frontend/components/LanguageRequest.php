<?php

namespace frontend\components;

use yii\web\Request;
use common\models\Language;

/**
 * Class LanguageRequest
 *
 * @package frontend\components
 */
class LanguageRequest extends Request
{
    /**
     * @var
     */
    private $_langUrl;

    /**
     * @return bool|string
     */
    public function getUrl()
    {
        if ($this->_langUrl !== null) {
            return $this->_langUrl;
        }

        $this->_langUrl = parent::getUrl();
        $url_list = explode('/', $this->_langUrl);
        $lang_url = isset($url_list[1]) ? $url_list[1] : null;
        if (Language::setCurrent($lang_url, true)) {
            $this->_langUrl = substr($this->_langUrl, strlen($lang_url) + 1);
        }

        return $this->_langUrl;
    }
}