<?php

namespace frontend\components;

use Yii;
use yii\web\Controller;

/**
 * Class FrontendController
 *
 * @package frontend\components
 */
class FrontendController extends Controller
{
    /**
     * @param \yii\base\Action $event
     * @return bool
     */
    public function beforeAction($event)
    {
        $this->view->title = Yii::$app->settingManager->get('mainTitle');
        $this->view->registerMetaTag([
            'name' => 'keywords',
            'content' => Yii::$app->settingManager->get('mainKeywords')
        ], 'keywords');
        $this->view->registerMetaTag([
            'name' => 'description',
            'content' => Yii::$app->settingManager->get('mainDescription')
        ], 'description');

        return parent::beforeAction($event);
    }

}
