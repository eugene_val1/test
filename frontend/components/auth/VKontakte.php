<?php

namespace frontend\components\auth;

use yii\authclient\clients\VKontakte as VKontakteClient;

class VKontakte extends VKontakteClient
{
    /** @inheritdoc */
    protected function defaultName()
    {
        return 'vk';
    }
    
    public $attributeNames = [
        'uid',
        'first_name',
        'last_name',
        'photo_200'
    ];
    
    protected function defaultNormalizeUserAttributeMap()
    {
        return [
            'id' => 'uid',
            'name' => function ($attributes) {
                return $attributes['first_name'] . ' ' . $attributes['last_name'];
            },
            'email' => function ($attributes) {
                return '';
            },
            'photo' => 'photo_200'                    
        ];
    }
}
