<?php
namespace frontend\components\auth;

use yii\authclient\OAuth2;
use Yii;

class Odnoklassniki extends OAuth2
{
    /** @inheritdoc */
    public $authUrl = 'http://www.odnoklassniki.ru/oauth/authorize';
    
    /** @inheritdoc */
    public $tokenUrl = 'http://api.odnoklassniki.ru/oauth/token.do';
    
    /** @inheritdoc */
    public $apiBaseUrl = 'http://api.odnoklassniki.ru/';
    
    /** @var string Публичный ключ */
    public $publicKey;
    
    /** @inheritdoc */
    protected function defaultName()
    {
        return 'odnoklassniki';
    }
    
    /** @inheritdoc */
    protected function defaultTitle()
    {
        return 'Odnoklassniki';
    }
    
    public $attributeNames = [
        'uid',
        'user.name',
        'user.pic128x128',
        'user.email'
    ];
    
    /** @inheritdoc */
    protected function initUserAttributes()
    {
        $token = $this->getAccessToken()->getParam('access_token');
        $fields = implode(',', $this->attributeNames);
        $sig = md5("application_key={$this->publicKey}fields={$fields}format=jsonmethod=users.getCurrentUser" . md5("{$token}{$this->clientSecret}"));
        $params = array(
            'method'          => 'users.getCurrentUser',
            'access_token'    => $token,
            'application_key' => $this->publicKey,
            'format'          => 'json',
            'sig'             => $sig,
            'fields'          => $fields,
        );
        return $this->api('fb.do', 'GET', $params);
    }
    
    /** @inheritdoc */
    protected function defaultNormalizeUserAttributeMap()
    {
        return [
            'id' => 'uid',
            'photo' => 'pic128x128',
            'email' => function ($attributes) {
                return (isset($attributes['email'])) ? $attributes['email'] : '' ;
            },
        ];
    }
    
    protected function defaultReturnUrl()
    {
        return Yii::$app->getUrlManager()->createDefaultAbsoluteUrl(['user/auth', 'authclient' => $this->defaultName()], true);
    }
}