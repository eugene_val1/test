<?php

namespace frontend\components\auth;

use Yii;
use yii\authclient\clients\Facebook as FacebookClient;

/**
 * Class Facebook
 * @package frontend\components\auth
 */
class Facebook extends FacebookClient
{
    /**
     * @inheritdoc
     */
    protected function initUserAttributes()
    {
        $user = $this->api('me?fields=id,name,email', 'GET');
        $user['photo'] = 'https://graph.facebook.com/' . $user['id'] . '/picture?type=large';
        if (!isset($user['email'])){
            $user['email'] = '';
        }
        return $user;
    }

    /**
     * @return string
     */
    public function getReturnUrl()
    {
        return Yii::$app
            ->urlManager
            ->createAbsoluteUrl(['user/auth', 'authclient' => 'facebook']);
    }
}
