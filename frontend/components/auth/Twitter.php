<?php

namespace frontend\components\auth;

use yii\authclient\clients\Twitter as TwitterClient;
use Yii;

/**
 * Class Twitter
 * @package frontend\components\auth
 */
class Twitter extends TwitterClient
{
    /**
     * @var array
     */
    public $attributeParams = [
        'include_email' => 'true',
    ];

    /**
     * @return array
     */
    protected function defaultNormalizeUserAttributeMap()
    {
        return [
            'email' => function ($attributes) {
                if (empty($attributes['email'])) {
                    Yii::error('Empty user email in Twitter: ' . json_encode($attributes));
                }
                return isset($attributes['email']) ? $attributes['email'] : '';
            },
            'photo' => function ($attributes) {
                if (!empty($attributes['profile_image_url_https'])) {
                    return str_replace('_normal', '', $attributes['profile_image_url_https']);
                }

                return '';
            }
        ];
    }

    /**
     * @return mixed
     */
    protected function defaultReturnUrl()
    {
        return Yii::$app->getUrlManager()
            ->createDefaultAbsoluteUrl(['user/auth', 'authclient' => $this->defaultName()], true);
    }
}
