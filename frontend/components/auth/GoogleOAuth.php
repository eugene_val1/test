<?php

namespace frontend\components\auth;

use yii\authclient\clients\GoogleOAuth as GoogleOAuthClient;
use Yii;

/**
 * Class GoogleOAuth
 *
 * @package frontend\components\auth
 */
class GoogleOAuth extends GoogleOAuthClient
{
    /**
     * @return array
     */
    protected function defaultNormalizeUserAttributeMap()
    {
        return [
            'name' => 'displayName',
            'email' => ['emails', '0', 'value'],
            'photo' => ['image', 'url']
        ];
    }

    /**
     * @return array
     */
    protected function initUserAttributes()
    {
        $attributes = parent::initUserAttributes();
        if (isset($attributes['image'])) {
            //set image size before download
            $attributes['image']['url'] = str_replace('sz=50', 'sz=300', $attributes['image']['url']);
        }

        return $attributes;
    }

    /**
     * @return mixed
     */
    protected function defaultReturnUrl()
    {
        return Yii::$app->getUrlManager()
            ->createDefaultAbsoluteUrl([
                'user/auth', 'authclient' => $this->defaultName()
            ], true);
    }
}
