<?php

namespace frontend\widgets;

use Yii;
use yii\helpers\Url;

/**
 * Class Sidebar
 * @package frontend\widgets
 */
class Sidebar extends \yii\bootstrap\Widget
{
    /**
     * @return string
     */
    public function run() {
        if(!Yii::$app->user->isGuest && Url::toRoute('site/index') != Url::current()){
            $user = Yii::$app->user->identity;
            $profile = $user->profile;
        
            return $this->render('sidebar/' . $profile->type, ['profile' => $profile]);
        }
    }
}
