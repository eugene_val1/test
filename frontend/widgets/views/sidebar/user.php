<?php

use common\models\Profile;
use common\models\SystemNews;
use yii\helpers\Url;

/** @var Profile $profile */

?>

<aside class="user-nav user-side-menu <?= (Url::toRoute('account/index') === Url::current()) ? 'scroll-nav' : '' ?>">
    <div class="nav-inner">

        <div class="nav-head">
            <div class="user-avatar user-side-avatar">
                <img src="<?= $profile->getFileUrl('image'); ?>" class="img-circle" alt="">
            </div>
            <div class="name-block">
                <div class="user-side-name">
                    <?= $profile->name; ?>
                </div>
                <div class="user-side-edit">
                    <a href="<?= Url::toRoute(['account/profile']); ?>"><?= Yii::t('main', 'Edit profile'); ?></a>
                </div>
            </div>
        </div>

        <div class="user-side-nav">
            <ul class="sidebar">
                <li>
                    <a class="hash-scroll" href="<?= Url::toRoute(['account/index', '#' => 'section-therapy']); ?>">
	                    <span class="icon"> <i class="fa fa-user"></i> </span>
	                    <span><?= Yii::t('main', 'Therapy'); ?></span>
                    </a>
                </li>
                <li>
                    <a href="<?= Url::toRoute(['messages/index']); ?>">
	                    <span class="icon"><i class="fa fa-comments-o"></i></span>
	                    <span><?= Yii::t('main', 'Messages'); ?></span>
                        <span class="conversation-counter"></span>
                    </a>
                </li>
                <li class="news">
                    <a class="hash-scroll" href="<?= Url::toRoute(['account/index', '#' => 'section-news']); ?>">
                        <img src="/img/news-treatfield-white.png" alt="">
                        <span>Новости Тритфилд</span>
                        <?php $newsCount = SystemNews::countNew() ?>
                        <?php if ($newsCount): ?>
                            <span class="notify-label blue-notify-label"><?= $newsCount ?></span>
                        <?php endif; ?>
                    </a>
                </li>
                <!--li>
                    <a data-target="#modal" href="<?= Url::toRoute(['account/feedback']); ?>">
                        <i class="fa fa-commenting-o"></i> <span><?= Yii::t('main', 'Leave feedback'); ?></span>
                    </a>
                </li-->
                <li>
                    <a data-target="#modal" href="<?= Url::toRoute(['contact/index']); ?>">
	                    <span class="icon"><i class="fa fa-envelope"></i> </span>
	                    <span><?= Yii::t('main', 'Feedback'); ?></span>
                    </a>
                </li>
                <?php if (Yii::$app->settingManager->get('showTooltips', false)): ?>
                    <li>
                        <a href="<?= Url::toRoute(['user/activate-tooltips']); ?>">
	                        <span class="icon"><i class="fa fa-info-circle"></i></span>
                            <span><?= Yii::t('main', 'How to use'); ?></span></a>
                    </li>
                <?php endif; ?>
                <li>
                    <a data-method="post" href="<?= Url::toRoute(['user/logout']); ?>">
	                    <span class="icon">
		                    <i class="fa fa-sign-out"></i>
	                    </span>
                        <span><?= Yii::t('main', 'Logout'); ?></span></a>
                </li>
            </ul>
        </div>

    </div>
</aside>