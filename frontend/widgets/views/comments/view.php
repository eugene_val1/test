<?php
use yii\widgets\ListView;
use nirvana\infinitescroll\InfiniteScrollPager;
?>

<div id="comments-list">
    <div class="comment-form">
        <?= $this->render('@frontend/views/comment/_send', ['comment' => $new]); ?>
    </div>
    <?= ListView::widget([
        'dataProvider' => $dataProvider,
        'options' => [
            'id' => 'comments',
            'class' => 'comments-container',
        ],
        'summary' => '',
        'emptyText' => Yii::t('main', 'No comments yet. Leave first comment.'),
        'itemOptions' => [
            'tag' => false,
        ],
        'itemView' => function ($model, $key, $index, $widget) use ($article) {        
            return $this->render('@frontend/views/comment/_comment', ['comment' => $model, 'article' => $article]);
        },
        'pager' => [
            'class' => InfiniteScrollPager::className(),
            'widgetId' => 'comments',
            'itemsCssClass' => 'comments-container',
            'contentLoadedCallback' => 'void(0)',
            'nextPageLabel' => Yii::t('main', 'Load more'),
            'linkOptions' => [
                'class' => 'btn btn-lg btn-block',
            ],
            'pluginOptions' => [
                'contentSelector' => '.comments-container',
                'loading' => [
                    'msgText' => Yii::t('main', 'Loading...'),
                    'finishedMsg' => Yii::t('main', 'No more comments to load...'),
                ],
            ],
        ],
    ]);
    ?>
</div>