<?php if (count($langs)): ?>
<p class="languages">
    <select class="form-control">
        <option value="<?= '/' . $current->url . Yii::$app->getRequest()->getUrl() ?>"><?= $current->name ?></option>
        <?php foreach ($langs as $lang): ?>
            <option value="<?= '/' . $lang->url . Yii::$app->getRequest()->getUrl() ?>"><?= $lang->name ?></option>
        <?php endforeach; ?>
    </select>
</p>
<?php endif; ?>