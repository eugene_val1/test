<?php
namespace frontend\widgets;

use common\models\Comment;
use yii\data\ActiveDataProvider;

class Comments extends \yii\bootstrap\Widget
{
    public $article;
    public $level = 0;
    
    public function run() {
        $query = Comment::find()
                ->where(['article_id' => $this->article->id, 'level' => $this->level])
                ->joinWith(['profile', 'therapistLang'])
                ->with([
                    'replies' => function ($query) {
                        $query->with(['vote']);
                    }, 
                    'vote'
                ]
        );

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 30,
            ],
            'sort'=> ['defaultOrder' => ['created' => SORT_DESC]]
        ]);
        
        $new = new Comment([
            'article_id' => $this->article->id,
            'reply_to' => 0
        ]);
        return $this->render('comments/view', ['dataProvider' => $dataProvider, 'article' => $this->article, 'new' => $new]);
    }
}
