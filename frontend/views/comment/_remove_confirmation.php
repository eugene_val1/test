<?php

use yii\helpers\Html;

Yii::$app->assetManager->bundles['yii\bootstrap\BootstrapAsset'] = false;
Yii::$app->assetManager->bundles['yii\web\JqueryAsset'] = false;
Yii::$app->assetManager->bundles['yii\web\YiiAsset'] = false;
?>

<div class="remove-confirmation">
    <h3><?= Yii::t('main', 'Please, confirm the deletion') ?></h3>
    <div class="row">
        <div class="col-lg-12">
            <p><?= nl2br(Html::encode($comment->text)); ?></p>
            <p>                
                <?= Html::a(Yii::t('backend', 'Cancel'), '#', ['class' => 'btn btn-default btn-sm pull-right close-modal']); ?>
                <?= Html::a(Yii::t('backend', 'Yes'), 
                    ['comment/delete', 'id' => $comment->id], 
                    [
                        'class' => 'btn btn-danger btn-sm pull-right',
                        'data-target' => '#ajax',
                        'data-callback' => 'deleteComment'
                    ]
                ); ?>
            </p>
        </div>
    </div>
</div>