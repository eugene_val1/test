<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;

?>
<?php if(!Yii::$app->user->isGuest): ?>
<div class="new-comment">
    <div class="row">
        <div class="col-sm-12 form-wrapper">  
            <?php
            $form = ActiveForm::begin([
                'action' => ['comment/send', 'id' => $comment->article_id, 'reply_to' => $comment->reply_to],
                'id' => 'comment-form',
                'enableAjaxValidation' => false,
                'enableClientValidation' => false,
                'options' => [
                    'data-reply' => $comment->reply_to
                ]
            ]);
            ?>
            <?= $form->field($comment, 'text')->textArea(['class' => "form-control comment-textarea", 'rows' => "3"]); ?>
            <div class="form-group form-submit">
                <?= Html::submitButton(Yii::t('main', 'Send'), ['class' => 'small-page-btn page-btn orange pull-right btn-submit']); ?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>  
    </div>
</div>
<?php else: ?>
<p><?= Yii::t('main', 'Please, login to leave comments'); ?></p>
<?php endif; ?>