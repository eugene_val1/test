<?php 
use common\models\Comment;
?>

<?php foreach ($replies as $index => $reply): ?>
    <?php if($index == Comment::VIEW_REPLIES_COUNT): ?>
        <div class="more-comments">
    <?php endif; ?>
    <?= $this->render('@frontend/views/comment/_comment', ['comment' => $reply, 'article' => $article]); ?>
    <?php if($index > Comment::VIEW_REPLIES_COUNT && $index + 1 == count($replies)): ?>        
        </div>
        <a><?= Yii::t('main', 'Show all replies') ?></a>
    <?php endif; ?>
<?php endforeach; ?>