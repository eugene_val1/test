<?php

use yii\helpers\Html;
use yii\helpers\Url;
use common\models\Comment;

?>

<div data-id="<?= $comment->id ?>" data-level="<?= $comment->level ?>" id="comment-<?= $comment->id ?>" class="comment level-<?= $comment->level ?>">
    <hr>

    <div class="comment-inner media">
        <?php if($comment->status !== Comment::STATUS_DELETED): ?>
        <div class="comment-avatar pull-left">
            <img class="media-object" width="75px" src="<?= $comment->profile->getFileUrl('image'); ?>" alt="">
        </div>
        <div class="media-body">
            <h5 class="media-heading">
                <?= $comment->authorName; ?>
                <span class="comment-time"><?= Yii::$app->formatter->asTime($comment->created, 'short'); ?> </span>
                <span class="comment-date"><?= Yii::$app->formatter->asDate($comment->created, 'medium'); ?></span>
            </h5>
            <?= nl2br(Html::encode($comment->text)); ?>
            <div class="comment-buttons">
                <span class="vote-button">
                    <a data-target="#ajax" data-callback="updateVoteCounter" href="<?= Url::toRoute(['comment/vote', 'id' => $comment->id]); ?>" class="btn btn-default btn-xs <?= ($comment->vote) ? 'btn-primary' : ''; ?>">
                        <span class="vote-counter"><?= ($comment->votes_count) ?: ''; ?></span> <i class="fa fa-thumbs-up"></i> <?= Yii::t('main', 'Like'); ?>
                    </a>                   
                </span>
                <?php if(!Yii::$app->user->isGuest): ?>
                    <a class="reply-to btn btn-default btn-xs" href="<?= Url::toRoute(['comment/send', 'id' => $comment->article_id, 'reply_to' => $comment->id])?>">
                        <i class="fa fa fa-commenting-o"></i> <?= Yii::t('main', 'Reply'); ?>
                    </a>
                    <?php if(Yii::$app->user->id == $comment->user_id || Yii::$app->user->id == $article->user_id): ?>
                    <a data-target="#modal" class="btn btn-default btn-xs" href="<?= Url::toRoute(['comment/delete-confirmation', 'id' => $comment->id])?>">
                        <i class="fa fa-trash-o"></i> <?= Yii::t('main', 'Delete'); ?>
                    </a>
                    <?php endif; ?>
                <?php endif; ?>
                <a class="report btn btn-default btn-xs" data-target="#ajax" href="<?= Url::toRoute(['comment/report', 'id' => $comment->id])?>">
                    <i class="fa fa-exclamation-triangle"></i> <?= Yii::t('main', 'Report'); ?>
                </a>
            </div>
        </div>
        <?php else: ?>
            <p><?= Yii::t('main', 'Comment deleted'); ?></p>
        <?php endif; ?>
    </div>

    <div id="answers-<?= $comment->id ?>">
        <div class="comment-form reply-form"></div>
        <div class="comment-list">
            <?php if($comment->level === 0 && $comment->replies_count > 0): ?>
                <?= $this->render('@frontend/views/comment/_replies', ['replies' => $comment->replies, 'article' => $article]); ?>
            <?php elseif($comment->level > 0 && $comment->replies_count > 0): ?>
            <a class="btn btn-default btn-xs" data-callback="loadReplies" data-target="#ajax" href="<?= Url::toRoute(['comment/replies', 'id' => $comment->id]); ?>">
                <i class="fa fa-caret-down"></i> <?= Yii::t('main', 'View') . ' ' . $comment->replies_count . ' ' . Yii::t('main', 'replies') ?>
            </a>
            <?php endif; ?>
        </div>
    </div>

</div>