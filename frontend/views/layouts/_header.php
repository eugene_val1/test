<?php

use yii\helpers\Url;
use common\models\Menu;
use common\models\Language;
use common\widgets\Languages;

$cookiesAccepted = isset($_COOKIE['accept-cookies']) && (int)$_COOKIE['accept-cookies'] === 1;

?>

<script>
    //MailChimp subscription delay
    SUBSCRIBE_POPUP_DELAY = <?= Yii::$app->settingManager->get('subscribePopupDelay', 10) * 1000 ?>;
</script>

<header>
    <nav id="nav" class="main-nav navbar navbar-default navbar-fixed-top <?= (isset($this->params['navOpacity']) && $this->params['navOpacity'] === true ) ? '' : ' compact-nav full-nav' ?>">

        <div id="cookies-info" class="col-md-12 <?= $cookiesAccepted ? 'accepted' : '' ?>">
            <div class="col-md-8">
                <p>
                    Мы используем файлы cookies, чтобы пользоваться сайтом было легко и удобно.
                    Оставаясь на сайте, вы соглашаетесь с нашей <a href="/page/privacy-policy">Политикой конфиденциальности</a>
                </p>
            </div>
            <div class="col-md-4 accept-block">
                <a class="page-btn small-page-btn transparent">Согласен</a>
            </div>
        </div>

        <div class="container-fluid flex-container">
            <div class="navbar-header pull-left">
                <?php
                    if (Yii::$app->controller->id === 'site' && Yii::$app->controller->action->id === 'index') {
                        $logoHref = '';
                    } else {
                        $logoHref = 'href="' . Yii::$app->homeUrl .'"';
                    }
                ?>
                <a class="navbar-brand" <?= $logoHref ?>>
                    <span class="logo"></span>
                </a>

                <?php if (\frontend\components\ContextMenuHandler::isVisible()): ?>
                    <button type="button" class="navbar-toggle" id="context-menu-btn" data-toggle="collapse">

                    </button>
                <?php endif; ?>

                <button type="button" class="navbar-toggle" id="main-menu-btn" data-toggle="collapse" data-target="#nav-collapse">
                    <span class="sr-only">Toggle navigation</span>
                </button>
            </div>

            <div class="right-block-container">
                <!-- login-btn -->
                <div class="navbar-header">
                    <ul class="nav navbar-nav">
                        <li>
                            <?= Menu::renderRoomHandlerButton(true); ?>
                        </li>
                        <?php if (Yii::$app->user->isGuest): ?>
                            <li class="login-button">
                                <a <?= (Yii::$app->controller->id !== 'user') ? 'data-target="#modal"' : '' ?> href="<?= Url::toRoute(['user/login']); ?>"><?= Yii::t('main', 'Login'); ?></a>
                            </li>
                        <?php else: ?>
                            <li class="account-block">
                                <a href="<?= Url::toRoute(['account/index']); ?>" class="account-link <?= (Url::toRoute('site/index') != Url::current()) ? 'launch-sidebar' : '' ?>">
                                    <div class="avatar user-avatar pull-left">
                                        <img src="<?= Yii::$app->user->identity->profile->getFileUrl('image'); ?>" class="img-circle" alt="">
                                    </div>

                                    <span class="conversation-counter"></span>
                                </a>
                            </li>
                        <?php endif; ?>
                    </ul>
                </div>

                <div id="nav-collapse" class="collapse navbar-collapse navbar-left">
                    <ul class="nav navbar-nav navbar-left">
                        <?php foreach (Menu::getLinksByPosition('top') as $link): ?>
                            <li>
                                <a <?= ($link->blank) ? 'target="_blank"' : '' ?> <?= ($link->modal) ? 'data-target="#modal"' : '' ?> href="<?= $link->url ?>"><?= $link->content ?></a>
                            </li>
                        <?php endforeach; ?>
                        <li id="pay-menu-btn">
                            <?= Menu::renderRoomHandlerButton(); ?>
                        </li>
                        <li class="search-nav">
                            <a class="search" href="#"></a>
                            <div class="search-bar">
                                <input class="main-search" type="text" value="">
                                <span class="search"></span>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>

        </div>
    </nav>
</header>