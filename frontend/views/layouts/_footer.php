<?php

use common\models\Menu;
use common\widgets\Languages;

?>

<footer>
    <div class="container">
        <div class="row footer-content">
            <div class="col-sm-3 col-xs-6">
                <a class="footer-brand" href="/"><h4><?= Yii::t('main', 'Treatfield') ?></h4></a>
                <ul>
                    <?php foreach (Menu::getLinksByPosition('general') as $link): ?>
                        <li>
                            <a <?= ($link->blank) ? 'target="_blank"' : '' ?> <?= ($link->modal) ? 'data-target="#modal"' : '' ?> href="<?= $link->href ?>">
                                <?= $link->content ?>
                            </a>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>
            <div class="col-sm-3 col-xs-6">
                <h4><?= Yii::t('main', 'Help') ?></h4>
                <ul>
                    <?php foreach (Menu::getLinksByPosition('help') as $link): ?>
                        <li>
                            <a <?= ($link->blank) ? 'target="_blank"' : '' ?> <?= ($link->modal) ? 'data-target="#modal"' : '' ?> href="<?= $link->href ?>"><?= $link->content ?></a>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>
            <div class="col-sm-3 col-xs-6">
                <h4><?= Yii::t('main', 'Contacts') ?></h4>
                <ul>
                    <?php foreach (Menu::getLinksByPosition('contact') as $link): ?>
                        <li>
                            <a <?= ($link->blank) ? 'target="_blank"' : '' ?> <?= ($link->modal) ? 'data-target="#modal"' : '' ?> href="<?= $link->href ?>">
                                <?= $link->content ?>
                            </a>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>
            <div class="col-sm-3 col-xs-6">
                <h4><?= Yii::t('main', 'Follow us') ?></h4>
                <ul class="social">
                    <?php foreach (Menu::getLinksByPosition('social') as $link): ?>
                        <li>
                            <a <?= ($link->blank) ? 'target="_blank"' : '' ?> <?= ($link->modal) ? 'data-target="#modal"' : '' ?> href="<?= $link->href ?>" rel="nofollow noreferrer noopener">
                                <?= $link->content ?>
                            </a>
                        </li>
                    <?php endforeach; ?>
                </ul>
                <?= Languages::widget(['onlyActive' => true]); ?>
            </div>
        </div>
    </div>
</footer>
