<?php

use common\components\SiteManager;
use yii\helpers\Html;
use yii\bootstrap\BootstrapPluginAsset;
use frontend\widgets\Sidebar;
use frontend\assets\AppAsset;

AppAsset::register($this);
BootstrapPluginAsset::register($this);

?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?= /*Yii::$app->settingManager->get('headContent');*/'' ?>
        <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
        <link rel="icon" href="/favicon.ico" type="image/x-icon">

        <?php if (SiteManager::isIndexingDisabled()): ?>
            <meta name="robots" content="noindex,nofollow">
        <?php endif; ?>

        <?php $this->head() ?>        
    </head>
    <body class="<?= Yii::$app->controller->id . '-body' . ((!Yii::$app->user->isGuest) ? ' logged-in' : '') ?>">
        <?php $this->beginBody() ?>
        <?php include_once("../widgets/analyticstracking.php") ?>

        <?= $this->render('_header') ?>
        <?= $content ?>
        <?= Sidebar::widget(); ?>

        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>