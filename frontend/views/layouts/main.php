<?php

use common\components\SiteManager;
use common\components\StructuredData;
use frontend\assets\AppAsset;
use frontend\widgets\Sidebar;
use yii\bootstrap\BootstrapPluginAsset;
use yii\helpers\Html;

AppAsset::register($this);
BootstrapPluginAsset::register($this);

?>

<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
        <head>
            <meta charset="<?= Yii::$app->charset ?>"/>
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <?= Html::csrfMetaTags() ?>
            <title><?= Html::encode($this->title) ?></title>
            <?= Yii::$app->settingManager->get('headContent') ?>
            <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
            <link rel="icon" href="/favicon.ico" type="image/x-icon">

            <script type="application/ld+json">
                <?= StructuredData::getJson() ?>
            </script>

            <?php if (SiteManager::isIndexingDisabled()): ?>
                <meta name="robots" content="noindex,nofollow">
            <?php endif; ?>

            <?php $this->head() ?>
        </head>
        <body class="<?= Yii::$app->controller->id . '-body' . ((!Yii::$app->user->isGuest) ? ' logged-in' : '') ?>">
        <?php $this->beginBody() ?>
        <?= $this->render('@frontend/widgets/analyticstracking') ?>

        <?= $this->render('_header') ?>
        <?= $content ?>
        <?= Sidebar::widget(); ?>
        <?= $this->render('_footer') ?>

        <?php $this->endBody() ?>
        </body>
    </html>
<?php $this->endPage() ?>