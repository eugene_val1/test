<?php

use common\models\Article;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use vova07\imperavi\Widget;
use common\models\Language;
use dosamigos\fileupload\FileUpload;
use kartik\widgets\Select2;
use yii\web\JsExpression;
use common\models\CategoryPage;
use yii\helpers\ArrayHelper;

$imageUploadError = "Ваше изображение больше 2 МБ, пожалуйста уменьшите размер и попробуйте еще раз. Вы можете воспользоваться онлайн сервисами для уменьшения размера фото";

?>

<h2><?= Yii::t('main', 'Create article') ?></h2>
<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
<div class="col-lg-8">
    <?= $form->field($model, 'title')->textInput() ?>
    <?=
        $form->field($model, 'content')->widget(Widget::className(), [
            'settings' => [
                'lang' => Language::getCurrent()->url,
                'minHeight' => 200,
                'toolbarFixedTopOffset' => 80,
                'imageUpload' => Url::to(['/redactor/image-upload']),
                'formattingAdd' => [
                    'fullwidth' => [
                        'title' => Yii::t('main', 'Fullwidth text'),
                        'tag' => 'p',
                        'class' => 'fullwidth'
                    ],
                    'fullwidth-h' => [
                        'title' => Yii::t('main', 'Fullwidth title'),
                        'tag' => 'h1',
                        'class' => 'fullwidth'
                    ],
                ],
                'buttonsHide' => ['indent', 'outdent'],
                'plugins' => [
                    'imagemanager',
                    'fontcolor',
                    'video',
                    'fontfamily',
                    'fontsize',
                ]
            ],
            'plugins' => [ 
                'lineheight' => 'frontend\assets\RedactorAsset'
            ]
        ]);
    ?>
</div>
<div class="col-lg-4">
    <div class="form-group">
        <img id="article-image" src="<?= $model->getFileUrl('image'); ?>" alt="" class="img-responsive full-width-img">            
        <span class="fileinput-button article-image-button">
            <span class="upload-link"><?= '<i class="fa fa-upload"></i> ' . Yii::t('main', 'Upload photo') ?></span>
            <?=
            FileUpload::widget([
                'model' => $model,
                'attribute' => 'cover_imageFile',
                'url' => ['article/upload-image'],
                'clientOptions' => [
                    'maxFileSize' => Article::MAX_IMAGE_SIZE,
                    'autoUpload' => true,
                ],
                'clientEvents' => [
                    'fileuploaddone' => 'function(e, data) {                        
                        if(data.result.status){
                            cropImage(data.result);
                        } else {
                            $("#image-error").html(data.result.error).show();
                        }
                    }',
                    'fileuploadfail' => 'function(e, data) {
                        $("#image-error").html("' . $imageUploadError . '").show();
                    }',
                ],
            ]);
            ?>
            </span>
        <div id="image-error" class="alert alert-danger"></div>
    </div>
    <?= $form->field($model, 'image', ['template' => '{input} {error}'])->hiddenInput(['id' => 'article-image-input']) ?>
    <?= $form->field($model, 'cover_image', ['template' => '{input} {error}'])->hiddenInput(['id' => 'article-cover-image-input']) ?>
    <?= $form->field($model, 'category_id')->dropDownList(ArrayHelper::map(CategoryPage::find()->with('translation')->all(), 'id', 'name'),['prompt'=>'']); ?>
    <?= $form->field($model, 'short_description')->textarea() ?>
    <?=
        $form->field($model, 'tagList')->widget(Select2::classname(), [
            'options' => ['multiple' => true, 'id' => 'article-tags'],
            'data' => $model->getTagList(),
            'showToggleAll' => false,
            'pluginLoading' => false,
            'pluginOptions' => [
                'minimumInputLength' => 3,
                'ajax' => [
                    'url' => Url::toRoute(['article/tags']),
                    'dataType' => 'json',
                    'data' => new JsExpression('function(params) { return {q:params.term}; }')
                ],
                'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                'templateResult' => new JsExpression('function(tag) { return tag.text; }'),
                'templateSelection' => new JsExpression('function (tag) { return tag.text; }'),
                'tags' => true,
                'maximumInputLength' => 25,                
            ],
        ]);
    ?>
    <div class="form-group">
    <?= Html::submitButton('<i class="fa fa-save"></i> ' . ($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Save')), ['class' => $model->isNewRecord ? 'btn btn-success btn-sm' : 'btn btn-primary btn-sm']) ?>
        <a href="<?= Url::toRoute('index'); ?>" class="btn btn-danger btn-sm"><i class="fa fa-backward"></i> <?= Yii::t('backend', 'Cancel'); ?></a>
    </div>
</div>
<?php ActiveForm::end(); ?>