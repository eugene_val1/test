<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use vova07\imperavi\Widget;
use common\models\Language;
use kartik\widgets\Select2;
use yii\web\JsExpression;
?>

<h2><?= Yii::t('main', 'Create treat') ?></h2>
<?php $form = ActiveForm::begin(); ?>
<div class="col-lg-12">
    <?=
        $form->field($model, 'content')->widget(Widget::className(), [
            'settings' => [
                'lang' => Language::getCurrent()->url,
                'minHeight' => 200,
                'toolbarFixedTopOffset' => 80,
                'buttons' => ['bold', 'italic', 'deleted', 'unorderedlist', 'orderedlist'],
                'limiter' => 500,
                'plugins' => [
                    'limiter'
                ]
            ]
        ]);
    ?>
    <?=
        $form->field($model, 'tagList')->widget(Select2::classname(), [
            'options' => ['multiple' => true, 'id' => 'article-tags'],
            'data' => $model->getTagList(),
            'showToggleAll' => false,
            'pluginLoading' => false,
            'pluginOptions' => [
                'minimumInputLength' => 3,
                'ajax' => [
                    'url' => Url::toRoute(['article/tags']),
                    'dataType' => 'json',
                    'data' => new JsExpression('function(params) { return {q:params.term}; }')
                ],
                'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                'templateResult' => new JsExpression('function(tag) { return tag.text; }'),
                'templateSelection' => new JsExpression('function (tag) { return tag.text; }'),
                'tags' => true,
                'maximumInputLength' => 25,                
            ],
        ]);
    ?>
    <div class="form-group">
    <?= Html::submitButton('<i class="fa fa-save"></i> ' . ($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Save')), ['class' => $model->isNewRecord ? 'btn btn-success btn-sm' : 'btn btn-primary btn-sm']) ?>
        <a href="<?= Url::toRoute('index'); ?>" class="btn btn-danger btn-sm"><i class="fa fa-backward"></i> <?= Yii::t('backend', 'Cancel'); ?></a>
    </div>
</div>
<?php ActiveForm::end(); ?>