<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>

<div class="crop-article-image">
    <h3><?= Yii::t('main', 'Crop image') ?></h3>
    <p class="modal-description"><?= Yii::t('main', 'Select part of image to be viewed in field') ?></p>
    <img id="need-crop-image" data-ratio="1.6" src="<?= $url ?>" />
    <?php $form = ActiveForm::begin([
        'id' => 'crop-form', 
        'action' => Url::toRoute('crop-image'),
        'options' => [
            'data-callback' => 'afterCrop'
        ]
    ]); ?>
    <?= $form->field($model, 'x', ['template' => '{input} {error}'])->hiddenInput(['id' => 'crop-x']) ?>
    <?= $form->field($model, 'y', ['template' => '{input} {error}'])->hiddenInput(['id' => 'crop-y']) ?>
    <?= $form->field($model, 'width', ['template' => '{input} {error}'])->hiddenInput(['id' => 'crop-w']) ?>
    <?= $form->field($model, 'height', ['template' => '{input} {error}'])->hiddenInput(['id' => 'crop-h']) ?>
    <?= $form->field($model, 'image', ['template' => '{input} {error}'])->hiddenInput() ?>
    <div id="crop-image-error" class="alert alert-danger"></div>
    <div class="row">
        <div class="col-sm-12">
            <div class="form-group form-submit">
                <?= Html::submitButton(Yii::t('main', 'Save'), ['class' => 'small-page-btn page-btn orange btn-submit', 'name' => 'save-button']) ?>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>