<?php
$this->title = $model->isNewRecord ? Yii::t('backend', 'Create article') : Yii::t('backend', 'Update Article') . ': ' . $model->title;
use frontend\assets\ArticleAsset;
ArticleAsset::register($this);
?>

<div class="content-page container-fluid">
    <div class="page-inner">
        <div class="page-container articles">
            <div class="row">
                <div class="col-lg-12">
                    <?php if($model->type): ?>
                        <?= $this->render('_' . $model->type, ['model' => $model]); ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>