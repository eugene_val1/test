<?php

use yii\helpers\Url;
use common\models\Article;
use common\models\Therapist;
use yii\helpers\StringHelper;
?>
<div class="article-item-wrapper" id="article-item-<?= $article->id ?>">
    <div class="row article-row">
        <div class="col-sm-3">
            <img src="<?= ($article->profile->type == Therapist::TYPE) ? '/img/therapist-treat.jpg' : '/img/user-treat.jpg' ?>" class="img-responsive full-width-img">
        </div>
        <div class="col-sm-9">
            <h3 class="title"><?= $article->title ?></h3>
            <p class="text-muted">
                <?php if ($article->published == Article::STATUS_PUBLISHED): ?>
                    <i class="fa fa-calendar"></i> <?= Yii::$app->formatter->asDate($article->created, "medium") ?>
                <?php else: ?>
                    <i class="fa fa-lock"></i> <?= Yii::t('main', 'Waiting for publication') ?>
                <?php endif; ?>
            </p>
            <p><?= StringHelper::truncate($article->content, 150) ?></p>                        
        </div>
        <a class="article-update-btn btn btn-default" href="<?= Url::toRoute(['update', 'id' => $article->id]); ?>">
            <i class="fa fa-pencil"></i>
        </a>
        <a class="article-remove-btn btn btn-default" data-target="#modal" href="<?= Url::toRoute(['remove-confirmation', 'id' => $article->id]); ?>">
            <i class="fa fa-trash-o"></i>
        </a>
    </div>                 
    <hr>
</div>