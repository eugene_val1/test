<?php
use yii\helpers\Url;
use common\models\Article;
use common\models\User;
use yii\widgets\ListView;
use nirvana\infinitescroll\InfiniteScrollPager;
?>

<div class="content-page container-fluid">
    <div class="page-inner">
        <a class="menu-btn"></a>
        <div class="articles">
            <h2><?= Yii::t('main', 'Articles') ?></h2>
            <?= ListView::widget([
                'dataProvider' => $provider,
                'options' => [
                    'id' => 'scroll-container',
                    'class' => 'page-container',
                ],
                'summary' => '<div class="grid-sizer"></div>',
                'emptyText' => Yii::t('main', 'No articles yet.'),
                'itemOptions' => [
                    'tag' => false,
                ],
                'itemView' => function ($model, $key, $index, $widget) {
                    return $this->render('_' . $model->type . '_item', ['article' => $model]);
                },
                'pager' => [
                    'class' => InfiniteScrollPager::className(),
                    'widgetId' => 'scroll-container',
                    'itemsCssClass' => 'page-container',
                    'contentLoadedCallback' => 'void(0)',
                    'nextPageLabel' => Yii::t('main', 'Load more'),
                    'linkOptions' => [
                        'class' => 'btn btn-lg btn-block',
                    ],
                    'pluginOptions' => [
                        'contentSelector' => '.page-container',
                        'loading' => [
                            'msgText' => Yii::t('main', 'Loading...'),
                            'finishedMsg' => Yii::t('main', 'No more articles to load'),
                        ]
                    ],
                ],
            ]);
            ?>   
        </div>
    </div>
</div>
<aside class="side-nav side-menu auto-open">
    <div class="nav-inner simple-side-menu">
        <a class="menu-close-btn"></a>
        <ul class="filter">
            <li>
                <a href="<?= Url::toRoute(['index']); ?>" class="active">
                    <?= Yii::t('main', 'All articles') . (($count['all']) ? ' (' . $count['all'] . ')'  : ''); ?>
                </a>
            </li>
            <?php foreach (Article::getPublishStatuses() as $publishedStatus => $statusName): ?>
            <li>
                <a href="<?= Url::toRoute(['index', 'status' => $publishedStatus]); ?>">
                    <?= $statusName . (($count[$publishedStatus]) ?  ' (' . $count[$publishedStatus] . ')' : ''); ?>
                </a>
            </li>
            <?php endforeach; ?>
            <?php if(Yii::$app->user->identity->role === User::ROLE_THERAPIST):?>
            <li>
                <a href="<?= Url::toRoute(['create']); ?>">
                    <img width="24px" src="/img/more-icon.png" alt="">
                    <?= Yii::t('main', 'Create article'); ?>
                </a>
            </li>
            <?php endif; ?>
            <li>
                <a href="<?= Url::toRoute(['treat']); ?>">
                    <img width="24px" src="/img/more-icon.png" alt="">
                    <?= Yii::t('main', 'Create a treat'); ?>
                </a>
            </li>
        </ul>
    </div>
    <div class="nav-footer"></div>
</aside>