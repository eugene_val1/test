<?php

use yii\helpers\Html;

Yii::$app->assetManager->bundles['yii\bootstrap\BootstrapAsset'] = false;
Yii::$app->assetManager->bundles['yii\web\JqueryAsset'] = false;
Yii::$app->assetManager->bundles['yii\web\YiiAsset'] = false;
?>

<div class="remove-confirmation">
    <h3><?= Yii::t('main', 'Please, confirm the deletion') ?></h3>
    <div class="row">
        <div class="col-lg-12">
            <p>
                <img src="<?= $model->getFileUrl('image'); ?>" class="img-responsive certificate-image" alt="">
                <?= Html::a(Yii::t('backend', 'Cancel'), '#', ['class' => 'btn btn-default btn-sm pull-right close-modal']); ?>
                <?= Html::a(Yii::t('backend', 'Yes'), ['remove', 'id' => $model->id], ['class' => 'btn btn-danger btn-sm remove-certificate pull-right']); ?>
            </p>
        </div>
    </div>
</div>