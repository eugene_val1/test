<?php

use yii\helpers\Html;

Yii::$app->assetManager->bundles['yii\bootstrap\BootstrapAsset'] = false;
Yii::$app->assetManager->bundles['yii\web\JqueryAsset'] = false;
Yii::$app->assetManager->bundles['yii\web\YiiAsset'] = false;
?>

<div class="col-lg-12">
    <h4><?= Yii::t('main', 'Certificates') ?></h4>
    <div id="certificates">
        <div class="row">
            <?php if(count($certificates) > 0): ?>
            <?php foreach($certificates as $certificate): ?>
                <div class="col-sm-3 certificate-item">
                    <div class="text-center">
                    <img src="<?= $certificate->getFileUrl('image'); ?>" alt="" class="img-responsive" >
                    </div>
                    <div class="text">
                        <?= ($certificate->description) ?: Yii::t('main', 'No description') ?>
                    </div>
                    <div class="certificate-buttons">
                        <?= Html::a('<i class="fa fa-pencil"></i> ' . Yii::t('main', 'Edit'), ['certificate/update', 'id' => $certificate->id], [
                            'data' => [
                                'pjax' => 0,
                                'target' => '#modal'
                            ],
                        ]); ?>
                        <?= Html::a('<i class="fa fa-trash"></i> ' . Yii::t('main', 'Remove'), ['certificate/remove-confirmation', 'id' => $certificate->id], [
                            'class' => 'pull-right',
                            'data' => [
                                'pjax' => 0,
                                'target' => '#modal'
                            ],
                        ]); ?>
                    </div>
                </div>
            <?php endforeach; ?>
            <?php else: ?>
                <p><?= Yii::t('main', 'Add your sertificates') ?></p>
            <?php endif;?>
        </div>
    </div>
    <?= Html::a('<i class="fa fa-plus"></i> ' . Yii::t('main', 'Add certificate'), ['certificate/create'], [
        'class' => 'btn btn-primary btn-sm pull-right add-certificate',
        'data' => [
            'pjax' => 0,
            'target' => '#modal'
        ],
    ]); ?>
</div>