<?php

use common\models\Certificate;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use frontend\models\UploadImage;
use dosamigos\fileupload\FileUpload;

$imageUploadError = "Ваше изображение больше 2 МБ, пожалуйста уменьшите размер и попробуйте еще раз. Вы можете воспользоваться онлайн сервисами для уменьшения размера фото";

?>

<div class="add-certificate">
    <h3><?= $model->isNewRecord ? Yii::t('main', 'Add certificate') : Yii::t('main', 'Update certificate') ?></h3>
    <div class="form-wrapper">
        <?php $form = ActiveForm::begin([
            'id' => 'certificate-form',
            'options' => [
                'data-callback' => 'afterUpdateCertificate',
            ]
        ]); ?>
        <?= $form->field($model, "image", ['template' => '{input}{error}', 'options' => ['class' => 'certificate-image-input']])->hiddenInput(); ?>
        <div class="row">
            <div id="image-error" class="alert alert-danger"></div>
            <div id="current-image" class="text-center">
                <?php if($model->image):?>
                <img class="img-responsive" src="<?= $model->isNewRecord ? UploadImage::getFileFolderUrl() . '/' . $model->image : $model->getFileUrl('image')  ?>" alt="">
                <?php endif; ?>
            </div>
            <div class="col-sm-12">
                <span class="fileinput-button certificate-form-button">
                    <span class="upload-link"><i class="fa fa-upload"></i> <?= Yii::t('main', 'Upload image file') ?></span>
                    <?=
                    FileUpload::widget([
                        'model' => $upload,
                        'attribute' => 'image',
                        'url' => ['certificate/upload-image'],
                        'clientOptions' => [
                            'maxFileSize' => Certificate::MAX_IMAGE_SIZE,
                            'autoUpload' => true,
                        ],
                        'clientEvents' => [
                            'fileuploaddone' => 'function(e, data) {
                                if(data.result.status){
                                    $("#current-image").html(data.result.img);
                                    $(".certificate-image-input input").val(data.result.name);
                                    $("#certificate-descriptions").show();
                                    $("#image-error").html("").hide();
                                } else {
                                    $("#image-error").html(data.result.error).show();
                                }
                            }',
                            'fileuploadfail' => 'function(e, data) {
                                $("#image-error").html("' . $imageUploadError . '").show();
                            }',
                        ],
                    ]);
                    ?>
                </span>
            </div>
            <div id="certificate-descriptions" <?= $model->image ? '' : 'style="display:none"' ?>>
                <div class="col-sm-12">
                    <ul class="nav nav-tabs nav-justified">
                    <?php foreach($languages as $language):?>
                        <li <?= ($language->local == Yii::$app->language) ? 'class="active"' : '' ?>>
                            <a data-toggle="tab" href="#<?= 'tab-' . $language->local ?>">
                            <?= $language->name ?>
                            <?php if($translations[$language->local]->hasErrors()):?>
                                <span class="label label-danger">!</span>
                            <?php endif;?>
                            </a>
                        </li>
                    <?php endforeach; ?>
                    </ul>
                    <div class="tab-content">
                        <?php foreach($translations as $local => $translation):?>
                        <div id="<?= 'tab-' . $local ?>" class="tab-pane fade <?= ($local == Yii::$app->language) ? 'in active' : '' ?>">
                            <?= $form->field($translation, "[$local]description")->textarea(); ?>
                        </div>
                        <?php endforeach; ?>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group form-submit text-center">
                        <?= Html::submitButton(
                                $model->isNewRecord ? Yii::t('main', 'Add') : Yii::t('main', 'Save'),
                                ['class' => 'small-page-btn page-btn orange btn-submit', 'name' => 'save-button']
                        ) ?>
                    </div>
                </div>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>