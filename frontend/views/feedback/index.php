<?php

use yii\data\BaseDataProvider;
use yii\widgets\ListView;
use nirvana\infinitescroll\InfiniteScrollPager;
use frontend\assets\AppAsset;

AppAsset::register($this);

/** @var BaseDataProvider $provider */

?>

<div class="content-page container">
    <div class="page-inner">
        <h3><?= Yii::t('main', 'Feedback') ?></h3>
        <?=
        ListView::widget([
            'dataProvider' => $provider,
            'options' => [
                'id' => 'feedback-container',
                'class' => 'feedback-container row',
            ],
            'summary' => '',
            'emptyText' => Yii::t('main', 'Nothing found'),
            'itemOptions' => [
                'tag' => false,
            ],
            'itemView' => function ($model, $key, $index, $widget) {
                return $this->render('_item', ['model' => $model]);
            },
            'pager' => [
                'class' => InfiniteScrollPager::className(),
                'widgetId' => 'feedback-container',
                'itemsCssClass' => 'feedback-container',
                'contentLoadedCallback' => 'void(0)',
                'nextPageLabel' => Yii::t('main', 'Load more'),
                'linkOptions' => [
                    'class' => 'btn btn-lg btn-block',
                ],
                'pluginOptions' => [
                    'contentSelector' => '.feedback-container',
                    'loading' => [
                        'msgText' => Yii::t('main', 'Loading...'),
                        'finishedMsg' => Yii::t('main', 'No more feedback results'),
                    ],
                ],
            ],
        ]);
        ?>
    </div>
</div>