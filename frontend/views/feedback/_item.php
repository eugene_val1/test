<?php

use yii\helpers\Url;
use common\models\User;
use common\models\Therapist;

?>

<div class="review-item-wrapper" id="review-item-<?= $model->id ?>">
    <div class="review-item">
        <div class="col-sm-2 review-item-name">
            <?php if( $model->profile): ?>
                <a href="<?= Url::toRoute([($model->profile->type == Therapist::TYPE) ? 'therapists/view' : 'user/profile', 'alias' => $model->profile->alias]); ?>">
                    <?= $model->authorName ?>
                </a>
            <?php else: ?>
                <a><?= $model->authorName ?></a>
            <?php endif; ?>
        </div>
        <div class="col-sm-10 review-item-text"><?= Yii::$app->formatter->asNtext($model->text) ?></div>
        <?php if(!Yii::$app->user->isGuest && ($model->user_id == Yii::$app->user->id || Yii::$app->user->identity->role == User::ROLE_ADMIN)): ?>
        <a class="feedback-update-btn btn btn-default" data-target="#modal" href="<?= Url::toRoute(['post', 'id' => $model->id]); ?>">
            <i class="fa fa-pencil"></i>
        </a>
        <?php endif; ?>
    </div>
    <hr>
</div>
