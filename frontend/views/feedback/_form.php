<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use jlorente\remainingcharacters\RemainingCharacters;

Yii::$app->assetManager->bundles['yii\bootstrap\BootstrapAsset'] = false;
Yii::$app->assetManager->bundles['yii\web\JqueryAsset'] = false;
Yii::$app->assetManager->bundles['yii\web\YiiAsset'] = false;
?>
<h3><?= Yii::t('main', 'Post feedback') ?></h3>
<div class="row publish-block">  
    <div class="col-lg-12 form-wrapper">
        <div class="help-block">
            <?= ($model->isNewRecord || $model->user_id == Yii::$app->user->id) ? Yii::t('main', 'Feedback description text') : Yii::t('main', 'Update this feedback review as admin'); ?>
        </div>
        <?php $form = ActiveForm::begin(['id' => 'feedback-form-modal', 
            'options' => [
                'data-callback' => 'afterUpdateReview',
                'data-id' => $model->isNewRecord ? '' : $model->id,
                'data-editor' => ($model->user_id == Yii::$app->user->id) ? 'owner' : 'admin'
            ]]); ?>
        <?=
        $form->field($model, 'text', ['template' => '{input}{error}'])->widget(RemainingCharacters::classname(), [
            'type' => RemainingCharacters::INPUT_TEXTAREA,
            'label' => [
                'tag' => 'span',
                'id' => 'treat-counter',
                'class' => 'textarea-counter',
                'invalidClass' => 'error'
            ],
            'options' => [
                'rows' => '5',
                'class' => 'col-md-12 form-control',
                'maxlength' => 256,
                'placeholder' => Yii::t('main', 'Give a feedback on the project')
            ]
        ]);
        ?>
        <div class="form-group text-center">
            <?= Html::submitButton(Yii::t('main', 'Publish'), ['class' => 'small-page-btn page-btn btn-submit', 'name' => 'feedback-button']) ?>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>