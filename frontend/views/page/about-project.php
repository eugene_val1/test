<?php

/* @var $this \yii\web\View; */

use frontend\assets\AboutProjectAsset;
AboutProjectAsset::register($this);

$page->setMetaTags();
$this->params['navOpacity'] = true;

?>

<div class="page-cover" <?= ($page && $page->image) ? 'style="background-image: url(' . $page->getFileUrl('image') . ')"' : ''; ?>>
    <div class="container">
        <div class="custom-page-header">
            <h1><?= $page->name ?></h1>
        </div>
    </div>
</div>
<div class="page-container about-project">
    <div class="container">
        <div class="col-lg-10 col-lg-offset-1 page-inner">
            <?= $page->content ?>

            <!-- Our team -->
            <div id="our-team">
                <p>Команда</p>
                <div class="col-md-12 row-1 flex-row">
	                <div class="flex-col col-md-3 col-sm-6 col-xs-6 team-member">
		                <img src="/img/team/Anton_00000.png"/>
		                <div>
			                <p>Anton Fedorets</p>
			                <p class="team-position">Co-founder</p>
		                </div>
	                </div>
                    <div class="flex-col col-md-3 col-sm-6 col-xs-6 team-member illia">
                        <img src="/img/team/Illia_00000.png"/>
                        <div>
                            <p>Illia Poludonnyi</p>
                            <p class="team-position">Co-founder</p>
                        </div>
                    </div>
                    <div class="flex-col col-md-3 col-sm-6 col-xs-6 team-member">
                        <img src="/img/team/Vova_00000.png"/>
                        <div>
                            <p>Volodymyr Tochytskyi</p>
                            <p class="team-position">Developer</p>
                        </div>
                    </div>
                    <div class="flex-col col-md-3 col-sm-6 col-xs-6 team-member">
                        <img src="/img/team/Max_00000.png"/>
                        <div>
                            <p>Max Popov</p>
                            <p class="team-position">Developer</p>
                        </div>
                    </div>
                </div>

                <div class="col-md-12 row-2 flex-row">
                    <div class="flex-col col-md-3 col-sm-6 col-xs-6 team-member">
                        <img src="/img/team/Cat_00000.png"/>
                        <div>
                            <p>Bugental The Cat</p>
                            <p class="team-position">Editor in chief</p>
                        </div>
                    </div>
                    <div class="flex-col col-md-3 col-sm-6 col-xs-6 team-member">
                        <img src="/img/team/Alena_00000.png"/>
                        <div>
                            <p>Olena Gapak</p>
                            <p class="team-position">Editor</p>
                        </div>
                    </div>
                    <div class="flex-col col-md-3 col-sm-6 col-xs-6 team-member">
                        <img src="/img/team/Anna_00000.png"/>
                        <div>
                            <p>Anna Ishenko</p>
                            <p class="team-position">Lawyer</p>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <a href="/therapists" class="mobile-fixed-btn hide-desktop">Выбрать терапевта</a>

</div>



