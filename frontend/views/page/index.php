<?php

use frontend\assets\AppAsset;
AppAsset::register($this);

$page->setMetaTags();
$this->params['navOpacity'] = true;
?>

<div class="page-cover" <?= ($page && $page->image) ? 'style="background-image: url(' . $page->getFileUrl('image') . ')"' : ''; ?>>
    <div class="container">
        <div class="custom-page-header">
            <h1><?= $page->name ?></h1>
        </div>
    </div>
</div>
<div class="page-container">
    <div class="container">
        <div class="col-lg-10 col-lg-offset-1 page-inner article-container how-to-choose">
	        <div class="article-inner">
		        <?= $page->content ?>
	        </div>
        </div>
    </div>

    <a href="/therapists" class="mobile-fixed-btn hide-desktop">Выбрать терапевта</a>
</div>