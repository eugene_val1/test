<?php

use yii\web\JqueryAsset;
use common\models\Page;
use frontend\assets\AppAsset;
use yii\web\View;

AppAsset::register($this);

/** @var $this View; */
/** @var Page $page */

$this->registerJsFile('/js/how-it-works.js', [
    'position' => View::POS_END,
    'depends' => JqueryAsset::class,
]);

$page->setMetaTags();
$this->params['navOpacity'] = true;

?>

<div class="page-cover how-it-works-cover" <?= ($page && $page->image) ? 'style="background-image: url(' . $page->getFileUrl('image') . ')"' : ''; ?>>
    <div class="container">
        <div class="custom-page-header">
            <h1>Как пользоваться <br>
                платформой Treat<span>Field</span></h1>
        </div>
    </div>
</div>

<div class="page-container how-it-works">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="step-content">
                    <div class="step-number">1</div>
                    <div class="step-header">Выбрать терапевта</div>
                    <div class="step-text">
                        <ul>
                            <li>Зарегистрируйтесь и зайдите на страницу Терапевты.</li>
                            <li>На сайте нет рейтинговой системы, поэтому список формируется в случайном порядке.</li>
                            <li>Используйте <a data-target="#modal" href="/therapists/filter">фильтр</a> по возрасту, полу и ключевым темам, чтобы упростить поиск.</li>
                        </ul>
                    </div>
                    <div class="step-accordeon">
                        <div class="acc-element">
                            <div class="acc-header">
                                <span class="header-name">Как Тритфилд подбирает специалистов?</span>
                                <span class="arrow"></span>
                            </div>
                            <div class="acc-content">
                                <p>На сайте нет открытой регистрации для терапевтов. Это значит, что на платформу попадают только те специалисты, в которых мы абсолютно уверены. Все терапевты Тритфилд имеют профильное образование, прошли дополнительное обучение в конкретном направлении психотерапии и имеют минимум 5 лет опыта консультирования.</p>
                            </div>
                        </div>
                        <div class="acc-element">
                            <div class="acc-header">
                                <span class="header-name">Как мне выбрать терапевта?</span>
                                <span class="arrow"></span>
                            </div>
                            <div class="acc-content">
                                <p>На странице каждого терапевта есть краткая биография и видеовизитка (несколько визиток еще в процессе записи), чтобы вы могли больше понять о терапевте. Обратите внимание на основные темы, с которыми работает психотерапевт, просмотрите статьи и видео специалиста. Не исключено, что кто-то из специалистов сразу вам понравится. Поскольку мы гарантируем квалификацию терапевтов и вам не надо сравнивать их образование и опыт, интуиция - вполне достойный инструмент для выбора.
                                </p>
                            </div>
                        </div>
                        <div class="acc-element">
                            <div class="acc-header">
                                <span class="header-name">Что делать, если я не могу выбрать?</span>
                                <span class="arrow"></span>
                            </div>
                            <div class="acc-content">
                                <p>Если выбрать никак не получается, мы проведем для вас бесплатную 30-минутную видеоконсультацию, где ответим на вопросы о психотерапии и поможем выбрать терапевта под ваш запрос. Напишите нам на <a href="mailto:<?= Yii::$app->params['supportEmail'] ?>"><?= Yii::$app->params['supportEmail'] ?></a>, чтобы записаться и договориться о времени.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 hidden-sm hidden-xs">
                <div class="step-images">
                    <img src="/img/step1.png" alt="Зарегистрируйтесь и зайдите на страницу Терапевты">
                    <img src="/img/step11.png" alt="Используйте фильтр по возрасту, полу и ключевым темам, чтобы упростить поиск">
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="step-content">
                    <div class="step-number">2</div>
                    <div class="step-header">Запишитесь на консультацию</div>
                    <div class="step-text">
                        <ul>
                            <li>На странице выбранного терапевта нажмите кнопку "Записаться к терапевту".</li>
                            <li>Кнопка "Записаться к терапевту" откроет окно записи. Кратко опишите свой запрос и удобное для вас время. Если остаются вопросы об условиях работы — спрашивайте.</li>
                            <li>Договоритесь о времени первой консультации и терапевт сам внесет его в систему. Время будет отображаться в вашем личном кабинете в разделе Терапия.</li>
                        </ul>
                    </div>
                    <div class="step-accordeon">
                        <div class="acc-element">
                            <div class="acc-header">
                                <span class="header-name">Пока не готов записываться, хочу больше понять про психотерапию</span>
                                <span class="arrow"></span>
                            </div>
                            <div class="acc-content">
                                <p>Если у вас остаются вопросы о формате работы, вы можете задать их понравившемуся психотерапевту в личных сообщениях или написать нам на
                                    <a href="mailto:<?= Yii::$app->params['supportEmail'] ?>"><?= Yii::$app->params['supportEmail'] ?></a>. На многие популярные вопросы о психотерапии мы отвечаем на нашей странице Facebook
                                    <a href="<?= Yii::$app->params['mediaLinks']['facebook'] ?>">вот в этом посте</a>, можете задать свой вопрос там.</p>
                            </div>
                        </div>
                        <div class="acc-element">
                            <div class="acc-header">
                                <span class="header-name">Хочу записаться, но не могу сформулировать проблему</span>
                                <span class="arrow"></span>
                            </div>
                            <div class="acc-content">
                                <p>Это нормально: “неправильных” запросов не бывает. Вам не обязательно описывать проблему со всей четкостью. Если вы просто недовольны своей жизнью или чувствуете, что что-то не так с вашим состоянием и настроением - этого достаточно, чтобы начать работу.</p>
                                <p>Разбираться в причинах и взаимосвязях, пытаться лучше понять свои чувства - важная часть психотерапии.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 hidden-sm hidden-xs">
                <div class="step-images">
                    <img src="/img/step2.png" alt="На странице выбранного терапевта нажмите кнопку Записаться к терапевту">
                    <img src="/img/step22.png" alt="Договоритесь о времени первой консультации и терапевт сам внесет его в систему. Время будет отображаться в вашем личном кабинете в разделе Терапия">
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="step-content">
                    <div class="step-number">3</div>
                    <div class="step-header">Оплатите сессию</div>
                    <div class="step-text">
                        <ul>
                            <li>Рядом с назначенной сессией в разделе Терапия появится кнопка "Оплатить". Такая же кнопка появится на панели верхнего меню.</li>
                            <li>Сессию можно оплатить картой Visa или Mastercard. Платежная система интегрирована в сайт.</li>
                            <li>Средства не списываются с карты в момент предоплаты, а только холдируются. Оплата завершится после окончания консультации.</li>
                        </ul>
                    </div>
                    <div class="step-accordeon">
                        <div class="acc-element">
                            <div class="acc-header">
                                <span class="header-name">Как работает система оплаты?</span>
                                <span class="arrow"></span>
                            </div>
                            <div class="acc-content">
                                <p>Самый первый платеж может обрабатываться несколько часов, поэтому постарайтесь не оплачивать сессию в последний момент. Когда вы вносите оплату, средства не списываются с карты окончательно, а “замораживаются” в системе. Если сессия состоится (или вы пропустите ее без предупреждения) - деньги спишутся. Если сессия не состоится по вине терапевта - мы вернем средства. </p>
                                <p>Начиная со второй сессии, вы сможете оплачивать несколько сессий сразу.</p>
                            </div>
                        </div>
                        <div class="acc-element">
                            <div class="acc-header">
                                <span class="header-name">Насколько безопасна система оплаты?</span>
                                <span class="arrow"></span>
                            </div>
                            <div class="acc-content">
                                <p>Платформа пользуется услугами проверенного платежного сервиса и не имеет доступа к реквизитам вашей карты. Подробнее о нашем платежном решении можно прочитать <a href="/page/payment">здесь</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 hidden-sm hidden-xs">
                <div class="step-images">
                    <img src="/img/step3.png" alt="Рядом с назначенной сессией в разделе Терапия появится кнопка Оплатить">
                    <img src="/img/step33.png" alt="Сессию можно оплатить картой Visa или Mastercard. Платежная система интегрирована в сайт">
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="step-content">
                    <div class="step-number">4</div>
                    <div class="step-header">Войдите в онлайн-комнату</div>
                    <div class="step-text">
                        <ul>
                            <li>В назначенное время войдите в онлайн-комнату.</li>
                            <li>Браузер попросит доступ к камере и микрофону компьютера, разрешите их (настройки доступа можно вызвать еще раз, нажав на иконку замочка в адресной строке).</li>
                            <li>Онлайн-комната будет работать с компьютера или с мобильных устройств на Аndroid (рекомендуем использовать браузер Chrome). Для работы на iPhone и iPad скачайте наше приложение в
                                <a href="<?= Yii::$app->params['appStoreLink'] ?>" target="_blank">AppStore</a>.
                            </li>
                        </ul>
                    </div>
                    <div class="step-accordeon">
                        <div class="acc-element">
                            <div class="acc-header">
                                <span class="header-name">Как войти в онлайн-комнату с мобильного?</span>
                                <span class="arrow"></span>
                            </div>
                            <div class="acc-content">
                                <p>Чтобы работать с устройства на Android, используйте браузер Chrome. Для работы на iOS устройствах есть специальное приложение, но оно создано только для видеосвязи. Это значит, что записываться на консультацию и оплачивать ее нужно из браузера.</p>
                                <p>Чтобы войти в онлайн-комнату на iPhone или iPad, авторизуйтесь в приложении, откройте список сессий и нажмите кнопку онлайн-комнаты. Если сессия назначена и оплачена, кнопка будет активна.</p>
                            </div>
                        </div>
                        <div class="acc-element">
                            <div class="acc-header">
                                <span class="header-name">Насколько безопасна видеосвязь?</span>
                                <span class="arrow"></span>
                            </div>
                            <div class="acc-content">
                                <p>Для видеосвязи Тритфилд использует технологию, которая обеспечивает сквозное шифрование между узлами по принципу peer-to-peer соединения. Это значит, что ваш браузер или телефон связывается с браузером или телефоном терапевта напрямую, видео- и аудиоданные не проходят через сервера-посредники, их нельзя перехватить или подслушать.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 hidden-sm hidden-xs">
                <div class="step-images">
                    <img src="/img/step4.png" alt="В назначенное время войдите в онлайн-комнату">
                    <img src="/img/step44.png" alt="Браузер попросит доступ к камере и микрофону компьютера, разрешите их">
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="next-meets">
                    <p>О следующих встречах можно договориться в конце сессии или написав сообщение терапевту.</p>
                    <img src="/img/next-meet.png" alt="О следующих встречах можно договориться в конце сессии или написав сообщение терапевту">
                    <p>Остались вопросы? Напишите нам — <a href="mailto:<?= Yii::$app->params['supportEmail'] ?>"><?= Yii::$app->params['supportEmail'] ?></a></p>
                    <div class="btn-container">
                        <a href="/therapists" class="btn-link mobile-fixed-btn">Выбрать терапевта</a>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>





