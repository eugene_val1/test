<?php

use frontend\assets\AppAsset;
AppAsset::register($this);

$this->title = Yii::t('main', 'Therapist reviews');
$this->params['navOpacity'] = true;

/** @var array $therapistReviews */
/** @var array $reviews */

Yii::$app->view->registerMetaTag([
    'name' => 'keywords',
    'content' => '',
], 'keywords');

Yii::$app->view->registerMetaTag([
    'name' => 'description',
    'content' => '',
], 'description');

?>

<style>
    .author-image img {
        width: 100%;
    }
</style>

<div class="page-cover">
    <div class="container">
        <div class="custom-page-header">
            <h1><?= $this->title ?></h1>
        </div>
    </div>
</div>

<div class="page-container">
    <div class="container">
        <div class="col-lg-10 col-lg-offset-1 page-inner">
            <?php foreach($therapistReviews as $therapistId => $reviews): ?>
                <div class="col-md-12">

                    <div class="col-md-12">
                        <h4>Отзывы о <a href="<?= '/therapist/' . $reviews[0]->therapist->alias; ?>"> <?= $reviews[0]->therapist->name; ?></a></h4>
                    </div>

                    <div class="col-md-3">
                        <a href="<?= '/therapist/' . $reviews[0]->therapist->alias; ?>">
                            <span class="author-image">
                                <img alt="<?= $reviews[0]->therapist->name;?>" src="<?= $reviews[0]->therapist->getFileUrl('main_image'); ?>">
                            </span>
                        </a>
                    </div>

                    <div class="col-md-9">
                        <?php foreach($reviews as $review): ?>
                            <div class="review-item">
                                <p class="col-md-2 review-item-name">
                                    <?= Yii::$app->formatter->asNtext($review->name) ?>
                                </p>
                                <div class="col-md-10 review-item-text">
                                    <?= Yii::$app->formatter->asNtext($review->text) ?>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>

            <?php endforeach; ?>
        </div>
    </div>

    <a href="/therapists" class="mobile-fixed-btn hide-desktop">Выбрать терапевта</a>
</div>
