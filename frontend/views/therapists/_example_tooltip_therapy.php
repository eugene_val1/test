<div class="client-row">
    <div class="mob-head">
        <div class="client">
            <div class="img-wrap">
                <img src="/img/walking-logo.png" alt="">
            </div>
            <div class="name">Имя терапевта</div>
        </div>
        <div class="next-session">
            <span class="session-date">
                cреда <span>13:00</span><br>
                <span>14 марта</span>
            </span>
        </div>
    </div>
    <div class="row-content">
        <div class="client mob-hidden">
            <div class="img-wrap">
                <img src="/img/walking-logo.png" alt="">
            </div>
            <div class="name">Имя терапевта</div>
        </div>
        <div class="messages">
            <span class="desktop-hidden">Сообщения</span>
            <div class="message">
                <div class="quantity-label">1</div>
            </div>
        </div>
        <div class="next-session mob-hidden">
            <span class="session-date">
                cреда <span>13:00</span><br>
                <span>14 марта</span>
            </span>
        </div>
        <div class="payment paid">
            <span class="desktop-hidden">Оплата</span>
            <a href="" class="table-btn-orange">Оплатить</a>
        </div>
        <div class="room active">
            <span class="desktop-hidden">Онлайн-комната</span>
            <a href="" class="table-btn-rom">
                Войти
            </a>
        </div>
        <div class="history">
            <span class="desktop-hidden">История</span>
            <div class="h-head">
                <div class="h-item">#5</div>
                <i class="fa fa-angle-down" aria-hidden="true"></i>
            </div>
        </div>
    </div>

    <div class="history-content">
        <div class="flex-container">
            <div class="h-item">№2</div>
            <div class="h-item">№53</div>
            <div class="h-item">№6</div>
            <div class="h-item">№2</div>
        </div>
    </div>
</div>