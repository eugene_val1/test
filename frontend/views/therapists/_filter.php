<?php
use kartik\widgets\ActiveForm;
use yii\helpers\Html;
use common\models\Property;
use kartik\slider\Slider;
use frontend\models\TherapistFilter;
use yii\web\JsExpression;

Yii::$app->assetManager->bundles['yii\bootstrap\BootstrapAsset'] = false;
Yii::$app->assetManager->bundles['yii\web\JqueryAsset'] = false;
Yii::$app->assetManager->bundles['yii\web\YiiAsset'] = false;
?>

<div class="therapists-filter">
    <h3><?= Yii::t('main', 'Choose therapist by following criteria') ?></h3>
    <div class="form-wrapper">
        <?php $form = ActiveForm::begin(['enableClientValidation' => false]); ?>
        <div class="row">
            <div class="col-sm-12">
                <div class="therapist-properties">
                    <div>
                        <label class="control-label"><?= $filterModel->getAttributeLabel('gender'); ?></label>
                    </div>
                    <?= $form->field($filterModel, 'gender')->radioButtonGroup($filterModel->getGenders())->label(false); ?>
                    <?= $form->field($filterModel, 'ageRange')->widget(Slider::classname(), [
                        'sliderColor'=>Slider::TYPE_GREY,
                        'pluginOptions'=>[
                            'min' => TherapistFilter::AGE_FROM,
                            'max' => TherapistFilter::AGE_TO,
                            'step' => 1,
                            'range' => true,
                            'formatter' => new JsExpression('function(val){
                                if (Array.isArray(val)) {
                                    return val[0] + " - " + val[1];
				} else {
                                    return val;
				}
                            }')
                        ]
                    ]); ?>                    
                    <?php foreach(Property::getWithTypes() as $type => $items): ?>
                    <div>
                        <label class="control-label"><?= Property::getTypeLabel($type); ?></label>
                    </div>
                    <?= $form->field($filterModel, $type . 'Values', ['template' => '{input}'])->checkboxButtonGroup($items, ['unselect' => null]); ?>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group form-submit">
                    <?= Html::submitButton(Yii::t('main', 'Seacrh'), ['class' => 'small-page-btn page-btn orange btn-submit', 'name' => 'save-button']) ?>
                </div>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>