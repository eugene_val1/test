<?php

use common\models\TherapistReview;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/** @var TherapistReview $model */

?>

<div class="review-form">
    <?php $form = ActiveForm::begin([
        'layout' => 'horizontal',
        'id' => 'contact-form',
        'action' => ['therapists/review', 'id' => $therapist->user_id],
        'options' => ['class' => 'form-horizontal'],
        'fieldConfig' => [
            'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
            'horizontalCssClasses' => [
                'label' => 'col-sm-2',
                'wrapper' => 'col-sm-10',
                'error' => '',
                'hint' => '',
            ],
        ],
    ]); ?>
    <?= Yii::$app->user->isGuest ? $form->field($model, 'name')->textInput() : '' ?>
    <?= $form->field($model, 'text')->textarea([
            'class' => 'form-control',
            'rows' => '3',
            'id' => 'message-text'
    ]); ?>

    <?php if (Yii::$app->user->isGuest) ?>
        <div class="captcha-block">
            <?= $form->field($model, 'cptch', ['enableAjaxValidation' => false])
                ->widget(\yii\captcha\Captcha::className(), [])
            ?>
        </div>
    <?php ?>

    <div class="form-group form-submit">
        <div class="col-sm-offset-2 col-sm-10">
            <?= Html::submitButton(Yii::t('main', 'Send'), [
                'class' => 'small-page-btn page-btn orange btn-submit',
                'name' => 'review-button',
            ]) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>