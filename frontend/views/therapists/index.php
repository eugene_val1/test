<?php

use yii\helpers\Url;
use yii\widgets\ListView;
use nirvana\infinitescroll\InfiniteScrollPager;
use frontend\assets\AppAsset;

AppAsset::register($this);

?>

<div class="therapists-page container-fluid">
    <div class="page-inner">
        <a class="menu-btn"></a>
        <div class="page-container therapists">
            <?=
                ListView::widget([
                    'dataProvider' => $dataProvider,
                    'options' => [
                        'id' => 'therapists-container',
                        'class' => 'therapists-container row',
                    ],
                    'summary' => '',
                    'emptyText' => Yii::t('main', 'No therapists yet.'),
                    'itemOptions' => [
                        'tag' => false,
                    ],
                    'itemView' => function ($model, $key, $index, $widget){
                        return $this->render('_therapist', ['therapist' => $model, 'index' => $index]);
                    },
                    'pager' => [
                        'class' => InfiniteScrollPager::className(),
                        'widgetId' => 'therapists-container',
                        'itemsCssClass' => 'therapists-container',
                        'contentLoadedCallback' => 'void(0)',
                        'nextPageLabel' => Yii::t('main', 'Load more'),
                        'linkOptions' => [
                            'class' => 'btn btn-lg btn-block',
                        ],
                        'pluginOptions' => [
                            'contentSelector' => '.therapists-container',
                            'loading' => [
                                'msgText' => Yii::t('main', 'Loading...'),
                                'finishedMsg' => Yii::t('main', 'No more therapists to load'),
                            ],
                        ],
                    ],
                ]);
            ?>
        </div>
    </div>
</div>
<aside class="side-nav side-menu auto-open">
    <div class="nav-inner simple-side-menu">
        <a class="menu-close-btn"></a>
        <ul class="filter">
            <li><a data-target="#modal" href="<?= Yii::$app->settingManager->get('therapistChoosePage'); ?>"><?= Yii::t('main', 'How to choose therapist'); ?></a></li>
            <li><a data-target="#modal" href="<?= Url::toRoute('therapists/random') ?>"><?= Yii::t('main', 'Trust to field'); ?></a></li>
            <li><a data-target="#modal" href="<?= Url::toRoute($filterModel->getUrl('therapists/filter')) ?>"><?= Yii::t('main', 'Filter'); ?></a></li>
            <li><a data-target="#modal" href="<?= Url::toRoute(['contact/index', 'request' => true]) ?>"><?= Yii::t('main', 'Request therapist'); ?></a></li>
        </ul>
    </div>
    <div class="nav-footer"></div>
</aside>

<!-- Begin MailChimp Signup Form -->
<?= $this->render('/site/_mailchipm_absolute'); ?>
<!--End mc_embed_signup-->