<?php $this->title = $this->title . ' | ' . $therapist->name . ' | ' . Yii::t('main', 'Terms and conditions'); ?>
<div class="content-page container-fluid">
    <div class="page-inner">
        <div class="page-container article">             
            <article class="article-content">
                <div class="article-info">
                    <div class="article-author">
                        <a href="<?= '/therapist/' . $therapist->alias; ?>">
                            <span class="author-image">
                                <img alt="<?= $therapist->name;?>" src="<?= $therapist->getFileUrl('main_image'); ?>">
                            </span>
                            <h3 class="author-name"><?= $therapist->name; ?></h3>
                        </a>
                    </div>                  
                </div>
                <div class="article-inner">
                    <h1><?= Yii::t('main', 'Terms and conditions') ?></h1>
                    <?= $therapist->terms ?>
                </div>
            </article>
        </div>
    </div>
</div>
