<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use common\models\Appointment;
?>
<h3><?= Yii::t('main', 'Complete therapy') ?></h3>
<div class="completed">
    <div class="row">            
        <div class="col-sm-12 form-wrapper">
            <?php if($model->client->nextAppointment->payment_status == Appointment::PAID): ?>
                <div class="alert alert-danger" role="alert"><?= Yii::t('main', 'Your next appointment is paid') ?></div>
            <?php endif; ?>
            <p class="reason-description"><?= Yii::t('main', 'Complete therapy reason description') ?></p>
            <?php
            $form = ActiveForm::begin([
                'enableAjaxValidation' => false,
                'enableClientValidation' => true,
                'id' => 'complete-therapy-form', 
                    'options' => [
                    'data-callback' => 'afterCompleteTherapy'
                ]
            ]);
            ?>
            <?= $form->field($model, 'reason')->textArea(['class' => "form-control", 'rows' => "3", 'id' => 'message-text']); ?>
            <?= $form->field($model, 'sendCopy')->checkbox(); ?>
            <div class="form-group form-submit">
                <?= Html::submitButton(Yii::t('main', 'Send'), ['class' => 'small-page-btn page-btn orange pull-right btn-submit']); ?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>  
    </div>
</div>