<?php

use common\models\Timezone;
use yii\data\Pagination;
use yii\helpers\Url;
use yii\web\YiiAsset;
use yii\web\JqueryAsset;
use yii\bootstrap\BootstrapAsset;
use common\models\Appointment;
use common\models\Client;
use common\models\PaidAppointments;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\LinkPager;
use yii\widgets\Pjax;

/** @var Client[] $models */
/** @var Pagination $pages */

$this->registerJsFile('https://secure.wayforpay.com/server/pay-widget.js', ['position' => View::POS_HEAD]);
Yii::$app->assetManager->bundles[BootstrapAsset::class] = false;
Yii::$app->assetManager->bundles[JqueryAsset::class] = false;
Yii::$app->assetManager->bundles[YiiAsset::class] = false;
$this->registerJs("$('[data-toggle=\"popover\"]').popover({'trigger': 'hover'})");

/**
 * @param int $date
 *
 * @return string
 */
function formatAppointmentDate($date) {
    return Yii::$app->formatter->asDate($date, 'php:l') . '</br>' .
        '<span>' . Timezone::convert($date, Yii::$app->formatter->timeZone, 'd') . ' '
        .  Yii::$app->formatter->asDate($date, 'php:F') . '</span>' . '</br>' .
        '<span>' . Timezone::convert($date, Yii::$app->formatter->timeZone, 'H:i') . '</span>';
}

?>

<div class="table-head">
    <div class="client">
        <span data-toggle="popover" data-placement="bottom" data-content="<?= Yii::t('main', 'Therapist column for therapy table description') ?>">Терапевт</span>
    </div>
    <div class="messages">Сообщения</div>
    <div class="next-session">Следущая сессия</div>
    <div class="payment">
        <span data-toggle="popover" data-placement="bottom" data-content="<?= Yii::t('main', 'Payment column for therapy table description') ?>">Оплата</span>
    </div>
    <div class="room">
        <span data-toggle="popover" data-placement="bottom" data-content="<?= Yii::t('main', 'Video room column for therapy table description') ?>">Онлайн комната</span>
    </div>
    <div class="history">Прошлая сессия</div>
</div>

<div class="table-body">

<?php if (Yii::$app->tooltip->showExampleAppointment()): ?>
    <?= $this->render('_example_tooltip_therapy'); ?>
<?php elseif (empty($models)): ?>
    <?= $this->render('_example_empty_therapy'); ?>
<?php else: ?>

    <?php Pjax::begin(['id' => 'therapy']) ?>
    <?php foreach ($models as $model): ?>

        <?php /** @var Client $model */ ?>
        <div class="client-row">

            <div class="mob-head">
                <div class="client">
                    <div class="dropdown">
                        <span class="therapist-name dropdown-toggle" id="therapist-<?= $model->therapist->id ?>" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                            <div class="img-wrap">
                                <img src="<?= $model->therapist->getFileUrl('image') ?>" alt="<?= $model->therapist->name ?>">
                            </div>
                        </span>
                        <ul class="dropdown-menu" aria-labelledby="therapist-<?= $model->therapist->id ?>">
                            <li><?= Html::a('<i class="fa fa-link"></i> ' . Yii::t('main', 'Therapist page'), ['/therapist/' . $model->therapist->alias], ['data-pjax' => 0]) ?></li>
                            <?php if ($model->complete_status == Client::UNCOMPLETED): ?>
                                <li><?= Html::a('<i class="fa fa-close"></i> ' . Yii::t('main', 'Complete therapy'), ['complete-therapy', 'id' => $model->id], ['data-pjax' => 0, 'data-target' => '#modal']) ?></li>
                            <?php endif; ?>
                        </ul>
                    </div>

                    <div class="name" data-toggle="popover" data-placement="bottom" data-content="<?= $model->therapist->name ?>">
                        <a href="/therapist/<?= $model->therapist->alias ?>" id="therapist-<?= $model->therapist->id ?>">
                            <?= $model->therapist->name ?>
                        </a>
                    </div>
                </div>
                <div class="next-session">
                    <span class="session-date">
                        <?php if ($model->complete_status == Client::COMPLETED): ?>
                            <?= Yii::t('main', 'Therapy has been completed'); ?>
                        <?php else: ?>
                            <?php
                                echo $model->next_appointment_start !== Client::NO_NEXT_APPPOINTMENT && $model->nextAppointment
                                    ? formatAppointmentDate($model->next_appointment_start)
                                    : Yii::t('main', 'Check with the therapist the date and time of next appointment');
                            ?>
                        <?php endif; ?>
                    </span>
                </div>
            </div>

            <div class="row-content">
                <div class="client mob-hidden">
                    <div class="dropdown">
                        <span class="therapist-name dropdown-toggle" id="therapist-<?= $model->therapist->id ?>" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                            <div class="img-wrap">
                                <img src="<?= $model->therapist->getFileUrl('image') ?>" alt="<?= $model->therapist->name ?>">
                            </div>
                        </span>
                        <ul class="dropdown-menu" aria-labelledby="therapist-<?= $model->therapist->id ?>">
                            <li><?= Html::a('<i class="fa fa-link"></i> ' . Yii::t('main', 'Therapist page'), ['/therapist/' . $model->therapist->alias], ['data-pjax' => 0]) ?></li>
                            <?php if ($model->complete_status == Client::UNCOMPLETED): ?>
                                <li><?= Html::a('<i class="fa fa-close"></i> ' . Yii::t('main', 'Complete therapy'), ['complete-therapy', 'id' => $model->id], ['data-pjax' => 0, 'data-target' => '#modal']) ?></li>
                            <?php endif; ?>
                        </ul>
                    </div>
                    <div class="name" data-toggle="popover" data-placement="bottom" data-content="<?= $model->therapist->name ?>">
                        <a href="/therapist/<?= $model->therapist->alias ?>">
                            <?= $model->therapist->name ?>
                        </a>
                    </div>
                </div>

                <?php $messagesCount = count($model->newTherapistMessages); ?>
                <div class="messages">
                    <span class="desktop-hidden">Сообщения</span>
                    <a href="<?= Url::to(['messages/conversation', 'id' => $model->conversation_id]) ?>">
                        <div class="message">
                            <?php if($messagesCount): ?>
                                <div class="quantity-label"><?= $messagesCount ?></div>
                            <?php endif; ?>
                        </div>
                    </a>
                </div>

                <div class="next-session mob-hidden">
                    <span class="session-date">
                        <?php if ($model->complete_status == Client::COMPLETED): ?>
                            <?= Yii::t('main', 'Therapy has been completed'); ?>
                        <?php else: ?>
                            <?php
                            echo $model->next_appointment_start !== Client::NO_NEXT_APPPOINTMENT && $model->nextAppointment
                                ? formatAppointmentDate($model->next_appointment_start)
                                : Yii::t('main', 'Check the date and time of next appointment');
                            ?>
                        <?php endif; ?>
                    </span>
                </div>

                <?php $isPaid = $model->nextAppointment && $model->nextAppointment->payment_status == Appointment::PAID; ?>
                <div class="payment <?= $isPaid ? 'paid' : '' ?>">
                    <span class="desktop-hidden">Оплата</span>
                    <?php
                        echo (function($model) {
                            /** @var Client $model */
                            $buttonOptions = [
                                'class' => 'table-btn-orange',
                                'data-target' => '#modal',
                                'data-pjax' => 0,
                            ];

                            if ($model->next_appointment_start == Client::NO_NEXT_APPPOINTMENT
                                || $model->complete_status == Client::COMPLETED
                            ) {
                                $buttonOptions['class'] = 'table-btn-grey disabled';
                            }

                            $paidAppointments = PaidAppointments::getForUser($model->user_id);
                            if ($paidAppointments > 0) {
                                $paidAppointments = ' (' . ($paidAppointments + 1) . ')';
                            } else {
                                $paidAppointments = '';
                            }

                            $html = Html::a(
                                Yii::t('main', 'Pay'),
                                null,
                                $buttonOptions
                            );

                            if ($model->complete_status == Client::COMPLETED) {
                                if ($model->nextAppointment && $model->nextAppointment->payment_status != Appointment::NOT_PAID) {
                                    return Yii::t('main', 'Paid') . $paidAppointments . ' <i class="fa fa-check"></i>';
                                }
                                return $html;
                            }

                            if ($model->nextAppointment) {
                                if ($model->nextAppointment->payment_status == Appointment::NOT_PAID) {
                                    $html = Html::a(
                                        Yii::t('main', 'Pay'),
                                        ['payment/create', 'id' => $model->nextAppointment->id],
                                        $buttonOptions
                                    );
                                } elseif ($model->nextAppointment->payment_status == Appointment::PENDING) {
                                    $html = Yii::t('main', 'In progress') . '...';
                                } else {
                                    $html = Yii::t('main', 'Paid') . $paidAppointments . ' <i class="fa fa-check"></i>';
                                }
                            }

                            return $html;
                        })($model);
                    ?>
                </div>

                <?php
                    $isActiveRoom = $model->nextAppointment
                        && $model->nextAppointment->payment_status === Appointment::PAID
                        && $model->next_appointment_start !== Client::NO_NEXT_APPPOINTMENT;
                ?>
                <div class="room <?= $isActiveRoom ? 'active' : '' ?>">
                    <span class="desktop-hidden">Онлайн-комната</span>
                    <?php echo (function($model) use ($isActiveRoom) {
                        $html = '<span href="" class="table-btn-rom">Не активна</span>';

                        if ($isActiveRoom) {
                            return Html::a(
                                'Войти',
                                ['appointment/video', 'id' => $model->nextAppointment->id],
                                ['class' => 'table-btn-rom', 'data-pjax' => 0]
                            );
                        }

                        return $html;
                    })($model) ?>
                </div>

                <?php $completedAppointmentsCount = count($model->completedAppointments); ?>
                <div class="history">
                    <span class="desktop-hidden">История</span>
                    <div class="h-head">
                        <?php if ($completedAppointmentsCount > 0): ?>
                            <div class="h-item empty"><?= $model->completedAppointments[0]->number ?></div>
                        <?php else: ?>
                            <div class="h-item empty">-</div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>

        </div>
    <?php endforeach; ?>

    <?php
        echo LinkPager::widget([
            'pagination' => $pages,
        ]);
    ?>

    <?php Pjax::end() ?>

<?php endif; ?>

</div>
