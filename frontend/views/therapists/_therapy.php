<?php

use yii\web\YiiAsset;
use yii\web\JqueryAsset;
use yii\bootstrap\BootstrapAsset;
use common\models\Appointment;
use common\models\Client;
use common\models\PaidAppointments;
use common\models\Timezone;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\Pjax;

/* @var \yii\data\BaseDataProvider $dataProvider */
/* @var Client $model */

$this->registerJsFile('https://secure.wayforpay.com/server/pay-widget.js', ['position' => View::POS_HEAD]);
Yii::$app->assetManager->bundles[BootstrapAsset::class] = false;
Yii::$app->assetManager->bundles[JqueryAsset::class] = false;
Yii::$app->assetManager->bundles[YiiAsset::class] = false;
$this->registerJs("$('[data-toggle=\"popover\"]').popover({'trigger': 'hover'})");

?>

<?php if (Yii::$app->tooltip->showExampleAppointment()): ?>
    <?= $this->render('_example_tooltip_therapy'); ?>
<?php else: ?>

    <?php Pjax::begin([
            'id' => 'pjax-therapy',
            'enablePushState' => false]
    ); ?>

    <?= GridView::widget([
        'id' => 'grid-therapy',
        'dataProvider' => $dataProvider,
        'tableOptions' => ['class' => 'table table-striped table-advance table-hover responsive'],
        'summary' => '',
        'rowOptions' => function ($model) {
            if ($model->therapist->isDeleted()) {
                return ['class' => 'danger'];
            }
        },
        'columns' => [
            [
                'class' => 'yii\grid\SerialColumn',
                'contentOptions' => [
                    'id' => 'counter',
                ]
            ],
            [
                'attribute' => 'user_name',
                'format' => 'raw',
                'headerOptions' => ['class' => 'desktop_hidden'],
                'contentOptions' => function ($model) {
                    return [
                        'data-label' => $model->therapist->name,
                        'class' => 'desktop_hidden user_appoint',
                        'onclick' => 'void(0);',
                    ];
                },
                'value' => function ($model) {
                    if ($model->complete_status == Client::COMPLETED) {
                        $result = Yii::t('main', 'Therapy has been completed');
                    } else {
                        $result = ($model->next_appointment_start !== Client::NO_NEXT_APPPOINTMENT && $model->nextAppointment)
                            ? '#' . $model->nextAppointment->number . ' ' . Yii::$app->formatter->asDate($model->next_appointment_start, 'medium') . ' ' . Yii::$app->formatter->asTime($model->next_appointment_start, 'short')
                            : Yii::t('main', 'Check with the therapist the date and time of next appointment');
                    }

                    $result .= '<ul aria-labelledby="therapist-' . $model->therapist->id . '"><li>' . Html::a('<i class="fa fa-link"></i> ' . Yii::t('main', 'Therapist page'), ['/therapist/' . $model->therapist->alias], ['data-pjax' => 0]) . '</li></ul>';

                    return $result;
                },
            ],
            /*[
                'attribute' => 'therapist',
                'headerOptions' => ['class' => 'desktop_hidden'],
                'header' => Yii::t('main', 'Therapist') . ' <span data-toggle="popover" data-placement="bottom" data-content="' . Yii::t('main', 'Therapist column for therapy table description') . '"><i class="fa fa-question-circle" aria-hidden="true"></i></span>',
                'format' => 'raw',
                'contentOptions' => ['class' => 'user_appoint desktop_hidden', 'data-label' => Yii::t('main', 'Therapist')],
                'value' => function ($model) {
                    return '<ul aria-labelledby="therapist-' . $model->therapist->id . '">
                                          <li>' . Html::a('<i class="fa fa-link"></i> ' . Yii::t('main', 'Therapist page'), ['/therapist/' . $model->therapist->alias], ['data-pjax' => 0]) . '</li>' .
                        ($model->complete_status == Client::UNCOMPLETED ? '<li>' . Html::a('<i class="fa fa-close"></i> ' . Yii::t('main', 'Complete therapy'), ['complete-therapy', 'id' => $model->id], ['data-pjax' => 0, 'data-target' => '#modal']) . '</li>' : '') .
                        '</ul>';
                },
            ],*/
            [
                'attribute' => 'therapist_id',
                'header' => Yii::t('main', 'Therapist') . ' <span data-toggle="popover" data-placement="bottom" data-content="' . Yii::t('main', 'Therapist column for therapy table description') . '"><i class="fa fa-question-circle" aria-hidden="true"></i></span>',
                'format' => 'raw',
                'contentOptions' => ['class' => 'user_appoint user_name', 'data-label' => Yii::t('main', 'Therapist')],
                'value' => function ($model) {
                    return '<div class="dropdown">
                                    <span class="therapist-name dropdown-toggle" id="therapist-' . $model->therapist->id . '" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                      ' . $model->therapist->name . '
                                    </span>
                                    <ul class="dropdown-menu" aria-labelledby="therapist-' . $model->therapist->id . '">
                                      <li>' . Html::a('<i class="fa fa-link"></i> ' . Yii::t('main', 'Therapist page'), ['/therapist/' . $model->therapist->alias], ['data-pjax' => 0]) . '</li>' .
                        ($model->complete_status == Client::UNCOMPLETED ? '<li>' . Html::a('<i class="fa fa-close"></i> ' . Yii::t('main', 'Complete therapy'), ['complete-therapy', 'id' => $model->id], ['data-pjax' => 0, 'data-target' => '#modal']) . '</li>' : '') .
                        '</ul></div>';
                },
            ],
            [
                'label' => Yii::t('main', 'Messages'),
                'format' => 'raw',
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center', 'data-label' => Yii::t('main', 'Messages')],
                'value' => function ($model) {
                    return Html::a('<i class="fa fa-envelope"></i> ' . (count($model->newTherapistMessages) ? '<span class="badge small-conversation-badge">' . count($model->newTherapistMessages) . '</span>' : ''), ['messages/conversation', 'id' => $model->conversation_id], [
                        'class' => 'btn btn-default btn-sm',
                        'data-pjax' => 0,
                    ]);
                },
            ],
            [
                'label' => Yii::t('main', 'Next appointment'),
                'format' => 'html',
                'contentOptions' => ['data-label' => Yii::t('main', 'Next appointment'), 'class' => 'appointment'],
                'value' => function ($model) {
                    if ($model->complete_status == Client::COMPLETED) {
                        return Yii::t('main', 'Therapy has been completed');
                    }

                    return ($model->next_appointment_start !== Client::NO_NEXT_APPPOINTMENT)
                        ? '#' . $model->nextAppointment->number . ' ' . Timezone::convert($model->next_appointment_start, Yii::$app->user->identity->timezone, 'd.m.Y, H:i')
                        : Yii::t('main', 'Check with the therapist the date and time of next appointment');
                },
            ],
            [
                'header' => Yii::t('main', 'Payment') . ' <span data-toggle="popover" data-placement="bottom" data-content="' . Yii::t('main', 'Payment column for therapy table description') . '"><i class="fa fa-question-circle" aria-hidden="true"></i></span>',
                'format' => 'raw',
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center', 'data-label' => Yii::t('main', 'Payment')],
                'value' => function ($model) {
                    $html = '';
                    $buttonOptions = [
                        'class' => 'btn btn-primary btn-sm',
                        'data-target' => '#modal'
                    ];
                    if ($model->next_appointment_start == Client::NO_NEXT_APPPOINTMENT) {
                        $buttonOptions = [
                            'class' => 'btn btn-sm disabled'
                        ];
                    };
                    $paidAppointments = PaidAppointments::getForUser($model->user_id);
                    if ($paidAppointments > 0) {
                        $paidAppointments = ' (' . ($paidAppointments + 1) . ')';
                    } else {
                        $paidAppointments = '';
                    }

                    if ($model->complete_status == Client::COMPLETED) {
                        if ($model->nextAppointment && $model->nextAppointment->payment_status != Appointment::NOT_PAID) {
                            return Yii::t('main', 'Paid') . $paidAppointments . ' <i class="fa fa-check"></i>';
                        }
                        return $html;
                    }

                    if ($model->nextAppointment->payment_status == Appointment::NOT_PAID) {
                        $html = Html::a(Yii::t('main', 'Pay'), ['payment/create', 'id' => $model->nextAppointment->id], $buttonOptions);
                    } else if ($model->nextAppointment->payment_status == Appointment::PENDING) {
                        $html = Yii::t('main', 'In progress') . '...';
                    } else {
                        $html = Yii::t('main', 'Paid') . $paidAppointments . ' <i class="fa fa-check"></i>';
                    }

                    return $html;
                },
            ],
            [
                'header' => Yii::t('main', 'Video chanel') . ' <span data-toggle="popover" data-placement="bottom" data-content="' . Yii::t('main', 'Video room column for therapy table description') . '"><i class="fa fa-question-circle" aria-hidden="true"></i></span>',
                'format' => 'raw',
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center', 'data-label' => Yii::t('main', 'Video chanel')],
                'value' => function ($model) {
                    $html = '';

                    if ($model->complete_status == Client::COMPLETED) {
                        return $html;
                    }

                    if ($model->nextAppointment->payment_status === Appointment::PAID && $model->next_appointment_start !== Client::NO_NEXT_APPPOINTMENT) {
                        $html = Html::a('<i class="fa fa-video-camera"></i>', ['appointment/video', 'id' => $model->nextAppointment->id], [
                            'class' => 'page-btn action-page-btn video-room-link',
                            'data-pjax' => 0,
                        ]);
                    } else {
                        $html = '<i class="fa fa-video-camera"></i>';
                    }

                    return $html;
                },
            ],
            [
                'label' => Yii::t('main', 'Appointment history'),
                'format' => 'raw',
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center appointment-list', 'data-label' => Yii::t('main', 'Appointment history')],
                'value' => function ($model) {
                    return $this->render('_appointment_list', ['model' => $model]);
                },
            ],
        ],
    ]);
    ?>
    <?php Pjax::end(); ?>
<?php endif; ?>
