<?php
use yii\helpers\Url;
?>

<div class="therapists-random">
    <h3><?= Yii::t('main', 'Trust to field') ?></h3>
    <p class="modal-description"><?= Yii::t('main', 'Trust to field description') ?></p>
    <div class="row">
        <div class="person-wrapper col-md-6 col-md-offset-3">
            <div class="content">                
                <a href="/therapist/<?= $therapist->alias ?>">
                    <img class="person-image" src="<?= $therapist->getFileUrl('catalog_image') ?>" alt="">
                    <div class="person-image-hover">
                        <img src="<?= $therapist->getFileUrl('catalog_image_hover') ?>" alt="">
                    </div>
                    <div>
                        <h4 class="person-name">
                            <?= $therapist->name ?>
                        </h4>
                    </div>
                </a>
            </div>
            <div class="text-center">
                <a class="small-page-btn page-btn orange" data-target="#modal" href="<?= Url::toRoute(['therapists/random', 'id' => $therapist->id]) ?>">
                    <?= Yii::t('main', 'Try another one'); ?>
                </a>
            </div>
        </div>
    </div>
</div>