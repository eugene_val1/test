<?php

use common\models\Therapist;

/** @var int $index */
/** @var Therapist $therapist */

?>

<div class="person-wrapper col-sm-6 col-xs-12 col-md-3">
    <div class="content">
        <a href="/therapist/<?= $therapist->alias ?>">
            <img class="person-image" src="<?= $therapist->getFileUrl('catalog_image') ?>" alt="<?= $therapist->additional->catalog_image_alt ?>">
            <div class="person-image-hover">
                <img src="<?= $therapist->getFileUrl('catalog_image_hover') ?>" alt="<?= $therapist->additional->catalog_image_alt ?>">
            </div>
            <div class="person-info-hover">
                <h4 class="person-name">
                    <?= $therapist->name ?>
                </h4>
            </div>
        </a>
    </div>
</div>

<?php if (($index + 1) % 4 === 0): ?>
    <div class="row-separator"></div>
<?php endif; ?>
