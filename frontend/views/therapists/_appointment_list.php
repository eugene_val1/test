<?php if (($count = count($model->completedAppointments)) > 0): ?>
    <div class="appointment-list collapse-item <?= ($count > 5) ? 'appointment-list-scroll' : '' ?>">
        <?php if ($count > 1): ?>
            <a class="show-appointments pull-right" data-toggle="collapse" data-target="#appointments-<?= $model->id ?>">
                <i class="fa fa-angle-down"></i>
            </a>
        <?php endif; ?>
        <?php foreach ($model->completedAppointments as $i => $appointment): ?>
            <?= ($i === 1) ? '<div id="appointments-' . $model->id . '" class="collapse appointment-list-collapse">' : '' ?>
            <p>
                <?= '#' . $appointment->number . '. ' . Yii::$app->formatter->asDate($appointment->start, "medium")  . ' ' . Yii::$app->formatter->asTime($appointment->start, "short"); ?>
            </p>
        <?php endforeach; ?>
        <?= ($count > 1) ? '</div>' : '' ?>
    </div>
<?php endif; ?>


<div class="history-content">
    <div class="flex-container">
        <div class="h-item">№2</div>
        <div class="h-item">№53</div>
        <div class="h-item">№6</div>
        <div class="h-item">№2</div>
    </div>
</div>

