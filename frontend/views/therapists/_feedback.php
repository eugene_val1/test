<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\Typeahead;

?>

<div class="therapist-feedback-block">
    <h3><?= Yii::t('main', 'Give feedback about therapist') ?></h3>
    <div class="row">
        <div class="col-lg-12 form-wrapper">
            <?php $form = ActiveForm::begin(); ?>
            <?= $form->field($model, 'therapist_name')->widget(Typeahead::className(), [
                'options' => ['placeholder' => Yii::t('main', 'Therapist name ...')],
                'pluginOptions' => ['highlight' => true],
                'pluginEvents' => [
                    'typeahead:select' => 'function(ev, item) {setSelectedTherapist(item);}',
                ],
                'dataset' => [
                    [
                        'datumTokenizer' => "Bloodhound.tokenizers.obj.whitespace('value')",
                        'display' => 'value',
                        'remote' => [
                            'url' => Url::to(['therapists/name-list']) . '?q=%QUERY',
                            'wildcard' => '%QUERY'
                        ]
                    ]
                ]
            ]);
            ?>
            <?= $form->field($model, 'text')->textarea([
                    'class' => 'form-control',
                    'rows' => '3',
                    'id' => 'message-text'
            ]); ?>
            <?= $form->field($model, 'therapist_id', ['template' => '{input}{error}'])
                ->hiddenInput(['id' => 'selected-therapist']);
            ?>

            <div class="form-group text-center">
                <?= Html::submitButton(Yii::t('main', 'Send'), [
                        'class' => 'small-page-btn page-btn orange btn-submit'
                ]); ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>