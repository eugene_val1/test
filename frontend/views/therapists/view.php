<?php

use common\models\Appointment;
use common\models\Article;
use common\models\Certificate;
use common\models\Client;
use common\models\Property;
use common\models\Therapist;
use frontend\assets\TherapistAsset;
use yii\data\BaseDataProvider;
use common\models\TherapistReview;
use common\models\User;
use frontend\assets\MasonryAsset;
use nirvana\infinitescroll\InfiniteScrollPager;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\ListView;

/** @var Therapist $therapist */
/** @var int $therapistPrice */
/** @var TherapistReview[] $reviews */
/** @var Client $client */
/** @var BaseDataProvider $articleDataProvider */
/** @var Certificate[] $certificates */

MasonryAsset::register($this);
TherapistAsset::register($this);

$therapist->setMetaTags();

$this->registerJs(
    '$(function () {$(\'[data-toggle="popover"]\').popover({html:true, trigger: "hover"})});',
    View::POS_END
);

$totalArticlesCount = $articleDataProvider->getTotalCount();
$guestOrClient = (Yii::$app->user->isGuest || Yii::$app->user->identity->role !== User::ROLE_THERAPIST)
    && !$therapist->isPseudoTherapist();

?>

<div class="therapists-page container">
    <div class="page-inner">
        <div class="row flex-row">
            <div class="therapist-photo col-lg-offset-1 col-lg-4">
                <img src="<?= $therapist->getFileUrl('main_image') ?>" alt="<?= $therapist->additional->main_image_alt ?>">
            </div>
            <div class="therapist-info col-lg-offset-1 col-lg-6">

                <div class="name-block">
                    <h1><?= $therapist->name ?></h1>

                    <?php if (!$therapist->isPseudoTherapist() && $therapistPrice > 0): ?>
                        <div class="therapist-price">
                            <?= '<span class="currency">$</span>' . $therapistPrice . ' <span class="payment-details" data-toggle="popover" data-placement="bottom" data-content="<span class=\'payment-detail-message\'>' . Yii::t('main', 'Prices are quoted in dollars. In order to avoid double conversions, we make all payments in UAH on the current NBU rate') . '</span>">*</span>' ?>
                            <span class="duration">/ <?= $therapist->duration . ' ' . Yii::t('main', 'Minutes'); ?></span>
                        </div>
                    <?php endif; ?>
                </div>

                <div class="therapist-short-description">
                    <?= Yii::$app->formatter->asHtml($therapist->short_description) ?>
                </div>

                <?php if (Yii::$app->user->id !== $therapist->user_id): ?>
                    <div class="get-therapist">

                        <?php if (!empty($therapist->business_card_url)): ?>
                            <div>
                                <a href="#"
                                   class="page-btn small-page-btn white youtube-btn"
                                   data-video-url="<?= $therapist->business_card_url ?>"
                                   data-target="#video-modal">

                                    <?= Yii::t('main', 'See business card') ?>
                                    <i class="fa fa-youtube-play"></i>
                                </a>
                            </div>
                        <?php endif; ?>

                        <!-- For Guest or Client -->
                        <?php if ($guestOrClient): ?>

                            <?php if ($client && $client->next_appointment_start !== Client::NO_NEXT_APPPOINTMENT): ?>
                                <?php if ($client->nextAppointment->payment_status === Appointment::PAID): ?>
                                    <a href="<?= Url::toRoute(['appointment/video', 'id' => $client->nextAppointment->id]); ?>"
                                       class="page-btn small-page-btn orange">
                                        <i class="fa fa-video-camera"></i>
                                        <?= Yii::t('main', 'Online room') ?>
                                    </a>
                                <?php elseif ($client->nextAppointment->payment_status === Appointment::NOT_PAID): ?>
                                    <?php $this->registerJsFile('https://secure.wayforpay.com/server/pay-widget.js', ['position' => View::POS_HEAD]); ?>
                                    <a href="<?= Url::toRoute(['payment/create', 'id' => $client->nextAppointment->id]); ?>"
                                       class="page-btn small-page-btn orange" data-target="#modal">
                                        <?= Yii::t('main', 'Pay') ?>
                                    </a>
                                <?php endif; ?>
                            <?php else: ?>
                                <a data-target="#modal"
                                   href="<?= (!Yii::$app->user->isGuest) ? Url::toRoute(['messages/request-appointment', 'id' => $therapist->user_id]) : Url::toRoute('user/guest'); ?>"
                                   class="page-btn small-page-btn orange request-btn"><?= Yii::t('main', 'Request appointment') ?></a>

                                <a data-target="#modal"
                                   href="<?= (!Yii::$app->user->isGuest) ? Url::toRoute(['messages/request-appointment', 'id' => $therapist->user_id]) : Url::toRoute('user/guest'); ?>"
                                   class="mobile-fixed-btn hide-desktop orange request-btn">
                                    Записаться к терапевту
                                </a>
                            <?php endif; ?>

                        <?php endif; ?>

                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>

    <div class="down-section">
        <ul class="nav nav-tabs nav-justified">
            <li class="active">
                <a data-toggle="tab" href="#posts"><?= Yii::t('main', 'Publications') ?></a>
            </li>
            <li>
                <a class="review-tab" data-toggle="tab" href="#reviews">
                    <?= Yii::t('main', 'Reviews/Write review') ?>
                    <?php if (count($reviews)): ?>
                        <span class="counter"><?= count($reviews) ?></span>
                    <?php endif; ?>
                </a>
            </li>
            <li>
                <a data-toggle="tab" href="#details"><?= Yii::t('main', 'Detail about therapist') ?></a>
            </li>
        </ul>

        <div class="tab-content">

            <div id="posts" class="tab-pane fade in active">
                <?=
                ListView::widget([
                    'dataProvider' => $articleDataProvider,
                    'options' => [
                        'id' => 'masonry-container',
                        'class' => 'page-container',
                    ],
                    'summary' => '<div class="grid-sizer"></div>',
                    'emptyText' => Yii::t('main', 'No articles yet.'),
                    'itemOptions' => [
                        'tag' => false,
                    ],
                    'itemView' => function ($model, $key, $index, $widget) use ($totalArticlesCount) {
                        return $this->render('@frontend/views/field/_' . $model->type . '_item', [
                            'model' => $model,
                            'index' => $index,
                            'isWideBlock' => Article::isWideBlock($index, $totalArticlesCount),
                            'showAuthor' => false,
                        ]);
                    },
                    'pager' => [
                        'class' => InfiniteScrollPager::className(),
                        'widgetId' => 'masonry-container',
                        'itemsCssClass' => 'page-container',
                        'contentLoadedCallback' => 'void(0)',
                        'nextPageLabel' => Yii::t('main', 'Load more'),
                        'linkOptions' => [
                            'class' => 'btn btn-lg btn-block',
                        ],
                        'pluginOptions' => [
                            'contentSelector' => '.page-container',
                            'loading' => [
                                'msgText' => Yii::t('main', 'Loading...'),
                                'finishedMsg' => Yii::t('main', 'No more articles to load'),
                            ],
                            'behavior' => InfiniteScrollPager::BEHAVIOR_MASONRY,
                        ],
                    ],
                ]);
                ?>
            </div>

            <div id="reviews" class="tab-pane fade">
                <div class="review-items">
                    <?php if (count($reviews) > 0): ?>
                        <?php foreach ($reviews as $item): ?>
                            <div class="review-item">
                                <p class="col-sm-2 review-item-name"><?= Yii::$app->formatter->asNtext($item->name) ?></p>
                                <div class="col-sm-10 review-item-text"><?= Yii::$app->formatter->asNtext($item->text) ?>
                                </div>
                            </div>
                            <hr>
                        <?php endforeach; ?>
                    <?php else: ?>
                        <div class="row">
                            <div class="col-sm-10 col-sm-offset-2"><?= Yii::t('main', 'No therapist reviews') ?></div>
                        </div>
                        <hr>
                    <?php endif; ?>
                </div>

                <?= $this->render('_reviewForm', ['model' => $review, 'therapist' => $therapist]); ?>
            </div>

            <div id="details" class="tab-pane fade therapist-details">
                <div class="panel-group">

                    <?php foreach ($therapist->getPropertiesByType() as $type => $items): ?>
                        <div class="panel panel-default">
                            <div class="panel-heading"><?= Property::getTypeLabel($type); ?></div>
                            <div class="panel-body"><?= Yii::$app->formatter->asNtext(implode(', ', $items)) ?></div>
                        </div>
                    <?php endforeach; ?>

                    <?php if ($therapist->age && !$therapist->hide_age): ?>
                        <div class="panel panel-default">
                            <div class="panel-heading"><?= Yii::t('main', 'Age') ?></div>
                            <div class="panel-body"><?= $therapist->age ?></div>
                        </div>
                    <?php endif; ?>

                    <?php if (!empty($therapist->education)): ?>
                        <div class="collapse-item panel panel-default">
                            <div class="panel-heading collapse-item-link" data-toggle="collapse"
                                 data-target="#therapist-education">
                                <?= Yii::t('main', 'Education') ?>
                                <i class="pull-right fa fa-angle-down"></i>
                            </div>
                            <div class="panel-collapse collapse" id="therapist-education">
                                <div class="panel-body">
                                    <?= Yii::$app->formatter->asHtml($therapist->education) ?>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>

                    <?php if (count($certificates) > 0): ?>
                        <div class="collapse-item panel panel-default">
                            <div class="panel-heading collapse-item-link" data-toggle="collapse"
                                 data-target="#therapist-certificates">
                                <?= Yii::t('main', 'Certifiates and diplomas') ?>
                                <i class="pull-right fa fa-angle-down"></i>
                            </div>
                            <div class="panel-collapse collapse" id="therapist-certificates">
                                <div class="panel-body">
                                    <div id="certificates">
                                        <div class="row">
                                            <?php foreach ($certificates as $certificate): ?>
                                                <div class="col-sm-3 certificate-item">
                                                    <div class="text-center">
                                                        <a href="<?= Url::toRoute(['certificate', 'id' => $certificate->id]); ?>"
                                                           data-target="#modal">
                                                            <img src="<?= $certificate->getFileUrl('image'); ?>" alt="<?= $therapist->additional->image_alt ?>"
                                                                 class="img-responsive">
                                                        </a>
                                                        <p class="text"><?= ($certificate->description) ? Yii::$app->formatter->asNtext($certificate->description) : ''; ?></p>
                                                    </div>
                                                </div>
                                            <?php endforeach; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>

                    <?php if (!empty($therapist->experience)): ?>
                        <div class="collapse-item panel panel-default">
                            <div class="panel-heading collapse-item-link" data-toggle="collapse"
                                 data-target="#therapist-experience">
                                <?= Yii::t('main', 'Experience') ?>
                                <i class="pull-right fa fa-angle-down"></i>
                            </div>
                            <div class="panel-collapse collapse" id="therapist-experience">
                                <div class="panel-body">
                                    <?= Yii::$app->formatter->asHtml($therapist->experience) ?>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>

                    <?php if (!empty($therapist->full_description)): ?>
                        <div class="collapse-item panel panel-default">
                            <div class="panel-heading collapse-item-link" data-toggle="collapse"
                                 data-target="#therapist-full-description">
                                <?= Yii::t('main', 'Detail about therapist') ?>
                                <i class="pull-right fa fa-angle-down"></i>
                            </div>
                            <div class="panel-collapse collapse" id="therapist-full-description">
                                <div class="panel-body">
                                    <?= Yii::$app->formatter->asHtml($therapist->full_description) ?>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>

                    <?php if (!empty($therapist->terms)): ?>
                        <div class="collapse-item panel panel-default">
                            <div class="panel-heading collapse-item-link" data-toggle="collapse"
                                 data-target="#therapist-terms">
                                <?= Yii::t('main', 'Terms and conditions') ?>
                                <i class="pull-right fa fa-angle-down"></i>
                            </div>
                            <div class="panel-collapse collapse" id="therapist-terms">
                                <div class="panel-body">
                                    <?= Yii::$app->formatter->asHtml($therapist->terms) ?>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>

                </div>
            </div>
        </div>
    </div>

</div>
