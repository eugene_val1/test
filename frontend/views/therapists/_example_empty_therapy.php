<div class="client-row">

    <div class="mob-head">
        <div class="client" data-toggle="popover" data-placement="bottom" data-content="Здесь появится имя вашего терапевта, когда вы начнете работу">
            <div class="img-wrap">
                <a href="/therapists"><img src="/img/walking-logo.png"></a>
            </div>
            <div class="name">Не выбран</div>
        </div>
        <div class="next-session" data-toggle="popover" data-placement="bottom" data-content="Здесь отображается дата назначенной сессии">
            <span class="session-date">Не назначена</span>
        </div>
    </div>

    <div class="row-content">
        <div class="client mob-hidden" data-toggle="popover" data-placement="bottom" data-content="Здесь появится имя вашего терапевта, когда вы начнете работу">
            <div class="img-wrap">
                <a href="/therapists"><img src="/img/walking-logo.png"></a>
            </div>
            <div class="name">Не выбран</div>
        </div>

        <div class="messages" data-toggle="popover" data-placement="bottom" data-content="Здесь вы сможете отправить своему терапевту сообщение">
            <span class="desktop-hidden">Сообщения</span>
            <a>
                <div class="message disabled"></div>
            </a>
        </div>

        <div class="next-session mob-hidden" data-toggle="popover" data-placement="bottom" data-content="Здесь отображается дата назначенной сессии">
            <span class="session-date" >Не назначена</span>
        </div>

        <div class="payment" data-toggle="popover" data-placement="bottom" data-content="Кнопка оплаты станет активной, когда терапевт назначит сессию">
            <span class="desktop-hidden">Оплата</span>
            <a class="table-btn-grey disabled">Оплатить</a>
        </div>

        <div class="room" data-toggle="popover" data-placement="bottom" data-content="Онлайн-комната будет активна после оплаты ">
            <span class="desktop-hidden">Онлайн-комната</span>
            <span class="table-btn-rom">Не активна</span>
        </div>

        <div class="history" data-toggle="popover" data-placement="bottom" data-content="Количество завершенных сессий">
            <span class="desktop-hidden">История</span>
            <div class="h-head">
                <div class="h-item empty">-</div>
            </div>
        </div>
    </div>

</div>