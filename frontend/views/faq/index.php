<?php

use frontend\assets\AppAsset;
use yii\helpers\Html;
use kartik\widgets\ActiveForm;

AppAsset::register($this);

if($current){
    $this->title = $this->title . ' | ' . $current->name;
}

$this->params['navOpacity'] = true;
?>

<div class="page-cover" <?= ($page && $page->image) ? 'style="background-image: url(' . $page->getFileUrl('image') . ')"' : ''; ?>>
    <div class="container">
    <div class="faq-page-header">
        <h1><?= Yii::t('main', 'FAQ'); ?></h1>
        <p><?= Yii::t('main', 'Here you will find answers to the most frequently asked questions.'); ?></p>
        <div class="faq-search">
            <?php $form = ActiveForm::begin(['enableClientValidation' => false, 'id' => 'faq-form', 'action' => ['search'], 'method' => 'get']); ?>
            <?= $form->field($search, 'key', [
                'template' => '{input}{error}',
                'inputOptions' => ['name' => 'key', 'placeholder' => $search->getAttributeLabel('key')],
                'addon' => [
                    'append' => [
                        'content' => Html::submitButton('<i class="fa fa-search"></i> ' . Yii::t('main', 'Search'), ['class' => 'btn btn-primary']),
                        'asButton' => true
                    ]
                ]
            ]); ?>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
    </div>
</div>
<div class="page-container">
    <div class="container">
        <div class="row page-inner">
            <div class="col-sm-4 faq-categories">
                <?php if(count($categories) > 0): ?>
                    <?= $this->render('_categories', ['categories' => $categories, 'current' => $current, 'level' => 0]); ?>
                <?php else: ?>
                    <p class="empty-message"><?= Yii::t('main', 'Currently there are no F.A.Q. categories'); ?></p>
                <?php endif; ?>
            </div>
            <div class="col-sm-8 faq-items collapse-items">                
                <div class="faq-title">
                    <?php if($search->key): ?>
                        <h3><?= Yii::t('main', 'Search results for') . ' "' . Html::encode($search->key) . '"' ?></h3>
                    <?php elseif($current): ?>
                        <h3><?= $current->name ?></h3>
                    <?php endif; ?>
                </div>
                <?php if(count($questions) > 0): ?>
                <div class="panel-group">
                    <?php foreach($questions as $item): ?>
                    <div class="collapse-item panel panel-default">
                        <div class="panel-heading collapse-item-link" data-toggle="collapse" data-target="#question-<?= $item->id ?>">
                            <?= $item->question ?>
                            <i class="pull-right fa fa-angle-down"></i>                           
                        </div>
                        <div class="panel-collapse collapse collapse-hash-change" id="question-<?= $item->id ?>">
                            <div class="panel-body">
                                <?= $item->answer ?>
                            </div>
                        </div>
                    </div>
                    <?php endforeach;?>
                </div>
                <?php else: ?>
                    <p class="empty-message">
                        <?= ($search->key) ? Yii::t('main', 'On the search did not match') : Yii::t('main', 'Currently there are no F.A.Q. questions in this category'); ?>
                    </p>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>