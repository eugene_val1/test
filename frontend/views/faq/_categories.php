<ul class="<?= ($level > 0) ? 'subcategories' : 'root-categories'; ?>">
    <?php foreach ($categories as $category): ?>
    <li <?= ($current && in_array($category->id, $current->withParents())) ? 'class="active"' : '' ?>>
            <a href="/faq/category/<?= $category->alias ?>"><?= $category->name ?></a>
            <?php if(count($category->children) > 0): ?>
                <?= $this->render('_categories', ['categories' => $category->children, 'current' => $current,  'level' => $level+1]); ?>
            <?php endif; ?>
        </li>
    <?php endforeach; ?>
</ul>