<?php

use common\models\Messages\ConversationMessage;
use yii\web\YiiAsset;
use yii\bootstrap\BootstrapAsset;
use talma\widgets\FullCalendar;
use common\models\Language;
use common\models\Therapist;
use yii\helpers\Url;

Yii::$app->assetManager->bundles[BootstrapAsset::class] = false;
Yii::$app->assetManager->bundles[YiiAsset::class] = false;

/** @var ConversationMessage $message */

?>

<div data-user-id="<?= $message->user->id; ?>" data-id="<?= $message->id; ?>"
     class="<?= (($message->user_id == Yii::$app->user->id) ? 'my-message' : 'user-message') . ' ' . (($message->user->id != $lastUserId) ? 'first-in-group' : '') ?> message-item">
    <?php if ($message->user->id != $lastUserId): ?>
        <div class="message-avatar">
            <a href="<?= Url::toRoute([($message->user->type == Therapist::TYPE) ? 'therapists/view' : 'user/profile', 'alias' => $message->user->alias]); ?>">
                <img width="35px" src="<?= $message->user->getFileUrl('image'); ?>" alt="">
            </a>
        </div>
    <?php endif; ?>
    <div class="message-inner">
        <div class="message-text full-width-message">
            <?= nl2br($message->text); ?>
            <?php if (count($message->getEvents()) > 0): ?>
                <div class="view-collapse-btn">
                    <button data-view="<?= Yii::t('main', 'View calendar'); ?>"
                            data-hide="<?= Yii::t('main', 'Hide calendar'); ?>" type="button"
                            class="mini-page-btn page-btn calendar-button" data-toggle="collapse"
                            data-target="#calendar-attachment-<?= $message->id; ?>">
                        <?= Yii::t('main', 'View calendar'); ?>
                    </button>
                </div>
                <div data-calendar="calendar<?= $message->id; ?>" id="calendar-attachment-<?= $message->id; ?>"
                     class="calendar-attachment collapse calendar-attachment-collapse">
                    <?= FullCalendar::widget([
                        'options' => [
                            'id' => 'calendar' . $message->id
                        ],
                        'config' => [
                            'header' => [
                                'left' => '',
                                'right' => 'prev,next',
                            ],
                            'slotDuration' => '00:15:00',
                            'events' => $message->getEvents(),
                            'googleCalendar' => true,
                            'allDaySlot' => false,
                            'lang' => Language::getCurrent()->url,
                            'defaultView' => 'agendaWeek',
                            'selectable' => false,
                            'aspectRatio' => 2,
                            'defaultDate' => $message->getFirst()
                        ],
                    ]); ?>
                </div>

            <?php endif; ?>
            <div class="message-created">
                <span class="message-date"><?= Yii::$app->formatter->asDate($message->created, "medium"); ?></span>
                <span class="message-time"><?= Yii::$app->formatter->asTime($message->created, "short"); ?></span>
            </div>
        </div>
        <?php if ($message->status && $message->user_id == Yii::$app->user->id): ?>
            <div class="message-mark-read"><?= Yii::t('main', 'Read') ?> &#10003;</div>
        <?php endif; ?>
    </div>
</div>