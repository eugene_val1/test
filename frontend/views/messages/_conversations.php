<?php

use common\models\Contact;
use yii\helpers\Url;

/** @var Contact[] $contacts */

?>

<ul class="filter">
<?php foreach ($contacts as $contact): ?>
    <li>
        <a href="<?= Url::toRoute(['conversation', 'id' => $contact['conversationId']]); ?>"
           class="<?= (isset($conversationId) && $conversationId == $contact['conversationId']) ? 'active' : '' ?> <?= $contact['newMessagesCount'] ? 'hasNewMessages' : '' ?>">
            <img width="24px" src="<?= $contact['user']->getFileUrl('image'); ?>" class="img-circle" alt="">
            <?= $contact['user']->name; ?>
            <?php if ($contact['newMessagesCount']): ?>
                <span class="badge pull-right"><?= $contact['newMessagesCount']; ?></span>
            <?php endif; ?>
        </a>
    </li>
<?php endforeach; ?>

    <li>
        <a href="<?= Url::toRoute(['start']); ?>">
            <img width="24px" src="/img/more-icon.png" alt="">
            <?= Yii::t('main', 'Add contact'); ?>
        </a>
    </li>

</ul>