<?php

use common\models\Appointment;
use common\models\Messages\ConversationAppointmentMessage;
use common\models\Messages\ConversationMessage;

/** @var ConversationMessage[] $messages */
/** @var Appointment[]|null $appointments */
/** @var integer $lastUserId user id */

?>

<?php foreach ($messages as $message): ?>
    <?php $message->text = ConversationMessage::handleMessageText(
        $message->text,
        isset($appointments) ? $appointments : []
    ); ?>

    <?php
        $template = $message->type === ConversationAppointmentMessage::TYPE
            ? ConversationAppointmentMessage::TYPE
            : ConversationMessage::TYPE;

        $template = '_' . $template . '_message';
    ?>
    <?= $this->render($template, [
        'message' => $message,
        'lastUserId' => $lastUserId,
    ]); ?>

    <?php $lastUserId = $message->user->id; ?>

<?php endforeach; ?>