<?php

use common\models\Appointment;
use common\models\Contact;
use common\models\Conversation;
use common\models\CountryLang;
use common\models\User;
use frontend\assets\ConversationAsset;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

/** @var Appointment[] $appointments */
/** @var Contact[] $contacts */
/** @var Contact $contact */
/** @var Conversation $conversation */
/** @var int $offset */

ConversationAsset::register($this);
$this->registerJsFile('https://secure.wayforpay.com/server/pay-widget.js', ['position' => View::POS_HEAD]);
$this->title = $this->title . ' | ' . $contact->name;

?>

<div class="content-page container-fluid">
    <div class="page-inner narrow-page">
        <a class="menu-btn"></a>
        <div class="page-container messages-list">
            <div class="row">
                <h3>
                    <?php if (Yii::$app->user->identity->role == User::ROLE_THERAPIST): ?>
                        <?= $contact->name . ' (' . $contact->user->getTimezoneOffset(Yii::$app->user->identity->timezone) . ', ' . CountryLang::findOne(['country_id' => $contact->user->country_id, 'language' => Yii::$app->language])->name . ')' ?>
                    <?php else: ?>
                        <?= $contact->name . ' (' . $contact->user->getTimezoneOffset(Yii::$app->user->identity->timezone) . ')' ?>
                    <?php endif; ?>
                </h3>

                <div class="hide-conversation">
                    <?php if (Yii::$app->user->identity->role == User::ROLE_THERAPIST): ?>
                        <?= Html::a(
                            Yii::t('main', 'Hide conversation'),
                            Url::to(['/messages/hide-conversation', 'id' => $conversation->id]),
                            ['onclick' => 'return confirm("' . Yii::t('main', 'Conversation with this client will be hidden until he send a message') . '")']
                        ); ?>
                    <?php endif; ?>
                </div>

                <?php if ($offset): ?>
                    <div class="load-previous">
                        <button class="mini-page-btn page-btn orange">
                            <span><?= Yii::t('main', 'Load previous messages'); ?></span>
                            <img src="/img/ajax-loader.gif" alt="">
                        </button>
                    </div>
                <?php endif; ?>

                <div data-conversation="<?= $conversation->id; ?>" id="conversation">
                    <?= $this->render('_messages', [
                        'messages' => $messages,
                        'appointments' => $appointments,
                        'lastUserId' => false,
                    ]); ?>
                </div>

                <?php if(!$contact->isDeleted()) :?>
                    <?= $this->render('_sendForm', [
                            'newMessage' => $newMessage,
                            'id' => $contact->user_id,
                    ]); ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>

<aside class="side-nav side-menu auto-open">
    <div class="nav-inner simple-side-menu">
        <a class="menu-close-btn"></a>
        <div id="conversations">
            <?= $this->render('_conversations', [
                'contacts' => $contacts,
                'conversationId' => $conversation->id,
            ]); ?>
        </div>
    </div>
    <div class="nav-footer"></div>
</aside>