<?php

use common\models\Contact;
use common\models\Messages\ConversationMessage;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use common\models\User;

/** @var int $id */
/** @var Contact|null $contact */
/** @var ConversationMessage $newMessage */

?>

<?php if (Yii::$app->request->isAjax && isset($contact)): ?>
    <h3><?= Yii::t('main', 'Send message to ' . ($contact->role == User::ROLE_THERAPIST ? 'therapist' : 'user')) ?></h3>
<?php endif; ?>

<div class="new-message">
    <div class="row">
        <div class="col-sm-12 form-wrapper">  
            <?php $form = ActiveForm::begin([
                'action' => ['send', 'id' => $id],
                'enableAjaxValidation' => false,
                'enableClientValidation' => true,
                'id' => 'newMessage',
            ]); ?>

            <?= $form->field($newMessage, 'text')
                ->textarea([
                    'class' => 'form-control',
                    'rows' => '3',
                    'id' => 'message-text',
            ]); ?>

            <?php if (isset($contact) && $contact->role == User::ROLE_THERAPIST && Yii::$app->user->identity->role == User::ROLE_USER): ?>
                <div class="message-warning">
                <?= Yii::t('main', 'We encourage you to read {link} of this therapist', [
                    'link' => Html::a('<i class="fa fa-link"></i> ' . Yii::t('main', 'terms and conditions'), ['/therapist/' . $contact->profile->alias . '/terms'], ['target' => '_blank'])
                ]) ?>
            </div>
            <br/>
            <?php endif; ?>

            <div class="form-group form-submit">
                <?= Html::submitButton(
                        Yii::t('main', 'Send'),
                        ['class' => 'small-page-btn page-btn orange pull-right btn-submit']
                ); ?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>  
    </div>
</div>