<?php

use common\models\Messages\ConversationMessage;
use common\models\Messages\EventMessage;
use common\models\Therapist;
use yii\helpers\Url;

/** @var ConversationMessage $message */

$messageClasses = ($message->user_id == Yii::$app->user->id) ? 'my-message' : 'user-message';
$messageClasses .= ' ';
$messageClasses .= ($message->user->id != $lastUserId) ? 'first-in-group' : '';
$messageClasses .= ' ';
$messageClasses .= 'message-item';

if ($message->type === EventMessage::TYPE) {
    $messageClasses .= ' ' . 'system-message';
}

?>

<div data-user-id="<?= $message->user->id; ?>" data-id="<?= $message->id; ?>"
     class="<?= $messageClasses ?>">
    <?php if ($message->user->id != $lastUserId): ?>
        <div class="message-avatar">
            <a href="<?= Url::toRoute([($message->user->type == Therapist::TYPE) ? 'therapists/view' : 'user/profile', 'alias' => $message->user->alias]); ?>">
                <img width="35px" src="<?= $message->user->getFileUrl('image'); ?>" alt="">
            </a>
        </div>
    <?php endif; ?>
    <div class="message-inner">
        <div class="message-text">
            <?= nl2br($message->text); ?>
            <div class="message-created">
                <span class="message-date"><?= Yii::$app->formatter->asDate($message->created, 'medium'); ?></span>
                <span class="message-time"><?= Yii::$app->formatter->asTime($message->created, 'short'); ?></span>
            </div>
        </div>
        <?php if ($message->status && $message->user_id == Yii::$app->user->id): ?>
            <div class="message-mark-read"><?= Yii::t('main', 'Read') ?> &#10003;</div>
        <?php endif; ?>
    </div>
</div>