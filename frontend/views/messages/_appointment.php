<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\User;

Yii::$app->assetManager->bundles['yii\bootstrap\BootstrapAsset'] = false;
Yii::$app->assetManager->bundles['yii\web\YiiAsset'] = false;
?>

<div class="appointment">
    <h3><?= Yii::t('main', 'Request appointment') ?></h3>
    <div class="form-wrapper">
    <?php $form = ActiveForm::begin(['id' => 'request-appointment-form']); ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="form-group">
            <?= /*FullCalendar::widget([
                'options' => [
                    'id' => 'calendar'
                ],
                'config' => [
                    'header' => [
                        'left'=>'title',
                        'right'=>'prev,next today',
                    ],
                    'googleCalendar' => true,
                    'allDaySlot' => false,
                    'lang' => Language::getCurrent()->url,
                    'defaultView' => 'agendaWeek',
                    'slotDuration' => '00:15:00',
                    'defaultTimedEventDuration' => $therapist->getDefaultDuration(),
                    'forceEventDuration' => true,
                    'selectable' => true,
                    'select' => new JsExpression("function( start, end, jsEvent, view ){
                        var calendar = $('#calendar');
                        var id = 'event-' + start;                        
                        calendar.fullCalendar('renderEvent',{id: id, title: '', start: start}, false);
                        
                        var appointments = $('#appointments');
                        var data = '[]';
                        var events = [];
                        var startUnix = start.unix();
                        var endUnix = start.clone().add(moment.duration(calendar.fullCalendar('option', 'defaultTimedEventDuration'))).unix();
                        
                        if(appointments.val()){
                            data = appointments.val();
                        }
                        
                        events = JSON.parse(data);
                        events.push({
                            'start' : startUnix,
                            'end' : endUnix
                        });                     
                        appointments.val(JSON.stringify(events));
                    }"),
                    'eventClick' => new JsExpression("function(calEvent, jsEvent, view){
                        var id = calEvent.id;
                        var calendar = $('#calendar');
                        calendar.fullCalendar('removeEvents', id);
                        
                        var appointments = $('#appointments');
                        var data = '[]';
                        var events = [];                  

                        if(appointments.val()){
                            data = appointments.val();
                        }
                        
                        events = JSON.parse(data);
                        events = events.filter(function(event) { return event.start !== calEvent.start.unix(); });
                        appointments.val(JSON.stringify(events));                        
                    }"),
                    'eventRender' => new JsExpression("function(event, element){
                        element.attr('id', event.id);
                    }")
                ],
            ]); */''?>
            </div>
            <?= $form->field($model, 'data', ['template' => '{input}{error}'])->hiddenInput(['id' => 'appointments']); ?>
            <?= $form->field($model, 'text')->textArea(['class' => "form-control", 'rows' => "3", 'id' => 'message-text'])->label(Yii::t('main', 'Write the convenient time, the therapist will contact you')); ?>
        </div>
        <div class="col-lg-12">
            <?php if (Yii::$app->user->identity->role == User::ROLE_USER): ?>
            <div class="message-warning">
                <?= Yii::t('main', 'We encourage you to read {link} of this therapist', [
                    'link' => Html::a('<i class="fa fa-link"></i> ' . Yii::t('main', 'terms and conditions'), ['/therapist/' . $therapist->alias . '/terms'], ['target' => '_blank'])
                ]) ?>
            </div>
            <br/>
            <?php endif; ?>
            <div class="form-group form-submit text-center">
                <?= Html::submitButton(
                        Yii::t('main', 'Send'),
                        ['class' => 'small-page-btn page-btn orange btn-submit send-request-btn', 'name' => 'appointment-button']
                ) ?>
            </div>     
        </div>
    </div>
    <?php ActiveForm::end(); ?>
    </div>
</div>