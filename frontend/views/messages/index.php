<?php

use yii\helpers\Url;
use common\models\Messages\ConversationMessage;
use frontend\assets\ConversationAsset;
ConversationAsset::register($this);
?>

<div class="content-page container-fluid">
    <div class="page-inner">
        <a class="menu-btn"></a>
        <div class="page-container messages-list">
            <h3><?= Yii::t('main', 'Last messages'); ?></h3>
            <div class="row">
            <?php if (count($messages) > 0): ?>
                <ul class="all-conversations">
                <?php foreach ($messages as $message): ?>
                    <li>
                    <a href="<?= Url::toRoute(['messages/conversation', 'id' => $message->conversation_id]); ?>"
                       class="<?= (($message->status == ConversationMessage::STATUS_NEW) ? 'new-message' : '') . ' ' . (($message->user_id == Yii::$app->user->id) ? 'out-message' : 'in-message')?>">
                       <span class="img-wrap">
                           <img width="50px" src="<?= $message->contact->getFileUrl('image'); ?>" class="img-circle pull-left" alt="">
                       </span>
                        <span class="message-content">
                             <h4><?= $message->contact->name ?></h4>
                            <span class="message-container">
                                 <span class="last-message-text"><?= strip_tags($message->text) ?></span>
                                <span class="last-message-created pull-right">
                                    <?= Yii::$app->formatter->asDate($message->created, "medium"); ?>
                                    <span><?= Yii::$app->formatter->asTime($message->created, "short"); ?></span>
                                </span>
                            </span>
                        </span>
                    </a>
                    </li>
                <?php endforeach; ?>
                </ul>
            <?php else: ?>
                <p><?= Yii::t('main', 'No messages yet'); ?></p>
            <?php endif; ?>
            </div>
        </div>
    </div>
</div>
<aside class="side-nav side-menu auto-open">
    <div class="nav-inner simple-side-menu">
        <a class="menu-close-btn"></a>
        <div id="conversations">
            <?= $this->render('_conversations', ['contacts' => $contacts]); ?>
        </div>
    </div>
    <div class="nav-footer"></div>
</aside>