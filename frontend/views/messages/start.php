<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\Typeahead;
use frontend\assets\ConversationAsset;
ConversationAsset::register($this);
$this->title = $this->title . ' | ' .Yii::t('main', 'Add contact');
?>

<div class="content-page container-fluid">
    <div class="page-inner">
        <a class="menu-btn"></a>
        <div class="page-container messages-list">
            <h3><?= Yii::t('main', 'New Conversation'); ?></h3>
            <div class="row">
                <?php $form = ActiveForm::begin(); ?>
                    <?= $form->field($newMessage, 'contact_name')->widget(Typeahead::classname(), [
                        'options' => ['placeholder' => Yii::t('main', 'Type to search user name...')],
                        'pluginOptions' => ['highlight'=>true],
                        'pluginEvents' => [
                            "typeahead:select" => 'function(ev, item) {setSelectedUser(item);}',
                        ],
                        'dataset' => [
                            [
                                'datumTokenizer' => "Bloodhound.tokenizers.obj.whitespace('value')",
                                'display' => 'value',
                                'remote' => [
                                    'url' => Url::to(['user/name-list']) . '?q=%QUERY',
                                    'wildcard' => '%QUERY'
                                ]
                            ]
                        ]
                    ]);
                    ?>
                    <?= $form->field($newMessage, 'contact_id', ['template' => '{input}{error}'])->hiddenInput(['id' => 'selected-user']); ?>
                    <?= $form->field($newMessage, 'text')->textArea(['class' => "form-control", 'rows' => "3", 'id' => 'message-text']); ?>
                    <div class="form-group">
                        <?= Html::submitButton(Yii::t('main', 'Send'), ['class' => 'small-page-btn page-btn orange pull-right']); ?>
                    </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>
<aside class="side-nav side-menu auto-open">
    <div class="nav-inner simple-side-menu">
        <a class="menu-close-btn"></a>
        <div id="conversations">
            <?= $this->render('_conversations', ['contacts' => $contacts]); ?>
        </div>
    </div>
    <div class="nav-footer"></div>
</aside>