<?php

use common\models\Contact;

(new \frontend\components\seo\ViewMetaHelper())
    ->setTitle('Написать команде Тритфилд')
    ->registerDescription('Написать команде Тритфилд. Мы поможем выбрать психотерапевта или ответить на любой волнующий вас вопрос.');

/** @var Contact $model */
/** @var string $title */

?>

<div class="content-page">
    <div class="container">
        <div class="page-inner">
            <?php if ($message): ?>
            <div class="message-wrapper">
                <h4 class="form-title"><?= Yii::t('main', 'Contact us') ?></h4>
                <div class="alert alert-success">
                    <?= $message; ?>
                </div>
            </div>
            <?php else: ?>
                <?= $this->render($formName, ['model' => $model, 'title' => $title]); ?>
            <?php endif; ?>
        </div>
    </div>
</div>