<?php

use common\models\Contact;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\CountryLang;

/** @var Contact $model */
/** @var string $title */

(new \frontend\components\seo\ViewMetaHelper())
    ->setTitle('Бесплатная онлайн-консультация')
    ->registerDescription('Бесплатная онлайн-консультация по выбору психолога или психотерапевта. Поможем выбрать терапевта под ваш запрос.');

?>

<div class="contact-question">
    <h3><?= $title ?></h3>
    <div class="form-wrapper">
        <?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>
        <div class="row">
            <?php if (Yii::$app->user->isGuest): ?>
                <div class="col-sm-6">
                    <?= $form->field($model, 'name', ['template' => '{input}{error}'])->textInput(['placeholder' => $model->getAttributeLabel('name')]) ?>
                    <?= $form->field($model, 'email', ['template' => '{input}{error}'])->textInput(['placeholder' => $model->getAttributeLabel('email')]) ?>
                    <?= $form->field($model, 'country_id', ['template' => '{input}{error}'])->dropDownList(ArrayHelper::map(CountryLang::findAll(['language' => Yii::$app->language]), 'country_id', 'name'), ['prompt' => Yii::t('main', 'Select a country')]); ?>
                </div>
                <div class="col-sm-6">
                    <?= $form->field($model, 'message', ['template' => '{input}{error}'])->textarea([
                            'placeholder' => $model->getAttributeLabel('message'),
                            'style' => 'height:131px;'
                    ]) ?>
                </div>
            <?php else: ?>
                <div class="col-sm-12">
                    <?= $form->field($model, 'message', ['template' => '{input}{error}'])->textarea(['placeholder' => $model->getAttributeLabel('message')]) ?>
                </div>
            <?php endif; ?>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group form-submit">
                    <?= Html::submitButton(
                            Yii::t('main', 'Send'),
                            [
                                'class' => 'small-page-btn page-btn orange btn-submit ' . (Yii::$app->user->isGuest ? ' check-human-math' : ''),
                                'name' => 'signup-button',
                            ]
                    ) ?>
                </div>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>