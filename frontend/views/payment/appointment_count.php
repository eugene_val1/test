<?php

use common\models\Appointment;
use common\models\Profile;
use yii\web\View;

/** @var View $this */
/** @var string $url */
/** @var string $urlTemplate */
/** @var Appointment $appointment */
/** @var Profile $profile */

$allowMultiple = $profile->getPaidAppointmentsCount() > 0;
if (!$allowMultiple) {
    $this->registerJs('$("#modal").hide(); var btn = $("#payment-link"); btn.trigger("click"); setTimeout(function(){$("#modal").modal("hide")},300);', View::POS_HEAD);
}

?>

<?php if (!$allowMultiple): ?>
    <h3><?= Yii::t('main', 'Loading...'); ?></h3>
<?php endif; ?>

<div style="<?= $allowMultiple ? '' : 'display:none'?>">
    <h3><?= Yii::t('main', 'Pay one or several appointments'); ?></h3>
    <p><?= Yii::t('main', 'If you pay several appointments at once you can request to reschedule the date, but you will not be able to cancel the payment and return the funds'); ?></p>
    <br/>
    <div class="form-wrapper">
        <form id="appointment-count-form" role="form">
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group required">
                        <select id="count" class="form-control" onchange="updatePaymentUrl(this)" aria-required="true" aria-invalid="true" <?= $allowMultiple ? '' : 'disabled' ?>>
                            <?php if ($allowMultiple): ?>
                                <?php foreach (range(1, Yii::$app->params['appointments']['maxAmountPayment']) as $i): ?>
                                    <option <?php if ($i === 1): ?>selected<?php endif; ?> value="<?= $i ?>"><?= $i ?></option>
                                <?php endforeach; ?>
                            <?php else: ?>
                                <option selected value="1">1</option>
                            <?php endif; ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group form-submit">
                        <a id="payment-link" data-href-template="<?= $urlTemplate ?>" href="<?= $url ?>" class="small-page-btn page-btn orange btn-submit" data-target="#ajax" data-callback="pay" data-ajax="0">
                            <?= Yii::t('main', 'Pay') ?>
                        </a>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>