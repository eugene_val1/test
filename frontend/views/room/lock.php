<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = Yii::t('main', 'Room locked');

?>

<div class="content-page">
    <div class="container">
        <div class="page-inner">
            <div class="row">
                <div class="col-lg-offset-3 col-lg-6">
                    <h4 class="form-title"><?= Yii::t('main', 'Room locked') ?></h4>
                    <p class="lock-help"><?= Yii::t('main', 'Please fill out password field to enter the room.') ?></p>
                    <div class="form-wrapper">
                        <?php $form = ActiveForm::begin(['id' => 'room-lock-form']); ?>
                        <?= $form->field($model, 'password') ?>
                        <div class="form-group">
                            <?= Html::submitButton(Yii::t('main', 'Enter'), ['class' => 'small-page-btn page-btn orange btn-submit', 'name' => 'unlock-button']) ?>
                        </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>