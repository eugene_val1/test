<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<div class="room-password">
    <h3><?= Yii::t('main', 'Set room password') ?></h3>
    <div class="form-wrapper">
        <?php $form = ActiveForm::begin(['id' => 'room-lock-form','options' => ['data-callback' => 'afterPasswordSet']]); ?>
        <div class="row">
            <div class="col-sm-12">
                <?= $form->field($model, 'password') ?>                
                <div class="form-group">
                    <?= Html::submitButton(Yii::t('main', 'Save'), ['class' => 'small-page-btn page-btn orange btn-submit', 'name' => 'unlock-button']) ?>
                </div>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>