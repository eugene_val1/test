<?php

use common\models\Room;
use frontend\components\DeviceManager;

/** @var Room $room */

$this->title = Yii::t('main', 'Room');

?>

<div class="content-page container-fluid">
    <div class="room-page-inner page-inner">
        <div class="page-container">
            <div class="streams">
                <div class="waiting-alert" style="margin-top: 100px;">
                    <p><?= Yii::t('main', 'Online room is available from any computer and devices on Android. Application for iPad and iPhone in development') ?></p>
                </div>
            </div>
        </div>
    </div>
    <?php if (DeviceManager::isIOS()): ?>
        <script>
            window.onload = function () {
                window.location = 'treatfield://<?= $room->getRoomData() ?>';
                setTimeout(function () {
                    if (!document.webkitHidden) {
                        window.location = "<?= Yii::$app->params['iosApp'] ?>";
                    }
                }, 25);
            }
        </script>
    <?php endif; ?>
</div>