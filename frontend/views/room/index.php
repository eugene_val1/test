<?php

use common\models\Room;
use frontend\assets\AuxiliaryVideoRoomAsset;
use frontend\components\DeviceManager;
use yii\helpers\Url;
use frontend\assets\VideoRoomAsset;
use yii\web\View;

/** @var View $this */
/** @var Room $room */
/** @var string $role */
/** @var string $videoServerUrl */
/** @var bool $isRussia */

if ($isRussia) {
    AuxiliaryVideoRoomAsset::register($this);
} else {
    VideoRoomAsset::register($this);
}

$username = Yii::$app->user->identity->getName();
if (empty($username)) {
    $username = Yii::$app->user->identity->email;
}

$this->registerJs("init('" . $videoServerUrl . "', '" . $room->alias . "', '" . $username . "', '" . $role . "')");
$this->title = Yii::t('main', 'Room');

?>

<div class="content-page container-fluid">
    <div class="room-page-inner page-inner">
        <a class="menu-btn"></a>
        <div class="page-container">
            <div class="streams">
                <div class="waiting-alert">
                    <?= Yii::t('main', 'Waiting for connection') ?>
                </div>
            </div>
            <a class="fullscreen-close-btn"></a>
            <div class="fullscreen-self-video">
                <video autoplay="autoplay" id="fullscreen-self" muted="muted" volume="0" ></video>           
            </div>
        </div>
    </div>
</div>
<aside class="side-nav side-menu auto-open">
    <div class="nav-inner simple-side-menu">
        <a class="menu-close-btn"></a>
        <div class="room-controls">
            <a class="page-btn invite-btn" href="<?= Url::toRoute(['room/invite', 'alias' => $room->alias]) ?>" data-target="#modal">
                <i class="fa fa-external-link-square"></i> <?= Yii::t('main', 'Invite') ?>
            </a>
            <a href="#" class="page-btn grey lock-btn <?= ($room->password) ? 'is-locked' : 'is-unlocked' ?>">
                <span class="room-locked" data-url="<?= Url::toRoute(['room/unlock', 'alias' => $room->alias]) ?>">
                    <i class="fa fa-unlock"></i> <?= Yii::t('main', 'Unlock') ?>
                </span>
                <span class="room-unlocked" data-url="<?= Url::toRoute(['room/lock', 'alias' => $room->alias]) ?>">
                    <i class="fa fa-lock"></i> <?= Yii::t('main', 'Lock') ?>
                </span>
            </a>
            <a class="page-btn grey leave-btn" href="<?= Url::toRoute(['account/index']) ?>">
                <i class="fa fa-phone"></i> <?= Yii::t('main', 'Leave') ?>
            </a>
        </div>
        <div class="self-video">
            <video autoplay="autoplay" id="self" muted="muted" volume="0" ></video>            
        </div>
        <div class="video-controls">
            <a href="#" class="btn btn-default toggle-video-btn is-on">
                <span class="btn-toggle-on">
                    <i class="fa fa-video-camera"></i>
                </span>
                <span class="btn-toggle-off">
                    <i class="fa fa-times-circle"></i>
                </span>
            </a>
            <a href="#" class="btn btn-default toggle-audio-btn is-on">
                <span class="btn-toggle-on">
                    <i class="fa fa-microphone"></i>
                </span>
                <span class="btn-toggle-off">
                    <i class="fa fa-microphone-slash"></i>
                </span>
            </a>
            <?php if($role == 'therapist'): ?>
            <div data-update="<?= Url::toRoute(['room/duration', 'alias' => $room->alias]) ?>" data-seconds="<?= $appointment ? $appointment->duration : 0; ?>" class="timer pull-right">00 : 00 : 00</div>
            <?php endif; ?>
        </div>
        <div class="toggle-speaker-btn text-center">
            <i class="fa fa-bullhorn"></i> <?= Yii::t('main', 'Auto view speaker') ?>
            <input id="toggle-speaker" type="checkbox" data-toggle="toggle" data-size="mini" data-onstyle="info" data-on="<?= Yii::t('main', 'On') ?>" data-off="<?= Yii::t('main', 'Off') ?>">
        </div>
        <div id="chat">
            <div class="panel collapse-item">
                <div class="panel-heading">
                    <i class="fa fa-comment"></i> <?= Yii::t('main', 'Chat') ?>
                    <a class="pull-right" data-toggle="collapse" href="#chat-collapse">
                        <i class="fa fa-angle-up"></i>
                    </a>
                </div>
                <div id="chat-collapse" class="panel-body collapse in" aria-expanded="true">
                    <div class="chat-messages enable-scroll"></div>
                    <div class="panel-footer">
                        <div class="input-group">
                            <input id="chat-text" type="text" class="form-control input-sm chat_input" placeholder="<?= Yii::t('main', 'Write your message here...') ?>" />
                            <span class="input-group-btn">
                            <button class="btn btn-primary btn-sm" id="chat-btn"><?= Yii::t('main', 'Send') ?></button>
                            </span>
                        </div>
                    </div>
                </div>                
            </div>
        </div>
        <?php if(DeviceManager::isIOS()): ?>
            <script>
                window.onload = function () {
                    window.location = 'treatfield://<?= $room->getRoomData() ?>';
                    setTimeout(function() {
                        if (!document.webkitHidden) {
                            window.location = "<?= Yii::$app->params['iosApp'] ?>";
                        }
                    }, 25);
                }
            </script>
        <?php endif; ?>
    </div>
</aside>