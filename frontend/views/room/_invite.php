<?php

use common\models\Room;
use yii\helpers\Html;

/** @var Room $room */

?>

<h3><?= Yii::t('main', 'Copy and share room link') ?></h3>
<div class="invite-link">
    <div class="form-group">
        <?= Html::input('text', 'invite', $room->getUrl(), ['class' => 'form-control', 'id' => 'invite-link']) ?>
    </div>
</div>