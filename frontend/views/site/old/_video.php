<section data-number="<?= $number; ?>" class="section-content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-7 section-video-container">
                <div class="section-video-background">
                    <img class="img-responsive" alt="" src="<?= $slide->getFileUrl('image'); ?>" >
                    <div style="top: 30%; left: 0;" class="section-video-content">
                        <a data-video-url="<?= $slide->video; ?>" data-target="#video-modal" class="section-video-link" href="#">
                            <img src="/img/play.png" alt="Play"><br>
                            <span class="section-video-title"><?= $slide->title; ?></span>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-lg-5">
                <div class="section-desription">
                    <?= $slide->content; ?>                           
                </div>
            </div>
        </div>
    </div>
</section>