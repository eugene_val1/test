<section data-number="<?= $number; ?>" data-file="<?= $slide->getFileUrl('video'); ?>" id="video-wrap" class="section-video" style="background-image: url('<?= $slide->getFileUrl('image'); ?>')">
    <div class="section-video-content">
        <div class="container-fluid">
            <div class="row">
                <!--<div class="col-sm-12">
                    <div class="field-title"><?= $slide->title; ?></div>
                </div>-->
                <?= $slide->content; ?>
            </div>
        </div>
    </div>
    <p style="text-align: center;" rel="text-align: center;">
        <img id="main-arrow-down" src="/img/arrow-down-main.png">
    </p>
</section>