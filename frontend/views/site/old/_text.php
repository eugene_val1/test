<?php if($slide->template == $slide::TEMPLATE_LEFT || $slide->template == $slide::TEMPLATE_RIGHT): ?>
<section data-number="<?= $number; ?>" class="section-content">
    <div class="container-fluid">
        <div class="row">
            <?php if($slide->template == $slide::TEMPLATE_RIGHT): ?>
            <div class="col-lg-7">
                <img class="img-responsive" alt="" src="<?= $slide->getFileUrl('image'); ?>" >
            </div>
            <div class="col-lg-5">
                <div class="section-desription">
                    <?= $slide->content; ?>
                </div>                
            </div>
            <?php else: ?>
            <div class="col-lg-5">
                <div class="section-desription">
                    <?= $slide->content; ?>
                </div>                
            </div>
            <div class="col-lg-7">
                <img class="img-responsive" alt="" src="<?= $slide->getFileUrl('image'); ?>" >
            </div>            
            <?php endif; ?>
        </div>
    </div>
</section>
<?php else: ?>
    <section data-number="<?= $number; ?>" class="section-text-over parallax" style="background-image: url('<?= $slide->getFileUrl('image'); ?>')">
        <div class="section-text-over-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12">
                        <h1><?= $slide->title; ?></h1>
                        <?= $slide->content; ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>