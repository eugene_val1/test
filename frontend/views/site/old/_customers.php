<?php
use yii\helpers\Url;
?>
<section data-number="<?= $number; ?>" class="section-customers parallax" style="background-image: url('<?= $slide->getFileUrl('image'); ?>')">
    <?php $showComments = \common\models\Setting::findOne(['key' => 'showUserComments']) ?>
    <div class="section-customers-content <?= (is_object($showComments) && $showComments->value) ? '' : 'hide-comments'?>">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <h1><?= $slide->title; ?></h1>
                    <?php if(count($slide->items) > 0): ?>
                    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner" role="listbox">
                            <?php foreach($slide->items as $i => $item): ?>
                            <div class="item <?= ($i === 0) ? 'active' : ''; ?>">
                                <p class="blockquote"><?= $item->text; ?></p>
                                <?php if (isset($item->profile)): ?>
                                    <p class="customer">
                                        <?= $item->profile->name; ?>
                                    </p>
                                <?php else: ?>
                                    <p class="customer">
                                        <a href="/therapist/<?= $item->therapist->alias ?>" title="<?= $item->therapist->name ?>">
                                            <?= $item->therapist->name; ?>
                                        </a>
                                    </p>
                                <?php endif; ?>
                            </div>
                            <?php endforeach;?>
                        </div>
                        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                            <span aria-hidden="true"><img src="/img/arrow-slider-left.png"/></span>
                            <span class="sr-only"><?= Yii::t('main', 'Previous'); ?></span>
                        </a>
                        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                            <span aria-hidden="true"><img src="/img/arrow-slider-right.png"/></span>
                            <span class="sr-only"><?= Yii::t('main', 'Next'); ?></span>
                        </a>
                    </div>
                    <?php else: ?>
                        <p class="reviews-message"><?= Yii::t('main', 'Waiting for customer reviews...'); ?></p>
                    <?php endif; ?>
                </div>
                <div class="col-sm-12">
                    <a class="page-btn orange" href="<?= Url::toRoute(['feedback/index']); ?>"><?= Yii::t('main', 'All reviews'); ?></a>
                </div>
            </div>
        </div>
    </div>
</section>