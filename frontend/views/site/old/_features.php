<section data-number="<?= $number; ?>" class="section-content">
    <div class="container-fluid items features">
        <div class="row">
            <div class="col-sm-12"><h1 class="section-title"><?= $slide->title; ?></h1></div>
            <?php foreach($slide->features as $index => $feature):?>
                <?php if($index%3 == 0): ?>
                    <div class="col-sm-12">
                        <div class="row">
                <?php endif; ?>
                <div class="col-sm-4 item">
                    <?= ($feature->url) ? '<a href="' . $feature->url . '">' : '' ; ?>
                    <div class="feature-icon">                        
                        <img src="<?= $feature->getFileUrl('image'); ?>">                        
                    </div>
                    <div class="text">
                        <h5>
                            <?= $feature->title ; ?>
                        </h5>
                        <?= $feature->description; ?>                     
                    </div>
                    <?= ($feature->url) ? '</a>' : '' ;?>
                </div>
                <?php if($index == count($slide->features) - 1 || ($index - 2)%3 == 0): ?>
                        </div>
                    </div>
                <?php endif; ?>
            <?php endforeach;?>
            <div class="col-sm-12">
                <?= $slide->content; ?>
            </div>
        </div>
    </div>
</section>