<?php 
use yii\helpers\Url;
?>
<section data-number="<?= $number; ?>" class="section-content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-4 col-lg-offset-1">
                <div class="section-desription">
                    <?= $slide->content; ?>
                </div>                
            </div>
            <div class="col-lg-5 col-lg-offset-1">               
                <div class="section-field-background">
                    <img class="img-responsive" alt="" src="<?= $slide->getFileUrl('image'); ?>" >
                    <div class="field-carousel">
                        <?php if(count($slide->categories) > 0): ?>
                        <div id="carousel-field-category" class="carousel slide" data-interval="<?= $slide->interval * 1000; ?>" data-ride="carousel">
                            <div class="carousel-inner" role="listbox">
                                <?php foreach($slide->categories as $i => $item): ?>
                                    <?php if($item->lastArticle): ?>
                                    <div class="item <?= ($i === 0) ? 'active' : ''; ?>">
                                        <a href="<?= Url::toRoute(['field/category', 'alias' => $item->alias]); ?>" class="field-slide-category">
                                            <?= $item->name; ?>
                                        </a>                                    
                                        <a href="<?= Url::toRoute(['field/view', 'alias' => $item->lastArticle->alias]); ?>" class="field-slide-article">
                                            <img src="<?= $item->lastArticle->getFileUrl('image'); ?>" alt="<?= $item->lastArticle->title; ?>">
                                            <div class="field-slide-article-overlay"></div>
                                            <div class="field-slide-article-content">
                                                <h6><?= $item->lastArticle->authorName; ?></h6>
                                                <h4><?= $item->lastArticle->title; ?></h4>
                                            </div>
                                        </a>                                    
                                    </div>
                                    <?php endif; ?>
                                <?php endforeach;?>
                            </div>
                            <a class="left carousel-control" href="#carousel-field-category" role="button" data-slide="prev">
                                <span aria-hidden="true"><img src="/img/arrow-slider-left.png"/></span>
                                <span class="sr-only"><?= Yii::t('main', 'Previous'); ?></span>
                            </a>
                            <a class="right carousel-control" href="#carousel-field-category" role="button" data-slide="next">
                                <span aria-hidden="true"><img src="/img/arrow-slider-right.png"/></span>
                                <span class="sr-only"><?= Yii::t('main', 'Next'); ?></span>
                            </a>
                        </div>
                        <?php else: ?>
                            <p class="reviews-message"><?= Yii::t('main', 'Waiting new articles...'); ?></p>
                        <?php endif; ?>
                    </div>
                </div>
            </div>            
        </div>
    </div>
</section>