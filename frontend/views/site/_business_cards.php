<?php

use common\models\Therapist;

/** @var Therapist[] $therapists */

?>

<section class="section-image section-business parallax">
    <div class="section-business-content">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div id="carousel-business-generic" class="carousel slide" data-ride="carousel">

                        <h3 hidden>Визитки терепавтов онлайн-платформы Тритфилд</h3>

                        <div class="carousel-inner owl-carousel owl-theme" role="listbox">
                            <?php foreach ($therapists as $i => $therapist): ?>
                                <div class="item <?= ($i === 0) ? 'active' : ''; ?>">
                                    <div class="col-md-12 youtube-wrapper">
                                        <div class="youtube">
                                            <a href="#" data-video-url="<?= $therapist->business_card_url ?>?autoplay=1"
                                               data-target="#video-modal">
                                                <div class="play-button"></div>
                                                <?php $embed = explode('/', $therapist->business_card_url); ?>
                                                <img src="https://img.youtube.com/vi/<?= end($embed) ?>/hqdefault.jpg" alt="<?= $therapist->additional->business_card_image_alt ?>">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <a href="/field?tag=vizitka" class="btn-orange">Все визитки</a>

</section>
