<?php

use common\models\Therapist;
use common\models\TherapistReview;
use frontend\assets\HomeAsset;

HomeAsset::register($this);

$this->params['navOpacity'] = true;

/** @var Therapist[] $businessCardTherapists */
/** @var TherapistReview[] $therapistReviews */

?>

<div class="landing-loader">
    <img src="/img/loading-man.svg" alt="<?= Yii::$app->settingManager->get('mainTitle') ?>">
</div>

<!--Main page markup-->
<section class="header-block">
    <div class="container">
        <h1>Психотерапия онлайн</h1>
        <h2>Улучшайте качество жизни, <br>где бы вы ни были.</h2>
        <p>TreatField делает поиск психотерапевта проще, а консультации и оплату – удобнее</p>

        <div class="link-container">
            <a href="/therapists" class="select-specialist mobile-fixed-btn">Выбрать терапевта</a>
        </div>
    </div>
    <span class="arrow-down" id="main-arrow-down"><i class="fa fa-angle-down" aria-hidden="true"></i></span>
    <?php if (!\frontend\components\DeviceManager::isMobile()): ?>
        <video muted loop autoplay class="header-block-video">
            <source src="/img/header-block-video.mp4" type="video/mp4">
        </video>
    <?php endif; ?>
    <div class="video-overlay"></div>
</section>

<section class="advantages" style="display: none">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="adv-item">
                    <span class="img img-select"></span>
                    <h3 class="txt">Выберите психотерапевта среди специалистов платформы.</h3>
                </div>
            </div>
            <div class="col-md-4">
                <div class="adv-item">
                    <span class="img img-pay"></span>
                    <h3 class="txt">Оплачивайте консультации банковской картой прямо на сайте.</h3>
                </div>
            </div>
            <div class="col-md-4">
                <div class="adv-item">
                    <span class="img img-call"></span>
                    <h3 class="txt">Общайтесь по защищенной видеосвязи через браузер или мобильное приложение.</h3>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="about-video" style="display: none">
    <h3 hidden>Видео о платформе Тритфилд</h3>
    <video muted id="home-video" poster="/img/poster.png">
        <source src="/img/video-about.264" type="video/H264">
        <source src="/img/video-about.webm" type="video/webm">
        <source src="/img/video-about.mp4" type="video/mp4">
    </video>
    <div class="mute-btn">
        <i class="icon"></i>
    </div>
</section>

<section class="services landing-section-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="text-description width-fix">
                    <h3 class="service-title">Получайте консультации психотерапевта из любой точки мира</h3>
                    <div class="service-content">
                        <ul>
                            <li>Онлайн-психотерапия так же эффективна, как и очные консультации.
                                Это <a href="/page/online-therapy">подтверждают исследования</a>.
                            </li>
                            <li>Онлайн-психотерапия помогает сэкономить время и не прерывать работу на время
                                командировок или путешествий.
                            </li>
                            <li>Если там, где вы живете, нет терапевтов говорящих на родном языке, видеоконсультации –
                                оптимальный вариант.
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="video">
                    <video muted class="manual-autoplay" data-attr-played="0" data-attr-max-played="2">
                        <source src="/img/service1.mp4" type="video/mp4">
                    </video>
                    <img src="/img/service1mob.png" alt="Получайте консультации психотерапевта из любой точки мира">
                    <!--a href="/" class="appstore"></a-->
                </div>
            </div>
        </div>
        <div class="row mob-img-last">
            <div class="col-md-6">
                <div class="video">
                    <video muted class="manual-autoplay" data-attr-played="0" data-attr-max-played="2">
                        <source src="/img/service2.mp4" type="video/mp4">
                    </video>
                    <img src="/img/service2mob.png" alt="Выбирайте среди проверенных профессионалов">
                    <a href="/therapists" class="btn-link">Все специалисты</a>
                </div>
            </div>
            <div class="col-md-6">
                <div class="text-description padding-fix">
                    <h3 class="service-title">Выбирайте среди проверенных профессионалов</h3>
                    <div class="service-content">
                        <ul>
                            <li>Мы гарантируем, что все специалисты платформы имеют профильное психологическое
                                образование, сертифицированы как психотерапевты и имеют опыт консультирования от 5 лет.
                            </li>
                            <li>Психотерапевты Тритфилд работают с состояниями депрессии, тревоги, стресса, паническими
                                атаками, консультируют при кризисах отношений в семье и разводах.
                            </li>
                            <li>Используйте <a data-target="#modal" href="/therapists/filter">фильтр</a> или напишите
                                нам – мы поможем выбрать.
                            </li>
                            <li>Все консультации конфиденциальны.</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- business card slider -->
<?= $this->render('_business_cards', [
    'therapists' => $businessCardTherapists,
]); ?>

<section class="services landing-section-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="text-description width-fix">
                    <h3 class="service-title">Общайтесь по защищенной видеосвязи</h3>
                    <div class="service-content">
                        <ul>
                            <li>Видеосервис интегрирован в платформу – устанавливать дополнительные программы не
                                нужно.
                            </li>
                            <li>Используйте браузер или мобильное приложение для iOS, если вам удобнее общаться с айпада
                                или айфона.
                            </li>
                            <li>Система видеосвязи использует peer-to-peer соединение. Это значит, что вы общаетесь с
                                психотерапевтом напрямую, а видеопоток не проходит через сторонние сервера. Видео нельзя
                                перехватить.
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="video">
                    <video muted class="manual-autoplay" data-attr-played="0" data-attr-max-played="2">
                        <source src="/img/service3.mp4" type="video/mp4">
                    </video>
                    <img src="/img/service3mob.png" alt="AppStore Общайтесь по защищенной видеосвязи">
                    <a href="<?= Yii::$app->params['appStoreLink'] ?>" class="appstore"
                       rel="nofollow noreferrer noopener" target="_blank"></a>
                </div>
            </div>
        </div>

        <div class="row mob-img-last">
            <div class="col-md-6">
                <div class="video">
                    <video muted class="manual-autoplay" data-attr-played="0" data-attr-max-played="2">
                        <source src="/img/service4.mp4" type="video/mp4">
                    </video>
                    <img src="/img/service4mob.png" alt="Оплачивайте консультации картой прямо на сайте">
                </div>
            </div>
            <div class="col-md-6">
                <div class="text-description padding-fix">
                    <h3 class="service-title">Оплачивайте консультации картой прямо на сайте</h3>
                    <div class="service-content">
                        <ul>
                            <li>Консультации можно оплатить любой банковской картой Visa или MasterCard.</li>
                            <li>Средства снимаются только после того, как консультация состоялась. Если она не
                                состоялась по вине платформы или психотерапевта – оплата возвращается на вашу карту.
                            </li>
                            <li>Для платежей мы используем защищенную платежную систему от наших партнеров.</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="we-can-help">
    <div class="container">
        <h3 class="block-title">Мы поможем выбрать психотерапевта</h3>
        <div class="text">
            <p>Если у вас есть вопросы о том, как устроена психотерапия или какой терапевт подходит под ваш запрос лучше
                всего, – мы с удовольствием ответим в формате <span>30-минутной бесплатной онлайн-консультации или в переписке.</span>
            </p>
            <p>Обратите внимание: мы не будем работать с вашим запросом, на этой встрече мы постараемся рассказать о
                процессе психотерапии, специализации терапевтов платформы и, конечно, ответим на все вопросы.</p>
        </div>
        <div class="photos">
            <div class="col">
                <div class="photo">
                    <div class="img-wrap">
                        <img src="/img/illya.jpg" alt="Илья Полуденный Тритфилд">
                    </div>
                    <div class="name">Илья Полуденный</div>
                    <div class="position">Психолог, психотерапевт, <br> сооснователь Тритфилд</div>
                </div>
            </div>
            <div class="col">
                <div class="photo">
                    <div class="img-wrap">
                        <img src="/img/anton.jpg" alt="Антон Федорец Тритфилд">
                    </div>
                    <div class="name">Антон Федорец</div>
                    <div class="position">К.психол.н., пcихотерапевт, <br> сооснователь Тритфилд</div>
                </div>
            </div>
        </div>
        <div class="center-container">
            <a data-target="#modal" href="/contact/free-consultation" class="btn-link">Оставить заявку на бесплатную
                консультацию</a>
        </div>
    </div>
</section>

<!-- therapists' reviews -->
<?= $this->render('_reviews', [
    'therapistReviews' => $therapistReviews,
]); ?>

<section class="we-can-help test-session">
    <div class="container">
        <h3 class="block-title">Демонстрационная сессия</h3>
        <div class="text">
            <p>Мы много рассказываем о психотерапии, но иногда лучше не рассказывать, а показать. Поэтому мы публикуем
                запись реальной психотерапевтической консультации.
            </p>
            <p><span>Это не постановка:</span> клиентка согласилась рассказать о своих реальных переживаниях, мы ничего
                не вырезали
                при монтаже и не переделывали. В каком-то смысле это уникальный материал: со слезами, перескакиванием с
                одной темы на другую, паузами, растерянностью — и даже звуками дрели, которую внезапно включили соседи.
                В финале мы попросили терапевта озвучить свои выводы и прокомментировать содержание сессии.
            </p>
        </div>
        <div class="video">
            <iframe width="700" height="380" src="https://www.youtube.com/embed/qXZt04FoWgg" frameborder="0"
                    allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                    allowfullscreen></iframe>
        </div>
    </div>
</section>

<section class="write-about-us">
    <div class="container">
        <h3 class="section-title">Мы в сми</h3>
        <div class="companies-logo">
            <a href="<?= Yii::$app->params['mediaLinks']['brodude'] ?>" rel="nofollow noreferrer noopener"
               target="_blank" style="transform: scale(0.6, 0.6);">
                <img src="/img/company-logo/company-logo9.png" alt="brodude.ru">
            </a>
            <a href="<?= Yii::$app->params['mediaLinks']['womanua'] ?>" rel="nofollow noreferrer noopener"
               target="_blank">
                <img src="/img/company-logo/company-logo3.png" alt="woman.ua">
            </a>
            <a href="<?= Yii::$app->params['mediaLinks']['thevillage'] ?>" rel="nofollow noreferrer noopener"
               target="_blank">
                <img src="/img/company-logo/company-logo6.png" alt="the-village.com.ua">
            </a>
            <a href="<?= Yii::$app->params['mediaLinks']['vcru'] ?>" rel="nofollow noreferrer noopener" target="_blank"
               style="transform: scale(0.7, 0.7);">
                <img src="/img/company-logo/company-logo7.jpg" alt="vc.ru">
            </a>
        </div>
        <div class="companies-logo">
            <a href="<?= Yii::$app->params['mediaLinks']['imi'] ?>" rel="nofollow noreferrer noopener" target="_blank">
                <img src="/img/company-logo/imi-logo.svg" alt="imi.org.ua">
            </a>
            <a href="<?= Yii::$app->params['mediaLinks']['inspired'] ?>" rel="nofollow noreferrer noopener" target="_blank"
               style="transform: scale(0.7, 0.7);">
                <img src="/img/company-logo/inspired-logo.png" alt="inspired.com.uaa">
            </a>
            <a href="<?= Yii::$app->params['mediaLinks']['wasmedia'] ?>" rel="nofollow noreferrer noopener"
               target="_blank">
                <img src="/img/company-logo/company-logo2.png" alt="was.media">
            </a>
            <a href="<?= Yii::$app->params['mediaLinks']['redbull'] ?>" rel="nofollow noreferrer noopener" target="_blank"
               style="transform: scale(0.7, 0.7);">
                <img src="/img/company-logo/red-bull.png" alt="redbull.com">
            </a>
            <a href="<?= Yii::$app->params['mediaLinks']['nova'] ?>" rel="nofollow noreferrer noopener" target="_blank"
               style="transform: scale(0.7, 0.7);">
                <img src="/img/company-logo/nova-logo.png" alt="novamagazine.com.ua">
            </a>
        </div>
    </div>
</section>

<section class="mission">
    <div class="container">
        <h3 class="section-title">
            5 фактов о Тритфилд
        </h3>
        <ul class="mission-text">
            <li>Созданный в 2015 году, Тритфилд стал первой полноценной платформой онлайн-психотерапии на русскоязычном
                пространстве. Кроме того, мы первыми сделали закрытую регистрацию для терапевтов, чтобы контролировать
                качество услуг.
            </li>
            <li>Мы стремимся формировать культуру ментального здоровья, поэтому около 30% доходов тратим на создание
                бесплатных <a href="/field/videos">видеоматериалов</a> от экспертов платформы. Оплачивая психотерапию на
                Тритфилд,
                наши клиенты
                помогают развивать эту культуру
            </li>
            <li>Платформа построена таким образом, чтобы привлекать к сотрудничеству исключительно опытных
                профессионалов. Так наши клиенты получают качественные консультации, а доверие к психотерапии как
                инструменту — растёт.
            </li>
            <li>Наши специалисты работают на русском, украинском, английском и грузинском языках. Мы создаем
                мультикультурный проект для людей со всего мира.
            </li>
            <li>Основатели Тритфилд — психотерапевты, хорошо знакомые с особенностями этой сферы, трудностями и
                потребностями клиентов. Вся <a href="/page/about-project">команда</a> Тритфилд имеет опыт психотерапии:
                мы верим в пользу
                инструмента,
                который предлагаем своим пользователем.
            </li>
        </ul>
        <?php if (Yii::$app->user->isGuest): ?>
            <div class="btn-container">
                <a data-target="#modal" class="reg-btn register-btn" href="/user/signup">Зарегистрироваться</a>
            </div>
        <?php endif; ?>
    </div>
</section>

<!--section class="how-to-select">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="select-title">
                    Как выбрать <br> психотерапевта:<br> <span class="bold">Руководство</span>
                </div>
                <span class="select-icon"></span>
            </div>
            <div class="col-md-6">
                <div class="select-content">
                    <p>Мы создали руководство которое поможет вам сориентироваться на рынке психотерапии понять чем
                        психолог отличается от психотерапевта и выбрать подходящего специалиста. Оставьте свой адрес и
                        мы пришлем его вам.</p>
                    <div class="btn-container-mob">
                        <a href="/" class="btn-orange">Читать</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section-->

<!-- Begin MailChimp Signup Form -->
<?= $this->render('_mailchipm'); ?>
<!--End mc_embed_signup-->
