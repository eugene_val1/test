<div id="mc_embed_signup" class="mc_embed_signup_landing">
    <form action="//treatfield.us15.list-manage.com/subscribe/post?u=164f88a778c7108c77893020b&amp;id=5acfdd87e6" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
        <div id="mc_embed_signup_scroll">
            <div class="container">
                    <h1 class="section-title">Рассылка</h1>
                    <span><?= Yii::t('main', 'Subscribe to the newsletter not to miss nothing') ?></span>
                    <input type="email" value="" name="EMAIL" class="email form-control" id="mce-EMAIL" required/>
                    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                    <div style="position: absolute; left: -5000px;" aria-hidden="true">
                        <input type="text" name="b_164f88a778c7108c77893020b_5acfdd87e6" tabindex="-1" value=""/>
                    </div>
                    <input type="submit" value="<?= Yii::t('main', 'Subscribe')?>" name="subscribe" id="mc-embedded-subscribe" class="btn-link subscribe-btn"/>

                <div class="scroll-to-top">
                    <a href="#top">
                        <i class="fa fa-angle-up" aria-hidden="true"></i>
                    </a>
                </div>
            </div>
        </div>
    </form>
</div>
