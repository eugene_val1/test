<?php

use frontend\assets\AppAsset;
use yii\web\NotFoundHttpException;

AppAsset::register($this);

$this->title = Yii::t('main', 'Error');

/** @var \yii\base\Exception $exception */

?>

<div class="content-page">
    <div class="container">
        <div class="page-inner">
            <?php if ($exception instanceof NotFoundHttpException || $exception->getCode() === 404): ?>
                <h3><?= Yii::t('main', 'Looks like this page does not exist') ?></h3>
            <?php else: ?>
                <h3><?= Yii::t('main', 'Looks like something went wrong on our end') ?></h3>
            <?php endif; ?>
            <p>
                <?= Yii::t('main', '{0}Contact{1} us to help solve this issue.', [
                    '<a data-target="#modal" href="/contact">',
                    '</a>'
                ]); ?>
            </p>
            <div class="row">
                <div class="col-md-4 col-sm-4 col-md-offset-4 col-sm-offset-4">
                    <img src="/img/error.png"
                         style="height: 100%; width: 100%; object-fit: contain;"
                         alt="<?= Yii::t('main', 'Looks like something went wrong on our end') ?>"/>
                </div>
            </div>
        </div>
    </div>
</div>