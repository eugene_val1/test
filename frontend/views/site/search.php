<?php

use frontend\assets\AppAsset;
use nirvana\infinitescroll\InfiniteScrollPager;
use yii\helpers\Html;
use yii\widgets\ListView;

AppAsset::register($this);

?>

<div class="search-page container">

    <div class="page-inner row">
        <div class="site-search">
            <form action="/search" method="get">
                <div class="form-group">
                    <div class="input-group">
                        <input type="text" name="q" class="form-control main-search" value="<?= Html::encode($q); ?>"
                               placeholder="<?= Yii::t('main', 'Search keywords'); ?>">
                        <span class="input-group-btn">
                        <button type="submit" class="btn btn-primary">
                            <i class="fa fa-search"></i> <?= Yii::t('main', 'Seacrh'); ?>
                        </button>
                    </span>

                    </div>
                </div>
            </form>
        </div>

        <?=
        ListView::widget([
            'dataProvider' => $provider,
            'options' => [
                'id' => 'search-container',
                'class' => 'search-container row',
            ],
            'summary' => '',
            'emptyText' => Yii::t('main', 'Nothing found'),
            'itemOptions' => [
                'tag' => false,
            ],
            'itemView' => function ($model, $key, $index, $widget) {
                return $this->render('_search_item', [
                    'model' => $model['_source'],
                    'type' => $model['_type'],
                    'index' => $index
                ]);
            },
            'pager' => [
                'class' => InfiniteScrollPager::className(),
                'widgetId' => 'search-container',
                'itemsCssClass' => 'search-container',
                'contentLoadedCallback' => 'void(0)',
                'nextPageLabel' => Yii::t('main', 'Load more'),
                'linkOptions' => [
                    'class' => 'btn btn-lg btn-block',
                ],
                'pluginOptions' => [
                    'contentSelector' => '.search-container',
                    'loading' => [
                        'msgText' => Yii::t('main', 'Loading...'),
                        'finishedMsg' => Yii::t('main', 'No more search results'),
                    ],
                ],
            ],
        ]);
        ?>
    </div>

</div>