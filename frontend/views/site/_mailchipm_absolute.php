<?php

use frontend\components\TooltipHelper;

$hideMailchimp = !Yii::$app->request->cookies->getValue('hideMailchimp', false);
$showTooltips = Yii::$app->tooltip->step < TooltipHelper::STEP_5;

?>

<?php if (!$hideMailchimp && !$showTooltips): ?>
    <style>
        #mc_embed_signup {
            background: #f5f5f5;
            font: 14px Helvetica, Arial, sans-serif;
            width: 100%;
            padding: 10px 80px;
            position: fixed;
            bottom: -2px;
            left: 0;
            z-index: 9990;
            display: none;
        }

        #mc_embed_signup #sub-title, #mobile_mc_embed_signup #sub-title {
            color: rgb(109, 193, 244);
            font-size: 18px;
        }

        #mc_embed_signup .subscribe-btn {
            margin: -2px 0;
            padding: 6px 19px;
        }

        #mc_embed_signup input.email {
            width: 40%;
            margin-right: 15px;
            margin-left: 15px;
        }

        #mc_embed_signup .close {
            margin-right: -60px;
        }

        .wait-for-popup .close {
            float: right;
            font-size: 45px;
            line-height: 0.5;
            font-weight: normal;
        }

        .wait-for-popup .close:hover, .wait-for-popup .close:focus {
            color: #000;
            text-decoration: none;
            cursor: pointer;
            filter: alpha(opacity=50);
            opacity: .5;
        }

        .wait-for-popup .close:focus {
            outline: 0 !important;
        }

        /** MOBILE **/
        #mobile_mc_embed_signup {
            height: 50px;
            width: 100%;
            text-align: center;
            padding-top: 6px;
            display: none;
            cursor: pointer;
            position: fixed;
            bottom: -2px;
            left: 0;
            z-index: 9990;
            background: #f4f4f4;
        }

        #mobile_mc_embed_signup img {
            position: absolute;
            height: 85px;
            left: -10px;
            bottom: -5px;
        }

        #mobile_mc_embed_signup > span {
            font-size: 18px;
            text-align: center;
            display: block;
            padding-top: 5px;
        }

        #mobile_mc_embed_signup .close {
            display: block;
            position: absolute;
            right: 15px;
            top: 12px;
        }

        #subscribe-modal .modal-body .page-btn {
            margin-left: auto;
            margin-right: auto;
            display: block;
        }

        @media (max-width: 1245px) {
            #mc_embed_signup span {
                display: block;
                margin-bottom: 5px;
                width: 100%;
            }
        }

        @media (max-width: 1024px) {
            #mc_embed_signup input.email {
                width: 50%;
            }
        }

        @media (max-width: 320px) {
            #mobile_mc_embed_signup > span {
                font-size: 15px;
            }
        }

    </style>

    <div id="mc_embed_signup" class="wait-for-popup">
        <form action="//treatfield.us15.list-manage.com/subscribe/post?u=164f88a778c7108c77893020b&amp;id=5acfdd87e6" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
            <div id="mc_embed_signup_scroll">
                <div class="row">
                    <span id="sub-title"><?= Yii::t('main', 'Подпишитесь на рассылку, чтобы ничего не упустить') ?></span>
                    <input type="email" value="" name="EMAIL" class="email form-control" id="mce-EMAIL" placeholder="example@mail.com" required/>
                    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                    <div style="position: absolute; left: -5000px;" aria-hidden="true">
                        <input type="text" name="b_164f88a778c7108c77893020b_5acfdd87e6" tabindex="-1" value=""/>
                    </div>
                    <input type="submit" value="<?= Yii::t('main', 'Subscribe')?>" name="subscribe" id="mc-embedded-subscribe" class="page-btn small-page-btn subscribe-btn"/>
                    <button type="button" class="close" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
        </form>
    </div>

    <div id="mobile_mc_embed_signup" class="wait-for-popup" data-toggle="modal" data-target="#subscribe-modal">
        <img src="/img/subscribe-man.png"/>
        <span><?= Yii::t('main', 'Подпишитесь на рассылку') ?></span>
        <button type="button" class="close" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>

    <!-- Modal -->
    <div id="subscribe-modal" class="modal fade standart-modal" role="dialog" aria-hidden="true" aria-labelledby="myModalLabel">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <form action="//treatfield.us15.list-manage.com/subscribe/post?u=164f88a778c7108c77893020b&amp;id=5acfdd87e6" method="post" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                        <div class="row">
                            <h3><?= Yii::t('main', 'Subscribe to the newsletter not to miss nothing') ?></h3>
                            <input type="email" value="" name="EMAIL" class="email form-control" id="mce-EMAIL" placeholder="example@mail.com" required/>
                            <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                            <div style="position: absolute; left: -5000px;" aria-hidden="true">
                                <input type="text" name="b_164f88a778c7108c77893020b_5acfdd87e6" tabindex="-1" value=""/>
                            </div>
                            <input type="submit" value="<?= Yii::t('main', 'Subscribe')?>" name="subscribe" class="page-btn small-page-btn subscribe-btn"/>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

<?php endif; ?>

