<?php

use yii\helpers\StringHelper;

?>

<div class="col-lg-12 search-item">

    <a class="search-item-link" href="<?= $model['url'] ?>">
        <h3 class="search-item-title">
            <?php if($type == 'therapist'): ?>
            <i class="fa fa-user" aria-hidden="true"></i>
            <?php else: ?>
            <i class="fa fa-newspaper-o" aria-hidden="true"></i>
            <?php endif; ?>
            <?= $model['title'] ?>            
        </h3>
    </a>

    <div class="search-item-content">
        <?= StringHelper::truncateWords(strip_tags($model['content']), 50) ?>
    </div>

</div>
