<?php

use common\models\TherapistReview;
use frontend\components\DeviceManager;

/** @var TherapistReview[] $therapistReviews */

$reviewTextLimit = DeviceManager::isMobile() ? 200 : 500;

?>

<section class="reviews">
    <div class="container">

        <h3 class="section-title">Отзывы пользователей</h3>

        <div id="reviews" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner" role="listbox">
                <?php foreach ($therapistReviews as $index => $review): ?>
                    <div class="item <?= $index === 0 ? 'active' : '' ?>">
                        <div class="rev-text">
                            <?php
                                $textLen = mb_strlen($review->text, 'utf-8');
                                $offset = $reviewTextLimit < $textLen ? $reviewTextLimit : $textLen;
                                $pivot = mb_strpos($review->text, ' ', $offset - 1, 'utf-8');
                                $pivot = !$pivot ? $reviewTextLimit : $pivot;
                            ?>
                            <?= mb_substr($review->text, 0, $pivot, 'utf-8') ?>
                            <?php if ($textLen > $pivot): ?>
                                <span class="full-text">
                                    <?= mb_substr($review->text, $pivot, null, 'utf-8') ?>
                                </span>
                                &mdash; <span class="read-more">читать весь</span>
                            <?php endif; ?>
                        </div>
                        <div class="comentator">
                            <?= $review->name ?> o
                            <a href="/therapist/<?= $review->therapist->alias ?>" class="doctor">
                                <?= $review->therapist->name ?>
                            </a>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>

            <a class="left carousel-control" href="#reviews" role="button" data-slide="prev">
                <!--span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span-->
                <span aria-hidden="true">
                    <span class="control-arrow"></span>
                </span>
                <span class="sr-only">
                    <?= Yii::t('main', 'Previous'); ?>
                </span>
            </a>

            <a class="right carousel-control" href="#reviews" role="button" data-slide="next">
                <!--span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span-->
                <span aria-hidden="true">
                    <span class="control-arrow"></span>
                </span>
                <span class="sr-only">
                    <?= Yii::t('main', 'Next'); ?>
                </span>
            </a>

            <!--ol class="carousel-indicators">
                <?php foreach ($therapistReviews as $index => $review): ?>
                    <li data-target="#reviews" data-slide-to="<?= $index ?>"
                        class="<?= $index === 0 ? 'active' : '' ?>"></li>
                <?php endforeach; ?>
            </ol-->
        </div>

        <!--div class="btn-container">
            <a href="/therapists/reviews" class="btn-orange">Все отзывы</a>
        </div-->

    </div>
</section>
