<?= '<?xml version="1.0" encoding="UTF-8"?>' . PHP_EOL; ?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:image="http://www.google.com/schemas/sitemap-image/1.1">
    <?php foreach ($urls as $url): ?>
        <url>
            <loc><?= $url['loc'] ?></loc>
            <?php if (isset($url['lastmod'])): ?>
                <lastmod><?= date(DATE_W3C, $url['lastmod']) ?></lastmod>
            <?php endif; ?>
            <?php if (isset($url['changefreq'])): ?>
                <changefreq><?= $url['changefreq'] ?></changefreq>
            <?php endif; ?>
            <?php if (isset($url['priority'])): ?>
                <priority><?= $url['priority'] ?></priority>
            <?php endif; ?>
            <?php if (isset($url['images'])):
                foreach ($url['images'] as $image): ?>
                    <image:image>
                        <image:loc><?= yii\helpers\Url::to($image['loc'], true) ?></image:loc>
                        <?= isset($image['title']) ? "<image:title>{$image['title']}</image:title>" : ''; ?>
                    </image:image>
                <?php endforeach;
            endif; ?>
        </url>
    <?php endforeach; ?>
</urlset>