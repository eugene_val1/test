<?= '<?xml version="1.0" encoding="UTF-8"?>' . PHP_EOL; ?>
<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    <?php foreach ($languages as $language): ?>
    <?php foreach ($maps as $map): ?>
    <sitemap>
        <loc><?= Yii::$app->params['siteUrl'] . '/' . $language->url . '/' . $map . '.xml'; ?></loc>
        <lastmod><?= date(DATE_W3C); ?></lastmod>
    </sitemap>
    <?php endforeach; ?>
    <?php endforeach; ?>
</sitemapindex>