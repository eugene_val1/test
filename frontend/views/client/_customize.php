<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\models\Client;
use yii\helpers\ArrayHelper;
?>

<div class="contact-question">
    <h3><?= Yii::t('main', 'Change client data') ?></h3>
    <div class="form-wrapper">
        <?php $form = ActiveForm::begin(
        [
            'id' => 'client-name-form', 
            'options' => [
                'data-callback' => 'afterClientNameChange', 
                'data-reload-grid' => ArrayHelper::keyExists($model->status, Client::getStatusData()) ? Client::getStatusData()[$model->status]['alias'] : false
                ]
        ]); ?>
        <div class="row">
            <div class="col-sm-12">
                <p class="form-help"><?= Yii::t('main', 'Leave blank to reset for real client name') ?></p>
                <?= $form->field($model, 'user_name')->textInput() ?>
                <p class="form-help"><?= Yii::t('main', 'Set price to 0 to use default region price') ?></p>
                <?= $form->field($model, 'price')->textInput() ?>
            </div>
            <div class="col-sm-12">
                <div class="form-group form-submit">
                    <?= Html::submitButton(Yii::t('main', 'Save'), ['class' => 'small-page-btn page-btn orange btn-submit', 'name' => 'save-button']) ?>
                </div>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>