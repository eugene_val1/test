<?php

use common\models\Timezone;
use yii\data\Pagination;
use common\models\Appointment;
use common\models\Client;
use common\models\PaidAppointments;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;
use yii\widgets\Pjax;

/** @var Client[] $models */
/** @var Pagination $pages */

/**
 * @param int $date
 *
 * @return string
 */
function formatAppointmentDate($date) {
    return Yii::$app->formatter->asDate($date, 'php:l') . '</br>' .
        '<span>' . Timezone::convert($date, Yii::$app->formatter->timeZone, 'd') . ' '
        .  Yii::$app->formatter->asDate($date, 'php:F') . '</span>' . '</br>' .
        '<span>' . Timezone::convert($date, Yii::$app->formatter->timeZone, 'H:i') . '</span>';
}

$this->registerJs("$('[data-toggle=\"popover\"]').popover({'trigger': 'hover'})");

?>

<div class="table-head">
    <div class="client">Клиент</div>
    <div class="messages">Сообщения</div>
    <div class="status">Статус</div>
    <div class="next-session">Следущая сессия</div>
    <div class="payment">Оплата</div>
    <div class="room">Онлайн комната</div>
    <div class="history">История</div>
</div>

<div class="table-body">

    <?php Pjax::begin(['id' => 'active-list', 'enablePushState' => false]) ?>
    <?php foreach ($models as $model): ?>

        <?php
            $isActiveRoom = $model->nextAppointment
                && $model->nextAppointment->payment_status === Appointment::PAID
                && $model->next_appointment_start !== Client::NO_NEXT_APPPOINTMENT;
        ?>

        <?php /** @var Client $model */ ?>

        <?php
            $rowClass = 'client-row';
            $target = '';
            if ($model->nextAppointment && $model->next_appointment_start > 0 && $model->next_appointment_start + Client::APPOINTMENT_OVERDUE < time()) {
                $rowClass .= ' not-read';
                $target = Url::to(['appointment/overdue', 'id' => $model->nextAppointment->id]);
            }
        ?>

        <div class="<?= $rowClass ?>" data-target="<?= $target ?>" data-informed="<?= $model->nextAppointment->overdue_informed ?>">

            <div class="mob-head">
                <div class="client">
                    <div class="img-wrap">
                        <?= Html::a('<img src="' . $model->user->getFileUrl('image') . '" alt="' . $model->user_name . '">',
                            ['customize', 'id' => $model->id],
                            ['data-pjax' => 0, 'data-target' => '#modal']
                        ); ?>
                    </div>
                    <div class="name" data-toggle="popover" data-placement="bottom" data-content="<?= $model->user_name ?>">
                        <?= $model->user_name ?>
                    </div>
                </div>
                <div class="next-session">
                    <span class="session-date">
                        <?php
                            echo $model->next_appointment_start !== Client::NO_NEXT_APPPOINTMENT && $model->nextAppointment
                                ? formatAppointmentDate($model->next_appointment_start)
                                : Yii::t('main', 'Check the date and time of next appointment');
                        ?>
                    </span>
                </div>
                <div class="room <?= $isActiveRoom ? 'active' : '' ?>">
                    <?php
                    echo (function ($model) use ($isActiveRoom) {
                        if ($isActiveRoom) {
                            return Html::a('Войти',
                                ['appointment/video', 'id' => $model->nextAppointment->id],
                                ['class' => 'table-btn-rom', 'data-pjax' => 0]
                            );
                        }

                        if ($model->status == Client::STATUS_PENDING) {
                            return '<a class="table-btn-rom">' . Yii::t('main', 'Pending') . '</a>';
                        }

                        return '<a class="table-btn-rom">Не активна</a>';
                    })($model);
                    ?>
                </div>
            </div>

            <div class="row-content">
                <div class="client mob-hidden">
                    <div class="img-wrap">
                        <?= Html::a('<img src="' . $model->user->getFileUrl('image') . '" alt="' . $model->user_name . '">',
                            ['customize', 'id' => $model->id],
                            ['data-pjax' => 0, 'data-target' => '#modal']
                        ); ?>
                    </div>
                    <div class="name" data-toggle="popover" data-placement="bottom" data-content="<?= $model->user_name ?>">
                        <?php
                        echo (function ($model) {
                            if ($model->user->isDeleted()) {
                                return Html::a($model->user_name . ' <i class="fa fa-ban"></i>');
                            }

                            return Html::a($model->user_name . ' <i class="fa fa-pencil"></i>',
                                ['customize', 'id' => $model->id],
                                ['data-pjax' => 0, 'data-target' => '#modal']
                            );
                        })($model);
                        ?>
                    </div>
                </div>

                <?php $messagesCount = count($model->newMessages); ?>
                <div class="messages">
                    <span class="desktop-hidden">Сообщения</span>
                    <div>
                        <a href="<?= Url::to(['messages/conversation', 'id' => $model->conversation_id]) ?>" data-pjax="0">
                            <div class="message">
                                <?php if($messagesCount): ?>
                                    <div class="quantity-label"><?= $messagesCount ?></div>
                                <?php endif; ?>
                            </div>
                        </a>
                    </div>
                </div>

                <div class="status">
                    <span class="desktop-hidden">Статус</span>
                    <div>
                        <?= (function ($model) {
                            if (!$model->nextAppointment) {
                                return Yii::t('main', 'Appointment status');
                            }
                            return Html::a(Yii::t('main', 'Appointment status'), ['appointment/mark', 'id' => $model->nextAppointment->id], [
                                'class' => 'table-btn-orange',
                                'data' => [
                                    'pjax' => 0,
                                    'target' => '#modal',
                                ],
                            ]);
                        })($model) ?>
                    </div>
                </div>

                <div class="next-session mob-hidden">
                    <span class="session-date">
                        <?php
                            echo $model->next_appointment_start !== Client::NO_NEXT_APPPOINTMENT && $model->nextAppointment
                                ? formatAppointmentDate($model->next_appointment_start)
                                : Yii::t('main', 'Check the date and time of next appointment');
                        ?>
                    </span>
                </div>

                <?php $isPaid = $model->nextAppointment && $model->nextAppointment->payment_status == Appointment::PAID; ?>
                <div class="payment <?= $isPaid ? 'paid' : '' ?>">
                    <span class="desktop-hidden">Оплата</span>
                    <span>
                        <?php
                        echo (function ($model) {
                            if ($model->status == Client::STATUS_INACTIVE || !$model->nextAppointment) {
                                return 'Не оплачена';
                            }

                            if ($model->nextAppointment->payment_status == Appointment::NOT_PAID) {
                                $html = Yii::t('main', 'Not paid');
                            } elseif ($model->nextAppointment->payment_status == Appointment::PENDING) {
                                $html = Yii::t('main', 'In progress');
                            } else {
                                $paidAppointments = PaidAppointments::getForUser($model->user_id);
                                if ($paidAppointments > 0) {
                                    $paidAppointments = ' (' . ($paidAppointments + 1) . ')';
                                } else {
                                    $paidAppointments = '';
                                }
                                $html = Yii::t('main', 'Paid') . $paidAppointments;
                            }

                            return $html;
                        })($model);
                        ?>
                    </span>
                </div>

                <div class="room mob-hidden <?= $isActiveRoom ? 'active' : '' ?>">
                    <span class="desktop-hidden">Комната</span>
                    <?php
                    echo (function ($model) use ($isActiveRoom) {
                        if ($isActiveRoom) {
                            return Html::a('Войти',
                                ['appointment/video', 'id' => $model->nextAppointment->id],
                                ['class' => 'table-btn-rom', 'data-pjax' => 0]
                            );
                        }

                        if ($model->status == Client::STATUS_PENDING) {
                            return '<a class="table-btn-rom">' . Yii::t('main', 'Pending') . '</a>';
                        }

                        return '<a class="table-btn-rom">Не активна</a>';
                    })($model);
                    ?>
                </div>

                <?php $completedAppointmentsCount = count($model->completedAppointments); ?>
                <div class="history">
                    <span class="desktop-hidden">История</span>
                    <div class="h-head">
                        <?php if ($completedAppointmentsCount > 0): ?>
                            <?= Html::a(
                                ' <div class="h-item' . (empty($model->completedAppointments[0]->comment) ? '' : ' comented') . '">' . $model->completedAppointments[0]->number . '</div>',
                                ['appointment/update', 'id' => $model->completedAppointments[0]->id],
                                ['data-pjax' => 0, 'data-target' => '#modal']
                            ); ?>
                            <i class="fa fa-angle-down <?= $completedAppointmentsCount <= 1 ? 'empty' : '' ?>" aria-hidden="true"></i>
                        <?php else: ?>
                            <div class="h-item">-</div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>

            <?php if ($completedAppointmentsCount > 1): ?>
                <div class="history-content">
                    <div class="flex-container">
                        <?php foreach ($model->completedAppointments as $i => $appointment): ?>
                            <?php if ($i === 0) {
                                continue;
                            } ?>
                            <?= Html::a(
                                ' <div class="h-item' . (empty($appointment->comment) ? '' : ' comented') . '">' . $appointment->number . '</div>',
                                ['appointment/update', 'id' => $appointment->id],
                                ['data-pjax' => 0, 'data-target' => '#modal']
                            ); ?>
                        <?php endforeach; ?>
                    </div>
                </div>
            <?php endif; ?>

        </div>
    <?php endforeach; ?>

    <?php
    echo LinkPager::widget([
        'pagination' => $pages,
    ]);
    ?>

    <?php Pjax::end() ?>
</div>
