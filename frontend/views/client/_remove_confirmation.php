<?php

use common\models\Client;
use yii\web\YiiAsset;
use yii\web\JqueryAsset;
use yii\bootstrap\BootstrapAsset;
use yii\helpers\Html;

Yii::$app->assetManager->bundles[BootstrapAsset::class] = false;
Yii::$app->assetManager->bundles[JqueryAsset::class] = false;
Yii::$app->assetManager->bundles[YiiAsset::class] = false;

/** @var Client $client */

?>

<div class="remove-confirmation">
    <h3><?= Yii::t('main', 'Please, confirm the deletion') ?></h3>
    <div class="row">
        <div class="col-lg-12">
            <p>
                <img width="30px" src="<?= $client->user->getFileUrl('image'); ?>" class="img-circle" alt="">
                <span><?= $client->user->name ?></span>
                <?= Html::a(
                        Yii::t('backend', 'Cancel'),
                        '#',
                        ['class' => 'btn btn-default btn-sm pull-right close-modal']
                ); ?>
                <?= Html::a(
                        Yii::t('backend', 'Yes'),
                        ['remove-client', 'id' => $client->id],
                        ['class' => 'btn btn-danger btn-sm remove-client pull-right']
                ); ?>
            </p>
        </div>
    </div>
</div>