<?php

use yii\data\Pagination;
use common\models\Appointment;
use common\models\Client;
use common\models\PaidAppointments;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;
use yii\widgets\Pjax;

/** @var Client[] $models */
/** @var Pagination $pages */

$this->registerJs("$('[data-toggle=\"popover\"]').popover({'trigger': 'hover'})");

?>

<div class="table-head">
    <div class="client">Клиент</div>
    <div class="messages">Сообщения</div>
    <div class="status">Статус клиента</div>
    <div class="payment">Оплата</div>
    <div class="room">Онлайн комната</div>
    <div class="history">История</div>
</div>

<div class="table-body">

    <?php Pjax::begin(['id' => 'pending-list', 'enablePushState' => false]) ?>
    <?php foreach ($models as $model): ?>

        <?php /** @var Client $model */ ?>
        <?php $messagesCount = count($model->newMessages); ?>

        <div class="client-row">
            <div class="mob-head">
                <div class="client">
                    <div class="img-wrap">
                        <?= Html::a('<img src="' . $model->user->getFileUrl('image') . '" alt="' . $model->user_name . '">',
                            ['customize', 'id' => $model->id],
                            ['data-pjax' => 0, 'data-target' => '#modal']
                        ); ?>
                    </div>
                    <div class="name" data-toggle="popover" data-placement="bottom" data-content="<?= $model->user_name ?>">
                        <?= $model->user_name ?>
                    </div>
                </div>
                <div class="messages">
                    <a href="<?= Url::to(['messages/conversation', 'id' => $model->conversation_id]) ?>" data-pjax="0">
                        <div class="message">
                            <?php if($messagesCount): ?>
                                <div class="quantity-label"><?= $messagesCount ?></div>
                            <?php endif; ?>
                        </div>
                    </a>
                </div>
            </div>

            <div class="row-content">
                <div class="client mob-hidden">
                    <div class="img-wrap">
                        <?= Html::a('<img src="' . $model->user->getFileUrl('image') . '" alt="' . $model->user_name . '">',
                            ['customize', 'id' => $model->id],
                            ['data-pjax' => 0, 'data-target' => '#modal']
                        ); ?>
                    </div>
                    <div class="name" data-toggle="popover" data-placement="bottom" data-content="<?= $model->user_name ?>">
                        <?php
                        echo (function($model) {
                            if ($model->user->isDeleted()) {
                                return Html::a($model->user_name . ' <i class="fa fa-ban"></i>');
                            }

                            return Html::a($model->user_name . ' <i class="fa fa-pencil"></i>',
                                ['customize', 'id' => $model->id],
                                ['data-pjax' => 0, 'data-target' => '#modal']
                            );
                        })($model);
                        ?>
                    </div>
                </div>

                <div class="messages mob-hidden">
                    <a href="<?= Url::to(['messages/conversation', 'id' => $model->conversation_id]) ?>" data-pjax="0">
                        <div class="message">
                            <?php if($messagesCount): ?>
                                <div class="quantity-label"><?= $messagesCount ?></div>
                            <?php endif; ?>
                        </div>
                    </a>
                </div>

                <div class="status">
                    <span class="desktop-hidden">Статус</span>
                    <div>
                        <?= (function ($model) {
                            return Html::a(Yii::t('main', 'Set an appointment'),
                                ['appointment', 'id' => $model->id, 'reschedule' => true],
                                [
                                    'class' => 'table-btn-blue',
                                    'data' => [
                                        'pjax' => 0,
                                        'target' => '#modal'
                                    ],
                                ]);
                        })($model) ?>

                        <?= (function ($model) {
                            return Html::a(
                                Yii::t('main', 'Set inactive'),
                                ['set-inactive-confirmation', 'id' => $model->id],
                                [
                                    'class' => 'table-btn-grey',
                                    'data' => [
                                        'pjax' => 0,
                                        'target' => '#modal'
                                    ],
                                ]
                            );
                        })($model) ?>
                    </div>
                </div>

                <?php $isPaid = $model->nextAppointment && $model->nextAppointment->payment_status == Appointment::PAID; ?>
                <div class="payment <?= $isPaid ? 'paid' : '' ?>">
                    <span class="desktop-hidden">Оплата</span>
                    <span>
                        <?php
                        echo (function($model) {
                            if ($model->status == Client::STATUS_INACTIVE || !$model->nextAppointment) {
                                return 'Не оплачена';
                            }

                            if ($model->nextAppointment->payment_status == Appointment::NOT_PAID) {
                                $html = Yii::t('main', 'Not paid');
                            } elseif ($model->nextAppointment->payment_status == Appointment::PENDING) {
                                $html = Yii::t('main', 'In progress');
                            } else {
                                $paidAppointments = PaidAppointments::getForUser($model->user_id);
                                if ($paidAppointments > 0) {
                                    $paidAppointments = ' (' . ($paidAppointments + 1) . ')';
                                } else {
                                    $paidAppointments = '';
                                }
                                $html = Yii::t('main', 'Paid') . $paidAppointments;
                            }

                            return $html;
                        })($model);
                        ?>
                    </span>
                </div>

                <?php
                    $isActiveRoom = $model->nextAppointment
                        && $model->nextAppointment->payment_status === Appointment::PAID
                        && $model->next_appointment_start !== Client::NO_NEXT_APPPOINTMENT;
                    ?>
                <div class="room <?= $isActiveRoom ? 'active' : '' ?>">
                    <span class="desktop-hidden">Комната</span>
                    <?php
                    echo (function($model) use ($isActiveRoom) {
                        if ($isActiveRoom) {
                            return Html::a('Войти',
                                ['appointment/video', 'id' => $model->nextAppointment->id],
                                ['class' => 'table-btn-rom', 'data-pjax' => 0]
                            );
                        }

                        if ($model->status == Client::STATUS_PENDING) {
                            return '<a class="table-btn-rom">Не активна</a>';
                        }

                        return '<a class="table-btn-rom">Не активна</a>';
                    })($model);
                    ?>
                </div>

                <?php $completedAppointmentsCount = count($model->completedAppointments); ?>
                <div class="history">
                    <span class="desktop-hidden">История</span>
                    <div class="h-head">
                        <?php if ($completedAppointmentsCount > 0): ?>
                            <?= Html::a(
                                ' <div class="h-item' . (empty($model->completedAppointments[0]->comment) ? '' : ' comented'). '">' . $model->completedAppointments[0]->number . '</div>',
                                ['appointment/update', 'id' => $model->completedAppointments[0]->id],
                                ['data-pjax' => 0, 'data-target' => '#modal']
                            ); ?>
                            <i class="fa fa-angle-down <?= $completedAppointmentsCount <= 1 ? 'empty' : '' ?>" aria-hidden="true"></i>
                        <?php else: ?>
                            <div class="h-item">-</div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>

            <?php if ($completedAppointmentsCount > 1): ?>
                <div class="history-content">
                    <div class="flex-container">
                        <?php foreach ($model->completedAppointments as $i => $appointment): ?>
                            <?php if ($i === 0) { continue; } ?>
                            <?= Html::a(
                                ' <div class="h-item' . (empty($appointment->comment) ? '' : ' comented'). '">' . $appointment->number . '</div>',
                                ['appointment/update', 'id' => $appointment->id],
                                ['data-pjax' => 0, 'data-target' => '#modal']
                            ); ?>
                        <?php endforeach; ?>
                    </div>
                </div>
            <?php endif; ?>

        </div>
    <?php endforeach; ?>

    <?php
        echo LinkPager::widget([
            'pagination' => $pages,
        ]);
    ?>

    <?php Pjax::end() ?>
</div>
