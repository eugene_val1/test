<?php
use common\models\PaidAppointments;
use common\models\Timezone;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;
use common\models\Client;
use common\models\Appointment;
use yii\helpers\Url;

Yii::$app->assetManager->bundles['yii\bootstrap\BootstrapAsset'] = false;
Yii::$app->assetManager->bundles['yii\web\JqueryAsset'] = false;
Yii::$app->assetManager->bundles['yii\web\YiiAsset'] = false;
$this->registerJs("$('[data-toggle=\"popover\"]').popover({'trigger': 'hover'}); checkOverdue();");

/** @var \yii\data\BaseDataProvider $dataProvider */

?>

<?php foreach ($dataProviders as $status => $dataProvider): ?>
    <div class="<?= Client::getStatusData()[$status]['alias'] ?>-client-section"><?= Client::getStatusData()[$status]['name'] ?></div>
    <?php Pjax::begin(['id' => Client::getStatusData()[$status]['alias'] . '-pjax-clients', 'enablePushState' => false]); ?>
    <?= GridView::widget([
        'id' => Client::getStatusData()[$status]['alias'] . '-clients-grid',
        'dataProvider' => $dataProvider,
        'tableOptions' => ['class' => 'table table-striped table-advance table-hover responsive'],
        'rowOptions' => function ($model, $key, $index, $grid) {
            $data = [
                'class' => ''
            ];
            if ($model->nextAppointment && $model->next_appointment_start > 0 && $model->next_appointment_start + Client::APPOINTMENT_OVERDUE < time()) {
                $data = [
                    'class' => 'overdue',
                    'data-target' => Url::to(['appointment/overdue', 'id' => $model->nextAppointment->id]),
                    'data-informed' => $model->nextAppointment->overdue_informed
                ];
            }

            if ($model->user->isDeleted()) {
                $data['class'] .= ' danger';
            }

            return $data;
        },
        'summary' => '',
        'columns' => [
            [
                'class' => 'yii\grid\SerialColumn',
                'contentOptions' => ['id' => 'counter']
            ],
            [
                'attribute' => 'user_name1',
                'header' => Yii::t('main', 'Client') . ' <span data-toggle="popover" data-placement="bottom" data-content="' . Yii::t('main', 'Client column for ' . Client::getStatusData()[$status]['alias'] . ' clients description') . '"><i class="fa fa-question-circle" aria-hidden="true"></i></span>',
                'format' => 'raw',
                'headerOptions' => ['class' => 'desktop_hidden'],
                'contentOptions' => function ($model) {
                    return ['data-label' => $model->user_name, 'class' => 'desktop_hidden user_appoint', 'onclick' => 'void(0);'];
                },

                'value' => function ($model) {
                    $html = '-';

                    if ($model->next_appointment_start !== Client::NO_NEXT_APPPOINTMENT) {
                        $html = Yii::$app->formatter->asDate($model->next_appointment_start, "medium") . ' ' . Yii::$app->formatter->asTime($model->next_appointment_start, "short");

                        if ($model->nextAppointment && $model->nextAppointment->payment_status === Appointment::PAID) {
                            $html .= Html::a('<i class="fa fa-video-camera"></i>', ['appointment/video', 'id' => $model->nextAppointment->id], [
                                'class' => 'page-btn action-page-btn video-room-link',
                                'data-pjax' => 0,
                            ]);
                        } else {
                            $html .= ' <i class="fa fa-video-camera video-room-icon"></i>';
                        }
                    } else if ($model->status == Client::STATUS_PENDING) {
                        $html = Yii::t('main', 'Pending') . '...';
                    }

                    return $html;
                },
            ],
            [
                'attribute' => 'user_name',
                'header' => Yii::t('main', 'Client') . ' <span data-toggle="popover" data-placement="bottom" data-content="' . Yii::t('main', 'Client column for ' . Client::getStatusData()[$status]['alias'] . ' clients description') . '"><i class="fa fa-question-circle" aria-hidden="true"></i></span>',
                'format' => 'raw',
                'contentOptions' => ['data-label' => Yii::t('main', 'Client'), 'class' => 'user_name'],
                'value' => function ($model) {
                    if ($model->user->isDeleted()) {
                        return Html::a($model->user_name . ' <i class="fa fa-ban"></i>');
                    }
                    return Html::a($model->user_name . ' <i class="fa fa-pencil"></i>', ['customize', 'id' => $model->id], [
                            'class' => 'client-name',
                            'data-pjax' => 0,
                            'data-target' => '#modal'
                        ]
                    );
                },
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => Yii::t('main', 'Status') . ' <span data-toggle="popover" data-placement="bottom" data-content="' . Yii::t('main', 'Status column for ' . Client::getStatusData()[$status]['alias'] . ' clients description') . '"><i class="fa fa-question-circle" aria-hidden="true"></i></span>',
                'template' => Client::getStatusData()[(int)$status]['gridButtons'],
                'contentOptions' => ['data-label' => Yii::t('main', 'Status')],
                'buttons' => [
                    'appointment' => function ($url, $model, $key) {
                        return Html::a('<i class="fa fa-calendar-plus-o"></i> ' . Yii::t('main', 'Set an appointment'),
                            ['appointment', 'id' => $model->id, 'reschedule' => ($model->status == Client::STATUS_PENDING)], [
                                'class' => 'page-btn action-page-btn',
                                'data' => [
                                    'pjax' => 0,
                                    'target' => '#modal'
                                ],
                            ]);
                    },
                    'status' => function ($url, $model, $key) {
                        if ($model->nextAppointment) {
                            return Html::a('<i class="fa fa-question-circle"></i> ' . Yii::t('main', 'Appointment status'), ['appointment/mark', 'id' => $model->nextAppointment->id], [
                                'class' => 'page-btn action-page-btn orange',
                                'data' => [
                                    'pjax' => 0,
                                    'target' => '#modal'
                                ],
                            ]);
                        }
                    },
                    'set-inactive' => function ($url, $model, $key) {
                        return Html::a('<i class="fa fa-arrow-down"></i> ' . Yii::t('main', 'Set inactive'), ['set-inactive-confirmation', 'id' => $model->id], [
                            'class' => 'page-btn action-page-btn grey',
                            'data' => [
                                'pjax' => 0,
                                'target' => '#modal'
                            ],
                        ]);
                    },
                ],
            ],
            [
                'label' => Yii::t('main', 'Messages'),
                'format' => 'html',
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center messages', 'data-label' => Yii::t('main', 'Messages')],
                'value' => function ($model) {
                    return Html::a('<i class="fa fa-envelope"></i> ' . (count($model->newMessages) ? '<span class="badge small-conversation-badge">' . count($model->newMessages) . '</span>' : ''), ['messages/conversation', 'id' => $model->conversation_id], [
                        'class' => 'btn btn-default btn-sm',
                        'data-pjax' => 0,
                    ]);
                },
            ],
            [
                'header' => Yii::t('main', 'Next appointment') . ' / ' . Yii::t('main', 'Online room') . ' <span data-toggle="popover" data-placement="bottom" data-content="' . Yii::t('main', 'Appointment column for ' . Client::getStatusData()[$status]['alias'] . ' clients description') . '"><i class="fa fa-question-circle" aria-hidden="true"></i></span>',
                'format' => 'raw',
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center appointment', 'data-label' => Yii::t('main', 'Next appointment')],
                'value' => function ($model) {
                    $html = '-';

                    if ($model->next_appointment_start !== Client::NO_NEXT_APPPOINTMENT) {
                        $html = Timezone::convert($model->next_appointment_start, Yii::$app->user->identity->timezone, 'd.m.Y, H:i');

                        if ($model->nextAppointment && $model->nextAppointment->payment_status === Appointment::PAID) {
                            $html .= Html::a('<i class="fa fa-video-camera"></i>', ['appointment/video', 'id' => $model->nextAppointment->id], [
                                'class' => 'page-btn action-page-btn video-room-link',
                                'data-pjax' => 0,
                            ]);
                        } else {
                            $html .= ' <i class="fa fa-video-camera video-room-icon"></i>';
                        }
                    } else if ($model->status == Client::STATUS_PENDING) {
                        $html = Yii::t('main', 'Pending') . '...';
                    }

                    return $html;
                },
            ],
            [
                'header' => Yii::t('main', 'Payment') . ' <span data-toggle="popover" data-placement="bottom" data-content="' . Yii::t('main', 'Payment column for ' . Client::getStatusData()[$status]['alias'] . ' clients description') . '"><i class="fa fa-question-circle" aria-hidden="true"></i></span>',
                'format' => 'html',
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center payment', 'data-label' => Yii::t('main', 'Payment')],
                'value' => function ($model) {
                    $html = '-';

                    if ($model->status != Client::STATUS_INACTIVE && $model->nextAppointment) {
                        if ($model->nextAppointment->payment_status == Appointment::NOT_PAID) {
                            $html = Yii::t('main', 'Not paid');
                        } else if ($model->nextAppointment->payment_status == Appointment::PENDING) {
                            $html = Yii::t('main', 'In progress') . '...';
                        } else {
                            $paidAppointments = PaidAppointments::getForUser($model->user_id);
                            if ($paidAppointments > 0) {
                                $paidAppointments = ' (' . ($paidAppointments + 1) . ')';
                            } else {
                                $paidAppointments = '';
                            }
                            $html = Yii::t('main', 'Paid') . $paidAppointments . ' <i class="fa fa-check"></i>';
                        }
                    }

                    return $html;
                },
            ],
            [
                'label' => Yii::t('main', 'Appointment history'),
                'format' => 'raw',
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center app-history', 'data-label' => Yii::t('main', 'Appointment history')],
                'value' => function ($model) {
                    return $this->render('_appointment_list', ['model' => $model]);
                },
            ],
        ],
    ]);
    ?>
    <?php Pjax::end(); ?>
<?php endforeach; ?>