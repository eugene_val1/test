<?php
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;

Yii::$app->assetManager->bundles['yii\bootstrap\BootstrapAsset'] = false;
Yii::$app->assetManager->bundles['yii\web\JqueryAsset'] = false;
Yii::$app->assetManager->bundles['yii\web\YiiAsset'] = false;
$this->registerJs("$('[data-toggle=\"popover\"]').popover({'trigger': 'hover'})");

?>
<?php Pjax::begin(['id' => 'pjax-new-requests', 'enablePushState' => false]); ?>
<?=
        GridView::widget([
            'id' => 'grid-new-requests',
            'dataProvider' => $dataProvider,
            'tableOptions' => ['class' => 'table table-striped table-advance table-hover'],
            'summary' => '',
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'attribute' => 'user_name',
                    'header' => Yii::t('main', 'Client')  . ' <span data-toggle="popover" data-placement="bottom" data-content="' . Yii::t('main', 'Client column for new requests description') . '"><i class="fa fa-question-circle" aria-hidden="true"></i></span>',
                    'format' => 'raw',
                    'value' => function ($model) {
                        return Html::a($model->user_name . ' <i class="fa fa-pencil"></i>', ['customize', 'id' => $model->id], [
                            'class' => 'client-name',
                            'data-pjax' => 0,
                            'data-target' => '#modal'
                        ]
                        );
                    },
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'header' => Yii::t('main', 'Status') . ' <span data-toggle="popover" data-placement="bottom" data-content="' . Yii::t('main', 'Status column for new requests description') . '"><i class="fa fa-question-circle" aria-hidden="true"></i></span>',
                    'template' => '{appointment} {remove-client}',
                    'buttons' => [
                        'appointment' => function ($url, $model, $key) {
                            return Html::a('<i class="fa fa-calendar-plus-o"></i> ' . Yii::t('main', 'Set an appointment'), $url, [
                                'class' => 'page-btn action-page-btn',
                                'data' => [
                                    'pjax' => 0,
                                    'target' => '#modal'
                                ],
                            ]);
                        },
                        'remove-client' => function ($url, $model, $key) {
                            return Html::a('<i class="fa fa-trash-o"></i> ' . Yii::t('backend', 'Delete'), ['remove-client-confirmation', 'id' => $model->id], [
                                'class' => 'page-btn action-page-btn red',
                                'data' => [
                                    'pjax' => 0,
                                    'target' => '#modal'
                                ],
                            ]);
                        },
                    ],
                ],
                [
                    'label' => Yii::t('main', 'Messages'),
                    'format' => 'html',
                    'headerOptions' => ['class' => 'text-center'],
                    'contentOptions' => ['class' => 'text-center'],
                    'value' => function ($model) {
                        return Html::a('<i class="fa fa-envelope"></i> ' . (count($model->newMessages) ? '<span class="badge small-conversation-badge">' . count($model->newMessages) . '</span>' : ''), ['messages/conversation', 'id' => $model->conversation_id], [
                                'class' => 'btn btn-default btn-sm',
                                'data-pjax' => 0,
                            ]);
                    },
                ],
            ],
        ]);
?>
<?php Pjax::end(); ?>