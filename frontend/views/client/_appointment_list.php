<?php

use common\models\Appointment;
use common\models\Timezone;
use yii\helpers\Html;

/** @var Appointment $model */

?>

<?php if (($count = count($model->completedAppointments)) > 0): ?>
<div class="appointment-list collapse-item <?= ($count > 5) ? 'appointment-list-scroll' : '' ?>">

    <?php if ($count > 1): ?>
        <a class="show-appointments pull-right" data-toggle="collapse"
           data-target="#appointments-<?= $model->id ?>">
            <i class="fa fa-angle-down"></i>
        </a>
    <?php endif; ?>

    <?php foreach ($model->completedAppointments as $i => $appointment): ?>
        <?= ($i === 1) ? '<div id="appointments-' . $model->id . '" class="collapse appointment-list-collapse">' : '' ?>
        <p>
            <?=
            Html::a('#' . $appointment->number . '. ' . Timezone::convert($appointment->start, Yii::$app->user->identity->timezone, 'd.m.Y') . ' <i class="fa fa-pencil ' . (empty($appointment->comment) ? '' : 'edited') . '"></i>', ['appointment/update', 'id' => $appointment->id], [
                'class' => 'appointment-detail',
                'data-pjax' => 0,
                'data-target' => '#modal'
            ]);
            ?>
        </p>
    <?php endforeach; ?>

    <?= ($count > 1) ? '</div>' : '' ?>
</div>
<?php endif; ?>


