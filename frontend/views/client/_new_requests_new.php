<?php

use common\models\CountryLang;
use yii\data\Pagination;
use common\models\Client;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;
use yii\widgets\Pjax;

/** @var Client[] $models */
/** @var Pagination $pages */

$this->registerJs("$('[data-toggle=\"popover\"]').popover({'trigger': 'hover'})");

?>

<div class="table-head">
    <div class="client">Клиент</div>
    <div class="messages">Сообщения</div>
    <div class="status">Статус клиента</div>
    <div class="country">Страна</div>
    <div class="time-different">Разница во времени</div>
</div>

<div class="table-body">
    <?php Pjax::begin(['id' => 'new-requests-list', 'enablePushState' => false]) ?>
    <?php foreach ($models as $model): ?>
        <?php /** @var Client $model */ ?>
        <?php $messagesCount = count($model->newMessages); ?>

        <div class="client-row">
            <div class="mob-head">
                <div class="client">
                    <div class="img-wrap">
                        <?= Html::a('<img src="' . $model->user->getFileUrl('image') . '" alt="' . $model->user_name . '">',
                            ['customize', 'id' => $model->id],
                            ['data-pjax' => 0, 'data-target' => '#modal']
                        ); ?>
                    </div>
                    <div class="name" data-toggle="popover" data-placement="bottom" data-content="<?= $model->user_name ?>">
                        <?= $model->user_name ?>
                    </div>
                </div>
                <div class="messages">
                    <div class="messages">
                        <a href="<?= Url::to(['messages/conversation', 'id' => $model->conversation_id]) ?>" data-pjax="0">
                            <div class="message">
                                <?php if($messagesCount): ?>
                                    <div class="quantity-label"><?= $messagesCount ?></div>
                                <?php endif; ?>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            <div class="row-content">
                <div class="client mob-hidden">
                    <div class="img-wrap">
                        <?= Html::a('<img src="' . $model->user->getFileUrl('image') . '" alt="' . $model->user_name . '">',
                            ['customize', 'id' => $model->id],
                            ['data-pjax' => 0, 'data-target' => '#modal']
                        ); ?>
                    </div>
                    <div class="name" data-toggle="popover" data-placement="bottom" data-content="<?= $model->user_name ?>">
                        <?php
                            echo (function($model) {
                                if ($model->user->isDeleted()) {
                                    return Html::a($model->user_name . ' <i class="fa fa-ban"></i>');
                                }

                                return Html::a($model->user_name . ' <i class="fa fa-pencil"></i>',
                                    ['customize', 'id' => $model->id],
                                    ['data-pjax' => 0, 'data-target' => '#modal']
                                );
                            })($model);
                        ?>
                    </div>
                </div>

                <div class="messages mob-hidden">
                    <a href="<?= Url::to(['messages/conversation', 'id' => $model->conversation_id]) ?>" data-pjax="0">
                        <div class="message">
                            <?php if($messagesCount): ?>
                                <div class="quantity-label"><?= $messagesCount ?></div>
                            <?php endif; ?>
                        </div>
                    </a>
                </div>

                <div class="status">
                    <span class="desktop-hidden">Статус</span>
                    <div>
                        <?= (function ($model) {
                            return Html::a(Yii::t('main', 'Set an appointment'), ['appointment', 'id' => $model->id], [
                                'class' => 'table-btn-blue',
                                'data' => [
                                    'pjax' => 0,
                                    'target' => '#modal',
                                ],
                            ]);
                        })($model) ?>

                        <?= (function ($model) {
                            return Html::a(Yii::t('backend', 'Delete'), ['remove-client-confirmation', 'id' => $model->id], [
                                'class' => 'table-btn-red',
                                'data' => [
                                    'pjax' => 0,
                                    'target' => '#modal',
                                ],
                            ]);
                        })($model) ?>
                    </div>
                </div>

                <div class="country">
                    <span class="desktop-hidden">Страна</span>
                    <?php if ($model->innerUser): ?>
                        <?= CountryLang::findOne([
                            'country_id' => $model->innerUser->country_id,
                            'language' => Yii::$app->language,
                        ])->name ?>
                    <?php else: ?>
                        -
                    <?php endif; ?>
                </div>

                <div class="time-different">
                    <span class="desktop-hidden">Пояс</span>
                    <?php if ($model->innerUser): ?>
                        <?= $model->innerUser->getTimezoneOffset(Yii::$app->user->identity->timezone) ?> часа
                    <?php else: ?>
                        -
                    <?php endif; ?>
                </div>
            </div>
        </div>
    <?php endforeach; ?>

    <?php
        echo LinkPager::widget([
            'pagination' => $pages,
        ]);
    ?>

    <?php Pjax::end() ?>
</div>