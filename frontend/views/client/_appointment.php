<?php

/* @var $this \yii\web\View */

use talma\widgets\FullCalendar;
use common\models\Language;
use yii\web\JsExpression;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Client;
use yii\helpers\ArrayHelper;

Yii::$app->assetManager->bundles['yii\bootstrap\BootstrapAsset'] = false;
Yii::$app->assetManager->bundles['yii\web\YiiAsset'] = false;
//HACK: re-render calendar to show correct Date slots
$this->registerJs('$(document).ready(function(){setTimeout(function(){$("#calendar").fullCalendar("next");$("#calendar").fullCalendar("prev")},500)})');
?>

<div class="appointment">
    <h3><?= Yii::t('main', 'New appointment') ?></h3>
    <div class="form-wrapper">
    <?php $form = ActiveForm::begin(
        [
            'id' => 'appointment-form', 
            'options' => [
                'data-callback' => 'afterAppointment',
                'data-reload-grid' => ArrayHelper::keyExists($client->status, Client::getStatusData()) ? Client::getStatusData()[$client->status]['alias'] : false
                ]
        ]); ?>
    <div class="row">
        <div class="col-lg-12">
            <?php if (Yii::$app->settingManager->get('allowFreeAppointment', false) && $appointment->therapist->free_appointment): ?>
                <?= $form->field($appointment, 'free', ['template' => '{input}{error}'])->checkbox(); ?>
            <?php endif; ?>
            <?= $form->field($appointment, 'start', ['template' => '{input}{error}'])->hiddenInput(['id' => 'appointment-start']); ?>
            <?= $form->field($appointment, 'end', ['template' => '{input}'])->hiddenInput(['id' => 'appointment-end']); ?>
            <?= FullCalendar::widget([
                'options' => [
                    'id' => 'calendar'
                ],
                'config' => [
                    'header' => [
                        'left'=>'title',
                        'right'=>'prev,next today',
                    ],
                    'googleCalendar' => true,
                    'allDaySlot' => false,
                    'lang' => Language::getCurrent()->url,
                    'defaultView' => 'agendaWeek',
                    'slotDuration' => '00:15:00',
                    'defaultTimedEventDuration' => $appointment->getTimedEventDuration(),
                    'forceEventDuration' => true,
                    'selectable' => true,
                    'select' => new JsExpression("function( start, end, jsEvent, view ){
                        var calendar = $('#calendar');
                        calendar.fullCalendar('changeView', 'agendaWeek');
                        var startInput = $('#appointment-start');
                        var endInput = $('#appointment-end');
                        calendar.fullCalendar('removeEvents');
                        calendar.fullCalendar('renderEvent',{title: '', start: start}, true);
                        startInput.val(start.unix());
                        endInput.val(start.clone().add(moment.duration(calendar.fullCalendar('option', 'defaultTimedEventDuration'))).unix());
                    }")
                ],
            ]); ?> 
        </div>
        <div class="col-lg-12">
            <div class="form-group form-submit text-center">
                <?= Html::submitButton(Yii::t('main', 'Save'), ['class' => 'small-page-btn page-btn orange btn-submit', 'name' => 'appointment-button']) ?>
            </div>     
        </div>
    </div>
    <?php ActiveForm::end(); ?>
    </div>
</div>