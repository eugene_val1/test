<?php

use yii\helpers\Html;
use common\models\Appointment;

Yii::$app->assetManager->bundles['yii\bootstrap\BootstrapAsset'] = false;
Yii::$app->assetManager->bundles['yii\web\JqueryAsset'] = false;
Yii::$app->assetManager->bundles['yii\web\YiiAsset'] = false;
?>

<div class="remove-confirmation">
    <h3><?= Yii::t('main', 'Please, confirm setting client status to inactive') ?></h3>
    <div class="row">
        <div class="col-lg-12">
            <p>
                <?php if($client->nextAppointment->payment_status == Appointment::PAID && $client->nextAppointment->safePeriod() > time()): ?>
                    <?= Yii::t('main', 'Client has appointment with paid status. You will be able to complete this appoinment and inactivate client after') . ' ' . Yii::$app->formatter->asDate($client->nextAppointment->safePeriod(), "medium") ?>
                <?php else: ?>
                <img width="30px" src="<?= $client->user->getFileUrl('image'); ?>" class="img-circle" alt="">
                <span><?= $client->user->name ?></span>
                <?= Html::a(Yii::t('backend', 'Cancel'), '#', ['class' => 'btn btn-default btn-sm pull-right close-modal']); ?>
                <?= Html::a(Yii::t('backend', 'Yes'), ['client/set-inactive', 'id' => $client->id], 
                    [
                        'class' => 'btn btn-danger btn-sm inactive-client pull-right',
                        'data-reload-grid' => 'pending'
                    ]); ?>
                <?php endif; ?>
            </p>
        </div>
    </div>
</div>