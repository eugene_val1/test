<?php

/** @var \common\models\User $user */

?>

<div class="col-md-10 col-sm-12">
    <h3>Вы успешно зарегистрировались на платформе. Спасибо!</h3>
    <p>
        Теперь вам доступны все разделы сайта, вы можете отправлять сообщения терапевтам и записываться на онлайн-консультации.
    </p>

    <?php if ($user->profile->agree_mailing): ?>
        <a href="/therapists" class="page-btn activate-btn">Выбрать терапевта</a>
    <?php else: ?>
        <p>
            Чтобы быть в курсе всех новостей проекта, подпишитесь на рассылку. В рассылке мы собираем самую полезную информацию о новом функционале платформы, наших статьях и видео.
        </p>
    <?php endif; ?>
</div>