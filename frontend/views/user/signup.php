<?php

(new \frontend\components\seo\ViewMetaHelper())
    ->setTitle('Зарегестрироваться на платформе')
    ->registerDescription('Зарегистрироваться на платформе психотерапии онлайн. Консультации психологов и психотерапевтом онлайн.');

?>

<div class="content-page">
    <div class="container">
        <div class="page-inner">
            <?php if ($message): ?>
                <div class="message-wrapper">
                    <h4 class="form-title"><?= Yii::t('main', 'Sign up') ?></h4>
                    <div class="alert alert-success">
                        <?= $message; ?>
                    </div>
                </div>
            <?php else: ?>
                <?= $this->render('_signupForm', ['user' => $user, 'profile' => $profile]); ?>
            <?php endif; ?>
        </div>
    </div>
</div>