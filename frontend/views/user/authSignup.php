<?php

use common\models\CountryLang;
use common\models\Timezone;
use frontend\models\AuthSignupForm;
use kartik\widgets\Select2;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/** @var $model AuthSignupForm */

$this->title = Yii::t('main', 'Auth signup');

?>

<div class="content-page">
    <div class="container">
        <div class="page-inner">

            <?php $form = ActiveForm::begin(['id' => 'auth-signup-form']); ?>
            <div class="row">

                <?= $this->render('_finishSignupText', [
                    'user' => $model->getUser(),
                ]); ?>

                <div class="col-md-6 col-sm-12 center-block">
                    <?php if (empty($model->getUser()->email)): ?>
                        <br/>
                        <?= $form->field($model, 'email')
                            ->textInput(['readonly' => !empty($model->getUser()->email)])
                            ->label('Ваш email недоступен. Добавьте новый чтобы продолжить')
                        ?>
                    <?php endif; ?>

                    <?php if (!$model->getUser()->profile->agree_mailing): ?>
                        <?= $form->field($model, 'agree_mailing')
                            ->checkbox()
                            ->label('Хочу получать рассылку с новостями проекта')
                        ?>
                    <?php endif; ?>

                    <div class="form-group">
                        <?= Html::submitButton(Yii::t('main', 'Save'), [
                                'class' => 'small-page-btn page-btn orange btn-submit activate-btn',
                                'name' => 'signup-finish-button',
                        ]) ?>
                    </div>
                </div>
            </div>
            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>