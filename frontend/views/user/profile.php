<?php

use common\models\Profile;
use yii\helpers\Url;

/** @var Profile $profile */

$this->title = $this->title . ' | ' . $profile->name;

?>

<div class="content-page container">
    <div class="page-inner">
        <div class="row">
            <div class="text-center">
                <img class="img-circle" src="<?= $profile->getFileUrl('image') ?>" alt="">
                <h3><?= $profile->name ?></h3>
                <a data-target="#modal" href="<?= Url::toRoute(['messages/send', 'id' => $profile->user_id]); ?>" class="page-btn small-page-btn"><?= Yii::t('main', 'Send message') ?></a>
            </div>
        </div>
    </div>
</div>