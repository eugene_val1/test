<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
?>

<div class="row">
    <div class="<?= (Yii::$app->request->isAjax) ? 'col-lg-12' : 'col-lg-offset-3 col-lg-6' ?>">
        <h4 class="form-title"><?= Yii::t('main', 'Restore password') ?></h4>
        <p class="restore-help"><?= Yii::t('main', "Please fill out your email. A link to reset password will be sent there.") ?></p>
        <div class="form-wrapper">
        <?php $form = ActiveForm::begin(['id' => 'request-password-reset-form']); ?>
        <?= $form->field($model, 'email') ?>                
        <div class="form-group">
            <?= Html::submitButton(Yii::t('main', 'Send'), ['class' => 'small-page-btn page-btn orange btn-submit', 'name' => 'signup-button']) ?>
        </div>
        <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
<div class="text-center">
    <?= Yii::t('main', "Did you remember your password?") ?> - <a <?= (Yii::$app->request->isAjax) ? 'data-target="#modal"' : '' ?> href="<?= Url::toRoute(['login']); ?>"><?= Yii::t('main', 'Login') ?></a>
</div>