<?php

$this->title = Yii::t('main', 'Restore password');
?>

<div class="content-page">
    <div class="container">
        <div class="page-inner">
            <?php if ($message): ?>
            <div class="message-wrapper">
                <h4 class="form-title"><?= Yii::t('main', 'Restore password') ?></h4>
                <div class="alert alert-success">
                <?= $message; ?>
                </div>
            </div>
            <?php else: ?>
                <?= $this->render('_requestPasswordResetForm', ['model' => $model]); ?>
            <?php endif; ?>
        </div>
    </div>
</div>