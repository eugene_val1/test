<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/* @var \yii\web\View $this */
/* @var ActiveForm $form */
/* @var \frontend\models\ResetPasswordForm $model */

$this->title = Yii::t('main', 'New password');

?>

<div class="content-page">
    <div class="container">
        <div class="page-inner">
            <div class="row">
                <div class="<?= Yii::$app->request->isAjax ? 'col-lg-12' : 'col-lg-offset-3 col-lg-6' ?>">
                    <h4 class="form-title"><?= Yii::t('main', 'New password') ?></h4>
                    <p class="restore-help"><?= Yii::t('main', 'Please choose your new password') ?>:</p>
                    <div class="form-wrapper">
                        <?php $form = ActiveForm::begin(['id' => 'reset-password-form']); ?>
                        <?= $form->field($model, 'password')->passwordInput(['data-toggle' => 'password']) ?>
                        <div class="form-group">
                            <?= Html::submitButton(Yii::t('main', 'Save'), ['class' => 'small-page-btn page-btn orange btn-submit', 'name' => 'signup-button']) ?>
                        </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>