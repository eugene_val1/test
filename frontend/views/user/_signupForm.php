<?php

use common\models\Profile;
use common\models\User;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\authclient\widgets\AuthChoice;
use yii\helpers\Url;

/** @var User $user */
/** @var Profile $profile */

if (Yii::$app->request->isAjax) {
    Yii::$app->assetManager->bundles['yii\web\JqueryAsset'] = false;
}

?>

<div class="form-wrapper">
    <?php $form = ActiveForm::begin(['id' => 'signup-form']); ?>
    <div class="row">

        <div class="col-lg-6">
            <h4 class="form-title"><?= Yii::t('main', 'Sign up') ?></h4>
            <?= $form->field($profile, 'name')->textInput() ?>
            <?= $form->field($user, 'email')->textInput() ?>
            <?= $form->field($user, 'user_password')->passwordInput(['data-toggle' => 'password']) ?>
            <div class="captcha-block">
                <?= $form->field($user, 'cptch', ['enableAjaxValidation' => false])
                    ->widget(\yii\captcha\Captcha::className(), []) ?>
            </div>
        </div>

        <div class="col-lg-6">
            <h4><?= Yii::t('main', 'Sign in with') ?></h4>
            <?php $authAuthChoice = AuthChoice::begin([
                    'baseAuthUrl' => ['user/auth'],
                    'popupMode' => false,
                ]);
            ?>
                <div class="social-accounts row">
                    <?php foreach ($authAuthChoice->getClients() as $client): ?>
                        <div class="col-xs-12 col-lg-6 social-account-item">
                            <?php $authAuthChoice->clientLink($client, '<i class="fa fa-' . $client->getName() . '"></i> ' . Yii::t('main', $client->getTitle()),
                                ['class' => 'page-btn btn-md btn-block social-account-item ' . $client->getName() . '-client']
                            ) ?>
                        </div>
                    <?php endforeach; ?>
                </div>
            <?php AuthChoice::end(); ?>
            <p class="text-muted">
                <?= Yii::t('main', 'I accept site {terms}', [
                    'terms' => Html::a(Yii::t('main', 'Terms and conditions'),
                        ['/page/terms'],
                        ['target' => '_blank']
                    ),
                ]) ?>
            </p>
        </div>

        <div class="col-lg-12">
            <?= $form->field($user, 'agree_mailing')
                ->checkbox()
                ->label(Yii::t('main', 'I agree to receive project news on email'))
            ?>
            <div class="form-group">
                <?= Html::submitButton(Yii::t('main', 'Send'), [
                    'class' => 'small-page-btn page-btn orange btn-submit register-btn',
                    'name' => 'signup-button',
                ]) ?>
            </div>
        </div>

    </div>    
</div>
<?php ActiveForm::end(); ?>

<div class="text-center">
    <?= Yii::t('main', 'Have an account?') ?> - <a <?= (Yii::$app->request->isAjax) ? 'data-target="#modal"' : '' ?> data-reload-on-hide="1" href="<?= Url::toRoute(['login']); ?>"><?= Yii::t('main', 'Login') ?></a>
</div>