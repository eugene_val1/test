<?php

use yii\helpers\Url;

?>

<div class="row">
    <h3><?= Yii::t('main', 'Please, login or register') ?></h3>
    <div class="col-lg-12">        
        <?= Yii::t('main', 'You need to login or register and accept terms and conditions to contact therapists') ?>
    </div>
    <div class="col-lg-12 text-center">
        <a class="page-btn small-page-btn register-btn-popup" data-target="#modal" data-reload-on-hide="1" href="<?= Url::toRoute(['signup']); ?>"><?= Yii::t('main', 'Sign up') ?></a>
    </div>
</div>
<div class="text-center">
    <?= Yii::t('main', 'Have an account?') ?> - <a data-target="#modal" href="<?= Url::toRoute(['login']); ?>"><?= Yii::t('main', 'Login') ?></a>
</div>