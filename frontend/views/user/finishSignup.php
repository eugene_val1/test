<?php

use frontend\models\AuthSignupForm;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/** @var $model AuthSignupForm */
/** @var \yii\web\View $this */

$this->title = Yii::t('main', 'Auth signup');

?>

<div class="content-page">
    <div class="container">
        <div class="page-inner">
            <?php $form = ActiveForm::begin(['id' => 'finish-signup-form']); ?>
            <div class="row">

                <?= $this->render('_finishSignupText', [
                        'user' => $model->getUser(),
                ]); ?>

                <?php if (!$model->agree_mailing): ?>
                    <div class="col-lg-6 col-sm-12">
                        <?= $form->field($model, 'agree_mailing')
                            ->checkbox()
                            ->label('Хочу получать рассылку с новостями проекта')
                        ?>

                        <div class="form-group">
                            <?= Html::submitButton(Yii::t('main', 'Save'), [
                                'class' => 'small-page-btn page-btn orange btn-submit activate-btn',
                                'name' => 'signup-finish-button',
                            ]) ?>
                        </div>
                    </div>
                <?php endif; ?>

            </div>
            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>