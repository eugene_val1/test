<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\authclient\widgets\AuthChoice;
use yii\helpers\Url;

?>

<div class="row">

    <div class="col-lg-6">
        <h4><?= Yii::t('main', 'Login') ?></h4>
        <?php $form = ActiveForm::begin([
            'id' => 'login-form',
            'fieldConfig' => [
                'errorOptions' => ['encode' => false],
            ],]); ?>
        <?= $form->field($model, 'username') ?>
        <?= $form->field($model, 'password')->passwordInput() ?>
        <?= $form->field($model, 'rememberMe')->checkbox() ?>
        <div class="form-group">
            <?= Html::submitButton(Yii::t('main', 'Login'), ['class' => 'small-page-btn page-btn orange btn-submit', 'name' => 'signup-button']) ?>
            <a class="restore-link" <?= Yii::$app->request->isAjax ? 'data-target="#modal"' : '' ?>
               href="<?= Url::toRoute(['request-password-reset']); ?>"><?= Yii::t('main', 'Forgot password?') ?></a>
        </div>
        <?php ActiveForm::end(); ?>
    </div>

    <div class="col-lg-6">
        <h4><?= Yii::t('main', 'Sign in with') ?></h4>
        <?php $authAuthChoice = AuthChoice::begin([
            'baseAuthUrl' => ['user/auth'],
            'popupMode' => false
        ]); ?>
        <div class="social-accounts row">
            <?php foreach ($authAuthChoice->getClients() as $client): ?>
                <div class="col-xs-12 col-lg-6 social-account-item">
                    <?php $authAuthChoice->clientLink($client, '<i class="fa fa-' . $client->getName() . '"></i> ' . Yii::t('main', $client->getTitle()),
                        ['class' => 'page-btn btn-md btn-block social-account-item ' . $client->getName() . '-client']
                    ) ?>
                </div>
            <?php endforeach; ?>
        </div>
        <?php AuthChoice::end(); ?>
        <p class="text-muted">
            <?= Yii::t('main', 'I accept site {terms}', [
                'terms' => Html::a(Yii::t('main', 'Terms and conditions'),
                    ['/page/terms'],
                    ['target' => '_blank']
                ),
            ]) ?>
        </p>
    </div>
</div>

<div class="text-center">
    <?= Yii::t('main', "Don't have an account?") ?> -
    <a <?= Yii::$app->request->isAjax ? 'data-target="#modal"' : '' ?>
            data-reload-on-hide="1" href="<?= Url::toRoute(['signup']); ?>"><?= Yii::t('main', 'Sign up') ?></a>
</div>