<?php

use common\widgets\Alert;

/** @var \yii\web\View $this */

(new \frontend\components\seo\ViewMetaHelper())
    ->setTitle('Вход в личный кабинет')
    ->registerDescription('Вход в личный кабинет пользователя Тритфилд. Видео консультации с психологом, психотерапевтом. Оплата сессий онлайн.')

?>

<div class="content-page">
    <div class="container">
        <div class="page-inner">
        <?= Alert::widget(); ?>
        <?= $this->render('_loginForm', ['model' => $model]); ?>
        </div>
    </div>
</div>