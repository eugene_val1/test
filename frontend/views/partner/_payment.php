<?php

use yii\helpers\Html;

Yii::$app->assetManager->bundles['yii\bootstrap\BootstrapAsset'] = false;
Yii::$app->assetManager->bundles['yii\web\JqueryAsset'] = false;
Yii::$app->assetManager->bundles['yii\web\YiiAsset'] = false;
?>

<div class="col-lg-12">
    <h4><?= Yii::t('main', 'Billing information') ?></h4>
    <div id="partner-data">
        <div class="row">
            <div class="col-lg-12">
            <?php if($partner): ?>
                <table class="table table-striped table-advance table-hover">
                    <thead>
                    <tr>
                        <th><?= Yii::t('main', 'Card number') ?></th>
                        <th><?= Yii::t('main', 'Expired') ?></th>
                        <th></th>
                    </tr>
                    </thead>
                    <tr>
                        <td><?= $partner->card_number ?></td>
                        <td><?= $partner->month . "/" . $partner->year ?></td>
                        <td>
                            <?= Html::a('<i class="fa fa-trash"></i> ' . Yii::t('main', 'Remove'), ['partner/remove-confirmation'], [
                                'class' => 'btn btn-danger btn-sm pull-right',
                                'data' => [
                                    'pjax' => 0,
                                    'target' => '#modal'
                                ],
                            ]); ?>
                            <?= Html::a('<i class="fa fa-pencil"></i> ' . Yii::t('main', 'Update billing information'), ['partner/update'], [
                                'class' => 'btn btn-primary btn-sm pull-right update-partner',
                                'data' => [
                                    'pjax' => 0,
                                    'target' => '#modal'
                                ],
                            ]); ?>                            
                        </td>
                    </tr>
                </table>
            <?php else: ?>
                <p><?= Yii::t('main', 'You have to add billing information for creating appoinments and receiving payments') ?></p>
            <?php endif;?>
            </div>
        </div>
        <?php if(!$partner): ?>
        <?= Html::a('<i class="fa fa-plus"></i> ' . Yii::t('main', 'Add billing information'), ['partner/create'], [
            'class' => 'btn btn-primary btn-sm pull-right add-partner',
            'data' => [
                'pjax' => 0,
                'target' => '#modal'
            ],
        ]); ?>        
        <?php endif;?>
    </div>    
</div>