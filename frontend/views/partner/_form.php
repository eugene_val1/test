<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\models\Partner;
?>

<div class="add-partner">
    <h3><?= $model->isNewRecord ? Yii::t('main', 'Add billing information') : Yii::t('main', 'Update billing information') ?></h3>
    <div class="form-wrapper">
        <?php $form = ActiveForm::begin([
            'id' => 'partner-form',
            'options' => [
                'data-callback' => 'afterUpdatePartner'
            ]
        ]); ?>      
        <div class="row">
            <div class="col-sm-12">
                <?= $form->errorSummary($model); ?>
                <?= $form->field($model, 'phone')->textInput() ?>
                <?= $form->field($model, 'card_holder')->textInput() ?>
                <?= $form->field($model, 'card_number')->textInput() ?>
                <div class="form-group">
                    <div>
                        <label class="control-label"><?= $model->getAttributeLabel('expired') ?></label>
                    </div>
                    <?= $form->field($model, 'month', ['template' => '{input}', 'options' => ['class' => 'inline-form-group']])
                            ->dropDownList(
                                Partner::getMonths(),
                                ['prompt' => Yii::t('main', 'Month')]
                            ); 
                    ?>
                    <?= $form->field($model, 'year', ['template' => '{input}', 'options' => ['class' => 'inline-form-group']])
                            ->dropDownList(
                                Partner::getYears(),
                                ['prompt' => Yii::t('main', 'Year')]
                            ); 
                    ?>
                    <?= $form->field($model, 'expired', ['template' => '{input}{error}'])->hiddenInput(); ?>
                </div>
                <?= $form->field($model, 'cvv')->passwordInput() ?>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group form-submit">
                    <?= Html::submitButton(Yii::t('main', 'Save'), ['class' => 'small-page-btn page-btn orange btn-submit', 'name' => 'save-button']) ?>
                </div>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>