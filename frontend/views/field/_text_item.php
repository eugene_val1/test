<?php

use yii\helpers\Url;
use common\models\Article;

/** @var int $index */
/** @var Article $model */
/** @var boolean $isWideBlock */
/** @var bool $showAuthor */

$showAuthor = !isset($showAuthor) || $showAuthor === true;

?>

<div class="text-item item <?= $isWideBlock ? 'wide' : ''; ?>">
    <a href="<?= Url::toRoute(['field/view', 'alias' => $model->alias]); ?>" class="wrapper">
        <img src="<?= $model->getFileUrl('image'); ?>" alt="<?= $model->image_alt ?>">
        <div class="item-overlay-wrapper"><div class="item-overlay"></div></div>
        <div class="content">
            <?php if ($showAuthor !== false): ?>
                <h6><?= $model->authorName; ?></h6>
            <?php endif; ?>
            <h4><?= $model->title; ?></h4>
            <div class="content-hide">
                <div class="item-meta">
                    <i class="fa fa-eye"></i> <span><?= $model->views_count; ?></span>
                    <i class="fa fa-thumbs-up"></i> <span><?= $model->votes_count; ?></span>
                    <i class="fa fa-commenting"></i> <span><?= $model->comments_count; ?></span>
                </div>
                <p class="item-description"><?= $model->short_description; ?></p>
            </div>
        </div>
    </a>
</div>