<?php
$this->title .= ' | ' . $page->title;
?>

<div class="field-page container-fluid">
    <div class="category-page">
        <div class="category-cover page-cover" <?= ($page && $page->image) ? 'style="background-image: url(' . $page->getFileUrl('image') . ')"' : ''; ?>>
            <div class="custom-page-header">
                <h1><?= $page->name ?></h1>
                <div class="category-description">
                    <?= $page->content ?>
                </div>
            </div>
        </div>
    </div>
    <div class="page-inner">
        <a class="menu-btn"></a>
        <?= $this->render('_list', ['dataProvider' => $dataProvider]); ?>
    </div>
</div>
<div class="side-nav side-menu auto-open">
    <div class="nav-inner">
        <a class="menu-close-btn"></a>
        <?= $this->render('_filter', ['filterModel' => $filterModel]); ?>
    </div>
    <div class="nav-footer"></div>
</div>