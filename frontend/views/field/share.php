<?php

use ijackua\sharelinks\ShareLinks;
use \yii\helpers\Html;

?>

<?= Html::a('<i class="fa fa-facebook"></i> ' . Yii::t('main', 'Facebook'), 
    $this->context->shareUrl(ShareLinks::SOCIAL_FACEBOOK), 
    ['title' => Yii::t('main', 'Share to Facebook'), 'class' => 'share-link btn btn-facebook'])
?>
<?= Html::a('<i class="fa fa-twitter"></i> ' . Yii::t('main', 'Twitter'), 
    $this->context->shareUrl(ShareLinks::SOCIAL_TWITTER), 
    ['title' => Yii::t('main', 'Share to Twitter'), 'class' => 'share-link btn btn-twitter'])
?>
<?= Html::a('<i class="fa fa-google-plus"></i> ' . Yii::t('main', 'Google Plus'), 
    $this->context->shareUrl(ShareLinks::SOCIAL_GPLUS), 
    ['title' => Yii::t('main', 'Share to Google Plus'), 'class' => 'share-link btn btn-googleplus'])
?>