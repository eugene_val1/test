<?php

use yii\helpers\Url;
use common\models\Article;

/** @var int $index */
/** @var boolean $isWideBlock */
/** @var Article $model */
/** @var bool $showAuthor */

$showAuthor = !isset($showAuthor) || $showAuthor === true;

?>

<div class="video-item item <?= $isWideBlock ? 'wide' : ''; ?>">
    <a href="<?= Url::toRoute(['field/view', 'alias' => $model->alias]); ?>" class="wrapper">
        <img src="<?= $model->getFileUrl('image'); ?>" alt="<?= $model->image_alt ?>">
        <div class="item-overlay-wrapper"><div class="item-overlay"></div></div>
        <div class="play-btn"><i class="fa fa-play-circle"></i></div>
        <div class="content">
            <?php if ($showAuthor !== false): ?>
                <h6><?= $model->authorName; ?></h6>
            <?php endif; ?>
            <h4><?= $model->title; ?></h4>
        </div>
        <div class="video-meta content-hide">
            <div class="item-meta">
                <i class="fa fa-eye"></i> <span><?= $model->views_count; ?></span>
                <i class="fa fa-thumbs-up"></i> <span><?= $model->votes_count; ?></span>
                <i class="fa fa-commenting"></i> <span><?= $model->comments_count; ?></span>
            </div>
        </div>
    </a>
</div>