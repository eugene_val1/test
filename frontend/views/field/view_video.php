<?php

use common\models\TextArticle;
use common\models\Therapist;
use common\models\VideoArticle;
use ijackua\sharelinks\ShareLinks;
use yii\data\BaseDataProvider;
use yii\helpers\Html;
use yii\helpers\Url;

/** @var VideoArticle $article */
/** @var BaseDataProvider $articlesProvider */

$article->setMetaTags();
$this->params['navOpacity'] = false;

?>

<style>
    .page-inner {
        padding-left: 0 !important;
        padding-right: 0 !important;
    }

    .article-inner > ul {
        padding-left: 40px;
    }
</style>

<div class="content-page video-article-container container-fluid">
    <div class="page-inner">
        <div class="page-container article video-article">
            <article class="article-content article-container" itemscope="true" itemtype="http://schema.org/Article">
                <h1 class="article-title"><?= Html::encode($article->title); ?></h1>
                <div class="article-subtitle"><?= Html::encode($article->short_description); ?></div>

                <div class="stats">
                    <div class="date"><?= Yii::$app->formatter->asDate($article->created, 'long'); ?></div>
                    <div class="views">
                        <i class="fa fa-eye"></i>
                        <span><?= $article->views_count; ?></span>
                    </div>
                </div>

                <div class="article-inner" itemprop="articleBody">
                    <?= $this->render('_' . $article->type, ['article' => $article]); ?>
                </div>

                <div class="article-footer">

                    <?php if ($article->tags): ?>
                        <div class="tags">
                            <?php foreach ($article->tags as $tag): ?>
                                <a href="<?= Url::toRoute(['index', 'tag' => $tag->name]); ?>">#<?= $tag->name ?></a>
                            <?php endforeach; ?>
                        </div>
                    <?php endif; ?>

                    <?= ShareLinks::widget([
                        'viewName' => '@frontend/views/field/share_new.php',
                        'linkSelector' => '.share-link',
                    ]); ?>
                </div>
            </article>
            <div class="article-sidebar">
                <div class="article-author">
                    <div class="author-image">
                        <img alt="Психотерапевт <?= ($article->profile->type == Therapist::TYPE) ? $article->therapistLang->name : $article->profile->name; ?>"
                             src="<?= ($article->profile->type == Therapist::TYPE) ? $article->profile->getFileUrl('main_image') : $article->profile->getFileUrl('image'); ?>">
                    </div>
                    <div class="author-inf">
                        <div class="author-name" itemprop="name">
                            <?= ($article->profile->type == Therapist::TYPE) ? $article->therapistLang->name : $article->profile->name; ?>
                        </div>
                        <div class="author-desc">
                            <?= Yii::$app->formatter->asHtml($article->profile->additional->article_description) ?>
                        </div>

                        <?php $showProfileLink = !$article->profile instanceof Therapist
                            || ($article->profile instanceof Therapist && !$article->profile->isPseudoTherapist());
                        ?>
                        <?php if ($showProfileLink): ?>
                            <div class="mobile-center">
                                <a href="<?= Url::toRoute([($article->profile->type == Therapist::TYPE) ? 'therapists/view' : 'user/profile', 'alias' => $article->profile->alias]); ?>"
                                   itemprop="url" rel="author"
                                   class="page-btn small-page-btn orange">
                                    Профиль терапевта
                                </a>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>

                <div class="article-container similar-posts">
                    <div class="similar-posts-title">Смотрите также:</div>

                    <div id="masonry-container">
                        <?php foreach ($articlesProvider->models as $video): ?>
                            <div class="item wide">
                                <a href="<?= Url::toRoute(['field/view', 'alias' => $video->alias]); ?>"
                                   class="wrapper">
                                    <img src="<?= $video->getFileUrl('image'); ?>" alt="<?= $video->image_alt ?>">
                                    <div class="item-overlay-wrapper">
                                        <div class="item-overlay"></div>
                                    </div>
                                    <div class="play-btn"><i class="fa fa-play-circle"></i></div>
                                    <div class="content">
                                        <h6><?= $video->authorName; ?></h6>
                                        <h4><?= $video->title; ?></h4>
                                    </div>
                                </a>
                            </div>
                        <?php endforeach; ?>
                    </div>

                    <div class="center-container">
                        <a href="/field"
                           itemprop="url" rel="author"
                           class="page-btn small-page-btn orange">
                            Все публикации
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <a href="/therapists" class="mobile-fixed-btn hide-desktop">Выбрать терапевта</a>

</div>

<!-- Begin MailChimp Signup Form -->
<?= $this->render('/site/_mailchipm_absolute'); ?>
<!--End mc_embed_signup-->