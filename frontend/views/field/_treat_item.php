<?php

use yii\helpers\Url;
use common\models\Article;
use common\models\Therapist;

/** @var int $index */
/** @var boolean $isWideBlock */
/** @var Article $model */
/** @var bool $showAuthor */

$showAuthor = !isset($showAuthor) || $showAuthor === true;

?>

<div class="treat-item item <?= $isWideBlock ? 'wide' : ''; ?>">
    <a href="<?= Url::toRoute(['field/view', 'alias' => $model->alias]); ?>" class="wrapper">
        <img src="<?= ($model->profile->type === Therapist::TYPE) ? '/img/therapist-treat.jpg' : '/img/user-treat.jpg' ?>" alt="<?= $model->image_alt ?>">
        <div class="content">
            <div class="treat-text">
                <div class="text-wrapper">
                    <?= $model->content; ?>
                </div>
            </div>

            <?php if ($showAuthor !== false): ?>
                <div class="treat-author">
                    <img class="img-circle" alt="<?= $model->authorName; ?>" src="<?= $model->profile->getFileUrl('image'); ?>">
                    <h6 class="treat-author-name"><?= $model->authorName; ?></h6>
                </div>
            <?php endif; ?>

            <div class="treat-meta content-hide">
                <div class="item-meta">
                    <i class="fa fa-eye"></i> <span><?= $model->views_count; ?></span>
                    <i class="fa fa-thumbs-up"></i> <span><?= $model->votes_count; ?></span>
                    <i class="fa fa-commenting"></i> <span><?= $model->comments_count; ?></span>
                </div>
            </div>
        </div>       
    </a>
</div>