<?php

use common\models\Article;
use frontend\assets\MasonryAsset;
use yii\data\BaseDataProvider;
use yii\widgets\ListView;
use nirvana\infinitescroll\InfiniteScrollPager;

MasonryAsset::register($this);

/** @var BaseDataProvider $dataProvider */

$totalItemsCount = $dataProvider->getTotalCount();

?>

<?=
ListView::widget([
    'dataProvider' => $dataProvider,
    'options' => [
        'id' => 'masonry-container',
        'class' => 'page-container',
    ],
    'summary' => '',
    'emptyText' => Yii::t('main', 'No articles yet.'),
    'itemOptions' => [
        'tag' => false,
    ],
    'itemView' => function ($model, $key, $index, $widget) use ($totalItemsCount) {
        return $this->render('_' . $model->type . '_item', [
            'model' => $model,
            'index' => $index,
            'isWideBlock' => Article::isWideBlock($index, $totalItemsCount),
        ]);
    },
    'pager' => [
        'class' => InfiniteScrollPager::className(),
        'widgetId' => 'masonry-container',
        'itemsCssClass' => 'page-container',
        'contentLoadedCallback' => 'void(0)',
        'nextPageLabel' => Yii::t('main', 'Load more'),
        'linkOptions' => [
            'class' => 'btn btn-lg btn-block',
        ],
        'pluginOptions' => [
            'contentSelector' => '.page-container',
            'loading' => [
                'msgText' => Yii::t('main', 'Loading...'),
                'finishedMsg' => Yii::t('main', 'No more articles to load'),
            ],
            'behavior' => InfiniteScrollPager::BEHAVIOR_MASONRY,
        ],
    ],
]);
?>