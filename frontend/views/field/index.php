<?php

(new \frontend\components\seo\FieldViewMetaHelper(Yii::$app->request->get('tag')))
    ->setTitle(null)
    ->registerDescription(null);
?>

<div class="field-page container-fluid">
    <div class="page-inner">
        <a class="menu-btn"></a>
        <?= $this->render('_list', ['dataProvider' => $dataProvider]); ?>
    </div>
</div>
<div class="side-nav side-menu auto-open">
    <div class="nav-inner">
        <a class="menu-close-btn"></a>
        <?= $this->render('_filter', ['filterModel' => $filterModel]); ?>
    </div>
    <div class="nav-footer"></div>
</div>

<!-- Begin MailChimp Signup Form -->
<?= $this->render('/site/_mailchipm_absolute'); ?>
<!--End mc_embed_signup-->