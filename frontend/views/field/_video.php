<?php

/** @var VideoArticle $article */

use common\models\VideoArticle;

?>
<div class="article-header">
    <div class="video-container">
        <?= $article::getResponsiveVideoCode($article->video_source, $article->video_source_id, 700) ?>
    </div>
</div>
<?= $article->content; ?>