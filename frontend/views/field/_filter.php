<?php
use yii\helpers\Url;
use common\models\CategoryPage;
?>

<ul class="filter article-filter">
    <?php foreach ($filterModel->getTypes() as $type => $typeName): ?>
        <li class="filter-item <?= ($type == $filterModel->type) ? 'active-type' : '' ?>">
            <a href="<?= Url::toRoute(($type == $filterModel::TYPE_ALL) ? '/field' : '/field/' . $type) ?>"><?= $typeName ?></a>
            <?php if ($type == $filterModel::TYPE_POSTS): ?>
                <ul class="sub-filter">
                    <?php foreach (CategoryPage::find()->with('translation')->all() as $category): ?>
                        <li <?= ($filterModel->category == $category->id) ? 'class="active-sort"' : '' ?>>
                            <a href="<?= Url::toRoute('/category/' . $category->alias) ?>"><?= $category->name ?></a>
                        </li>
                    <?php endforeach; ?>
                </ul>
            <?php endif; ?>
        </li>
    <?php endforeach; ?>
    <li class="filter-item">
        <span class="filter-type"><?= Yii::t('main', 'Sort'); ?></span>
        <ul class="sub-filter">
            <li <?= (!$filterModel->sort) ? 'class="active-sort"' : '' ?>>
                <a href="<?= Url::toRoute(Yii::$app->request->pathInfo . '/') ?>"><?= Yii::t('main', 'Sort by date') ?></a>
            </li>
            <li <?= ($filterModel->sort == $filterModel::SORT_RATING) ? 'class="active-sort"' : '' ?>>
                <a href="<?= Url::toRoute([Yii::$app->request->pathInfo . '/', 'sort' => $filterModel::SORT_RATING]) ?>"><?= Yii::t('main', 'Sort by rating') ?></a>
            </li>
            <li <?= ($filterModel->sort == $filterModel::SORT_RANDOM) ? 'class="active-sort"' : '' ?>>
                <a href="<?= Url::toRoute([Yii::$app->request->pathInfo . '/', 'sort' => $filterModel::SORT_RANDOM]) ?>"><?= Yii::t('main', 'Random') ?></a>
            </li>
        </ul>
    </li>
</ul>