<?php

use common\models\Article;
use common\models\TextArticle;
use common\models\Therapist;
use frontend\components\ShareLinks;
use yii\data\BaseDataProvider;
use yii\helpers\Html;
use yii\helpers\Url;

/** @var \common\models\TreatArticle $article */
/** @var BaseDataProvider $articlesProvider */

$article->setMetaTags();
$this->params['navOpacity'] = false;

?>

<style>
    .article-inner > ul {
        padding-left: 40px;
    }
</style>

<div class="content-page article-background treat-page">
    <div class="page-inner article-container">
        <div class="page-conp iner article">
            <article class="article-content" itemscope="true" itemtype="http://schema.org/Article">
                <div class="article-inner-mobile">
                    <div class="article-inner" itemprop="articleBody">
                        <?= $this->render('_' . $article->type, ['article' => $article]); ?>
                    </div>
                </div>

                <div class="article-inner-pc">
                    <div class="article-inner" itemprop="articleBody">
                        <?= $this->render('_' . $article->type, ['article' => $article]); ?>
                    </div>
                </div>

                <div class="article-footer">

                    <?php if ($article->tags): ?>
                        <div class="tags">
                            <?php foreach ($article->tags as $tag): ?>
                                <a href="<?= Url::toRoute(['index', 'tag' => $tag->name]); ?>">#<?= $tag->name ?></a>
                            <?php endforeach; ?>
                        </div>
                    <?php endif; ?>

                    <?= ShareLinks::widget([
                        'viewName' => '@frontend/views/field/share_new.php',
                        'linkSelector' => '.share-link',
                    ]); ?>

                </div>
            </article>
        </div>
    </div>

</div>

<!-- Begin MailChimp Signup Form -->
<?= $this->render('/site/_mailchipm_absolute'); ?>
<!--End mc_embed_signup-->