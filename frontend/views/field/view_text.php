<?php

use common\models\Article;
use common\models\TextArticle;
use common\models\Therapist;
use frontend\components\ShareLinks;
use yii\data\BaseDataProvider;
use yii\helpers\Html;
use yii\helpers\Url;

/** @var TextArticle $article */
/** @var BaseDataProvider $articlesProvider */

$article->setMetaTags();
$this->params['navOpacity'] = true;

?>

<style>
    .article-inner > ul {
        padding-left: 40px;
    }
</style>

<div class="page-cover" <?= ($article && $article->cover_image) ? 'style="background-image: url(' . $article->getFileUrl('cover_image') . ')"' : ''; ?>>
    <div class="container">
        <div class="custom-page-header article-header">
            <h1><?= Html::encode($article->title); ?></h1>
            <div class="author">
                <div class="author-name" itemprop="name">
                    <?php if ($article->profile->type === Therapist::TYPE): ?>
                        <a href="/therapist/<?= $article->therapist->alias; ?>">
                            <?= $article->getAuthorName(); ?>
                        </a>
                    <?php else: ?>
                        <?= $article->getAuthorName(); ?>
                    <?php endif; ?>
                </div>
                <time class="article-created"
                      datetime="<?= Yii::$app->formatter->asDate($article->created, 'long'); ?>"
                      itemprop="datePublished"><?= Yii::$app->formatter->asDate($article->created, 'long'); ?></time>
                <div class="views">
                    <i class="fa fa-eye"></i>
                    <?= $article->views_count; ?>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="text-article-page article-background">
    <div class="page-inner article-container">
        <div class="page-conp iner article">
            <article class="article-content" itemscope="true" itemtype="http://schema.org/Article">
                <div class="article-inner-mobile">
                    <div class="article-inner" itemprop="articleBody">
                        <?= $this->render('_' . $article->type, ['article' => $article]); ?>
                    </div>
                </div>

                <div class="article-inner-pc">
                    <div class="article-inner" itemprop="articleBody">
                        <?= $this->render('_' . $article->type, ['article' => $article]); ?>
                    </div>
                </div>

                <div class="article-footer">

                    <?php if ($article->tags): ?>
                        <div class="tags">
                            <?php foreach ($article->tags as $tag): ?>
                                <a href="<?= Url::toRoute(['index', 'tag' => $tag->name]); ?>">#<?= $tag->name ?></a>
                            <?php endforeach; ?>
                        </div>
                    <?php endif; ?>

                    <?= ShareLinks::widget([
                        'viewName' => '@frontend/views/field/share_new.php',
                        'linkSelector' => '.share-link',
                    ]); ?>

                    <div class="article-author">
                        <div class="author-image">
                            <img alt="Психотерапевт <?= ($article->profile->isTherapist()) ? $article->therapistLang->name : $article->profile->name; ?>"
                                 src="<?= ($article->profile->isTherapist()) ? $article->profile->getFileUrl('main_image') : $article->profile->getFileUrl('image'); ?>">
                        </div>
                        <div class="author-inf">
                            <div class="author-name" itemprop="name">
                                <span>Автор</span>
                                <?= ($article->profile->isTherapist()) ? $article->therapistLang->name : $article->profile->name; ?>
                            </div>
                            <?php if ($article->profile->isTherapist()): ?>
                                <div class="author-desc">
                                    <?= Yii::$app->formatter->asHtml($article->profile->additional->article_description) ?>
                                </div>
                            <?php endif; ?>

                            <?php $showProfileLink = !$article->profile instanceof Therapist
                                || ($article->profile instanceof Therapist && !$article->profile->isPseudoTherapist());
                            ?>
                            <?php if ($showProfileLink): ?>
                                <div class="mobile-center">
                                    <a href="<?= Url::toRoute([($article->profile->isTherapist()) ? 'therapists/view' : 'user/profile', 'alias' => $article->profile->alias]); ?>"
                                       itemprop="url" rel="author"
                                       class="page-btn small-page-btn orange">
                                        Профиль терапевта
                                    </a>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>

                </div>
            </article>
        </div>
    </div>

    <div class="article-container similar-posts">
        <div class="similar-posts-title">Другие публикации</div>
        <?= $this->render('_list_similar', ['dataProvider' => $articlesProvider]); ?>

        <div class="center-container">
            <a href="/field"
               itemprop="url" rel="author"
               class="page-btn small-page-btn orange">
                Все публикации
            </a>
        </div>
    </div>

    <a href="/therapists" class="mobile-fixed-btn hide-desktop">Выбрать терапевта</a>

</div>

<!-- Begin MailChimp Signup Form -->
<?= $this->render('/site/_mailchipm_absolute'); ?>
<!--End mc_embed_signup-->