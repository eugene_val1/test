<?php

use frontend\components\ShareLinks;

?>

<div class="share-block">
    <div class="share-buttons">
        <div class="col">
            <a class="fb" href="<?= $this->context->shareUrl(ShareLinks::SOCIAL_FACEBOOK) ?>">
                <i class="fa fa-facebook" aria-hidden="true"></i>
                Поделиться
            </a>
        </div>
        <div class="col">
            <a class="tg" href="<?= $this->context->shareUrl(ShareLinks::SOCIAL_TELEGRAM) ?>">
                <i class="fa fa-telegram" aria-hidden="true"></i>
                Репостнуть
            </a>
        </div>
        <div class="col">
            <a class="tw" href="<?= $this->context->shareUrl(ShareLinks::SOCIAL_TWITTER) ?>">
                <i class="fa fa-twitter" aria-hidden="true"></i>
                Твитнуть
            </a>
        </div>
    </div>
    <div class="subscribe">
        <div class="title">Подпишитесь на расылку</div>
        <form action="//treatfield.us15.list-manage.com/subscribe/post?u=164f88a778c7108c77893020b&amp;id=5acfdd87e6" method="post" target="_blank">
            <input type="email" value="" name="EMAIL" id="mce-EMAIL" required/>
            <input type="submit" class="page-btn small-page-btn orange" value="<?= Yii::t('main', 'Subscribe')?>">
        </form>
    </div>
</div>
