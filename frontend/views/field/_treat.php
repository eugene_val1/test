<?php

use common\models\Article;
use common\models\TextArticle;
use common\models\Therapist;
use yii\data\BaseDataProvider;
use yii\helpers\Url;

/** @var Article $article */
/** @var BaseDataProvider $articlesProvider */

$article->setMetaTags();
$this->params['navOpacity'] = $article->type === TextArticle::TYPE;


/** @var \common\models\Article $article */

?>
<div class="article-header">
	<div class="article-author">
		<div class="author-image">
			<img alt="Психотерапевт <?= ($article->profile->type == Therapist::TYPE) ? $article->therapistLang->name : $article->profile->name; ?>"
			     src="<?= ($article->profile->type == Therapist::TYPE) ? $article->profile->getFileUrl('main_image') : $article->profile->getFileUrl('image'); ?>">
		</div>
		<div class="author-inf">
			<div class="author-name" itemprop="name">
				<span>Автор</span>
				<?= ($article->profile->type == Therapist::TYPE) ? $article->therapistLang->name : $article->profile->name; ?>
			</div>
			<div class="author-desc">
				<?= Yii::$app->formatter->asHtml($article->profile->additional->article_description) ?>
			</div>
			<div class="mobile-center">
				<a href="<?= Url::toRoute([($article->profile->type == Therapist::TYPE) ? 'therapists/view' : 'user/profile', 'alias' => $article->profile->alias]); ?>"
				   itemprop="url" rel="author"
				   class="page-btn small-page-btn orange">
					Профиль терапевта
				</a>
			</div>
		</div>
	</div>
    <div class="treat-wrapper <?= ($article->profile->type === Therapist::TYPE) ? 'therapist-treat' : 'user-treat' ?>">
        <div class="treat-inner">
            <?= $article->content; ?>
        </div>
    </div>    
</div>