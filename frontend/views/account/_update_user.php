<?php

use common\models\NotificationSettings;
use common\models\Profile;
use common\models\User;
use common\models\UserBots;
use dosamigos\fileupload\FileUpload;
use yii\web\View;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use common\models\CountryLang;
use common\models\Timezone;
use yii\helpers\ArrayHelper;
use kartik\widgets\Select2;

/** @var View $this */
/** @var User $user */
/** @var Profile $profile */
/** @var NotificationSettings $notifications */
/** @var UserBots[] $bots */

$this->registerJs("$('#password-field').password();");

$imageUploadError = "Ваше изображение больше 1 МБ, пожалуйста уменьшите размер и попробуйте еще раз. Вы можете воспользоваться онлайн сервисами для уменьшения размера фото";

?>

<div class="col-lg-4">
    <div class="user-image-wrapper">
        <div class="user-avatar">
            <img src="<?= $profile->getFileUrl('image'); ?>" class="img-circle" alt="">
        </div>
        <span class="fileinput-button">
            <span class="upload-link"><?= Yii::t('main', 'Upload photo') ?></span>
            <?=
            FileUpload::widget([
                'model' => $profile,
                'attribute' => 'imageFile',
                'url' => ['account/upload-image', 'id' => $profile->id],
                'clientOptions' => [
                    'maxFileSize' => Profile::MAX_IMAGE_SIZE,
                    'autoUpload' => true,
                ],
                'clientEvents' => [
                    'fileuploaddone' => 'function(e, data) {
                        $(".user-avatar img").attr("src",data.result);
                    }',
                    'fileuploadfail' => 'function(e, data) {
                        alert("' . $imageUploadError .'");
                    }',
                ],
            ]);
            ?>
        </span>
    </div>
    <div class="delete-profile">
        <a href="/user/delete" class="page-btn action-page-btn red" onclick="return confirm('<?= Yii::t('main', 'Are you sure? Your profile will be deleted forever') ?>')">
            <?= Yii::t('main', 'Delete profile') ?>
        </a>
    </div>
</div>

<div class="col-lg-8">
    <div class="row">
        <?php $form = ActiveForm::begin([
            'enableClientValidation' => false,
            'action' => ['account/profile'],
        ]); ?>

        <div class="col-lg-12">
            <h4><?= Yii::t('main', 'Account information') ?></h4>
            <div class="col-md-6">
                <?= $form->field($profile, 'name')->textInput() ?>
                <?= $form->field($user, 'email')->textInput() ?>
                <?= $form->field($user, 'user_password')->passwordInput(['id' => 'password-field']) ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($user, 'country_id')->widget(Select2::classname(), [
                        'data' => ArrayHelper::map(CountryLang::findAll(['language' => Yii::$app->language]), 'country_id', 'name'),
                        'options' => ['placeholder' => Yii::t('main', 'Select a country')],
                ]); ?>
                <?= $form->field($user, 'timezone')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(Timezone::findAll(['language' => Yii::$app->language]), 'timezone_id',
                        function($model, $defaultValue) {
                            return $model->name . ' (' . $model->getOffset() . ' )';
                        }),
                    'options' => ['placeholder' => Yii::t('main', 'Select a timezone')],
                ]); ?>
            </div>
        </div>

        <div class="col-lg-12">
            <h4><?= Yii::t('main', 'Notification Settings') ?></h4>
            <div class="col-lg-6">
                <?= $form->field($notifications, 'messages')->checkbox() ?>
                <?= $form->field($notifications, 'appointment_new')->checkbox() ?>
                <?= $form->field($notifications, 'appointment_rescheduled')->checkbox() ?>
                <?= $form->field($notifications, 'appointment_completed')->checkbox() ?>
            </div>
            <div class="col-lg-6">
                <?= $form->field($notifications, 'appointment_missed')->checkbox() ?>
                <?= $form->field($notifications, 'appointment_paid')->checkbox() ?>
                <?= $form->field($notifications, 'appointment_not_paid')->checkbox() ?>
                <?php if ($user->role == User::ROLE_USER): ?>
                    <?= $form->field($notifications, 'appointment_reminder')->checkbox() ?>
                <?php endif; ?>
            </div>
        </div>

        <div class="col-lg-12">
            <h4><?= Yii::t('main', 'Notification Targets') ?></h4>
            <?= $form->field($notifications, 'target_email', [
                    'options' => ['class' => 'col-md-2']
            ])->checkbox() ?>
            <?= $form->field($notifications, 'target_bots', [
                'options' => ['class' => 'col-md-2']
            ])->checkbox() ?>
        </div>

        <div class="col-md-12">
            <div class="form-group">
                <?= Html::submitButton(Yii::t('main', 'Save'), ['class' => 'small-page-btn page-btn orange btn-submit']) ?>
            </div>
        </div>

        <?php ActiveForm::end(); ?>
    </div>

    <!-- Bots -->
    <div class="row">
        <div class="col-md-12 bots-section">
            <?= $this->render('_bots', ['bots' => $bots]) ?>
        </div>
    </div>

</div>
