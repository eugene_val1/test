<?php

use common\models\NotificationSettings;
use common\models\Profile;
use common\models\User;
use common\models\UserBots;
use common\widgets\Alert;
use frontend\assets\AccountAsset;

AccountAsset::register($this);

$this->title = $this->title . ' | ' . Yii::t('main', 'Edit profile');

/**
 * @var User $user
 * @var Profile $profile
 * @var NotificationSettings $notifications
 * @var UserBots[] $bots
 */

?>

<div class="account-page container-fluid">
    <div class="page-inner">
        <div class="page-container">
            <div id="section-profile" class="row account-section">

                <div class="section-title">
                    <h2><i class="fa fa-cogs"></i> <?= Yii::t('main', 'Edit profile') ?></h2>
                </div>

                <?= Alert::widget(); ?>

                <div class="section-inner sync">
                    <?= $this->render('_update_user', [
                        'user' => $user,
                        'profile' => $profile,
                        'notifications' => $notifications,
                        'bots' => $bots,
                    ]); ?>
                </div>

            </div>
        </div>
    </div>
</div>