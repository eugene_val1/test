<?php
use vova07\imperavi\Widget;
use yii\helpers\Url;
use common\models\Language;
?>

<?= $form->field($translation, 'name')->textInput() ?>
<?= $form->field($translation, 'short_description')->widget(Widget::className(), [
    'options' => ['id' => 'short_description-' . Yii::$app->language],
    'settings' => [
        'lang' => Language::getCurrent()->url,
        'minHeight' => 200,
        'limiter' => 1000,
        'plugins' => [
            'limiter'
        ]
    ],
    'plugins' => [ 
        'lineheight' => 'frontend\assets\RedactorAsset'
    ]
]);
?>
<?php foreach(['education', 'experience', 'full_description', 'terms'] as $attribute): ?>
<?= $form->field($translation, $attribute, ['inputOptions' => ['id' => $attribute . '-' . Yii::$app->language]])->widget(Widget::className(), [
    'options' => ['id' => $attribute . '-' . Yii::$app->language],
    'settings' => [
        'lang' => Language::getCurrent()->url,
        'minHeight' => 180,
        'imageUpload' => Url::to(['/redactor/image-upload']),
        'plugins' => [
            'imagemanager',
            'video',
            'fontcolor',
            'fontfamily',
            'fontsize',
            'limiter',
        ]
    ],
    'plugins' => [ 
        'lineheight' => 'frontend\assets\RedactorAsset'
    ]
]);
?>
<?php endforeach; ?>