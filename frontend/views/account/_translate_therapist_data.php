<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;

Yii::$app->assetManager->bundles['yii\bootstrap\BootstrapAsset'] = false;
Yii::$app->assetManager->bundles['yii\web\JqueryAsset'] = false;
Yii::$app->assetManager->bundles['yii\web\YiiAsset'] = false;
?>

<div class="translate-profile">
    <h3><?= Yii::t('main', 'Translate profile information') ?></h3>
    <div class="form-wrapper">
        <?php $form = ActiveForm::begin(['enableClientValidation' => false]); ?>
        <div class="row">
            <div class="col-sm-12">
                <?= $this->render('_data_form', ['form' => $form, 'translation' => $translation]); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group form-submit">
                    <?= Html::submitButton(Yii::t('main', 'Save'), ['class' => 'small-page-btn page-btn orange btn-submit', 'name' => 'save-button']) ?>
                </div>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>