<?php

use common\models\SystemNews;
use common\models\Timezone;
use yii\data\BaseDataProvider;
use yii\data\Pagination;
use yii\widgets\LinkPager;
use yii\widgets\Pjax;

/** @var BaseDataProvider $dataProvider */
/** @var SystemNews[] $models */
/** @var Pagination $pages */
/** @var array $readNewsIds */

/**
 * @param int $date
 *
 * @return string
 */
function formatDate($date) {
    return Yii::$app->formatter->asDate(strtotime($date), 'php:d.m.Y');
}

?>

<?php Pjax::begin(['id' => 'system-news', 'enablePushState' => false]) ?>
<div class="table-body">
    <?php foreach ($models as $i => $model): ?>
        <?php /** @var SystemNews $model */ ?>
        <?php $notRead = !in_array($model->id, $readNewsIds); ?>
        <div class="new-row <?= $notRead ? 'not-read' : '' ?>">
            <div class="number">
                <div class="digit"><?= $model->id ?></div>
            </div>
            <div class="date"><?= formatDate($model->created) ?></div>
            <div class="theme"><?= $model->title ?></div>
            <div class="notifications">
                <div class="text"><?= $model->text ?></div>
            </div>
            <div class="arrow">
                <div class="icon"></div>
            </div>
        </div>
    <?php endforeach; ?>

    <?php
        echo LinkPager::widget([
            'pagination' => $pages,
        ]);
    ?>
</div>

<?php Pjax::end() ?>
