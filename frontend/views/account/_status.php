<?php
use common\models\Therapist;
use yii\helpers\Html;
?>

<?php if ($profile->publish_status == Therapist::STATUS_NEW): ?>
    <div class="alert-warning alert fade in">
        <?= Yii::t('main', 'Your account is just created. Fill and save all needed information in sections below and press submit. Your account will appear in catalog after moderation.') ?>
        <?= Html::submitButton(Yii::t('main', 'Submit account'), ['class' => 'btn btn-success btn-xs btn-submit', 'name' => 'submit-button']) ?>
    </div>
<?php elseif ($profile->publish_status == Therapist::STATUS_WAITING): ?>
    <div class="alert-info alert fade in">
        <?= Yii::t('main', 'Your account has been sent for moderation. If verification is successful, it will be published in catalog.') ?>
    </div>
<?php elseif ($profile->publish_status == Therapist::STATUS_REVIEWED): ?>
    <div class="alert-danger alert fade in">
        <?= Yii::t('main', 'Please, fix some issues with your account, check your email for details. After fixing you need to press resubmit.') ?>
        <?= Html::submitButton(Yii::t('main', 'Resubmit account'), ['class' => 'btn btn-success btn-xs btn-submit', 'name' => 'submit-button']) ?>
    </div>
<?php endif; ?>