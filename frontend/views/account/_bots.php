<?php

use common\models\UserBots;

/** @var UserBots[string] $bots */

?>

<h4><?= Yii::t('main', 'Connect messengers to stay tuned') ?></h4>

<p>
    <i class="fa fa-telegram" aria-hidden="true"></i>
    <?php if(!empty($bots['telegram'])): ?>
        <a href="<?= $bots['telegram']?>" target="_blank"> <?= Yii::t('bots', 'Telegram Bot') ?></a>
    <?php else: ?>
        <a><?= Yii::t('bots', 'Telegram Bot') ?></a>
        (<a href="/bot/delete?type=telegram"><?= Yii::t('bots', 'unsubscribe') ?></a>)
    <?php endif; ?>
    <br/>
</p>

<p>
    <i class="fa fa-facebook-official" aria-hidden="true"></i>
    <?php if(!empty($bots['facebook'])): ?>
        <a href="<?= $bots['facebook']?>" target="_blank">  <?= Yii::t('bots', 'Facebook Messenger') ?></a>
    <?php else: ?>
        <a><?= Yii::t('bots', 'Facebook Messenger') ?></a>
        (<a href="/bot/delete?type=facebook"><?= Yii::t('bots', 'unsubscribe') ?></a>)
    <?php endif; ?>
</p>
