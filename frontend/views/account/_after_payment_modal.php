<?php

use yii\web\YiiAsset;
use common\models\Transaction;
use yii\helpers\Html;

/** @var $status int */
Yii::$app->assetManager->bundles[YiiAsset::class] = false;

?>

<div class="remove-confirmation">
    <h3>
        <?php if ($status == Transaction::STATUS_SUCCESS): ?>
            <?= Yii::t('main', 'Appointment paid successfully') ?>
        <?php elseif ($status == Transaction::STATUS_CANCELED): ?>
            <?= Yii::t('main', 'Error during payment') ?>
        <?php elseif ($status == Transaction::STATUS_PENDING): ?>
            <?= Yii::t('main', 'Payment pending') ?>
        <?php elseif ($status == Transaction::STATUS_OPEN): ?>
            Вы отменили транзакцию. Сессия не оплачена
        <?php endif; ?>
    </h3>
    <div class="row">
        <div class="col-lg-12 text-center">
            <?php if ($status == Transaction::STATUS_SUCCESS): ?>
                <p><?= Yii::t('main', 'At the appointed time click on the online room button and it will open automatically. Do not forget to allow the access the camera and microphone.') ?></p>
            <?php elseif ($status == Transaction::STATUS_CANCELED): ?>
                <p><?= Yii::t('main', 'It is likely that the payment was declined on the side of your bank. Some banks reject online transactions, requiring personal confirmation from the cardholder. Please contact the technical support of your bank.') ?></p>
            <?php elseif ($status == Transaction::STATUS_PENDING): ?>
                <?= Yii::t('main', 'Usually it takes from 5 minutes to an hour. Online room will become active as soon as the payment is verified.') ?>
            <?php endif; ?>
        </div>
        <div class="col-lg-12 text-center" style="margin-top: 20px;">

            <div class="col-sm-12 col-md-6 col-md-offset-3 text-center">
                <?= Html::a(
                    Yii::t('main', 'OK'),
                    '/account',
                    [
                        'class' => 'page-btn green set-status-btn action-page-btn',
                        'data-toggle' => 'popover',
                        'data-placement' => 'bottom',
                        'data-content' => Yii::t('main', 'OK')
                    ]
                );
                ?>
            </div>

        </div>
    </div>
</div>