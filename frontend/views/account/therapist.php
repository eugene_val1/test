<?php

use frontend\assets\AccountAsset;
use yii\bootstrap\BootstrapAsset;
use yii\web\JqueryAsset;
use yii\web\View;
use yii\web\YiiAsset;

AccountAsset::register($this);

$this->registerJsFile('https://secure.wayforpay.com/server/pay-widget.js', ['position' => View::POS_HEAD]);
$this->registerJs("$('[data-toggle=\"popover\"]').popover({'trigger': 'hover'})");

Yii::$app->assetManager->bundles[BootstrapAsset::class] = false;
Yii::$app->assetManager->bundles[JqueryAsset::class] = false;
Yii::$app->assetManager->bundles[YiiAsset::class] = false;

?>

<div class="account-page container-fluid">
    <div class="page-inner">
        <div class="page-container" id="account-sections">
            <div id="section-clients" class="row account-section load-content" data-url="/client/active-list" data-container-id="active-list">
                <div class="section-title">
                    <h2><?= Yii::t('main', 'My clients') ?></h2>
                </div>
                <div class="clients-table cabinet-table section-inner">Загрузка...</div>
            </div>

            <div id="section-requests" class="row account-section load-content" data-url="/client/pending-list" data-container-id="pending-list">
                <div class="section-title">
                    <h2>В ожидании</h2>
                </div>
                <div class="cabinet-table requests-table section-inner">Загрузка...</div>
            </div>

            <div id="section-new-requests" class="row account-section load-content" data-url="/client/new-requests-list" data-container-id="new-requests-list">
                <div class="section-title">
                    <h2>Новые запросы</h2>
                </div>
                <div class="cabinet-table requests-table new-requests-table section-inner">Загрузка...</div>
            </div>

            <div id="section-non-active" class="row account-section load-content" data-url="/client/non-active-list" data-container-id="non-active-list">
                <div class="section-title">
                    <h2>Не активные</h2>
                </div>
                <div class="cabinet-table non-active-table section-inner">Загрузка...</div>
            </div>

            <div id="section-finances" class="row account-section load-content" data-url="/account/finances" data-container-id="finances">
                <div class="section-title">
                    <h2><i class="fa fa-credit-card-alt"></i> История оплат</h2>
                </div>
                <div class="cabinet-table finances-table section-inner">Загрузка...</div>
            </div>

            <div id="section-news" class="row account-section load-content" data-url="/account/system-news" data-container-id="system-news">
                <div class="section-title">
                    <h2>Новости Тритфилд</h2>
                </div>
                <div class="cabinet-table news-table section-inner">Загрузка...</div>
            </div>
        </div>
    </div>
</div>