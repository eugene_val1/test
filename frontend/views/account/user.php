<?php

use common\models\Transaction;
use frontend\assets\AccountAsset;
use yii\web\View;

/** @var View $this */
/** @var Transaction $transaction */
/** @var int $tid transaction ID */

AccountAsset::register($this);

if ($transaction && $transaction->status != Transaction::STATUS_OPEN) {
    $this->registerJs("setTimeout(function () {
        $('#payment-trigger-link').trigger('click');
    }, 1000);");
}

?>

<div class="account-page user-cabinet container-fluid">
    <div class="page-inner">
        <div class="page-container">
            <div id="section-therapy" class="row account-section load-content" data-url="/therapists/therapy" data-container-id="therapy">
                <div class="section-title">
                    <h2><i class="fa fa-user" aria-hidden="true"></i>Терапия</h2>
                </div>
                <div class="clients-table therapy-table cabinet-table section-inner">Загрузка...</div>
            </div>

            <div id="section-finances" class="row account-section load-content" data-url="/account/finances" data-container-id="finances">
                <div class="section-title">
                    <h2><i class="fa fa-credit-card-alt"></i> История оплат</h2>
                </div>
                <div class="cabinet-table finances-table section-inner">Загрузка...</div>
            </div>

            <div id="section-news" class="row account-section load-content" data-url="/account/system-news" data-container-id="system-news">
                <div class="section-title">
                    <h2>Новости Тритфилд</h2>
                </div>
                <div class="cabinet-table news-table section-inner">Загрузка...</div>
            </div>
        </div>
    </div>
</div>

<a id="payment-trigger-link" data-target="#modal" href="/account/payment?tid=<?= $tid ?>" style="display:none;"></a>

