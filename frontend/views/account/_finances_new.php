<?php

use backend\models\TransactionSearch;
use common\models\Timezone;
use common\models\Transaction;
use yii\data\Pagination;
use yii\helpers\Html;
use yii\web\YiiAsset;
use yii\web\JqueryAsset;
use yii\bootstrap\BootstrapAsset;
use yii\widgets\LinkPager;
use yii\widgets\Pjax;

Yii::$app->assetManager->bundles[BootstrapAsset::class] = false;
Yii::$app->assetManager->bundles[JqueryAsset::class] = false;
Yii::$app->assetManager->bundles[YiiAsset::class] = false;

/** @var Transaction[] $models */
/** @var Pagination $pages */

/**
 * @param int $date
 *
 * @return string
 */
function formatDate($date) {
    return Timezone::convert($date, Yii::$app->formatter->timeZone, 'd.m.Y')
        . '</br>' . Timezone::convert($date, Yii::$app->formatter->timeZone, 'H:i');
}

?>

<div class="table-head">
    <div class="number">ID</div>
    <div class="payment">Оплата создана</div>
    <div class="client"><?= Yii::$app->user->identity->isTherapist() ? 'Клиент' : 'Терапевт' ?></div>
    <div class="meeting">Встреча</div>
    <div class="quantity">Количество</div>
    <div class="status">Статус</div>
</div>

<?php Pjax::begin(['id' => 'finances', 'enablePushState' => false]) ?>
<div class="table-body">
    <?php foreach ($models as $model): ?>
        <?php /** @var TransactionSearch $model */ ?>
        <div class="client-row">
            <div class="number">
                <?= $model->payment_id ?>
            </div>
            <div class="payment">
                <?= formatDate($model->created); ?>
            </div>
            <div class="client">
                <?php if (Yii::$app->user->identity->isTherapist() && $model->profile): ?>
                    <?= Html::a($model->profile->name, ['/user/profile', 'alias' => $model->profile->alias]) ?>
                <?php elseif ($model->therapist) : ?>
                    <?= Html::a($model->therapist->name, ['/therapist/' . $model->therapist->alias]) ?>
                <?php else: ?>
                    -
                <?php endif; ?>
            </div>
            <div class="meeting">
                <?php if ($model->appointment): ?>
                    <?= formatDate($model->appointment->start)
                        . '<br/><b>#' . $model->appointment->number . '</b>';
                    ?>
                <?php else: ?>
                    <?= '-' ?>
                <?php endif; ?>
            </div>
            <div class="quantity">
                <?= $model->appointmentAmount . ' - $' . $model->getFullAmount(); ?>
            </div>
            <div class="status">
                <?= $model->getStatusName() ?>
            </div>
        </div>
    <?php endforeach; ?>

    <?php
        echo LinkPager::widget([
            'pagination' => $pages,
        ]);
    ?>
</div>
<?php Pjax::end() ?>
