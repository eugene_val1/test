<?php

use yii\helpers\Html;
use common\models\Therapist;
use common\models\Language;
use common\models\Property;
use kartik\widgets\ActiveForm;

Yii::$app->assetManager->bundles['yii\bootstrap\BootstrapAsset'] = false;
Yii::$app->assetManager->bundles['yii\web\JqueryAsset'] = false;
Yii::$app->assetManager->bundles['yii\web\YiiAsset'] = false;
?>

<div class="col-lg-12">
    <h4><?= Yii::t('main', 'Profile information') ?></h4>
    <?php $form = ActiveForm::begin(['enableClientValidation' => false]); ?>
    <div>
        <label class="control-label"><?= $profile->getAttributeLabel('gender'); ?></label>
    </div>
    <?= $form->field($profile, 'gender')->radioButtonGroup($profile->getGenders())->label(false); ?>
    <?= $this->render('_data_form', ['form' => $form, 'translation' => $translation]); ?>
    <div class="therapist-properties">
    <?php foreach(Property::getWithTypes() as $type => $items): ?>
    <div>
        <label class="control-label"><?= Property::getTypeLabel($type); ?></label>
    </div>
    <?= $form->field($profile, 'properties', ['template' => '{input}'])->checkboxButtonGroup($items, ['unselect' => null]); ?>
    <?php endforeach; ?>
    </div>
    <div class="form-group">
        <div>
            <label class="control-label"><?= $profile->getAttributeLabel('birth_date') ?></label>
        </div>
        <?= $form->field($profile, 'date', ['template' => '{input}', 'options' => ['class' => 'inline-form-group']])
            ->dropDownList(
                Therapist::getDays(),
                ['prompt' => Yii::t('main', 'Day')]
            ); 
        ?>
        <?= $form->field($profile, 'month', ['template' => '{input}', 'options' => ['class' => 'inline-form-group']])
                ->dropDownList(
                    Therapist::getMonths(),
                    ['prompt' => Yii::t('main', 'Month')]
                ); 
        ?>
        <?= $form->field($profile, 'year', ['template' => '{input}', 'options' => ['class' => 'inline-form-group']])
                ->dropDownList(
                    Therapist::getYears(),
                    ['prompt' => Yii::t('main', 'Year')]
                ); 
        ?>
        <?= $form->field($profile, 'birth_date', ['template' => '{input}{error}'])->hiddenInput(); ?>
    </div>
    <?= $form->field($profile, 'hide_age')->checkbox(); ?>
    <?= $form->field($profile, 'duration')->textInput()->hint(Yii::t('main', 'Minimum') . ' ' . $profile->minDuration . ', ' . Yii::t('main', 'maximum') . ' ' . $profile->maxDuration); ?>
    <div class="form-group">
        <?= Html::submitButton(Yii::t('main', 'Save'), ['class' => 'small-page-btn page-btn orange btn-submit', 'name' => 'signup-button']) ?>
        <span data-error-title="<?= Yii::t('main', 'There are errors in data, that you try to save!'); ?>" data-success-title="<?= Yii::t('main', 'Data saved successfully'); ?>" class="button-addon"></span>
        <div class="pull-right translate-dropdown">
            <div class="dropdown">
                <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                    <?= Yii::t('main', 'Translate') ?>
                    <span class="caret"></span>
                  </button>
                <ul class="dropdown-menu">
                    <?php foreach (Language::find()->where('id != :current_id', [':current_id' => Language::getCurrent()->id])->all() as $lang): ?>
                        <li>
                            <?= Html::a('<img src="' . $lang->getFileUrl('image') . '" alt=""> ' . $lang->name, ['account/translate', 'language' => $lang->local], ['data-target' => '#modal']) ?>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>  
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>