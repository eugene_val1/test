<?php
    use yii\helpers\Html;
?>

<div class="feedback-choose-action publish-block">
    <h3><?= Yii::t('main', 'Leave feedback') ?></h3>
    <p class="modal-description"><?= Yii::t('main', 'Post feedback description') ?></p>
    <div class="row">
        <div class="col-lg-12 text-center">
            <?= Html::a(
                Yii::t('main', 'Give feedback about Treatfield'),
                ['feedback/post'], 
                [
                    'class' => 'page-btn action-page-btn', 
                    'data-target' => '#modal',
                ]
            ) ?>
            <?= Html::a(
                Yii::t('main', 'Give feedback about therapist'), 
                ['therapists/feedback', ], 
                [
                    'class' => 'page-btn action-page-btn orange',
                    'data-target' => '#modal',
                ]
            ); ?>
        </div>
    </div>
</div>