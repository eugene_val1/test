<?php

use common\models\NotificationSettings;
use common\models\Profile;
use common\models\User;
use dosamigos\fileupload\FileUpload;

Yii::$app->assetManager->bundles['yii\bootstrap\BootstrapAsset'] = false;
Yii::$app->assetManager->bundles['yii\web\JqueryAsset'] = false;
Yii::$app->assetManager->bundles['yii\web\YiiAsset'] = false;

/** @var User $user */
/** @var Profile $profile */
/** @var NotificationSettings $notifications */
/** @var array $bots */

$imageUploadError = "Ваше изображение больше 1 МБ, пожалуйста уменьшите размер и попробуйте еще раз. Вы можете воспользоваться онлайн сервисами для уменьшения размера фото";

?>

<?= $this->render('_update_user', [
        'profile' => $profile,
        'user' => $user,
        'notifications' => $notifications,
        'bots' => $bots,
]) ?>

<div class="col-lg-4">
    <h4 style="height: 25px;"><?= Yii::t('main', 'Your main image') ?></h4>
    <div class="user-image-wrapper">
        <div class="user-main_avatar">
            <img height="128" src="<?= $profile->getFileUrl('main_image'); ?>" alt="">
        </div>
        <span class="fileinput-button">
            <span class="upload-link"><?= Yii::t('main', 'Upload photo') ?></span>
            <?=
            FileUpload::widget([
                'model' => $profile,
                'attribute' => 'main_imageFile',
                'url' => ['account/upload-image', 'id' => $profile->id, 'attribute' => 'main_image'],
                'clientOptions' => [
                    'maxFileSize' => Profile::MAX_IMAGE_SIZE,
                    'autoUpload' => true,
                ],
                'clientEvents' => [
                    'fileuploaddone' => 'function(e, data) {
                        $(".user-main_avatar img").attr("src",data.result);
                    }',
                    'fileuploadfail' => 'function(e, data) {
                        alert("' . $imageUploadError .'");
                    }',
                ],
            ]);
            ?>
        </span>
    </div>
</div>

<div class="col-lg-4">
    <h4 style="height: 25px;"><?= Yii::t('main', 'Your catalog image') ?></h4>
    <div class="user-image-wrapper">
        <div class="user-catalog_avatar">
            <img height="128" src="<?= $profile->getFileUrl('catalog_image'); ?>" alt="">
        </div>
        <span class="fileinput-button">
            <span class="upload-link"><?= Yii::t('main', 'Upload photo') ?></span>
            <?=
            FileUpload::widget([
                'model' => $profile,
                'attribute' => 'catalog_imageFile',
                'url' => ['account/upload-image', 'id' => $profile->id, 'attribute' => 'catalog_image'],
                'clientOptions' => [
                    'maxFileSize' => Profile::MAX_IMAGE_SIZE,
                    'autoUpload' => true,
                ],
                'clientEvents' => [
                    'fileuploaddone' => 'function(e, data) {
                        $(".user-catalog_avatar img").attr("src",data.result);
                    }',
                    'fileuploadfail' => 'function(e, data) {
                        alert("' . $imageUploadError .'");
                    }',
                ],
            ]);
            ?>
        </span>
    </div>
</div>

<div class="col-lg-4">
    <h4 style="height: 25px;"><?= Yii::t('main', 'Your catalog image') ?></h4>
    <div class="user-image-wrapper">
        <div class="user-catalog_hover_avatar">
            <img height="128"  src="<?= $profile->getFileUrl('catalog_image_hover'); ?>" alt="">
        </div>
        <span class="fileinput-button">
            <span class="upload-link"><?= Yii::t('main', 'Upload photo') ?></span>
            <?=
            FileUpload::widget([
                'model' => $profile,
                'attribute' => 'catalog_image_hoverFile',
                'url' => ['account/upload-image', 'id' => $profile->id, 'attribute' => 'catalog_image_hover'],
                'clientOptions' => [
                    'maxFileSize' => Profile::MAX_IMAGE_SIZE,
                    'autoUpload' => true,
                ],
                'clientEvents' => [
                    'fileuploaddone' => 'function(e, data) {
                        $(".user-catalog_hover_avatar img").attr("src",data.result);
                    }',
                    'fileuploadfail' => 'function(e, data) {
                        alert("' . $imageUploadError .'");
                    }',
                ],
            ]);
            ?>
        </span>
    </div>
</div>