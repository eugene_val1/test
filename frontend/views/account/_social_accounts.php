<?php
    use yii\authclient\widgets\AuthChoice;
?>

<?php
$authAuthChoice = AuthChoice::begin([
    'baseAuthUrl' => ['user/auth'],
    'popupMode' => false
]); ?>

<div class="social-accounts">
    <?php foreach ($authAuthChoice->getClients() as $client): ?>
        <div class="col-xs-6 social-account-item">
            <?php $authAuthChoice->clientLink($client, '<i class="fa fa-' . $client->getName() . '"></i> ' . Yii::t('main', $client->getTitle()), ['class' => 'btn btn-md btn-block social-item-link ' . $client->getName() . '-client' . ($user->hasConnected($client->getName()) ? ' active-client' : '')]) ?>
        </div>
    <?php endforeach; ?>
</div>

<?php AuthChoice::end(); ?>

<div class="col-lg-12">
    <span
            data-error-title="<?= Yii::t('main', 'There are errors in data, that you try to save!'); ?>"
            data-success-title="<?= Yii::t('main', 'Data saved successfully'); ?>"
            class="button-addon">
    </span>
</div>
