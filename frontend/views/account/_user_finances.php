<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;

Yii::$app->assetManager->bundles['yii\bootstrap\BootstrapAsset'] = false;
Yii::$app->assetManager->bundles['yii\web\JqueryAsset'] = false;
Yii::$app->assetManager->bundles['yii\web\YiiAsset'] = false;

/** @var $dataProvider \yii\data\DataProviderInterface */

?>
<?php Pjax::begin(['id' => 'pjax-finances', 'enablePushState' => false]); ?>
<?=
GridView::widget([
    'id' => 'grid-finances',
    'dataProvider' => $dataProvider,
    'tableOptions' => ['class' => 'table table-striped table-advance table-hover responsive'],
    'summary' => '',
    'columns' => [
        [
            'class' => 'yii\grid\SerialColumn',
            'contentOptions' => [
                'id' => 'counter']
        ],
        [
            'attribute' => 'user_name',
            'format' => 'raw',
            'headerOptions' => ['class' => 'desktop_hidden'],
            'contentOptions' => function ($model) {
                return ['data-label' => $model->therapist->name, 'class' => 'desktop_hidden user_appoint', 'onclick' => 'void(0);'];
            },

            'value' => function ($model) {
                if ($model->appointment) {
                    return '#' . $model->appointment->number . ' ' . Yii::$app->formatter->asDate($model->appointment->start, "medium") . ' ' . Yii::$app->formatter->asTime($model->appointment->start, "short");
                }

                return '#';
            },
        ],

        [
            'attribute' => 'payment_id',
            'format' => 'raw',
            'contentOptions' => ['data-label' => 'ID'],
            'value' => 'payment_id',
        ],
        [
            'attribute' => 'created',
            'label' => Yii::t('main', 'Payment created'),
            'format' => 'html',
            'contentOptions' => ['data-label' => Yii::t('main', 'Payment created')],
            'value' => function ($model) {
                return Yii::$app->formatter->asDate($model->created, "medium") . ' ' . Yii::$app->formatter->asTime($model->created, "short");
            },
        ],
        [
            'attribute' => 'therapist_id',
            'label' => Yii::t('main', 'Therapist'),
            'format' => 'raw',
            'contentOptions' => ['data-label' => Yii::t('main', 'Therapist'), 'class' => 'user_name'],
            'value' => function ($model) {
                return Html::a('<i class="fa fa-link"></i> ' . $model->therapist->name, ['/therapist/' . $model->therapist->alias], ['data-pjax' => 0]);
            },
        ],
        [
            'label' => Yii::t('main', 'Appointment'),
            'format' => 'html',
            'contentOptions' => ['data-label' => Yii::t('main', 'Appointment'), 'class' => 'appointment'],
            'value' => function ($model) {
                if ($model->appointment) {
                    return '#' . $model->appointment->number . ' ' . Yii::$app->formatter->asDate($model->appointment->start, "medium") . ' ' . Yii::$app->formatter->asTime($model->appointment->start, "short");
                }
                return '';
            }
        ],
        [
            'attribute' => 'amount',
            'format' => 'html',
            'contentOptions' => ['data-label' => Yii::t('main', 'Amount')],
            'value' => function ($model) {
                /** @var $model \common\models\Transaction */
                return $model->appointmentAmount . ' - $' . $model->getFullAmount();
            },
        ],
        [
            'label' => Yii::t('main', 'Status'),
            'attribute' => 'status',
            'format' => 'html',
            'contentOptions' => ['data-label' => Yii::t('main', 'Status')],
            'value' => function ($model) {
                /** @var $model \common\models\Transaction */
                return $model->getStatusName();
            },
        ],
    ],
]);
?>
<?php Pjax::end(); ?>