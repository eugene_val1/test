<?php

/** @var \common\models\User $user */

?>

<div class="account-page after-first-payment-container container-fluid">
    <div class="page-inner">
        <div class="page-container">
            <h3>Сессия успешно оплачена.</h3>
            <p>
                В назначенное время подключитесь к онлайн-комнате.
                Сайт автоматически запросит доступ к камере и микрофону, — не забудьте их разрешить.
            </p>
            <p>
                Если у вас будут вопросы — их можно задать вашему терапевту в сообщениях.
            </p>
            <a href="/account#section-therapy" class="page-btn activate-btn">Продолжить</a>
        </div>
    </div>
</div>