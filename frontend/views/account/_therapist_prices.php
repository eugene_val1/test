<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use common\models\Therapist;

Yii::$app->assetManager->bundles['yii\bootstrap\BootstrapAsset'] = false;
Yii::$app->assetManager->bundles['yii\web\JqueryAsset'] = false;
Yii::$app->assetManager->bundles['yii\web\YiiAsset'] = false;
?>

<div class="col-lg-12">
    <h4><?= Yii::t('main', 'Prices') ?></h4>
    <?php $form = ActiveForm::begin(['enableClientValidation' => false]); ?>
    <table class="table table-striped table-advance table-hover">
        <thead>
        <tr>
            <th><?= Yii::t('main', 'Region') ?></th>
            <th><?= Yii::t('main', 'Price') ?></th>
            <th><?= Yii::t('main', 'Final price') ?></th>
        </tr>
        </thead>
        <?php foreach($prices as $i=>$price): ?>
            <tr class="price-wrapper" data-percent="<?= Yii::$app->settingManager->get('serviceFeePercent'); ?>" data-fee="<?= $therapist->personal_fee ?: Yii::$app->settingManager->get('serviceFee'); ?>">
                <td><?= ($price->regionName) ?: $price->region->name ?></td>
                <td><?= $form->field($price, "[$i]amount", ['template' => '{input}{error}', 'inputOptions' => ['class' => 'form-control input-price']]); ?></td>
                <td>$<span class="final-price"><?= $price->amount + $therapist->getFee($price->amount); ?></span></td>
            </tr>
        <?php endforeach; ?>
    </table>
    <div class="form-group">
        <?= Html::submitButton(Yii::t('main', 'Save'), ['class' => 'small-page-btn page-btn orange btn-submit', 'name' => 'signup-button']) ?>
        <span data-error-title="<?= Yii::t('main', 'There are errors in data, that you try to save!'); ?>" data-success-title="<?= Yii::t('main', 'Data saved successfully'); ?>" class="button-addon"></span>
    </div>
    <?php ActiveForm::end(); ?>
</div>