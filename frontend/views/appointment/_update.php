<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\widgets\StarRating;
?>

<div class="contact-question">
    <h3><?= Yii::t('main', 'Update appointment information') ?></h3>
    <div class="form-wrapper">
        <?php $form = ActiveForm::begin(['id' => 'appointment-form']); ?>
        <div class="row">
            <div class="col-lg-12 appointment-info">
                <p>
                    <img width="30px" src="<?= $model->user->getFileUrl('image'); ?>" class="img-circle" alt="">
                    <span><?= $model->user->name ?></span>
                    <span class="pull-right appointment-date"><?= '<i class="fa fa-calendar-check-o"></i> ' . Yii::$app->formatter->asDate($model->start, "medium") . ' ' . Yii::$app->formatter->asTime($model->start, "short") ?></span>
                </p>
            </div>
            <div class="col-sm-12">
                <?= $form->field($model, 'comment')->textarea() ?>
                <?= $form->field($model, 'rating')->widget(StarRating::classname(), [
                    'pluginOptions' => [
                        'step' => 1,
                        'stars' => 10,
                        'max' => 10,
                        'size'=>'xs',
                        'showClear'=>false,
                        'starCaptions' => [
                            0 => Yii::t('main', 'Extremely Poor'),
                            1 => Yii::t('main', 'Very Poor'),
                            2 => Yii::t('main', 'Very Poor'),
                            3 => Yii::t('main', 'Poor'),
                            4 => Yii::t('main', 'Poor'),
                            5 => Yii::t('main', 'Ok'),
                            6 => Yii::t('main', 'Ok'),
                            7 => Yii::t('main', 'Good'),
                            8 => Yii::t('main', 'Good'),
                            9 => Yii::t('main', 'Very Good'),
                            10=> Yii::t('main', 'Extremely Good')
                        ],
                        'starCaptionClasses' => [
                            0 => 'text-danger',
                            1 => 'text-danger',
                            2 => 'text-danger',
                            3 => 'text-danger',
                            4 => 'text-warning',
                            5 => 'text-warning',
                            6 => 'text-info',
                            7 => 'text-info',
                            8 => 'text-primary',
                            9 => 'text-success',
                            10 => 'text-success'
                        ],
                    ]
                ]);?>
            </div>
            <div class="col-sm-12">
                <div class="form-group form-submit">
                    <?= Html::submitButton(Yii::t('main', 'Save'), ['class' => 'small-page-btn page-btn orange btn-submit', 'name' => 'save-button']) ?>
                </div>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>