<?php

use yii\helpers\Html;
use common\models\Appointment;
use yii\helpers\Url;

Yii::$app->assetManager->bundles['yii\web\YiiAsset'] = false;

$this->registerJs("$('[data-toggle=\"popover\"]').popover({'trigger': 'hover'})");
?>

<div class="remove-confirmation appointment-set-status">
    <h3><?= Yii::t('main', 'Confirm action') ?></h3>
    <div class="row">
        <div class="col-lg-12 text-center">
            <?php if ($status == Appointment::STATUS_COMPLETED): ?>
                <p><b><?= Yii::t('main', 'Do you confirm that the appointment is completed?') ?></b></p>
            <?php else: ?>
                <p><b><?= Yii::t('main', 'Do you confirm that money will be withdrawn from the client for the missed appointment?') ?></b></p>
            <?php endif; ?>
            <p><?= Yii::t('main', 'User will be moved to {0} section', ['<b>' . Yii::t('main', 'Pending') . '</b>']) ?></p>
        </div>
        <div class="col-lg-12 text-center" style="margin-top: 20px;">

            <div class="col-sm-12 col-md-8 col-md-offset-2 text-center">
                <?=
                Html::a(
                    '<i class="fa fa-check-circle"></i> ' . Yii::t('main', 'Confirm'),
                    ['client/complete-appointment', 'id' => $appointment->client->id, 'apId' => $appointment->id, 'status' => $status],
                    [
                        'class' => 'page-btn action-page-btn green set-status-btn',
                        'data-toggle' => 'popover',
                        'data-placement' => 'bottom',
                        'data-content' => Yii::t('main', 'Confirm completed appointment')
                    ]
                )
                ?>
            </div>

            <div class="col-sm-12 col-md-8 col-md-offset-2 text-center">
            <?= Html::a(
                '<i class="fa fa-minus-circle"></i> ' . Yii::t('main', 'Close'),
                '/',
                [
                    'class' => 'page-btn action-page-btn red set-status-btn',
                    'data-target' => '#ajax',
                    'data-toggle' => 'popover',
                    'data-placement' => 'bottom',
                    'data-dismiss' => 'modal',
                    'data-content' => Yii::t('main', 'Close')
                ]
            );
            ?>
            </div>

        </div>
    </div>
</div>