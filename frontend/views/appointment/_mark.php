<?php

use yii\helpers\Html;
use common\models\Appointment;

Yii::$app->assetManager->bundles['yii\web\YiiAsset'] = false;

$this->registerJs("$('[data-toggle=\"popover\"]').popover({'trigger': 'hover'})");
?>

<div class="set-status">
    <h3><?= Yii::t('main', 'Please, mark the session status') ?></h3>
    <div class="row">
        <div class="col-lg-12 appointment-info">
            <p>
                <img width="30px" src="<?= $appointment->user->getFileUrl('image'); ?>" class="img-circle" alt="">
                <span><?= $appointment->user->name ?></span>
                <span class="pull-right appointment-date"><?= '<i class="fa fa-calendar-check-o"></i> ' . Yii::$app->formatter->asDate($appointment->start, "medium") . ' ' . Yii::$app->formatter->asTime($appointment->start, "short") ?></span>
            </p>
        </div>
        <div class="col-lg-12 text-center">

            <?php if ($appointment->payment_status == Appointment::PAID): ?>
            <div class="col-sm-12 col-md-8 col-md-offset-2 text-center">
                <?=
                Html::a(
                    '<i class="fa fa-check-circle"></i> ' . Yii::t('main', 'Session was held'),
                    ['appointment/set-status', 'id' => $appointment->id, 'status' => Appointment::STATUS_COMPLETED],
                    [
                        'class' => 'page-btn action-page-btn green set-status-btn',
                        'data-target' => '#modal',
                        'data-toggle' => 'popover',
                        'data-placement' => 'bottom',
                        'data-content' => Yii::t('main', 'Completed session button description')
                    ]
                )
                ?>
            </div>
            <?php endif; ?>

            <div class="col-sm-12 col-md-8 col-md-offset-2 text-center">
            <?= Html::a(
                '<i class="fa fa-minus-circle"></i> ' . Yii::t('main', 'Reschedule') . '/' . Yii::t('main', 'Cancel session'),
                ['appointment/set-status', 'id' => $appointment->id, 'status' => Appointment::STATUS_RESCHEDULE],
                [
                    'class' => 'page-btn action-page-btn orange set-status-btn',
                    'data-target' => '#modal',
                    'data-toggle' => 'popover',
                    'data-placement' => 'bottom',
                    'data-content' => Yii::t('main', 'Uncompleted session button description')
                ]
            ); ?>
            </div>

            <?php if ($appointment->payment_status == Appointment::PAID): ?>
                <div class="col-sm-12 col-md-8 col-md-offset-2 text-center">
                    <?=
                    Html::a(
                        '<i class="fa fa-plus-circle"></i> ' . Yii::t('main', 'Paid missed session'),
                        ['appointment/set-status', 'id' => $appointment->id, 'status' => Appointment::STATUS_MISSED],
                        [
                            'class' => 'page-btn action-page-btn red set-status-btn',
                            'data-target' => '#modal',
                            'data-toggle' => 'popover',
                            'data-placement' => 'bottom',
                            'data-content' => Yii::t('main', 'Missed session button description')
                        ]
                    )
                    ?>
                </div>
            <?php endif; ?>

        </div>
    </div>
</div>