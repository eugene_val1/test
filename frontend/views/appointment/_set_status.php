<?php

use yii\web\YiiAsset;
use yii\helpers\Html;
use common\models\Appointment;

/** @var int $status */

Yii::$app->assetManager->bundles[YiiAsset::class] = false;

$this->registerJs("$('[data-toggle=\"popover\"]').popover({'trigger': 'hover'})");
?>

<div class="remove-confirmation appointment-set-status">
    <h3><?= Yii::t('main', 'Choose action') ?></h3>
    <div class="row">

        <?php if ($status == Appointment::STATUS_RESCHEDULE): ?>
            <div class="col-sm-12 col-md-8 col-md-offset-2 text-center">
                <?= Html::a(
                    '<i class="fa fa-calendar-o"></i><br>' . Yii::t('main', 'Reschedule appointment'),
                    ['client/appointment', 'id' => $appointment->client->id, 'reschedule' => true],
                    [
                        'class' => 'big-btn set-active set-status-btn',
                        'data-target' => '#modal',
                        'data-toggle' => 'popover',
                        'data-placement' => 'bottom',
                        'data-content' => Yii::t('main', 'Reschedule appointment button description')
                    ]
                ); ?>
            </div>
        <?php endif; ?>

        <div class="col-sm-12 col-md-8 col-md-offset-2 text-center">
            <?= Html::a('<i class="fa fa-hourglass-half"></i><br>' . (($status == Appointment::STATUS_RESCHEDULE) ? Yii::t('main', 'Cancel session and replace client to pending section') : Yii::t('main', 'Replace client to pending section')),
                ['client/set-pending', 'id' => $appointment->client->id, 'status' => $status],
                [
                    'class' => 'big-btn pending-client set-pending set-status-btn',
                    'data-toggle' => 'popover',
                    'data-placement' => 'bottom',
                    'data-content' => ($status == Appointment::STATUS_RESCHEDULE) ? Yii::t('main', 'Cancel session and replace client to pending section button description') : Yii::t('main', 'Replace client to pending section button description')
                ]); ?>
        </div>

        <?php if (!($status == Appointment::STATUS_RESCHEDULE && $appointment->payment_status == Appointment::PAID)): ?>
            <div class="col-sm-12 col-md-8 col-md-offset-2 text-center">
                <?= Html::a(
                    '<i class="fa fa-arrow-down"></i><br>' . (($status == Appointment::STATUS_RESCHEDULE) ? Yii::t('main', 'Cancel session and replace client to inactive section') : Yii::t('main', 'Replace client to inactive section')),
                    ['client/set-inactive', 'id' => $appointment->client->id, 'status' => $status],
                    [
                        'class' => 'big-btn inactive-client set-inactive set-status-btn',
                        'data-reload-grid' => 'active',
                        'data-toggle' => 'popover',
                        'data-placement' => 'bottom',
                        'data-content' => ($status == Appointment::STATUS_RESCHEDULE) ? Yii::t('main', 'Cancel session and replace client to inactive section button description') : Yii::t('main', 'Replace client to inactive section button description'),
                    ]
                ); ?>
            </div>
        <?php endif; ?>

    </div>
</div>