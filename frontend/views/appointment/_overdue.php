<?php
use yii\helpers\Html;
Yii::$app->assetManager->bundles['yii\web\YiiAsset'] = false;
?>

<div class="remove-confirmation appointment-set-status">
    <h3><?= Yii::t('main', 'Appointment status required') ?></h3>
    <p class="modal-description"><?= Yii::t('main', 'Appointment status required description') ?></p>
    <div class="row">
        <div class="col-lg-12 appointment-info">
            <p>
                <img width="30px" src="<?= $appointment->user->getFileUrl('image'); ?>" class="img-circle" alt="">
                <span><?= $appointment->user->name ?></span>
                <span class="pull-right appointment-date"><?= '<i class="fa fa-calendar-check-o"></i> ' . Yii::$app->formatter->asDate($appointment->start, "medium") . ' ' . Yii::$app->formatter->asTime($appointment->start, "short") ?></span>
            </p>
        </div>
        <div class="col-lg-12 text-center">
            <div class="col-sm-12 col-md-8 col-md-offset-2 text-center">
            <?= Html::a(
                    '<i class="fa fa fa-question-circle"></i> ' . Yii::t('main', 'Appointment status'),
                    ['appointment/mark', 'id' => $appointment->id], 
                    [
                        'class' => 'page-btn action-page-btn orange set-status-btn',
                        'data-target' => '#modal'
                    ]
            ) ?>
            </div>

            <div class="col-sm-12 col-md-8 col-md-offset-2 text-center">
            <?= Html::a(
                '<i class="fa fa fa-minus-circle"></i> ' . Yii::t('main', 'Later'), 
                ['appointment/later', 'id' => $appointment->id], 
                [
                    'class' => 'page-btn action-page-btn grey set-status-btn',
                    'data-target' => '#ajax',
                    'data-callback' => 'afterOverdueInformed'
                ]
            ); ?>
            </div>

        </div>
    </div>
</div>