<?php
return [
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost;dbname=treatfield_dev',
            'username' => 'treatfield',
            'password' => '#f57Refy&S',
            'charset' => 'utf8',
            'enableSchemaCache' => false,
            'schemaCacheDuration' => 3600,
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
            /*'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp.gmail.com',
                'username' => 'contact@treatfield.com',
                'password' => 'password',
                'port' => '587',
                'encryption' => 'tls',
            ],*/
        ],
    ],
];
