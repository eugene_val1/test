// Load required modules
var https   = require("https");     // https server core module
var fs      = require("fs");        // file system core module
var express = require("express");   // web framework external module
var io      = require("socket.io"); // web socket external module
var easyrtc = require("easyrtc");   // EasyRTC external module

// Setup and configure Express http server. Expect a subfolder called "static" to be the web root.
var httpApp = express();
httpApp.use(express.static(__dirname + "/static/"));

// Start Express https server on port 8443
var webServer = https.createServer(
{
    key:  fs.readFileSync("/etc/apache2/ssl/treatfield.com.key"),
    cert: fs.readFileSync("/etc/apache2/ssl/www.treatfield.com.crt")
},
httpApp).listen(8443);

// Start Socket.io so it attaches itself to Express server
var socketServer = io.listen(webServer, {"log level":1});

var myIceServers = [
  {url: "stun:46.101.212.28:3478"},
  {url: "turn:46.101.212.28:3478", "username":"treatfield", "credential":"tUi97Kmp"},
  {url: "turn:46.101.212.28:3478?transport=tcp", "username":"treatfield", "credential":"tUi97Kmp"}
];

easyrtc.setOption("appIceServers", myIceServers);

easyrtc.on("getIceConfig", function(connectionObj, callback){
  callback(null, myIceServers);
});

// Start EasyRTC server
var rtc = easyrtc.listen(httpApp, socketServer);