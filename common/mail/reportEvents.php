<?php

use yii\helpers\Html;

/**
 * @var \common\models\Event[] $events
 */

?>

<div class="intro">
    <p>На платформе <a href="<?= Yii::$app->params['baseUrl'] ?>">Treat<span>Field.</span>com</a> новые события</p>
</div>

<div class="info">
    <table width="100%">
        <?php foreach($events as $event): ?>
            <tr>
                <td>
                    <?= $event->getTypeName(); ?>
                </td>
                <td>
                    <?= date('d.m.Y', $event->created); ?>
                </td>
                <td>
                    <?= $this->render('@backend/views/event/_' . $event->alias, ['model' => $event]); ?>
                </td>
            </tr>
        <?php endforeach; ?>
    </table>
</div>