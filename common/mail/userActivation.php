<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */

$link = Yii::$app->urlManager->createAbsoluteUrl(['user/activate', 'token' => $user->token]);

?>

<div class="intro">
    <p><?= Yii::t('mail', 'Hello') ?>, <?= Html::encode($user->getName()) ?>!</p>
    <p><?= Yii::t('mail', 'Please confirm your account by clicking on the link below') ?></p>

    <p><?= Html::a(Yii::t('mail', 'Confirm my account'), $link) ?></p>

    <p>
        <?= Yii::t('mail', 'Or copy this link and paste in browser') ?>
        <?= Html::a($link, $link) ?>
    </p>
</div>