<div class="intro">
    <p><?= Yii::t('mail', 'Hello') ?>, <?= $receiver->user->name ?>!</p>
    <p><?= Yii::t('mail', 'You have new messages on') ?> <a href="<?= Yii::$app->params['baseUrl'] ?>">Treat<span>Field.</span>com</a></p>
</div>

<div class="info">
    <table width="100%">
        <?php foreach($messages as $message): ?>
            <tr>
                <td style="text-align: left">
                    <b><?= $message->user->name ?></b>
                    <br/>
                    <?= Yii::$app->formatter->asDatetime($message->created, 'php:d.m.Y H:i') ?>
                </td>
                <td style="text-align: left"><?= $message->text ?></td>
            </tr>
        <?php endforeach; ?>
    </table>
    <div style="width: 100%">
        <a href="<?= Yii::$app->params['baseUrl'] ?>/messages/index" style="background: #6dc1f4; -webkit-border-radius: 4px; -moz-border-radius: 4px; border-radius: 4px; font-size: 14px !important; padding: 10px 20px !important; text-decoration: none; color: #fff; border: 1px solid #fff; display: block; margin: 15px auto; width: 75px; text-align: center;">
            <?= Yii::t('mail', 'Reply') ?>
        </a>
    </div>
</div>

<h3>
    <?= Yii::t('mail', 'Visit this {0}link{1} to reply', [
            '<a href="' .  Yii::$app->params['baseUrl'] . '/messages/index" target="_blank">',
            '</a>'
    ]) ?>
</h3>