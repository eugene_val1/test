<?php

use common\models\Therapist;
use common\models\Timezone;
use common\models\User;
use yii\helpers\Html;
use common\models\Appointment;
use yii\helpers\Url;

/** @var Appointment $appointment */
/** @var User $user */
/** @var Therapist $therapist */

$showPayButton = in_array($appointment->status, [Appointment::STATUS_NEW, Appointment::STATUS_RESCHEDULE])
    && $appointment->payment_status == Appointment::NOT_PAID;

if ($showPayButton) {
    $statusMessage = mb_strtolower(Yii::t('mail', 'Not paid'), 'UTF-8');
} else {
    $statusMessage = mb_strtolower(Yii::t('mail', 'Paid'), 'UTF-8');
}

//format time
Yii::$app->formatter->timeZone = $user->timezone;
$date = Timezone::convert($appointment->start, $user->timezone, 'd.m');
$dateShort = Timezone::convert($appointment->start, $user->timezone, 'H:i');

?>

<div class="intro">
    <p><?= Yii::t('mail', 'Hello') ?>, <?= Html::encode($user->profile->name) ?>!</p>
    <p>На платформе <a style="color: #747474" href="<?= Yii::$app->params['baseUrl'] ?>">Treat<span>Field.</span>com</a> состоится сессия через 24 часа</p>
</div>

<div class="info">
    <table width="100%" class="appointment-status">

        <thead>
            <tr>
                <td><?= Yii::t('mail', 'Appointment') ?></td>
                <td><?= Yii::t('mail', 'Assigned') ?></td>
                <td><?= Yii::t('mail', 'Status') ?></td>
                <td><?= Yii::t('mail', 'Therapist') ?></td>
            </tr>
        </thead>

        <tbody>
            <tr>
                <td>no.<span style="color: #00ABEE"><?= $appointment ? $appointment->number : '?' ?></span></td>
                <td>на <span style="color: #00ABEE"><?= $date ?></span>
                    <br/> в <span style="font-size: 20px; font-weight: normal"><?= $dateShort ?></span>
                </td>
                <td>
                    <?= $statusMessage ?>
                    <?php if ($showPayButton): ?>
                        <br/>
                        <!-- Pay Button -->
                        <a style="color: #747474" href="<?= Yii::$app->urlManager->createAbsoluteUrl(['account']) ?>">
                            <span style="font-weight: 600; font-size: 18px;">
                                <?= Yii::t('main', 'Pay') ?>
                            </span>
                        </a>
                        <!-- Pay Button -->
                    <?php endif; ?>
                </td>
                <td>
                    <a style="color: #747474" href="<?= Url::toRoute(['therapists/view', 'alias' => $therapist->alias]) ?>"><?= $therapist->name ?></a>
                </td>
            </tr>
        </tbody>

    </table>
</div>