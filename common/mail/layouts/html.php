<?php
use yii\helpers\Html;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */
/* @var $content string main view render result */
/* @var $hideFooter boolean */
?>
<?php $this->beginPage() ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=<?= Yii::$app->charset ?>" />
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>

    <style type="text/css">
        body {
            color: #747474 !important;
            margin: 0;
            padding: 0;
            width: 100%;
            font-family: Roboto-Regular,Fira,Arial,serif;
            line-height: 32px;
        }

        .content {
            width: 100%;
            margin: 0;
            padding: 0;
        }

        a {
            color: #747474;
            text-decoration: none;
            border-bottom: 1px solid #00ABEE;
        }

        .logo a {
            border-bottom: none;
        }

        a:active, a:visited, a:link, a:hover {
            color: #747474 !important;
        }

        a > span {
            color: #00ABEE;
        }

        .intro {
            border-top: 3px solid #00ABEE;
        }

        .info {
            background-color: #eff0f1;
            border-radius: 5px;
            padding: 10px;
            margin-bottom: 15px;
        }

        .footer {
            border-top: 3px solid #00ABEE;
            width: 100%;
            padding-top: 15px;
        }

        .footer div {
            width: 50%;
            margin-left: auto;
            margin-right: auto;
        }

        .footer div {
            text-align: center;
        }

        .footer div img {
            width: 120px;
        }

        .footer .rights {
            font-size: 14px;
        }

        .footer .logo {
            margin-top: 15px;
        }

        table td {
            text-align: center;
        }

        table thead td {
            font-size: 22px;
            font-weight: 600;
        }

        table tbody td > span:first-child {
            color: #00ABEE !important;
        }

        table.appointment-status tbody td > span {
            font-size: 24px;
            font-weight: 800;
        }

    </style>

</head>

<body>
<?php $this->beginBody() ?>
<div class="content" style="color: #747474">

    <?= $content ?>

    <div class="footer">
        <div class="rights">Copyright &copy; <?= date('Y') ?> <a style="color: #747474" href="<?= Yii::$app->params['baseUrl'] ?>" target="_blank">Treat<span>Field.</span>com</a>. All rights reserved</div>
        <div class="logo">
            <a style="color: #747474" href="<?= Yii::$app->params['baseUrl'] ?>" target="_blank">
                <img src="<?= Yii::$app->params['baseUrl'] ?>/img/mail-logo.png" alt="treatfield.com"/>
            </a>
        </div>
    </div>
</div>
<?php $this->endBody() ?>
</body>

</html>
<?php $this->endPage() ?>