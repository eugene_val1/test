<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */

$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['user/reset-password', 'token' => $user->token]);
?>

<div class="intro">
    <p><?= Yii::t('mail', 'Hello') ?>, <?= Html::encode($user->getName()) ?>,</p>

    <p><?= Yii::t('mail', 'Follow the link below to reset your password:') ?></p>

    <p><?= Html::a(Html::encode($resetLink), $resetLink) ?></p>
</div>

