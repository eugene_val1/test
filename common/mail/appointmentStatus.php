<?php

use common\components\MailHelper;
use common\models\Client;
use common\models\Timezone;
use common\models\User;
use yii\helpers\Html;
use common\models\Appointment;

/** @var $event \common\models\AppointmentEvent */
/** @var $receiver array */

/**
 * @var $appointment Appointment
 */
$appointment = $event->getAppointment();
/**
 * @var $therapist User
 */
$therapist = $event->getTherapist();

$showPayButton = !empty($appointment)
    && in_array($appointment->status, [Appointment::STATUS_NEW, Appointment::STATUS_RESCHEDULE])
    && $event->appointmentStatus != Appointment::PAID
    && $appointment->payment_status == Appointment::NOT_PAID
    && $event->getClient()->status == Client::STATUS_ACTIVE;

if ($showPayButton) {
    $statusMessage = mb_strtolower(Yii::t('mail', 'Not paid'), 'UTF-8');
} else {
    $statusMessage = mb_strtolower($event->mailStatus, 'UTF-8');
}

/**
 * @var boolean is current user is Therapist
 */
$isTherapist = $therapist->email === $receiver['email'];

/**
 * @var string link to add to Google Calendar
 */
$googleEventLink = '';
if ($appointment && in_array($appointment->status, [Appointment::STATUS_NEW, Appointment::STATUS_RESCHEDULE])) {
    $googleEventLink = MailHelper::generateGoogleEventLink(
        Yii::t('mail', 'Appointment #{0} on TreatField.com', [$appointment->number]),
        Yii::t('mail', 'Appointment #{0} with therapist {1}', [$appointment->number, $therapist->name]),
        $event->getData()['appointment_start'],
        $event->getData()['appointment_start'] + (int)$therapist->profile->duration * 60
    );
}

//format time
Yii::$app->formatter->timeZone = $receiver['timezone'];
$date = Timezone::convert($event->getData()['appointment_start'], $receiver['timezone'], 'd.m');
$dateShort = Timezone::convert($event->getData()['appointment_start'], $receiver['timezone'], 'H:i');

?>

<div class="intro">
    <p><?= Yii::t('mail', 'Hello') ?>, <?= Html::encode($receiver['name']) ?>!</p>
    <p>На платформе <a style="color: #747474" href="<?= Yii::$app->params['baseUrl'] ?>">Treat<span>Field.</span>com</a>
        новое событие</p>
</div>

<div class="info">
    <table width="100%" class="appointment-status">

        <thead>
            <tr>
                <td><?= Yii::t('mail', 'Appointment') ?></td>
                <td><?= Yii::t('mail', 'Assigned') ?></td>
                <td><?= Yii::t('mail', 'Status') ?></td>
                <td>
                    <?= $isTherapist ?
                        Yii::t('mail', 'Client') :
                        Yii::t('mail', 'Therapist')
                    ?>
                </td>
            </tr>
        </thead>

        <tbody>
            <tr>
                <td>no.<span style="color: #00ABEE"><?= $appointment ? $appointment->number : '?' ?></span></td>
                <?php if (empty($event->getData()['appointment_start'])): ?>
                    <td><?= Yii::t('mail', 'Check with the therapist the date and time of next appointment') ?></td>
                <?php else: ?>
                    <td>на <span style="color: #00ABEE"><?= $date ?></span>
                        <br/> в <span style="font-size: 20px; font-weight: normal"><?= $dateShort ?></span>
                    </td>
                <?php endif; ?>
                <td>
                    <?= $statusMessage ?>
                    <?php if ($showPayButton && $therapist->email !== $receiver['email']): ?>
                        <br/>
                        <!-- Pay Button -->
                        <a style="color: #747474" href="<?= Yii::$app->urlManager->createAbsoluteUrl(['account']) ?>">
                            <span style="font-weight: 600; font-size: 18px;">
                                <?= Yii::t('main', 'Pay') ?>
                            </span>
                        </a>
                        <!-- Pay Button -->
                    <?php endif; ?>
                </td>
                <td>
                    <a style="color: #747474" href="<?= $receiver['targetLink'] ?>"><?= $receiver['targetName'] ?></a>
                </td>
            </tr>
        </tbody>

    </table>
</div>

<br/>
<!-- Google Calendar Button -->
<?php if (!$isTherapist && !empty($googleEventLink)): ?>
    <div style="margin: auto; display: block; width: 200px;">
        <a style="color: #747474; text-align: center;" href="<?= $googleEventLink ?>">
            <span style="font-weight: 600; font-size: 15px;">
                <?= Yii::t('mail', 'Add to Google Calendar') ?>
            </span>
        </a>
    </div>
<?php endif; ?>
<!-- Google Calendar Button -->
<br/>