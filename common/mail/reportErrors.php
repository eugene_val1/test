<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */
/* @var $text string */

?>

<div class="intro">
    <p><?= Yii::t('mail', 'Hello') ?>, <?= Html::encode($user->getName()) ?>!</p>
    <p><?= Yii::$app->formatter->asNtext($text) ?></p>
</div>
