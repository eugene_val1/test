<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */

?>

<div class="intro">
    <p><?= Yii::t('mail', 'Hello') ?>, <?= Html::encode($user->getName()) ?>!</p>
    <p><?= Yii::t('backend', 'Your therapist account created'); ?>.</p>
    <p><?= Yii::t('backend', 'Login:') ?> <strong><?= Html::encode($user->username) ?></strong></p>
    <p><?= Yii::t('backend', 'Password:') ?> <strong><?= Html::encode($user->user_password) ?></strong></p>
</div>
