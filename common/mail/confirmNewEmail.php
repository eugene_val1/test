<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */
/* @var $email string */
/* @var $token string */

$link = Yii::$app->urlManager->createAbsoluteUrl(['user/confirm-new-email', 'token' => $token]);

?>

<div class="intro">
    <p><?= Yii::t('mail', 'Hello') ?>, <?= Html::encode($user->getName()) ?>!</p>
    <p><?= Yii::t('mail', 'Please confirm your email {0} by clicking on the link below',
            ['<b>'  . $email . '</b>'])
        ?>
    </p>

    <p><?= Html::a(Yii::t('mail', 'Confirm my new email'), $link) ?></p>

    <p>
        <?= Yii::t('mail', 'Or copy this link and paste in browser') ?>
        <?= Html::a($link, $link) ?>
    </p>
</div>