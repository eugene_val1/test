<?php

namespace common\models;

use Yii;
use yii\helpers\Inflector;
use common\components\UploadedFileBehavior;

/**
 * Class TextArticle
 *
 * @package common\models
 */
class TextArticle extends Article
{
    const TYPE = 'text';

    /**
     * @var string
     */
    public $imageFile;

    /**
     * @var string
     */
    public $cover_imageFile;

    /**
     * @return TypeQuery
     */
    public static function find()
    {
        return new TypeQuery(get_called_class(), ['type' => self::TYPE]);
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                [
                    'class' => UploadedFileBehavior::className(),
                    'subdirectoryName' => $this->tableName(),
                    'defaultImage' => 'default-article.jpg',
                    'attributes' => $this->getImageAttributes(),
                    'resizeType' => UploadedFileBehavior::RESIZE_FORCE_DIMENSIONS,
                ],
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(
            parent::rules(),
            [
                [['content', 'short_description', 'title', 'user_id'], 'required'],
                [['image', 'cover_image'], 'required', 'on' => 'insert'],
                [['imageFile', 'cover_imageFile'], 'file', 'extensions' => 'jpg, gif, png'],
                [['category_id'], 'integer'],
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
            parent::attributeLabels(),
            [
                'imageFile' => Yii::t('backend', 'Image'),
                'cover_imageFile' => Yii::t('backend', 'Cover image'),
                'content' => Yii::t('backend', 'Article content'),
                'short_description' => Yii::t('backend', 'Article short description'),
                'category_id' => Yii::t('backend', 'Category'),
            ]
        );
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if (!$this->alias) {
            $this->alias = Inflector::slug($this->title);
        }

        return parent::beforeSave($insert);
    }

    /**
     * @return bool
     */
    public function beforeDelete()
    {
        $this->deleteMediaFile('image');

        return parent::beforeDelete();
    }

    /**
     * @return array
     */
    public function getImageAttributes()
    {
        return array_merge(
            parent::getImageAttributes(),
            [
                'image' => ['fileAttribute' => 'imageFile', 'width' => 800, 'height' => 500],
                'cover_image' => ['fileAttribute' => 'cover_imageFile', 'width' => 1920, 'height' => 500],
            ]
        );
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(PageLang::className(), ['page_id' => 'category_id'], ['language' => Yii::$app->language]);
    }
}