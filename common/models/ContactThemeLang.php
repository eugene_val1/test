<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "contact_theme_lang".
 *
 * @property integer $id
 * @property integer $contact_theme_id
 * @property string $language
 * @property string $name
 *
 * @property ContactTheme $contact_theme
 */
class ContactThemeLang extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'contact_theme_lang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['language', 'title'], 'required'],
            [
                'language',
                'unique',
                'targetAttribute' => ['language', 'contact_theme_id'],
                'message' => Yii::t('backend', 'Translation already exists'),
            ],
            [['contact_theme_id'], 'integer'],
            [['language'], 'string', 'max' => 6],
            [['title', 'additional_title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('main', 'ID'),
            'contact_theme_id' => Yii::t('backend', 'ContactTheme'),
            'language' => Yii::t('main', 'Language'),
            'title' => Yii::t('backend', 'Title'),
            'additional_title' => Yii::t('backend', 'Additional field title'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRelatedModel()
    {
        return $this->hasOne(ContactTheme::className(), ['id' => 'contact_theme_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguageModel()
    {
        return $this->hasOne(Language::className(), ['local' => 'language']);
    }

    /**
     * @return mixed
     */
    public function getLanguageName()
    {
        return $this->languageModel->name;
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if ($insert && $this->language === Yii::$app->sourceLanguage) {
            $languages = Language::find()->where('local != :local', [':local' => Yii::$app->sourceLanguage])->all();

            foreach ($languages as $language) {
                $langTranslation = new self();
                $langTranslation->attributes = $this->attributes;
                $langTranslation->language = $language->local;
                $langTranslation->save(false);
            }
        }
    }
}
