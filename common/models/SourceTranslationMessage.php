<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "source_translation_message".
 *
 * @property integer $id
 * @property string $category
 * @property integer $type
 * @property string $message
 *
 * @property TranslationMessage[] $messages
 */
class SourceTranslationMessage extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'source_translation_message';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['message'], 'string'],
            [['id'], 'integer'],
            ['type', 'in', 'range' => array_keys(self::getTypes())],
            [['category'], 'string', 'max' => 32]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'category' => Yii::t('backend', 'Category'),
            'type' => Yii::t('backend', 'Type'),
            'message' => Yii::t('backend', 'Message'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(TranslationMessage::className(), ['id' => 'id'])->indexBy('language');
    }

    /**
     * @return array
     */
    public static function getCategories()
    {
        return [
            'main' => Yii::t('backend', 'Main'),
            'backend' => Yii::t('backend', 'Backend'),
            'tooltip' => Yii::t('backend', 'Tooltips'),
            'mail' => Yii::t('backend', 'Mail'),
            'bots' => Yii::t('backend', 'Bots'),
        ];
    }

    /**
     * @return array
     */
    public static function getTypes()
    {
        return [
            'string' => Yii::t('backend', 'String'),
            'text' => Yii::t('backend', 'Text'),
        ];
    }
}
