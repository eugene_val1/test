<?php

namespace common\models;

use Yii;
use common\components\UploadedFileBehavior;

/**
 * Class HomeFullscreenSlide
 * @package common\models
 */
class HomeFullscreenSlide extends HomeSlide
{
    const TYPE = 'fullscreen';
    public $imageFile;
    public $image;
    public $videoFile;
    public $video;

    /**
     * @return TypeQuery
     */
    public static function find()
    {
        return new TypeQuery(get_called_class(), ['type' => self::TYPE]);
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                [
                    'class' => UploadedFileBehavior::className(),
                    'subdirectoryName' => 'home',
                    'attributes' => [
                        'image' => ['fileAttribute' => 'imageFile', 'width' => 1920, 'height' => 1080],
                        'video' => ['fileAttribute' => 'videoFile']
                    ]
                ],
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $rules = parent::rules();

        return array_merge($rules, [
            [['image', 'video'], 'string', 'max' => 255],
            [['imageFile'], 'file', 'extensions' => 'jpg, gif, png'],
            [['videoFile'], 'file', 'extensions' => 'mp4', 'maxSize' => 1024 * 1024 * 50],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'image' => Yii::t('backend', 'Cover image'),
            'imageFile' => Yii::t('backend', 'Cover image'),
            'video' => Yii::t('backend', 'Video'),
            'videoFile' => Yii::t('backend', 'Video')
        ];
    }

    /**
     * @return array
     */
    public function getData()
    {
        return [
            'image' => $this->image,
            'video' => $this->video
        ];
    }

    /**
     * @return bool
     */
    public function beforeDelete()
    {
        $this->deleteMediaFile('image');
        $this->deleteMediaFile('video');
        return parent::beforeDelete();
    }
}