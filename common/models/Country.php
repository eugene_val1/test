<?php

namespace common\models;

use common\components\GeoService;
use common\components\UserManager;
use omnilight\sypexgeo\SypexGeo;
use Yii;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "country".
 *
 * @property integer $id
 * @property string $code
 * @property string $name
 * @property integer $region_id
 */
class Country extends \yii\db\ActiveRecord
{
    /**
     * @var string
     */
    public $name;

    /**
     * @var array
     */
    public $translatedAttributes = [
        'name' => 'string',
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'country';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['region_id'], 'required'],
            [['code'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'code' => Yii::t('backend', 'Code'),
            'name' => Yii::t('backend', 'Name'),
            'region_id' => Yii::t('backend', 'Region'),
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(CountryLang::className(), ['country_id' => 'id'])->indexBy('language');
    }

    /**
     * @return ActiveQuery
     */
    public function getTranslation()
    {
        return $this->hasOne(CountryLang::className(), ['country_id' => 'id'])->where('`country_lang`.language = :language', [':language' => Yii::$app->language]);
    }

    /**
     * @return ActiveQuery
     */
    public function getDefaultTranslation()
    {
        return $this->hasOne(CountryLang::className(), ['country_id' => 'id'])->where('`country_lang`.language = :language', [':language' => Yii::$app->sourceLanguage]);
    }

    /**
     * @return ActiveQuery
     */
    public function getRegion()
    {
        return $this->hasOne(RegionLang::className(), ['region_id' => 'region_id']);
    }

    /**
     * After find
     */
    public function afterFind()
    {
        $translation = ($this->translation !== null) ? $this->translation : $this->defaultTranslation;

        foreach ($this->translatedAttributes as $attribute => $type) {
            $this->{$attribute} = ($translation) ? $translation->{$attribute} : '';
        }

        parent::afterFind();
    }

    /**
     * Finds country by code
     *
     * @param string $code
     * @return static|null
     */
    public static function findByCode($code)
    {
        return static::findOne(['code' => $code]);
    }

    /**
     * @param bool $log
     * @return Country|null
     */
    public static function getCurrentByIP($log = true)
    {
        $geoService = new GeoService(UserManager::getUserIp());
        if ($log) {
            Yii::info([
                'type' => 'country',
                'data' => $geoService->getInfo(),
                'source' => 'geoService',
            ], 'geo');
        }

        $countryCode = $geoService->getCountryCode($log);
        $country = self::findByCode($countryCode);

        if (empty($country)) {
            if ($log) {
                Yii::info([
                    'type' => 'country',
                    'code' => $countryCode,
                    'source' => 'geoService',
                ], 'geo');
            }
            $country = self::getCurrentByIpSypexGeo();
        }

        return $country;
    }

    /**
     * @return Country
     */
    public static function getCurrentByIpSypexGeo()
    {
        $sypexGeo = new SypexGeo([
            'database' => '@common/data/SxGeoCityMax.dat',
        ]);

        $geoip = $sypexGeo->getCityFull(UserManager::getUserIp());
        $country = self::findByCode($geoip['country']['iso']);

        Yii::info([
            'type' => 'country',
            'data' => $geoip,
            'source' => 'sypexGeo',
        ], 'geo');

        if (!$country) {
            Yii::info([
                'type' => 'country',
                'source' => 'defaultCountry',
            ], 'geo');
            $country = self::findByCode(Yii::$app->params['defaultCountry']);
        }

        return $country;
    }

    /**
     * @return bool
     */
    public function beforeDelete()
    {
        if ($this->code == Yii::$app->params['defaultCountry']) {
            return false;
        }

        return parent::beforeDelete();
    }
}
