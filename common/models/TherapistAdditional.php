<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * Class TherapistAdditional
 *
 * @property int $user_id
 * @property string $article_description
 * @property string $image_alt
 * @property string $main_image_alt
 * @property string $catalog_image_alt
 * @property string $business_card_image_alt
 * @property string $page_title
 * @property string $page_description
 *
 * @package common\models
 */
class TherapistAdditional extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'therapist_additional';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'article_description',
                    'image_alt',
                    'main_image_alt',
                    'catalog_image_alt',
                    'business_card_image_alt',
                    'page_title'
                ],
                'string',
                'max' => 1024,
            ],
            [
                [
                    'page_description'
                ],
                'string',
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'article_description' => Yii::t('main', 'Articles description'),
            'image_alt' => Yii::t('main', 'Image description'),
            'main_image_alt' => Yii::t('main', 'Main image description'),
            'catalog_image_alt' => Yii::t('main', 'Catalog image description'),
            'business_card_image_alt' => Yii::t('main', 'Business card image description'),
            'page_title' => Yii::t('main', 'Page title'),
            'page_description' => Yii::t('main', 'Page description'),
        ];
    }
}
