<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "comment".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $article_id
 * @property integer $reply_to
 * @property integer $level
 * @property string $text
 * @property integer $created
 * @property integer $deleted
 * @property integer $votes_count
 * @property integer $replies_count
 *
 * @property Article $article
 * @property User $user
 */
class Comment extends \yii\db\ActiveRecord
{
    const VIEW_REPLIES_COUNT = 3;
    
    const STATUS_PUBLISHED = 0;
    const STATUS_REPORTED = 1;
    const STATUS_DELETED = 2;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'comment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'article_id', 'text'], 'required'],
            [['user_id', 'article_id', 'reply_to', 'created', 'status', 'votes_count', 'replies_count', 'level'], 'integer'],
            [['text'], 'string']
        ];
    }
    
    public function behaviors() {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    \yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['created'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('main', 'ID'),
            'user_id' => Yii::t('main', 'User ID'),
            'article_id' => Yii::t('main', 'Article ID'),
            'reply_to' => Yii::t('main', 'Reply To'),
            'level' => Yii::t('main', 'Level'),
            'text' => Yii::t('main', 'Your comment'),
            'created' => Yii::t('main', 'Created'),
            'status' => Yii::t('main', 'Status'),
            'votes_count' => Yii::t('main', 'Votes count'),
            'replies_count' => Yii::t('main', 'Replies count'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticle()
    {
        return $this->hasOne(Article::className(), ['id' => 'article_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
    
    public function getProfile() {
        return $this->hasOne(Profile::className(), ['user_id' => 'user_id']);
    }
    
    public function getTherapistLang() {
        return $this->hasOne(TherapistLang::className(), ['user_id' => 'user_id'], ['language' => Yii::$app->language]);
    }
    
    public function getVote() {
        return $this->hasOne(Vote::className(), ['target_id' => 'id'])->where(['target' => $this->tableName(), 'user_id' => Yii::$app->user->id]);
    }
    
    public function getReplies() {
        return $this->hasMany(self::className(), ['reply_to' => 'id'])->orderBy('created DESC');
    }
    
    public function updateCounter($attribute, $value){
        $this->{$attribute} += $value;
        $this->save(false);
    }
    
    public function setStatus($status){
        $this->status = $status;
        $this->save(false);
    }
    
    public static function getStatuses(){
        return [
            self::STATUS_PUBLISHED => Yii::t('main', 'Published'), 
            self::STATUS_REPORTED => Yii::t('main', 'Reported'), 
            self::STATUS_DELETED => Yii::t('main', 'Deleted'),
        ];
    }
    
    public function getStatusName(){
        $statuses = $this->getStatuses();
        return $statuses[$this->status];
    }
    
    public function getAuthorName(){
        if($this->profile->type === Therapist::TYPE){
            return $this->therapistLang->name;
        } else {
            return $this->profile->name;
        }
    }
}
