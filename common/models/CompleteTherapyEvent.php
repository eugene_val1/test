<?php

namespace common\models;

use common\models\Event;
use common\models\Client;

/**
 * Class CompleteTherapyEvent
 * @package common\models
 */
class CompleteTherapyEvent extends Event
{
    /**
     * @return TypeQuery
     */
    public static function find()
    {
        return new TypeQuery(get_called_class(), ['type' => Event::TYPE_COMPLETE_THERAPY]);
    }

    /**
     * @return array|null|\yii\db\ActiveRecord
     */
    public function getClient()
    {
        $id = (isset($this->_data['client_id'])) ? $this->_data['client_id'] : null;
        return Client::find()->where(['id' => $id])->with(['user', 'therapist'])->one();
    }

    /**
     * @return string
     */
    public function getAlias()
    {
        return 'complete_therapy';
    }
}
