<?php

namespace common\models;

use Yii;
use yii\caching\TagDependency;
use yii\db\ActiveRecord;
use yii\helpers\Json;

/**
 * This is the model class for table "therapist_review".
 *
 * @property integer $id
 * @property integer $therapist_id
 * @property string $name
 * @property string $text
 * @property integer $published
 * @property integer $created
 *
 * @property User $therapist
 */
class TherapistReview extends ActiveRecord
{
    const STATUS_PUBLISHED = 1;
    const STATUS_DRAFT = 0;

    const CACHE_DURATION = 86400; //1 day in seconds

    public $therapist_name;
    public $cptch;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'therapist_review';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required', 'on' => 'guest'],
            [['therapist_id', 'text'], 'required'],
            [['therapist_id', 'published', 'created'], 'integer'],
            [['text'], 'string', 'max' => 5000],
            [['name'], 'string', 'max' => 255],
            [['therapist_name'], 'safe'],
            ['cptch', 'required', 'on' => ['guest']],
            ['cptch', 'captcha', 'on' => ['guest']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('main', 'ID'),
            'therapist_id' => Yii::t('main', 'Therapist'),
            'therapist_name' => Yii::t('main', 'Therapist'),
            'name' => Yii::t('main', 'Name'),
            'text' => Yii::t('main', 'Review text'),
            'published' => Yii::t('main', 'Published'),
            'created' => Yii::t('main', 'Created'),
            'therapist_name' => Yii::t('main', 'Choose therapist to leave feedback'),
            'cptch' => Yii::t('main', 'Security code'),
        ];
    }
    /**
     * @return array
     */
    public function behaviors() {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created'],
                ],
            ],
        ];
    }

    /**
     * @param int $therapistId
     * @return string
     */
    public static function getCacheKey($therapistId)
    {
        return 'therapist-review-' . $therapistId;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTherapist()
    {
        return $this->hasOne(Profile::className(), ['user_id' => 'therapist_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTherapistLang()
    {
        return $this->hasOne(TherapistLang::className(), ['user_id' => 'therapist_id'], ['language' => Yii::$app->language]);
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        if ($insert) {
            $event = new NeedModerationEvent([
                'importance' => Event::IMPORTANCE_LOW,
                'text' => NeedModerationEvent::TARGET_THERAPIST_REVIEW,
                'data' => Json::encode([
                    'therapist_id' => $this->therapist_id,
                ])
            ]);
            $event->save(false);
        }

        TagDependency::invalidate(Yii::$app->frontendCache, self::getCacheKey($this->therapist_id));

        return parent::afterSave($insert, $changedAttributes);
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        TagDependency::invalidate(Yii::$app->frontendCache, self::getCacheKey($this->therapist_id));

        return parent::beforeSave($insert);
    }

    /**
     * @return bool
     */
    public function beforeDelete()
    {
        TagDependency::invalidate(Yii::$app->frontendCache, self::getCacheKey($this->therapist_id));

        return parent::beforeDelete();
    }
}
