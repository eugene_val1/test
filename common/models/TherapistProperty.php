<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "therapist_property".
 *
 * @property integer $therapist_id
 * @property integer $property_id
 */
class TherapistProperty extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'therapist_property';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['therapist_id', 'property_id'], 'required'],
            [['therapist_id', 'property_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'therapist_id' => Yii::t('main', 'Therapist'),
            'property_id' => Yii::t('main', 'Property'),
        ];
    }
}
