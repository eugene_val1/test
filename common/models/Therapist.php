<?php

namespace common\models;

use common\components\UploadedFileBehavior;
use DateTime;
use himiklab\sortablegrid\SortableGridBehavior;
use Yii;
use yii\caching\TagDependency;
use yii\db\ActiveQuery;
use yii\helpers\Html;
use yii\helpers\Json;

/**
 * This is the model class for table "therapist".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $number
 * @property string $alias
 * @property string $duration
 * @property integer $gender
 * @property string $image
 * @property string $business_card_url
 * @property string $catalog_image
 * @property string $catalog_image_hover
 * @property integer $publish_status
 * @property boolean $free_appointment
 *
 * @property TherapistLang[] $therapistLangs
 * @property TherapistAdditional $additional
 */
class Therapist extends Profile
{
    const TYPE = 'therapist';

    const STATUS_NEW = 0;
    const STATUS_WAITING = 1;
    const STATUS_REVIEWED = 2;
    const STATUS_PUBLISHED = 3;
    const STATUS_UNPUBLISHED = 4;

    const MALE = 'male';
    const FEMALE = 'female';

    public static $catBugentalAliases = [
        'Bugental',
    ];
    public static $treatfieldAliases = [
        'manul',
        'treatfield',
        'alena-gapak',
    ];

    const CACHE_DURATION = 86400; //1 day in seconds

    public $year;
    public $month;
    public $date;
    public $main_imageFile;
    public $catalog_imageFile;
    public $catalog_image_hoverFile;
    public $education;
    public $experience;
    public $short_description;
    public $full_description;
    public $terms;

    public $translatedAttributes = [
        'name' => 'string',
        'education' => 'string',
        'experience' => 'string',
        'short_description' => 'html',
        'full_description' => 'html',
        'terms' => 'html',
    ];

    public $properties = [];

    /**
     * @return TypeQuery
     */
    public static function find()
    {
        return new TypeQuery(get_called_class(), ['type' => self::TYPE, 'tableName' => self::tableName()]);
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => UploadedFileBehavior::className(),
                'subdirectoryName' => self::TYPE,
                'defaultImage' => 'default-user.jpg',
                'attributes' => $this->getImageAttributes(),
                'resizeType' => UploadedFileBehavior::RESIZE_FORCE_DIMENSIONS
            ],
            'sort' => [
                'class' => SortableGridBehavior::className(),
                'sortableAttribute' => 'number'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(
            parent::rules(),
            [
                [['duration'], 'required', 'on' => 'update'],
                [['duration'], 'integer', 'min' => $this->minDuration, 'max' => $this->maxDuration],
                [['personal_fee'], 'integer', 'min' => 0],
                [['publish_status', 'duration', 'month', 'year', 'date'], 'integer'],
                [['catalog_image', 'main_image', 'catalog_image_hover'], 'string', 'max' => 255],
                ['birth_date', 'date', 'format' => 'yyyy-mm-dd', 'skipOnEmpty' => false, 'on' => 'update'],
                [['catalog_imageFile', 'main_imageFile', 'catalog_image_hoverFile'], 'file', 'extensions' => 'jpg, gif, png'],
                [['properties'], 'each', 'rule' => ['integer']],
                ['alias', 'unique'],
                ['business_card_url', 'url'],
                ['gender', 'default', 'value' => self::MALE],
                ['gender', 'in', 'range' => [self::MALE, self::FEMALE]],
                [['free_appointment'], 'boolean'],
                [['free_appointment'], 'default', 'value' => false],
            ]
        );
    }

    /**
     * @param string $alias
     * @return string
     */
    public static function getCacheKey($alias)
    {
        return 'therapist-' . $alias;
    }

    /**
     * @return ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(TherapistLang::className(), ['user_id' => 'user_id'])
            ->indexBy('language');
    }

    /**
     * @return ActiveQuery
     */
    public function getTranslation()
    {
        return $this->hasOne(TherapistLang::className(), ['user_id' => 'user_id'])
            ->where('language = :language', [':language' => Yii::$app->language]);
    }

    /**
     * @return ActiveQuery
     */
    public function getAdditional()
    {
        return $this->hasOne(TherapistAdditional::className(), ['user_id' => 'user_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getDefaultTranslation()
    {
        return $this->hasOne(TherapistLang::className(), ['user_id' => 'user_id'])
            ->where('language = :language', [':language' => Yii::$app->sourceLanguage]);
    }

    /**
     * @return ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPropertyList()
    {
        return $this->hasMany(Property::className(), ['id' => 'property_id'])
            ->viaTable('therapist_property', ['therapist_id' => 'user_id'])->with('translation');
    }

    /**
     * @return ActiveQuery
     */
    public function getPropertyIds()
    {
        return $this->hasMany(TherapistProperty::className(), ['therapist_id' => 'user_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function setProperties()
    {
        $this->properties = array_keys(TherapistProperty::find()
            ->where('therapist_id = :id', [':id' => $this->user_id])->indexBy('property_id')->all());
    }

    /**
     * @return array
     */
    public function getPropertiesByType()
    {
        $properties = [];
        foreach ($this->propertyList as $property) {
            $properties[$property->type][] = $property->name;
        }
        return $properties;
    }

    /**
     * After find
     */
    public function afterFind()
    {
        $translation = ($this->translation !== null) ? $this->translation : $this->defaultTranslation;

        foreach ($this->translatedAttributes as $attribute => $type) {
            $this->{$attribute} = $translation->{$attribute};
        }

        $date = strtotime($this->birth_date);
        if ($date) {
            $this->year = date("Y", $date);
            $this->month = date("m", $date);
            $this->date = date("d", $date);
        }

        parent::afterFind();
    }

    /**
     * @return bool
     */
    public function beforeValidate()
    {
        if ($this->scenario === 'update') {
            $this->birth_date = $this->year . '-' . $this->month . '-' . $this->date;
        }

        return parent::beforeValidate();
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if ($insert && empty($this->duration)) {
            $this->duration = Yii::$app->settingManager->get('defaultDuration');
        }
        $this->type = self::TYPE;

        if (!$insert) {
            TagDependency::invalidate(Yii::$app->frontendCache, self::getCacheKey($this->alias));
        }

        return parent::beforeSave($insert);
    }

    /**
     * @return bool
     */
    public function beforeDelete()
    {
        $this->deleteMediaFile('main_image');
        $this->deleteMediaFile('catalog_image');
        $this->deleteMediaFile('catalog_image_hover');

        TherapistLang::deleteAll('user_id = :user_id', [':user_id' => $this->user_id]);
        TherapistProperty::deleteAll('therapist_id = :user_id', [':user_id' => $this->user_id]);

        TagDependency::invalidate(Yii::$app->frontendCache, self::getCacheKey($this->alias));

        return parent::beforeDelete();
    }

    /**
     * Finds therapist by alias
     *
     * @param string $alias
     * @return static|null
     */
    public static function findByAlias($alias)
    {
        return static::findOne(['alias' => $alias]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
            parent::attributeLabels(),
            [
                'user_id' => Yii::t('backend', 'User'),
                'main_image' => Yii::t('backend', 'Main image'),
                'main_imageFile' => Yii::t('backend', 'Main image'),
                'catalog_image' => Yii::t('backend', 'Catalog image'),
                'catalog_imageFile' => Yii::t('backend', 'Catalog image'),
                'catalog_image_hover' => Yii::t('backend', 'Catalog image'),
                'catalog_image_hoverFile' => Yii::t('backend', 'Catalog image'),
                'publish_status' => Yii::t('backend', 'Status'),
                'short_description' => Yii::t('main', 'Short title text'),
                'full_description' => Yii::t('main', 'Custom text'),
                'education' => Yii::t('backend', 'Education'),
                'experience' => Yii::t('backend', 'Experience'),
                'birth_date' => Yii::t('backend', 'Birth date'),
                'duration' => Yii::t('backend', 'Session duration (minutes)'),
                'hide_age' => Yii::t('backend', 'Hide age in profile'),
                'personal_fee' => Yii::t('backend', 'Personal fee'),
                'gender' => Yii::t('main', 'Gender'),
                'business_card_url' => Yii::t('main', 'Business Card URL'),
                'free_appointment' => Yii::t('main', 'Free appointment'),
            ]
        );
    }

    /**
     * @return array
     */
    public function getImageAttributes()
    {
        return [
            'image' => [
                'fileAttribute' => 'imageFile',
                'width' => 128,
                'height' => 128
            ],
            'main_image' => [
                'fileAttribute' => 'main_imageFile',
                'width' => 800,
                'height' => 1000
            ],
            'catalog_image' => [
                'fileAttribute' => 'catalog_imageFile',
                'width' => 730,
                'height' => 1096
                //'width' => 430,
                //'height' => 645
            ],
            'catalog_image_hover' => [
                'fileAttribute' => 'catalog_image_hoverFile',
                'width' => 730,
                'height' => 1096,
                //'width' => 430,
                //'height' => 645
            ]
        ];
    }

    /**
     * @return array
     */
    public static function getDays()
    {
        $days = [];
        for ($x = 1; $x <= 31; $x++) {
            $day = str_pad($x, 2, "0", STR_PAD_LEFT);
            $days[$day] = $day;
        }
        return $days;
    }

    /**
     * @return array
     */
    public static function getMonths()
    {
        $months = [];
        for ($x = 1; $x <= 12; $x++) {
            $x = str_pad($x, 2, '0', STR_PAD_LEFT);
            $months[$x] = Yii::t('main', date("F", mktime(0, 0, 0, $x, 10)));
        }

        return $months;
    }

    /**
     * @return array
     */
    public static function getYears()
    {
        $years = array();
        $year = date('Y');
        $limit = 80;
        for ($x = $year; $x > $year - $limit; $x--) {
            $years[$x] = $x;
        }
        return $years;
    }

    /**
     * @return int
     */
    public function getAge()
    {
        $birthday = new DateTime($this->birth_date);
        $now = new DateTime();
        $interval = $now->diff($birthday);

        return $interval->y;
    }

    /**
     * @return array
     */
    public function getStatusNames()
    {
        return [
            self::STATUS_NEW => Yii::t('main', 'New'),
            self::STATUS_REVIEWED => Yii::t('main', 'Reviwed'),
            self::STATUS_WAITING => Yii::t('main', 'Waiting'),
            self::STATUS_PUBLISHED => Yii::t('main', 'Published'),
            self::STATUS_UNPUBLISHED => Yii::t('main', 'Unpublished'),
        ];
    }

    /**
     * @return array
     */
    public static function getGenders()
    {
        return [
            self::MALE => Yii::t('main', 'Male'),
            self::FEMALE => Yii::t('main', 'Female'),
        ];
    }

    /**
     * @return mixed
     */
    public function getGenderTitle()
    {
        $genders = self::getGenders();

        return $genders[$this->gender];
    }

    /**
     * @param $status
     */
    public function setStatus($status)
    {
        $this->publish_status = $status;
        $this->save(false);

        if ($status === self::STATUS_WAITING) {
            $event = new NeedModerationEvent([
                'type' => Event::TYPE_NEED_MODERATION,
                'importance' => Event::IMPORTANCE_HIGH,
                'text' => NeedModerationEvent::TARGET_THERAPIST,
                'data' => Json::encode([
                    'therapist_id' => $this->user_id,
                ])
            ]);
            $event->save(false);
        } else if ($status === self::STATUS_PUBLISHED) {
            foreach ($this->translations as $translation) {
                $translation->index_status = ($translation->index_status == TherapistLang::IN_INDEX) ? TherapistLang::NEED_REINDEX : TherapistLang::NEED_INDEX;
                $translation->save(false);
            }
        }
    }

    /**
     * @return string
     */
    public function getDefaultDuration()
    {
        if (empty($this->duration)) {
            return '01:00:00';
        }

        $hours = floor($this->duration / 60);
        $minutes = ($this->duration % 60);

        return sprintf('%02d:%02d:00', $hours, $minutes);
    }

    /**
     * @return mixed
     */
    public function getMinDuration()
    {
        return Yii::$app->settingManager->get('minDuration');
    }

    /**
     * @return mixed
     */
    public function getMaxDuration()
    {
        return Yii::$app->settingManager->get('maxDuration');
    }

    /**
     * @return int|string
     */
    public static function countAll()
    {
        return static::find()
            ->where(['publish_status' => static::STATUS_PUBLISHED])
            ->count();
    }

    /**
     * @param $limit
     * @param $offset
     * @return array
     */
    public static function sitemapUrls($limit, $offset)
    {
        $urls = [];
        $items = static::find()
            ->where(['publish_status' => static::STATUS_PUBLISHED])
            ->limit($limit)
            ->offset($offset)
            ->all();

        foreach ($items as $item) {
            /** @var Therapist $item */
            $urls[$item->id] = [
                'loc' => Yii::$app->urlManager->createAbsoluteUrl(['therapists/view', 'alias' => $item->alias]),
                'changefreq' => 'weekly',
                'priority' => '0.6'
            ];

            if ($item->catalog_image) {
                $urls[$item->id]['images'][] = [
                    'loc' => \htmlspecialchars($item->getFileUrl('catalog_image')),
                    'title' => $item->name
                ];
            }
        }

        return $urls;
    }

    /**
     * Return total price with fee (used in therapist list)
     *
     * @param int $userId
     * @param bool $includeFee
     *
     * @return int
     */
    public function getPrice($userId, $includeFee = true)
    {
        $user = User::findOne($userId);

        return $this->getPriceByCountry($user ? $user->country : null, $includeFee);
    }

    /**
     * @param Country $country
     * @param bool $includeFee
     *
     * @return int
     */
    public function getPriceByCountry($country, $includeFee = true)
    {
        $price = $this->getRegionPrice($country);

        if ($includeFee) {
            $price += $this->getFee($price);
        }

        return $price;
    }

    /**
     * Fee
     *
     * @param int $price
     * @param integer $amount appointments amount
     *
     * @return float|int|mixed
     */
    public function getFee($price, $amount = 1)
    {
        if (!$price) {
            return 0;
        }

        $serviceFee = $this->personal_fee
            ? $this->personal_fee
            : Yii::$app->settingManager->get('serviceFee');

        if (Yii::$app->settingManager->get('serviceFeePercent')) {
            //if in percents
            return ceil(($amount * $price * $serviceFee) / 100);
        }

        //absolute value
        return $amount * $serviceFee;
    }

    /**
     * Price for user region by his ID
     *
     * @param Country $country
     *
     * @return int|mixed
     */
    protected function getRegionPrice($country = null)
    {
        $price = RegionPrice::DEFAULT_PRICE;
        if (!$country) {
            return $price;
        }

        $regionPrice = RegionPrice::find()
            ->where('therapist_id = :therapist AND region_id = :region', [
                ':therapist' => $this->user_id,
                ':region' => $country->region_id,
            ])
            ->one();

        if ($regionPrice) {
            return $regionPrice->amount;
        }

        return $price;
    }

    /**
     * Is pseudo therapist that has differences
     *
     * @return bool
     */
    public function isPseudoTherapist()
    {
        $aliases = array_merge(self::$catBugentalAliases, self::$treatfieldAliases);
        $aliases = array_map('strtolower', $aliases);

        return \in_array(strtolower($this->alias), $aliases, true);
    }

    /**
     * Meta tags
     */
    public function setMetaTags()
    {
        if (empty($this->additional->page_title)) {
            Yii::$app->view->title = 'Психотерапевты | ' . Html::encode($this->name);
        } else {
            Yii::$app->view->title = $this->additional->page_title;
        }

        if (empty($this->additional->page_description)) {
            Yii::$app->view->registerMetaTag([
                'name' => 'description',
                'content' => strip_tags($this->short_description)
            ], 'description');
        } else {
            Yii::$app->view->registerMetaTag([
                'name' => 'description',
                'content' => strip_tags($this->additional->page_description)
            ], 'description');
        }

        Yii::$app->view->registerMetaTag([
            'property' => 'og:title',
            'content' => Html::encode($this->name)
        ], 'og:title');

        Yii::$app->view->registerMetaTag([
            'property' => 'og:url',
            'content' => Yii::$app->urlManager->createAbsoluteUrl(['therapist/' . $this->alias])
        ], 'og:url');

        Yii::$app->view->registerMetaTag([
            'property' => 'og:description',
            'content' => strip_tags($this->short_description)
        ], 'og:description');

        Yii::$app->view->registerMetaTag([
            'property' => 'og:image',
            'content' => $this->getFileUrl('main_image')
        ], 'og:image');

        Yii::$app->view->registerMetaTag([
            'property' => 'og:image:secure_url',
            'content' => $this->getFileUrl('main_image')
        ], 'og:image:secure_url');
    }
}
