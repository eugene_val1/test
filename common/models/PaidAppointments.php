<?php

namespace common\models;
use yii\db\Expression;

/**
 * Class PaidAppointments
 * @package common\models
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $amount
 */
class PaidAppointments extends \yii\db\ActiveRecord
{
    public $name;
    public $translatedAttributes = [
        'name' => 'string',
    ];
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_paid_appointments';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['amount'], 'default', 'value' => 1],
        ];
    }

    /**
     * Add paid appointments count
     * @param $userId
     * @param $amount
     * @return bool
     */
    public static function add($userId, $amount)
    {
        $model = self::findOne(['user_id' => $userId]);
        if (empty($model)) {
            $model = new self();
            $model->user_id = $userId;
            $model->amount = $amount;
            $model->save();
        } else {
            self::increase($userId, $amount);
        }

        return true;
    }

    /**
     * @param $userId
     * @return int
     */
    public static function getForUser($userId)
    {
        $row = self::findOne(['user_id' => $userId]);
        if (empty($row)) {
            return 0;
        }

        return (int)$row->amount;
    }

    /**
     * @param $userId
     * @param int $amount
     */
    public static function decrease($userId, $amount = 1)
    {
        self::updateAll(['amount' => new Expression("IF(amount >= {$amount}, amount - {$amount}, 0)")], ['user_id' => $userId]);
    }

    /**
     * @param $userId
     * @param int $amount
     */
    public static function increase($userId, $amount = 1)
    {
        self::updateAll(['amount' => new Expression("amount + {$amount}")], ['user_id' => $userId]);
    }
}
