<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "setting".
 *
 * @property integer $id
 * @property string $type
 * @property string $title
 * @property string $value
 */
class Setting extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'setting';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'title', 'key'], 'required'],
            [['value'], 'string'],
            [['type', 'title', 'key'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('main', 'ID'),
            'type' => Yii::t('main', 'Type'),
            'title' => Yii::t('main', 'Title'),
            'value' => Yii::t('main', 'Value'),
        ];
    }

    /**
     *
     */
    public function afterFind()
    {
        if ($this->type == 'multilangual') {
            $translation = ($this->translation !== null) ? $this->translation : $this->defaultTranslation;
            $this->value = $translation->content;
        }

        parent::afterFind();
    }

    public function getTranslations()
    {
        return $this->hasMany(SettingLang::className(), ['setting_id' => 'id'])->indexBy('language');
    }

    public function getTranslation()
    {
        return $this->hasOne(SettingLang::className(), ['setting_id' => 'id'])->where('language = :language', [':language' => Yii::$app->language]);
    }

    public function getDefaultTranslation()
    {
        return $this->hasOne(SettingLang::className(), ['setting_id' => 'id'])->where('language = :language', [':language' => Yii::$app->sourceLanguage]);
    }

    public static function getCategories()
    {
        return [
            'main' => Yii::t('backend', 'Main'),
            'therapist' => Yii::t('backend', 'Therapists'),
            'notification' => Yii::t('backend', 'Notifications'),
            'payment' => Yii::t('backend', 'Payments'),
            'seo' => Yii::t('backend', 'SEO'),
        ];
    }
}
