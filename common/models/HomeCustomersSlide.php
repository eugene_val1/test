<?php

namespace common\models;

use Yii;
use common\models\Feedback;
use common\components\UploadedFileBehavior;

class HomeCustomersSlide extends HomeSlide
{
    const TYPE = 'customers';
    public $imageFile;
    public $image;
    private $_items;

    public static function find()
    {
        return new TypeQuery(get_called_class(), ['type' => self::TYPE]);
    }
    
    /**
     * @inheritdoc
     */
    public function behaviors() {
        return array_merge(
            parent::behaviors(),
            [
                [
                    'class' => UploadedFileBehavior::className(),
                    'subdirectoryName' => 'home',
                    'attributes' => ['image' => ['fileAttribute' => 'imageFile']],
                    'resizeType' => UploadedFileBehavior::RESIZE_NONE
                ],
            ]
        );
    }
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        $rules = parent::rules();
        
        return array_merge($rules, [
            [['image'], 'string', 'max' => 255],
            [['imageFile'], 'file', 'extensions'=>'jpg, gif, png'],
        ]);
    }
    
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'image' => Yii::t('backend', 'Image'),
            'imageFile' => Yii::t('backend', 'Image')
        ];
    }

    public function getData(){
        return [
            'image' => $this->image
        ];
    }
    
    public function beforeDelete() {
        $this->deleteMediaFile('image');
        return parent::beforeDelete();
    }
    
    public function getItems(){
        if($this->_items === null){
            $this->_items = Feedback::find()
                    ->where(['`feedback`.published' => Feedback::STATUS_PUBLISHED])
                    ->joinWith(['profile'])
                    ->orderBy('created DESC')
                    ->limit(5)
                    ->all();

            $therapistFeedbacks = TherapistReview::find()
                ->where(['`therapist_review`.published' => TherapistReview::STATUS_PUBLISHED])
                ->joinWith(['therapist'])
                ->orderBy('created DESC')
                ->limit(5)
                ->all();

            $this->_items = array_merge($this->_items, $therapistFeedbacks);
        }        
        return $this->_items;
    }
}