<?php

namespace common\models;

use common\models\Messages\ConversationMessage;
use Yii;

/**
 * This is the model class for table "conversation".
 *
 * @property integer $id
 * @property integer $sender_id
 * @property integer $receiver_id
 * @property boolean $hidden
 */
class Conversation extends \yii\db\ActiveRecord
{    
    public static function tableName()
    {
        return 'conversation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sender_id', 'receiver_id'], 'required'],
            [['sender_id', 'receiver_id'], 'integer'],
            [['hidden'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('main', 'ID'),
            'sender_id' => Yii::t('main', 'Sender ID'),
            'receiver_id' => Yii::t('main', 'Receiver ID')
        ];
    }
    
    public function getSender()
    {      
        return $this->hasOne(Profile::className(), ['user_id' => 'sender_id']);
    }
    
    public function getReceiver()
    {      
        return $this->hasOne(Profile::className(), ['user_id' => 'receiver_id']);
    }
    
    public function getMessages()
    {      
        return $this->hasMany(ConversationMessage::className(), ['conversation_id' => 'id'])->orderBy('created');
    }
    
    public function getNewMessages()
    {      
        return $this->hasMany(ConversationMessage::className(), ['conversation_id' => 'id'])->where('status = :status AND user_id != :user_id', [':status' => ConversationMessage::STATUS_NEW, ':user_id' => Yii::$app->user->id]);
    }
    
    public function getLastMessage()
    {      
        return $this->hasOne(ConversationMessage::className(), ['conversation_id' => 'id'])->orderBy('created DESC');
    }
}
