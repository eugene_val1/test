<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "confirm_tokens".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $token
 * @property string $type
 * @property string $data
 * @property integer $expires
 *
 * @property User $user
 */
class ConfirmTokens extends ActiveRecord
{
    const EXPIRE_DURATION = 86400; //day

    const TYPE_CONFIRM_EMAIL = 'confirm-email';
    const TYPE_UPDATE_EMAIL = 'update-email';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'confirm_tokens';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'type'], 'required'],
            [['user_id', 'expires'], 'integer'],
            [['type'], 'string', 'max' => 255],
            [['data'], 'string', 'max' => 2000],
            [['token'], 'string', 'max' => 30],
            [['type'], 'in', 'range' => [
                self::TYPE_CONFIRM_EMAIL,
                self::TYPE_UPDATE_EMAIL,
            ]],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @param bool $insert
     *
     * @return bool
     * @throws \yii\base\InvalidParamException
     * @throws \yii\base\Exception
     */
    public function beforeSave($insert)
    {
        if (empty($this->expires)) {
            $this->setExpiration();
        }

        if ($insert) {
            $this->token = Yii::$app->security->generateRandomString(10) . '_' . time();
        }

        return parent::beforeSave($insert);
    }

    /**
     * @param int $duration
     */
    public function setExpiration($duration = self::EXPIRE_DURATION)
    {
        $this->expires = time() + $duration;
    }

    /**
     * @return bool
     */
    public function isExpired()
    {
        return time() > $this->expires;
    }
}
