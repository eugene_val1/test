<?php

namespace common\models;

use himiklab\sortablegrid\SortableGridBehavior;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "contact_theme".
 *
 * @property integer $id
 * @property string $code
 * @property string $name
 */
class ContactTheme extends ActiveRecord
{
    public $title;
    public $additional_title;
    public $translatedAttributes = [
        'title' => 'string',
        'additional_title' => 'string',
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'contact_theme';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'sort' => [
                'class' => SortableGridBehavior::className(),
                'sortableAttribute' => 'number',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['number', 'use_additional_field', 'use_for_request'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'use_additional_field' => Yii::t('backend', 'Use additional field'),
            'title' => Yii::t('backend', 'Title'),
            'additional_title' => Yii::t('backend', 'Additional field title'),
            'use_for_request' => Yii::t('backend', 'Use for request')
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(ContactThemeLang::className(), ['contact_theme_id' => 'id'])->indexBy('language');
    }

    /**
     * @return ActiveQuery
     */
    public function getTranslation()
    {
        return $this->hasOne(ContactThemeLang::className(), ['contact_theme_id' => 'id'])->where('language = :language', [':language' => Yii::$app->language]);
    }

    /**
     * @return ActiveQuery
     */
    public function getDefaultTranslation()
    {
        return $this->hasOne(ContactThemeLang::className(), ['contact_theme_id' => 'id'])->where('language = :language', [':language' => Yii::$app->sourceLanguage]);
    }

    /**
     * After find
     */
    public function afterFind()
    {
        $translation = ($this->translation !== null) ? $this->translation : $this->defaultTranslation;

        foreach ($this->translatedAttributes as $attribute => $type) {
            $this->{$attribute} = $translation->{$attribute};
        }

        parent::afterFind();
    }
}
