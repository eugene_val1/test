<?php

namespace common\models;

use Yii;

/**
 * Class ClientEvent
 * @package common\models
 */
class ClientEvent extends Event
{
    public $status;

    /**
     * @return TypeQuery
     */
    public static function find()
    {
        return new TypeQuery(get_called_class(), ['type' => Event::TYPE_CLIENT]);
    }

    /**
     * @return array|null|\yii\db\ActiveRecord
     */
    public function getClient()
    {
        $id = (isset($this->_data['client_id'])) ? $this->_data['client_id'] : null;
        return Client::find()->where(['id' => $id])->with(['user', 'therapist'])->one();
    }

    /**
     * @return string
     */
    public function getAlias()
    {
        return 'client';
    }

    /**
     * @return string
     */
    public function statusMessage()
    {
        switch ($this->_data['status']) {
            case Client::STATUS_NEW:
                return Yii::t('backend', 'requested appointment with');
            case Client::STATUS_ACTIVE:
                return Yii::t('backend', 'became active client of');
            case Client::STATUS_PENDING:
                return Yii::t('backend', 'became pending client of');
            case Client::STATUS_INACTIVE:
                return Yii::t('backend', 'became inactive client of');
            default:
                return '';
        }
    }
}
