<?php

namespace common\models;

use Yii;
use common\models\Language;

/**
 * This is the model class for table "home_slide_lang".
 *
 * @property integer $id
 * @property integer $home_slide_id
 * @property string $language
 * @property string $title
 *
 * @property HomeSlide $homeSlide
 */
class HomeSlideLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'home_slide_lang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['language', 'title'], 'required'],
            ['language', 'unique', 'targetAttribute' => ['language', 'home_slide_id'], 'message' => Yii::t('backend', 'Translation already exists'),],
            [['home_slide_id'], 'integer'],
            [['title'], 'string', 'max' => 500],
            [['language'], 'string', 'max' => 6],
            [['content'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'language' => Yii::t('main', 'Language'),
            'title' => Yii::t('main', 'Title'),
            'content' => Yii::t('main', 'Content'),
        ];
    }
    
    public function getRelatedModel()
    {        
        return $this->hasOne(HomeSlide::className(), ['id' => 'home_slide_id']);
    }
    
    public function getLanguageModel()
    {        
        return $this->hasOne(Language::className(), ['local' => 'language']);
    }
    
    public function getLanguageName()
    {        
        return $this->languageModel->name;
    }
    
    public function afterSave($insert, $changedAttributes) {
        parent::afterSave($insert, $changedAttributes);
        
        if($insert && $this->language == Yii::$app->sourceLanguage){
            $languages = Language::find()->where('local != :local', [':local' => Yii::$app->sourceLanguage])->all();

            foreach ($languages as $language) {
                $langTranslation = new self();
                $langTranslation->attributes = $this->attributes;
                $langTranslation->language = $language->local;
                $langTranslation->save(false);
            }
        }
    }
}
