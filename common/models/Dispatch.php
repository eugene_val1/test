<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "dispatch".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $type
 * @property string $message
 * @property integer $last_id
 * @property integer $count
 * @property string $status
 * @property integer $sent
 */
class Dispatch extends \yii\db\ActiveRecord
{
    const TYPE_ALL = 'all';
    const TYPE_THERAPISTS = 'therappists';
    const TYPE_CLIENTS = 'clients';
    
    const STATUS_WAITING = 'waiting';
    const STATUS_RUNNING = 'running';
    const STATUS_COMPLETED = 'completed';
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dispatch';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['message', 'type'], 'required'],
            [['user_id', 'last_id', 'count', 'sent'], 'integer'],
            [['message'], 'string'],
            [['type', 'status'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'user_id' => Yii::t('backend', 'Sender'),
            'type' => Yii::t('backend', 'Type'),
            'message' => Yii::t('backend', 'Message'),
            'last_id' => Yii::t('backend', 'Last'),
            'count' => Yii::t('backend', 'Sent count'),
            'status' => Yii::t('backend', 'Status'),
            'sent' => Yii::t('backend', 'Sent'),
        ];
    }
    
    public function getTypes(){
        return [
            self::TYPE_ALL => Yii::t('backend', 'All users'),
            self::TYPE_THERAPISTS => Yii::t('backend', 'Therapists only'),
            self::TYPE_CLIENTS => Yii::t('backend', 'Clients only'),
        ];
    }
    
    public function getTypeName(){
        $types = $this->getTypes();
        return $types[$this->type];
    }
    
    public function getStatus(){
        return [
            self::STATUS_WAITING => Yii::t('backend', 'Waiting'),
            self::STATUS_RUNNING => Yii::t('backend', 'Running'),
            self::STATUS_COMPLETED => Yii::t('backend', 'Completed'),
        ];
    }
    
    public function getStatusName(){
        $status = $this->getStatus();
        return $status[$this->status];
    }
    
    public function getProfile() {
        return $this->hasOne(Profile::className(), ['user_id' => 'user_id']);
    }
    
    public function getUser() {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
    
    public function setStatus($status){
        $this->status = $status;
        if($status == self::STATUS_COMPLETED){
            $this->sent = true;
        }
        $this->save(false);
    }
}
