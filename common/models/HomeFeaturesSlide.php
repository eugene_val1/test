<?php

namespace common\models;

class HomeFeaturesSlide extends HomeSlide
{
    const TYPE = 'features';
    private $_features;

    public static function find()
    {
        return new TypeQuery(get_called_class(), ['type' => self::TYPE]);
    }
    
    public function getData(){
        return [];
    }
    
    public function getFeatures(){
        if(!$this->_features){
            $this->_features = Feature::find()->with('translation')->orderBy('number')->all();
        }
        return $this->_features;
    }
}