<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "system_news".
 *
 * @property integer $id
 * @property string $title
 * @property string $text
 * @property int $target
 * @property int $status
 * @property integer $created
 * @property integer $updated
 */
class SystemNews extends ActiveRecord
{
    const STATUS_CREATED = 0;
    const STATUS_PUBLISHED = 1;

    const TARGET_ALL = 0;
    const TARGET_CLIENTS = 1;
    const TARGET_THERAPISTS = 2;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'system_news';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status', 'target'], 'integer'],
            [['title'], 'string'],
            [['title'], 'string', 'max' => 1024],
            ['text', 'string'],
            [['created', 'updated'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('main', 'ID'),
            'title' => 'Заголовок',
            'text' => 'Текст',
            'status' => 'Статус',
            'target' => 'Пользователи',
            'created' => 'Создано',
            'updated' => 'Обновлено',
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created',
                'updatedAtAttribute' => 'updated',
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @return array
     */
    public static function getTargets()
    {
        return [
            self::TARGET_ALL => 'Все пользователи',
            self::TARGET_CLIENTS => 'Клиенты',
            self::TARGET_THERAPISTS => 'Терапевты',
        ];
    }

    /**
     * @return array
     */
    public static function getStatuses()
    {
        return [
            self::STATUS_CREATED => 'Не активная',
            self::STATUS_PUBLISHED => 'Опубликовано',
        ];
    }

    /**
     * @return int
     */
    public static function countNew()
    {
        $readNewsIds = ArrayHelper::getColumn(Yii::$app->user->identity->readSystemNews, 'system_news_id');

        $target = Yii::$app->user->identity->isTherapist()
            ? SystemNews::TARGET_THERAPISTS
            : SystemNews::TARGET_CLIENTS;

        return (int)SystemNews::find()
            ->where(['status' => SystemNews::STATUS_PUBLISHED])
            ->andWhere(['target' => [SystemNews::TARGET_ALL, $target]])
            ->andWhere(['not in', 'id', $readNewsIds])
            ->count();
    }
}
