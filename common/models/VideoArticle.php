<?php

namespace common\models;

use Yii;
use yii\helpers\Inflector;
use common\components\UploadedFileBehavior;

/**
 * Class VideoArticle
 * @package common\models
 */
class VideoArticle extends Article
{
    const TYPE = 'video';

    public $imageFile;
    public $videoCode;
    public $videoUrl;

    /**
     * @return TypeQuery
     */
    public static function find()
    {
        return new TypeQuery(get_called_class(), ['type' => self::TYPE]);
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                [
                    'class' => UploadedFileBehavior::className(),
                    'subdirectoryName' => $this->tableName(),
                    'defaultImage' => 'default-article.jpg',
                    'attributes' => $this->getImageAttributes(),
                    'resizeType' => UploadedFileBehavior::RESIZE_FORCE_DIMENSIONS
                ],
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(
            parent::rules(),
            [
                [['title', 'video_source', 'user_id'], 'required'],
                [['video_source'], 'checkUrl'],
                [['videoUrl'], 'safe'],
                [['imageFile'], 'file', 'extensions' => 'jpg, gif, png'],
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
            parent::attributeLabels(),
            [
                'imageFile' => Yii::t('backend', 'Video thumbnail'),
                'content' => Yii::t('backend', 'Video description'),
                'videoUrl' => Yii::t('backend', 'Video url'),
            ]
        );
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if (!$this->imageFile && $this->video_source_id && $this->videoUrl) {
            $this->deleteOldImage('image');
            switch ($this->video_source) {
                case 'youtube':
                    $url = $this->getThumbUrl($this->video_source, $this->video_source_id);
                    $this->saveRemoteImage('image', $url);
                    break;
                default:
                    break;
            }
        }

        if (!$this->alias) {
            $this->alias = Inflector::slug($this->title);
        }

        return parent::beforeSave($insert);
    }

    /**
     * @return bool
     */
    public function beforeDelete()
    {
        $this->deleteMediaFile('image');
        return parent::beforeDelete();
    }

    /**
     * After find
     */
    public function afterFind()
    {
        parent::afterFind();

        $this->videoCode = self::getVideoCode($this->video_source, $this->video_source_id);
    }

    /**
     * @return array
     */
    public function getSources()
    {
        return ['youtube' => 'YouTube'];
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function checkUrl($attribute, $params)
    {
        if ($this->videoUrl && $this->video_source) {
            $sourceId = self::parseSourceUrl($this->videoUrl, $this->video_source);

            if ($sourceId) {
                $this->video_source_id = $sourceId;
                $this->videoCode = $this->getVideoCode($this->video_source, $this->video_source_id);
            }
        }

        if (!$this->video_source_id) {
            $this->addError('videoUrl', Yii::t('backend', 'Wrong video url'));
        }
    }

    /**
     * @param $url
     * @param $source
     * @return bool
     */
    public static function parseSourceUrl($url, $source)
    {
        $sourceId = false;

        switch ($source) {
            case 'youtube':
                parse_str(parse_url($url, PHP_URL_QUERY), $vars);
                if (isset($vars['v'])) {
                    $sourceId = $vars['v'];
                }
                break;
            default:
                break;
        }

        return $sourceId;
    }

    /**
     * @param $source
     * @param $sourceId
     * @param int $width
     * @return string
     */
    public static function getVideoCode($source, $sourceId, $width = 560)
    {
        $videoCode = '';
        $height = ($width / 16) * 9;
        switch ($source) {
            case 'youtube':
                $videoCode = '<iframe title="YouTube video player" width="' . $width . '" height="' . $height . '" src="https://www.youtube.com/embed/' . $sourceId . '?rel=0" frameborder="0" allowfullscreen></iframe>';
                break;
            default:
                break;
        }

        return $videoCode;
    }

    /**
     * @param $source
     * @param $sourceId
     * @param int $width
     * @return string
     */
    public static function getResponsiveVideoCode($source, $sourceId, $width = 560)
    {
        $videoCode = '';
        $height = ($width / 16) * 9;
        switch ($source) {
            case 'youtube':
                $videoCode = '<iframe class="video" title="YouTube video player" src="https://www.youtube.com/embed/' . $sourceId . '?rel=0" frameborder="0" allowfullscreen></iframe>';
                break;
            default:
                break;
        }

        return $videoCode;
    }

    /**
     * @return array
     */
    public function getImageAttributes()
    {
        return array_merge(
            parent::getImageAttributes(),
            [
                'image' => ['fileAttribute' => 'imageFile', 'width' => 800, 'height' => 500],
            ]
        );
    }

    /**
     * @param $source
     * @param $sourceId
     * @return null|string
     */
    public static function getThumbUrl($source, $sourceId)
    {
        switch ($source) {
            case 'youtube':
                return 'http://img.youtube.com/vi/' . $sourceId . '/mqdefault.jpg';
            default:
                return null;
        }
    }
}