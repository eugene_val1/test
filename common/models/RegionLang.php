<?php

namespace common\models;

use Yii;
use common\models\Language;

/**
 * This is the model class for table "region_lang".
 *
 * @property integer $id
 * @property integer $region_id
 * @property string $language
 * @property string $name
 *
 * @property Region $region
 */
class RegionLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'region_lang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['language'], 'required'],
            ['language', 'unique', 'targetAttribute' => ['language', 'region_id'], 'message' => Yii::t('backend', 'Translation already exists'),],
            [['region_id'], 'integer'],
            [['language'], 'string', 'max' => 6],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('main', 'ID'),
            'region_id' => Yii::t('backend', 'Region'),
            'language' => Yii::t('main', 'Language'),
            'name' => Yii::t('backend', 'Name'),
        ];
    }
    
    public function getRelatedModel()
    {        
        return $this->hasOne(Region::className(), ['id' => 'region_id']);
    }
    
    public function getLanguageModel()
    {        
        return $this->hasOne(Language::className(), ['local' => 'language']);
    }
    
    public function getLanguageName()
    {        
        return $this->languageModel->name;
    }
    
    public function afterSave($insert, $changedAttributes) {
        parent::afterSave($insert, $changedAttributes);
        
        if($insert && $this->language == Yii::$app->sourceLanguage){
            $languages = Language::find()->where('local != :local', [':local' => Yii::$app->sourceLanguage])->all();

            foreach ($languages as $language) {
                $langTranslation = new self();
                $langTranslation->attributes = $this->attributes;
                $langTranslation->language = $language->local;
                $langTranslation->save(false);
            }
        }
    }
}
