<?php

namespace common\models;

use Yii;
use yii\helpers\Url;

/**
 * Class NeedModerationEvent
 * @package common\models
 *
 * @inheritdoc array $data
 */
class NeedModerationEvent extends Event
{
    const TARGET_THERAPIST = 'therapist';
    const TARGET_THERAPIST_REVIEW = 'review';
    const TARGET_FEEDBACK = 'feedback';
    const TARGET_ARTICLE = 'article';
    const TARGET_CONTACT = 'contact';

    public $target;
    public $title;
    public $url;
    public $icon;

    /**
     * @return TypeQuery
     */
    public static function find()
    {
        return new TypeQuery(get_called_class(), ['type' => Event::TYPE_NEED_MODERATION]);
    }

    /**
     * @return string
     */
    public function getAlias()
    {
        return 'need_moderation';
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        $this->type = Event::TYPE_NEED_MODERATION;

        return parent::beforeSave($insert);
    }

    /**
     * After fing
     */
    public function afterFind()
    {
        parent::afterFind();

        switch ($this->text) {
            case self::TARGET_THERAPIST:
                $this->setTherapist();
                break;
            case self::TARGET_THERAPIST_REVIEW:
                $this->setTherapist();
                break;
            case self::TARGET_ARTICLE:
                $this->setArticle();
                break;
            case self::TARGET_FEEDBACK:
                $this->setFeedback();
                break;
            case self::TARGET_CONTACT:
                $this->setContact();
                break;
            default:
                break;
        }
    }

    /**
     * Set therapist
     */
    public function setTherapist()
    {
        if (!isset($this->_data['therapist_id'])) {
            return;
        }

        $therapist = Therapist::find()->where(['user_id' => $this->_data['therapist_id']])->one();
        if (!$therapist) {
            return;
        }

        $this->target = ($this->text == self::TARGET_THERAPIST)
            ? Yii::t('main', 'Therapist')
            : Yii::t('main', 'Review on therapist');
        $this->icon = '<i class="fa fa-user"></i>';
        $this->title = $therapist->name;
        $this->url = Url::toRoute(['therapist/view', 'id' => $therapist->id]);
    }

    /**
     * Set article
     */
    public function setArticle()
    {
        if (!isset($this->_data['article_id'])) {
            return;
        }

        $article = Article::findOne($this->_data['article_id']);
        if (!$article) {
            return;
        }

        $this->target = Yii::t('main', 'Article');
        $this->icon = '<i class="fa fa-newspaper-o"></i>';
        $this->title = $article->title;
        $this->url = Url::toRoute(['article/view', 'id' => $article->id]);
    }

    /**
     * Set feedback
     */
    public function setFeedback()
    {
        if (!isset($this->_data['feedback_id'])) {
            return;
        }

        $feedback = Feedback::findOne($this->_data['feedback_id']);
        if (!$feedback) {
            return;
        }

        $this->target = Yii::t('main', 'New unread');
        $this->icon = '<i class="fa fa-smile-o"></i>';
        $this->title = Yii::t('main', 'Feedback message');
        $this->url = Url::toRoute(['feedback/update', 'id' => $feedback->id]);
    }

    /**
     * Set contact
     */
    public function setContact()
    {
        if (!isset($this->_data['contact_id'])) {
            return;
        }

        $contact = Contact::findOne($this->_data['contact_id']);
        if (!$contact) {
            return;
        }

        $this->target = Yii::t('main', 'New unread');
        $this->icon = '<i class="fa fa-envelope"></i>';
        $this->title = Yii::t('main', 'Contact message');
        $this->url = Url::toRoute(['contact/view', 'id' => $contact->id]);
    }
}