<?php

namespace common\models;

use Yii;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * Class AppointmentEvent
 *
 * @package common\models
 */
class AppointmentEvent extends Event
{
    /**
     * @var Appointment
     */
    private $_appointment;

    /**
     * @var Client
     */
    private $_client;

    /**
     * @var User
     */
    private $_therapist;

    /**
     * @var User
     */
    private $_user;

    public $appointmentStatus;
    public $subject;
    public $message;
    public $mailText;
    public $mailStatus;
    public $senderId = 0;
    public $conversationId = 0;
    public $template = 'appointmentStatus';
    /**
     * @var array
     */
    public $receivers = [];

    const TARGET_STATUS = 'status';
    const TARGET_PAYMENT = 'payment';

    /**
     * @return TypeQuery
     */
    public static function find()
    {
        return new TypeQuery(get_called_class(), ['type' => Event::TYPE_APPOINTMENT]);
    }

    /**
     * @return string
     */
    public function getAlias()
    {
        return 'appointments';
    }

    /**
     * After find
     */
    public function afterFind()
    {
        parent::afterFind();

        $this->appointmentStatus = isset($this->_data['status'])
            ? $this->_data['status']
            : null;

        if ($this->target === Event::RECEIVER_USER) {
            $this->handleUserEvent();
        } else {
            $this->handleAdminEvent();
        }
    }

    /**
     * Handle user event
     */
    protected function handleUserEvent()
    {
        $appointment = $this->getAppointment();
        $client = $this->getClient();

        if (!$appointment || !$client) {
            return;
        }

        if ($this->appointmentStatus === Appointment::STATUS_CANCELED) {
            return;
        }

        if ($this->text === self::TARGET_STATUS) {
            switch ($this->appointmentStatus) {
                case Appointment::STATUS_NEW:
                    $this->subject = Yii::t('main', 'New appointment has been created');
                    $this->message = 'New appointment {number} has been created';
                    $this->mailText = 'New appointment {number} with {contact} has been created';
                    $this->mailStatus = $appointment->getStatusName(Appointment::STATUS_NEW, 'mail');
                    break;
                case Appointment::STATUS_RESCHEDULE:
                    $this->subject = Yii::t('main', 'Appointment has been rescheduled');
                    $this->message = 'Appointment {number} has been rescheduled';
                    $this->mailText = 'Appointment {number} with {contact} has been rescheduled';
                    $this->mailStatus = $appointment->getStatusName(Appointment::STATUS_RESCHEDULE, 'mail');
                    break;
                case Appointment::STATUS_CANCELED:
                    $this->subject = Yii::t('main', 'Appointment has been canceled');
                    $this->message = 'Appointment {number} has been canceled';
                    $this->mailText = 'Appointment {number} with {contact} has been canceled';
                    $this->mailStatus = $appointment->getStatusName(Appointment::STATUS_CANCELED, 'mail');
                    break;
                case Appointment::STATUS_COMPLETED:
                    $this->subject = Yii::t('main', 'Appointment has been completed');
                    $this->message = 'Appointment {number} has been completed';
                    $this->mailText = 'Appointment {number} with {contact} has been completed';
                    $this->mailStatus = $appointment->getStatusName(Appointment::STATUS_COMPLETED, 'mail');
                    break;
                case Appointment::STATUS_MISSED:
                    $this->subject = Yii::t('main', 'Appointment has been missed');
                    $this->message = 'Appointment {number} has been missed';
                    $this->mailText = 'Appointment {number} with {contact} has been missed';
                    $this->mailStatus = $appointment->getStatusName(Appointment::STATUS_MISSED, 'mail');
                    break;
            }
        } else if ($this->text === self::TARGET_PAYMENT) {
            switch ($this->appointmentStatus) {
                case Appointment::PAID:
                    $this->subject = Yii::t('main', 'Appointment has been paid');
                    $this->message = 'Appointment {number} has been paid';
                    $this->mailText = 'Appointment {number} with {contact} has been paid';
                    $this->mailStatus = $appointment->getPaymentStatusName(Appointment::PAID, 'mail');
                    break;
                case Appointment::NOT_PAID:
                    $this->subject = Yii::t('main', 'Appointment changed payment status to not paid');
                    $this->message = 'Appointment {number} changed payment status to not paid';
                    $this->mailText = 'Appointment {number} with {contact} changed payment status to not paid';
                    $this->mailStatus = $appointment->getPaymentStatusName(Appointment::NOT_PAID, 'mail');
                    break;
            }
        }

        $params = ['number' => ($appointment) ? 'No.' . $appointment->number : ''];
        $this->senderId = $this->text === self::TARGET_STATUS
            ? $client->therapist_id
            : $client->user_id;
        $this->conversationId = $client->conversation_id;
        $this->message = Yii::t('main', $this->message, $params);

        //prepend appointment id
        $appointmentId = isset($this->_data['appointment_id'])
            ? (int)$this->_data['appointment_id']
            : 0;

        if (!empty($this->message)) {
            $this->message .= ' <span style="display: none" data-appointment-id="' . $appointmentId . '"></span>';
        }

        $user = $this->getUser();
        $therapist = $this->getTherapist();

        $allowUserNotification = $user
            && $user->isNoticeAllowed(NotificationSettings::TYPE_APPOINTMENT, $this->appointmentStatus, $this->text);

        if ($user && $allowUserNotification) {
            $params['contact'] = ($therapist && $therapist->profile)
                ? Html::encode($therapist->profile->name)
                : Yii::t('main', 'therapist');

            $this->receivers[] = [
                'id' => $user->id,
                'user' => $user,
                'name' => $user->profile->name,
                'targetRole' => User::ROLE_THERAPIST,
                'targetName' => $therapist->profile->name,
                'email' => $user->email,
                'targetLink' => Url::toRoute(['therapists/view', 'alias' => $therapist->profile->alias]),
                'text' => Yii::t('main', $this->mailText, $params),
                'timezone' => $user->timezone,
            ];
        }

        $allowTherapistNotification = $therapist
            && $therapist->isNoticeAllowed(NotificationSettings::TYPE_APPOINTMENT, $this->appointmentStatus, $this->text);

        if ($therapist && $allowTherapistNotification) {
            $params['contact'] = ($user && $user->profile)
                ? Html::encode($user->profile->name)
                : Yii::t('main', 'client');

            $this->receivers[] = [
                'id' => $therapist->id,
                'user' => $therapist,
                'name' => $therapist->profile->name,
                'targetRole' => User::ROLE_USER,
                'targetName' => $user->profile->name,
                'email' => $therapist->email,
                'targetLink' => Url::toRoute(['user/profile', 'alias' => $user->profile->alias]),
                'text' => Yii::t('main', $this->mailText, $params),
                'timezone' => $therapist->timezone,
            ];
        }
    }

    /**
     * Handle admin events
     */
    protected function handleAdminEvent()
    {
        if ($this->text === self::TARGET_STATUS) {
            switch ($this->appointmentStatus) {
                case Appointment::STATUS_NEW:
                    $this->message = Yii::t('backend', 'has been created');
                    $this->getClient();
                    if (isset($this->_data['appointment_start']) && $this->getClient()) {
                        $this->message .= ' (' . Timezone::convert($this->_data['appointment_start'], $this->getTherapist()->timezone) . ')';
                    }
                    break;
                case Appointment::STATUS_RESCHEDULE:
                    $this->message = Yii::t('backend', 'has been rescheduled');
                    $this->getClient();
                    if (isset($this->_data['appointment_start']) && $this->getClient()) {
                        $this->message .= ' (' . Timezone::convert($this->_data['appointment_start'], $this->getTherapist()->timezone) . ')';
                    }
                    break;
                case Appointment::STATUS_CANCELED:
                    $this->message = Yii::t('backend', 'has been canceled');
                    break;
                case Appointment::STATUS_COMPLETED:
                    $this->message = Yii::t('backend', 'has been completed');
                    break;
                case Appointment::STATUS_MISSED:
                    $this->message = Yii::t('backend', 'has been missed');
                    break;
            }
        } else if ($this->text === self::TARGET_PAYMENT) {
            switch ($this->appointmentStatus) {
                case Appointment::PAID:
                    $this->message = Yii::t('backend', 'has been paid');
                    break;
                case Appointment::NOT_PAID:
                    $this->message = Yii::t('backend', 'changed payment status to not paid');
                    break;
            }
        }
    }

    /**
     * Appointment
     * @return Appointment
     */
    public function getAppointment()
    {
        if ($this->_appointment === null) {
            $id = isset($this->_data['appointment_id'])
                ? $this->_data['appointment_id']
                : null;

            $this->_appointment = Appointment::find()
                ->where(['id' => $id])
                ->one();
        }

        return $this->_appointment;
    }

    /**
     * Client
     * @return Client|null
     */
    public function getClient()
    {
        if ($this->_client === null) {
            $therapist_id = isset($this->_data['therapist_id'])
                ? $this->_data['therapist_id']
                : null;
            $user_id = isset($this->_data['user_id'])
                ? $this->_data['user_id']
                : null;
            $this->_client = Client::find()
                ->with(['nextAppointment'])
                ->where(['therapist_id' => $therapist_id, 'user_id' => $user_id])
                ->one();
        }

        return $this->_client;
    }

    /**
     * @return User
     */
    public function getTherapist()
    {
        if ($this->_therapist === null) {
            $this->_therapist = User::find()
                ->with(['profile', 'bots', 'notificationSettings'])
                ->where(['id' => $this->_client->therapist_id])
                ->one();
        }

        return $this->_therapist;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        if ($this->_user === null) {
            $this->_user = User::find()
                ->with(['profile', 'bots', 'notificationSettings'])
                ->where(['id' => $this->_client->user_id])
                ->one();
        }

        return $this->_user;
    }
}