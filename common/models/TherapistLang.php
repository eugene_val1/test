<?php

namespace common\models;

use Yii;
use common\models\Language;

/**
 * This is the model class for table "therapist_lang".
 *
 * @property integer $id
 * @property integer $therapist_id
 * @property string $language
 * @property string $name
 * @property string $short_description
 * @property string $education
 * @property string $experience
 * @property string $full_description
 * @property string $terms
 *
 * @property Therapist $therapist
 */
class TherapistLang extends \yii\db\ActiveRecord
{
    const NOT_INDEX = 0;
    const IN_INDEX = 1;
    const NEED_INDEX = 2;
    const NEED_REINDEX = 3;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'therapist_lang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['language', 'name'], 'required'],
            [['short_description'], 'required', 'on' => 'update'],
            [['user_id', 'index_status'], 'integer'],
            [['short_description', 'full_description', 'education', 'experience', 'terms'], 'string'],
            [['language'], 'string', 'max' => 6],
            [['name'], 'string', 'max' => 100],
            [['short_description'], 'string', 'max' => 2000]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'language' => Yii::t('main', 'Language'),
            'name' => Yii::t('main', 'Name'),
            'short_description' => Yii::t('main', 'Short title text'),
            'full_description' => Yii::t('main', 'Custom text'),
            'education' => Yii::t('main', 'Education'),
            'experience' => Yii::t('main', 'Experience'),
            'terms' => Yii::t('main', 'Terms and conditions'),
        ];
    }

    public function getRelatedModel()
    {        
        return $this->hasOne(Therapist::className(), ['user_id' => 'user_id']);
    }
    
    public function getLanguageModel()
    {        
        return $this->hasOne(Language::className(), ['local' => 'language']);
    }
    
    public function getLanguageName()
    {        
        return $this->languageModel->name;
    }
    
    public function afterSave($insert, $changedAttributes) {
        parent::afterSave($insert, $changedAttributes);
        
        if($insert && $this->language == Yii::$app->sourceLanguage){
            $languages = Language::find()->where('local != :local', [':local' => Yii::$app->sourceLanguage])->all();

            foreach ($languages as $language) {
                $langTranslation = new self();
                $langTranslation->attributes = $this->attributes;
                $langTranslation->language = $language->local;
                $langTranslation->save(false);
            }
        }
    }
}
