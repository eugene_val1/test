<?php

namespace common\models;

use Yii;
use common\components\UploadedFileBehavior;
use yii\web\Cookie;

/**
 * This is the model class for table "lang".
 *
 * @property integer $id
 * @property string $url
 * @property string $local
 * @property string $name
 * @property string $image
 * @property integer $active
 */

class Language extends \yii\db\ActiveRecord {
    
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;
    
    static $current = null;
    public $imageFile;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'language';
    }
    
    public function behaviors() {
        return [
            [
                'class' => UploadedFileBehavior::className(),
                'subdirectoryName' => $this->tableName(),
                'attributes' => $this->getImageAttributes()
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['url', 'local', 'name','active'], 'required'],
            [['url', 'local', 'name', 'image'], 'string', 'max' => 255],
            [['imageFile'], 'file', 'extensions'=>'jpg, gif, png'],
            [['active'], 'validateStatus']
        ];
    }
    
    public function validateStatus($attribute, $params)
    {
        if ($this->active == self::STATUS_INACTIVE && $this->local == Yii::$app->sourceLanguage) {
            $this->addError($attribute, Yii::t('main', 'Main language must be active'));
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'url' => Yii::t('main', 'Url'),
            'local' => Yii::t('main', 'Local'),
            'name' => Yii::t('main', 'Name'),
            'image' => Yii::t('backend', 'Image'),
            'imageFile' => Yii::t('backend', 'Image'),
            'active' => Yii::t('backend', 'Active'),
        ];
    }

    static function getCurrent() {
        if (self::$current === null) {
            self::$current = self::getDefaultLanguage();
        }
        return self::$current;
    }

    static function setCurrent($url = null, $only_active = false) {
        $language = self::getLanguageByUrl($url);
        if($language === null || ($only_active && $language->active == self::STATUS_INACTIVE)){
            self::$current = self::getDefaultLanguage();
        } else {
            self::$current = $language;
            if($language->local !== Yii::$app->getRequest()->getCookies()->getValue('lang')){
                self::setCookie($language->local);   
            }
        }
        Yii::$app->language = self::$current->local;
        return $language;
    }
    
    static function setCookie($lang){
        $cookie = new Cookie([
            'name' => 'lang',
            'value' => $lang,
            'expire' => time() + 86400 * 365,
        ]);
            
        Yii::$app->getResponse()->getCookies()->add($cookie);
    }

    static function getDefaultLanguage() {
        $local = Yii::$app->getRequest()->getCookies()->getValue('lang');
        
        if(!$local) {
            $local = Yii::$app->language;
            self::setCookie($local);
        }
        
        return Language::find()->where('local = :local', [':local' => $local])->one();
    }

    static function getLanguageByUrl($url = null) {
        if ($url === null) {
            return null;
        } else {
            return Language::find()->where('url = :url', [':url' => $url])->one();
        }
    }
    
    public function getImageAttributes(){
        return [
            'image' => ['fileAttribute' => 'imageFile', 'width' => 16, 'height' => 16],
        ];
    }
    
    public static function getStatuses(){
        return [
            self::STATUS_ACTIVE => Yii::t('main', 'Active'),
            self::STATUS_INACTIVE => Yii::t('main', 'Inactive')        
        ];
    }
    
    public function getStatusName(){
        $statuses = $this->getStatuses();
        return $statuses[$this->active];
    }
}
