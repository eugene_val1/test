<?php

namespace common\models;

use common\models\Messages\ConversationMessage;
use common\models\Messages\EventMessage;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class ChangeTimezoneEvent
 * @package common\models
 */
class ChangeTimezoneEvent extends Event
{
    public $user_id;
    public $timezone;

    /**
     * @return TypeQuery
     */
    public static function find()
    {
        return new TypeQuery(get_called_class(), ['type' => Event::TYPE_CHANGE_TIMEZONE]);
    }

    /**
     * After find
     */
    public function afterFind()
    {
        parent::afterFind();

        if (isset($this->_data['user_id'])) {
            $this->user_id = (int)$this->_data['user_id'];
        }
        if (isset($this->_data['timezone'])) {
            $this->timezone = $this->_data['timezone'];
        }
    }

    /**
     * Send message about this event
     * @return bool
     */
    public function sendMessage()
    {
        if (empty($this->user_id) || empty($this->timezone)) {
            //still no conversations
            return true;
        }

        /** @var User $user */
        $user = User::findOne(['id' => $this->user_id]);
        if (empty($user)) {
            return true;
        }

        /** @var Conversation[] $conversations */
        $conversations = Conversation::find()
            ->where(['hidden' => 0])
            ->andWhere([
                'or', ['sender_id' => $this->user_id], ['receiver_id' => $this->user_id]
            ])
            ->all();

        /** @var Timezone $timezone */
        $timezone = Timezone::findOne(['timezone_id' => $this->timezone]);

        if (empty($conversations) || empty($timezone)) {
            return true;
        }

        //find all therapist active/pending client
        $activeClientsIDs = [];
        if ($user->role == User::ROLE_THERAPIST) {
            $clients = Client::find()
                ->select(['user_id'])
                ->where([
                    'status' => [Client::STATUS_ACTIVE, Client::STATUS_PENDING]
                ])
                ->andWhere(['therapist_id' => $this->user_id])
                ->all();
            $activeClientsIDs = ArrayHelper::getColumn($clients, 'user_id');
        } else {
            $clients = Client::find()
                ->select(['therapist_id'])
                ->where([
                    'status' => [Client::STATUS_ACTIVE, Client::STATUS_PENDING]
                ])
                ->andWhere(['user_id' => $this->user_id])
                ->all();
            $activeClientsIDs = ArrayHelper::getColumn($clients, 'therapist_id');
        }

        foreach ($conversations as $conversation) {
            if ($this->user_id == $conversation->receiver_id && !in_array($conversation->sender_id, $activeClientsIDs)) {
                continue;
            }
            if ($this->user_id == $conversation->sender_id && !in_array($conversation->receiver_id, $activeClientsIDs)) {
                continue;
            }

            $conversationMessage = new EventMessage([
                'user_id' => $this->user_id,
                'receiver_id' => $this->user_id == $conversation->receiver_id
                    ? $conversation->sender_id
                    : $conversation->receiver_id,
                'text' => Yii::t('main','{0} has changed timezone to {1}', [
                    $user->profile->name,
                    $timezone->getOffset()
                ]),
                'status' => ConversationMessage::STATUS_READ,
                'conversation_id' => $conversation->id,
            ]);

            $conversationMessage->save();
        }

        return true;
    }

    /**
     * @return string
     */
    public function getAlias()
    {
        return 'timezone';
    }

}
