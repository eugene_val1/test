<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "partner".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $partner_code
 * @property string $phone
 * @property string $card_number
 * @property string $card_holder
 * @property integer $expired
 */
class Partner extends \yii\db\ActiveRecord
{
    public $email;
    public $month;
    public $year;
    public $cvv;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'partner';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'card_number', 'card_holder', 'phone', 'email', 'cvv', 'expired', 'partner_code'], 'required'],
            [['user_id', 'expired', 'month', 'year'], 'integer'],
            [['phone', 'card_holder', 'card_number', 'email', 'cvv'], 'trim']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('main', 'ID'),
            'user_id' => Yii::t('main', 'User'),
            'card_number' => Yii::t('main', 'Card number'),
            'expired' => Yii::t('main', 'Expired'),
            'cvv' => Yii::t('main', 'Card CVV'),
            'card_holder' => Yii::t('main', 'Card holder')
        ];
    }

    public function getProfile() {
        return $this->hasOne(Profile::className(), ['user_id' => 'user_id']);
    }
    
    public function getTherapistLang() {
        return $this->hasOne(TherapistLang::className(), ['user_id' => 'user_id'], ['language' => Yii::$app->language]);
    }
    
    public function afterFind() {
        if($this->expired){           
            $this->year = date("Y", $this->expired);
            $this->month = date("m", $this->expired);
        }
        parent::afterFind();
    }
    
    public function beforeValidate() {
        if ($this->card_number) {
            $this->card_number = preg_replace('/\D/', '', $this->card_number);
            if (strlen($this->card_number) !== 16) {
                $this->addError('card_number', Yii::t('main', 'Invalid card number'));
                return false;
            }
        }
        if($this->month && $this->year){
            $this->expired = mktime(0, 0, 0, $this->month, 1, $this->year);
        }        
        return parent::beforeValidate();
    }

    public static function getMonths(){
        $months = [];
        for ($x = 1; $x <= 12; $x++) {
            $x = str_pad($x, 2, '0', STR_PAD_LEFT);
            $months[$x] = $x;
        }
 
        return $months;
    }
    
    public static function getYears() {
        $years = array();
        $year = date("Y");
        $limit = 10;
        for ($x = $year; $x < $year + $limit; $x++) {
            $years[$x] = $x;
        }
        return $years;
    }
    
    public function clearAttributes($attributes){
        if(is_array($attributes)){
            foreach ($attributes as $attribute){
                if(isset($this->$attribute)){
                    $this->$attribute = '';
                }
            }
        }
    }
    
    function beforeSave($insert) {
        $this->card_number = str_pad(substr($this->card_number, -4), 16, "*", STR_PAD_LEFT);
        return parent::beforeSave($insert);
    }
}
