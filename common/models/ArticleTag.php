<?php

namespace common\models;

use Yii;
use yii\caching\TagDependency;

/**
 * This is the model class for table "article_tag".
 *
 * @property integer $article_id
 * @property integer $tag_id
 */
class ArticleTag extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'article_tag';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['article_id', 'tag_id'], 'required'],
            [['article_id', 'tag_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'article_id' => Yii::t('main', 'Article ID'),
            'tag_id' => Yii::t('main', 'Tag ID'),
        ];
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        $article = Article::find()
            ->where(['id' => $this->article_id])
            ->one();

        if ($article) {
            TagDependency::invalidate(Yii::$app->frontendCache, Article::getCacheKey($article->alias));
        }

        return parent::beforeSave($insert);
    }
}
