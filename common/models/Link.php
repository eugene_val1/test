<?php

namespace common\models;

use Yii;
use himiklab\sortablegrid\SortableGridBehavior;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "link".
 *
 * @property integer $id
 * @property integer $menu_id
 * @property integer $number
 * @property string $url
 * @property integer $modal
 * @property integer $blank
 * @property string $icon
 * @property integer $template
 */
class Link extends \yii\db\ActiveRecord
{
    const TEMPLATE_TEXT = 0;
    const TEMPLATE_ICON_TEXT = 1;
    const TEMPLATE_ICON = 2;

    /**
     * @var string
     */
    public $title;

    /**
     * @var array
     */
    public $translatedAttributes = [
        'title' => 'string',
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'link';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'sort' => [
                'class' => SortableGridBehavior::className(),
                'sortableAttribute' => 'number'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['number', 'menu_id', 'modal', 'blank', 'template'], 'integer'],
            [['icon'], 'string'],
            [['url'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'number' => Yii::t('main', 'Number'),
            'title' => Yii::t('main', 'Title'),
            'menu_id' => Yii::t('backend', 'Menu'),
            'url' => Yii::t('backend', 'Url'),
            'modal' => Yii::t('backend', 'Open in modal window'),
            'blank' => Yii::t('backend', 'Open in a blank page'),
            'icon' => Yii::t('backend', 'Icon class'),
            'template' => Yii::t('backend', 'Link template'),
        ];
    }

    /**
     * @return array
     */
    public function getTemplates()
    {
        return [
            self::TEMPLATE_TEXT => Yii::t('backend', 'Text link'),
            self::TEMPLATE_ICON_TEXT => Yii::t('backend', 'Text link with icon'),
            self::TEMPLATE_ICON => Yii::t('backend', 'Only icon'),
        ];
    }

    /**
     * @return mixed
     */
    public function getTypeName()
    {
        $templates = $this->getTemplates();
        return $templates[$this->template];
    }

    /**
     * @return string
     */
    public function getContent()
    {
        $html = '';

        if ($this->template !== self::TEMPLATE_TEXT && $this->icon) {
            $html .= '<i class="' . $this->icon . '"></i> ';
        }

        if ($this->template !== self::TEMPLATE_ICON) {
            $html .= $this->title;
        }

        return $html;
    }

    /**
     * @return string
     */
    public function getHref()
    {
        if (strpos($this->url, '//') !== false ||
            strpos($this->url, 'mailto:') !== false ||
            strpos($this->url, 'skype:') !== false
        ) {
            return $this->url;
        }

        return $this->url;
    }

    /**
     * @return ActiveQuery
     */
    public function getTranslations()
    {
        return $this
            ->hasMany(LinkLang::className(), ['link_id' => 'id'])
            ->indexBy('language');
    }

    /**
     * @return ActiveQuery
     */
    public function getTranslation()
    {
        return $this
            ->hasOne(LinkLang::className(), ['link_id' => 'id'])
            ->where('language = :language', [':language' => Yii::$app->language]);
    }

    /**
     * @return ActiveQuery
     */
    public function getDefaultTranslation()
    {
        return $this
            ->hasOne(LinkLang::className(), ['link_id' => 'id'])
            ->where('language = :language', [':language' => Yii::$app->sourceLanguage]);
    }

    /**
     * after find
     */
    public function afterFind()
    {
        $translation = ($this->translation !== null)
            ? $this->translation
            : $this->defaultTranslation;

        foreach ($this->translatedAttributes as $attribute => $type) {
            $this->{$attribute} = $translation->{$attribute};
        }

        parent::afterFind();
    }
}
