<?php

namespace common\models;

use Yii;
use common\components\UploadedFileBehavior;
use frontend\models\UploadImage;
use yii\caching\TagDependency;
use yii\db\ActiveRecord;
use yii\helpers\FileHelper;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "certificate".
 *
 * @property integer $id
 * @property integer $therapist_id
 * @property string $image
 *
 * @property CertificateLang[] $certificateLangs
 */
class Certificate extends ActiveRecord
{
    const CACHE_DURATION = 86400; //1 day in seconds
    const MAX_IMAGE_SIZE = 2097152; //2mb

    public $imageFile;
    public $description;
    public $translatedAttributes = [
        'description' => 'string',
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'certificate';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => UploadedFileBehavior::className(),
                'subdirectoryName' => $this->tableName(),
                'attributes' => $this->getImageAttributes()
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['therapist_id', 'image'], 'required'],
            [['therapist_id'], 'integer'],
            [['image'], 'string', 'max' => 255],
            [['imageFile'], 'file', 'extensions' => 'jpg, gif, png'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('main', 'ID'),
            'therapist_id' => Yii::t('main', 'Therapist'),
            'image' => Yii::t('main', 'Image'),
            'description' => Yii::t('main', 'Description'),
        ];
    }

    /**
     * @param int $therapistId
     * @return string
     */
    public static function getCacheKey($therapistId)
    {
        return 'certificate-' . $therapistId;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(CertificateLang::className(), ['certificate_id' => 'id'])
            ->indexBy('language');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslation()
    {
        return $this->hasOne(CertificateLang::className(), ['certificate_id' => 'id'])
            ->where('language = :language', [':language' => Yii::$app->language]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDefaultTranslation()
    {
        return $this->hasOne(CertificateLang::className(), ['certificate_id' => 'id'])
            ->where('language = :language', [':language' => Yii::$app->sourceLanguage]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTherapist()
    {
        return $this->hasOne(Therapist::className(), ['user_id' => 'therapist_id']);
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        $temp = new UploadImage(['name' => $this->image]);
        $tempImageFile = $temp->getMediaFile('name');

        if (!$this->imageFile && is_file($tempImageFile) && $this->isAttributeChanged('image')) {
            copy($tempImageFile, $this->getMediaFile('image'));
            //$this->resizeFile($this->getMediaFile('image'), ArrayHelper::getValue($this->getImageAttributes(), 'image', false));
            FileHelper::removeDirectory($temp->getMediaFolder());
            if (!$insert) {
                $this->deleteOldImage('image');
            }
        }

        TagDependency::invalidate(Yii::$app->frontendCache, self::getCacheKey($this->therapist_id));

        return parent::beforeSave($insert);
    }

    /**
     * After find
     */
    public function afterFind()
    {
        $translation = $this->translation !== null
            ? $this->translation
            : $this->defaultTranslation;

        foreach ($this->translatedAttributes as $attribute => $type) {
            $this->{$attribute} = $translation->{$attribute};
        }

        parent::afterFind();
    }

    /**
     * @return bool
     */
    public function beforeDelete()
    {
        $this->deleteMediaFile('image');
        TagDependency::invalidate(Yii::$app->frontendCache, self::getCacheKey($this->therapist_id));

        return parent::beforeDelete();
    }

    /**
     * @return array
     */
    public function getImageAttributes()
    {
        return [
            'image' => [
                'fileAttribute' => 'imageFile',
                'width' => 1920,
                'height' => 700
            ],
        ];
    }
}
