<?php

namespace common\models;

use Yii;
use common\models\Language;

/**
 * This is the model class for table "feature_lang".
 *
 * @property integer $id
 * @property integer $feature_id
 * @property string $language
 * @property string $title
 *
 * @property Feature $feature
 */
class FeatureLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'feature_lang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['language', 'title'], 'required'],
            ['language', 'unique', 'targetAttribute' => ['language', 'feature_id'], 'message' => Yii::t('backend', 'Translation already exists'),],
            [['feature_id'], 'integer'],
            [['language'], 'string', 'max' => 6],
            [['title'], 'string', 'max' => 255],
            [['description'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('main', 'ID'),
            'feature_id' => Yii::t('main', 'Feature ID'),
            'language' => Yii::t('main', 'Language'),
            'title' => Yii::t('main', 'Title'),
            'description' => Yii::t('main', 'Description'),
        ];
    }
    
    public function getRelatedModel()
    {        
        return $this->hasOne(Feature::className(), ['id' => 'feature_id']);
    }
    
    public function getLanguageModel()
    {        
        return $this->hasOne(Language::className(), ['local' => 'language']);
    }
    
    public function getLanguageName()
    {        
        return $this->languageModel->name;
    }
    
    public function afterSave($insert, $changedAttributes) {
        parent::afterSave($insert, $changedAttributes);
        
        if($insert && $this->language == Yii::$app->sourceLanguage){
            $languages = Language::find()->where('local != :local', [':local' => Yii::$app->sourceLanguage])->all();

            foreach ($languages as $language) {
                $langTranslation = new self();
                $langTranslation->attributes = $this->attributes;
                $langTranslation->language = $language->local;
                $langTranslation->save(false);
            }
        }
    }
}
