<?php

namespace common\models;

use Yii;
use common\components\UploadedFileBehavior;
use himiklab\sortablegrid\SortableGridBehavior;

/**
 * This is the model class for table "feature".
 *
 * @property integer $id
 * @property string $title
 * @property string $image
 * @property integer $url
 */
class Feature extends \yii\db\ActiveRecord
{
    public $imageFile;
    public $title;
    public $description;
    public $translatedAttributes = [
        'title' => 'string',
        'description' => 'html',
    ];
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'feature';
    }
    
    public function behaviors() {
        return [
            'sort' => [
                'class' => SortableGridBehavior::className(),
                'sortableAttribute' => 'number'
            ],
            [
                'class' => UploadedFileBehavior::className(),
                'subdirectoryName' => $this->tableName(),
                'attributes' => $this->getImageAttributes()
            ],
        ];
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['number'], 'integer'],
            [['image'], 'string', 'max' => 255],
            [['url'], 'string'],
            [['imageFile'], 'file', 'extensions'=>'jpg, gif, png'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'image' => Yii::t('backend', 'Image'),
            'url' => Yii::t('backend', 'Link url'),
            'number' => Yii::t('main', 'Created'),
            'imageFile' => Yii::t('backend', 'Image'),
            'title' => Yii::t('main', 'Title'),
            'description' => Yii::t('main', 'Description'),
        ];
    }
    
    public function getTranslations() {
        return $this->hasMany(FeatureLang::className(), ['feature_id' => 'id'])->indexBy('language');
    }

    public function getTranslation() {
        return $this->hasOne(FeatureLang::className(), ['feature_id' => 'id'])->where('language = :language', [':language' => Yii::$app->language]);
    }
    
    public function getDefaultTranslation() {
        return $this->hasOne(FeatureLang::className(), ['feature_id' => 'id'])->where('language = :language', [':language' => Yii::$app->sourceLanguage]);
    }

    public function afterFind() {
        $translation = ($this->translation !== null) ? $this->translation : $this->defaultTranslation;
        
        foreach ($this->translatedAttributes as $attribute => $type) {
                $this->{$attribute} = $translation->{$attribute};
        }
            
        parent::afterFind();
    }
    
    public function getImageAttributes(){
        return [
            'image' => ['fileAttribute' => 'imageFile', 'width' => 128, 'height' => 128],
        ];
    }
}
