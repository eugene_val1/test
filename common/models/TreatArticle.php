<?php

namespace common\models;

use Yii;
use yii\helpers\Inflector;

/**
 * Class TreatArticle
 * @package common\models
 */
class TreatArticle extends Article
{
    const TYPE = 'treat';

    /**
     * @return TypeQuery
     */
    public static function find()
    {
        return new TypeQuery(get_called_class(), ['type' => self::TYPE]);
    }

    /**
     * After find
     */
    public function afterFind()
    {
        parent::afterFind();

        $this->title = Yii::t('main', 'Treat') . " #" . $this->id;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(
            parent::rules(),
            [
                [['content', 'user_id'], 'required'],
                [['content'], 'string', 'max' => 15000],
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
            parent::attributeLabels(),
            [
                'content' => Yii::t('backend', 'Treat text'),
            ]
        );
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if ($insert) {
            $random = '';
            for ($i = 0; $i < 7; $i++) {
                $random .= mt_rand(0, 9);
            }
            $this->alias = Inflector::slug("treat-" . $random);
        }
        return parent::beforeSave($insert);
    }

    /**
     * @return mixed
     */
    public function getAuthorImage()
    {
        return ($this->profile->type == Therapist::TYPE) ? $this->profile->getFileUrl('main_image') : $this->profile->getFileUrl('image');
    }
}