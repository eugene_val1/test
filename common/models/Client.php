<?php

namespace common\models;

use common\models\Messages\ConversationMessage;
use Yii;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\helpers\Json;

/**
 * This is the model class for table "client".
 *
 * @property integer $id
 * @property integer $therapist_id
 * @property integer $user_id
 * @property string $user_name
 * @property integer $price
 * @property integer $conversation_id
 * @property integer $status
 * @property integer $created
 * @property integer $next_appointment_start
 * @property integer $complete_status
 * @property integer $last_message_created
 */
class Client extends ActiveRecord
{
    const STATUS_NEW = 0;
    const STATUS_ACTIVE = 1;
    const STATUS_PENDING = 2;
    const STATUS_INACTIVE = 3;

    const NO_NEXT_APPPOINTMENT = null;

    const UNCOMPLETED = 0;
    const COMPLETED = 1;

    const APPOINTMENT_OVERDUE = 7200;

    public static function tableName()
    {
        return 'client';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['therapist_id', 'user_id'], 'required'],
            [
                [
                    'status',
                    'created',
                    'conversation_id',
                    'therapist_id',
                    'user_id',
                    'next_appointment_start',
                    'complete_status',
                    'price'
                ], 'integer'
            ],
            ['status', 'in', 'range' => [
                self::STATUS_NEW,
                self::STATUS_ACTIVE,
                self::STATUS_PENDING,
                self::STATUS_INACTIVE
            ]],
            [['user_name'], 'string', 'max' => 500],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'therapist_id' => Yii::t('main', 'Therapist'),
            'user_id' => Yii::t('main', 'Client'),
            'user_name' => Yii::t('main', 'Custom client name'),
            'price' => Yii::t('main', 'Individual price'),
            'conversation_id' => Yii::t('main', 'Conversation'),
            'status' => Yii::t('main', 'Status'),
            'created' => Yii::t('main', 'Created'),
            'next_appointment_start' => Yii::t('main', 'Next appointment start'),
            'complete_status' => Yii::t('main', 'Complete status'),
        ];
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created'],
                ],
            ],
        ];
    }

    /**
     * @return array
     */
    public static function getStatusData()
    {
        return [
            self::STATUS_ACTIVE => [
                'alias' => 'active',
                'name' => Yii::t('main', 'Active clients'),
                'gridButtons' => '{status}',
                'sort' => [
                    'defaultOrder' => [
                        'next_appointment_start' => SORT_ASC
                    ]
                ]
            ],
            self::STATUS_PENDING => [
                'alias' => 'pending',
                'name' => Yii::t('main', 'Pedning clients'),
                'gridButtons' => '{appointment} <br/> {set-inactive}',
                'sort' => [
                    'defaultOrder' => [
                        'created' => SORT_ASC
                    ]
                ]
            ],
            self::STATUS_INACTIVE => [
                'alias' => 'inactive',
                'name' => Yii::t('main', 'Inactive clients'),
                'gridButtons' => '{appointment}',
                'sort' => [
                    'defaultOrder' => [
                        'created' => SORT_ASC
                    ]
                ]
            ],
        ];
    }

    /**
     * @param $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
        $this->save(false);
        $this->event($status);
    }

    /**
     * @param $status
     */
    public function event($status)
    {
        $event = new ClientEvent([
            'type' => ClientEvent::TYPE_CLIENT,
            'importance' => ClientEvent::IMPORTANCE_LOW,
            'text' => 'client_status',
            'target' => ClientEvent::RECEIVER_ADMIN,
            'data' => Json::encode([
                'status' => $status,
                'client_id' => $this->id
            ]),
        ]);
        $event->save(false);
    }

    /**
     * @return array
     */
    public function getStatuses()
    {
        return [
            self::STATUS_NEW => Yii::t('main', 'New'),
            self::STATUS_ACTIVE => Yii::t('main', 'Active'),
            self::STATUS_PENDING => Yii::t('main', 'Pending'),
            self::STATUS_INACTIVE => Yii::t('main', 'Inactive'),
        ];
    }

    /**
     * @return mixed|string
     */
    public function getStatusName()
    {
        $status = $this->getStatuses();

        return isset($status[$this->status])
            ? $status[$this->status]
            : Yii::t('main', 'Not set');
    }

    /**
     * After find
     */
    public function afterFind()
    {
        if (!$this->user_name && $this->therapist_id == Yii::$app->user->id) {
            $this->user_name = $this->user->name;
        }

        return parent::afterFind();
    }

    /**
     * @return int|string
     */
    public static function getTherapistNewClientsCount()
    {
        return self::find()->where([
            'therapist_id' => Yii::$app->user->id,
            'status' => self::STATUS_NEW
        ])->count();
    }

    /**
     * @return int
     */
    public function getTherapistPrice()
    {
        return $this->price == 0
            ? $this->therapist->getPrice($this->user_id)
            : $this->price;
    }

    /**
     * @return mixed
     */
    public function getDuration()
    {
        return $this->therapist->getDefaultDuration();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTherapist()
    {
        return $this->hasOne(Profile::className(), ['user_id' => 'therapist_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTherapistLang()
    {
        return $this->hasOne(TherapistLang::className(), ['user_id' => 'therapist_id'], ['language' => Yii::$app->language]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Profile::className(), ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInnerUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConversation()
    {
        return $this->hasOne(Conversation::className(), ['id' => 'conversation_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMessages()
    {
        return $this->hasMany(ConversationMessage::className(), ['conversation_id' => 'conversation_id', 'user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNewMessages()
    {
        return $this->hasMany(ConversationMessage::className(), ['conversation_id' => 'conversation_id', 'user_id' => 'user_id'])->where('`conversation_message`.status = :status', [':status' => ConversationMessage::STATUS_NEW]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNewTherapistMessages()
    {
        return $this->hasMany(ConversationMessage::className(), ['conversation_id' => 'conversation_id', 'user_id' => 'therapist_id'])->where('`conversation_message`.status = :status', [':status' => ConversationMessage::STATUS_NEW]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAppointments()
    {
        return $this->hasMany(Appointment::className(), ['therapist_id' => 'therapist_id', 'user_id' => 'user_id'])->orderBy('start DESC');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompletedAppointments()
    {
        return $this
            ->hasMany(Appointment::className(), ['therapist_id' => 'therapist_id', 'user_id' => 'user_id'])
            ->where(
                '`appointment`.status IN (:completed, :missed)',
                    [':completed' => Appointment::STATUS_COMPLETED, ':missed' => Appointment::STATUS_MISSED]
            )
            ->orderBy('number DESC');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNextAppointment()
    {
        return $this->hasOne(Appointment::className(), ['therapist_id' => 'therapist_id', 'user_id' => 'user_id'])
            ->where('`appointment`.status = :status', [':status' => Appointment::STATUS_NEW])
            ->orderBy('start DESC');
    }

    /**
     * Last message date
     */
    public function updateLastMessageDate()
    {
        $this->last_message_created = new Expression('NOW()');
        $this->save();
    }
}
