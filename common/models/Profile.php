<?php

namespace common\models;

use Yii;
use common\components\UploadedFileBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "profile".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $name
 * @property string $image
 * @property boolean $agree_mailing
 * @property string $birth_date
 * @property string $rec_token
 * @property string $type
 * @property boolean $show_tooltips
 */
class Profile extends ActiveRecord
{
    public $imageFile;

    const TYPE = 'user';
    const MAX_IMAGE_SIZE = 1048576; //1mb

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'profile';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => UploadedFileBehavior::className(),
                'subdirectoryName' => self::TYPE,
                'defaultImage' => 'default-user.jpg',
                'attributes' => $this->getImageAttributes(),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'requiredName'],
            [['user_id', 'number', 'hide_age'], 'integer'],
            [['alias', 'name', 'type'], 'string', 'max' => 255],
            [['image'], 'string', 'max' => 255],
            [['agree_mailing', 'show_tooltips'], 'boolean'],
            [['agree_mailing', 'show_tooltips'], 'default', 'value' => true],
            [['imageFile'], 'file', 'extensions' => 'jpg, gif, png'],
        ];
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function requiredName($attribute, $params)
    {
        if ($this->name == null && $this->type == 'user') {
            $this->addError($attribute, Yii::t('backend', 'Name is required'));
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('main', 'Name'),
            'image' => Yii::t('backend', 'Image'),
            'imageFile' => Yii::t('backend', 'Image'),
        ];
    }

    /**
     * @param array $row
     * @return Profile|Therapist
     */
    public static function instantiate($row)
    {
        switch ($row['type']) {
            case Therapist::TYPE:
                return new Therapist();
            default:
                return new self;
        }
    }

    /**
     * @return bool
     */
    public function beforeDelete()
    {
        $this->deleteMediaFile('image');

        return parent::beforeDelete();
    }

    /**
     * @return bool
     */
    public function isTherapist()
    {
        return $this->type === Therapist::TYPE;
    }

    /**
     * @return ActiveQuery
     */
    public function getAdditional()
    {
        return $this->hasOne(TherapistAdditional::className(), ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBots()
    {
        return $this->hasMany(UserBots::className(), ['user_id' => 'user_id']);
    }

    /**
     * @return array
     */
    public function getImageAttributes()
    {
        return [
            'image' => [
                'fileAttribute' => 'imageFile',
                'width' => 128,
                'height' => 128
            ],
        ];
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if ($insert && empty($this->alias)) {
            $this->alias = self::getUniqueAlias();
        }
        return parent::beforeSave($insert);
    }

    /**
     * @param int $length
     * @return string
     */
    public static function getUniqueAlias($length = 20)
    {
        $randomString = Yii::$app->getSecurity()->generateRandomString($length);
        if (!self::findOne(['alias' => $randomString])) {
            return $randomString;
        }

        return self::generateUniqueRandomString($length);
    }

    /**
     * @return bool
     */
    public function isPseudoTherapist()
    {
        return false;
    }

    /**
     * Is profile user deleted
     * @return bool
     */
    public function isDeleted()
    {
        return empty($this->user) || $this->user->isDeleted();
    }

    /**
     * @return bool
     */
    public function isProfileImageSocial()
    {
        if (substr($this->image, 0, 7) === 'http://') {
            return true;
        }

        if (substr($this->image, 0, 8) === 'https://') {
            return true;
        }

        return false;
    }

    /**
     * @return int|string
     */
    public function getPaidAppointmentsCount()
    {
        return Appointment::find()
            ->where(['user_id' => $this->user_id])
            ->andWhere(['payment_status' => Appointment::PAID])
            ->count();
    }
}
