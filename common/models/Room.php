<?php

namespace common\models;

use Yii;
use yii\helpers\Inflector;

/**
 * This is the model class for table "room".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $name
 * @property string $alias
 * @property string $type
 * @property string $password
 * @property string $status
 * @property integer $created
 */
class Room extends \yii\db\ActiveRecord
{
    const TYPE_APPOINTMENT = 'appointment';
    const TYPE_CUSTOM = 'custom';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'room';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    \yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['created'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'name'], 'required'],
            ['type', 'in', 'range' => [self::TYPE_APPOINTMENT, self::TYPE_CUSTOM]],
            [['alias'], 'unique'],
            [['user_id', 'created'], 'integer'],
            [['alias', 'status', 'password'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('main', 'ID'),
            'user_id' => Yii::t('main', 'User'),
            'name' => Yii::t('main', 'Room name'),
            'alias' => Yii::t('main', 'Alias'),
            'type' => Yii::t('main', 'Type'),
            'password' => Yii::t('main', 'Password'),
            'status' => Yii::t('main', 'Status'),
            'created' => Yii::t('main', 'Created'),
        ];
    }

    /**
     * @param $alias
     * @return static
     */
    public static function findByAlias($alias)
    {
        return static::findOne(['alias' => $alias]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfile()
    {
        return $this->hasOne(Profile::className(), ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTherapistLang()
    {
        return $this->hasOne(TherapistLang::className(), ['user_id' => 'user_id'], ['language' => Yii::$app->language]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAppointment()
    {
        return $this->hasOne(Appointment::className(), ['room_id' => 'id']);
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if (empty($this->alias)) {
            $this->alias = Inflector::slug($this->name);
        }
        return parent::beforeSave($insert);
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return Yii::$app->params['siteUrl'] . '/room/id/' . $this->alias;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current room
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password);
    }

    /**
     * @param $password
     */
    public function setPassword($password)
    {
        $this->password = Yii::$app->security->generatePasswordHash($password);
        $this->save(false);
    }

    /**
     *
     */
    public function removePassword()
    {
        $this->password = '';
        $this->save(false);
    }

    /**
     *
     */
    public function rememberPassword()
    {
        $session = Yii::$app->session;
        $session['room-' . $this->id] = $this->password;
    }

    /**
     * @return bool
     */
    public function checkRememberedPassword()
    {
        $session = Yii::$app->session;
        if (isset($session['room-' . $this->id]) && $session['room-' . $this->id] == $this->password) {
            return true;
        }

        return false;
    }

    /**
     * @param int $length
     * @return string
     */
    public static function generateAlias($length = 8)
    {
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $string = '';

        for ($i = 0; $i < $length; $i++) {
            $string .= $characters[mt_rand(0, strlen($characters) - 1)];
        }

        return $string;
    }

    /**
     * Create Base64 encoded room data for IOS devices
     *
     *  {
     *      "room":"ilia", // room id (in fact this is an EasyRTC appliaction name)
     *      "name":"Александр", // user name (unicode symbols should be encoded in UTF-8)
     *      "start":1503185754, // session start time in unix format
     *      "end":1503185954 // session end time in unix format
     *  }
     */
    public function getRoomData()
    {
        $appointment = $this->appointment;

        $data = [
            'room' => $this->alias,
            'name' => $appointment ? $appointment->user->name : Yii::$app->user->identity->getName(),
            'start' => $appointment ? $appointment->start : null,
            'end' => $appointment ? $appointment->end : null,
        ];

        return base64_encode(json_encode($data));
    }
}
