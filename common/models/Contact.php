<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use yii\helpers\Json;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "contact".
 *
 * @property integer $id
 * @property string $email
 * @property string $name
 * @property integer $country_id
 * @property integer $theme_id
 * @property string $theme
 * @property string $message
 * @property integer $created
 * @property integer $read
 */
class Contact extends ActiveRecord
{
    const STATUS_READ = 1;
    const STATUS_UNREAD = 0;

    const TYPE_CONSULTATION = 6;

    public $themeOptions = [];
    public $cptch;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'contact';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email', 'name', 'country_id'], 'required', 'on' => 'guest'],
            [['theme_id', 'message'], 'required'],
            [['country_id', 'theme_id', 'created', 'read'], 'integer'],
            [['message'], 'string'],
            ['email', 'email'],
            [['email', 'name'], 'string', 'max' => 255],
            [['theme'], 'string', 'max' => 500],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('main', 'ID'),
            'email' => Yii::t('main', 'Email'),
            'name' => Yii::t('main', 'Name'),
            'country_id' => Yii::t('main', 'Country'),
            'theme_id' => Yii::t('main', 'Theme'),
            'theme' => Yii::t('main', 'Theme'),
            'message' => Yii::t('main', 'Message'),
            'created' => Yii::t('main', 'Created'),
            'read' => Yii::t('main', 'Read'),
            'cptch' => Yii::t('main', 'Security code'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContactTheme()
    {
        return $this->hasOne(ContactTheme::className(), ['id' => 'theme_id']);
    }

    /**
     * @return array|ActiveRecord[]
     */
    public function getMenuContactThemes()
    {
        return ContactTheme::find()
            ->where(['not in', 'id', $this->customThemes()])
            ->all();
    }

    /**
     * @return array
     */
    public function customThemes()
    {
        return [self::TYPE_CONSULTATION];
    }

    /**
     * @return array
     */
    public function getThemesList()
    {
        $themes = $this->getMenuContactThemes();
        $items = [];
        $options = [];

        foreach ($themes as $theme) {
            if ($theme->id == 4) {
                //TODO: remove totally from DB
                continue;
            }

            $items[$theme->id] = $theme->title;

            if ($theme->use_additional_field) {
                $options[$theme->id] = [
                    'class' => 'use-additional-field',
                    'data-field-title' => $theme->additional_title,
                ];
            }
        }

        $this->themeOptions = $options;

        return $items;
    }

    /**
     * @return array
     */
    public function getThemesOptions()
    {
        return $this->themeOptions;
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        if ($insert) {
            $event = new NeedModerationEvent([
                'importance' => Event::IMPORTANCE_LOW,
                'text' => NeedModerationEvent::TARGET_CONTACT,
            ]);
            $event->data = Json::encode([
                'contact_id' => $this->id,
                'email' => $this->email,
                'name' => $this->name,
                'message' => $this->message,
                'theme' => ContactTheme::findOne($this->theme_id)->title . (empty($this->theme) ? '' : ' (' . $this->theme . ')'),
            ]);
            $event->save(false);
        }

        return parent::afterSave($insert, $changedAttributes);
    }
}
