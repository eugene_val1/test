<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use yii\web\HttpException;
use common\models\Partner;
use yii\helpers\Json;
use common\models\AppointmentEvent;

/**
 * This is the model class for table "appointment".
 *
 * @property integer $id
 * @property integer $therapist_id
 * @property integer $number
 * @property integer $user_id
 * @property integer $room_id
 * @property integer $start
 * @property integer $end
 * @property integer $duration
 * @property string  $comment
 * @property integer $rating
 * @property integer $amount
 * @property integer $payment_status
 * @property boolean $free
 * @property integer $status
 * @property integer $created
 * @property integer $overdue_informed
 */
class Appointment extends ActiveRecord
{
    const STATUS_NEW = 0;
    const STATUS_COMPLETED = 1;
    const STATUS_MISSED = 2;
    const STATUS_RESCHEDULE = 3;
    const STATUS_CANCELED = 4;

    const NOT_PAID = 0;
    const PENDING = 1;
    const PAID = 2;

    const OVERDUE_INFORMED = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'appointment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['therapist_id', 'user_id', 'status'], 'required'],
            [['start'], 'required', 'on' => 'schedule', 'message' => Yii::t('main', 'Please, choose time period')],
            [['therapist_id', 'user_id', 'room_id', 'start', 'end', 'duration', 'rating', 'amount', 'payment_status', 'status', 'created', 'number', 'overdue_informed'], 'integer'],
            [['start'], 'validateAmount'],
            [['start'], 'validatePartnership'],
            [['comment'], 'string'],
            [['start'], 'validateTime', 'on' => 'schedule'],
            [['free'], 'boolean'],
            [['free'], 'default', 'value' => false]
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created'],
                ],
            ],
        ];
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function validateTime($attribute, $params)
    {
        $duration = (($this->therapist) ? $this->therapist->duration : 60) * 60;
        $start = self::convertLocalToGMT($this->start);
        $end = self::convertLocalToGMT($this->end);
        if ($start < time() || ($end + $duration) < time() || ($end - $start) > $duration) {
            $this->addError($attribute, Yii::t('main', 'Start of the appointment must be in future!'));
        }
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function validateAmount($attribute, $params)
    {
        if ($this->amount == RegionPrice::DEFAULT_PRICE) {
            $this->addError($attribute, Yii::t('main', 'You have to set price for client country region'));
        }
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function validatePartnership($attribute, $params)
    {
        $partner = Partner::findOne(['user_id' => Yii::$app->user->id]);
        if (!$partner) {
            $this->addError($attribute, Yii::t('main', 'You have to provide billing information to create appoinments'));
        } else if ($partner->expired < time()) {
            $this->addError($attribute, Yii::t('main', 'Your billing information has expired. Update it before you create appoinment'));
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('main', 'ID'),
            'therapist_id' => Yii::t('main', 'Therapist'),
            'user_id' => Yii::t('main', 'User'),
            'room_id' => Yii::t('main', 'Room'),
            'number' => Yii::t('main', 'Number'),
            'start' => Yii::t('main', 'Session start'),
            'end' => Yii::t('main', 'Session end'),
            'comment' => Yii::t('main', 'Comment'),
            'rating' => Yii::t('main', 'Rating'),
            'amount' => Yii::t('main', 'Appointment cost'),
            'payment_status' => Yii::t('main', 'Payment status'),
            'status' => Yii::t('main', 'Status'),
            'free' => Yii::t('main', 'Free appointment')
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTherapist()
    {
        return $this->hasOne(Profile::className(), ['user_id' => 'therapist_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTherapistUser()
    {
        return $this->hasOne(User::className(), ['id' => 'therapist_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTherapistLang()
    {
        return $this->hasOne(TherapistLang::className(), ['user_id' => 'therapist_id'], ['language' => Yii::$app->language]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Profile::className(), ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserIdentity()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransaction()
    {
        return $this->hasOne(Transaction::className(), ['appointment_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Client::className(), ['therapist_id' => 'therapist_id', 'user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoom()
    {
        return $this->hasOne(Room::className(), ['id' => 'room_id']);
    }

    /**
     * Remove date
     */
    public function removeDate()
    {
        $start = $this->start;
        $this->start = 0;
        $this->end = 0;
        $this->save(false);

        $this->event(AppointmentEvent::TARGET_STATUS, self::STATUS_CANCELED, $start);
    }

    /**
     * @param $status
     */
    public function setStatus($status)
    {
        $this->status = ($status == self::STATUS_RESCHEDULE) ? self::STATUS_NEW : $status;
        $this->save(false);

        $this->event(AppointmentEvent::TARGET_STATUS, $status);
        if (($status == self::STATUS_COMPLETED || $status == self::STATUS_MISSED) && $this->payment_status == self::PAID && $this->transaction) {
            $this->transaction->settle();
        }
    }

    /**
     * @param $status
     */
    public function setPaymentStatus($status)
    {
        if ($this->payment_status != $status) {
            if ($status == self::PAID && !$this->room_id) {
                $room = new Room([
                    'user_id' => $this->therapist_id,
                    'type' => Room::TYPE_APPOINTMENT,
                    'name' => 'Appointment #' . $this->id,
                    'alias' => Room::generateAlias()
                ]);
                $room->save(false);
                $this->room_id = $room->id;
            }

            $this->payment_status = $status;
            $this->save(false);

            if ($status != self::PENDING) {
                $this->event(AppointmentEvent::TARGET_PAYMENT, $status);
            }
        }
    }

    /**
     * @param $timestamp
     * @return false|int
     */
    public static function convertLocalToGMT($timestamp)
    {
        if (Yii::$app->user->identity->timezone !== null) {
            Yii::$app->timezone = Yii::$app->user->identity->timezone;
        }

        return mktime(gmdate('H', $timestamp), gmdate('i', $timestamp), gmdate('s', $timestamp), gmdate('m', $timestamp), gmdate('d', $timestamp), gmdate('Y', $timestamp));
    }

    /**
     * Before save
     * @param bool $insert
     * @return bool
     */
    function beforeSave($insert)
    {
        if ($insert) {
            $number = Yii::$app->db->createCommand('SELECT MAX(number)+1 FROM appointment WHERE user_id = :user_id AND therapist_id = :therapist_id', [':user_id' => $this->user_id, ':therapist_id' => $this->therapist_id])->queryScalar();
            $this->number = ($number) ?: 1;
        }

        //free appointment
        if (!Yii::$app->settingManager->get('allowFreeAppointment', false) || !$this->therapist->free_appointment) {
            $this->free = false;
        } elseif ($this->free) {
            $this->payment_status = Appointment::PAID;
        }

        return parent::beforeSave($insert);
    }

    /**
     * After save
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if ($this->free && empty($this->room_id)) {
            $room = new Room([
                'user_id' => $this->therapist_id,
                'type' => Room::TYPE_APPOINTMENT,
                'name' => 'Appointment #' . $this->id,
                'alias' => Room::generateAlias()
            ]);
            $room->save(false);

            $this->room_id = $room->id;
            $this->save(false);
        }
    }

    /**
     * @return bool
     * @throws HttpException
     */
    function beforeDelete()
    {
        if ($this->payment_status == self::PAID) {
            if ($this->safePeriod() < time()) {
                $this->start = time();
                $this->setStatus(self::STATUS_COMPLETED);
                return false;
            } else {
                throw new HttpException(500, 'System error');
            }
        }

        return parent::beforeDelete();
    }

    /**
     * @return false|int
     */
    public function safePeriod()
    {
        return strtotime("+6 month", $this->created);
    }

    /**
     * @param string $category
     * @return array
     */
    public function getStatuses($category = 'main')
    {
        return [
            self::STATUS_NEW => Yii::t($category, 'New'),
            self::STATUS_RESCHEDULE => Yii::t($category, 'Reschedule'),
            self::STATUS_COMPLETED => Yii::t($category, 'Completed'),
            self::STATUS_MISSED => Yii::t($category, 'Missed'),
            self::STATUS_CANCELED => Yii::t($category, 'Canceled'),
        ];
    }

    /**
     * @param null $status
     * @param string $category
     * @return mixed|string
     */
    public function getStatusName($status = null, $category = 'main')
    {
        $statuses = $this->getStatuses($category);
        $status = isset($status) ? $status : $this->status;
        return isset($statuses[$status]) ? $statuses[$status] : Yii::t('main', 'Not set');
    }

    /**
     * @param string $category
     * @return array
     */
    public function getPaymentStatuses($category = 'main')
    {
        return [
            self::NOT_PAID => Yii::t($category, 'Not paid'),
            self::PENDING => Yii::t($category, 'Pending'),
            self::PAID => Yii::t($category, 'Paid')
        ];
    }

    /**
     * @return bool
     */
    public function isFirst()
    {
        return (int)$this->number === 1;
    }

    /**
     * @param null $status
     * @param string $category
     * @return mixed|string
     */
    public function getPaymentStatusName($status = null, $category = 'main')
    {
        $statuses = $this->getPaymentStatuses($category);
        $status = isset($status) ? $status : $this->payment_status;
        return isset($statuses[$status]) ? $statuses[$status] : Yii::t($category, 'Not set');
    }

    /**
     * Event
     * @param $text
     * @param $status
     * @param $start
     */
    private function event($text, $status, $start = null)
    {
        $event = [
            'type' => Event::TYPE_APPOINTMENT,
            'importance' => Event::IMPORTANCE_NORMAL,
            'text' => $text,
            'data' => Json::encode([
                'status' => $status,
                'appointment_id' => $this->id,
                'therapist_id' => $this->therapist_id,
                'user_id' => $this->user_id,
                'appointment_start' => empty($start) ? $this->start : $start
            ])
        ];
        foreach ([Event::RECEIVER_USER, Event::RECEIVER_ADMIN] as $target) {
            $targetEvent = new AppointmentEvent($event);
            $targetEvent->target = $target;
            $targetEvent->save(false);
        }
    }

    /**
     * '01:00:00'
     * @return string
     */
    public function getTimedEventDuration()
    {
        //in minutes
        $therapistDuration = ($this->therapist) ? $this->therapist->duration : 60;
        //in seconds
        $therapistDuration *= 60;

        return $this->seconds2human($therapistDuration);
    }

    /**
     * @param $ss
     * @return string
     */
    private function seconds2human($ss)
    {
        $s = $ss % 60;
        $m = floor(($ss % 3600) / 60);
        $h = floor(($ss % 86400) / 3600);
        $d = floor(($ss % 2592000) / 86400);
        $M = floor($ss / 2592000);

        return "$h:$m:$s";
    }
}