<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "certificate_lang".
 *
 * @property integer $id
 * @property integer $certificate_id
 * @property string $language
 * @property string $description
 *
 * @property Certificate $certificate
 */
class CertificateLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'certificate_lang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['language'], 'required'],
            [['certificate_id'], 'integer'],
            [['description'], 'string', 'max' => 100],
            [['language'], 'string', 'max' => 6]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('main', 'ID'),
            'certificate_id' => Yii::t('main', 'Certificate ID'),
            'language' => Yii::t('main', 'Language'),
            'description' => Yii::t('main', 'Description'),
        ];
    }

    public function getRelatedModel()
    {        
        return $this->hasOne(Certificate::className(), ['id' => 'region_id']);
    }
    
    public function getLanguageModel()
    {        
        return $this->hasOne(Language::className(), ['local' => 'language']);
    }
    
    public function getLanguageName()
    {        
        return $this->languageModel->name;
    }
}
