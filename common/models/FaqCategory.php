<?php

namespace common\models;

use Yii;
use himiklab\sortablegrid\SortableGridBehavior;

/**
 * This is the model class for table "faq_category".
 *
 * @property integer $id
 * @property string $number
 * @property string $name
 */
class FaqCategory extends \yii\db\ActiveRecord
{
    public $name;
    public $translatedAttributes = [
        'name' => 'string',
    ];
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'faq_category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['number', 'parent_id'], 'integer'],
            [['alias'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'number' => Yii::t('backend', 'Number'),
            'name' => Yii::t('backend', 'Name'),
            'parent_id' => Yii::t('backend', 'Parent category'),
        ];
    }
    
    public function behaviors() {
        return [
            'sort' => [
                'class' => SortableGridBehavior::className(),
                'sortableAttribute' => 'number'
            ],
        ];
    }
    
    public function getTranslations() {
        return $this->hasMany(FaqCategoryLang::className(), ['faq_category_id' => 'id'])->indexBy('language');
    }

    public function getTranslation() {
        return $this->hasOne(FaqCategoryLang::className(), ['faq_category_id' => 'id'])->where('language = :language', [':language' => Yii::$app->language]);
    }
    
    public function getDefaultTranslation() {
        return $this->hasOne(FaqCategoryLang::className(), ['faq_category_id' => 'id'])->where('language = :language', [':language' => Yii::$app->sourceLanguage]);
    }
    
    public function getQuestions() {
        return $this->hasMany(FAQ::className(), ['category_id' => 'id']);
    }
    
    public function getChildren() {
        return $this->hasMany(self::className(), ['parent_id' => 'id'])->orderBy('number');
    }
    
    public function getParent() {
        return $this->hasOne(self::className(), ['id' => 'parent_id']);
    }
    
    public static function getTree() {
        $root = 0;
        $categories = self::find()->with(['translation', 'children'])->where(['parent_id' => $root])->orderBy('number')->all();
        return self::treeCategory([], $categories, $root);
    }
    
    public static function treeCategory($result, $categories, $level) {
        foreach($categories as $category){
            $result[$category->id] = str_repeat("---", $level) . ' ' . $category->translation->name;
            if(count($category->children) > 0){
                $result = self::treeCategory($result, $category->children, $level+1);
            }
        }
        
        return $result;
    }
    
    public function withParents() {
        return $this->addParent($this, []);
    }
    
    public function addParent($category, $paretns) {
        $paretns[] = $category->id;
        if($category->parent){
            $paretns = $this->addParent($category->parent, $paretns);
        }
        return $paretns;
    }
    
    public function withChildren() {
        return $this->addChild($this, []);
    }
    
    public function addChild($category, $children) {
        $children[] = $category->id;
        if(count($category->children) > 0){
            foreach($category->children as $child){
                $children = $this->addChild($child, $children);
            }
        }
        return $children;
    }

    public function afterFind() {
        $translation = ($this->translation !== null) ? $this->translation : $this->defaultTranslation;
        
        foreach ($this->translatedAttributes as $attribute => $type) {
                $this->{$attribute} = $translation->{$attribute};
        }
            
        parent::afterFind();
    }
    
    public static function findByAlias($alias)
    {
        return static::findOne(['alias' => $alias]);
    }
}
