<?php

namespace common\models;

use Yii;
use common\models\Language;

/**
 * This is the model class for table "page_lang".
 *
 * @property integer $id
 * @property integer $page_id
 * @property string $language
 * @property string $name
 * @property string $title
 * @property string $content
 * @property string $keywords
 * @property string $description
 *
 * @property Page $page
 */
class PageLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'page_lang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['language', 'name'], 'required'],
            [['page_id'], 'integer'],
            [['title', 'content', 'keywords', 'description', 'name'], 'string'],
            [['language'], 'string', 'max' => 6],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'language' => Yii::t('main', 'Language'),
            'title' => Yii::t('main', 'Title'),
            'content' => Yii::t('backend', 'Content'),
            'name' => Yii::t('main', 'Name'),
            'keywords' => Yii::t('main', 'Keywords'),
            'description' => Yii::t('backend', 'Page description'),
        ];
    }

    public function getRelatedModel()
    {        
        return $this->hasOne(Page::className(), ['id' => 'page_id']);
    }
    
    public function getLanguageModel()
    {        
        return $this->hasOne(Language::className(), ['local' => 'language']);
    }
    
    public function getLanguageName()
    {        
        return $this->languageModel->name;
    }
    
    public function afterSave($insert, $changedAttributes) {
        parent::afterSave($insert, $changedAttributes);
        
        if($insert && $this->language == Yii::$app->sourceLanguage){
            $languages = Language::find()->where('local != :local', [':local' => Yii::$app->sourceLanguage])->all();

            foreach ($languages as $language) {
                $langTranslation = new self();
                $langTranslation->attributes = $this->attributes;
                $langTranslation->language = $language->local;
                $langTranslation->save(false);
            }
        }
    }
}
