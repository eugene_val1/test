<?php

namespace common\models;

use Yii;
use common\components\UploadedFileBehavior;
use common\models\CategoryPage;

class HomeFieldSlide extends HomeSlide
{
    const TYPE = 'field';
    public $imageFile;
    public $image;
    public $interval;
    
    private $_categories;

    public static function find()
    {
        return new TypeQuery(get_called_class(), ['type' => self::TYPE]);
    }
    
    /**
     * @inheritdoc
     */
    public function behaviors() {
        return array_merge(
            parent::behaviors(),
            [
                [
                    'class' => UploadedFileBehavior::className(),
                    'subdirectoryName' => 'home',
                    'attributes' => ['image' => ['fileAttribute' => 'imageFile']],
                    'resizeType' => UploadedFileBehavior::RESIZE_NONE
                ],
            ]
        );
    }
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        $rules = parent::rules();
        return array_merge($rules, [
            [['image'], 'string', 'max' => 255],
            ['interval', 'integer'],
            [['imageFile'], 'file', 'extensions'=>'jpg, gif, png'],
        ]);
    }
    
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'image' => Yii::t('backend', 'Image'),
            'imageFile' => Yii::t('backend', 'Image'),
            'interval' => Yii::t('backend', 'Carousel rotating interval, sec')
        ];
    }
  
    public function getData(){
        return [
            'image' => $this->image,
            'interval' => $this->interval
        ];
    }
    
    public function beforeDelete() {
        $this->deleteMediaFile('image');
        return parent::beforeDelete();
    }

    public function getCategories(){
        if($this->_categories === null){
            $this->_categories = CategoryPage::find()->with('lastArticle')->all();
        }        
        return $this->_categories;
    }
}