<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "region_price".
 *
 * @property integer $id
 * @property integer $region_id
 * @property integer $therapist_id
 * @property integer $amount
 *
 * @property User $therapist
 * @property Region $region
 */
class RegionPrice extends ActiveRecord
{
    const DEFAULT_PRICE = 0;

    /**
     * @var string
     */
    public $regionName;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'region_price';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['region_id', 'therapist_id', 'amount'], 'required'],
            [['region_id', 'therapist_id', 'amount'], 'integer', 'min' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('main', 'ID'),
            'region_id' => Yii::t('main', 'Region'),
            'therapist_id' => Yii::t('main', 'Therapist'),
            'amount' => Yii::t('main', 'Price'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTherapist()
    {
        return $this->hasOne(User::className(), ['id' => 'therapist_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRegion()
    {
        return $this->hasOne(Region::className(), ['id' => 'region_id']);
    }
}
