<?php

namespace common\models;

use Yii;
use common\components\UploadedFileBehavior;
use yii\caching\TagDependency;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "page".
 *
 * @property integer $id
 * @property string $alias
 * @property string $image
 * @property integer $published
 */
class Page extends ActiveRecord
{
    const TYPE = 'custom';
    const CACHE_DURATION = 86400; //1 day in seconds

    public $imageFile;
    public $title;
    public $content;
    public $name;
    public $keywords;
    public $description;
    public $translatedAttributes = [
        'title' => 'string',
        'content' => 'html',
        'keywords' => 'text',
        'description' => 'text',
        'name' => 'string'
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'page';
    }

    /**
     * @param string $alias
     * @return string
     */
    public static function getCacheKey($alias)
    {
        return 'page-' . $alias;
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => UploadedFileBehavior::className(),
                'subdirectoryName' => $this->tableName(),
                'attributes' => $this->getImageAttributes()
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['published', 'noindex', 'nofollow'], 'integer'],
            ['alias', 'unique'],
            [['alias', 'image', 'type', 'section'], 'string', 'max' => 255],
            [['imageFile'], 'file', 'extensions' => 'jpg, gif, png'],
        ];
    }

    /**
     * @param array $row
     * @return CategoryPage|Page|SystemPage
     */
    public static function instantiate($row)
    {
        switch ($row['type']) {
            case SystemPage::TYPE:
                return new SystemPage();
            case CategoryPage::TYPE:
                return new CategoryPage();
            default:
                return new self;
        }
    }

    /**
     * @return array
     */
    public function getTypes()
    {
        return [
            'custom' => Yii::t('main', 'Custom page'),
            'system' => Yii::t('main', 'System page'),
            'category' => Yii::t('main', 'Category page'),
        ];
    }

    /**
     * @return array
     */
    public function getCreatableTypes()
    {
        return [
            'custom' => Yii::t('main', 'Custom page'),
            'category' => Yii::t('main', 'Category page'),
        ];
    }

    /**
     * @return mixed
     */
    public function getTypeName()
    {
        $types = $this->getTypes();

        return $types[$this->type];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(PageLang::className(), ['page_id' => 'id'])
            ->indexBy('language');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslation()
    {
        return $this->hasOne(PageLang::className(), ['page_id' => 'id'])
            ->where('language = :language', [':language' => Yii::$app->language]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDefaultTranslation()
    {
        return $this->hasOne(PageLang::className(), ['page_id' => 'id'])
            ->where('language = :language', [':language' => Yii::$app->sourceLanguage]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * After find
     */
    public function afterFind()
    {
        $translation = ($this->translation !== null)
            ? $this->translation
            : $this->defaultTranslation;

        foreach ($this->translatedAttributes as $attribute => $type) {
            $this->{$attribute} = $translation->{$attribute};
        }

        parent::afterFind();
    }

    /**
     * Finds page by alias
     *
     * @param string $alias
     * @return static|null
     */
    public static function findByAlias($alias)
    {
        return static::findOne(['alias' => $alias]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'image' => Yii::t('backend', 'Image'),
            'alias' => Yii::t('backend', 'Alias'),
            'imageFile' => Yii::t('backend', 'Image'),
            'type' => Yii::t('main', 'Type'),
            'section' => Yii::t('backend', 'Section'),
            'published' => Yii::t('backend', 'Published'),
            'title' => Yii::t('main', 'Title'),
            'content' => Yii::t('backend', 'Content'),
            'name' => Yii::t('main', 'Name'),
            'keywords' => Yii::t('main', 'Keywords'),
            'description' => Yii::t('backend', 'Page description'),
            'noindex' => Yii::t('backend', 'Add noindex tag'),
            'nofollow' => Yii::t('backend', 'Add nofollow tag'),
        ];
    }

    /**
     * @return array
     */
    public function getImageAttributes()
    {
        return [
            'image' => [
                'fileAttribute' => 'imageFile',
                'width' => 1920,
                'height' => 500,
            ]
        ];
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if (!$this->type) {
            $this->type = self::TYPE;
        }

        if (!$insert) {
            TagDependency::invalidate(Yii::$app->frontendCache, self::getCacheKey($this->alias));
        }

        return parent::beforeSave($insert);
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return '/page/' . $this->alias;
    }

    /**
     * @return int|string
     */
    public static function countAll()
    {
        return static::find()->count();
    }

    /**
     * @param $limit
     * @param $offset
     * @return array
     */
    public static function sitemapUrls($limit, $offset)
    {
        $urls = [];
        $items = static::find()
            ->limit($limit)
            ->offset($offset)
            ->all();

        foreach ($items as $item) {
            $urls[$item->id] = [
                'loc' => Yii::$app->params['siteUrl'] . $item->getUrl(),
                'changefreq' => 'weekly',
                'priority' => '0.6',
            ];

            if ($item->image) {
                $urls[$item->id]['images'][] = [
                    'loc' => \htmlspecialchars($item->getFileUrl('image')),
                    'title' => $item->title,
                ];
            }
        }

        return $urls;
    }

    /**
     * @return bool
     */
    public function beforeDelete()
    {
        if (parent::beforeDelete() && $this->type != SystemPage::TYPE) {
            if ($this->type == CategoryPage::TYPE) {
                Article::updateAll(['category_id' => 0], ['category_id' => $this->id]);
            }

            TagDependency::invalidate(Yii::$app->frontendCache, self::getCacheKey($this->alias));

            return true;
        }

        return false;
    }

    /**
     * Set meta tags
     */
    public function setMetaTags()
    {
        Yii::$app->controller->view->title = Yii::$app->view->title;

        if (!empty($this->title)) {
            Yii::$app->controller->view->title .= ' | ' . $this->title;
        }

        if ($this->keywords !== '') {
            Yii::$app->controller->view->registerMetaTag([
                'name' => 'keywords',
                'content' => $this->keywords,
            ], 'keywords');
        }
        if ($this->description !== '') {
            Yii::$app->controller->view->registerMetaTag([
                'name' => 'description',
                'content' => $this->description
            ], 'description');
        }

        if ($this->noindex || $this->nofollow) {
            $robots = $this->noindex ? 'noindex' : '';
            if ($this->nofollow) {
                $robots .= ($robots == '') ? 'nofollow' : ', nofollow';
            }

            Yii::$app->controller->view->registerMetaTag([
                'name' => 'robots',
                'content' => $robots
            ], 'robots');
        }
    }

}
