<?php

namespace common\models;

use common\models\Event;
use common\models\Transaction;

/**
 * Class PaymentErrorEvent
 * @package common\models
 */
class PaymentErrorEvent extends Event
{
    /**
     * @return string
     */
    public function getAlias()
    {
        return 'payment_error';
    }

    /**
     * @return TypeQuery
     */
    public static function find()
    {
        return new TypeQuery(get_called_class(), ['type' => Event::TYPE_PAYMENT_ERROR]);
    }

    /**
     * @return array|null|\yii\db\ActiveRecord
     */
    public function getTransaction()
    {
        $id = (isset($this->_data['transaction_id'])) ? $this->_data['transaction_id'] : null;
        return Transaction::find()->where(['id' => $id])->one();
    }

    /**
     * @return null
     */
    public function getError()
    {
        return (isset($this->_data['error'])) ? $this->_data['error'] : null;
    }
}