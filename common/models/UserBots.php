<?php

namespace common\models;
use common\components\bots\FacebookBotService;
use common\components\bots\TelegramBotService;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * Class PaidAppointments
 * @package common\models
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $type
 * @property string $target_token
 * @property integer $created
 */
class UserBots extends ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_bots';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'target_token'], 'required'],
            [['type'], 'in', 'range' => [TelegramBotService::TYPE, FacebookBotService::TYPE]],
            [['type', 'user_id'], 'unique', 'targetAttribute' => ['type', 'user_id']]
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created',
                'updatedAtAttribute' => false,
                'value' => gmdate("Y-m-d H:i:s", time())
            ],
        ];
    }
}
