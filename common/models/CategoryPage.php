<?php

namespace common\models;

class CategoryPage extends Page
{
    const TYPE = 'category';   

    public static function find()
    {
        return new TypeQuery(get_called_class(), ['type' => self::TYPE]);
    }
    
    public function beforeSave($insert) {
        $this->type = self::TYPE;
        return true;
    }
    
    public function getUrl(){
        return '/category/' . $this->alias;
    }
    
    public function getLastArticle()
    {      
        return $this->hasOne(TextArticle::className(), ['category_id' => 'id'])->orderBy('created DESC');
    }
}