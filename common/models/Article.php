<?php

namespace common\models;

use common\components\UploadedFileBehavior;
use Yii;
use yii\caching\TagDependency;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "article".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $alias
 * @property integer $type
 * @property string $title
 * @property string $content
 * @property string $image
 * @property string $preview_image
 * @property string $cover_image
 * @property string $video_source
 * @property string $video_source_id
 * @property integer $created
 * @property integer $updated
 * @property integer $published
 * @property integer $featured
 * @property integer $votes_count
 * @property integer $comments_count
 * @property integer $views_count
 * @property string $preview_image_alt
 * @property string $image_alt
 * @property string $cover_image_alt
 * @property string $page_description
 * @property string $short_description
 */
class Article extends ActiveRecord
{
    const STATUS_PENDING = 'pending';
    const STATUS_PUBLISHED = 'published';
    const STATUS_REVIEWED = 'reviewed';

    const MAX_IMAGE_SIZE = 2097152;//2mb

    const FEATURED = 1;
    const NOT_FEATURED = 0;

    const NOT_INDEX = 0;
    const IN_INDEX = 1;
    const NEED_INDEX = 2;
    const NEED_REINDEX = 3;

    const CACHE_DURATION = 86400; //1 day in seconds

    const VIEW_COOKIES_NAME = 'article-view';
    const VIEW_COOKIES_EXPIRE = 157680000; //5 years

    /**
     * @var array
     */
    public $tagList = [];

    /**
     * @var string
     */
    public $preview_imageFile;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'article';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'user_id',
                    'created',
                    'updated',
                    'votes_count',
                    'views_count',
                    'featured',
                    'comments_count',
                    'index_status',
                    'noindex',
                    'nofollow',
                ],
                'integer',
            ],
            [
                [
                    'content',
                    'short_description',
                    'type', 'page_title',
                    'page_description',
                    'page_keywords',
                ],
                'string',
            ],
            [['title', 'published', 'alias'], 'string', 'max' => 500],
            [
                [
                    'image',
                    'preview_image',
                    'cover_image',
                    'video_source',
                    'video_source_id',
                ], 'string', 'max' => 255,
            ],
            [
                [
                    'preview_image_alt',
                    'image_alt',
                    'cover_image_alt',
                ], 'string', 'max' => 1024,
            ],
            [['tagList'], 'each', 'rule' => ['string']],

            [['preview_image'], 'required', 'on' => 'insert'],
            [['preview_imageFile'], 'file', 'extensions' => 'jpg, gif, png'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('main', 'ID'),
            'user_id' => Yii::t('main', 'Post as'),
            'user_name' => Yii::t('main', 'User name'),
            'type' => Yii::t('main', 'Type'),
            'title' => Yii::t('main', 'Title'),
            'content' => Yii::t('main', 'Content'),
            'image' => Yii::t('main', 'Image'),
            'preview_image' => Yii::t('backend', 'Preview image'),
            'preview_imageFile' => Yii::t('backend', 'Preview image'),
            'cover_image' => Yii::t('main', 'Cover image'),
            'image_alt' => Yii::t('main', 'Image description'),
            'preview_image_alt' => Yii::t('backend', 'Preview image description'),
            'cover_image_alt' => Yii::t('backend', 'Cover image description'),
            'video_source' => Yii::t('backend', 'Video source'),
            'video_url' => Yii::t('main', 'Video url'),
            'created' => Yii::t('main', 'Created'),
            'updated' => Yii::t('main', 'Updated'),
            'published' => Yii::t('main', 'Published'),
            'featured' => Yii::t('main', 'Featured'),
            'tagList' => Yii::t('main', 'Tags'),
            'page_title' => Yii::t('main', 'Page title'),
            'page_description' => Yii::t('main', 'Page description'),
            'page_keywords' => Yii::t('main', 'Page keywords'),
            'noindex' => Yii::t('backend', 'Add noindex tag'),
            'nofollow' => Yii::t('backend', 'Add nofollow tag'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated'],
                ],
            ],
            [
                'class' => UploadedFileBehavior::className(),
                'subdirectoryName' => $this->tableName(),
                'defaultImage' => 'default-article.jpg',
                'attributes' => $this->getImageAttributes(),
                'resizeType' => UploadedFileBehavior::RESIZE_FORCE_DIMENSIONS,
            ],
        ];
    }

    /**
     * @return array
     */
    public function getImageAttributes()
    {
        return [
            'preview_image' => [
                'fileAttribute' => 'preview_imageFile',
                'width' => 1200,
                'height' => 630,
            ],
        ];
    }

    /**
     * @param string $alias
     * @return string
     */
    public static function getCacheKey($alias)
    {
        return 'article-' . $alias;
    }

    /**
     * @param $alias
     * @return static
     */
    public static function findByAlias($alias)
    {
        return static::findOne(['alias' => $alias]);
    }

    /**
     * @return ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getVote()
    {
        return $this->hasOne(Vote::className(), ['target_id' => 'id'])
            ->where([
                'target' => static::tableName(),
                'user_id' => Yii::$app->user->id,
            ]);
    }

    /**
     * @return ActiveQuery
     */
    public function getProfile()
    {
        return $this->hasOne(Profile::className(), ['user_id' => 'user_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getTherapist()
    {
        return $this->hasOne(Therapist::className(), ['user_id' => 'user_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getTherapistLang()
    {
        return $this->hasOne(TherapistLang::className(), ['user_id' => 'user_id'], ['language' => Yii::$app->language]);
    }

    /**
     * @return ActiveQuery
     */
    public function getTags()
    {
        return $this->hasMany(Tag::className(), ['id' => 'tag_id'])
            ->viaTable('{{%article_tag}}', ['article_id' => 'id']);
    }

    /**
     * After find
     */
    public function afterFind()
    {
        if (count($this->tags) > 0) {
            $this->tagList = $this->mapTagList($this->tags);
        }
        return parent::afterFind();
    }

    /**
     * Before save
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if (!$this->page_title) {
            $this->page_title = $this->title;
        }

        if (!$insert) {
            TagDependency::invalidate(Yii::$app->frontendCache, self::getCacheKey($this->alias));
        }

        return parent::beforeSave($insert);
    }

    /**
     * After save
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        if ($insert || $this->scenario = 'update') {
            $this->updateTags();
        }

        return parent::afterSave($insert, $changedAttributes);
    }

    /**
     * @param $tags
     * @return array
     */
    public function mapTagList($tags)
    {
        return ArrayHelper::map($tags, 'name', 'name');
    }

    /**
     * @return array
     */
    public function getTagList()
    {
        if (is_array($this->tagList) && count($this->tagList) > 0) {
            return array_combine($this->tagList, $this->tagList);
        }
        return [];
    }

    /**
     * Update tags
     */
    public function updateTags()
    {
        $tags = Tag::find()->where(['name' => $this->tagList])->all();

        if (is_array($this->tagList) && count($this->tagList) > 0) {
            $newTags = array_diff($this->tagList, $this->mapTagList($tags));
        } else {
            $newTags = $this->mapTagList($tags);
        }

        foreach ($newTags as $newTag) {
            $tag = new Tag([
                'name' => $newTag
            ]);
            $tag->save(false);
            $tags[] = $tag;
        }

        $this->addTags($this->getTagDifference($tags, $this->tags));
        $this->deleteTags($this->getTagDifference($this->tags, $tags));
    }

    /**
     * @param array $array1
     * @param array $array2
     * @return array
     */
    public function getTagDifference($array1, $array2)
    {
        return array_udiff($array1, $array2, function ($object1, $object2) {
            return $object1->id - $object2->id;
        });
    }

    /**
     * @param $tags
     * @throws \yii\db\Exception
     */
    public function addTags($tags)
    {
        if (count($tags) > 0) {
            $rows = [];
            foreach ($tags as $tag) {
                /** @var Tag $tag */
                $tag->updateFrequency(+1);
                $rows[] = [
                    'article_id' => $this->id,
                    'tag_id' => $tag->id
                ];
            }

            Yii::$app->db
                ->createCommand()
                ->batchInsert(ArticleTag::tableName(), ['article_id', 'tag_id'], $rows)
                ->execute();
        }
    }

    /**
     * @param $tags
     */
    public function deleteTags($tags)
    {
        if (empty($tags)) {
            return;
        }
        $tagIDs = [];
        foreach ($tags as $tag) {
            /** @var Tag $tag */
            $tag->updateFrequency(-1);
            $tagIDs[] = $tag->id;

        }

        ArticleTag::deleteAll([
            'article_id' => $this->id,
            'tag_id' => $tagIDs
        ]);
    }

    /**
     * @return bool
     */
    public function beforeDelete()
    {
        $tagIDs = ArticleTag::find()
            ->select('tag_id')
            ->where(['article_id' => $this->id])
            ->column();

        Tag::updateAllCounters(['frequency' => -1], ['in', 'id', $tagIDs]);
        ArticleTag::deleteAll(['article_id' => $this->id]);

        if (!empty($this->preview_image)) {
            $this->deleteMediaFile('preview_image');
        }

        TagDependency::invalidate(Yii::$app->frontendCache, self::getCacheKey($this->alias));

        return parent::beforeDelete();
    }

    /**
     * @param $attribute
     * @param $value
     */
    public function updateCounter($attribute, $value)
    {
        $this->{$attribute} += $value;
        $this->save(false);
    }

    /**
     * @param array $row
     * @return Article|TextArticle|TreatArticle|VideoArticle
     */
    public static function instantiate($row)
    {
        switch ($row['type']) {
            case TreatArticle::TYPE:
                return new TreatArticle(['type' => TreatArticle::TYPE]);
            case VideoArticle::TYPE:
                return new VideoArticle(['type' => VideoArticle::TYPE]);
            case TextArticle::TYPE:
                return new TextArticle(['type' => TextArticle::TYPE]);
            default:
                return new self;
        }
    }

    /**
     * @return array
     */
    public static function getTypes()
    {
        return [
            'treat' => Yii::t('main', 'Treat'),
            'video' => Yii::t('main', 'Video'),
            'text' => Yii::t('main', 'Publication'),
        ];
    }

    /**
     * @return mixed
     */
    public function getTypeName()
    {
        $types = self::getTypes();

        return $types[$this->type];
    }

    /**
     * @return array
     */
    public static function getPublishStatuses()
    {
        return [
            self::STATUS_PUBLISHED => Yii::t('main', 'Published'),
            self::STATUS_REVIEWED => Yii::t('main', 'Reviewed'),
            self::STATUS_PENDING => Yii::t('main', 'Pending'),
        ];
    }

    /**
     * @return array
     */
    public static function getFeaturedStatuses()
    {
        return [
            self::FEATURED => Yii::t('main', 'Featured'),
            self::NOT_FEATURED => Yii::t('main', 'Not featured'),
        ];
    }

    /**
     * @return mixed
     */
    public function getPublishStatusName()
    {
        $statuses = self::getPublishStatuses();

        return $statuses[$this->published];
    }

    /**
     * @return mixed
     */
    public function getFeaturedStatusName()
    {
        $statuses = self::getFeaturedStatuses();

        return $statuses[$this->featured];
    }

    /**
     * @param $status
     */
    public function setStatus($status)
    {
        if ($status === self::STATUS_PENDING) {
            $event = new NeedModerationEvent([
                'type' => Event::TYPE_NEED_MODERATION,
                'importance' => Event::IMPORTANCE_NORMAL,
                'text' => NeedModerationEvent::TARGET_ARTICLE,
                'data' => Json::encode([
                    'article_id' => $this->id,
                ])
            ]);
            $event->save(false);
        } else if ($status === self::STATUS_PUBLISHED) {
            $this->index_status = $this->index_status == self::IN_INDEX
                ? self::NEED_REINDEX
                : self::NEED_INDEX;
        }

        $this->published = $status;
        $this->save(false);
    }

    /**
     * @param $status
     */
    public function setFeaturedStatus($status)
    {
        $this->featured = $status;
        $this->save(false);
    }

    /**
     * @return int|string
     */
    public static function countAll()
    {
        return static::find()
            ->where(['published' => static::STATUS_PUBLISHED])
            ->count();
    }

    /**
     * @param $limit
     * @param $offset
     * @return array
     */
    public static function sitemapUrls($limit, $offset)
    {
        $urls = [];
        $items = static::find()
            ->where(['published' => static::STATUS_PUBLISHED])
            ->limit($limit)
            ->offset($offset)
            ->all();

        foreach ($items as $item) {
            /** @var Article $item */
            $urls[$item->id] = [
                'loc' => Yii::$app->urlManager->createAbsoluteUrl([
                    'field/view',
                    'alias' => $item->alias,
                ]),
                'changefreq' => 'weekly',
                'lastmod' => $item->updated,
                'priority' => '0.6',
            ];

            if ($item->image) {
                $urls[$item->id]['images'][] = [
                    'loc' => \htmlspecialchars($item->getFileUrl('image')),
                    'title' => $item->title,
                ];
            }
        }

        return $urls;
    }

    /**
     * @return string
     */
    public function getAuthorName()
    {
        if (!$this->profile) {
            return 'Пользователь';
        }

        return $this->profile->type === Therapist::TYPE && $this->therapistLang
            ? $this->therapistLang->name
            : $this->profile->name;
    }

    /**
     * View helper fore rendering articles blocks
     * @param int $index
     * @param int $total
     *
     * @return bool
     */
    public static function isWideBlock($index, $total)
    {
        $total = $total < 12 ? 20 : $total;

        return in_array($index, range(0, $total, 6), true)
            || in_array($index, range(5, $total, 6), true);
    }

    /**
     * Meta tags
     */
    public function setMetaTags()
    {
        if (!empty($this->page_title)) {
            Yii::$app->view->title = Html::encode($this->page_title);
            Yii::$app->view->registerMetaTag([
                'property' => 'og:title',
                'content' => ($this->type == TreatArticle::TYPE)
                    ? Yii::t('main', 'Treat')
                    : strip_tags($this->page_title)
            ], 'og:title');
        }

        if ($this->page_keywords !== '') {
            Yii::$app->view->registerMetaTag([
                'name' => 'keywords',
                'content' => $this->page_keywords,
            ], 'keywords');
        }

        if ($this->page_description !== '') {
            Yii::$app->view->registerMetaTag([
                'name' => 'description',
                'content' => strip_tags($this->page_description)
            ], 'description');
        }

        if ($this->noindex || $this->nofollow) {
            $robots = $this->noindex ? 'noindex' : '';
            if ($this->nofollow) {
                $robots .= ($robots == '') ? 'nofollow' : ', nofollow';
            }

            Yii::$app->view->registerMetaTag([
                'name' => 'robots',
                'content' => $robots
            ], 'robots');
        }

        Yii::$app->view->registerMetaTag([
            'property' => 'og:url',
            'content' => Yii::$app->urlManager->createAbsoluteUrl(['field/view', 'alias' => $this->alias])
        ], 'og:url');

        $ogDescription = strip_tags($this->short_description);
        if (empty($ogDescription)) {
            $ogDescription = strip_tags($this->page_description);
        }
        Yii::$app->view->registerMetaTag([
            'property' => 'og:description',
            'content' => $ogDescription,
        ], 'og:description');

        if (empty($this->preview_image)) {
            $ogImage = $this->type === TreatArticle::TYPE
                ? $this->getAuthorImage()
                : $this->getFileUrl('image');
        } else {
            $ogImage = $this->getFileUrl('preview_image');
        }

        Yii::$app->view->registerMetaTag([
            'property' => 'og:image',
            'content' => $ogImage,
        ], 'og:image');

        Yii::$app->view->registerMetaTag([
            'property' => 'og:image:secure_url',
            'content' => $ogImage,
        ], 'og:image:secure_url');
    }
}
