<?php

namespace common\models;

use Yii;
use yii\helpers\Json;
use himiklab\sortablegrid\SortableGridBehavior;

/**
 * This is the model class for table "home_slide".
 *
 * @property integer $id
 */
class HomeSlide extends \yii\db\ActiveRecord
{
    public $title;
    public $content;
    public $translatedAttributes = [
        'title' => 'string',
        'content' => 'html'
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'home_slide';
    }

    public function behaviors()
    {
        return [
            'sort' => [
                'class' => SortableGridBehavior::className(),
                'sortableAttribute' => 'number'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['number'], 'integer'],
            [['type'], 'required'],
            ['type', 'in', 'range' => array_keys($this->getTypes())],
            [['data'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'number' => Yii::t('main', 'Number'),
            'title' => Yii::t('main', 'Title'),
            'content' => Yii::t('main', 'Content'),
            'type' => Yii::t('main', 'Type')
        ];
    }

    public function getTranslations()
    {
        return $this->hasMany(HomeSlideLang::className(), ['home_slide_id' => 'id'])->indexBy('language');
    }

    public function getTranslation()
    {
        return $this->hasOne(HomeSlideLang::className(), ['home_slide_id' => 'id'])->where('language = :language', [':language' => Yii::$app->language]);
    }

    public function getDefaultTranslation()
    {
        return $this->hasOne(HomeSlideLang::className(), ['home_slide_id' => 'id'])->where('language = :language', [':language' => Yii::$app->sourceLanguage]);
    }

    /**
     *
     */
    public function afterFind()
    {
        $translation = ($this->translation !== null) ? $this->translation : $this->defaultTranslation;

        foreach ($this->translatedAttributes as $attribute => $type) {
            $this->{$attribute} = $translation->{$attribute};
        }

        $data = Json::decode($this->data);
        foreach ($data as $attribute => $value) {
            $this->{$attribute} = $value;
        }

        parent::afterFind();
    }

    /**
     * @param array $row
     * @return HomeCustomersSlide|HomeFeaturesSlide|HomeFieldSlide|HomeFullscreenSlide|HomeImageSlide|HomeSlide|HomeTextSlide|HomeVideoSlide
     */
    public static function instantiate($row)
    {
        switch ($row['type']) {
            case HomeTextSlide::TYPE:
                return new HomeTextSlide();
            case HomeImageSlide::TYPE:
                return new HomeImageSlide();
            case HomeFullscreenSlide::TYPE:
                return new HomeFullscreenSlide();
            case HomeVideoSlide::TYPE:
                return new HomeVideoSlide();
            case HomeFeaturesSlide::TYPE:
                return new HomeFeaturesSlide();
            case HomeCustomersSlide::TYPE:
                return new HomeCustomersSlide();
            case HomeFieldSlide::TYPE:
                return new HomeFieldSlide();
            default:
                return new self;
        }
    }

    /**
     * @return array
     */
    public function getTypes()
    {
        return [
            'text' => Yii::t('backend', 'Text slide'),
            'image' => Yii::t('backend', 'Image slide'),
            'fullscreen' => Yii::t('backend', 'Fullscreen slide'),
            'video' => Yii::t('backend', 'Video slide'),
            'features' => Yii::t('backend', 'Features slide'),
            'customers' => Yii::t('backend', 'Customers feedback slide'),
            'field' => Yii::t('backend', 'Field posts slide'),
        ];
    }

    /**
     * @return mixed
     */
    public function getTypeName()
    {
        $types = $this->getTypes();
        return $types[$this->type];
    }
}
