<?php

namespace common\models;

use Yii;
use common\components\UploadedFileBehavior;

class HomeImageSlide extends HomeSlide
{
    const TYPE = 'image';
    public $imageFile;
    public $image;
    private $_items;

    public static function find()
    {
        return new TypeQuery(get_called_class(), ['type' => self::TYPE]);
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                [
                    'class' => UploadedFileBehavior::className(),
                    'subdirectoryName' => 'home',
                    'attributes' => ['image' => ['fileAttribute' => 'imageFile']],
                    'resizeType' => UploadedFileBehavior::RESIZE_NONE
                ],
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $rules = parent::rules();

        return array_merge($rules, [
            [['image'], 'string', 'max' => 255],
            [['imageFile'], 'file', 'extensions' => 'jpg, gif, png'],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'image' => Yii::t('backend', 'Image'),
            'imageFile' => Yii::t('backend', 'Image')
        ];
    }

    public function getData()
    {
        return [
            'image' => $this->image
        ];
    }

    public function getItems()
    {
        if ($this->_items === null) {
            $this->_items = Therapist::find()
                ->where(['publish_status' => Therapist::STATUS_PUBLISHED])
                ->andWhere(['not', ['business_card_url' => null]])
                ->andWhere(['not', ['business_card_url' => '']])
                ->all();

            foreach ($this->_items as $item) {
                $item->business_card_url = str_replace('?autoplay=1', '', $item->business_card_url);
            }
        }
        return $this->_items;
    }

    public function beforeDelete()
    {
        $this->deleteMediaFile('image');
        return parent::beforeDelete();
    }
}