<?php

namespace common\models;

use Yii;
use common\components\UploadedFileBehavior;

class HomeTextSlide extends HomeSlide
{
    const TYPE = 'text';
    
    const TEMPLATE_LEFT = 'left';
    const TEMPLATE_RIGHT = 'right';
    const TEMPLATE_OVER = 'over';
    
    public $imageFile;
    public $image;
    public $template;
    
    public static function find()
    {
        return new TypeQuery(get_called_class(), ['type' => self::TYPE]);
    }
    
    /**
     * @inheritdoc
     */
    public function behaviors() {
        return array_merge(
            parent::behaviors(),
            [
                [
                    'class' => UploadedFileBehavior::className(),
                    'subdirectoryName' => 'home',
                    'attributes' => ['image' => ['fileAttribute' => 'imageFile']],
                    'resizeType' => UploadedFileBehavior::RESIZE_NONE
                ],
            ]
        );
    }
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        $rules = parent::rules();
        return array_merge($rules, [
            [['image'], 'string', 'max' => 255],
            [['imageFile'], 'file', 'extensions'=>'jpg, gif, png'],
            ['template', 'in', 'range' => [self::TEMPLATE_LEFT, self::TEMPLATE_RIGHT, self::TEMPLATE_OVER]],
        ]);
    }
    
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'image' => Yii::t('backend', 'Image'),
            'imageFile' => Yii::t('backend', 'Image')
        ];
    }
  
    public function getData(){
        return [
            'image' => $this->image,
            'template' => $this->template
        ];
    }
    
    public function beforeDelete() {
        $this->deleteMediaFile('image');
        return parent::beforeDelete();
    }
    
    public function getTemplates(){
        return [
            self::TEMPLATE_LEFT => Yii::t('backend', 'Left text'),
            self::TEMPLATE_RIGHT => Yii::t('backend', 'Right text'),
            self::TEMPLATE_OVER => Yii::t('backend', 'Text over image'),
        ];
    }
}