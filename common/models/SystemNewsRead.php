<?php

namespace common\models;

use common\components\UploadedFileBehavior;
use Yii;
use yii\caching\TagDependency;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;

/**
 * This is the model class for table "system_news_read".
 *
 * @property integer $id
 * @property integer $system_news_id
 * @property integer $user_id
 */
class SystemNewsRead extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'system_news_read';
    }
}
