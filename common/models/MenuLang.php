<?php

namespace common\models;

use Yii;
use common\models\Language;

/**
 * This is the model class for table "menu_lang".
 *
 * @property integer $id
 * @property integer $menu_id
 * @property string $language
 * @property string $title
 * @property string $description
 * @property Menu $menu
 */
class MenuLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'menu_lang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['language', 'title'], 'required'],
            ['language', 'unique', 'targetAttribute' => ['language', 'menu_id'], 'message' => Yii::t('backend', 'Translation already exists'),],
            [['menu_id'], 'integer'],
            [['language'], 'string', 'max' => 6],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('main', 'ID'),
            'menu_id' => Yii::t('main', 'Menu'),
            'language' => Yii::t('main', 'Language'),
            'title' => Yii::t('main', 'Title'),
        ];
    }
    
    public function getRelatedModel()
    {        
        return $this->hasOne(Menu::className(), ['id' => 'menu_id']);
    }
    
    public function getLanguageModel()
    {        
        return $this->hasOne(Language::className(), ['local' => 'language']);
    }
    
    public function getLanguageName()
    {        
        return $this->languageModel->name;
    }
    
    public function afterSave($insert, $changedAttributes) {
        parent::afterSave($insert, $changedAttributes);
        
        if($insert && $this->language == Yii::$app->sourceLanguage){
            $languages = Language::find()->where('local != :local', [':local' => Yii::$app->sourceLanguage])->all();

            foreach ($languages as $language) {
                $langTranslation = new self();
                $langTranslation->attributes = $this->attributes;
                $langTranslation->language = $language->local;
                $langTranslation->save(false);
            }
        }
    }
}
