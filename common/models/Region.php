<?php

namespace common\models;

use Yii;
use himiklab\sortablegrid\SortableGridBehavior;

/**
 * This is the model class for table "region".
 *
 * @property integer $id
 * @property string $code
 * @property string $name
 */
class Region extends \yii\db\ActiveRecord
{
    public $name;
    public $translatedAttributes = [
        'name' => 'string',
    ];
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'region';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['number'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'number' => Yii::t('backend', 'Number'),
            'name' => Yii::t('backend', 'Name'),
        ];
    }
    
    public function behaviors() {
        return [
            'sort' => [
                'class' => SortableGridBehavior::className(),
                'sortableAttribute' => 'number'
            ],
        ];
    }
    
    public function getTranslations() {
        return $this->hasMany(RegionLang::className(), ['region_id' => 'id'])->indexBy('language');
    }

    public function getTranslation() {
        return $this->hasOne(RegionLang::className(), ['region_id' => 'id'])->where('language = :language', [':language' => Yii::$app->language]);
    }
    
    public function getDefaultTranslation() {
        return $this->hasOne(RegionLang::className(), ['region_id' => 'id'])->where('language = :language', [':language' => Yii::$app->sourceLanguage]);
    }
    
    public function getCountries() {
        return $this->hasMany(Country::className(), ['region_id' => 'id']);
    }

    public function afterFind() {
        $translation = ($this->translation !== null) ? $this->translation : $this->defaultTranslation;
        
        foreach ($this->translatedAttributes as $attribute => $type) {
                $this->{$attribute} = $translation->{$attribute};
        }
            
        parent::afterFind();
    }
}
