<?php

namespace common\models;

use common\components\payments\WayForPay;
use Yii;
use yii\db\ActiveRecord;
use yii\helpers\Json;

/**
 * This is the model class for table "transaction".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $therapist_id
 * @property integer $payment_id
 * @property integer $appointment_id
 * @property float $amount
 * @property string $currency
 * @property float $fee
 * @property float $payment_fee
 * @property string $converted_amounts
 * @property string $reason
 * @property integer $settled
 * @property string $type
 * @property string $status
 * @property boolean $status_update_informed
 * @property integer $appointmentAmount
 * @property float $ratio
 * @property integer $created
 */
class Transaction extends ActiveRecord
{
    const TYPE_DEBIT = 'debit';
    const TYPE_CREDIT = 'credit';

    const STATUS_OPEN = 'open';
    const STATUS_PENDING = 'pending';
    const STATUS_SUCCESS = 'success';
    const STATUS_CANCELED = 'canceled';

    const REASON_PAY = 'pay';

    const STATUS_SETTLED = 1;
    const STATUS_NOT_SETTLED = 0;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'transaction';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'therapist_id', 'created', 'settled', 'appointmentAmount'], 'integer'],
            [['user_id', 'therapist_id', 'amount'], 'required'],
            [['amount', 'fee', 'payment_fee', 'ratio'], 'number'],
            [['reason', 'payment_id', 'currency', 'status'], 'string'],
            [['appointment_id'], 'integer'],
            [['appointmentAmount'], 'default', 'value' => 1],
            [['status_update_informed'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('main', 'ID'),
            'payment_id' => Yii::t('main', 'Payment ID'),
            'user_id' => Yii::t('main', 'User'),
            'therapist_id' => Yii::t('main', 'Therapist'),
            'amount' => Yii::t('main', 'Amount'),
            'currency' => Yii::t('main', 'Currency'),
            'fee' => Yii::t('main', 'Fee'),
            'payment_fee' => Yii::t('main', 'Payment fee'),
            'reason' => Yii::t('main', 'Reason'),
            'settled' => Yii::t('main', 'Settled'),
            'created' => Yii::t('main', 'Created'),
            'appointment_id' => Yii::t('main', 'Appointment'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created'],
                ],
            ],
        ];
    }

    /**
     * @return array
     */
    public function getStatuses()
    {
        return [
            self::STATUS_OPEN => Yii::t('main', 'New'),
            self::STATUS_PENDING => Yii::t('main', 'Pending'),
            self::STATUS_SUCCESS => Yii::t('main', 'Success'),
            self::STATUS_CANCELED => Yii::t('main', 'Canceled'),
        ];
    }

    /**
     * @return mixed
     */
    public function getStatusName()
    {
        return isset($this->getStatuses()[$this->status])
            ? $this->getStatuses()[$this->status]
            : '-';
    }

    /**
     * @param $status
     */
    public function setStatus($status)
    {
        $prevStatus = $this->status;
        $this->status_update_informed = false;
        $this->status = $status;
        $this->save(false);

        if (!$this->appointment) {
            return;
        }

        switch ($status) {
            case self::STATUS_SUCCESS:
                if ($prevStatus != self::STATUS_SUCCESS) {
                    PaidAppointments::add($this->appointment->user_id, $this->appointmentAmount - 1);
                }
                $this->appointment->setPaymentStatus(Appointment::PAID);
                break;
            case self::STATUS_PENDING:
                $this->appointment->setPaymentStatus(Appointment::PENDING);
                break;
            case self::STATUS_CANCELED:
                if ($prevStatus != self::STATUS_CANCELED) {
                    PaidAppointments::decrease($this->appointment->user_id, $this->appointmentAmount - 1);
                }
                $this->appointment->setPaymentStatus(Appointment::NOT_PAID);
                break;
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfile()
    {
        return $this->hasOne(Profile::className(), ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTherapist()
    {
        return $this->hasOne(Profile::className(), ['user_id' => 'therapist_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTherapistLang()
    {
        return $this->hasOne(TherapistLang::className(), ['user_id' => 'therapist_id'], ['language' => Yii::$app->language]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAppointment()
    {
        return $this->hasOne(Appointment::className(), ['id' => 'appointment_id']);
    }

    /**
     * @return array
     */
    public function getReasons()
    {
        return [
            self::REASON_PAY => Yii::t('main', 'Client payment'),
        ];
    }

    /**
     * @return mixed|string
     */
    public function getReasonName()
    {
        return isset($this->getReasons()[$this->reason]) ? $this->getReasons()[$this->reason] : '';
    }

    /**
     * @return float
     */
    public function getFullAmount()
    {
        return $this->amount + $this->fee + $this->payment_fee;
    }

    /**
     * Convert amount
     * @param $amount
     * @param int $mode
     * @return float
     */
    public function convertAmount($amount, $mode = PHP_ROUND_HALF_UP)
    {
        return round($amount * $this->ratio, 2, $mode);
    }

    /**
     * Settle transaction
     * @return array
     */
    public function settle()
    {
        if ($this->settled == self::STATUS_SETTLED) {
            return [
                'status' => false,
                'error' => Yii::t('main', 'Already settled transaction')
            ];
        }

        $error = '';
        $partner = Partner::findOne(['user_id' => $this->therapist_id]);

        if (!$partner || $partner->expired < time()) {
            $error = Yii::t('main', 'Wrong partner information or expired billing account');
        } else {
            $payment = new WayForPay();
            $convertedAmounts = json_decode($this->converted_amounts, true);

            //test_merch_p2p
            $result = $payment->request('settleRequest', [
                'transactionType' => 'SETTLE',
                'orderReference' => $this->payment_id,
                'amount' => round($convertedAmounts['amount'] + $convertedAmounts['fee'] + $convertedAmounts['payment_fee'], 2),
                'currency' => WayForPay::TRANSACTION_CURRENCY,
                'apiVersion' => Yii::$app->settingManager->get('wayforpayApiVersion'),
                'partnerCode' => [
                    Yii::$app->settingManager->get('wayforpayMerchantAccount'),
                    Yii::$app->settingManager->get('wayforpayMerchantAccount') . ':' . $partner->partner_code
                ],
                'partnerPrice' => [
                    $convertedAmounts['fee'],
                    $convertedAmounts['amount'],
                ]
            ]);

            if (isset($result['status'], $result['response']) && $result['status']) {
                if ($payment->isPaymentValid($result['response'], 'settleResponse') === true) {
                    $this->settled = self::STATUS_SETTLED;
                    $this->save(false);
                } else {
                    $error = Yii::t('main', 'Wrong merchant or merchant signature');
                }
            } else {
                $error = isset($result['error']) ? $result['error'] : '';
            }
        }

        if ($error) {
            $event = new Event([
                'type' => Event::TYPE_PAYMENT_ERROR,
                'text' => 'payment',
                'importance' => Event::IMPORTANCE_HIGH,
                'data' => Json::encode([
                    'transaction_id' => $this->id,
                    'error' => $error,
                ])
            ]);
            $event->save(false);

            return ['status' => false, 'error' => $error];
        }

        return ['status' => true];
    }
}
