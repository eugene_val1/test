<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "vote".
 *
 * @property integer $user_id
 * @property string $target
 * @property integer $target_id
 * @property integer $value
 *
 * @property User $user
 */
class Vote extends \yii\db\ActiveRecord
{
    const VOTE_UP = 1;
    const VOTE_DOWN = -1;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vote';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'target', 'target_id', 'value'], 'required'],
            [['user_id', 'target_id', 'value'], 'integer'],
            [['target'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => Yii::t('main', 'User ID'),
            'target' => Yii::t('main', 'Target'),
            'target_id' => Yii::t('main', 'Target ID'),
            'value' => Yii::t('main', 'Value'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
