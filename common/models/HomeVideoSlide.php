<?php

namespace common\models;

use Yii;
use common\components\UploadedFileBehavior;

/**
 * Class HomeVideoSlide
 * @package common\models
 */
class HomeVideoSlide extends HomeSlide
{
    const TYPE = 'video';
    public $imageFile;
    public $image;
    public $video;

    /**
     * @return TypeQuery
     */
    public static function find()
    {
        return new TypeQuery(get_called_class(), ['type' => self::TYPE]);
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                [
                    'class' => UploadedFileBehavior::className(),
                    'subdirectoryName' => 'home',
                    'attributes' => ['image' => ['fileAttribute' => 'imageFile']],
                    'resizeType' => UploadedFileBehavior::RESIZE_NONE
                ],
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $rules = parent::rules();

        return array_merge($rules, [
            [['image', 'video'], 'string', 'max' => 255],
            [['imageFile'], 'file', 'extensions' => 'jpg, gif, png'],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'image' => Yii::t('backend', 'Cover image'),
            'imageFile' => Yii::t('backend', 'Cover image'),
            'video' => Yii::t('backend', 'Video embed url')
        ];
    }

    /**
     * @return array
     */
    public function getData()
    {
        return [
            'image' => $this->image,
            'video' => $this->video
        ];
    }

    /**
     * @return bool
     */
    public function beforeDelete()
    {
        $this->deleteMediaFile('image');
        return parent::beforeDelete();
    }
}