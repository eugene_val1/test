<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "menu".
 *
 * @property integer $id
 * @property string $position
 * @property string $title
 * @property string $description
 */
class Menu extends \yii\db\ActiveRecord
{
    public $title;
    public $translatedAttributes = [
        'title' => 'string',
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'menu';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['position'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'position' => Yii::t('backend', 'Position'),
            'title' => Yii::t('main', 'Title'),
            'description' => Yii::t('main', 'Description'),
        ];
    }

    public function getLinks()
    {
        return $this->hasMany(Link::className(), ['menu_id' => 'id'])->orderBy('number');
    }

    public function getTranslations()
    {
        return $this->hasMany(MenuLang::className(), ['menu_id' => 'id'])->indexBy('language');
    }

    public function getTranslation()
    {
        return $this->hasOne(MenuLang::className(), ['menu_id' => 'id'])->where('language = :language', [':language' => Yii::$app->language]);
    }

    public function getDefaultTranslation()
    {
        return $this->hasOne(MenuLang::className(), ['menu_id' => 'id'])->where('language = :language', [':language' => Yii::$app->sourceLanguage]);
    }

    /**
     * After find
     */
    public function afterFind()
    {
        $translation = ($this->translation !== null) ? $this->translation : $this->defaultTranslation;

        foreach ($this->translatedAttributes as $attribute => $type) {
            $this->{$attribute} = $translation->{$attribute};
        }

        parent::afterFind();
    }

    /**
     * @param $position
     * @return array|mixed
     */
    public static function getLinksByPosition($position)
    {
        $menu = self::find()
            ->with([
                'links' => function ($query) {
                    return $query->joinWith('translation');
                }
            ])->where(['position' => $position])->one();

        return $menu ? $menu->links : [];
    }

    /**
     * Render button in the main header menu
     * @param bool $mobile
     * @return string
     */
    public static function renderRoomHandlerButton($mobile = false)
    {
        if (Yii::$app->user->isGuest || Yii::$app->user->identity->role == User::ROLE_THERAPIST) {
            return '';
        }

        //1. Check if current user has not paid appointment
        $nextAppointment = Appointment::find()->where([
                'user_id' => Yii::$app->user->getId(),
                'status' => [Appointment::STATUS_NEW, Appointment::STATUS_RESCHEDULE]
            ])
            ->orderBy('start DESC')
            ->one();

        if (!empty($nextAppointment) && $nextAppointment->payment_status == Appointment::NOT_PAID) {
            if ($mobile) {
                return '<button type="button" class="navbar-toggle card-btn" id="mobile-pay-menu-btn" onclick="window.location.href=\'/account\'" data-toggle="collapse">
                    
                </button>';
            } else {
                return '<a href="/account" style="color: #fff; margin: 0 10px 0 0;" class="page-btn small-page-btn header-menu-btn">' . Yii::t('main', 'Pay session') . '</a>';
            }
        }

        //2. Check if current user has paid appointment
        $appointments = Appointment::find()->where([
                'user_id' => Yii::$app->user->getId(),
                'payment_status' => Appointment::PAID,
                'status' => [Appointment::STATUS_NEW, Appointment::STATUS_RESCHEDULE]
            ])
            ->all();

        //only 1 paid appointment
        if (count($appointments) == 1) {
            $roomLink = '/appointment/video?id=' . $appointments[0]->id;
            if ($mobile) {
                return '<button type="button" class="navbar-toggle video-btn" id="mobile-pay-menu-btn" onclick="window.location.href=\'' . $roomLink . '\'" data-toggle="collapse">
                        
                    </button>';
            } else {
                return '<a href="' . $roomLink . '" style="color: #fff; margin: 0 10px 0 0;" class="page-btn small-page-btn header-menu-btn">' . Yii::t('main', 'Online room') . '</a>';
            }
        //several paid appointments => just show profile
        } else if (count($appointments) > 1) {
            if ($mobile) {
                return '<button type="button" class="navbar-toggle video-btn" id="mobile-pay-menu-btn" onclick="window.location.href=\'/account\'" data-toggle="collapse">
                            
                        </button>';
            } else {
                return '<a href="/account" style="color: #fff; margin: 0 10px 0 0;" class="page-btn small-page-btn header-menu-btn">' . Yii::t('main', 'Online room') . '</a>';
            }
        }

        return '';
    }
}
