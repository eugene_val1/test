<?php

namespace common\models;

use common\models\Messages\ConversationMessage;
use Yii;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use DateTime;
use DateTimeZone;
use \Firebase\JWT\JWT;
use yii\behaviors\TimestampBehavior;

/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $password_hash
 * @property string $token
 * @property string $email
 * @property string $auth_key
 * @property integer $role
 * @property integer $status
 * @property string $timezone
 * @property integer $created
 * @property integer $updated
 * @property integer $country_id
 * @property string $password write-only password
 *
 * @property NotificationSettings $notificationSettings
 * @property Profile|Therapist $profile
 */
class User extends ActiveRecord implements IdentityInterface
{
    const STATUS_DELETED = 0;
    const STATUS_CREATED = 5;
    const STATUS_ACTIVE = 10;
    const ROLE_USER = 1;
    const ROLE_THERAPIST = 5;
    const ROLE_EDITOR = 7;
    const ROLE_ADMIN = 10;
    const DELETED_PREFIX = 'DELETED-rsBGh-';

    public $user_password;
    public $cptch;
    public $agree_mailing = false;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email', 'role'], 'required'],
            [['email'], 'unique', 'on' => [
                'create',
                'create-admin',
                'update-email',
            ]],
            ['email', 'email'],
            [['email'], 'filter', 'filter' => 'trim'],
            ['username', 'string', 'max' => 255],
            [['country_id'], 'integer'],
            [['user_password'], 'required', 'on' => ['create', 'create-admin']],
            [['user_password', 'timezone', 'country_id'], 'safe'],
            ['status', 'default', 'value' => self::STATUS_CREATED],
            ['status', 'in', 'range' => [
                    self::STATUS_ACTIVE,
                    self::STATUS_CREATED,
                    self::STATUS_DELETED,
            ]],
            ['role', 'default', 'value' => self::ROLE_USER],
            ['role', 'in', 'range' => [
                self::ROLE_USER,
                self::ROLE_ADMIN,
                self::ROLE_THERAPIST,
                self::ROLE_EDITOR,
            ]],
            [['agree_mailing'], 'boolean'],
            //['reCaptcha', ReCaptchaValidator::className(), 'on' => ['create', 'create-admin']],
            ['cptch', 'required', 'on' => ['create']],
            ['cptch', 'captcha', 'on' => ['create']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'role' => Yii::t('main', 'Role'),
            'username' => Yii::t('main', 'Username'),
            'timezone' => Yii::t('main', 'Timezone'),
            'password' => Yii::t('main', 'Password'),
            'user_password' => Yii::t('main', 'Password'),
            'country_id' => Yii::t('backend', 'Country'),
            'cptch' => Yii::t('main', 'Security code'),
        ];
    }

    /**
     * @param ActiveRecord $condition
     * @return \yii\db\ActiveQueryInterface
     */
    /*public static function findByCondition($condition)
    {
        return parent::findByCondition($condition)->andWhere(['status' => self::STATUS_ACTIVE]);
    }*/

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne([
            'id' => $id,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        try {
            $decoded = JWT::decode($token, Yii::$app->params['apiSecret'], array('HS256'));
            return static::findOne([
                'id' => $decoded->data->id,
                'status' => self::STATUS_ACTIVE,
            ]);
        } catch (\Exception $e) {
            return null;
        }
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne([
            'username' => $username,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds user by username
     *
     * @param string $email
     * @return static|null
     */
    public static function findByEmail($email)
    {
        /**
         * @var User $model
         */
        $model = static::find()
            ->where([
                'email' => $email,
                'status' => self::STATUS_ACTIVE,
            ])
            ->orderBy('created DESC')
            ->one();

        return $model;
    }

    /**
     * Finds user by email/username
     *
     * @param string $login
     *
     * @return null|static
     */
    public static function findByEmailOrUsername($login)
    {
        /**
         * @var User $model
         */
        $model = static::find()
            ->orFilterWhere([
                'email' => $login,
                'status' => self::STATUS_ACTIVE,
            ])
            ->orFilterWhere([
                'username' => $login,
                'status' => self::STATUS_ACTIVE,
            ])
            ->orderBy('created DESC')
            ->one();

        return $model;
    }

    /**
     * Finds user by token
     *
     * @param string $token token
     * @return static|null
     */
    public static function findByToken($token)
    {
        if (!static::isTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return boolean
     */
    public static function isTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $expire = Yii::$app->params['user.tokenExpire'];
        $parts = explode('_', $token);
        $timestamp = (int)end($parts);

        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        if (empty($this->password_hash)) {
            return false;
        }

        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new token
     */
    public function generateToken()
    {
        $this->token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removeToken()
    {
        $this->token = null;
    }

    /**
     * @return bool
     */
    public function isTherapist()
    {
        return (int)$this->role === self::ROLE_THERAPIST;
    }

    /**
     * @return array
     */
    public function getRoles()
    {
        return [
            self::ROLE_USER => Yii::t('main', 'User'),
            self::ROLE_EDITOR => Yii::t('main', 'Editor'),
            self::ROLE_ADMIN => Yii::t('main', 'Administrator'),
            self::ROLE_THERAPIST => Yii::t('main', 'Therapist')
        ];
    }

    /**
     * @return array
     */
    public function getMainRoles()
    {
        return [
            self::ROLE_USER => Yii::t('main', 'User'),
            self::ROLE_EDITOR => Yii::t('main', 'Editor'),
            self::ROLE_ADMIN => Yii::t('main', 'Administrator')
        ];
    }

    /**
     * @return array
     */
    public function getStatuses()
    {
        return [
            self::STATUS_DELETED => Yii::t('main', 'Deleted'),
            self::STATUS_CREATED => Yii::t('main', 'Created'),
            self::STATUS_ACTIVE => Yii::t('main', 'Active'),
        ];
    }

    /**
     * Check if notification should be sent to user
     * @param $type
     * @param null $status
     * @param null $target
     * @return int
     */
    public function isNoticeAllowed($type, $status = null, $target = null)
    {
        /* @var $settings NotificationSettings */
        $settings = $this->notificationSettings;

        if ($type === NotificationSettings::TYPE_MESSAGES) {
            return $settings->messages;
        }

        if ($type === NotificationSettings::TYPE_APPOINTMENT_REMINDER) {
            return $settings->appointment_reminder;
        }

        if ($type === NotificationSettings::TYPE_APPOINTMENT) {
            if ($target == AppointmentEvent::TARGET_PAYMENT) {
                switch ($status) {
                    case Appointment::PAID:
                        return $settings->appointment_paid;
                    case Appointment::NOT_PAID:
                        return $settings->appointment_not_paid;
                }
            } else {
                switch ($status) {
                    case Appointment::STATUS_NEW:
                        return $settings->appointment_new;
                    case Appointment::STATUS_RESCHEDULE:
                        return $settings->appointment_rescheduled;
                    case Appointment::STATUS_COMPLETED:
                        return $settings->appointment_completed;
                    case Appointment::STATUS_CANCELED:
                        return $settings->appointment_canceled;
                    case Appointment::STATUS_MISSED:
                        return $settings->appointment_missed;
                }
            }
        }

        return false;
    }

    /**
     * @return mixed
     */
    public function getRoleName()
    {
        return $this->getRoles()[$this->role];
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return trim($this->profile->name);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProviders()
    {
        return $this->hasMany(AuthProvider::className(), ['user_id' => 'id'])->indexBy('name');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBots()
    {
        return $this->hasMany(UserBots::className(), ['user_id' => 'id']);
    }

    /**
     * @param $provider
     * @return bool
     */
    public function hasConnected($provider)
    {
        return isset($this->providers[$provider]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfile()
    {
        $profileClass = ($this->role === self::ROLE_THERAPIST)
            ? Therapist::className()
            : Profile::className();

        return $this->hasOne($profileClass, ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Client::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNotificationSettings()
    {
        return $this->hasOne(NotificationSettings::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReadSystemNews()
    {
        return $this->hasMany(SystemNewsRead::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaidAppointments()
    {
        return $this->hasOne(PaidAppointments::className(), ['user_id' => 'id']);
    }

    /**
     * After save
     * @param bool $insert
     * @param array $changedAttributes
     * @return bool
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if ($insert) {
            //create default notification settings
            Yii::$app->db
                ->createCommand()
                ->batchInsert('notification_settings', ['user_id'], [[$this->id]])
                ->execute();
        }

        return true;
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        $usernameIsEmail = $this->getOldAttribute('email') === $this->username;

        if ($insert || $usernameIsEmail) {
            $this->username = $this->email;
        }

        return parent::beforeSave($insert);
    }

    /**
     * Before delete
     * @return bool
     */
    public function beforeDelete()
    {
        $adminsCount = User::find()
            ->where(['role' => self::ROLE_ADMIN])
            ->count();

        if ($this->role === self::ROLE_ADMIN && $adminsCount < 2) {
            return false;
        }

        //delete dependencies
        Profile::deleteAll(['user_id' => $this->id]);
        Article::deleteAll('user_id = :user_id', [':user_id' => $this->id]);
        AuthProvider::deleteAll('user_id = :user_id', [':user_id' => $this->id]);
        NotificationSettings::deleteAll(['user_id' => $this->id]);
        Client::deleteAll(['user_id' => $this->id]);
        Appointment::deleteAll(['user_id' => $this->id]);
        Conversation::deleteAll([
            'or',
            ['sender_id' => $this->id],
            ['receiver_id' => $this->id]
        ]);
        ConversationMessage::deleteAll([
            'or',
            ['user_id' => $this->id],
            ['receiver_id' => $this->id]
        ]);

        return parent::beforeDelete();
    }

    /**
     * @param $userTimezone
     * @return string
     */
    public function getTimezoneOffset($userTimezone = null)
    {
        $now = new DateTime();
        $now->setTimezone(new DateTimeZone($this->timezone));
        $offset = $now->getOffset();

        if (isset($userTimezone)) {
            $now->setTimezone(new DateTimeZone($userTimezone));
            $currentUserOffset = $now->getOffset();
            return $this->formatOffset($offset - $currentUserOffset);
        }

        return '(GMT' . $this->formatOffset($offset) . ')';
    }

    /**
     * @param $offset
     * @return string
     */
    public function formatOffset($offset)
    {
        $hours = (int)$offset / 3600;
        $minutes = abs((int)$offset % 3600 / 60);

        return $offset ? sprintf('%+03d:%02d', $hours, $minutes) : '00:00';
    }

    /**
     * Is user deleted by itself
     * @return bool
     */
    public function isDeleted()
    {
        return $this->status == self::STATUS_DELETED;
    }
}