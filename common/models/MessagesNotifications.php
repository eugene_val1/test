<?php

namespace common\models;

use Yii;
use common\models\ContactTheme;
use yii\helpers\Json;

/**
 * This is the model class for table "contact".
 *
 * @property integer $id
 * @property integer $message_id
 * @property integer $created
 */
class MessagesNotifications extends \yii\db\ActiveRecord
{
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'messages_notifications';
    }
    
        /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    \yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['created'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['message_id'], 'required'],
        ];
    }
}
