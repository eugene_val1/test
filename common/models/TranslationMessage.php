<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "translation_message".
 *
 * @property integer $id
 * @property string $language
 * @property string $translation_message
 *
 * @property SourceTranslationMessage $id0
 */
class TranslationMessage extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'translation_message';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'language'], 'required'],
            [['translation'], 'string'],
            [['language'], 'string', 'max' => 16]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'language' => Yii::t('backend', 'Language'),
            'translation' => Yii::t('backend', 'Translation'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKey()
    {
        return $this->hasOne(SourceTranslationMessage::className(), ['id' => 'id']);
    }
}
