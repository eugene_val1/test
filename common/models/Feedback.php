<?php

namespace common\models;

use Yii;
use yii\helpers\Json;

/**
 * This is the model class for table "feedback".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $text
 * @property integer $published
 * @property integer $created
 */
class Feedback extends \yii\db\ActiveRecord
{
    const STATUS_PUBLISHED = 1;
    const STATUS_DRAFT = 0;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'feedback';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'text'], 'required'],
            [['user_id', 'published', 'created'], 'integer'],
            [['text'], 'string', 'max' => 256]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('main', 'ID'),
            'user_id' => Yii::t('main', 'User'),
            'user_name' => Yii::t('main', 'User name'),
            'text' => Yii::t('main', 'Text'),
            'published' => Yii::t('main', 'Published'),
            'created' => Yii::t('main', 'Created'),
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    \yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['created'],
                ],
            ],
        ];
    }
    
    public function getUser() {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
    
    public function getProfile() {
        return $this->hasOne(Profile::className(), ['user_id' => 'user_id']);
    }
    
    public function getTherapistLang() {
        return $this->hasOne(TherapistLang::className(), ['user_id' => 'user_id'], ['language' => Yii::$app->language]);
    }
    
    public function afterSave($insert, $changedAttributes) {
        if(!Yii::$app->user->isGuest && $this->user_id == Yii::$app->user->id){
            $event = new NeedModerationEvent([
                'importance' => Event::IMPORTANCE_NORMAL,
                'text' => NeedModerationEvent::TARGET_FEEDBACK,
                'data' => Json::encode([
                    'feedback_id' => $this->id,
                ])
            ]);
            $event->save(false);
        }
        return parent::afterSave($insert, $changedAttributes);
    }
    
    public function getAuthorName(){
        $name = '';
        if($this->profile){
            $name = ($this->profile->type == Therapist::TYPE  && $this->therapistLang) ? $this->therapistLang->name : $this->profile->name;
        }
        return $name;
    }
}
