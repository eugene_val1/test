<?php

namespace common\models;

use Yii;
use common\models\Language;

/**
 * This is the model class for table "setting_lang".
 *
 * @property integer $id
 * @property integer $setting_id
 * @property string $language
 * @property string $name
 *
 * @property Setting $setting
 */
class SettingLang extends \yii\db\ActiveRecord
{
    public $title;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'setting_lang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['language'], 'required'],
            ['language', 'unique', 'targetAttribute' => ['language', 'setting_id'], 'message' => Yii::t('backend', 'Translation already exists'),],
            [['setting_id'], 'integer'],
            [['language'], 'string', 'max' => 6],
            [['content'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('main', 'ID'),
            'setting_id' => Yii::t('backend', 'Setting'),
            'language' => Yii::t('main', 'Language'),
            'content' => Yii::t('backend', 'Content'),
        ];
    }
    
    public function getRelatedModel()
    {        
        return $this->hasOne(Setting::className(), ['id' => 'setting_id']);
    }
    
    public function getLanguageModel()
    {        
        return $this->hasOne(Language::className(), ['local' => 'language']);
    }
    
    public function getLanguageName()
    {        
        return $this->languageModel->name;
    }
}
