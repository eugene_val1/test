<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use yii\helpers\Json;

/**
 * This is the model class for table "event".
 *
 * @property integer $id
 * @property integer $type
 * @property integer $target
 * @property integer $importance
 * @property string $text
 * @property string $data
 * @property integer $created
 * @property integer $informed
 */
class Event extends ActiveRecord
{
    const IMPORTANCE_LOW = 1;
    const IMPORTANCE_NORMAL = 5;
    const IMPORTANCE_HIGH = 10;

    const TYPE_COMPLETE_THERAPY = 1;
    const TYPE_NEED_MODERATION = 2;
    const TYPE_PAYMENT_ERROR = 3;
    const TYPE_APPOINTMENT = 4;
    const TYPE_CLIENT = 5;
    const TYPE_CHANGE_TIMEZONE = 6;

    const STATUS_INFORMED = 1;
    const STATUS_NOT_INFORMED = 0;

    const RECEIVER_USER = 1;
    const RECEIVER_ADMIN = 0;
    
    protected $_data;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'event';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'importance', 'data'], 'required'],
            [['type', 'importance', 'created', 'informed', 'target'], 'integer'],
            [['text', 'data'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('main', 'ID'),
            'type' => Yii::t('main', 'Type'),
            'target' => Yii::t('main', 'Target'),
            'importance' => Yii::t('main', 'Importance'),
            'text' => Yii::t('main', 'Text'),
            'data' => Yii::t('main', 'Data'),
            'created' => Yii::t('main', 'Created'),
            'informed' => Yii::t('main', 'Informed')
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created'],
                ],
            ],
        ];
    }

    /**
     * @param array $row
     * @return AppointmentEvent|ChangeTimezoneEvent|ClientEvent|CompleteTherapyEvent|Event|NeedModerationEvent|PaymentErrorEvent
     */
    public static function instantiate($row)
    {
        switch ($row['type']) {
            case self::TYPE_COMPLETE_THERAPY:
                return new CompleteTherapyEvent();
            case self::TYPE_NEED_MODERATION:
                return new NeedModerationEvent();
            case self::TYPE_PAYMENT_ERROR:
                return new PaymentErrorEvent();
            case self::TYPE_APPOINTMENT:
                return new AppointmentEvent();
            case self::TYPE_CLIENT:
                return new ClientEvent();
            case self::TYPE_CHANGE_TIMEZONE:
                return new ChangeTimezoneEvent();
            default:
                return new self;
        }
    }

    /**
     * After find
     */
    public function afterFind()
    {
        parent::afterFind();

        $this->_data = Json::decode($this->data);
    }

    /**
     * @return array
     */
    public function getTypes()
    {
        return [
            self::TYPE_COMPLETE_THERAPY => Yii::t('main', 'Completed therapy'),
            self::TYPE_NEED_MODERATION => Yii::t('main', 'Need moderation'),
            self::TYPE_PAYMENT_ERROR => Yii::t('main', 'Errors while payment settle'),
            self::TYPE_CLIENT => Yii::t('main', 'Client events'),
            self::TYPE_APPOINTMENT => Yii::t('main', 'Appointment events'),
            self::TYPE_CHANGE_TIMEZONE => Yii::t('main', 'Changed timezone events')
        ];
    }

    /**
     * @return mixed
     */
    public function getTypeName()
    {
        $types = $this->getTypes();
        return $types[$this->type];
    }

    /**
     * @return array
     */
    public function getImportanceLevels()
    {
        return [
            self::IMPORTANCE_LOW => Yii::t('main', 'Low'),
            self::IMPORTANCE_NORMAL => Yii::t('main', 'Normal'),
            self::IMPORTANCE_HIGH => Yii::t('main', 'High'),
        ];
    }

    /**
     * @return array
     */
    public function getImportanceIcon()
    {
        return [
            self::IMPORTANCE_LOW => '<span class="label label-success"><i class="fa fa-exclamation"></i><span>',
            self::IMPORTANCE_NORMAL => '<span class="label label-warning"><i class="fa fa-exclamation"></i><span>',
            self::IMPORTANCE_HIGH => '<span class="label label-danger"><i class="fa fa-exclamation"></i><span>',
        ];
    }

    /**
     * @return string
     */
    public function getAlias()
    {
        return 'event';
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->_data;
    }

    /**
     * Set informed
     */
    public function setInformed()
    {
        $this->informed = self::STATUS_INFORMED;
        $this->save(false);
    }
}
