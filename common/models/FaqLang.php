<?php

namespace common\models;

use Yii;
use common\models\Language;

/**
 * This is the model class for table "faq_lang".
 *
 * @property integer $id
 * @property integer $faq_id
 * @property string $language
 * @property string $title
 * @property Faq $faq
 */
class FaqLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'faq_lang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['language', 'question', 'answer'], 'required'],
            ['language', 'unique', 'targetAttribute' => ['language', 'faq_id'], 'message' => Yii::t('backend', 'Translation already exists'),],
            [['faq_id'], 'integer'],
            [['language'], 'string', 'max' => 6],
            [['question', 'answer'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('main', 'ID'),
            'faq_id' => Yii::t('main', 'Faq'),
            'language' => Yii::t('main', 'Language'),
            'question' => Yii::t('backend', 'Question'),
            'answer' => Yii::t('backend', 'Answer'),
        ];
    }
    
    public function getRelatedModel()
    {        
        return $this->hasOne(Faq::className(), ['id' => 'faq_id']);
    }
    
    public function getLanguageModel()
    {        
        return $this->hasOne(Language::className(), ['local' => 'language']);
    }
    
    public function getLanguageName()
    {        
        return $this->languageModel->name;
    }
    
    public function afterSave($insert, $changedAttributes) {
        parent::afterSave($insert, $changedAttributes);
        
        if($insert && $this->language == Yii::$app->sourceLanguage){
            $languages = Language::find()->where('local != :local', [':local' => Yii::$app->sourceLanguage])->all();

            foreach ($languages as $language) {
                $langTranslation = new self();
                $langTranslation->attributes = $this->attributes;
                $langTranslation->language = $language->local;
                $langTranslation->save(false);
            }
        }
    }
}
