<?php

namespace common\models;

use common\components\GeoService;
use common\components\UserManager;
use Yii;
use omnilight\sypexgeo\SypexGeo;

/**
 * This is the model class for table "timezone".
 *
 * @property integer $id
 * @property string $timezone_id
 * @property string $language
 * @property string $name
 */
class Timezone extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'timezone';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['language', 'name', 'timezone_id'], 'required'],
            ['language', 'unique', 'targetAttribute' => ['language', 'timezone_id'], 'message' => Yii::t('backend', 'Translation already exists'),],
            [['language'], 'string', 'max' => 6],
            [['name', 'timezone_id'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('main', 'ID'),
            'timezone_id' => Yii::t('backend', 'Timezone'),
            'language' => Yii::t('main', 'Language'),
            'name' => Yii::t('backend', 'Name'),
        ];
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if ($insert && $this->language == Yii::$app->sourceLanguage) {
            $languages = Language::find()
                ->where('local != :local', [':local' => Yii::$app->sourceLanguage])
                ->all();

            foreach ($languages as $language) {
                $timezone = new self();
                $timezone->attributes = $this->attributes;
                $timezone->language = $language->local;
                $timezone->save(false);
            }
        }
    }

    /**
     * @return mixed|string
     */
    public static function getCurrentByIP()
    {
        //1 - get from client side
        if (isset($_COOKIE['timezone']) && ($countryTimezone = self::findByTimezoneID($_COOKIE['timezone']))) {
            Yii::info([
                'type' => 'timezone',
                'userIp' => UserManager::getUserIp(),
                'source' => 'JS',
            ], 'geo');
            return $countryTimezone->timezone_id;
        }

        //2 - get from SypexGeo
        $sypexGeo = new SypexGeo([
            'database' => '@common/data/SxGeoCityMax.dat',
        ]);

        $geoip = $sypexGeo->getCityFull(UserManager::getUserIp());

        if ($regionTimezone = self::findByTimezoneID($geoip['region']['timezone'])) {
            Yii::info([
                'type' => 'timezone',
                'timezone' => $geoip['region']['timezone'],
                'userIp' => UserManager::getUserIp(),
                'source' => 'sypexGeo region',
            ], 'geo');
            return $regionTimezone->timezone_id;
        }

        if ($countryTimezone = self::findByTimezoneID($geoip['country']['timezone'])) {
            Yii::info([
                'type' => 'timezone',
                'timezone' => $geoip['country']['timezone'],
                'userIp' => UserManager::getUserIp(),
                'source' => 'sypexGeo country',
            ], 'geo');
            return $countryTimezone->timezone_id;
        }

        Yii::info([
            'type' => 'timezone',
            'userIp' => UserManager::getUserIp(),
            'sypexGeo' => $geoip,
        ], 'geo');

        //3 - get from 3d-party service
        $geoService = new GeoService(UserManager::getUserIp());
        $geoServiceTimezone = $geoService->getTimezone();
        if ($timezone = self::findByTimezoneID($geoService->getTimezone())) {
            Yii::info([
                'type' => 'timezone',
                'timezone' => $geoServiceTimezone,
                'userIp' => UserManager::getUserIp(),
                'source' => 'GeoService',
            ], 'geo');
            return $timezone->timezone_id;
        }

        Yii::info([
            'type' => 'timezone',
            'source' => 'UTC',
            'userIp' => UserManager::getUserIp(),
        ], 'geo');

        return 'UTC';
    }

    /**
     * @param $id
     * @return array|null|\yii\db\ActiveRecord
     */
    public static function findByTimezoneID($id)
    {
        return self::find()
            ->where('timezone_id = :timezone AND language = :language', [
                ':timezone' => $id,
                ':language' => Yii::$app->language
            ])
            ->one();
    }

    /**
     * @return bool
     */
    public function beforeDelete()
    {
        if ($this->timezone_id == Yii::$app->params['defaultTimezone']) {
            return false;
        }

        return parent::beforeDelete();
    }

    /**
     * @return string
     */
    public function getOffset()
    {
        $time = new \DateTime('now', new \DateTimeZone($this->timezone_id));
        return $time->format('P');
    }

    /**
     * Custom date formatter
     *
     * @param $timestamp
     * @param string $timezone
     * @param string $format
     * @return string
     */
    public static function convert($timestamp, $timezone = 'UTC', $format = 'd.m.Y H:i')
    {
        $dt = new \DateTime();
        $dt->setTimestamp($timestamp);
        $dt->setTimezone(new \DateTimeZone($timezone));

        return $dt->format($format);
    }

    /**
     * Custom timestamp converter
     *
     * @param $timestamp
     * @param string $timezone
     * @return string
     */
    public static function convertTimestamp($timestamp, $timezone = 'UTC')
    {
        $dt = new \DateTime();
        $dt->setTimestamp($timestamp);
        $dt->setTimezone(new \DateTimeZone($timezone));

        return $dt->getTimestamp();
    }
}
