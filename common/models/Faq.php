<?php

namespace common\models;

use Yii;
use himiklab\sortablegrid\SortableGridBehavior;

/**
 * This is the model class for table "faq".
 *
 * @property integer $id
 */
class Faq extends \yii\db\ActiveRecord
{
    const FEATURED = 1;
    
    public $question;
    public $answer;
    public $translatedAttributes = [
        'question' => 'string',
        'answer' => 'html',
    ];
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'faq';
    }
    
    public function behaviors() {
        return [
            'sort' => [
                'class' => SortableGridBehavior::className(),
                'sortableAttribute' => 'number'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['number', 'category_id', 'featured'], 'integer'],
            [['category_id'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'number' => Yii::t('main', 'Number'),
            'category_id' => Yii::t('backend', 'Category'),
            'featured' => Yii::t('backend', 'Featured'),
            'question' => Yii::t('backend', 'Question'),
            'answer' => Yii::t('backend', 'Answer'),
        ];
    }
    
    public function getTranslations() {
        return $this->hasMany(FaqLang::className(), ['faq_id' => 'id'])->indexBy('language');
    }

    public function getTranslation() {
        return $this->hasOne(FaqLang::className(), ['faq_id' => 'id'])->where('language = :language', [':language' => Yii::$app->language]);
    }
    
    public function getDefaultTranslation() {
        return $this->hasOne(FaqLang::className(), ['faq_id' => 'id'])->where('language = :language', [':language' => Yii::$app->sourceLanguage]);
    }
    
    public function getCategory() {
        return $this->hasOne(FaqCategoryLang::className(), ['faq_category_id' => 'category_id']);
    }

    public function afterFind() {
        $translation = ($this->translation !== null) ? $this->translation : $this->defaultTranslation;
        
        foreach ($this->translatedAttributes as $attribute => $type) {
                $this->{$attribute} = $translation->{$attribute};
        }
        
        parent::afterFind();
    }
}
