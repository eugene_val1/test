<?php

namespace common\models;

use yii\db\ActiveQuery;

/**
 * Class TypeQuery
 * @package common\models
 */
class TypeQuery extends ActiveQuery
{
    public $type;
    public $tableName;

    /**
     * @param \yii\db\QueryBuilder $builder
     * @return ActiveQuery
     */
    public function prepare($builder)
    {
        if ($this->type !== null) {
            $column = 'type';
            if(!empty($this->tableName)){
                $column = '`' . $this->tableName . '`.' . $column;
            }
            $this->andWhere([$column => $this->type]);
        }

        return parent::prepare($builder);
    }
}