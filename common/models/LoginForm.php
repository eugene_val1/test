<?php

namespace common\models;

use frontend\components\TooltipHelper;
use Yii;
use yii\base\Model;

/**
 * Login form
 */
class LoginForm extends Model
{
    const SESSION_DURATION = 2592000;

    public $username;
    public $password;
    public $rememberMe = true;

    private $_user = false;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'password'], 'required'],
            [['username', 'password'], 'trim'],
            ['username', 'string', 'max' => 255],
            ['rememberMe', 'boolean'],
            ['username', 'validateStatus'],
            ['password', 'validatePassword'],
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user && empty($user->password_hash)) {
                $this->addError($attribute, Yii::t('main', 'It seems that you have signed up on our platform using one of the social networks. Please, log in with the same account you used on sign up. If there are problems - {0}contact us{1}', [
                    '<a data-target="#modal" href="/contact">',
                    '</a>'
                ]));
            } elseif (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, Yii::t('main', 'Incorrect username or password.'));
            }
        }
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function validateStatus($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user || $user->status !== USER::STATUS_ACTIVE) {
                $this->addError($attribute, Yii::t('main', 'Account not activated or deleted'));
            }
        }
    }

    /**
     * Logs in a user using the provided username and password.
     *
     * @return boolean whether the user is logged in successfully
     */
    public function login()
    {
        if (!$this->validate()) {
            return false;
        }

        $user = $this->getUser();
        if (!$user) {
            return false;
        }

        return Yii::$app->user->login($user, self::SESSION_DURATION);
    }

    /**
     * Finds user by [[username]] or [[email]]
     *
     * @return User|null
     */
    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = User::findByEmailOrUsername($this->username);
        }

        return $this->_user;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'rememberMe' => Yii::t('main', 'Remember me'),
            'username' => Yii::t('main', 'Email or login'),
            'password' => Yii::t('main', 'Password'),
        ];
    }
}
