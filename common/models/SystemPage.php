<?php

namespace common\models;
use Yii;

class SystemPage extends Page
{
    const TYPE = 'system';   

    public static function find()
    {
        return new TypeQuery(get_called_class(), ['type' => self::TYPE]);
    }
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(
            parent::rules(), 
            [
                [['section'], 'required'],
            ]
        );
    }
    
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
            parent::attributeLabels(), 
            [
                'section' => Yii::t('backend', 'Section'),
            ]
        );
    }
    
    public function beforeSave($insert) {
        $this->type = self::TYPE;
        return true;
    }
    
    public function getUrl(){
        $url = '/';
        if($this->section !== 'home'){
            $url .= $this->section;
        }
        return $url;
    }
}