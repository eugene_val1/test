<?php

namespace common\models;

use Yii;
use common\models\Language;

/**
 * This is the model class for table "country_lang".
 *
 * @property integer $id
 * @property integer $country_id
 * @property string $language
 * @property string $name
 *
 * @property Country $country
 */
class CountryLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'country_lang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['language'], 'required'],
            ['language', 'unique', 'targetAttribute' => ['language', 'country_id'], 'message' => Yii::t('backend', 'Translation already exists'),],
            [['country_id'], 'integer'],
            [['language'], 'string', 'max' => 6],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('main', 'ID'),
            'country_id' => Yii::t('backend', 'Country'),
            'language' => Yii::t('main', 'Language'),
            'name' => Yii::t('backend', 'Name'),
        ];
    }
    
    public function getRelatedModel()
    {        
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }
    
    public function getLanguageModel()
    {        
        return $this->hasOne(Language::className(), ['local' => 'language']);
    }
    
    public function getLanguageName()
    {        
        return $this->languageModel->name;
    }
    
    public function afterSave($insert, $changedAttributes) {
        parent::afterSave($insert, $changedAttributes);
        
        if($insert && $this->language == Yii::$app->sourceLanguage){
            $languages = Language::find()->where('local != :local', [':local' => Yii::$app->sourceLanguage])->all();

            foreach ($languages as $language) {
                $langTranslation = new self();
                $langTranslation->attributes = $this->attributes;
                $langTranslation->language = $language->local;
                $langTranslation->save(false);
            }
        }
    }
}
