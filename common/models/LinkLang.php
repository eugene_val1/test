<?php

namespace common\models;

use Yii;
use common\models\Language;

/**
 * This is the model class for table "link_lang".
 *
 * @property integer $id
 * @property integer $link_id
 * @property string $language
 * @property string $title
 * @property Link $link
 */
class LinkLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'link_lang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['language', 'title'], 'required'],
            ['language', 'unique', 'targetAttribute' => ['language', 'link_id'], 'message' => Yii::t('backend', 'Translation already exists'),],
            [['link_id'], 'integer'],
            [['language'], 'string', 'max' => 6],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('main', 'ID'),
            'link_id' => Yii::t('main', 'Link'),
            'language' => Yii::t('main', 'Language'),
            'title' => Yii::t('main', 'Title'),
        ];
    }
    
    public function getRelatedModel()
    {        
        return $this->hasOne(Link::className(), ['id' => 'link_id']);
    }
    
    public function getLanguageModel()
    {        
        return $this->hasOne(Language::className(), ['local' => 'language']);
    }
    
    public function getLanguageName()
    {        
        return $this->languageModel->name;
    }
    
    public function afterSave($insert, $changedAttributes) {
        parent::afterSave($insert, $changedAttributes);
        
        if($insert && $this->language == Yii::$app->sourceLanguage){
            $languages = Language::find()->where('local != :local', [':local' => Yii::$app->sourceLanguage])->all();

            foreach ($languages as $language) {
                $langTranslation = new self();
                $langTranslation->attributes = $this->attributes;
                $langTranslation->language = $language->local;
                $langTranslation->save(false);
            }
        }
    }
}
