<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "notification_settings".
 *
 * @property integer $user_id
 *
 * @property integer $target_email
 * @property integer $target_bots
 *
 * @property integer $messages
 * @property integer $appointment_new
 * @property integer $appointment_rescheduled
 * @property integer $appointment_canceled
 * @property integer $appointment_completed
 * @property integer $appointment_missed
 * @property integer $appointment_paid
 * @property integer $appointment_not_paid
 * @property integer $appointment_reminder
 */
class NotificationSettings extends ActiveRecord
{
    const TYPE_MESSAGES = 'messages';
    const TYPE_APPOINTMENT = 'appointment';
    const TYPE_APPOINTMENT_REMINDER = 'appointment_reminder';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'notification_settings';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            ['user_id', 'integer'],
            [['user_id'], 'unique'],
            [
                [
                    'target_email',
                    'target_bots',

                    'messages',
                    'appointment_new',
                    'appointment_rescheduled',
                    'appointment_canceled',
                    'appointment_completed',
                    'appointment_missed',
                    'appointment_paid',
                    'appointment_not_paid',
                    'appointment_reminder',
                ],
                'boolean'
            ],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'User',
            'target_email' => Yii::t('main', 'Email'),
            'target_bots' => Yii::t('main', 'Bots'),
            'messages' => Yii::t('main', 'New messages'),
            'appointment_new' => Yii::t('main', 'New Appointments'),
            'appointment_rescheduled' => Yii::t('main', 'Rescheduled Appointments'),
            'appointment_canceled' => Yii::t('main', 'Canceled Appointments'),
            'appointment_completed' => Yii::t('main', 'Completed Appointments'),
            'appointment_missed' => Yii::t('main', 'Missed Appointments'),
            'appointment_paid' => Yii::t('main', 'Paid Appointments'),
            'appointment_not_paid' => Yii::t('main', 'Not Paid Appointments'),
            'appointment_reminder' => Yii::t('main', 'Remind of Appointments'),
        ];
    }
}
