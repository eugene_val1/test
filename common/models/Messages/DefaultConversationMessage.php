<?php

namespace common\models\Messages;

/**
 * Class DefaultConversationMessage
 *
 * @package common\models\Messages
 */
class DefaultConversationMessage extends ConversationMessage
{
    const TYPE = 'system';

    /**
     * DefaultConversationMessage constructor.
     */
    public function __construct()
    {
        parent::__construct([]);

        $this->type = self::TYPE;
        $this->scenario = 'contact';
        $this->text = <<<EOT
[Это автоматическое сообщение]

Сессия назначена.

Вот несколько подсказок, чтобы пользоваться платформой было удобнее:

1. Доступ в онлайн-комнату откроется только после оплаты. Поэтому оплачивайте сессию не позже, чем за 24 часа до начала, чтобы платеж гарантированно успел пройти.

2. Используйте браузер Гугл Хром. На всякий случай отключите блокиратор рекламы АдБлок: иногда он ошибочно блокирует всплывающие сообщения и влияет на работу видеосвязи. Рекламы на платформе нет.

3. Во время первого входа в онлайн-комнату разрешите доступ к камере и микрофону. Браузер запросит его автоматически.

4. Если вы хотите работать с айфона или айпада, скачайте наше приложение https://itunes.apple.com/us/app/treatfield/id1275918641 и не забудьте полностью зарядить девайс.
EOT;

    }
}
