<?php

namespace common\models\Messages;

use common\models\Appointment;
use common\models\TypeQuery;
use Yii;
use yii\helpers\Json;

/**
 * Class ConversationAppointmentMessage
 *
 * @package common\models
 */
class ConversationAppointmentMessage extends ConversationMessage
{
    const TYPE = 'appointment';

    private $events;

    /**
     * @return TypeQuery
     */
    public static function find()
    {
        return new TypeQuery(get_called_class(), ['type' => self::TYPE]);
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        parent::beforeSave($insert);

        if ($insert) {
            $this->type = self::TYPE;
            $data = Json::decode($this->data);

            if (is_array($data)) {
                foreach ($data as $k => $item) {
                    $data[$k] = [
                        'start' => Appointment::convertLocalToGMT($item['start']),
                        'end' => Appointment::convertLocalToGMT($item['end']),
                    ];
                }
                $this->data = Json::encode($data);
            } else {
                $this->data = '[]';
            }
        }

        return true;
    }

    /**
     * @return array
     */
    public function rules()
    {
        return array_merge(
            parent::rules(),
            [
                [['data'], 'string'],
            ]
        );
    }

    /**
     * @return array|null
     */
    public function getEvents()
    {
        if ($this->events === null) {
            $events = [];
            $data = Json::decode($this->data);

            if (is_array($data)) {
                foreach ($data as $event) {
                    $events[] = [
                        'title' => '',
                        'start' => Yii::$app->formatter->asDatetime($event['start'], 'php:Y-m-d H:i:s'),
                        'end' => Yii::$app->formatter->asDatetime($event['end'], 'php:Y-m-d H:i:s'),
                    ];
                }
            }

            $this->events = $events;
        }

        return $this->events;
    }

    /**
     * @return string
     */
    public function getFirst()
    {
        $events = $this->getEvents();

        if (count($events) > 0) {
            return Yii::$app->formatter->asDatetime($events[0]['start'], 'php:Y-m-d');
        }

        return Yii::$app->formatter->asDatetime(time(), 'php:Y-m-d');
    }
}
