<?php

namespace common\models\Messages;

use common\models\Appointment;
use common\models\AppointmentEvent;
use common\models\Client;
use common\models\Conversation;
use common\models\Event;
use common\models\MessagesNotifications;
use common\models\Profile;
use common\models\Timezone;
use common\models\User;
use common\models\UserBots;
use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "conversation_message".
 *
 * @property integer $id
 * @property integer $conversation_id
 * @property integer $receiver_id
 * @property integer $user_id
 * @property string  $text
 * @property integer $status
 * @property string  $type
 * @property integer $created
 */
class ConversationMessage extends ActiveRecord
{
    const STATUS_NEW = 0;
    const STATUS_READ = 1;
    const TYPE = 'standart';

    public $contact;
    public $contact_id;
    public $contact_name;

    /**
     * @return string
     */
    public static function tableName()
    {
        return 'conversation_message';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['text'], 'required'],
            [['contact_id'], 'required', 'on' => 'contact'],
            ['contact_id', 'validateContact', 'on' => 'contact'],
            [['status', 'created', 'conversation_id', 'user_id', 'contact_id', 'receiver_id'], 'integer'],
            [['text'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('main', 'ID'),
            'conversation_id' => Yii::t('main', 'Conversation'),
            'user_id' => Yii::t('main', 'User'),
            'contact_id' => Yii::t('main', 'User'),
            'contact_name' => Yii::t('main', 'New contact name'),
            'text' => Yii::t('main', 'Message text'),
            'status' => Yii::t('main', 'Status'),
            'created' => Yii::t('main', 'Created'),
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created'],
                ],
            ],
        ];
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function validateContact($attribute, $params)
    {
        if (!$this->hasErrors()) {
            if ($this->contact_id == Yii::$app->user->id) {
                $this->addError($attribute, Yii::t('main', "You can't send message to yourself!"));
            }
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConversation()
    {
        return $this->hasOne(Conversation::className(), ['id' => 'conversation_id']);
    }

    /**
     * Message creator
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Profile::className(), ['user_id' => 'user_id']);
    }

    /**
     * Message receiver
     * @return \yii\db\ActiveQuery
     */
    public function getReceiver()
    {
        return $this->hasOne(Profile::className(), ['user_id' => 'receiver_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Client::className(), ['conversation_id' => 'conversation_id']);
    }

    /**
     * Bot receiver
     * @return \yii\db\ActiveQuery
     */
    public function getBotReceivers()
    {
        return $this->hasMany(UserBots::className(), ['user_id' => 'receiver_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNotification()
    {
        return $this->hasOne(MessagesNotifications::className(), ['message_id' => 'id']);
    }

    /**
     * @param array $row
     * @return ConversationAppointmentMessage|ConversationMessage
     */
    public static function instantiate($row)
    {
        switch ($row['type']) {
            case ConversationAppointmentMessage::TYPE:
                return new ConversationAppointmentMessage();
            default:
                return new self;
        }
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        $this->type = static::TYPE;
        Conversation::updateAll(['hidden' => false], ['id' => $this->conversation_id]);
        return parent::beforeSave($insert);
    }

    /**
     * Change message text here according to an event
     * 'Сессия No.1 назначена на [time] в [date]. Оплатить сейчас.'
     *
     * @param Event|AppointmentEvent $event
     */
    public function processEventMessage($event)
    {
        if ($event->getAppointment() && in_array($event->appointmentStatus, [Appointment::STATUS_NEW, Appointment::STATUS_RESCHEDULE])) {

            Yii::$app->formatter->timeZone = $event->getUser()->timezone;
            $date = Timezone::convert($event->getClient()->next_appointment_start, $event->getUser()->timezone, 'd.m');
            $time = Timezone::convert($event->getClient()->next_appointment_start, $event->getUser()->timezone, 'H:i');

            $this->text = Yii::t('main', 'Appointment No.{0} has been rescheduled for {1} at {2}. {3}Pay now{4}', [
                $event->getAppointment()->number,
                $date,
                $time,
                $event->getAppointment()->payment_status == Appointment::NOT_PAID ?
                    '<a href="/payment/create?id=' . $event->getAppointment()->id . '" data-target="#modal">' :
                    '<a href="" style="pointer-events: none; color: #747474">'
                ,
                '</a>'
            ]);

            $this->text .= ' <span style="display: none" data-appointment-id="' . ($event->getAppointment()->id) . '"></span>';
        }
    }

    /**
     * Replace links with href tags
     *
     * @param $text
     * @param array $appointments
     * @return mixed
     */
    public static function handleMessageText($text, array $appointments)
    {
        // The Regular Expression filter
        $urlPattern = '@(http(s)?://)(([a-zA-Z])([-\w]+\.)+([^\s\.]+[^\s]*)+[^,.\s])@';
        $appointmentPattern = '/(Appointment No\.\d+)|(Сессия No\.\d+)|(Appointment #\d+)|(Сессия #\d+)/iu';
        $payAppointmentPattern = '/(Оплатить сейчас)|(Pay now)/iu';
        $appointmentIdPattern = '/data-appointment-id="([^"]*)"/i';

        // Check if there is a url in the text
        if (preg_match($urlPattern, $text, $url)) {
            $text = preg_replace($urlPattern, "<a href='{$url[0]}' target='_blank'>{$url[0]}</a>", $text);
        }

        // Check if there is a Appointment #
        if (preg_match($appointmentPattern, $text, $url)) {
            $text = preg_replace($appointmentPattern, "<a href='/account'>{$url[0]}</a>", $text);
        }

        //Hide Pay now button
        preg_match($appointmentIdPattern, $text, $values);
        $appointmentId = isset($values[1]) ? (int)$values[1] : 0;
        $appointmentStatus = null;
        if ($appointmentId > 0 && isset($appointments[$appointmentId]) && Yii::$app->user->identity->role == User::ROLE_USER) {
            $appointmentStatus = (int)$appointments[$appointmentId];
        }

        if (Yii::$app->user->identity->role == User::ROLE_THERAPIST || $appointmentStatus === Appointment::PAID) {
            $text = preg_replace($payAppointmentPattern, "", $text);
        }

        return $text;
    }
}
