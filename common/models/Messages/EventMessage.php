<?php

namespace common\models\Messages;

/**
 * Class EventMessage
 *
 * @package common\models\Messages
 */
class EventMessage extends ConversationMessage
{
    const TYPE = 'system';
}
