<?php

namespace common\models;

use Yii;
use himiklab\sortablegrid\SortableGridBehavior;

/**
 * This is the model class for table "property".
 *
 * @property integer $id
 * @property integer $number
 * @property string $type
 */
class Property extends \yii\db\ActiveRecord
{
    const TYPE_SPECIALTY = 'specialty';
    const TYPE_APPROACH = 'approach';
    const TYPE_FORMAT = 'format';
    const TYPE_LANGUAGE = 'language';
    
    public $name;
    public $translatedAttributes = [
        'name' => 'string',
    ];
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'property';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['number'], 'integer'],
            [['type'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'number' => Yii::t('backend', 'Number'),
            'name' => Yii::t('backend', 'Name'),
            'type' => Yii::t('backend', 'Type'),
        ];
    }
    
    public function behaviors() {
        return [
            'sort' => [
                'class' => SortableGridBehavior::className(),
                'sortableAttribute' => 'number'
            ],
        ];
    }
    
    public function getTranslations() {
        return $this->hasMany(PropertyLang::className(), ['property_id' => 'id'])->indexBy('language');
    }

    public function getTranslation() {
        return $this->hasOne(PropertyLang::className(), ['property_id' => 'id'])->where('language = :language', [':language' => Yii::$app->language]);
    }
    
    public function getDefaultTranslation() {
        return $this->hasOne(PropertyLang::className(), ['property_id' => 'id'])->where('language = :language', [':language' => Yii::$app->sourceLanguage]);
    }

    public function afterFind() {
        $translation = ($this->translation !== null) ? $this->translation : $this->defaultTranslation;
        
        foreach ($this->translatedAttributes as $attribute => $type) {
                $this->{$attribute} = $translation->{$attribute};
        }
            
        parent::afterFind();
    }
    
    public static function getTypes() {
        return [
            self::TYPE_SPECIALTY => Yii::t('main', 'Specialties'),
            self::TYPE_APPROACH => Yii::t('main', 'Treatment Approach'),
            self::TYPE_FORMAT => Yii::t('main', 'Session formats'),
            self::TYPE_LANGUAGE=> Yii::t('main', 'Working languages')
        ];
    }
    
    public function getTypeName() {
        return Yii::t('main', ucfirst($this->type));
    }
    
    public static function getTypeLabel($type) {
        $types = self::getTypes();
        return isset($types[$type]) ? $types[$type] : '';
    }
    
    public static function getWithTypes(){
        $items= self::find()->with('translation')->all();
        $properties = [];
        foreach($items as $item){
            $properties[$item->type][$item->id] = $item->name;
        }
        
        return $properties;
    }
    
    public function beforeDelete() {
        TherapistProperty::deleteAll('property_id = :property_id', [':property_id' => $this->id]);
        return parent::beforeDelete();
    }
}
