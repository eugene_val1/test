<?php

use yii\caching\FileCache;
use yii\rbac\PhpManager;
use common\components\Formatter;
use yii\i18n\DbMessageSource;
use common\components\TranslationEventHandler;
use yii\elasticsearch\Connection;
use yii\caching\MemCache;
use common\components\Mailer;
use common\components\SettingManager;
use common\components\SiteManager;

return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'name' => 'TreatField',
    'language' => 'ru-RU',
    'sourceLanguage' => 'ru-RU',
    'bootstrap' => [],
    'components' => [
        'siteManager' => [
            'class' => SiteManager::class,
        ],
        'cache' => [
            'class' => MemCache::class,
            'useMemcached' => true,
            'servers' => [
                [
                    'host' => '127.0.0.1',
                    'port' => 11211,
                    'weight' => 60,
                ]
            ],
        ],
        'frontendCache' => [
            'class' => FileCache::class,
            'cachePath' => Yii::getAlias('@frontend') . '/runtime/cache'
        ],
        'settingManager' => [
            'class' => SettingManager::class,
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '/' => 'site/index',
            ],
        ],
        'authManager' => [
            'class' => PhpManager::class,
            'defaultRoles' => ['user', 'editor', 'admin', 'therapist'],
            'itemFile' => '@common/components/rbac/items.php',
            'assignmentFile' => '@common/components/rbac/assignments.php',
            'ruleFile' => '@common/components/rbac/rules.php'
        ],
        'formatter' => [
            'class' => Formatter::class,
            'locale' => 'ru-RU',
        ],
        'i18n' => [
            'translations' => [
                '*' => [
                    'class' => DbMessageSource::class,
                    'forceTranslation' => true,
                    'sourceMessageTable' => '{{%source_translation_message}}',
                    'messageTable' => '{{%translation_message}}',
                    'enableCaching' => false,
                    'cachingDuration' => 3600,
                    'on missingTranslation' => [TranslationEventHandler::class, 'handleMissingTranslation']
                ],
            ],
        ],
        'elasticsearch' => [
            'class' => Connection::class,
            'nodes' => [
                ['http_address' => '127.0.0.1:9200']
            ],
        ],
        'mailer' => [
            'class' => Mailer::class,
            'viewPath' => '@common/mail',
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp.gmail.com',
                'username' => 'team@treatfield.com',
                'password' => 'IAVAtreatfield2017@ua!',
                'port' => '587',
                'encryption' => 'tls',
            ],
        ],
        'mailerWorker' => [
            'class' => \yii\swiftmailer\Mailer::class,
            'useFileTransport' => false,
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp.gmail.com',
                'username' => 'family.massage.cabinet@gmail.com',
                'password' => 'massage2017',
                'port' => '587',
                'encryption' => 'tls',
            ],
        ]
    ],
];
