<?php

return [
    'domainName' => 'treatfield.com',
    'supportEmail' => 'contact@treatfield.com',

    'defaultCountry' => 'UA',
    'defaultTimezone' => 'UTC',

    'user.tokenExpire' => 3600,

    'uploadUrl' => '/media/',
    'uploadPath' => '/web/media/',

    'videoServerUrl' => 'https://video-treatfield.top',
    'auxiliaryVideoServerUrl' => 'https://video-treatfield.top',
    'baseUrl' => 'https://www.treatfield.com',
    'testBaseUrl' => 'https://test.treatfield.com',

    'iosApp' => 'itms://itunes.apple.com/us/app/treatfield/id1275918641',
    'appStoreLink' => 'https://itunes.apple.com/us/app/treatfield/id1275918641?l=ru&mt=8',

    'autologinHash' => 'WdGS3KzCLcy0gW1lx2qe',
    'apiSecret' => 'ONONIO8YUG57UTVIUVOUBOI74VUVIU78IVIV65fJHVIUI',

    //https://ipstack.com
    'geoApi' => [
        'url' => 'http://api.ipstack.com',
        'accessToken' => 'ba1affeaeee4381ba4c484a1d33b032c',
    ],

    'mediaLinks' => [
        'facebook' => 'https://www.facebook.com/treatfield',
        'wasmedia' => 'https://was.media/2018-04-02-7-velikih-snov-chto-skryvaet-soznanie-teh-kto-tvoril-istoriju',
        'womanua' => 'http://woman.ua/104853-kak-vybrat-psihoterapevta',
        'thevillage' => 'https://www.the-village.com.ua/village/knowledge/simple-words',
        'vcru' => 'https://vc.ru/hr/61225-karera-ili-otnosheniya-kak-sdelat-pravilnyy-vybor?fbclid=IwAR2JrwaDjzZohvPvVimmHcZ2INcFo2AemluIL4SPyvZHLU8Z_3tIxinETJQ',
        'nova' => 'http://www.novamagazine.com.ua/psihoterapevt-anton-fedorets-na-psihoterapiyu-hodyat-sil-ny-e-lyudi/?fbclid=IwAR2S5SpdAQCuEYwNgHDjUc_-E6plpEXxyojHMGMP0na2SEALVUQVl7i_L5w',
        'brodude' => 'https://brodude.ru/5-psihologicheskih-problem-s-kotorymi-stalkivaetsya-kazhdyj-muzhchina-2/',
        'redbull' => 'https://www.redbull.com/ua-uk/yak-efektyvno-vidpochyty-vid-roboty',
        'imi' => 'https://imi.org.ua/monitorings/ataka-troliv-on-lajn-nebezpeky-dlya-zhurnalistiv-i29542?fbclid=IwAR1eZTpKiQ9BsKuiRnRlh7EhYtZ4rJ24ZucELEWlZR6pN1FkS4nw_HAOqZg',
        'inspired' => 'https://inspired.com.ua/world/lifehacks/brudne-povitrya-j-okeany-plastyku-shho-robyty-koly-tryvozhat-problemy-dovkillya/',
    ],

    'payments' => [
        'defaultRatio' => 25.64,
    ],

    'appointments' => [
        //amount of appointments that could be payed at once
        'maxAmountPayment' => 5,
    ],

    //bots
    'telegram' => [
        //webhook: https://api.telegram.org/bot{botKey}/setWebhook?url={url}?h={secretHash}
        'botName' => 'TreatfieldBot',
        'botKey' => '541827805:AAGr6rJdXmgjaw-Au0xgttxdyVfiDcA3zFQ',
        'botUrl' => 'https://telegram.me/TeatfieldBot?start=[ref]',
        'secretHash' => 'ww4SQi',
    ],
    'facebook' => [
        'botName' => 'FacebookBot',
        'botKey' => 'EAAMkoovC4AsBANPn9UksTP8CuwX5yNCibQgPdAi5vjdUga9HWGAJ23pQSTEZB646njlA9fvZAyzehmszx4OJ4QZB4Ig8hMUtZBWsZCpXIDKEagh4WS9qyZAC0IWTDP3d56fePZBPyZBiUzv1Cmb51PAPYWSWtTzlN4WEsP1ge81sJlhL2XKzlfiG',
        'botUrl' => 'http://m.me/treatfield.bot.help?ref=[ref]',
        'secretHash' => 'ArwAk',
    ],

    'sentry' => [
        'main-dns' => 'https://d3d2b028c67543c292d1b6209d915a39@sentry.io/1527090',
        'admin-dns' => 'https://657583c462e646dca7d4b9571ebb2c72@sentry.io/1527099',
        'console-dns' => 'https://09cd394a61874351bd8e5443db8c1ee1@sentry.io/1527640',
    ],
];
