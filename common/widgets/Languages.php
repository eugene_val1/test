<?php

namespace common\widgets;

use common\models\Language;

/**
 * Class Languages
 *
 * @package common\widgets
 */
class Languages extends \yii\bootstrap\Widget
{
    /**
     * @var bool
     */
    public $onlyActive = false;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('@app/widgets/views/languages/view', [
            'current' => Language::getCurrent(),
            'langs' => $this->getLanguages(),
        ]);
    }

    /**
     * @return array|\yii\db\ActiveRecord[]
     */
    public function getLanguages()
    {
        $query = 'id != :current_id';
        $params = [':current_id' => Language::getCurrent()->id];

        if ($this->onlyActive) {
            $query .= ' AND active = :active';
            $params[':active'] = Language::STATUS_ACTIVE;
        }

        return Language::find()->where($query, $params)->all();
    }
}