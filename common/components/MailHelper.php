<?php

namespace common\components;

use common\models\Appointment;
use common\models\Therapist;
use common\models\User;
use DateTime;
use DateTimeZone;
use DrewM\MailChimp\MailChimp;
use Yii;
use yii\base\Component;
use yii\mail\MessageInterface;

/**
 * Class MailHelper
 * @package common\components
 */
class MailHelper extends Component
{
    const SWIFT_SIGNER_SELECTOR = '1511900561.treatfield';

    /**
     * Prepare DKIM signer
     * @return \Swift_Signers_DKIMSigner
     */
    public static function getSwiftSigner()
    {
        return new \Swift_Signers_DKIMSigner(self::getPrivateKeyFile(), Yii::$app->params['domainName'], self::SWIFT_SIGNER_SELECTOR);
    }

    /**
     * Sign message
     * @param MessageInterface $message
     * @return MessageInterface
     */
    public static function signMessage(MessageInterface $message)
    {
        if (YII_ENV_DEV || isset(Yii::$app->params['skipSignEmail'])) {
            return $message;
        }
        try {
            $message->getSwiftMessage()->attachSigner(self::getSwiftSigner());
        } catch (\Swift_TransportException $e) {
            Yii::error('Can not sign email message:' . $e->getMessage());
        } catch (\Swift_SwiftException $e) {
            Yii::error('Can not sign email message:' . $e->getMessage());
        } catch (\Exception $e) {
            Yii::error($e->getMessage());
        }

        return $message;
    }

    /**
     * @return string
     */
    public static function getPrivateKeyFile()
    {
        return 'file://' . Yii::getAlias('@common/config/privatekey.pem');
    }

    /**
     * Generate Google Calendar event link
     * @param string $text
     * @param string $details
     * @param integer $start
     * @param integer $end
     * @return string
     */
    public static function generateGoogleEventLink($text, $details, $start, $end)
    {
        $link = "https://www.google.com/calendar/render?action=TEMPLATE";
        $dateFormat = 'Ymd\THi\0\0\Z';

        $link .= "&text=" . urlencode($text);
        $link .= "&details=" . urlencode($details);

        $dt = new DateTime();
        $dt->setTimeZone(new DateTimeZone('UTC'));

        $dt->setTimestamp($start);
        $dates = $dt->format($dateFormat);

        $dt->setTimestamp($end);
        $dates .= '/' . $dt->format($dateFormat);

        $link .= '&dates=' . $dates;

        return $link;
    }

    /**
     * @param string $email
     * @param string $status
     */
    public static function addToMailchimp($email, $status = 'pending')
    {
        if (YII_ENV_DEV) {
            return;
        }

        try {
            if (empty(Yii::$app->params['mailchimp']['apiKey'])) {
                throw new \RuntimeException('Empty mailchimp.apiKey');
            }
            $mailChimp = new MailChimp(Yii::$app->params['mailchimp']['apiKey']);
            $lists = $mailChimp->get('lists');

            if (!is_array($lists)) {
                return;
            }

            foreach ($lists['lists'] as $list) {
                $mailChimp->post('lists/' . $list['id'] . '/members', [
                    'email_address' => $email,
                    'status' => $status,
                ]);
            }
        } catch (\Exception $e) {
            Yii::trace('Can not add user to MailChimp: ' . $e->getMessage());
        }
    }

    /**
     * @param User $user
     *
     * @throws \RuntimeException
     */
    public static function sendActivationEmail($user)
    {
        $email = Yii::$app->mailer->compose('userActivation', ['user' => $user])
            ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name])
            ->setTo($user->email)
            ->setSubject(Yii::$app->name . ' ' . Yii::t('main', 'Account activation'));

        //sign message
        self::signMessage($email);

        if (!$email->send()) {
            throw new \RuntimeException('Internal error');
        }
    }

    /**
     * @param User $user
     * @param string $newEmail
     * @param string $token
     *
     * @throws \RuntimeException
     */
    public static function sendConfirmNewEmail($user, $newEmail, $token)
    {
        $email = Yii::$app->mailer->compose('confirmNewEmail', [
                'user' => $user,
                'email' => $newEmail,
                'token' => $token,
            ])
            ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name])
            ->setTo($newEmail)
            ->setSubject(Yii::$app->name . ' ' . Yii::t('main', 'Email verification'));

        //sign message
        self::signMessage($email);

        if (!$email->send()) {
            throw new \RuntimeException('Internal error');
        }
    }

    /**
     * @param User $user
     * @param Therapist $therapist
     * @param Appointment $appointment
     *
     * @throws \RuntimeException
     */
    public static function appointmentReminder($appointment, $user, $therapist)
    {
        $email = Yii::$app->mailer->compose('appointmentReminder', [
            'appointment' => $appointment,
            'user' => $user,
            'therapist' => $therapist,
        ])
        ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name])
        ->setTo($user->email)
        ->setSubject(Yii::t('mail', 'New appointment in 24 hours'));

        //sign message
        self::signMessage($email);

        if (!$email->send()) {
            throw new \RuntimeException('Internal error');
        }
    }
} 