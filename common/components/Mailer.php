<?php

namespace common\components;

use common\models\User;
use yii\mail\MessageInterface;

/**
 * Class Mailer
 *
 * @package common\components
 */
class Mailer extends \yii\swiftmailer\Mailer
{
    /**
     * Mailer constructor
     *
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        parent::__construct($config);
    }

    /**
     * @param MessageInterface $message
     *
     * @return bool
     */
    public function send($message)
    {
        $addresses = $message->getTo();
        $newAddresses = [];

        if (!is_array($addresses) && $this->checkEmail($addresses)) {
            return parent::send($message);
        }

        foreach ($addresses as $address => $name) {
            if (!$this->checkEmail($address)) {
                continue;
            }
            $newAddresses[$address] = $name;
        }

        $message->setTo($newAddresses);

        return parent::send($message);
    }

    /**
     * @param string $email
     *
     * @return bool
     */
    protected function checkEmail($email)
    {
        return strpos($email, User::DELETED_PREFIX) === false;
    }
}