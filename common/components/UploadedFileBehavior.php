<?php

namespace common\components;

use Yii;
use yii\base\Behavior;
use yii\web\BadRequestHttpException;
use yii\web\UploadedFile;
use yii\imagine\Image;
use Imagine\Image\Box;
use Imagine\Image\Point;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * Class UploadedFileBehavior
 * @package common\components
 */
class UploadedFileBehavior extends Behavior
{
    const RESIZE_NONE = 'none';
    const RESIZE_THUMBNAIL = 'thumbnail';
    const RESIZE_FORCE_DIMENSIONS = 'force';

    /**
     * @var string
     */
    public $subdirectoryName;

    /**
     * @var string
     */
    public $defaultImage;

    /**
     * @var string
     */
    public $resizeType = self::RESIZE_THUMBNAIL;

    /**
     * @var array
     */
    public $attributes = [];

    /**
     * @var array
     */
    public $oldFiles = [];

    /**
     * @return array
     */
    public function events()
    {
        return [
            ActiveRecord::EVENT_BEFORE_VALIDATE => 'uploadFiles',
            ActiveRecord::EVENT_AFTER_VALIDATE => 'saveFiles',
            ActiveRecord::EVENT_AFTER_DELETE => 'deleteFiles',
            ActiveRecord::EVENT_AFTER_FIND => 'setOldFiles',
        ];
    }

    /**
     * Save old attribute value
     */
    public function setOldFiles($event)
    {
        foreach ($this->attributes as $attribute => $params) {
            $this->oldFiles[$attribute] = $this->getMediaFile($attribute);
        }
    }

    /**
     * @param $baseName
     * @return string
     */
    private function createFilename($baseName, $extension)
    {
        return sprintf(
            '%s-treatfield-%d-%d.%s',
            (string)substr($baseName, 0, 40),
            time(),
            random_int(1000, 9999),
            (string)$extension
        );
    }

    /**
     * @param $filename
     * @return string
     */
    private function getFileSubdirectory($filename)
    {
        preg_match_all("/^.*-treatfield-(.*)-(.*?)\..*/", $filename, $matches);
        $defaultSubdirectories = substr($filename, 0, 2) . '/' . substr($filename, 2, 2);

        if (empty($matches[1][0]) || empty($matches[2][0])) {
            return $defaultSubdirectories;
        }

        return $matches[1][0] . '/' . $matches[2][0];
    }

    /**
     * Upload all files before validation
     */
    public function uploadFiles($event)
    {
        foreach ($this->attributes as $attribute => $params) {
            $fileAtrribute = ArrayHelper::getValue($params, 'fileAttribute', false);
            if ($fileAtrribute) {
                $this->owner->$fileAtrribute = UploadedFile::getInstance($this->owner, $fileAtrribute);
                if ($this->owner->$fileAtrribute) {
                    $this->owner->$attribute = $this->createFilename($this->owner->$fileAtrribute->baseName, $this->owner->$fileAtrribute->extension);
                }
            }
        }
    }


    /**
     * Save all image diles after validation
     */
    public function saveFiles($event)
    {
        foreach ($this->attributes as $attribute => $params) {
            $fileAtrribute = ArrayHelper::getValue($params, 'fileAttribute', false);
            if ($fileAtrribute && $this->owner->$fileAtrribute && !$this->owner->hasErrors($fileAtrribute)) {
                $path = $this->getMediaFile($attribute);
                $this->owner->$fileAtrribute->saveAs($path);

                if ($this->resizeType != self::RESIZE_NONE) {
                    $this->resizeFile($path, $params);
                }

                $this->deleteOldImage($attribute);
            }
        }
    }

    /**
     * @param $path
     * @param $params
     */
    public function resizeFile($path, $params)
    {
        $width = ArrayHelper::getValue($params, 'width', false);
        $height = ArrayHelper::getValue($params, 'height', false);
        if ($width && $height) {
            if ($this->resizeType == self::RESIZE_THUMBNAIL) {
                Image::thumbnail($path, $width, $height)->save($path);
            } else if ($this->resizeType == self::RESIZE_FORCE_DIMENSIONS) {
                $this->forceResize($path, $width, $height);
            }
        }
    }

    /**
     * Delete all image files
     */
    public function deleteFiles($event)
    {
        foreach ($this->attributes as $attribute => $params) {
            $this->deleteMediaFile($attribute);
        }
    }

    /**
     * Fetch stored media folder path
     * @return string
     */
    public function getMediaFolder()
    {
        $path = Yii::getAlias('@frontend') . Yii::$app->params['uploadPath'] . $this->subdirectoryName;
        $this->checkDirectory($path);

        return $path;
    }

    /**
     * Path according to image filename
     * @return string
     */
    public function getImagePath($attribute, $create = false)
    {
        $subdirectories = $this->getFileSubdirectory($this->owner->$attribute);
        $path = $this->getMediaFolder() . '/' . $subdirectories;
        if ($create || is_file($path . '/' . $this->owner->$attribute)) {
            $this->checkDirectory($path);
            return $subdirectories . '/' . $this->owner->$attribute;
        } else {
            return $this->owner->$attribute;
        }
    }

    /**
     * Create directory if not exist
     */
    public function checkDirectory($path)
    {
        if (!is_dir($path)) {
            mkdir($path, 0777, true);
        }
    }

    /**
     * Fetch stored file name with complete path
     * @return string
     */
    public function getMediaFile($attribute)
    {
        return $this->owner->$attribute ?
            $this->getMediaFolder() . '/' . $this->getImagePath($attribute, true)
            : null;
    }

    /**
     * Fetch stored file url
     * @return string
     */
    public function getFileUrl($attribute)
    {
        $defaultImage = $this->defaultImage
            ? Yii::$app->params['siteUrl'] . '/img/' . $this->defaultImage
            : false;

        if (!$this->owner->$attribute) {
            return $defaultImage;
        }

        if (substr($this->owner->$attribute, 0, 7 ) !== 'http://'
            && substr($this->owner->$attribute, 0, 8 ) !== 'https://'
        ) {
            return $this->getMediaUrl() . '/' . $this->getImagePath($attribute);
        }

        //check external resource
        $ch = curl_init($this->owner->$attribute);
        curl_setopt($ch, CURLOPT_NOBODY, true);
        curl_exec($ch);
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        if ($code >= 400) {
            return $defaultImage;
        }

        return $this->owner->$attribute;
    }

    /**
     * @return string
     */
    public function getMediaUrl()
    {
        return Yii::$app->params['siteUrl'] . Yii::$app->params['uploadUrl'] . $this->subdirectoryName;
    }

    /**
     * Process deletion of image
     * @return boolean the status of deletion
     */
    public function deleteMediaFile($attribute)
    {
        $this->deleteFile($this->getMediaFile($attribute));
    }

    /**
     * @param $attribute
     */
    public function deleteOldImage($attribute)
    {
        $oldFile = ArrayHelper::getValue($this->oldFiles, $attribute, false);
        if ($oldFile) {
            $this->deleteFile($oldFile);
        }
    }

    /**
     * @param $file
     */
    public function deleteFile($file)
    {
        if (!empty($file) && file_exists($file)) {
            unlink($file);
        }
    }

    /**
     * @param $path
     * @param $imageWidth
     * @param $imageHeight
     */
    public function forceResize($path, $imageWidth, $imageHeight)
    {
        $image = Image::getImagine()->open($path);
        $imageSize = $image->getSize();
        $imageRatio = $imageSize->getWidth() / $imageSize->getHeight();
        $ratio = $imageWidth / $imageHeight;

        if ($imageRatio < $ratio) {
            $width = $imageWidth;
            $height = $width / $imageRatio;
        } else {
            $height = $imageHeight;
            $width = $height * $imageRatio;
        }

        $image->resize(new Box($width, $height));
        $image->crop(new Point(0, 0), new Box($imageWidth, $imageHeight));
        $image->save($path);
    }

    /**
     * @param $attribute
     * @param $url
     * @return bool
     */
    public function saveRemoteImage($attribute, $url)
    {
        $oldImage = $this->getMediaFile($attribute);

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        $rawdata = curl_exec($ch);

        if (curl_exec($ch) === false) {
            curl_close($ch);
            return false;
        }

        curl_close($ch);

        $this->owner->$attribute = Yii::$app->security->generateRandomString() . '.jpg';
        $file = $this->getMediaFile($attribute);
        $fp = fopen($file, 'w');
        fwrite($fp, $rawdata);
        fclose($fp);

        $this->deleteFile($oldImage);

        if ($this->resizeType != self::RESIZE_NONE) {
            $this->resizeFile($file, $this->attributes[$attribute]);
        }

        return true;
    }

    /**
     * @param $fullAttribute
     * @param $cropAttribute
     * @param $width
     * @param $height
     * @param $start
     */
    public function crop($fullAttribute, $cropAttribute, $width, $height, $start)
    {
        $this->owner->$cropAttribute = Yii::$app->security->generateRandomString() . '.jpg';
        Image::crop($this->getMediaFile($fullAttribute), $width, $height, $start)->save($this->getMediaFile($cropAttribute));
    }
}
