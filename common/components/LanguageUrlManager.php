<?php

namespace common\components;

use common\models\Language;
use yii\web\UrlManager;

/**
 * Class LanguageUrlManager
 *
 * @package common\components
 */
class LanguageUrlManager extends UrlManager
{
    /**
     * @param array|string $params
     *
     * @return string
     */
    public function createUrl($params)
    {
        $lang = Language::getCurrent();
        $url = parent::createUrl($params);
        if ($lang->url === 'ru') {
            return $url;
        }

        if ($url === '/') {
            return '/' . $lang->url;
        }

        return '/' . $lang->url . $url;
    }

    /**
     * @param $params
     * @param null $scheme
     *
     * @return string
     */
    public function createDefaultAbsoluteUrl($params, $scheme = null)
    {
        $params = (array)$params;
        $url = parent::createUrl($params);

        if (strpos($url, '://') === false) {
            $url = $this->getHostInfo() . $url;
        }

        if (is_string($scheme) && ($pos = strpos($url, '://')) !== false) {
            $url = $scheme . substr($url, $pos);
        }

        return $url;
    }
}