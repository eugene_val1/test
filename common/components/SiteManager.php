<?php

namespace common\components;

use Yii;
use yii\base\Component;
use yii\web\NotFoundHttpException;

/**
 * Class SiteManager
 *
 * @package common\components
 */
class SiteManager extends Component
{
    const APP_CONSOLE_ID = 'app-console';
    const APP_FRONTEND_ID = 'app-frontend';
    const APP_BACKEND_ID = 'app-backend';

    /**
     * @throws \yii\web\NotFoundHttpException
     */
    public function init()
    {
        parent::init();
    }

    /**
     * @return bool
     */
    public static function isIndexingDisabled()
    {
        return isset(Yii::$app->params['disableIndexing'])
            ? (bool)Yii::$app->params['disableIndexing']
            : false;
    }

    /**
     * @return mixed
     */
    public static function getFrontendBaseUrl()
    {
        if (YII_ENV_TEST) {
            return Yii::$app->params['testBaseUrl'];
        }

        return Yii::$app->params['baseUrl'];
    }

    /**
     * @return bool
     */
    public static function isConsoleApp()
    {
        return self::APP_FRONTEND_ID === Yii::$app->id;
    }
}