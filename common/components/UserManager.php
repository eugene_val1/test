<?php

namespace common\components;

use yii\base\Component;

/**
 * Class UserManager
 *
 * @package common\components
 */
class UserManager extends Component
{
    /**
     * @return string
     */
    public static function getUserIp()
    {
        return isset($_SERVER['HTTP_CF_CONNECTING_IP'])
            ? $_SERVER['HTTP_CF_CONNECTING_IP']
            : $_SERVER['REMOTE_ADDR'];
    }
}