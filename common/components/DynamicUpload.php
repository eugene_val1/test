<?php

namespace common\components;

use yii\base\DynamicModel;
use common\components\UploadedFileBehavior;

/**
 * Class DynamicUpload
 * @package common\components
 */
class DynamicUpload extends DynamicModel
{
    public $formName = '';
    public $subdirectoryName = 'images';
    public $attribute = 'image';
    public $fileAttribute = 'imageFile';

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => UploadedFileBehavior::className(),
                'subdirectoryName' => $this->subdirectoryName,
                'resizeType' => UploadedFileBehavior::RESIZE_NONE,
                'attributes' => [
                    $this->attribute => ['fileAttribute' => $this->fileAttribute],
                ]
            ],
        ];
    }

    /**
     * @return string
     */
    public function formName()
    {
        return $this->formName;
    }
}
