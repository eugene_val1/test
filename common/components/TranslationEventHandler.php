<?php

namespace common\components;

use yii\i18n\MissingTranslationEvent;
use common\models\SourceTranslationMessage;

/**
 * Class TranslationEventHandler
 * @package common\components
 */
class TranslationEventHandler
{

    /**
     * @param MissingTranslationEvent $event
     */
    public static function handleMissingTranslation(MissingTranslationEvent $event)
    {
        $sourceTransalation = SourceTranslationMessage::find()
            ->where('`category` = :category AND `message` = :message', [
                ':category' => $event->category,
                ':message' => $event->message,
            ])
            ->one();

        if (!$sourceTransalation) {
            $sourceTransalation = new SourceTranslationMessage([
                'category' => $event->category,
                'message' => $event->message,
                'type' => 'string'
            ]);

            $sourceTransalation->save(false);
        }
    }
}
