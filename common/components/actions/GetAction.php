<?php

namespace common\components\actions;

use Yii;
use yii\base\DynamicModel;
use common\components\UploadedFileBehavior;
use yii\base\Action;
use yii\web\Response;
use vova07\imperavi\helpers\FileHelper;

/**
 * Class GetAction
 * @package common\components\actions
 */
class GetAction extends Action
{
    /**
     * @inheritdoc
     */
    public function run()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = new DynamicModel();
        $model->attachBehavior('uploaded', [
            'class' => UploadedFileBehavior::className(),
            'subdirectoryName' => 'images/' . Yii::$app->user->id,
        ]);
        return FileHelper::findFiles($model->getMediaFolder(), ['url' => $model->getMediaUrl()]);
    }
}
