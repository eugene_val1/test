<?php

namespace common\components\actions;

use yii\base\Action;
use yii\web\BadRequestHttpException;
use yii\web\Response;
use common\components\DynamicUpload;
use Yii;

/**
 * Class UploadAction
 * @package common\components\actions
 */
class UploadAction extends Action
{
    /**
     * @return array
     * @throws BadRequestHttpException
     */
    public function run()
    {
        if (Yii::$app->request->isPost) {
            $model = new DynamicUpload(['file', 'image'], [
                'fileAttribute' => 'file',
                'subdirectoryName' => 'images/' . Yii::$app->user->id
            ]);
            $model->addRule('file', 'image');

            if ($model->validate()) {
                $result = [
                    'filelink' => $model->getFileUrl('image')
                ];
            } else {
                $result = [
                    'error' => $model->getFirstError('file')
                ];
            }
            Yii::$app->response->format = Response::FORMAT_JSON;

            return $result;
        } else {
            throw new BadRequestHttpException('Only POST is allowed');
        }
    }
}
