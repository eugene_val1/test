<?php

namespace common\components;

use Yii;
use yii\base\Component;
use yii\console\Application;
use yii\helpers\ArrayHelper;

/**
 * Class ConsoleHelper
 * @package common\components
 */
class ConsoleHelper extends Component
{
    /**
     * Create and return an application instance
     * if the console app doesn't exist we create a new instance
     * otherwise, it returns the existing instance
     *
     * @return null|\yii\base\Module|Application
     */
    public static function switchToConsoleApp()
    {
        $config = self::getConsoleConfig();
        if (!$consoleApp = Yii::$app->getModule($config['id']))
            $consoleApp = new Application($config);
        return $consoleApp;
    }

    /**
     * @param $action
     * @param array $params
     * @return bool
     */
    public static function runConsoleActionFromWebApp($action, $params = [])
    {
        $webApp = Yii::$app;
        $consoleApp = self::switchToConsoleApp();
        $result = ($consoleApp->runAction($action, $params) == \yii\console\Controller::EXIT_CODE_NORMAL);
        Yii::$app = $webApp;
        return $result;
    }

    /**
     * @return array
     */
    public static function getConsoleConfig()
    {
        return ArrayHelper::merge(
            require(Yii::getAlias('@common/config/main.php')),
            require(Yii::getAlias('@common/config/main-local.php')),
            require(Yii::getAlias('@console/config/main.php')),
            require(Yii::getAlias('@console/config/main-local.php'))
        );
    }
} 