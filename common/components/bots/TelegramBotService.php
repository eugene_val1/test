<?php

namespace common\components\bots;

use Longman\TelegramBot\Request;
use Longman\TelegramBot\Telegram;
use Yii;

/**
 * Class TelegramBotService
 * @package common\components
 */
class TelegramBotService extends AbstractBot
{
    const TYPE = 'telegram';

    /**
     * @var Telegram
     */
    private $bot;

    /**
     * @var TelegramBotService
     */
    private static $instance;

    /**
     * BotService constructor
     */
    private function __construct()
    {
        $this->bot = new Telegram(Yii::$app->params[self::TYPE]['botKey'], Yii::$app->params[self::TYPE]['botName']);
    }

    /**
     * Clone
     */
    protected function __clone() {}

    /**
     * @return string
     */
    public function getType()
    {
        return self::TYPE;
    }

    /**
     * @return TelegramBotService
     */
    public static function getInstance()
    {
        if (!isset(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * @return Telegram
     */
    public function getBot()
    {
        return $this->bot;
    }

    /**
     * @param string $target chat ID
     * @param string $message
     * @return bool
     */
    public function sendMessage($target, $message)
    {
        $result = Request::sendMessage([
            'chat_id' => $target,
            'text' => $message,
            'parse_mode' => 'html',
        ]);

        return $result->isOk();
    }
}