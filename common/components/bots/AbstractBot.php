<?php

namespace common\components\bots;
use common\models\UserBots;
use Yii;

/**
 * Interface BotInterface
 * @package common\components\bots
 */
abstract class AbstractBot
{
    const LOG_CATEGORY = 'bots';

    /**
     * @return mixed
     */
    public abstract function getBot();

    /**
     * @return mixed
     */
    public abstract function getType();

    /**
     * @param string $target
     * @param string $message
     * @return boolean
     */
    public abstract function sendMessage($target, $message);

    /**
     * @return string
     */
    public function getSecretHash()
    {
        return Yii::$app->params[$this->getType()]['secretHash'];
    }

    /**
     * @param string $hash
     * @return string
     */
    public function validateSecretHash($hash)
    {
        return $hash === $this->getSecretHash();
    }

    /**
     * @param integer $userId
     * @param string $target
     * @return bool
     */
    public function addUser($userId, $target)
    {
        $user = new UserBots();
        $user->target_token = $target;
        $user->type = $this->getType();
        $user->user_id = $userId;

        return $user->save();
    }

    /**
     * @param integer $userId
     * @return bool
     */
    public function isConnected($userId)
    {
        return UserBots::find()
            ->where([
                'user_id' => $userId,
                'type' => $this->getType()
            ])
            ->exists();
    }

    /**
     * @param integer $userId
     * @return string
     */
    public function buildConnectLink($userId)
    {
        //save to cache
        $userUniqueKey = md5($userId . Yii::$app->params[$this->getType()]['botName']);
        Yii::$app->cache->set($userUniqueKey, $userId, 1800);

        //render a link
        $link = str_replace('[ref]', $userUniqueKey, Yii::$app->params[$this->getType()]['botUrl']);

        return $link;
    }
}