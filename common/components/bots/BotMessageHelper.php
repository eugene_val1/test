<?php

namespace common\components\bots;

use common\models\Timezone;
use common\models\UserBots;
use Yii;
use yii\base\InvalidParamException;
use yii\helpers\Url;

/**
 * Class BotMessageHelper
 *
 * @package common\components\bots
 */
class BotMessageHelper
{
    /**
     * New messages on site
     *
     * @param UserBots $botReceiver
     * @param int $count
     *
     * @throws InvalidParamException
     */
    public static function sendNewMessages(UserBots $botReceiver, $count)
    {
        $url = Url::toRoute('messages/index', 'https');

        if ($botReceiver->type === TelegramBotService::TYPE) {
            $messageText = Yii::t('bots', 'You have new private messages ({0}) - {1}read{2}', [
                $count,
                '<a href="' . $url . '">',
                '</a>'
            ]);
            TelegramBotService::getInstance()
                ->sendMessage($botReceiver->target_token, $messageText);
        }

        if ($botReceiver->type === FacebookBotService::TYPE) {
            $messageText = Yii::t('bots', 'You have new private messages ({0})', [$count]);
             FacebookBotService::getInstance()
                ->sendButtonMessage($botReceiver->target_token, $messageText, $url, Yii::t('bots', 'Read messages'));
        }
    }

    /**
     * Appointment reminder
     *
     * @param UserBots $botReceiver
     * @param string $with
     * @param string $start
     */
    public static function sendAppointmentReminder(UserBots $botReceiver, $with, $start)
    {
        $messageText = Yii::t('bots', 'Your appointment with {0} will start in 24 hours: at {1}', [
            $with,
            $start
        ]);

        self::sendToBots($botReceiver, $messageText);
    }

    /**
     * @param UserBots $userBotCredentials
     * @param array $receiver
     * @param string $start
     *
     * @return bool
     */
    public static function sendAppointmentEvent(UserBots $userBotCredentials, $receiver, $start = '')
    {
        $date = Timezone::convert($start, $receiver['timezone'], 'd.m');
        $dateShort = Timezone::convert($start, $receiver['timezone'], 'H:i');

        $messageText = $receiver['text'];
        if (!empty($start)) {
            $messageText .= ' на ' . $date . ' в ' . $dateShort;
        }

        self::sendToBots($userBotCredentials, $messageText);
    }

    /**
     * @param UserBots $userBotCredentials
     * @param string $messageText
     *
     * @return bool
     */
    public static function sendToBots(UserBots $userBotCredentials, $messageText)
    {
        if ($userBotCredentials->type === TelegramBotService::TYPE) {
            return TelegramBotService::getInstance()
                ->sendMessage($userBotCredentials->target_token, $messageText);
        }

        if ($userBotCredentials->type === FacebookBotService::TYPE) {
            return FacebookBotService::getInstance()
                ->sendMessage($userBotCredentials->target_token, $messageText);
        }

        return false;
    }

    /**
     * @return string
     */
    public static function getDefaultMessage()
    {
        return Yii::t('bots', 'Sorry, but I do not understand you for now. I will just send you all the important updates');
    }
}