<?php

namespace common\components\bots;

use pimax\FbBotApp;
use pimax\Messages\Message;
use pimax\Messages\MessageButton;
use pimax\Messages\StructuredMessage;
use Yii;

/**
 * Class FacebookBotService
 * @package common\components\bots
 */
class FacebookBotService extends AbstractBot
{
    const TYPE = 'facebook';

    /**
     * @var FbBotApp
     */
    private $bot;

    /**
     * @var FacebookBotService
     */
    private static $instance;

    /**
     * BotService constructor
     */
    private function __construct()
    {
        $this->bot = new FbBotApp(Yii::$app->params['facebook']['botKey']);
    }

    /**
     * Clone
     */
    protected function __clone() {}

    /**
     * @return string
     */
    public function getType()
    {
        return self::TYPE;
    }

    /**
     * @return FacebookBotService
     */
    public static function getInstance()
    {
        if (!isset(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * @return FbBotApp
     */
    public function getBot()
    {
        return $this->bot;
    }

    /**
     * @param string $target chat ID
     * @param string $message
     * @return bool
     */
    public function sendMessage($target, $message)
    {
        $this->bot->send(new Message($target, $message));
        return true;
    }

    /**
     * @param string $target chat ID
     * @param string $message
     * @param string $link
     * @param string $linkTitle
     *
     * @return bool
     */
    public function sendButtonMessage($target, $message, $link, $linkTitle)
    {
        $messageButton = new MessageButton(MessageButton::TYPE_WEB, $linkTitle, $link);
        $data = [
            'text' => $message,
            'buttons' => [
                $messageButton,
            ]
        ];

        $structuredButton = new StructuredMessage($target, StructuredMessage::TYPE_BUTTON, $data);
        $this->bot->send($structuredButton);

        return true;
    }
}