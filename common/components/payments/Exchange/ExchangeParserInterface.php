<?php

namespace common\components\payments\Exchange;

/**
 * Interface ExchangeParserInterface
 *
 * @package common\components\payments\Exchange
 */
interface ExchangeParserInterface
{
    /**
     * @param string $currencyTo
     *
     * @return float
     */
    public function getRatio($currencyTo = 'USD');
}