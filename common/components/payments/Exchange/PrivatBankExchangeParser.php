<?php

namespace common\components\payments\Exchange;

use common\models\Therapist;
use Yii;
use yii\base\Component;
use yii\helpers\Url;

/**
 * Class PrivatBankExchangeParser
 *
 * @package common\components\payments\Exchange
 */
class PrivatBankExchangeParser extends Component implements ExchangeParserInterface
{
    const API_URL = 'https://api.privatbank.ua/p24api/pubinfo?exchange&json&coursid=11';

    /**
     * Init
     */
    public function init()
    {
        parent::init();
    }

    /**
     * @inheritdoc
     */
    public function getRatio($currencyTo = 'USD')
    {
        $ctx = stream_context_create(['http' =>
            ['timeout' => 5],  //5 seconds
        ]);

        try {
            $exchange = file_get_contents(self::API_URL, false, $ctx);
        } catch (\Exception $e) {
            $exchange = false;
            Yii::error('bank.gov.ua: ' . $e->getMessage(), 'payments');
        }

        //default ratio
        $ratio = Yii::$app->params['payments']['defaultRatio'];

        if ($exchange === false) {
            Yii::error('Unable to parse exchange API data', 'payments');
            return $ratio;
        }

        /** @var array $exchangeJson */
        $exchangeJson = json_decode($exchange, true);

        if (empty($exchangeJson)) {
            Yii::error('Unable to parse exchange API data', 'payments');
            return $ratio;
        }

        foreach ($exchangeJson as $currencyData) {
            if ($currencyData['ccy'] === $currencyTo) {
                return (double)$currencyData['sale'];
            }
        }

        Yii::error('Unable to parse exchange API data', 'payments');

        return $ratio;
    }
}