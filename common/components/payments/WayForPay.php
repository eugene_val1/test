<?php

namespace common\components\payments;

use common\components\payments\Exchange\ExchangeParserInterface;
use common\components\payments\Exchange\PrivatBankExchangeParser;
use common\models\Appointment;
use common\models\Transaction;
use Yii;
use yii\base\Component;
use yii\base\InvalidParamException;
use yii\helpers\Json;

/**
 * Class WayForPay
 *
 * @package common\components\payments
 */
class WayForPay extends Component
{
    const STATUS_APPROVED = 'Approved';
    const STATUS_PROCESSING = 'InProcessing';
    const STATUS_PENDING = 'Pending';
    const STATUS_DECLINED = 'Declined';
    const STATUS_EXPIRED = 'Expired';
    const STATUS_AUTH_COMPLETE = 'WaitingAuthComplete';
    const STATUS_REFUNDED = 'Refunded';
    const STATUS_VOIDED = 'Voided';

    const REASON_SUCCESS = '1100';

    //inner currency for WayForPay transactions
    const TRANSACTION_CURRENCY = 'UAH';

    /**
     * Prepare options for payment widget.
     *
     * @param Transaction $transaction
     * @param Appointment $appointment
     * @param int $productCount
     *
     * @return array
     */
    public function getPaymentOptions($transaction, $appointment, $productCount = 1)
    {
        $convertedAmounts = json_decode($transaction->converted_amounts, true);
        $fullConvertedAmount = round($convertedAmounts['amount'] + $convertedAmounts['fee'] + $convertedAmounts['payment_fee'], 2);

        $options = [
            //'requestType' => 'VERIFY',
            'merchantAccount' => Yii::$app->settingManager->get('wayforpayMerchantAccount'),
            'merchantDomainName' => Yii::$app->request->hostinfo,
            'merchantAuthType' => 'SimpleSignature',
            'merchantTransactionType' => 'AUTH',
            'orderReference' => $transaction->payment_id,
            'orderDate' => time(),
            'amount' => $fullConvertedAmount,
            'currency' => self::TRANSACTION_CURRENCY,
            'alternativeAmount' => $transaction->getFullAmount(),
            'alternativeCurrency' => $transaction->currency,
            'productName' => $appointment->therapist->name . ' - ' . Yii::t('main', 'Session') . ' #' . $appointment->number,
            'productCount' => $productCount,
            'productPrice' => $fullConvertedAmount,
            'cardHolder' => $appointment->user->name,
            'clientEmail' => $appointment->user->user->email,
        ];

        $options['merchantSignature'] = $this->getRequestSignature($options, $this->keys('widgetRequest'));
        $options['language'] = substr(Yii::$app->language, 0, 2);
        $options['paymentSystems'] = 'card;btc;masterPass;visaCheckout;googlePay;applePay';
        $options['serviceUrl'] = Yii::$app
            ->urlManager
            ->createAbsoluteUrl(['payment/callback', 'id' => $transaction->id]);
        $options['returnUrl'] = Yii::$app
            ->urlManager
            ->createAbsoluteUrl(['account/index', 'tid' => $transaction->id]);

        if (Yii::$app->settingManager->get('useRecToken', false) && !empty($appointment->user->rec_token)) {
            $options['recToken'] = $appointment->user->rec_token;
        }

        return $options;
    }

    /**
     * @param $options
     * @return string
     */
    public function getRequestSignature($options, $keys)
    {
        return $this->getSignature($options, $keys);
    }

    /**
     * @param $options
     * @return string
     */
    public function getResponseSignature($options, $type)
    {
        return $this->getSignature($options, $this->keys($type));
    }

    /**
     * Build signature.
     * @param array $options
     * @return string
     */
    public function getSignature($options, $keys)
    {
        $hash = [];
        foreach ($keys as $key) {
            if (!array_key_exists($key, $options)) {
                throw new InvalidParamException('Missed signature field ' . $key);
            }
            if (is_array($options[$key])) {
                foreach ($options[$key] as $v) {
                    $hash[] = $v;
                }
            } else {
                $hash[] = $options[$key];
            }
        }
        $hash = implode(';', $hash);

        return hash_hmac('md5', $hash, Yii::$app->settingManager->get('wayforpayMerchantSecretKey'));
    }

    /**
     * Is valid payment
     * @param $response
     * @param $type
     * @return bool
     */
    public function isPaymentValid($response, $type)
    {
        if (!isset($response['merchantAccount'], $response['merchantSignature'])) {
            return false;
        }

        if (Yii::$app->settingManager->get('wayforpayMerchantAccount') != $response['merchantAccount']
            || $this->getResponseSignature($response, $type) != $response['merchantSignature']
        ) {
            return false;
        }

        return true;
    }

    /**
     * Responce
     * @param $data
     * @return array
     */
    public function getResponse($data)
    {
        $response = [
            'orderReference' => $data['orderReference'],
            'status' => 'accept',
            'time' => time(),
        ];
        $response['signature'] = hash_hmac('md5', implode(';', $response), Yii::$app->settingManager->get('wayforpayMerchantSecretKey'));

        return $response;
    }

    /**
     * Keys
     * @param $method
     * @return array|mixed
     */
    public function keys($method)
    {
        $keys = [
            'widgetRequest' => [
                'merchantAccount',
                'merchantDomainName',
                'orderReference',
                'orderDate',
                'amount',
                'currency',
                'productName',
                'productCount',
                'productPrice',
            ],
            'widgetResponse' => [
                'merchantAccount',
                'orderReference',
                'amount',
                'currency',
                'authCode',
                'cardPan',
                'transactionStatus',
                'reasonCode',
            ],
            'addPartnerRequest' => [
                'merchantAccount',
                'partnerCode',
                'phone',
                'email',
            ],
            'editPartnerRequest' => [
                'merchantAccount',
                'partnerCode',
            ],
            'settleRequest' => [
                'merchantAccount',
                'orderReference',
                'amount',
                'currency',
            ],
            'settleResponse' => [
                'merchantAccount',
                'orderReference',
                'transactionStatus',
                'reasonCode',
            ],
        ];

        return isset($keys[$method]) ? $keys[$method] : [];
    }

    /**
     * Api urls
     *
     * @param $method
     * @return mixed|string
     */
    public function urls($method)
    {
        $urls = [
            'addPartnerRequest' => 'https://api.wayforpay.com/mms/addPartner.php',
            'editPartnerRequest' => 'https://api.wayforpay.com/mms/updatePartner.php',
            'settleRequest' => 'https://api.wayforpay.com/api',
        ];

        return isset($urls[$method]) ? $urls[$method] : '';
    }

    /**
     * Request
     *
     * @param $method
     * @param $data
     * @return array
     */
    public function request($method, $data)
    {
        $data['merchantAccount'] = Yii::$app->settingManager->get('wayforpayMerchantAccount');
        $data['merchantSignature'] = $this->getSignature($data, $this->keys($method));
        $data_string = Json::encode($data);

        $ch = curl_init($this->urls($method));
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string),
            ]
        );

        $raw = curl_exec($ch);
        $error = curl_error($ch);
        curl_close($ch);

        $response = [];
        try {
            $response = Json::decode($raw);
        } catch (\Exception $exception) {
            Yii::info($data, 'payments');
            Yii::info($raw, 'payments');
            Yii::error($exception->getMessage(), 'payments');
            $error = Yii::t('main', 'Unable to parse server json response');
        }

        if ($error !== '') {
            return [
                'status' => false,
                'error' => $error,
            ];
        }

        if ($response['reasonCode'] == self::REASON_SUCCESS) {
            return [
                'status' => true,
                'response' => $response,
            ];
        }

        return [
            'status' => false,
            'error' => $response['reason'],
        ];
    }

    /**
     * @return ExchangeParserInterface
     */
    public static function getExchangeParser()
    {
        return new PrivatBankExchangeParser();
    }

    /**
     * USD to UAH
     *
     * @param $amount
     * @param null $ratio
     *
     * @return float
     */
    public static function convert($amount, $ratio = null)
    {
        $ratio = !empty($ratio) ? $ratio : self::getExchangeParser()->getRatio();

        return round($amount * $ratio, 2);
    }
}