<?php

namespace common\components;

/**
 * Class TagDependencyHelper
 * @package common\components
 */
class TagDependencyHelper
{
    const UPDATED_THERAPISTS = 'tag-updated-therapists';
    const UPDATED_PAGE = 'tag-updated-page';
    const UPDATED_SLIDE = 'tag-updated-slide';
    const UPDATED_FAQ = 'tag-updated-faq';
    const UPDATED_ARTICLES = 'tag-updated-articles';
}