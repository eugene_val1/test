<?php

namespace common\components;

use yii\base\Component;
use yii\helpers\ArrayHelper;

/**
 * Class SettingManager
 * @package common\components\SettingManager
 */
class SettingManager extends Component
{
    /**
     * @var string
     */
    public $cachePrefix = '_Setting';
    /**
     * @var int
     */
    public $cachingDuration = 60;
    /**
     * @var string
     */
    public $modelClass = 'common\models\Setting';

    /**
     * @var array Runtime values cache
     */
    private $_values = [];

    /**
     * @param $key
     * @param $value
     * @return mixed
     */
    public function set($key, $value)
    {
        $this->_values[$key] = $value;
        $model = $this->getModel($key);
        if (!$model) $model = new $this->modelClass;
        $model->key = $key;
        $model->value = $value;
        return $model->save();
    }

    /**
     * @param array $values
     */
    public function setAll(array $values)
    {
        foreach ($values as $key => $value) {
            $this->set($key, $value);
        }
    }

    /**
     * @param $key
     * @param null $default
     * @param bool $cache
     * @return mixed|null
     */
    public function get($key, $default = null, $cache = true)
    {
        if ($cache) {
            $cacheKey = sprintf('%s.%s', $this->cachePrefix, $key . \Yii::$app->language);
            $value = ArrayHelper::getValue($this->_values, $key, false)
                ? ArrayHelper::getValue($this->_values, $key, false)
                : $value = \Yii::$app->cache->get($cacheKey);
            if ($value === false) {
                $model = $this->getModel($key);
                if ($model) {
                    $value = $model->value;
                    $this->_values[$key] = $value;
                    \Yii::$app->cache->set($cacheKey, $value, $this->cachingDuration);
                } else {
                    $value = $default;
                }
            }
        } else {
            $model = $this->getModel($key);
            $value = $model ? $model->value : $default;
        }
        return $value;
    }

    /**
     * @param array $keys
     * @return array
     */
    public function getAll(array $keys)
    {
        $values = [];
        foreach ($keys as $key) {
            $values[$key] = $this->get($key);
        }
        return $values;
    }

    /**
     * @param $key
     * @return mixed
     */
    public function remove($key)
    {
        return call_user_func($this->modelClass . '::deleteAll', ['key' => $key]);
    }

    /**
     * @param array $keys
     */
    public function removeAll(array $keys)
    {
        foreach ($keys as $key) {
            $this->remove($key);
        }
    }

    /**
     * @param $groupKey
     * @return mixed
     */
    public function removeGroup($groupKey)
    {
        return call_user_func($this->modelClass . '::deleteAll', ['like', 'key', $groupKey]);
    }

    /**
     * @param $key
     * @return mixed
     */
    protected function getModel($key)
    {
        $query = call_user_func($this->modelClass . '::find');
        return $query->where(['key' => $key])->one();
    }
}