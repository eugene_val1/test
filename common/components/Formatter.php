<?php

namespace common\components;

use Yii;
use yii\helpers\Html;

/**
 * Class Formatter
 * @package common\components
 */
class Formatter extends \yii\i18n\Formatter
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        if (!Yii::$app->user->isGuest && Yii::$app->user->identity->timezone !== null) {
            $this->timeZone = Yii::$app->user->identity->timezone;
        }

        $this->nullDisplay = Yii::t('main', 'Not set');
    }

    /**
     * @param string $value
     * @return string
     */
    public function asNtext($value)
    {
        if ($value === null || $value === '') {
            return $this->nullDisplay;
        }
        return nl2br(Html::encode($value));
    }
}