<?php

namespace common\components;

use common\models\Therapist;
use yii\base\Component;
use yii\helpers\Url;

/**
 * Class StructuredData
 * @package common\components
 */
class StructuredData extends Component
{
    /**
     * Init
     */
    public function init()
    {
        parent::init();
    }

    /**
     * @return string
     */
    public static function getJson()
    {
        $data = [
            [
                "@context" => "http://schema.org",
                "@type" => "Organization",
                "name" => "TreatField",
                "alternateName" => "TreatField. Online therapy",
                "url" => Url::base(true),
                "logo" => Url::base(true) . "/img/logo-hd.png",
                "sameAs" => [
                    "https://www.facebook.com/treatfield",
                    "https://www.linkedin.com/company-beta/18137104/",
                    "https://plus.google.com/+Treatfield"
                ]
            ]
        ];

        $therapists = self::getTherapistCarousel();
        if (!empty($therapists['itemListElement'])) {
            $data[] = $therapists;
        }

        return json_encode($data, JSON_UNESCAPED_SLASHES);
    }

    /**
     * Render Therapist Carousel
     * @return array
     */
    public static function getTherapistCarousel()
    {
        $therapists = Therapist::getDb()->cache(function ($db) {
            return Therapist::find()
                ->all();
        }, 3600 * 24);

        $result = [
            "@context" => "http://schema.org",
            "@type" => "ItemList",
            "itemListElement" => []
        ];

        foreach ($therapists as $key => $therapist) {
            $result['itemListElement'][] = [
                '@type' => 'ListItem',
                'position' => $key + 1,
                'name' => $therapist->name,
                'url' => Url::base(true) . '/therapists?alias=' . $therapist->alias,
                'image' => Url::base(true) . '/therapists/catalog-image?id=' . $therapist->user_id,
            ];
        }

        return $result;
    }
}