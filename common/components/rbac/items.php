<?php
return [
    'user' => [
        'type' => 1,
        'ruleName' => 'userRole',
    ],
    'editor' => [
        'type' => 1,
        'ruleName' => 'userRole',
    ],
    'admin' => [
        'type' => 1,
        'ruleName' => 'userRole',
        'children' => [
            'editor',
            'user',
        ],
    ],
    'therapist' => [
        'type' => 1,
        'ruleName' => 'userRole',
    ],
];
