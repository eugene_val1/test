<?php

namespace common\components\rbac;

use Yii;
use yii\rbac\Rule;
use common\models\User;

/**
 * Class UserRoleRule
 * @package common\components\rbac
 */
class UserRoleRule extends Rule
{
    public $name = 'userRole';

    /**
     * @param int|string $user
     * @param \yii\rbac\Item $item
     * @param array $params
     * @return bool
     */
    public function execute($user, $item, $params)
    {
        if (!\Yii::$app->user->isGuest) {
            $role = \Yii::$app->user->identity->role;
            if ($item->name === 'admin') {
                return $role == User::ROLE_ADMIN;
            } elseif ($item->name === 'editor') {
                return $role == User::ROLE_ADMIN || $role == User::ROLE_EDITOR;
            } elseif ($item->name === 'user') {
                return $role == User::ROLE_ADMIN || $role == User::ROLE_USER || User::ROLE_EDITOR;
            } elseif ($item->name === 'therapist') {
                return $role == User::ROLE_THERAPIST;
            }
        }
        return false;
    }
}