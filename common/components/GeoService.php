<?php

namespace common\components;

use Yii;

/**
 * Class GeoService
 *
 * @package common\components
 */
class GeoService
{
    /**
     * @var string
     */
    protected $userIP;

    /**
     * @var string
     */
    protected $accessKeyParam = 'access_key';

    /**
     * @var int
     */
    protected $format = 1;

    /**
     * @var array
     */
    protected $info = [];

    /**
     * GeoService constructor.
     * @param string $userIP
     */
    public function __construct($userIP)
    {
        $this->userIP = $userIP;
        $this->init();
    }

    /**
     * @return array
     */
    public function getInfo()
    {
        return $this->info;
    }

    /**
     * @param bool $log
     * @return mixed|null
     */
    public function getCountryCode($log = true)
    {
        if (empty($this->info['country_code'])) {
            if ($log) {
                Yii::info([
                    'message' => 'Empty country code',
                    'data' => $this->info
                ], 'geo');
            }

            return null;
        }

        return $this->info['country_code'];
    }

    /**
     * @param bool $log
     * @return mixed|null
     */
    public function getTimezone($log = true)
    {
        if (empty($this->info['time_zone'])) {
            if ($log) {
                Yii::info([
                    'message' => 'Empty timezone',
                    'data' => $this->info
                ], 'geo');
            }

            return null;
        }

        return $this->info['time_zone'];
    }

    /**
     * @return bool
     */
    public function isRussia()
    {
        return strtolower($this->getCountryCode()) === 'ru';
    }

    /**
     * @return string
     */
    protected function getUrl()
    {
        $url = Yii::$app->params['geoApi']['url'] . DIRECTORY_SEPARATOR;
        $url .= $this->userIP;
        $url .= '?' . $this->accessKeyParam . '=' . Yii::$app->params['geoApi']['accessToken'];
        $url .= '&format=' . $this->format;

        return $url;
    }

    /**
     * @return array|mixed
     */
    protected function init()
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->getUrl());
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CONNECTION_TIMEOUT, 5);
        $response = curl_exec($ch);
        curl_close($ch);

        if ($response === false) {
            Yii::info('Can not get geo info for ' . $this->userIP, 'geo');
        }

        $result = json_decode($response, true);

        if (!is_array($result)) {
            Yii::info([
                'message' => 'Wrong response',
                'data' => $result
            ], 'geo');

            return [];
        }

        if (isset($result['success']) && $result['success'] === false) {
            Yii::info([
                'message' => 'Request error',
                'data' => $result
            ], 'geo');

            return [];
        }

        $this->info = $result;

        return $result;
    }
}