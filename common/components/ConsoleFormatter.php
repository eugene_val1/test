<?php

namespace common\components;

use common\models\User;
use Yii;
use yii\helpers\Html;

/**
 * Class ConsoleFormatter
 * @package common\components
 */
class ConsoleFormatter extends \yii\i18n\Formatter
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        $this->nullDisplay = Yii::t('main', 'Not set');
    }

    /**
     * @param string $value
     * @return string
     */
    public function asNtext($value)
    {
        if ($value === null || $value === '') {
            return $this->nullDisplay;
        }
        return nl2br(Html::encode($value));
    }
}