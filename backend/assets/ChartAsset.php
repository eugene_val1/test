<?php
namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Class ChartAsset
 * @package backend\assets
 */
class ChartAsset extends AssetBundle
{
    /**
     * @var string
     */
    public $basePath = '@webroot';

    /**
     * @var string
     */
    public $baseUrl = '@web';

    /**
     * @var array
     */
    public $css = [
        'css/chart/chart.min.css',
    ];

    /**
     * @var array
     */
    public $js = [
        'js/plugins/chart/chart.min.js',
        'js/chart.js',
    ];
}
