<?php
namespace backend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class RedactorAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'redactor/plugins/clips/clips.css',
    ];
    public $js = [
        'redactor/plugins/clips/clips.js',
        'redactor/plugins/lineheight/lineheight.js',
        'redactor/plugins/fontfamily/fontfamily.js',
    ];
    public $depends = [
        'vova07\imperavi\Asset'
    ];
}
