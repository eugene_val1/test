/**
 * Step
 * @param int max
 * @returns {number}
 */
function getStep(max) {
    if (max < 50) {
        return 2;
    } else if (max < 100) {
        return 4;
    } else if (max < 200) {
        return 15;
    } else if (max < 500) {
        return 30;
    } else if (max < 1000) {
        return 50;
    } else {
        return 100;
    }
}

/**
 * @param data string
 */
function userCountriesChart(data) {
    var ctx = document
        .getElementById('chart-countries')
        .getContext('2d');

    data = JSON.parse(data);

    var totalUsersCount = data.datasets[0].data.reduce(function (a, b) {
        return a + b;
    }, 0);

    new Chart(ctx, {
        type: 'horizontalBar',
        data: data,
        options: {
            title: {
                display: true,
                text: 'Страны зарегистрированных пользователей (пользователей - ' + totalUsersCount + ', стран - ' + data.labels.length + ')'

            },
            legend: {
                display: false
            },
            scales: {
                yAxes: [{
                    scaleLabel: {
                        display: true
                    }
                }],
                xAxes: [{
                    ticks: {
                        min: 0,
                        stepSize: getStep(Math.max(...data.datasets[0].data))
                    },
                    scaleLabel: {
                        display: true
                    }
                }]
            }
        }
    })
    ;
}

/**
 * @param data string
 */
function paidUsersCountriesChart(data) {
    var ctx = document
        .getElementById('paid-users-chart-countries')
        .getContext('2d');

    data = JSON.parse(data);

    var totalUsersCount = data.datasets[0].data.reduce(function (a, b) {
        return a + b;
    }, 0);

    new Chart(ctx, {
        type: 'horizontalBar',
        data: data,
        options: {
            title: {
                display: true,
                text: 'Страны пользователей, которые хотя бы раз платили (пользователей - ' + totalUsersCount + ', стран - ' + data.labels.length + ')'

            },
            legend: {
                display: false
            },
            scales: {
                yAxes: [{
                    scaleLabel: {
                        display: true
                    }
                }],
                xAxes: [{
                    ticks: {
                        min: 0,
                        stepSize: getStep(Math.max(...data.datasets[0].data))
                    },
                    scaleLabel: {
                        display: true
                    }
                }]
            }
        }
    })
    ;
}

/**
 * @param data string
 */
function appointmentAmountRangeChart(data) {
    var ctx = document
        .getElementById('appointment-amount-range')
        .getContext('2d');

    data = JSON.parse(data);

    new Chart(ctx, {
        type: 'bar',
        data: data,
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        min: 0,
                        stepSize: getStep(Math.max(...data.datasets[0].data))
                    }
                }],
                xAxes: [{
                    scaleLabel: {
                        display: true
                    }
                }]
            }
        }
    })
    ;
}

/**
 * @param data string
 */
function appointmentCountRangeChart(data) {
    var ctx = document
        .getElementById('appointment-count-range')
        .getContext('2d');

    data = JSON.parse(data);

    new Chart(ctx, {
        type: 'bar',
        data: data,
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        min: 0,
                        stepSize: getStep(Math.max(...data.datasets[0].data))
                    }
                }],
                xAxes: [{
                    scaleLabel: {
                        display: true
                    }
                }]
            }
        }
    })
    ;
}
