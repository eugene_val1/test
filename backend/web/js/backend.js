function updateType(type) {
    if (type) {
        $.ajax({
            type: 'POST',
            url: "/home/type",
            dataType: "html",
            data: {type: type},
            success: function (data) {
                $('#slide-type').html(data);
            }
        });
    } else {
        $('#slide-type').html('');
    }
}

function parseSourceId(link) {
    var url = $('#video-url').val();
    var type = $('#video-source option:selected').val()
    
        $.ajax({
            type: 'POST',
            url: "/article/parse-video-url",
            cashe: false,
            dataType: "json",
            data: {url: url, type: type},
            success: function(data) {
                if (data.sourceId) {
                   $('#video-info').show().find('.inner').html(data.video);
                   $('#thumb-info').show().find('.inner').html(data.thumb);
                } else {
                    alert($('#video-url').attr('data-message'));
                }
            }
        });
  
}

$(document).on("click", "a[data-target='#modal']", function(ev) {
    ev.preventDefault();
    var target = $(this).attr("href");
    var title = $(this).attr("data-title");
    
    appendModal(title);

    $.ajax({
        url: target,
        success: function (response) {
            if (response.result) {
                $("#modal .modal-body").html(response.html);
            }
            $("#modal").modal("show");
        }
    });
});

function appendModal(title){
    if ($('#modal').size() === 0)
    {
        var modal = '<div class="modal fade" id="modal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">' +
                        '<div class="modal-dialog">' +
                            '<div class="modal-content">' +
                                '<div class="modal-header">' +
                                    '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
                                    '<h4 class="modal-title">' + title + '</h4>' +
                                '</div>' +
                                '<div class="modal-body"></div>' +
                            '</div>' +
                        '</div>' +
                    '</div>';
            
        $('body').append(modal);
    }
}

$(document).on("click", "#modal .btn-submit", function(ev) {
    ev.preventDefault();
    var form = $(this).closest("form");
    sendForm(form);
});

$(document).on("click", "#modal .btn-cancel", function(ev) {
    ev.preventDefault();
    $('#modal').modal('hide');
});

function sendForm(form) {
    var reload = true;
    var callback = form.attr('data-callback');
    
    $.ajax({
        url: form.attr('action'),
        type: 'post',
        data: form.serialize(),
        success: function (response) {            
            if(response.errors){
                $("#modal .modal-body").html(response.html);
                reload = false;
                callback = false;
            } else if(response.message !== false) {
                $("#modal .modal-body").html('<div class="alert alert-success">' + response.message + '</div>');
                reload = false;
            }
            
            if (typeof window[callback] === 'function'){
                window[callback](form, response.data);
            } else if (reload) {
                location.reload();
            }   
        }
    });
}

function updateList(form, data) {
    $('#modal').modal('hide');    
    $(data.selector).append($('<option>', {
        value: data.id,
        text: data.name,
        selected: 'selected'
    }));
}

$(document).on("click", "a[data-target='#ajax']", function (ev) {
    ev.preventDefault();    
    requestUrl($(this));
});

function requestUrl(link, title) {
    var url = link.attr("href");
    var title = link.attr("data-title");
    var callback = link.attr('data-callback');

    $.ajax({
        url: url,
        type: 'post',
        success: function (response) {
            if(response.message !== false){               
                appendModal(title);
                $("#modal .modal-body").html(response.message);
                $("#modal").modal("show");
            } else if (typeof window[callback] === 'function'){
                window[callback](link, response);
            } else {
                location.reload();
            }   
        }
    });
}

$('.grid-check-all').on('ifChecked', function(event){
    $('.grid-check-item').iCheck('check');
});

$('.grid-check-all').on('ifUnchecked', function(event){
    $('.grid-check-item').iCheck('uncheck');
});

$(document).on("click", ".delete-selected", function (ev) {
    ev.preventDefault();
    var url = $(this).attr("href");
    var rows = $('#backend-grid').yiiGridView('getSelectedRows');
    if(rows.length){
        $.ajax({
            url: url,
            type: 'post',
            data: {rows: rows},
            success: function (response) {
                location.reload();
            }
        });
    }
});