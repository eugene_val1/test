<?php

namespace backend\widgets;

use common\models\Article;
use common\models\Contact;
use common\models\Feedback;
use common\models\Therapist;
use common\models\TherapistReview;
use Yii;
use yii\helpers\Url;

/**
 * Class Sidebar
 * @package backend\widgets
 */
class Sidebar extends \yii\bootstrap\Widget
{
    /**
     * @var array
     */
    public $menuItems = [];

    /**
     * Init
     */
    public function init()
    {
        $counters = [];

        $counters['articles'] = Article::find()
            ->where(['published' => Article::STATUS_PENDING])
            ->count();

        $counters['therapists'] = Therapist::find()
            ->where(['publish_status' => Therapist::STATUS_WAITING])
            ->count();

        $counters['reviews'] = TherapistReview::find()
            ->where(['published' => TherapistReview::STATUS_DRAFT])
            ->count();

        $counters['feedback'] = Feedback::find()
            ->where(['published' => Feedback::STATUS_DRAFT])
            ->count();

        $counters['contacts'] = Contact::find()
            ->where(['read' => Contact::STATUS_UNREAD])
            ->andWhere(['not in', 'theme_id', (new Contact)->customThemes()])
            ->count();

        $counters['consultations'] = Contact::find()
            ->where([
                'read' => Contact::STATUS_UNREAD,
                'theme_id' => Contact::TYPE_CONSULTATION,
            ])
            ->count();

        $counters['contactsAll'] = $counters['contacts'] + $counters['consultations'];

        $this->menuItems = $this->getItems($counters);
    }

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('sidebar/view', [
            'menuItems' => $this->menuItems,
        ]);
    }

    /**
     * @param $item
     * @return bool
     */
    public function isActive($item)
    {
        if ((isset($item['controller']) && $item['controller'] == Yii::$app->controller->id)
            || (isset($item['href']) && $item['href'] == Yii::$app->getRequest()->url)
        ) {
            return true;
        }

        if (!isset($item['subitems'])) {
            return false;
        }

        foreach ($item['subitems'] as $subitem) {
            if ($subitem['href'] == Yii::$app->getRequest()->url || $subitem['controller'] == Yii::$app->controller->id) {
                return true;
            }
        }
    }

    /**
     * @param $counters
     * @return array
     */
    public function getItems($counters)
    {
        return [
            [
                'title' => Yii::t('backend', 'Dashboard'),
                'icon' => 'fa fa-tachometer',
                'controller' => 'site',
                'href' => Url::toRoute('site/index'),
                'visible' => Yii::$app->user->can('editor'),
            ],
            [
                'title' => Yii::t('backend', 'Articles'),
                'icon' => 'fa fa-newspaper-o',
                'counter' => $counters['articles'],
                'controller' => 'article',
                'href' => Url::toRoute('article/index'),
                'visible' => Yii::$app->user->can('editor'),
            ],
            [
                'title' => Yii::t('backend', 'Post'),
                'icon' => 'fa fa-pencil-square',
                'visible' => Yii::$app->user->can('editor'),
                'subitems' => [
                    [
                        'title' => Yii::t('backend', 'Article'),
                        'controller' => 'article',
                        'href' => Url::toRoute(['article/create', 'type' => 'text']),
                    ],
                    [
                        'title' => Yii::t('backend', 'Video'),
                        'controller' => 'article',
                        'href' => Url::toRoute(['article/create', 'type' => 'video']),
                    ],
                    [
                        'title' => Yii::t('backend', 'Treat'),
                        'controller' => 'article',
                        'href' => Url::toRoute(['article/create', 'type' => 'treat']),
                    ],
                ]
            ],
            [
                'title' => Yii::t('backend', 'System news'),
                'icon' => 'fa fa-info-circle',
                'visible' => Yii::$app->user->can('admin'),
                'subitems' => [
                    [
                        'title' => Yii::t('backend', 'News'),
                        'controller' => 'system-news',
                        'href' => Url::toRoute(['system-news/index']),
                    ],
                    [
                        'title' => Yii::t('backend', 'Post'),
                        'controller' => 'system-news',
                        'href' => Url::toRoute(['system-news/create']),
                    ],
                ]
            ],
            [
                'title' => Yii::t('backend', 'Home page'),
                'icon' => 'fa fa-home',
                'visible' => Yii::$app->user->can('editor'),
                'subitems' => [
                    [
                        'title' => Yii::t('backend', 'All slides'),
                        'controller' => 'home',
                        'href' => Url::toRoute('home/index'),
                    ],
                    [
                        'title' => Yii::t('backend', 'Create slide'),
                        'controller' => 'home',
                        'href' => Url::toRoute('home/create'),
                    ],
                    [
                        'title' => Yii::t('backend', 'Features'),
                        'controller' => 'feature',
                        'href' => Url::toRoute('feature/index'),
                    ],
                ]
            ],
            [
                'title' => Yii::t('backend', 'Menu'),
                'icon' => 'fa fa-bars',
                'controller' => 'menu',
                'href' => Url::toRoute('menu/index'),
                'visible' => Yii::$app->user->can('admin'),
            ],
            [
                'title' => Yii::t('backend', 'Therapists'),
                'icon' => 'fa fa-user',
                'counter' => $counters['therapists'] + $counters['reviews'],
                'visible' => Yii::$app->user->can('editor'),
                'subitems' => [
                    [
                        'title' => Yii::t('backend', 'All therapists'),
                        'controller' => 'therapist',
                        'counter' => $counters['therapists'],
                        'href' => Url::toRoute('therapist/index'),
                    ],
                    [
                        'title' => Yii::t('backend', 'Create therapist'),
                        'controller' => 'therapist',
                        'href' => Url::toRoute('therapist/create'),
                    ],
                    [
                        'title' => Yii::t('backend', 'Therapist reviews'),
                        'controller' => 'review',
                        'counter' => $counters['reviews'],
                        'href' => Url::toRoute('review/index'),
                    ],
                    [
                        'title' => Yii::t('backend', 'Appointments'),
                        'controller' => 'appointment',
                        'href' => Url::toRoute('appointment/index'),
                    ],
                    [
                        'title' => Yii::t('backend', 'Clients'),
                        'controller' => 'client',
                        'href' => Url::toRoute('client/index'),
                    ],
                    [
                        'title' => Yii::t('backend', 'Properties'),
                        'controller' => 'property',
                        'href' => Url::toRoute('property/index'),
                    ],
                ]
            ],
            [
                'title' => Yii::t('backend', 'Video rooms'),
                'icon' => 'fa fa-video-camera',
                'controller' => 'room',
                'href' => Url::toRoute('room/index'),
                'visible' => Yii::$app->user->can('admin'),
            ],
            [
                'title' => Yii::t('backend', 'Pages'),
                'icon' => 'fa fa-book',
                'controller' => 'page',
                'href' => Url::toRoute('page/index'),
                'visible' => Yii::$app->user->can('editor'),
            ],
            [
                'title' => Yii::t('backend', 'FAQ'),
                'icon' => 'fa fa-question',
                'controller' => 'faq-category',
                'href' => Url::toRoute('faq-category/index'),
                'visible' => Yii::$app->user->can('editor'),
            ],
            [
                'title' => Yii::t('backend', 'Transactions'),
                'icon' => 'fa fa-money',
                'controller' => 'transaction',
                'href' => Url::toRoute('transaction/index'),
                'visible' => Yii::$app->user->can('admin'),
            ],
            [
                'title' => Yii::t('backend', 'Reviews'),
                'icon' => 'fa fa-smile-o',
                'controller' => 'feedback',
                'counter' => $counters['feedback'],
                'href' => Url::toRoute('feedback/index'),
                'visible' => Yii::$app->user->can('admin'),
            ],
            [
                'title' => Yii::t('backend', 'Events'),
                'icon' => 'fa fa-exclamation',
                'controller' => 'event',
                'href' => Url::toRoute('event/index'),
                'visible' => Yii::$app->user->can('admin'),
            ],
            [
                'title' => Yii::t('backend', 'Contact messages'),
                'icon' => 'fa fa-envelope',
                'counter' => $counters['contactsAll'],
                'visible' => Yii::$app->user->can('admin'),
                'subitems' => [
                    [
                        'title' => Yii::t('backend', 'All contact messages'),
                        'controller' => 'contact',
                        'counter' => $counters['contacts'],
                        'href' => Url::toRoute('contact/index'),
                    ],
                    [
                        'title' => Yii::t('backend', 'Free consultations'),
                        'controller' => 'contact',
                        'counter' => $counters['consultations'],
                        'href' => Url::toRoute('contact/consultation'),
                    ],
                    [
                        'title' => Yii::t('backend', 'Contact themes'),
                        'controller' => 'contact-theme',
                        'href' => Url::toRoute('contact-theme/index'),
                    ],
                ]
            ],
            [
                'title' => Yii::t('backend', 'Users'),
                'icon' => 'fa fa-child',
                'visible' => Yii::$app->user->can('admin'),
                'subitems' => [
                    [
                        'title' => Yii::t('backend', 'All users'),
                        'controller' => 'user',
                        'href' => Url::toRoute('user/index'),
                    ],
                    [
                        'title' => Yii::t('backend', 'Create user'),
                        'controller' => 'user',
                        'href' => Url::toRoute('user/create'),
                    ],
                    [
                        'title' => Yii::t('backend', 'Regions'),
                        'controller' => 'region',
                        'href' => Url::toRoute('region/index'),
                    ],
                    [
                        'title' => Yii::t('backend', 'Countries'),
                        'controller' => 'country',
                        'href' => Url::toRoute('country/index'),
                    ],
                    [
                        'title' => Yii::t('backend', 'Timezones'),
                        'controller' => 'timezone',
                        'href' => Url::toRoute('timezone/index'),
                    ],
                    [
                        'title' => Yii::t('backend', 'Dispatch'),
                        'controller' => 'dispatch',
                        'href' => Url::toRoute('dispatch/index'),
                    ],
                    [
                        'title' => Yii::t('backend', 'Export'),
                        'controller' => 'export',
                        'href' => Url::toRoute('user/export'),
                    ],
                ]
            ],
            [
                'title' => Yii::t('backend', 'SEO'),
                'icon' => 'fa fa-line-chart',
                'visible' => Yii::$app->user->can('admin'),
                'subitems' => [
                    [
                        'title' => Yii::t('backend', 'Robots.txt'),
                        'controller' => 'seo',
                        'href' => Url::toRoute('seo/robots'),
                    ],
                ]
            ],
            [
                'title' => Yii::t('backend', 'Statistics'),
                'icon' => 'fa fa-bar-chart',
                'visible' => Yii::$app->user->can('admin'),
                'subitems' => [
                    [
                        'title' => Yii::t('backend', 'General'),
                        'controller' => 'stat',
                        'href' => Url::toRoute('statistics/index'),
                    ],
                    [
                        'title' => Yii::t('backend', 'Clients'),
                        'controller' => 'stat',
                        'href' => Url::toRoute('statistics/clients'),
                    ]
                ]
            ],
            [
                'title' => Yii::t('backend', 'Localization'),
                'icon' => 'fa fa-bullhorn',
                'visible' => Yii::$app->user->can('editor'),
                'subitems' => [
                    [
                        'title' => Yii::t('backend', 'Languages'),
                        'controller' => 'language',
                        'href' => Url::toRoute('language/index'),
                    ],
                    [
                        'title' => Yii::t('backend', 'Messages'),
                        'controller' => 'message',
                        'href' => Url::toRoute('translation-message/index'),
                    ],
                ]
            ],
            [
                'title' => Yii::t('backend', 'Settings'),
                'icon' => 'fa fa-cogs',
                'controller' => 'setting',
                'href' => Url::toRoute('setting/index'),
                'visible' => Yii::$app->user->can('admin'),
            ],
        ];
    }
}