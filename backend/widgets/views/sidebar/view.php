<ul class="sidebar-menu">
    <?php foreach ($menuItems as $item): ?>
        <?php if ($item['visible']): ?>
            <li class="<?= ($this->context->isActive($item)) ? 'active' : ''; ?> <?= (isset($item['subitems'])) ? 'treeview' : ''; ?>">
                <a  href="<?php echo (!isset($item['subitems']) || count($item['subitems']) == 0) ? $item['href'] : 'javascript:;'; ?>" >
                    <i class="<?= $item['icon']; ?>"></i>
                    <span><?= $item['title']; ?></span>
                    <?php if(isset($item['counter']) && $item['counter'] > 0):?>
                        <small class="badge pull-right bg-yellow"><?= $item['counter'] ?></small>
                    <?php endif; ?>
                </a>
                <?php if (isset($item['subitems'])): ?>
                    <ul class="treeview-menu">
                        <?php foreach ($item['subitems'] as $subitem): ?>
                            <li <?php echo ($this->context->isActive($subitem)) ? 'class="active"' : ''; ?>>
                                <a  href="<?= $subitem['href']; ?>">
                                    <i class="fa fa-angle-double-right"></i>
                                    <?= $subitem['title']; ?>
                                    <?php if(isset($subitem['counter']) && $subitem['counter'] > 0):?>
                                        <small class="badge pull-right bg-green"><?= $subitem['counter'] ?></small>
                                    <?php endif; ?>
                                </a>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                <?php endif; ?>
            </li>
        <?php endif; ?>
    <?php endforeach; ?>
</ul>
