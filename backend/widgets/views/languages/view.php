<?php

use yii\helpers\Html;
?>

<li class="dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
        <?= '<img src="' . $current->getFileUrl('image') . '" alt=""> ' .  $current->name; ?> <span class="caret"></span>
    </a>
    <ul class="dropdown-menu">
        <?php foreach ($langs as $lang): ?>
            <li>
                <?= Html::a('<img src="' . $lang->getFileUrl('image') . '" alt=""> ' . $lang->name, '/' . $lang->url . Yii::$app->getRequest()->getUrl()) ?>
            </li>
        <?php endforeach; ?>
    </ul>
</li>    