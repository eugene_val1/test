<?php

use yii\helpers\Html;
use common\models\Article;
use common\widgets\Alert;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = Html::encode($model->title);
?>
<div class="row">
    <div class="col-lg-12 clearfix">
        <div style="margin-bottom: 15px" class="pull-right">
            <?= Html::a('<i class="fa fa-backward"></i> ' . Yii::t('backend', 'Back'), Url::previous('articles'), ['class' => 'btn btn-success btn-sm']) ?>
            <?= Html::a('<i class="fa fa-pencil"></i> ' . Yii::t('backend', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary btn-sm']) ?>
            <?=
            Html::a('<i class="fa fa-trash-o"></i> ' . Yii::t('backend', 'Delete'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger btn-sm',
                'data' => [
                    'confirm' => Yii::t('backend', 'Delete element?'),
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
        <div style="margin-bottom: 15px" class="pull-left">
            <?php
            if($model->published == Article::STATUS_PUBLISHED){
                echo Html::a('<i class="fa fa-close"></i> ' . Yii::t('backend', 'Unpublish'), ['unpublish', 'id' => $model->id], [
                    'class' => 'btn btn-danger btn-sm',
                    'data' => [
                        'confirm' => Yii::t('backend', 'Unpublish this article?'),
                        'method' => 'post',
                    ],
                ]);
            } else {
                echo Html::a('<i class="fa fa-check"></i> ' . Yii::t('backend', 'Publish'), ['publish', 'id' => $model->id], [
                    'class' => 'btn btn-success btn-sm',
                    'data' => [
                        'confirm' => Yii::t('backend', 'Publish this article?'),
                        'method' => 'post',
                    ],
                ]);
                echo Html::a('<i class="fa fa-close"></i> ' . Yii::t('backend', 'Report errors'), ['send', 'id' => $model->id], [
                    'class' => 'btn btn-danger btn-sm',
                    'style' => 'margin-left: 3px',
                    'data' => [
                        'target' => '#modal',
                        'title' => Yii::t('backend', 'Report errors')
                    ],
                ]);
            }
        ?>
        </div>
    </div>
    <div class="col-lg-12"><?= Alert::widget(); ?></div>
    <div class="col-lg-12">
        <?php if($model->type): ?>
            <?= $this->render('_view_' . $model->type, ['model' => $model]); ?>
        <?php endif; ?>
    </div>
</div>