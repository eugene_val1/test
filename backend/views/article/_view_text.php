<?php
use yii\helpers\Html;
?>

<div class="col-lg-9">
    <h2><?= Html::encode($model->title); ?></h2>
    <?= $model->content; ?>
</div>
<div class="col-lg-3">
    <h4><?= Yii::t('main', 'Image') ?></h4>
    <img id="article-image" src="<?= $model->getFileUrl('image'); ?>" alt="" class="img-responsive full-width-img">
    <h4><?= Yii::t('main', 'Category') ?></h4>
    <?= $model->category ? Html::encode($model->category->name) : Yii::t('main', 'Not set'); ?>
    <h4><?= Yii::t('main', 'Short description') ?></h4>
    <?= Html::encode($model->short_description); ?>    
    <?php if($model->tags): ?>
        <h4><?= Yii::t('main', 'Tags') ?></h4>
        <div class="tags">
            <?php foreach($model->tags as $tag): ?>
            <a href="javascript:void(0)"><?= $tag->name ?></a>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>
</div>