<?php

use backend\models\ArticleSearch;
use common\models\Article;
use kartik\widgets\DatePicker;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\helpers\Html;

/** @var Article $model */
/** @var yii\web\View $this */
/** @var backend\models\HomeSlideSearch $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */
/** @var ArticleSearch $searchModel */

$this->title = Yii::t('backend', 'Articles');

?>

<div class="row mt">
    <div class="col-lg-12">
        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'tableOptions' => ['class' => 'table table-striped table-advance table-hover'],
            'columns' => [
                'title',
                [
                    'attribute' => 'user_name',
                    'value' => function ($model) {
                        return $model->user->profile->name;
                    },
                ],
                [
                    'attribute' => 'type',
                    'filter' => ArticleSearch::getTypes(),
                    'value' => function ($model) {
                        return $model->getTypeName();
                    },
                ],
                [
                    'attribute' => 'created',
                    'filter' => DatePicker::widget([
                        'model' => $searchModel,
                        'attribute' => 'created',
                        'type' => 1,
                        'pluginOptions' => [
                            'autoclose' => true,
                            'format' => 'dd.mm.yyyy',
                        ]
                    ]),
                    'format' => 'html',
                    'value' => function ($model) {
                        return date('d.m.Y', $model->created);
                    },
                ],
                [
                    'attribute' => 'published',
                    'filter' => ArticleSearch::getPublishStatuses(),
                    'format' => 'html',
                    'value' => function ($model) {
                        $html = '';
                        if ($model->published === Article::STATUS_PUBLISHED) {
                            $html = '<span class="has-success"><i class="help-block fa fa-check"></i></span>';
                        } else if ($model->published === Article::STATUS_REVIEWED) {
                            $html = '<span class="has-error"><i class="help-block fa fa-exclamation-triangle"></i></span>';
                        } else {
                            $html = '<span class="has-warning"><i class="help-block fa fa-question-circle"></i></span>';
                        }
                        return $html . ' ' . $model->publishStatusName;
                    },
                ],
                [
                    'attribute' => 'featured',
                    'filter' => ArticleSearch::getFeaturedStatuses(),
                    'format' => 'raw',
                    'contentOptions' => ['class' => 'status-column'],
                    'value' => function ($model) {
                        /** @var Article $model */

                        $html = '';

                        if ($model->published === Article::STATUS_PUBLISHED) {
                            if ($model->featured == Article::NOT_FEATURED) {
                                $html .= Html::a('<i class="status fa fa-star"></i> ' . $model->featuredStatusName, ['feature', 'id' => $model->id], [
                                    'class' => 'btn btn-success btn-xs status-button',
                                    'title' => Yii::t('backend', 'Feature'),
                                ]);
                            } else {
                                $html .= Html::a('<i class="status fa fa-close"></i> ' . $model->featuredStatusName, ['unfeature', 'id' => $model->id], [
                                    'class' => 'btn btn-danger btn-xs status-button',
                                    'title' => Yii::t('backend', 'Unfeature'),
                                ]);
                            }
                        }

                        return $html;
                    },
                ],
                [
                    'class' => ActionColumn::class,
                    'template' => '{view} {update} {delete}',
                    'buttons' => [
                        'view' => function ($url, $model, $key) {
                            return Html::a('<i class="fa fa-eye"></i>', $url, [
                                'class' => 'btn btn-success btn-sm',
                                'style' => 'margin-top: 2px;',
                                'data-pjax' => 0,
                            ]);
                        },
                        'update' => function ($url, $model, $key) {
                            return Html::a('<i class="fa fa-pencil"></i>', $url, [
                                'class' => 'btn btn-primary btn-sm',
                                'style' => 'margin-top: 2px;',
                                'data-pjax' => 0,
                            ]);
                        },
                        'delete' => function ($url, $model, $key) {
                            return Html::a('<i class="fa fa-trash-o "></i>', $url, [
                                'class' => 'btn btn-danger btn-sm',
                                'style' => 'margin-top: 2px;',
                                'data' => [
                                    'pjax' => 0,
                                    'method' => 'post',
                                    'confirm' => Yii::t('backend', 'Delete element?'),
                                ],
                            ]);
                        }
                    ],
                ],
            ],
        ]);
        ?>

    </div>
</div>
