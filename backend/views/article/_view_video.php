<?php

use yii\helpers\Html;

?>

<div class="col-lg-7">
    <h2><?= Html::encode($model->title); ?></h2>
    <?= $model::getVideoCode($model->video_source, $model->video_source_id) ?>
</div>

<div class="col-lg-5">
    <h4><?= Yii::t('main', 'Image') ?></h4>
    <img id="article-image" src="<?= $model->getFileUrl('image'); ?>" alt="" class="img-responsive">
    <h4><?= Yii::t('main', 'Short description') ?></h4>
    <?= Html::encode($model->short_description); ?>
    <h4><?= Yii::t('main', 'Content') ?></h4>

    <?= $model->content; ?>

    <?php if ($model->tags): ?>
        <h4><?= Yii::t('main', 'Tags') ?></h4>
        <div class="tags">
            <?php foreach ($model->tags as $tag): ?>
                <a href="javascript:void(0)"><?= $tag->name ?></a>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>

</div>