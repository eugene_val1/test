<?php
use yii\helpers\Html;
?>

<div class="col-lg-12">
    <h2><?= Html::encode($model->title); ?></h2>
    <?= $model->content; ?>
    <?php if($model->tags): ?>
        <h4><?= Yii::t('main', 'Tags') ?></h4>
        <div class="tags">
            <?php foreach($model->tags as $tag): ?>
            <a href="javascript:void(0)"><?= $tag->name ?></a>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>
</div>