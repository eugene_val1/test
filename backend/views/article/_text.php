<?php

use backend\assets\RedactorAsset;
use common\models\CategoryPage;
use common\models\Language;
use common\models\TextArticle;
use common\models\TherapistLang;
use kartik\widgets\Select2;
use vova07\imperavi\Widget;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;

/** @var TextArticle $model */

?>

<?= $form->field($model, 'title')->textInput() ?>
<?= $form->field($model, 'user_id')->dropDownList(ArrayHelper::map(TherapistLang::find()->where(['language' => Yii::$app->language])->all(), 'user_id', 'name'), ['prompt' => Yii::t('backend', 'Select author')]); ?>
<?= $form->field($model, 'category_id')->dropDownList(ArrayHelper::map(CategoryPage::find()->with('translation')->all(), 'id', 'name'), ['prompt' => '']); ?>

<?php if ($model->cover_image): ?>
    <div class="form-group">
        <label class="control-label"><?= Yii::t('backend', 'Current cover image'); ?></label>
        <div>
            <?= Html::img($model->getFileUrl('cover_image'), ['class' => 'img-thumbnail', 'alt' => '', 'width' => '320px']); ?>
        </div>
    </div>
<?php endif; ?>
<?= $form->field($model, 'cover_imageFile')->fileInput() ?>
<?= $form->field($model, 'cover_image')->hiddenInput()->label(false); ?>

<?php if ($model->image): ?>
    <div class="form-group">
        <label class="control-label"><?= Yii::t('backend', 'Current image'); ?></label>
        <div>
            <?= Html::img($model->getFileUrl('image'), ['class' => 'img-thumbnail', 'alt' => '', 'width' => '320px']); ?>
        </div>
    </div>
<?php endif; ?>
<?= $form->field($model, 'imageFile')->fileInput() ?>
<?= $form->field($model, 'image')->hiddenInput()->label(false); ?>
<?= $form->field($model, 'image_alt')->textInput(); ?>
<?= $form->field($model, 'short_description')->textarea() ?>
<?=
$form->field($model, 'content')->widget(Widget::className(), [
    'settings' => [
        'lang' => Language::getCurrent()->url,
        'minHeight' => 200,
        'imageUpload' => Url::to(['/redactor/image-upload']),
        'imageManagerJson' => Url::to(['/redactor/images-get']),
        'formattingAdd' => [
            'fullwidth' => [
                'title' => Yii::t('main', 'Fullwidth text'),
                'tag' => 'p',
                'class' => 'fullwidth',
            ],
            'fullwidth-h' => [
                'title' => Yii::t('main', 'Fullwidth title'),
                'tag' => 'h1',
                'class' => 'fullwidth',
            ],
        ],
        'plugins' => [
            'fullscreen',
            'imagemanager',
            'fontcolor',
            'video',
            'fontfamily',
            'fontsize',
            'definedlinks',
        ]
    ],
    'plugins' => [
        'lineheight' => RedactorAsset::class,
        'clips' => RedactorAsset::class,
    ]
]);
?>

<?= $form->field($model, 'tagList')->widget(Select2::classname(), [
    'options' => ['multiple' => true, 'id' => 'article-tags'],
    'data' => $model->getTagList(),
    'showToggleAll' => false,
    'pluginLoading' => false,
    'pluginOptions' => [
        'minimumInputLength' => 3,
        'ajax' => [
            'url' => Url::toRoute(['article/tags']),
            'dataType' => 'json',
            'data' => new JsExpression('function(params) { return {q:params.term}; }')
        ],
        'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
        'templateResult' => new JsExpression('function(tag) { return tag.text; }'),
        'templateSelection' => new JsExpression('function (tag) { return tag.text; }'),
        'tags' => true,
        'maximumInputLength' => 25,
    ],
]);
?>