<?php

use backend\assets\RedactorAsset;
use yii\helpers\Url;
use kartik\widgets\Select2;
use yii\web\JsExpression;
use common\models\TherapistLang;
use yii\helpers\ArrayHelper;
use \vova07\imperavi\Widget;
use common\models\Language;

?>

<?= $form->field($model, 'user_id')
    ->dropDownList(
        ArrayHelper::map(TherapistLang::find()->where(['language' => Yii::$app->language])->all(), 'user_id', 'name'),
        ['prompt' => Yii::t('backend', 'Select author')]
    );
?>
<?=
    $form->field($model, 'content')->widget(Widget::className(), [
        'settings' => [
            'lang' => Language::getCurrent()->url,
            'minHeight' => 200,
            'imageUpload' => Url::to(['/redactor/image-upload']),
            'imageManagerJson' => Url::to(['/redactor/images-get']),
            'formattingAdd' => [
                'fullwidth' => [
                    'title' => Yii::t('main', 'Fullwidth text'),
                    'tag' => 'p',
                    'class' => 'fullwidth'
                ],
                'fullwidth-h' => [
                    'title' => Yii::t('main', 'Fullwidth title'),
                    'tag' => 'h1',
                    'class' => 'fullwidth'
                ],
            ],
            'plugins' => [
                'fullscreen',
                'imagemanager',
                'fontcolor',
                'video',
                'fontfamily',
                'fontsize',
                'definedlinks'
            ]
        ],
        'plugins' => [
            'lineheight' => RedactorAsset::class,
            'clips' => RedactorAsset::class
        ]
    ]);
?>

<?=
    $form->field($model, 'tagList')->widget(Select2::classname(), [
        'options' => ['multiple' => true, 'id' => 'article-tags'],
        'data' => $model->getTagList(),
        'showToggleAll' => false,
        'pluginLoading' => false,
        'pluginOptions' => [
            'minimumInputLength' => 3,
            'ajax' => [
                'url' => Url::toRoute(['article/tags']),
                'dataType' => 'json',
                'data' => new JsExpression('function(params) { return {q:params.term}; }')
            ],
            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
            'templateResult' => new JsExpression('function(tag) { return tag.text; }'),
            'templateSelection' => new JsExpression('function (tag) { return tag.text; }'),
            'tags' => true,
            'maximumInputLength' => 25,
        ],
    ]);
?>