<?php

use backend\assets\RedactorAsset;
use yii\helpers\Html;
use yii\helpers\Url;
use \vova07\imperavi\Widget;
use kartik\widgets\Select2;
use yii\web\JsExpression;
use common\models\TherapistLang;
use yii\helpers\ArrayHelper;
use common\models\Language;
use common\models\VideoArticle;

/** @var VideoArticle $model */

?>

<?= $form->field($model, 'title')->textInput() ?>
<?= $form->field($model, 'short_description')->textarea() ?>
<?= $form->field($model, 'user_id')->dropDownList(ArrayHelper::map(TherapistLang::find()->where(['language' => Yii::$app->language])->all(), 'user_id', 'name'), ['prompt' => Yii::t('backend', 'Select author')]); ?>
<?= $form->field($model, 'video_source')->dropDownList($model->getSources(), ['id' => 'video-source']); ?>
<?= $form->field($model, 'videoUrl')->textInput(['id' => 'video-url', 'data-message' => Yii::t('backend', 'Unable to find video. Check video source and url!')]) ?>
    <div class="form-group">
        <a onclick="parseSourceId()" class="btn btn-sm btn-success"><i
                    class="fa fa-question"></i> <?= Yii::t('backend', 'Check link'); ?></a>
    </div>

    <div id="video-info" <?= ($model->videoCode) ? '' : 'style="display: none;"' ?> class="form-group">
        <label class="control-label"><?= Yii::t('backend', 'Current video'); ?></label>
        <div class="inner"><?= $model->videoCode; ?></div>
    </div>

    <div id="thumb-info" <?= ($model->image) ? '' : 'style="display: none;"' ?> class="form-group">
        <label class="control-label"><?= Yii::t('backend', 'Current image'); ?></label>
        <div class="inner">
            <?= ($model->image) ? Html::img($model->getFileUrl('image'), ['class' => 'img-thumbnail', 'alt' => "", 'width' => '320px']) : ''; ?>
        </div>
    </div>

<?= $form->field($model, 'imageFile')->fileInput() ?>
<?= $form->field($model, 'image')->hiddenInput()->label(false); ?>
<?= $form->field($model, 'image_alt')->textInput()->label(Yii::t('backend', 'Video image description')); ?>
<?=
$form->field($model, 'content')->widget(Widget::className(), [
    'settings' => [
        'lang' => Language::getCurrent()->url,
        'minHeight' => 200,
        'imageUpload' => Url::to(['/redactor/image-upload']),
        'imageManagerJson' => Url::to(['/redactor/images-get']),
        'plugins' => [
            'fullscreen',
            'imagemanager',
            'fontcolor',
            'video',
            'fontfamily',
            'fontsize',
            'definedlinks',
        ]
    ],
    'plugins' => [
        'lineheight' => RedactorAsset::class,
        'clips' => RedactorAsset::class,
    ]
]);
?>

<?=
$form->field($model, 'tagList')->widget(Select2::classname(), [
    'options' => ['multiple' => true, 'id' => 'article-tags'],
    'data' => $model->getTagList(),
    'showToggleAll' => false,
    'pluginLoading' => false,
    'pluginOptions' => [
        'minimumInputLength' => 3,
        'ajax' => [
            'url' => Url::toRoute(['article/tags']),
            'dataType' => 'json',
            'data' => new JsExpression('function(params) { return {q:params.term}; }')
        ],
        'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
        'templateResult' => new JsExpression('function(tag) { return tag.text; }'),
        'templateSelection' => new JsExpression('function (tag) { return tag.text; }'),
        'tags' => true,
        'maximumInputLength' => 25,
    ],
]);
?>