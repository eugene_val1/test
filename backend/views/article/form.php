<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

/** @var \common\models\Article $model title */

$this->title = $model->isNewRecord
    ? Yii::t('backend', 'Create article')
    : Yii::t('backend', 'Update Article') . ': ' . $model->title;

?>

<div class="row">
    <div class="col-lg-12">
        <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

        <?php if($model->type): ?>
            <?= $this->render('_' . $model->type, ['form' => $form,'model' => $model]); ?>
        <?php endif; ?>

        <?php if ($model->preview_image): ?>
            <div class="form-group">
                <label class="control-label"><?= Yii::t('backend', 'Current preview image'); ?></label>
                <div>
                    <?= Html::img($model->getFileUrl('preview_image'), ['class' => 'img-thumbnail', 'alt' => '', 'width' => '320px']); ?>
                </div>
            </div>
        <?php endif; ?>
        <?= $form->field($model, 'preview_imageFile')->fileInput() ?>
        <?= $form->field($model, 'preview_image')->hiddenInput()->label(false); ?>

        <?= $form->field($model, 'alias')->textInput() ?>
        <?= $form->field($model, 'page_title')->textInput() ?>
        <?= $form->field($model, 'page_keywords')->textarea() ?>
        <?= $form->field($model, 'page_description')->textarea() ?>
        <?= $form->field($model, 'published')->dropDownList($model->getPublishStatuses()); ?>
        <?= $form->field($model, 'noindex')->checkbox(); ?>
        <?= $form->field($model, 'nofollow')->checkbox(); ?>

        <div class="form-group">
        <?= Html::submitButton('<i class="fa fa-save"></i> ' . ($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Save')), ['class' => $model->isNewRecord ? 'btn btn-success btn-sm' : 'btn btn-primary btn-sm']) ?>
            <a href="<?= Url::toRoute('index'); ?>" class="btn btn-danger btn-sm"><i class="fa fa-backward"></i> <?= Yii::t('backend', 'Cancel'); ?></a>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>