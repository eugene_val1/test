<?php
/* @var $this yii\web\View */
/* @var $searchModel backend\models\ClientSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

use kartik\widgets\DatePicker;
use yii\grid\GridView;

$this->title = Yii::t('backend', 'Clients');
?>
<div class="row">
    <div class="col-lg-12">
        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'tableOptions' => ['class' => 'table table-striped table-advance table-hover'],
            'columns' => [
                [
                    'label' => Yii::t('backend', 'Therapist'),
                    'attribute' => 'therapist_name',
                    'value' => function ($model) {
                        return $model->therapistLang ? $model->therapistLang->name : '';
                    },
                ],
                [
                    'label' => Yii::t('backend', 'User'),
                    'attribute' => 'user_name',
                    'value' => function ($model) {
                        return $model->user ? $model->user->name : '';
                    },
                ],
                [
                    'label' => Yii::t('backend', 'Email'),
                    'attribute' => 'email',
                    'value' => function ($model) {
                        return $model->innerUser ? $model->innerUser->email : '';
                    },
                ],
                [
                    'attribute' => 'created',
                    'filter' => DatePicker::widget([
                        'model' => $searchModel,
                        'attribute' => 'created',
                        'type' => 1,
                        'pluginOptions' => [
                            'autoclose' => true,
                            'format' => 'dd.mm.yyyy',
                        ],
                    ]),
                    'format' => 'html',
                    'value' => function ($model) {
                        return Yii::$app->formatter->asDate($model->created, 'medium')
                            . ' ' . Yii::$app->formatter->asTime($model->created, 'short');
                    },
                ],
                [
                    'attribute' => 'status',
                    'filter' => $searchModel->getStatuses(),
                    'value' => function ($model) {
                        return $model->statusName;
                    },
                ]
            ],
        ]);
        ?>
    </div>
</div>