<?php
/* @var $this yii\web\View */
/* @var $model common\models\Language */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\SettingLang;
use common\models\Setting;
use common\widgets\Alert;

$this->title = Yii::t('backend', 'Settings');
?>
<div class="row">
    <div class="col-lg-12">
        <?= Alert::widget(); ?>
        <?php $form = ActiveForm::begin(); ?>
        <ul class="nav nav-tabs" role="tablist">
            <?php foreach(Setting::getCategories() as $category => $categoryName):?>
            <li role="presentation" <?= ($category == 'main') ? 'class="active"' : '' ?>><a href="#<?= $category ?>" aria-controls="<?= $category ?>" role="tab" data-toggle="tab"><?= $categoryName ?></a></li>
            <?php endforeach; ?>
        </ul>
        <div class="tab-content">
            <?php foreach(Setting::getCategories() as $category => $categoryName):?>
            <div role="tabpanel" class="tab-pane <?= ($category == 'main') ? 'active' : '' ?>" id="<?= $category ?>">
                <table class="table table-striped table-advance table-hover">
                    <?php foreach ($settings[$category] as $id => $setting): ?>
                        <tr>    
                            <td width="30%">
                                <?= Yii::t('backend', $setting->title) ?>
                            </td>
                            <td>
                                <?php
                                if(is_a($setting, SettingLang::className())){
                                    echo $form->field($setting, '[' . $id . ']content', ['template' => '{input}{error}'])->textarea(['rows' => 5]);
                                } else {
                                    $field = $form->field($setting, '[' . $id . ']value');
                                    $field->template = "{input}\n{error}";
                                    if ($setting->type == 'string'){
                                        echo $field->textInput();
                                    } elseif ($setting->type == 'text') {
                                        echo $field->textArea(['rows' => 5]);
                                    } else {
                                        echo $field->checkBox([],false);
                                    }
                                }
                                ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </table>
            </div>
            <?php endforeach; ?>
         </div>        
        <div class="form-group">
            <?= Html::submitButton('<i class="fa fa-save"></i> ' . Yii::t('backend', 'Save'), ['class' => 'btn btn-success btn-sm']) ?>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>