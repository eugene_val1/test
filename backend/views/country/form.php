<?php
/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $form yii\widgets\ActiveForm */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\RegionLang;

$this->title = $model->isNewRecord ? Yii::t('backend', 'Create country') : Yii::t('backend', 'Update country') . ': ' . $model->name;
?>

<div class="row">
    <div class="col-lg-12">
        <?php $form = ActiveForm::begin(); ?>
        <?= $form->field($translation, 'name')->textInput() ?>        
        <?= $form->field($model, 'region_id')->dropDownList(ArrayHelper::map(RegionLang::find()->where(['language' => Yii::$app->language])->all(), 'region_id', 'name'),['prompt'=>'']); ?>
        <?= $form->field($model, 'code')->textInput() ?>
        <div class="form-group">
            <?= Html::submitButton('<i class="fa fa-save"></i> ' . ($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Save')), ['class' => $model->isNewRecord ? 'btn btn-success btn-sm' : 'btn btn-primary btn-sm']) ?>
            <a href="<?= Url::toRoute('index'); ?>" class="btn btn-danger btn-sm"><i class="fa fa-backward"></i> <?= Yii::t('backend', 'Cancel'); ?></a>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>