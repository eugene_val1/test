<?php
/* @var $this yii\web\View */
/* @var $searchModel backend\models\FaqCategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

use yii\helpers\Html;
use yii\helpers\Url;
use himiklab\sortablegrid\SortableGridView;
use common\widgets\Alert;

$this->title = Yii::t('backend', 'FAQ categories');
?>

<div class="row mt">
    <div class="col-lg-10 col-lg-offset-1"><?= Alert::widget(); ?></div>
    <div class="col-lg-12">
        <div class="pull-right">
            <a href="<?= Url::toRoute(['create', 'parent_id' => ($parent) ? $parent->id : 0]); ?>" class="btn btn-success btn-sm pull-right">
                <i class="fa fa-plus"></i> <?= Yii::t('backend', 'Create'); ?>
            </a>
            <?php if($parent): ?>
            <a href="<?= Url::toRoute(['index', 'id' => $parent->parent_id]); ?>" style="margin-right: 5px;" class="btn btn-default btn-sm pull-right">
                <i class="fa fa-level-up"></i> <?= Yii::t('backend', 'Back'); ?>
            </a>
            <?php endif; ?>
        </div>
        <?=
        SortableGridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'tableOptions' => ['class' => 'table table-striped table-advance table-hover'],
            'columns' => [
                'name',
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{subcategories} {questions} {update} {delete}',
                    'buttons' => [
                        'subcategories' => function ($url, $model, $key) {
                            return Html::a('<i class="fa fa-level-down"></i> ' . Yii::t('backend', 'Subcategories'), ['faq-category/index', 'id' => $model->id], [
                                'class' => 'btn btn-default btn-sm',
                                'data-pjax' => 0,
                            ]);
                        },
                        'questions' => function ($url, $model, $key) {
                            return Html::a('<i class="fa fa-eye"></i> ' . Yii::t('backend', 'Questions'), ['faq/index', 'id' => $model->id], [
                                'class' => 'btn btn-success btn-sm',
                                'data-pjax' => 0,
                            ]);
                        },
                        'update' => function ($url, $model, $key) {
                            return Html::a('<i class="fa fa-pencil"></i> ' . Yii::t('backend', 'Edit'), $url, [
                                'class' => 'btn btn-primary btn-sm',
                                'data-pjax' => 0,
                            ]);
                        },
                        'delete' => function ($url, $model, $key) {
                            return Html::a('<i class="fa fa-trash-o "></i> ' . Yii::t('backend', 'Delete'), $url, [
                                'class' => 'btn btn-danger btn-sm',
                                'data' => ['pjax' => 0, 'method' => 'post', 'confirm' => Yii::t('backend', 'Delete category with all nested categories and questions?')],
                            ]);
                        }
                    ],
                ],
            ],
        ]);
        ?>

    </div>
</div>
