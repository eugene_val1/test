<?php
/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $form yii\widgets\ActiveForm */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$this->title = $model->isNewRecord ? Yii::t('backend', 'Create FAQ category') : Yii::t('backend', 'Update FAQ category') . ': ' . $model->name;
?>

<div class="row">
    <div class="col-lg-12">
        <?php $form = ActiveForm::begin(); ?>
        <?= $form->field($model, 'parent_id')->dropDownList($model->getTree(),['prompt' => Yii::t('backend', 'Root')]); ?>
        <?= $form->field($translation, 'name')->textInput() ?>
        <div class="form-group">
            <?= Html::submitButton('<i class="fa fa-save"></i> ' . ($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Save')), ['class' => $model->isNewRecord ? 'btn btn-success btn-sm' : 'btn btn-primary btn-sm']) ?>
            <a href="<?= Url::toRoute(['index', 'id' => $model->parent_id]); ?>" class="btn btn-danger btn-sm"><i class="fa fa-backward"></i> <?= Yii::t('backend', 'Cancel'); ?></a>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>