<?php
use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use backend\widgets\Sidebar;
use common\widgets\Languages;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>"/>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="skin-blue">
        <header class="header">
            <a href="<?= Yii::$app->homeUrl ?>" class="logo">
                Treatfield
            </a>
            <nav class="navbar navbar-static-top" role="navigation">
                <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <div class="navbar-right">
                    <ul class="nav navbar-nav">
                        <li>
                            <a target="_blank" href="<?= Yii::$app->params['siteUrl'] ?>">
                                <i class="fa fa-globe"></i> 
                                <?= Yii::t('backend', 'Browse site') ?>
                            </a>
                        </li>
                        <?php echo Languages::widget(); ?>
                        <?php if (!Yii::$app->user->isGuest):?>
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-user"></i>
                                <span><?= ucfirst(Yii::$app->user->identity->username); ?> <i class="caret"></i></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="user-header bg-light-blue">
                                    <p>
                                        <?= ucfirst(Yii::$app->user->identity->username); ?> - <?= Yii::$app->user->identity->getRoleName(); ?>
                                        <small><?= Yii::t('backend', 'Member since') . ': ' . date('m-d-Y', Yii::$app->user->identity->created); ?></small>
                                    </p>
                                    <p>
                                        <img width="98" src="<?= Yii::$app->user->identity->profile->getFileUrl('image'); ?>" class="img-circle" alt="">
                                    </p>
                                </li>
                                <li class="user-footer">
                                    <div class="pull-right">
                                        <a data-method="post" href="<?php echo Url::toRoute('site/logout'); ?>" class="btn btn-default btn-flat"><?= Yii::t('backend', 'Logout'); ?></a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        <?php endif; ?>
                    </ul>
                </div>
            </nav>
        </header>
        <div class="wrapper row-offcanvas row-offcanvas-left">
            <aside class="left-side sidebar-offcanvas">
                <section class="sidebar">
                    <?php echo Sidebar::widget(); ?>
                </section>
            </aside>
            <aside class="right-side">
                <section class="content-header">
                    <h1>
                        <?= $this->title ?> 
                        <small><?= Yii::t('backend', 'Control panel'); ?></small>
                    </h1>
                    <?= Breadcrumbs::widget([
                        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                    ]) ?>
                </section>
                <section class="content">
                    <?php foreach (Yii::$app->session->getAllFlashes() as $key => $message): ?>
                        <div class="alert alert-<?= $key; ?>">
                            <?= $message; ?>
                        </div>
                    <?php endforeach; ?>
                    <?= $content ?>  
                </section>
            </aside>
        </div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
