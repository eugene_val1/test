<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = Yii::t('main', 'Message from') . ' ' . $model->name;

?>

<div class="row">
    <div class="col-lg-12">
        <div style="margin-bottom: 15px" class="pull-right">
            <?= Html::a('<i class="fa fa-backward"></i> ' . Yii::t('backend', 'Back'), Yii::$app->request->referrer, ['class' => 'btn btn-success btn-sm']) ?>
            <?=
                Html::a('<i class="fa fa-trash-o"></i> ' . Yii::t('backend', 'Delete'), ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger btn-sm',
                    'data' => [
                        'confirm' => Yii::t('backend', 'Delete element?'),
                        'method' => 'post',
                    ],
                ])
            ?>
        </div>
        <?=
        DetailView::widget([
            'model' => $model,
            'attributes' => [
                'name',
                'email:email',
                [
                    'attribute' => 'country_id',
                    'value'=> $model->country->name,
                ],
                [
                    'attribute' => 'created',
                    'format' => 'html',
                    'value'=> date('d.m.Y', $model->created),
                ],
                [
                    'attribute' => 'theme_id',
                    'format' => 'html',
                    'value'=> isset($model->contactTheme)
                        ? $model->contactTheme->title . ($model->theme ? ' - ' . $model->theme : '')
                        : '(Тема удалена)',
                ],
                'message',
            ],
        ])
        ?> 
    </div>
</div>