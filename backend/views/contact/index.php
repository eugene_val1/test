<?php

/** @var $this yii\web\View */
/** @var $searchModel backend\models\ContactSearch */
/** @var $dataProvider yii\data\ActiveDataProvider */

use common\models\Contact;
use common\models\ContactThemeLang;
use common\models\CountryLang;
use kartik\widgets\DatePicker;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

?>

<div class="row">
    <div class="col-lg-12">
        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'tableOptions' => ['class' => 'table table-striped table-advance table-hover'],
            'columns' => [
                'name',
                'email',
                [
                    'attribute' => 'country_id',
                    'filter' => ArrayHelper::map(CountryLang::findAll(['language' => Yii::$app->language]), 'country_id', 'name'),
                    'value' => function ($model) {
                        return $model->country->name;
                    },
                ],
                [
                    'visible' => $searchModel->showThemeColumn,
                    'attribute' => 'theme_id',
                    'filter' => ArrayHelper::map(ContactThemeLang::findAll(['language' => Yii::$app->language]), 'contact_theme_id', 'title'),
                    'value' => function ($model) {
                        if (empty($model->contactTheme)) {
                            return '(Тема удалена)';
                        }

                        return $model->contactTheme->title . ($model->theme ? ' - ' . $model->theme : '');
                    },
                ],
                [
                    'attribute' => 'created',
                    'filter' => DatePicker::widget([
                        'model' => $searchModel,
                        'attribute' => 'created',
                        'type' => 1,
                        'pluginOptions' => [
                            'autoclose' => true,
                            'format' => 'dd.mm.yyyy',
                        ]
                    ]),
                    'format' => 'html',
                    'value' => function ($model) {
                        return date('d.m.Y', $model->created);
                    },
                ],
                [
                    'attribute' => 'read',
                    'filter' => [
                        Contact::STATUS_READ => Yii::t('main', 'Read'),
                        Contact::STATUS_UNREAD => Yii::t('main', 'Unread'),
                    ],
                    'format' => 'html',
                    'value' => function ($model) {
                        return ($model->read > 0) ? '<span class="glyphicon glyphicon-ok text-success"></span>' : '<span class="glyphicon glyphicon-remove text-danger"></span>';
                    },
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{view} {delete}',
                    'buttons' => [
                        'view' => function ($url, $model, $key) {
                            return Html::a('<i class="fa fa-eye"></i>', $url, [
                                'class' => 'btn btn-success btn-sm',
                                'data-pjax' => 0,
                            ]);
                        },
                        'delete' => function ($url, $model, $key) {
                            return Html::a('<i class="fa fa-trash-o "></i>', $url, [
                                'class' => 'btn btn-danger btn-sm',
                                'data' => ['pjax' => 0, 'method' => 'post', 'confirm' => Yii::t('backend', 'Delete element?')],
                            ]);
                        }
                    ],
                ],
            ],
        ]);
        ?>
    </div>
</div>
