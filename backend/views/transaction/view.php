<?php

use common\models\Transaction;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = $model->payment_id;

?>

<div class="row">
    <div class="col-lg-12">
        <div style="margin-bottom: 15px" class="pull-right">
            <?= Html::a('<i class="fa fa-backward"></i> ' . Yii::t('backend', 'Back'), 'index', ['class' => 'btn btn-success btn-sm']) ?>
            <?php if ($model->status == Transaction::STATUS_SUCCESS && $model->settled == Transaction::STATUS_NOT_SETTLED): ?>
                <?= Html::a('<i class="fa fa-check-circle"></i> ' . Yii::t('backend', 'Settle'), ['settle', 'id' => $model->id], [
                        'class' => 'btn btn-primary btn-sm',
                        'title' => Yii::t('backend', 'Settle'),
                        'data-target' => '#ajax']
                ) ?>
            <?php endif; ?>
        </div>
        <?=
        DetailView::widget([
            'model' => $model,
            'attributes' => [
                'payment_id',
                [
                    'attribute' => 'created',
                    'value' => Yii::$app->formatter->asDate($model->created, 'medium') . ' ' . Yii::$app->formatter->asTime($model->created, "short"),
                ],
                [
                    'label' => Yii::t('backend', 'Therapist'),
                    'attribute' => 'therapist_name',
                    'value' => $model->therapistLang
                        ? $model->therapistLang->name
                        : '',
                ],
                [
                    'attribute' => 'status',
                    'value' => $model->statusName,
                ],
                'amount',
                'fee',
                'payment_fee',
                'ratio',
                'converted_amounts',
                [
                    'label' => Yii::t('backend', 'Total'),
                    'attribute' => 'total',
                    'value' => $model->getFullAmount(),
                ],
            ],
        ])
        ?>
    </div>
</div>