<?php

/* @var $this yii\web\View */
/* @var $searchModel backend\models\TaskSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

use common\models\Transaction;
use kartik\widgets\DatePicker;
use yii\grid\GridView;
use yii\helpers\Html;

$this->title = Yii::t('backend', 'Transactions');

?>

<div class="row">
    <div class="col-lg-12">
        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'tableOptions' => ['class' => 'table table-striped table-advance table-hover'],
            'columns' => [
                'payment_id',
                [
                    'attribute' => 'created',
                    'filter' => DatePicker::widget([
                        'model' => $searchModel,
                        'attribute' => 'created',
                        'type' => 1,
                        'pluginOptions' => [
                            'autoclose' => true,
                            'format' => 'dd.mm.yyyy'
                        ]
                    ]),
                    'format' => 'html',
                    'value' => function ($model) {
                        return Yii::$app->formatter->asDate($model->created, 'medium') . ' ' . Yii::$app->formatter->asTime($model->created, "short");
                    },
                ],
                [
                    'label' => Yii::t('backend', 'Therapist'),
                    'attribute' => 'therapist_name',
                    'value' => function ($model) {
                        return $model->therapistLang ? $model->therapistLang->name : '';
                    },
                ],
                [
                    'attribute' => 'status',
                    'filter' => $searchModel->getStatuses(),
                    'value' => function ($model) {
                        return $model->statusName;
                    },
                ],
                'ratio',
                'amount',
                'fee',
                'payment_fee',
                [
                    'label' => Yii::t('backend', 'Total'),
                    'attribute' => 'total',
                    'value' => function ($model) {
                        return $model->getFullAmount();
                    },
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{settle} {view}',
                    'buttons' => [
                        'settle' => function ($url, $model, $key) {
                            if ($model->status == Transaction::STATUS_SUCCESS && $model->settled == Transaction::STATUS_NOT_SETTLED) {
                                return Html::a('<i class="fa fa-check-circle"></i> ' . Yii::t('backend', 'Settle'), $url, [
                                    'class' => 'btn btn-primary btn-sm',
                                    'title' => Yii::t('backend', 'Settle'),
                                    'data-target' => '#ajax',
                                ]);
                            }
                        },
                        'view' => function ($url, $model, $key) {
                            return Html::a('<i class="fa fa-eye"></i> ' . Yii::t('backend', 'View'), $url, [
                                'class' => 'btn btn-success btn-sm',
                                'data-pjax' => 0,
                            ]);
                        }
                    ],
                ]
            ],
        ]);
        ?>
    </div>
</div>