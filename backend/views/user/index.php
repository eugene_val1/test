<?php

use yii\grid\ActionColumn;
use backend\models\UserSearch;
use common\models\User;
use kartik\daterange\DateRangePicker;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

/* @var View $this */
/* @var UserSearch $searchModel */
/* @var ActiveDataProvider $dataProvider */

$this->title = Yii::t('backend', 'Users');
$countries = $searchModel->getCountries();

?>

<div class="row">
    <div class="col-lg-12">

        <div class="pull-right">
            <a href="<?= Url::toRoute('create'); ?>" class="btn btn-success btn-sm pull-right">
                <i class="fa fa-plus"></i> <?= Yii::t('backend', 'Create'); ?>
            </a>
        </div>

        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'tableOptions' => ['class' => 'table table-striped table-advance table-hover'],
            'columns' => [
                'email',
                [
                    'attribute' => 'name',
                    'label' => Yii::t('main', 'Name'),
                    'value' => function ($model) {
                        return $model->profile ? $model->profile->name : '-';
                    },
                ],
                [
                    'attribute' => 'role',
                    'filter' => $searchModel->getRoles(),
                    'value' => function (User $model) {
                        return $model->getRoles()[$model->role];
                    },
                ],
                [
                    'attribute' => 'country_id',
                    'label' => Yii::t('main', 'Country'),
                    'filter' => $countries,
                    'value' => function (User $model) use ($countries) {
                        return $countries[$model->country_id];
                    },
                ],
                [
                    'attribute' => 'created',
                    'label' => Yii::t('main', 'Sign up date'),
                    'format' => 'html',
                    'filter' => DateRangePicker::widget([
                        'model' => $searchModel,
                        'attribute' => 'created_range',
                        'convertFormat' => true,
                        'pluginOptions' => [
                            'locale' => [
                                'format' => 'd.m.Y',
                            ],
                            'autoclose' => true,
                            'opens' => 'left'
                        ],
                        'pluginEvents' => [
                            //'cancel.daterangepicker' => "function() { $('#statsearch-created_range').val(null).daterangepicker('update'); }",
                        ]
                    ]),
                    'value' => function ($model) {
                        return date('d.m.Y', $model->created);
                    },
                ],
                [
                    'attribute' => 'status',
                    'label' => Yii::t('backend', 'Status'),
                    'filter' => $searchModel->getStatuses(),
                    'format' => 'html',
                    'value' => function ($model) {
                        $status = $model->getStatuses()[$model->status];

                        switch ($model->status) {
                            case User::STATUS_DELETED:
                                $label = 'danger';
                                break;
                            case User::STATUS_CREATED:
                                $label = 'warning';
                                break;
                            case User::STATUS_ACTIVE:
                                $label = 'success';
                                break;
                            default:
                                $label = 'success';
                                break;
                        }

                        return "<span class='label label-{$label}'>{$status}</span>";
                    },
                ],
                [
                    'class' => ActionColumn::class,
                    'template' => '{update} {delete} {login}',
                    'buttons' => [
                        'update' => function ($url, $model, $key) {
                            return Html::a(
                                    '<i class="fa fa-pencil"></i> ',
                                    ($model->role == $model::ROLE_THERAPIST)
                                    ? ['therapist/update', 'id' => $model->profile->id]
                                    : $url,
                                [
                                    'class' => 'btn btn-primary btn-sm',
                                    'title' => 'Edit profile',
                                    'style' => 'margin-top:2px;',
                                    'data-pjax' => 0,
                                ]
                            );
                        },
                        'delete' => function ($url, $model, $key) {
                            return Html::a('<i class="fa fa-trash-o"></i>', $url, [
                                'class' => 'btn btn-danger btn-sm',
                                'title' => 'Delete profile',
                                'style' => 'margin-top:2px;',
                                'data' => [
                                    'pjax' => 0,
                                    'method' => 'post',
                                    'confirm' => Yii::t('backend', 'Delete element?'),
                                ],
                            ]);
                        },
                        'login' => function ($url, $model, $key) {
                            return Html::a('<i class="fa fa-sign-in"></i>', '/user/login-as?id=' . $model->id, [
                                'class' => 'btn btn-warning btn-sm',
                                'target' => '_blank',
                                'style' => 'margin-top:2px;',
                                'title' => 'Login as this user',
                            ]);
                        }
                    ],
                ],
            ],
        ]);
        ?>

    </div>
</div>
