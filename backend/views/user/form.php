<?php
/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $form yii\widgets\ActiveForm */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use common\models\CountryLang;
use common\models\Timezone;
use yii\helpers\ArrayHelper;
use kartik\widgets\Select2;

$this->title = $user->isNewRecord ? Yii::t('backend', 'Create user') : Yii::t('backend', 'Update user') . ': ' . $user->username;
?>

<div class="row">
    <div class="col-lg-12">
        <?php $form = ActiveForm::begin(['options' => ['enctype'=>'multipart/form-data']]); ?>
        <?= $form->field($user, 'role')->dropDownList($user->mainRoles); ?>
        <?= $form->field($user, 'username')->textInput() ?>
        <?= $form->field($user, 'email')->textInput() ?>
        <?= $form->field($user, 'user_password')->textInput() ?>
        <?= $form->field($profile, 'name')->textInput() ?>
        <?= $form->field($user, 'country_id')->widget(Select2::classname(), [
            'data' => ArrayHelper::map(CountryLang::findAll(['language' => Yii::$app->language]), 'country_id', 'name'),
            'options' => ['placeholder' => Yii::t('main', 'Select a country')],
        ]); ?>
        <?= $form->field($user, 'timezone')->widget(Select2::classname(), [
            'data' => ArrayHelper::map(Timezone::findAll(['language' => Yii::$app->language]), 'timezone_id', 
                    function($model, $defaultValue) {
                            return $model->name . ' (' . $model->getOffset() . ' )';
                    }),
            'options' => ['placeholder' => Yii::t('main', 'Select a timezone')],
        ]); ?>
        <?php if ($profile->image): ?>
            <div class="form-group">
                <label class="control-label"><?= Yii::t('backend', 'Current image'); ?></label>
                <div>
                    <?= Html::img($profile->getFileUrl('image'), ['class' => 'img-thumbnail', 'alt' => "", 'width' => '128px']); ?>
                </div>
            </div>
        <?php endif; ?>
        <?= $form->field($profile, 'image')->hiddenInput()->label(false); ?>
        <?= $form->field($profile, 'imageFile')->fileInput() ?>
        <div class="form-group">
            <?= Html::submitButton('<i class="fa fa-save"></i> ' . ($user->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Save')), ['class' => $user->isNewRecord ? 'btn btn-success btn-sm' : 'btn btn-primary btn-sm']) ?>
            <a href="<?= Url::toRoute('index'); ?>" class="btn btn-danger btn-sm"><i class="fa fa-backward"></i> <?= Yii::t('backend', 'Cancel'); ?></a>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>