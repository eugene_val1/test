<?php

use kartik\date\DatePicker;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $model \backend\models\UserExportForm */

$this->title = 'Export users';
?>
<div class="row">
    <div class="col-lg-6">
        <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
        <?php
        echo $form->field($model, 'from')->widget(DatePicker::classname(), [
            'options' => ['placeholder' => 'Created From'],
            'pluginOptions' => [
                'autoclose' => true,
                //'format' => 'yyyy.dd.mm',
            ]
        ]);
        ?>
        <?php
        echo $form->field($model, 'to')->widget(DatePicker::classname(), [
            'options' => ['placeholder' => 'Created To'],
            'pluginOptions' => [
                'autoclose' => true,
                //'format' => 'yyyy.dd.mm',
            ]
        ]);
        ?>
        <?= $form->field($model, 'agree_mailing')->checkbox() ?>
        <div class="form-group">
            <?= Html::submitButton('Export', ['class' => 'btn btn-success btn-sm']) ?>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>