<?php

use backend\assets\ChartAsset;
use backend\models\Statistics\MainStatistics;
use backend\models\StatisticsSearch;
use kartik\daterange\DateRangePicker;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\web\View;

ChartAsset::register($this);

/** @var View $this */
/** @var StatisticsSearch $searchModel */
/** @var MainStatistics $mainStatisticsModel */

$this->title = 'Общая статистика';

$totalRegisteredUsers = $mainStatisticsModel->getRegistrationsCount();
$ltv = $mainStatisticsModel->getLTV();
$spl = $mainStatisticsModel->getSPL();
$averagePrice = $mainStatisticsModel->getAverageAppointmentPrice();
$ltvPure = round(($spl-1)*10 + (0.5*$averagePrice), 2);

//render charts
$userCountriesData = $mainStatisticsModel->getCountriesChartData();
$paidUsersCountriesData = $mainStatisticsModel->getPaidUsersCountriesChartData();
$this->registerJs("userCountriesChart('{$userCountriesData}')");
$this->registerJs("paidUsersCountriesChart('{$paidUsersCountriesData}')");

$appointmentAmountRangeData = $mainStatisticsModel->getAppointmentAmountRangeData();
$appointmentCountRangeData = $mainStatisticsModel->getAppointmentCountRangeData();
$this->registerJs("appointmentAmountRangeChart('{$appointmentAmountRangeData}')");
$this->registerJs("appointmentCountRangeChart('{$appointmentCountRangeData}')");

?>

<div class="row">
    <div class="col-lg-6">
        <?php $form = ActiveForm::begin([
            'enableClientValidation' => false,
            'id' => 'search-form',
            'method' => 'get',
        ]); ?>
        <?= $form->field($searchModel, 'created_range', [])
            ->label('Период')
            ->widget(DateRangePicker::classname(), [
                'convertFormat' => true,
                'pluginOptions' => [
                    'locale' => [
                        'format' => 'd.m.Y',
                    ],
                    'autoclose' => true,
                    'opens' => 'left',
                ],
            ]);
        ?>
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group form-submit">
                    <?= Html::submitButton('Применить', ['class' => 'small-page-btn page-btn orange btn-submit', 'name' => 'save-button']) ?>
                    <small>(по умолчанию: от начала текущего месяца до сегодня)</small>
                </div>
            </div>
        </div>
        <?php ActiveForm::end(); ?>

        <br/>
    </div>
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-6">
                <ul class="list-group">
                    <li class="list-group-item">
                        <span class="badge"><?= $searchModel->getNewAppointmentsCount() ?></span>
                        Назначеные сессии
                    </li>
                    <li class="list-group-item">
                        <span class="badge"><?= $searchModel->getCompletedAppointmentsCount() ?></span>
                        Завершенные сессии
                    </li>
                    <li class="list-group-item">
                        <span class="badge"><?= $searchModel->getMissedAppointmentsCount() ?></span>
                        Пропущеные сессии
                    </li>
                </ul>
                <ul class="list-group">
                    <li class="list-group-item">
                        <span class="badge"><?= $totalRegisteredUsers ?></span>
                        Количество регистраций на платформе
                    </li>
                    <li class="list-group-item">
                        <span class="badge"><?= $mainStatisticsModel->getRequestsCount() ?></span>
                        Количество первых запросов на сессию
                    </li>
                    <li class="list-group-item">
                        <span class="badge"><?= $mainStatisticsModel->getFirstPaymentsCount() ?></span>
                        Количество первых оплат
                    </li>
                </ul>
            </div>
            <div class="col-md-6">
                <ul class="list-group">
                    <li class="list-group-item">
                        <span class="badge"><?= $ltv ?></span>
                        <b>LTV</b>
                        <br/>
                        <small>Сумма доходов от всех платящих клиентов вместе / поделить на количество платящих клиентов</small>
                    </li>
                    <li class="list-group-item">
                        <span class="badge"><?= $ltvPure ?></span>
                        <b>LTV pure</b>
                        <br/>
                        <small>(SPL - 1) * 10 + (0.5 * Средний чек)</small>
                    </li>
                    <li class="list-group-item">
                        <span class="badge"><?= $spl?></span>
                        <b>SPL</b>
                        <br/>
                        <small>Среднее количество купленных сессий (на множестве хотя бы раз заплативших клиентов)</small>
                    </li>
                    <li class="list-group-item">
                        <span class="badge"><?= $averagePrice ?></span>
                        <b>Средний чек</b>
                        <br/>
                        <small>В валюте платформы, USD</small>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <canvas id="chart-countries" width="200" height="200"></canvas>
    </div>
</div>

<br/><br/>

<div class="row">
    <div class="col-md-12">
        <canvas id="paid-users-chart-countries" width="200" height="200"></canvas>
    </div>
</div>

<br/><br/>

<div class="row">
    <div class="col-md-12">
        <div class="col-md-6">
            <canvas id="appointment-amount-range" width="200" height="200"></canvas>
        </div>
        <div class="col-md-6">
            <canvas id="appointment-count-range" width="200" height="200"></canvas>
        </div>
    </div>
</div>
