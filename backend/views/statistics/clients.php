<?php

use backend\models\StatisticsSearch;
use common\models\Client;
use kartik\daterange\DateRangePicker;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\web\View;

/** @var View $this */
/** @var StatisticsSearch $searchModel */
/** @var ActiveDataProvider $dataProvider */

$this->title = 'Общая статистика по клиентам';

?>

<div class="row">
    <div class="col-lg-12">
        <?= Html::a(Yii::t('backend', 'Reset filters'), '/statistics/clients', ['class' => 'btn btn-success btn-sm pull-right']) ?>
        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'tableOptions' => ['class' => 'table table-striped table-advance table-hover'],
            'columns' => [
                [
                    'attribute' => 'user_name',
                    'label' => Yii::t('main', 'Name'),
                    'format' => 'html',
                    'value' => function (Client $model) {
                        return '<a href="https://www.treatfield.com/user/profile?id=' . $model->user_id . '" target="_blank">' . $model->user_name . '</a>';
                    }
                ],
                [
                    'attribute' => 'email',
                    'label' => Yii::t('main', 'Email'),
                    'value' => function (Client $model) {
                        return isset($model->innerUser) ? $model->innerUser->email : '-';
                    }
                ],
                [
                    'attribute' => 'therapist',
                    'label' => Yii::t('main', 'Therapist'),
                    'format' => 'html',
                    'value' => function (Client $model) {
                        return '<a href="https://www.treatfield.com/therapists?alias=' . $model->therapist->alias . '" target="_blank">' . $model->therapist->name . '</a>';
                    }
                ],
                [
                    'attribute' => 'created',
                    'label' => Yii::t('main', 'Sign up date'),
                    'format' => 'html',
                    'filter' => DateRangePicker::widget([
                        'model' => $searchModel,
                        'attribute' => 'created_range',
                        'convertFormat' => true,
                        'pluginOptions' => [
                            'locale' => [
                                'format' => 'd.m.Y',
                            ],
                            'autoclose' => true,
                            'opens' => 'left'
                        ],
                        'pluginEvents' => [
                            //'cancel.daterangepicker' => "function() { $('#statsearch-created_range').val(null).daterangepicker('update'); }",
                        ]
                    ]),
                    'value' => function ($model) {
                        return date('d.m.Y', $model->created);
                    },
                ],
                [
                    'attribute' => 'status',
                    'label' => Yii::t('main', 'Status'),
                    'format' => 'html',
                    'filter' => Html::activeDropDownList(
                        $searchModel,
                        'status',
                        (new Client())->getStatuses(),
                        [
                            'class' => 'form-control',
                            'prompt' => Yii::t('backend', 'Select status')
                        ]),
                    'value' => function ($model) {
                        $labelText = '';
                        $labelClass = '';

                        switch ((int)$model->status) {
                            case Client::STATUS_NEW:
                                $labelText = Yii::t('main', 'New');
                                $labelClass = 'primary';
                                break;
                            case Client::STATUS_ACTIVE:
                                $labelText = Yii::t('main', 'Active');
                                $labelClass = 'success';
                                break;
                            case Client::STATUS_PENDING:
                                $labelText = Yii::t('main', 'Pending');
                                $labelClass = 'warning';
                                break;
                            case Client::STATUS_INACTIVE:
                                $labelText = Yii::t('main', 'Inactive');
                                $labelClass = 'danger';
                                break;
                        }

                        return "<span class=\"label label-{$labelClass}\">{$labelText}</span>";
                    },
                ],
            ],
        ]);
        ?>
    </div>
</div>
