<?php

use common\models\Feedback;
use kartik\widgets\DatePicker;
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\SearchFeedback */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Feedbacks on the project');

?>

<div class="row">
    <div class="col-lg-12">
        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'tableOptions' => ['class' => 'table table-striped table-advance table-hover'],
            'columns' => [
                [
                    'attribute' => 'user_name',
                    'value' => function ($model) {
                        return $model->profile ? $model->profile->name : '-';
                    },
                ],
                'text:ntext',
                [
                    'attribute' => 'created',
                    'filter' => DatePicker::widget([
                        'model' => $searchModel,
                        'attribute' => 'created',
                        'type' => 1,
                        'pluginOptions' => [
                            'autoclose' => true,
                            'format' => 'dd.mm.yyyy'
                        ]
                    ]),
                    'format' => 'html',
                    'value' => function ($model) {
                        return date('d.m.Y', $model->created);
                    },
                ],
                [
                    'attribute' => 'published',
                    'filter' => [
                        Feedback::STATUS_PUBLISHED => Yii::t('main', 'Published'),
                        Feedback::STATUS_DRAFT => Yii::t('main', 'Draft'),
                    ],
                    'format' => 'html',
                    'value' => function ($model) {
                        return ($model->published > 0)
                            ? '<span class="glyphicon glyphicon-ok text-success"></span>'
                            : '<span class="glyphicon glyphicon-remove text-danger"></span>';
                    },
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{update} {delete}',
                    'buttons' => [
                        'update' => function ($url, $model, $key) {
                            return Html::a('<i class="fa fa-pencil"></i> ', $url, [
                                'class' => 'btn btn-primary btn-sm',
                                'data-pjax' => 0,
                            ]);
                        },
                        'delete' => function ($url, $model, $key) {
                            return Html::a('<i class="fa fa-trash-o "></i>', $url, [
                                'class' => 'btn btn-danger btn-sm',
                                'data' => [
                                    'pjax' => 0,
                                    'method' => 'post',
                                    'confirm' => Yii::t('backend', 'Delete element?'),
                                ],
                            ]);
                        }
                    ],
                ],
            ],
        ]);
        ?>
    </div>
</div>
