<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\SystemNews */

$this->title = 'Update System News: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'System News', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="system-news-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
