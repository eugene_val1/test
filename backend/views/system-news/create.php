<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\SystemNews */

$this->title = 'Create System News';
$this->params['breadcrumbs'][] = ['label' => 'System News', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="system-news-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
