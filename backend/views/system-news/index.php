<?php

use kartik\daterange\DateRangePicker;
use yii\grid\ActionColumn;
use common\models\SystemNews;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\SystemNewsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'System News';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="system-news-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create System News', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                'id',
                [
                    'attribute' => 'target',
                    'filter' => SystemNews::getTargets(),
                    'value' => function ($model) {
                        return SystemNews::getStatuses()[$model->target];
                    },
                ],
                'text:html',
                'title',
                [
                    'attribute' => 'status',
                    'filter' => SystemNews::getStatuses(),
                    'value' => function ($model) {
                        return SystemNews::getStatuses()[$model->status];
                    },
                ],
                [
                    'attribute' => 'created',
                    'filter' => DateRangePicker::widget([
                        'model' => $searchModel,
                        'attribute' => 'created_range',
                        'convertFormat' => true,
                        'pluginOptions' => [
                            'locale' => [
                                'format' => 'd.m.Y',
                            ],
                            'autoclose' => true,
                            'opens' => 'left'
                        ],
                        'pluginEvents' => [
                            //'cancel.daterangepicker' => "function() { $('#statsearch-created_range').val(null).daterangepicker('update'); }",
                        ]
                    ]),
                    'value' => function ($model) {
                        return Yii::$app->formatter->asDate($model->created, 'long');
                    },
                ],

                ['class' => ActionColumn::class],
            ],
        ]); ?>
    <?php Pjax::end(); ?>
</div>
