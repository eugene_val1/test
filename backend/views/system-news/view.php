<?php

use common\models\SystemNews;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\SystemNews */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'System News', 'url' => ['index']];

?>

<div class="system-news-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'label' => 'Пользователи',
                'value' => SystemNews::getTargets()[$model->target],
            ],
            [
                'label' => 'Статус',
                'value' => SystemNews::getStatuses()[$model->status],
            ],
            'text:html',
            'title',
            [
                'label' => 'Создано',
                'value' => Yii::$app->formatter->asDate($model->created, 'long'),
            ],
            [
                'label' => 'Обновлено',
                'value' => Yii::$app->formatter->asDate($model->updated, 'long'),
            ]
        ],
    ]) ?>

</div>
