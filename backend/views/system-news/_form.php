<?php

use backend\assets\RedactorAsset;
use common\models\Language;
use common\models\SystemNews;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use \vova07\imperavi\Widget;

/* @var $this yii\web\View */
/* @var $model common\models\SystemNews */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="system-news-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'target')->dropDownList(SystemNews::getTargets()); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => 1024]) ?>

    <?= $form->field($model, 'status')->dropDownList(SystemNews::getStatuses()); ?>

    <?= $form->field($model, 'text')->widget(Widget::className(), [
        'settings' => [
            'lang' => Language::getCurrent()->url,
            'minHeight' => 200,
            'imageUpload' => Url::to(['/redactor/image-upload']),
            'imageManagerJson' => Url::to(['/redactor/images-get']),
            'formattingAdd' => [
                'fullwidth' => [
                    'title' => Yii::t('main', 'Fullwidth text'),
                    'tag' => 'p',
                    'class' => 'fullwidth'
                ],
                'fullwidth-h' => [
                    'title' => Yii::t('main', 'Fullwidth title'),
                    'tag' => 'h1',
                    'class' => 'fullwidth'
                ],
            ],
            'plugins' => [
                'fullscreen',
                'imagemanager',
                'fontcolor',
                'video',
                'fontfamily',
                'fontsize',
                'definedlinks'
            ]
        ],
        'plugins' => [
            'lineheight' => RedactorAsset::class,
            'clips' => RedactorAsset::class
        ]
    ]); ?>

    <div class="form-group">
        <?= Html::submitButton(
            $model->isNewRecord ? 'Create' : 'Update',
                ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']
        ) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
