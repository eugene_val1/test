<?php
/* @var $this yii\web\View */
/* @var $searchModel backend\models\DispatchSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

use yii\grid\GridView;
use yii\helpers\Html;
use common\models\Dispatch;
use yii\helpers\Url;

$this->title = Yii::t('backend', 'Dispatch');
?>
<div class="row">
    <div class="col-lg-12">
        <div class="pull-right">
            <a href="<?= Url::toRoute('create'); ?>" class="btn btn-success btn-sm pull-right">
                <i class="fa fa-plus"></i> <?= Yii::t('backend', 'Create'); ?>
            </a>
        </div>
        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'tableOptions' => ['class' => 'table table-striped table-advance table-hover'],
            'columns' => [
                [
                    'label' => Yii::t('backend', 'User'),
                    'attribute' => 'user_name',
                    'value' => function ($model) {
                        return ($model->profile) ? $model->profile->name : '';
                    },
                ],
                'message',
                [
                    'attribute' => 'type',
                    'filter' => $searchModel->getTypes(),
                    'value' => function ($model) {
                        return $model->typeName;
                    },
                ],
                [
                    'attribute' => 'status',
                    'filter' => $searchModel->getStatus(),
                    'value' => function ($model) {
                        return $model->statusName;
                    },
                ],
                'count',
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{run} {stop}',
                    'buttons' => [
                        'run' => function ($url, $model, $key) {
                            if($model->status == Dispatch::STATUS_WAITING && !$model->sent){
                                return Html::a('<i class="fa fa-play"></i> ' . Yii::t('backend', 'Run'), $url, [
                                    'class' => 'btn btn-success btn-sm',
                                    'title' => Yii::t('backend', 'Run'),
                                    'data-target' => '#ajax',
                                ]);
                            }
                        },
                        'stop' => function ($url, $model, $key) {
                            if($model->status == Dispatch::STATUS_RUNNING && !$model->sent){
                                return Html::a('<i class="fa fa-stop"></i> ' . Yii::t('backend', 'Stop'), $url, [
                                    'class' => 'btn btn-danger btn-sm',
                                    'title' => Yii::t('backend', 'Stop'),
                                    'data-target' => '#ajax',
                                ]);
                            }
                        },
                    ],
                ]
            ],
        ]);
        ?>
    </div>
</div>