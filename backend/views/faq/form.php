<?php
/* @var $this yii\web\View */
/* @var $model common\models\Faq */
/* @var $form yii\widgets\ActiveForm */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use common\models\FaqCategory;
use common\models\Language;
use \vova07\imperavi\Widget;

$this->title = $model->isNewRecord ? Yii::t('backend', 'Create question') : Yii::t('backend', 'Update question') . ': ' . $model->question;
?>

<div class="row">
    <div class="col-lg-12">
        <?php $form = ActiveForm::begin(); ?>
        <?= $form->field($model, 'category_id')->dropDownList(FaqCategory::getTree(),['prompt'=>'']); ?>
        <?= $form->field($translation, 'question')->textInput() ?>
        <?=
            $form->field($translation, 'answer')->widget(Widget::className(), [
                'settings' => [
                    'lang' => Language::getCurrent()->url,
                    'minHeight' => 200,
                    'imageUpload' => Url::to(['/redactor/image-upload']),
                    'imageManagerJson' => Url::to(['/redactor/images-get']),
                    'plugins' => [
                        'fullscreen',
                        'imagemanager',
                        'fontcolor',
                        'video',
                        'fontfamily'
                    ]
                ]
            ]);
        ?>
        <?= $form->field($model, 'featured')->checkbox() ?>
        <div class="form-group">
            <?= Html::submitButton('<i class="fa fa-save"></i> ' . ($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Save')), ['class' => $model->isNewRecord ? 'btn btn-success btn-sm' : 'btn btn-primary btn-sm']) ?>
            <a href="<?= Url::toRoute(['index', 'id' => $model->category_id]); ?>" class="btn btn-danger btn-sm"><i class="fa fa-backward"></i> <?= Yii::t('backend', 'Cancel'); ?></a>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>