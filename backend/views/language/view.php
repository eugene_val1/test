<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Language */

$this->title = $model->name;
?>
<div class="row mt">
    <div class="col-lg-12">
        <div style="margin-bottom: 15px" class="pull-right">
            <?= Html::a('<i class="fa fa-backward"></i> ' . Yii::t('backend', 'Back'), 'index', ['class' => 'btn btn-success btn-sm']) ?>
            <?= Html::a('<i class="fa fa-pencil"></i> ' . Yii::t('backend', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary btn-sm']) ?>
        </div>
        <?=
        DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'name',
                'url',
                'local',
            ],
        ])
        ?>

    </div>
</div>
