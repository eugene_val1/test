<?php
/* @var $this yii\web\View */
/* @var $model common\models\Language */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use common\models\SourceTranslationMessage;

$this->title = Yii::t('backend', 'Translations for') . ' ' . $language->name;
?>
<div class="row">
    <div class="col-lg-12">
        <?php $form = ActiveForm::begin([
            'enableClientValidation' => false,
            'validateOnChange' => false,
            'validateOnSubmit' => false,
        ]); ?>
        <ul class="nav nav-tabs" role="tablist">
            <?php foreach(SourceTranslationMessage::getCategories() as $category => $categoryName):?>
            <li role="presentation" <?= ($category == 'main') ? 'class="active"' : '' ?>><a href="#<?= $category ?>" aria-controls="<?= $category ?>" role="tab" data-toggle="tab"><?= $categoryName ?></a></li>
            <?php endforeach; ?>
        </ul>
        <div class="tab-content">
            <?php foreach(SourceTranslationMessage::getCategories() as $category => $categoryName):?>
                <?php if($messages[$category]): ?>
                    <div role="tabpanel" class="tab-pane <?= ($category == 'main') ? 'active' : '' ?>" id="<?= $category ?>">
                        <table class="table table-striped table-advance table-hover" style="table-layout: fixed">
                            <?php foreach ($messages[$category] as $id => $model): ?>
                                <tr>
                                    <td width="10%">
                                        <?= $sourceMessages[$id]->id ?>
                                    </td>
                                    <td width="30%">
                                        <?= $sourceMessages[$id]->message ?>
                                    </td>
                                    <td>
                                        <?php
                                        $field = $form->field($model, '[' . $id . ']translation');
                                        $field->template = "{input}\n{error}";
                                        echo ($sourceMessages[$id]->type == 'string') ? $field->textInput() : $field->textArea(['rows' => 5]);
                                        ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </table>
                    </div>
                <?php endif; ?>
            <?php endforeach; ?>
         </div>
        <div class="form-group">
            <?= Html::submitButton('<i class="fa fa-save"></i> ' . Yii::t('backend', 'Save'), ['class' => 'btn btn-success btn-sm']) ?>
            <a href="<?= Url::toRoute('index'); ?>" class="btn btn-danger btn-sm"><i class="fa fa-backward"></i> <?= Yii::t('backend', 'Cancel'); ?></a>
        </div>
        
        
        <?php ActiveForm::end(); ?>
    </div>
</div>
