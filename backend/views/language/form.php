<?php
/* @var $this yii\web\View */
/* @var $model common\models\Language */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$this->title = $model->isNewRecord ? Yii::t('backend', 'Create language') : Yii::t('backend', 'Update language') . ': ' . $model->name;
?>
<div class="row">
    <div class="col-lg-12">
        <?php $form = ActiveForm::begin(['options' => ['enctype'=>'multipart/form-data']]); ?>
        <?= $form->field($model, 'name')->textInput(['maxlength' => 255]) ?>
        <?= $form->field($model, 'url')->textInput(['maxlength' => 255]) ?>
        <?= $form->field($model, 'local')->textInput(['maxlength' => 255]) ?>
        <?php if ($model->image): ?>
            <div class="form-group">
                <label class="control-label"><?= Yii::t('backend', 'Current image'); ?></label>
                <div>
                    <?= Html::img($model->getFileUrl('image'), ['class' => 'img-thumbnail', 'alt' => "", 'width' => '24px']); ?>
                </div>
            </div>
        <?php endif; ?>
        <?= $form->field($model, 'image')->hiddenInput()->label(false); ?>
        <?= $form->field($model, 'imageFile')->fileInput() ?>
        <?= $form->field($model, 'active')->checkbox(); ?>
        <div class="form-group">
            <?= Html::submitButton('<i class="fa fa-save"></i> ' . ($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Save')), ['class' => $model->isNewRecord ? 'btn btn-success btn-sm' : 'btn btn-primary btn-sm']) ?>
            <a href="<?= Url::toRoute('index'); ?>" class="btn btn-danger btn-sm"><i class="fa fa-backward"></i> <?= Yii::t('backend', 'Cancel'); ?></a>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
