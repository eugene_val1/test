<?php
/* @var $this yii\web\View */
/* @var $searchModel backend\models\LanguageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\Language;

$this->title = Yii::t('backend', 'Languages');
?>

<div class="row">
    <div class="col-lg-12">
        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'tableOptions' => ['class' => 'table table-striped table-advance table-hover'],
            'columns' => [
                'name',
                'local',
                'url',
                [
                    'attribute' => 'active',
                    'filter' => $searchModel->getStatuses(),
                    'format' => 'html',
                    'value'=>function ($model) {
                        $html = '';
                        if($model->active === Language::STATUS_ACTIVE){
                            $html = '<span class="has-success"><i class="help-block fa fa-check"></i></span>';
                        } else {
                            $html = '<span class="has-error"><i class="help-block fa fa-exclamation-triangle"></i></span>';
                        }
                        return $html . ' ' . $model->getStatusName();
                    },
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{translations} {view} {update}',
                    'buttons' => [
                        'translations' => function ($url, $model, $key) {
                            return Html::a('<i class="fa fa-font"></i> ' . Yii::t('backend', 'Translations'), $url, [
                                        'class' => 'btn btn-warning btn-sm',
                                        'data-pjax' => 0,
                            ]);
                        },
                        'view' => function ($url, $model, $key) {
                            return Html::a('<i class="fa fa-eye"></i> ' . Yii::t('backend', 'View'), $url, [
                                        'class' => 'btn btn-success btn-sm',
                                        'data-pjax' => 0,
                            ]);
                        },
                        'update' => function ($url, $model, $key) {
                            return Html::a('<i class="fa fa-pencil"></i> ' . Yii::t('backend', 'Edit'), $url, [
                                        'class' => 'btn btn-primary btn-sm',
                                        'data-pjax' => 0,
                            ]);
                        },
                            ],
                        ],
                    ],
                ]);
                ?>
    </div>
</div>
