<?php

/* @var $this yii\web\View */
/* @var $searchModel backend\models\TaskSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

use common\models\Appointment;
use kartik\daterange\DateRangePicker;
use kartik\widgets\DatePicker;
use yii\grid\GridView;
use yii\helpers\Html;

$this->title = Yii::t('backend', 'Appointments');

?>
<div class="row">
    <div class="col-lg-12">
        <?= Html::a(Yii::t('backend', 'Reset filters'), '/appointment/index', ['class' => 'btn btn-success btn-sm pull-right']) ?>
        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'tableOptions' => ['class' => 'table table-striped table-advance table-hover'],
            'columns' => [
                [
                    'label' => Yii::t('backend', 'User'),
                    'attribute' => 'user_name',
                    'value' => function ($model) {
                        return $model->user ? $model->user->name : '';
                    },
                ],
                [
                    'label' => Yii::t('backend', 'Therapist'),
                    'attribute' => 'therapist_name',
                    'value' => function ($model) {
                        return $model->therapistLang ? $model->therapistLang->name : '';
                    },
                ],
                [
                    'attribute' => 'created',
                    'label' => Yii::t('main', 'Created'),
                    'format' => 'html',
                    'filter' => DateRangePicker::widget([
                        'model' => $searchModel,
                        'attribute' => 'appointment_created_range',
                        'convertFormat' => true,
                        'pluginOptions' => [
                            'locale' => [
                                'format' => 'd.m.Y',
                            ],
                            'autoclose' => true,
                            'opens' => 'right',
                        ]
                    ]),
                    'value' => function ($model) {
                        return Yii::$app->formatter->asDate($model->created, 'medium')
                            . ' ' . Yii::$app->formatter->asTime($model->created, 'short');
                    },
                ],
                [
                    'attribute' => 'start',
                    'filter' => DatePicker::widget([
                        'model' => $searchModel,
                        'attribute' => 'start',
                        'type' => 1,
                        'pluginOptions' => [
                            'autoclose' => true,
                            'format' => 'dd.mm.yyyy',
                        ]
                    ]),
                    'format' => 'html',
                    'value' => function ($model) {
                        return Yii::$app->formatter->asDate($model->start, 'medium')
                            . ' ' . Yii::$app->formatter->asTime($model->start, 'short');
                    },
                ],
                [
                    'attribute' => 'status',
                    'filter' => $searchModel->getStatuses(),
                    'format' => 'html',
                    'value' => function ($model) {
                        $labelText = $model->statusName;
                        $labelClass = '';

                        switch ((int)$model->status) {
                            case Appointment::STATUS_NEW:
                                $labelClass = 'primary';
                                break;
                            case Appointment::STATUS_RESCHEDULE:
                                $labelClass = 'primary';
                                break;
                            case Appointment::STATUS_COMPLETED:
                                $labelClass = 'success';
                                break;
                            case Appointment::STATUS_MISSED:
                                $labelClass = 'warning';
                                break;
                            case Appointment::STATUS_CANCELED:
                                $labelClass = 'danger';
                                break;
                        }

                        return "<span class=\"label label-{$labelClass}\">{$labelText}</span>";
                    },
                ],
                [
                    'attribute' => 'payment_status',
                    'filter' => $searchModel->getPaymentStatuses(),
                    'value' => function ($model) {
                        return $model->paymentStatusName;
                    },
                ],
                [
                    'label' => Yii::t('backend', 'Duration'),
                    'content' => function ($model) {
                        if ($model->duration) {
                            return Yii::$app->formatter->asDuration($model->duration);
                        }
                    },
                ],
                /*[
                    'label' => Yii::t('backend', 'Room'),
                    'content' => function ($model) {
                        if ($model->room) {
                            return Html::a('<i class="fa fa-link"></i> ' . Yii::t('backend', 'Room'), $model->room->getUrl(), [
                                'class' => 'btn btn-success btn-sm',
                                'target' => '_blank',
                                'data-pjax' => 0,
                            ]);
                        }
                    },
                ],*/
                [
                    'label' => Yii::t('backend', 'Transaction'),
                    'content' => function ($model) {
                        if ($model->transaction) {
                            return Html::a('<i class="fa fa-link"></i> ' . $model->transaction->payment_id, ['transaction/view', 'id' => $model->transaction->id], [
                                'class' => 'btn btn-success btn-sm',
                                'target' => '_blank',
                                'data-pjax' => 0,
                            ]);
                        }
                    },
                ],
            ],
        ]);
        ?>
    </div>
</div>