<?php

use common\models\NeedModerationEvent;
use yii\helpers\Html;

/** @var NeedModerationEvent $model */

?>

<p>
    <?php if($model->target): ?>   
        <?= $model->target . ' ' ?>
        <?= Html::a($model->icon . ' ' . $model->title, $model->url) ?>
        <?= Yii::t('backend', 'is waiting for moderation') ?>
    <?php else:?>
        <?= Yii::t('backend', "Can't show event text") ?>
    <?php endif; ?>
</p>

<?php if ($model->text === NeedModerationEvent::TARGET_CONTACT): ?>
    <p><?= Yii::t('main', 'From') ?>: <?= $model->getData()['name'] ?> (<?= $model->getData()['email'] ?>)</p>
    <p><?= Yii::t('main', 'Theme') ?>: <?= $model->getData()['theme'] ?> </p>
    <p style="font-style: italic;">
        <?= $model->getData()['message'] ?>
    </p>
<?php endif; ?>