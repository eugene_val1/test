<?php 
use yii\helpers\Html;
use yii\helpers\Url;
?>

<p>
    <?php if($model->client && $model->client->user): ?>
        <?= Yii::t('backend', 'User') ?>
        <?= Html::a('<i class="fa fa-user"></i> ' . $model->client->user->name, Url::toRoute(['user/view', 'id' => $model->client->user_id])) ?>
    <?php else: ?>
        <?= Yii::t('backend', 'Deleted user') ?>
    <?php endif; ?>
    <?= $model->statusMessage() ?>
    <?php if($model->client && $model->client->therapist): ?>
        <?= Html::a('<i class="fa fa-user"></i> ' . $model->client->therapist->name, Url::toRoute(['therapist/view', 'id' => $model->client->therapist->id])); ?>
    <?php endif; ?>
</p>