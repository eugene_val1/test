<?php
/* @var $this yii\web\View */
/* @var $searchModel backend\models\HomeSlideSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\widgets\DatePicker;

$this->title = Yii::t('backend', 'Events');
?>

<div class="row mt">
    <div class="col-lg-12">
        <div style="margin-bottom: 15px" class="pull-right">
            <?= Html::a('<i class="fa fa-trash-o "></i> ' . Yii::t('backend', 'Delete selected'), 'delete-selected', [
                'class' => 'btn btn-danger btn-sm delete-selected'
            ]) ?>
        </div>
        <?=
        GridView::widget([
            'id' => 'backend-grid',
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'tableOptions' => ['class' => 'table table-striped table-advance table-hover'],
            'columns' => [
                [
                    'class' => 'yii\grid\CheckboxColumn',
                    'checkboxOptions' => [
                        'class' => 'grid-check-item'
                    ],
                    'cssClass' => 'grid-check-item',
                    'header' => Html::checkBox('selection', false, ['class' => 'grid-check-all'])
                ],
                [
                    'attribute' => 'type',
                    'filter' => $searchModel->getTypes(),
                    'value'=>function ($model) {
                         return $model->getTypeName();
                     },
                ],
                [
                    'attribute' => 'importance',
                    'format' => 'html',
                    'contentOptions' => ['class' => 'text-center'],
                    'filter' => $searchModel->getImportanceLevels(),
                    'value'=>function ($model) {
                         return $model->getImportanceIcon()[$model->importance];
                     },
                ],
                [
                    'attribute' => 'created',
                    'filter' => DatePicker::widget([
                        'model' => $searchModel,
                        'attribute' => 'created',
                        'type' => 1,
                        'pluginOptions' => [
                            'autoclose'=>true,
                            'format' => 'dd.mm.yyyy'
                        ]
                    ]),
                    'format' => 'html',
                    'value'=>function ($model) {
                        return date('d.m.Y', $model->created);
                    },
                ],
                [
                    'label' => Yii::t('main', 'Event content'),
                    'format' => 'raw',
                    'value'=>function ($model) {
                        return $this->render('_' . $model->alias, ['model' => $model]);
                    },
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{delete}',
                    'buttons' => [
                        'delete' => function ($url, $model, $key) {
                            return Html::a('<i class="fa fa-trash-o "></i> ' . Yii::t('backend', 'Delete'), $url, [
                                'class' => 'btn btn-danger btn-sm',
                                'data' => ['pjax' => 0, 'method' => 'post', 'confirm' => Yii::t('backend', 'Delete element?')],
                            ]);
                        }
                    ],
                ],
            ],
        ]);
        ?>

    </div>
</div>
