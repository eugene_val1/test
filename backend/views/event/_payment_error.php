<?php 
use yii\helpers\Html;
use yii\helpers\Url;
?>

<p>
    <?php if($model->transaction): ?>
    <?= Yii::t('backend', 'Transaction') ?>
    <?= Html::a($model->transaction->payment_id, Url::toRoute(['transaction/view', 'id' => $model->transaction->id])) ?>
    <?= Yii::t('backend', 'running errors while settle') . ": " . $model->getError(); ?>
    <?php else: ?>
    <?= Yii::t('backend', 'Wrong transaction ID') ?>
    <?php endif; ?>
</p>