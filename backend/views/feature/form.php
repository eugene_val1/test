<?php
/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $form yii\widgets\ActiveForm */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use vova07\imperavi\Widget;
use common\models\Language;

$this->title = $model->isNewRecord ? Yii::t('backend', 'Create feature') : Yii::t('backend', 'Update feature') . ': ' . $model->title;
?>

<div class="row">
    <div class="col-lg-12">
        <?php $form = ActiveForm::begin(['enableClientValidation' => false,'options' => ['enctype'=>'multipart/form-data']]); ?>
        <?= $form->field($translation, 'title')->textInput() ?>
        <?=
        $form->field($translation, 'description')->widget(Widget::className(), [
            'settings' => [
                'lang' => Language::getCurrent()->url,
                'minHeight' => 200,
                'plugins' => [
                    'fullscreen'
                ]
            ]
        ]);
        ?>
        <?= $form->field($model, 'url')->textInput() ?>
        <?php if ($model->image): ?>
            <div class="form-group">
                <label class="control-label"><?= Yii::t('backend', 'Current image'); ?></label>
                <div>
                    <?= Html::img($model->getFileUrl('image'), ['class' => 'img-thumbnail', 'alt' => "", 'width' => '128px']); ?>
                </div>
            </div>
        <?php endif; ?>
        <?= $form->field($model, 'image')->hiddenInput()->label(false); ?>
        <?= $form->field($model, 'imageFile')->fileInput() ?>
        <div class="form-group">
            <?= Html::submitButton('<i class="fa fa-save"></i> ' . ($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Save')), ['class' => $model->isNewRecord ? 'btn btn-success btn-sm' : 'btn btn-primary btn-sm']) ?>
            <a href="<?= Url::toRoute('index'); ?>" class="btn btn-danger btn-sm"><i class="fa fa-backward"></i> <?= Yii::t('backend', 'Cancel'); ?></a>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>