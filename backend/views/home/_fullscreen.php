<?php

/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $form yii\widgets\ActiveForm */

use yii\helpers\Html;

?>
<?php if ($model->video): ?>
    <div class="form-group">
        <label class="control-label"><?= Yii::t('backend', 'Current video'); ?></label>
        <div>
            <video width="320" controls>
                <source src="<?= $model->getFileUrl('video'); ?>" type="video/mp4">
            </video>
        </div>
    </div>
<?php endif; ?>

<?= $form->field($model, 'videoFile')->fileInput() ?>
<?= $form->field($model, 'video')->hiddenInput()->label(false); ?>

<?php if ($model->image): ?>
<div class="form-group">
    <label class="control-label"><?= Yii::t('backend', 'Current image'); ?></label>
    <div>
    <?= Html::img($model->getFileUrl('image'), ['class' => 'img-thumbnail', 'alt' => "", 'width' => '320px']); ?>
    </div>
</div>
<?php endif; ?>

<?= $form->field($model, 'imageFile')->fileInput() ?>
<?= $form->field($model, 'image')->hiddenInput()->label(false); ?>