<?php

use common\models\Feature;
?>
<div class="row features"> 
<?php foreach (Feature::find()->orderBy('number')->all() as $feature): ?>
        <div class="col-sm-4 item">
            <div class="icon"><span style="background: url('<?= $feature->getFileUrl('image'); ?>') no-repeat;"></span></div>
            <div class="text">
                <h5><?= $feature->title; ?></h5>
                <p><?= $feature->description; ?></p>
            </div>
        </div>
<?php endforeach; ?>
</div>