<?php

/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $form yii\widgets\ActiveForm */

use yii\helpers\Html;

?>
<?php if ($model->image): ?>
<div class="form-group">
    <label class="control-label"><?= Yii::t('backend', 'Current image'); ?></label>
    <div>
    <?= Html::img($model->getFileUrl('image'), ['class' => 'img-thumbnail', 'alt' => "", 'width' => '320px']); ?>
    </div>
</div>
<?php endif; ?>

<?= $form->field($model, 'imageFile')->fileInput() ?>
<?= $form->field($model, 'image')->hiddenInput()->label(false); ?>