<?php
/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $form yii\widgets\ActiveForm */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use \vova07\imperavi\Widget;
use common\models\Language;

$this->title = $model->isNewRecord ? Yii::t('backend', 'Create slide') : Yii::t('backend', 'Update slide') . ': ' . $model->title;
?>

<div class="row">
    <div class="col-lg-12">
        <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
        <?= $model->isNewRecord ? '' : Html::activeHiddenInput($model, 'type')?>
        <?=
        $form->field($model, 'type')->dropDownList(
                $model->getTypes(), 
                array_merge(
                    [
                        'prompt' => Yii::t('backend', 'Choose slide type'),
                        'onchange' => 'updateType(this.value)',
                    ], 
                    $model->isNewRecord ? [] : ['disabled' => 'disabled']
                )
        );
        ?>
        <?= $form->field($translation, 'title')->textInput() ?>
        <div id="slide-type">
            <?php if($model->type && $typeModel !== ''): ?>
                <?= $this->render('_' . $model->type, ['model' => $typeModel, 'translation' => $translation, 'form' => $form]); ?>
            <?php endif; ?>
        </div>
        <?=
        $form->field($translation, 'content')->widget(Widget::className(), [
            'settings' => [
                'lang' => Language::getCurrent()->url,
                'minHeight' => 200,
                'definedLinks' => '/site/links',
                'imageUpload' => Url::to(['/redactor/image-upload']),
                'imageManagerJson' => Url::to(['/redactor/images-get']),
                'plugins' => [
                    'fullscreen',
                    'imagemanager',
                    'fontcolor',
                    'video',
                    'fontfamily',
                    'fontsize',
                    'definedlinks'
                ]
            ],
            'plugins' => [ 
                'lineheight' => 'backend\assets\RedactorAsset',
                'clips' => 'backend\assets\RedactorAsset'
            ]
        ]);
        ?>
        <div class="form-group">
        <?= Html::submitButton('<i class="fa fa-save"></i> ' . ($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Save')), ['class' => $model->isNewRecord ? 'btn btn-success btn-sm' : 'btn btn-primary btn-sm']) ?>
            <a href="<?= Url::toRoute('index'); ?>" class="btn btn-danger btn-sm"><i class="fa fa-backward"></i> <?= Yii::t('backend', 'Cancel'); ?></a>
        </div>
    <?php ActiveForm::end(); ?>
    </div>
</div>