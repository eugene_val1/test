<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = $model->title;
?>
<div class="row">
    <div class="col-lg-12">
        <div style="margin-bottom: 15px" class="pull-right">
            <?= Html::a('<i class="fa fa-backward"></i> ' . Yii::t('backend', 'Back'), 'index', ['class' => 'btn btn-success btn-sm']) ?>
            <?= Html::a('<i class="fa fa-pencil"></i> ' . Yii::t('backend', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary btn-sm']) ?>
            <?=
            Html::a('<i class="fa fa-trash-o"></i> ' . Yii::t('backend', 'Delete'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger btn-sm',
                'data' => [
                    'confirm' => Yii::t('backend', 'Delete element?'),
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
        <?=
        DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'title',
            ],
        ])
        ?> 
    </div>
</div>