<?php

use yii\helpers\Html;
use yii\helpers\Url;
use common\widgets\Alert;
use common\models\Therapist;
use common\models\Property;
use yii\web\View;
use common\models\User;

/** @var View $this */
/** @var User $model */
/** @var Therapist $therapist */

$this->title = $therapist->name;

?>

<div class="row">
    <div class="col-lg-12 clearfix">
        <div style="margin-bottom: 15px" class="pull-right">
            <?= Html::a('<i class="fa fa-backward"></i> ' . Yii::t('backend', 'Back'), 'index', ['class' => 'btn btn-success btn-sm']) ?>
            <?= Html::a('<i class="fa fa-pencil"></i> ' . Yii::t('backend', 'Update'), ['update', 'id' => $therapist->id], ['class' => 'btn btn-primary btn-sm']) ?>
            <?=
            Html::a('<i class="fa fa-trash-o"></i> ' . Yii::t('backend', 'Delete'), ['delete', 'id' => $therapist->id], [
                'class' => 'btn btn-danger btn-sm',
                'data' => [
                    'confirm' => Yii::t('backend', 'Delete element?'),
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
        <div style="margin-bottom: 15px" class="pull-left">
            <?php
            if ($therapist->publish_status == Therapist::STATUS_PUBLISHED) {
                echo Html::a('<i class="fa fa-close"></i> ' . Yii::t('backend', 'Unpublish'), ['unpublish', 'id' => $therapist->id], [
                    'class' => 'btn btn-danger btn-sm',
                    'data' => [
                        'confirm' => Yii::t('backend', 'Unpublish this therapist?'),
                        'method' => 'post',
                    ],
                ]);
            } else {
                echo Html::a('<i class="fa fa-check"></i> ' . Yii::t('backend', 'Publish'), ['publish', 'id' => $therapist->id], [
                    'class' => 'btn btn-success btn-sm',
                    'data' => [
                        'confirm' => Yii::t('backend', 'Publish this therapist?'),
                        'method' => 'post',
                    ],
                ]);
                echo Html::a('<i class="fa fa-close"></i> ' . Yii::t('backend', 'Report errors'), ['send', 'id' => $therapist->id], [
                    'class' => 'btn btn-danger btn-sm',
                    'style' => 'margin-left: 3px',
                    'data' => [
                        'target' => '#modal',
                        'title' => Yii::t('backend', 'Report errors')
                    ],
                ]);
            }
            ?>
        </div>
    </div>
    <div class="col-lg-12"><?= Alert::widget(); ?></div>
    <div class="col-lg-12">

        <div class="col-lg-3">
            <h4><?= Yii::t('main', 'Image') ?></h4>
            <div class="text-center">
                <img src="<?= $therapist->getFileUrl('main_image') ?>" alt="">
            </div>
            <h4><?= Yii::t('backend', 'Catalog image') ?></h4>
            <div class="person-wrapper">
                <div class="catalog-content">
                    <img class="person-image" src="<?= $therapist->getFileUrl('catalog_image') ?>" alt="">
                    <div class="person-image-hover">
                        <img src="<?= $therapist->getFileUrl('catalog_image_hover') ?>" alt="">
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-6">
            <h4><?= Yii::t('main', 'Short description') ?></h4>
            <?= strip_tags($therapist->short_description) ?>
            <?php foreach ($therapist->getPropertiesByType() as $type => $items): ?>
                <h4><?= Property::getTypeLabel($type); ?></h4>
                <?= strip_tags(implode(', ', $items)) ?>
            <?php endforeach; ?>
            <h4><?= Yii::t('main', 'Age') . ' - ' . $therapist->age ?></h4>
            <h4><?= Yii::t('main', 'Gender') . ' - ' . $therapist->genderTitle ?></h4>
            <h4><?= Yii::t('main', 'Education') ?></h4>
            <?= strip_tags($therapist->education) ?>
            <h4><?= Yii::t('main', 'Experience') ?></h4>
            <?= strip_tags($therapist->experience) ?>
            <h4><?= Yii::t('main', 'More info') ?></h4>
            <?= strip_tags($therapist->full_description) ?>
            <h4><?= Yii::t('main', 'Terms and conditions') ?></h4>
            <?= strip_tags($therapist->terms) ?>
            <h4><?= Yii::t('main', 'Articles description') ?></h4>
            <?= strip_tags($therapist->additional->article_description) ?>
        </div>

        <div class="col-lg-3">
            <h4><?= Yii::t('main', 'Prices') ?></h4>
            <?php if (count($prices) > 0): ?>
                <table class="table table-striped table-advance table-hover">
                    <thead>
                    <tr>
                        <th><?= Yii::t('main', 'Region') ?></th>
                        <th><?= Yii::t('main', 'Price') ?></th>
                    </tr>
                    </thead>
                    <?php foreach ($prices as $i => $price): ?>
                        <tr>
                            <td><?= ($price->regionName) ?: $price->region->name ?></td>
                            <td><?= $price->amount ?></td>
                        </tr>
                    <?php endforeach; ?>
                </table>
            <?php else: ?>
                <p><?= Yii::t('main', 'Not set') ?></p>
            <?php endif; ?>
            <h4><?= Yii::t('main', 'Certificates and diplomas') ?></h4>
            <?php if (count($certificates) > 0): ?>
                <div id="certificates">
                    <div class="row">
                        <?php foreach ($certificates as $certificate): ?>
                            <div class="col-sm-6 certificate-item">
                                <div class="text-center">
                                    <a data-title="<?= Yii::t('main', 'Certificates and diplomas') ?>"
                                       href="<?= Url::toRoute(['certificate', 'id' => $certificate->id]); ?>"
                                       data-target="#modal">
                                        <img src="<?= $certificate->getFileUrl('image'); ?>" alt=""
                                             class="img-responsive">
                                    </a>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            <?php else: ?>
                <p><?= Yii::t('main', 'Not set') ?></p>
            <?php endif; ?>
        </div>

    </div>
</div>