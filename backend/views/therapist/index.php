<?php

/* @var $this yii\web\View */
/* @var $searchModel backend\models\TherapistSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

use yii\helpers\Html;
use yii\helpers\Url;
use himiklab\sortablegrid\SortableGridView;

$this->title = Yii::t('backend', 'Therapists');

?>

<div class="row mt">
    <div class="col-lg-12">
        <div class="pull-right">
            <a href="<?= Url::toRoute('create'); ?>" class="btn btn-success btn-sm pull-right">
                <i class="fa fa-plus"></i> <?= Yii::t('backend', 'Create'); ?>
            </a>
        </div>
        <?=
        SortableGridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'tableOptions' => ['class' => 'table table-striped table-advance table-hover'],
            'columns' => [
                'name',
                [
                    'attribute' => 'username',
                    'value' => function ($model) {
                        return $model->user->username;
                    },
                ],
                [
                    'attribute' => 'email',
                    'value' => function ($model) {
                        return $model->user->email;
                    },
                ],
                [
                    'attribute' => 'publish_status',
                    'filter' => $searchModel->getStatusNames(),
                    'value' => function ($model) {
                        return $model->getStatusNames()[$model->publish_status];
                    },
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{view} {update} {delete} {login}',
                    'buttons' => [
                        'view' => function ($url, $model, $key) {
                            return Html::a('<i class="fa fa-eye"></i>', $url, [
                                'class' => 'btn btn-success btn-sm',
                                'data-pjax' => 0,
                                'title' => Yii::t('backend', 'View & Publish')
                            ]);
                        },
                        'update' => function ($url, $model, $key) {
                            return Html::a('<i class="fa fa-pencil"></i>', $url, [
                                'class' => 'btn btn-primary btn-sm',
                                'data-pjax' => 0,
                                'title' => Yii::t('backend', 'Edit')
                            ]);
                        },
                        'delete' => function ($url, $model, $key) {
                            return Html::a('<i class="fa fa-trash-o "></i>', $url, [
                                'class' => 'btn btn-danger btn-sm',
                                'data' => ['pjax' => 0, 'method' => 'post', 'confirm' => Yii::t('backend', 'Delete element?')],
                                'title' => Yii::t('backend', 'Delete')
                            ]);
                        },
                        'login' => function ($url, $model, $key) {
                            return Html::a('<i class="fa fa-sign-in"></i>', '/user/login-as?id=' . $model->user_id, [
                                'class' => 'btn btn-warning btn-sm',
                                'target' => '_blank',
                                'title' => Yii::t('backend', 'Login')
                            ]);
                        }
                    ],
                ],
            ],
        ]);
        ?>

    </div>
</div>
