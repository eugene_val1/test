<div class="certificate-wrapper">
    <div class="row">
        <div class="col-sm-12 text-center">
            <?php if ($model->image): ?>
                <img class="img-responsive" src="<?= $model->getFileUrl('image') ?>" alt="">
            <?php endif; ?>            
        </div>
        <div class="col-sm-12">
            <p><?= Yii::$app->formatter->asNtext($model->description) ?></p>
        </div>        
    </div>
</div>