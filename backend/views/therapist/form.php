<?php

use backend\assets\RedactorAsset;
use common\models\TherapistLang;
use yii\web\View;
use common\models\User;
use yii\widgets\ActiveForm;
use common\models\Therapist;
use common\models\TherapistAdditional;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use common\models\Timezone;
use common\models\CountryLang;
use kartik\widgets\Select2;
use vova07\imperavi\Widget;
use common\models\Language;

/** @var View $this */
/** @var User $user */
/** @var ActiveForm $form */
/** @var TherapistLang $translation */
/** @var Therapist $model */
/** @var TherapistAdditional $additional */

$this->title = $model->isNewRecord ? Yii::t('backend', 'Create therapist') : Yii::t('backend', 'Update therapist') . ': ' . $model->name;
?>

<div class="row">
    <div class="col-lg-12">
        <?php $form = ActiveForm::begin(['options' => ['enctype'=>'multipart/form-data']]); ?>

        <?= $form->errorSummary($model) ?>

        <?= $form->field($user, 'username')->textInput() ?>
        <?= $form->field($user, 'email')->textInput() ?>
        <?= $form->field($user, 'user_password')->textInput() ?>
        <?= $form->field($translation, 'name')->textInput() ?>
        <?= $form->field($model, 'alias')->textInput() ?>
        <?= $form->field($model, 'gender')->radioList($model::getGenders()); ?>
        <?= $form->field($user, 'country_id')->widget(Select2::classname(), [
            'data' => ArrayHelper::map(CountryLang::findAll(['language' => Yii::$app->language]), 'country_id', 'name'),
            'options' => ['placeholder' => Yii::t('main', 'Select a country')],
        ]); ?>
        <?= $form->field($user, 'timezone')->widget(Select2::classname(), [
            'data' => ArrayHelper::map(Timezone::findAll(['language' => Yii::$app->language]), 'timezone_id', 
                    function($model, $defaultValue) {
                            return $model->name . ' (' . $model->getOffset() . ' )';
                    }),
            'options' => ['placeholder' => Yii::t('main', 'Select a timezone')],
        ]); ?>
        <?= $form->field($model, 'free_appointment')->checkbox() ?>
        <?= $form->field($model, 'personal_fee')->textInput() ?>
        <?= $form->field($model, 'business_card_url')->textInput() ?>
        <?= $form->field($additional, 'business_card_image_alt')->textInput(); ?>

        <?php if ($model->image): ?>
            <div class="form-group">
                <label class="control-label"><?= Yii::t('backend', 'Current image'); ?></label>
                <div>
                    <?= Html::img($model->getFileUrl('image'), ['class' => 'img-thumbnail', 'alt' => '', 'width' => '128px']); ?>
                </div>
            </div>
        <?php endif; ?>
        <?= $form->field($model, 'image')->hiddenInput()->label(false); ?>
        <?= $form->field($model, 'imageFile')->fileInput() ?>
        <?= $form->field($additional, 'image_alt')->textInput(); ?>

        <?php if ($model->main_image): ?>
            <div class="form-group">
                <label class="control-label"><?= Yii::t('backend', 'Current main image'); ?></label>
                <div>
                    <?= Html::img($model->getFileUrl('main_image'), ['class' => 'img-thumbnail', 'alt' => '', 'width' => '128px']); ?>
                </div>
            </div>
        <?php endif; ?>
        <?= $form->field($model, 'main_image')->hiddenInput()->label(false); ?>
        <?= $form->field($model, 'main_imageFile')->fileInput() ?>
        <?= $form->field($additional, 'main_image_alt')->textInput(); ?>

        <?php if ($model->catalog_image): ?>
            <div class="form-group">
                <label class="control-label"><?= Yii::t('backend', 'Current main image'); ?></label>
                <div>
                    <?= Html::img($model->getFileUrl('catalog_image'), ['class' => 'img-thumbnail', 'alt' => '', 'width' => '128px']); ?>
                </div>
            </div>
        <?php endif; ?>
        <?= $form->field($model, 'catalog_image')->hiddenInput()->label(false); ?>
        <?= $form->field($model, 'catalog_imageFile')->fileInput() ?>
        <?= $form->field($additional, 'catalog_image_alt')->textInput(); ?>

        <?php if ($model->catalog_image_hover): ?>
            <div class="form-group">
                <label class="control-label"><?= Yii::t('backend', 'Current image'); ?></label>
                <div>
                    <?= Html::img($model->getFileUrl('catalog_image_hover'), ['class' => 'img-thumbnail', 'alt' => '', 'width' => '128px']); ?>
                </div>
            </div>
        <?php endif; ?>
        <?= $form->field($model, 'catalog_image_hover')->hiddenInput()->label(false); ?>
        <?= $form->field($model, 'catalog_image_hoverFile')->fileInput() ?>

        <?= $form->field($translation, 'short_description')->widget(Widget::className(), [
            'options' => ['id' => 'short_description-' . Yii::$app->language],
            'settings' => [
                'lang' => Language::getCurrent()->url,
                'minHeight' => 200,
                'limiter' => 1500,
                'plugins' => [
                    'limiter',
                ]
            ],
            'plugins' => [ 
                'lineheight' => RedactorAsset::class,
            ]
        ]);
        ?>

        <?= $form->field($additional, 'article_description')->widget(Widget::className(), [
            'options' => ['id' => 'article_description-' . Yii::$app->language],
            'settings' => [
                'lang' => Language::getCurrent()->url,
                'minHeight' => 200,
                'limiter' => 1500,
                'plugins' => [
                    'limiter',
                ]
            ],
            'plugins' => [
                'lineheight' => RedactorAsset::class,
            ]
        ]);
        ?>

        <?php foreach(['education', 'experience', 'full_description', 'terms'] as $attribute): ?>
            <?= $form->field($translation, $attribute, ['inputOptions' => ['id' => $attribute . '-' . Yii::$app->language]])->widget(Widget::className(), [
                'options' => ['id' => $attribute . '-' . Yii::$app->language],
                'settings' => [
                    'lang' => Language::getCurrent()->url,
                    'minHeight' => 180,
                    'imageUpload' => Url::to(['/redactor/image-upload']),
                    'plugins' => [
                        'imagemanager',
                        'video',
                        'fontcolor',
                        'fontfamily',
                        'fontsize',
                        'limiter',
                    ]
                ],
                'plugins' => [
                    'lineheight' => RedactorAsset::class,
                ]
            ]);
            ?>
        <?php endforeach; ?>

        <?= $form->field($additional, 'page_title')->textInput(); ?>
        <?= $form->field($additional, 'page_description')->textInput(); ?>

        <div class="form-group">
            <?= Html::submitButton('<i class="fa fa-save"></i> ' . ($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Save')), ['class' => $model->isNewRecord ? 'btn btn-success btn-sm' : 'btn btn-primary btn-sm']) ?>
            <a href="<?= Url::toRoute('index'); ?>" class="btn btn-danger btn-sm"><i class="fa fa-backward"></i> <?= Yii::t('backend', 'Cancel'); ?></a>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>