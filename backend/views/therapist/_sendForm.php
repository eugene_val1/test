<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
?>

<div class="new-message">
    <div class="row">
        <div class="col-sm-12 form-wrapper">  
            <?php
            $form = ActiveForm::begin([
                        'enableAjaxValidation' => false,
                        'enableClientValidation' => true,
                        'id' => 'report-errors',
            ]);
            ?>
            <?= $form->field($model, 'text')->textArea(['class' => "form-control", 'rows' => "7", 'id' => 'message-text']); ?>
            <div class="form-group form-submit">
                <?= Html::submitButton(Yii::t('main', 'Send'), ['class' => 'small-page-btn page-btn orange pull-right btn-submit']); ?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>  
    </div>
</div>