<?php

use common\models\Room;
use common\models\Therapist;
use common\widgets\Alert;
use kartik\widgets\DatePicker;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var yii\web\View $this */
/* @var backend\models\RoomSearch $searchModel */
/* @var yii\data\ActiveDataProvider $dataProvider */

$this->title = Yii::t('backend', 'Rooms');

?>

<div class="row mt">
    <div class="col-lg-10 col-lg-offset-1"><?= Alert::widget(); ?></div>
    <div class="col-lg-12">
        <div class="pull-right">
            <a href="<?= Url::toRoute('create'); ?>" class="btn btn-success btn-sm pull-right">
                <i class="fa fa-plus"></i> <?= Yii::t('backend', 'Create'); ?>
            </a>
        </div>
        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'tableOptions' => ['class' => 'table table-striped table-advance table-hover'],
            'columns' => [
                'name',
                [
                    'attribute' => 'user_name',
                    'label' => $searchModel->getAttributeLabel('user_id'),
                    'value' => function ($model) {
                        return $model->profile->type === Therapist::TYPE
                            ? $model->therapistLang->name
                            : $model->profile->name;
                    },
                ],
                [
                    'attribute' => 'created',
                    'filter' => DatePicker::widget([
                        'model' => $searchModel,
                        'attribute' => 'created',
                        'type' => 1,
                        'pluginOptions' => [
                            'autoclose' => true,
                            'format' => 'dd.mm.yyyy',
                        ]
                    ]),
                    'format' => 'html',
                    'value' => function ($model) {
                        return date('d.m.Y', $model->created);
                    },
                ],
                [
                    'label' => Yii::t('backend', 'Room link'),
                    'content' => function ($model) {
                        /** @var Room $model */

                        if ($model->user_id !== Yii::$app->user->id) {
                            return '';
                        }

                        return Html::a('<i class="fa fa-link"></i> ' . Yii::t('backend', 'Visit room'), $model->getUrl(), [
                            'class' => 'btn btn-success btn-sm',
                            'target' => '_blank',
                            'data-pjax' => 0,
                        ]);
                    },
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{update} {delete}',
                    'buttons' => [
                        'update' => function ($url, $model, $key) {
                            return Html::a('<i class="fa fa-pencil"></i> ', $url, [
                                'class' => 'btn btn-primary btn-sm',
                                'data-pjax' => 0,
                            ]);
                        },
                        'delete' => function ($url, $model, $key) {
                            return Html::a('<i class="fa fa-trash-o "></i>', $url, [
                                'class' => 'btn btn-danger btn-sm',
                                'data' => [
                                    'pjax' => 0,
                                    'method' => 'post',
                                    'confirm' => Yii::t('backend', 'Delete element?')
                                ],
                            ]);
                        }
                    ],
                ],
            ],
        ]);
        ?>

    </div>
</div>
