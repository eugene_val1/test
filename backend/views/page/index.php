<?php

/* @var $this yii\web\View */
/* @var $searchModel backend\models\PageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

use common\components\SiteManager;
use common\models\SystemPage;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = Yii::t('backend', 'Pages');

?>

<div class="row mt">
    <div class="col-lg-12">
        <div class="pull-right">
            <a href="<?= Url::toRoute('create'); ?>" class="btn btn-success btn-sm pull-right">
                <i class="fa fa-plus"></i> <?= Yii::t('backend', 'Create'); ?>
            </a>
        </div>
        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'tableOptions' => ['class' => 'table table-striped table-advance table-hover'],
            'columns' => [
                'name',
                [
                    'attribute' => 'type',
                    'filter' => $searchModel->getTypes(),
                    'value' => function ($model) {
                        return $model->getTypeName();
                    },
                ],
                [
                    'label' => Yii::t('backend', 'Page url'),
                    'format' => 'raw',
                    'value' => function ($model) {
                        return Html::a(Html::encode($model->getUrl()), SiteManager::getFrontendBaseUrl() . $model->getUrl(), ['target' => '_blank']);
                    },
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{update} {delete}',
                    'buttons' => [
                        'update' => function ($url, $model, $key) {
                            return Html::a('<i class="fa fa-pencil"></i> ', $url, [
                                'class' => 'btn btn-primary btn-sm',
                                'data-pjax' => 0,
                            ]);
                        },
                        'delete' => function ($url, $model, $key) {
                            if ($model->type == SystemPage::TYPE) {
                                return;
                            }

                            return Html::a('<i class="fa fa-trash-o "></i>', $url, [
                                'class' => 'btn btn-danger btn-sm',
                                'data' => [
                                    'pjax' => 0,
                                    'method' => 'post',
                                    'confirm' => Yii::t('backend', 'Delete element?'),
                                ],
                            ]);
                        }
                    ],
                ],
            ],
        ]);
        ?>

    </div>
</div>
