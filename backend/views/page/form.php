<?php
/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $form yii\widgets\ActiveForm */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use vova07\imperavi\Widget;
use common\models\Language;
use common\models\SystemPage;

$this->title = $model->isNewRecord ? Yii::t('backend', 'Create page') : Yii::t('backend', 'Update page') . ': ' . $model->name;
?>

<div class="row">
    <div class="col-lg-12">
        <?php $form = ActiveForm::begin(['options' => ['enctype'=>'multipart/form-data']]); ?>
        <?= $form->errorSummary($translation) ?>
        <?= $form->field($translation, 'name')->textInput() ?>
        <?= $form->field($model, 'type')->dropDownList(
            $model->getCreatableTypes(), 
            array_merge(
                ['prompt' => Yii::t('backend', 'Choose page type')], 
                $model->isNewRecord ? [] : ['disabled' => 'disabled']
            )
        ); ?>
        <?php if ($model->type != SystemPage::TYPE): ?>
        <?= $form->field($model, 'alias')->textInput() ?>
        <?=
        $form->field($translation, 'content')->widget(Widget::className(), [
            'settings' => [
                'lang' => Language::getCurrent()->url,
                'minHeight' => 200,
                'definedLinks' => '/site/links',
                'imageUpload' => Url::to(['/redactor/image-upload']),
                'imageManagerJson' => Url::to(['/redactor/images-get']),
                'plugins' => [
                    'fullscreen',
                    'imagemanager',
                    'fontcolor',
                    'video',
                    'fontfamily',
                    'fontsize',
                    'definedlinks'                    
                ]
            ],
            'plugins' => [ 
                'lineheight' => 'backend\assets\RedactorAsset',
                'clips' => 'backend\assets\RedactorAsset'
            ]
        ]);
        ?>
        <?php endif; ?>
        <?php if ($model->image): ?>
            <div class="form-group">
                <label class="control-label"><?= Yii::t('backend', 'Current image'); ?></label>
                <div>
                    <?= Html::img($model->getFileUrl('image'), ['class' => 'img-thumbnail', 'alt' => "", 'width' => '300px']); ?>
                </div>
            </div>
        <?php endif; ?>
        <?= $form->field($model, 'image')->hiddenInput()->label(false); ?>
        <?= $form->field($model, 'imageFile')->fileInput() ?>
        <?= $form->field($translation, 'title')->textInput() ?>
        <?= $form->field($translation, 'keywords')->textarea(); ?>
        <?= $form->field($translation, 'description')->textarea(); ?>
        <?= $form->field($model, 'noindex')->checkbox(); ?>
        <?= $form->field($model, 'nofollow')->checkbox(); ?>
        <div class="form-group">
            <?= Html::submitButton('<i class="fa fa-save"></i> ' . ($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Save')), ['class' => $model->isNewRecord ? 'btn btn-success btn-sm' : 'btn btn-primary btn-sm']) ?>
            <a href="<?= Url::toRoute('index'); ?>" class="btn btn-danger btn-sm"><i class="fa fa-backward"></i> <?= Yii::t('backend', 'Cancel'); ?></a>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>