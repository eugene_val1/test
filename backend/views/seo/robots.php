<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = Yii::t('backend', 'Robots.txt');
?>

<div class="row">
    <div class="col-lg-12">
        <?php $form = ActiveForm::begin(); ?>       
        <?= $form->field($model, 'content')->textarea(['rows' => 20]); ?>
        <div class="form-group">
            <?= Html::submitButton('<i class="fa fa-save"></i> ' . Yii::t('backend', 'Save'), ['class' => 'btn btn-success btn-sm']) ?>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>