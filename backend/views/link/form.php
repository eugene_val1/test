<?php
/* @var $this yii\web\View */
/* @var $model common\models\Link */
/* @var $form yii\widgets\ActiveForm */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\Menu;

$this->title = $model->isNewRecord ? Yii::t('backend', 'Create link') : Yii::t('backend', 'Update link') . ': ' . $model->title;
?>

<div class="row">
    <div class="col-lg-12">
        <?php $form = ActiveForm::begin(); ?>
        <?= $form->field($model, 'menu_id')->dropDownList(ArrayHelper::map(Menu::find()->all(), 'id', 'title'),['prompt'=>'']); ?>
        <?= $form->field($translation, 'title')->textInput() ?>
        <?= $form->field($model, 'url')->textInput() ?>
        <?= $form->field($model, 'modal')->checkbox() ?>
        <?= $form->field($model, 'blank')->checkbox() ?>
        <?= $form->field($model, 'icon')->textInput() ?>
        <?= $form->field($model, 'template')->dropDownList($model->getTemplates()); ?>
        <div class="form-group">
            <?= Html::submitButton('<i class="fa fa-save"></i> ' . ($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Save')), ['class' => $model->isNewRecord ? 'btn btn-success btn-sm' : 'btn btn-primary btn-sm']) ?>
            <a href="<?= Url::toRoute(['index', 'id' => $model->menu_id]); ?>" class="btn btn-danger btn-sm"><i class="fa fa-backward"></i> <?= Yii::t('backend', 'Cancel'); ?></a>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>