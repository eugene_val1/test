<?php

/* @var $this yii\web\View */
/* @var $searchModel backend\models\FeatureSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;

$this->title = Yii::t('backend', 'Menu');

?>

<div class="row mt">

    <div class="col-lg-12">

        <div class="pull-right">
            <a href="<?= Url::toRoute('create'); ?>" class="btn btn-success btn-sm pull-right">
                <i class="fa fa-plus"></i> <?= Yii::t('backend', 'Create'); ?>
            </a>
        </div>

        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'tableOptions' => ['class' => 'table table-striped table-advance table-hover'],
            'columns' => [
                'title',
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{links} {update} {delete}',
                    'buttons' => [
                        'links' => function ($url, $model, $key) {
                            return Html::a('<i class="fa fa-eye"></i> ' . Yii::t('backend', 'Links'), ['link/index', 'id' => $model->id], [
                                'class' => 'btn btn-success btn-sm',
                                'data-pjax' => 0,
                            ]);
                        },
                        'update' => function ($url, $model, $key) {
                            return Html::a('<i class="fa fa-pencil"></i> ' . Yii::t('backend', 'Edit'), $url, [
                                'class' => 'btn btn-primary btn-sm',
                                'data-pjax' => 0,
                            ]);
                        },
                        'delete' => function ($url, $model, $key) {
                            return Html::a('<i class="fa fa-trash-o "></i> ' . Yii::t('backend', 'Delete'), $url, [
                                'class' => 'btn btn-danger btn-sm',
                                'data' => ['pjax' => 0, 'method' => 'post', 'confirm' => Yii::t('backend', 'Delete element?')],
                            ]);
                        }
                    ],
                ],
            ],
        ]);
        ?>

    </div>
</div>
