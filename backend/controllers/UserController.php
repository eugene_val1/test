<?php

namespace backend\controllers;

use backend\models\UserExportForm;
use Yii;
use yii\base\Model;
use common\models\User;
use backend\models\UserSearch;
use common\models\Profile;
use common\models\Country;
use common\models\Timezone;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $user = new User(['status' => User::STATUS_ACTIVE]);
        $user->scenario = 'create-admin';
        
        $country = Country::getCurrentByIP();
        $timezone = Timezone::getCurrentByIP();
        $user->country_id = ($country) ? $country->id : null;
        $user->timezone = ($timezone) ?: null;
        
        $profile = new Profile(['type' => Profile::TYPE]);

        if ($user->load(Yii::$app->request->post()) && $profile->load(Yii::$app->request->post()) && Model::validateMultiple([$user, $profile])) {
            $user->setPassword($user->user_password);
            $user->generateAuthKey();
            $user->save(false);
            $profile->user_id = $user->id;
            $profile->save(false);
            
            return $this->redirect(['view', 'id' => $user->id]);
        } else {
            return $this->render('form', [
                'user' => $user,
                'profile' => $profile,
            ]);
        }
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $user = $this->findModel($id);
        $profile= $user->profile;
        if ($user->load(Yii::$app->request->post()) && $profile->load(Yii::$app->request->post()) && Model::validateMultiple([$user, $profile])) {

            if($user->user_password){
                $user->setPassword($user->user_password);
                $user->generateAuthKey();
            }
            
            $user->save(false);
            $profile->save(false);
    
            return $this->redirect(['view', 'id' => $user->id]);
        } else {
            return $this->render('form', [
                'user' => $user,
                'profile' => $profile,
            ]);
        }
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $user = $this->findModel($id);
        $user->delete();
        return $this->redirect(['index']);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Export email to CSV
     */
    public function actionExport()
    {
        $model = new UserExportForm();

        if (Yii::$app->request->isPost && $model->load(Yii::$app->request->post())) {
            $model->exportCSV();
        }

        return $this->render('export', [
            'model' => $model
        ]);
    }

    /**
     * Login as user
     * @param int $id
     * @return \yii\web\Response
     */
    public function actionLoginAs($id)
    {
        $user = $this->findModel((int)$id);

        if ($id == Yii::$app->user->getId() || empty($user)) {
            return $this->redirect('/user/index');
        }

        if (YII_ENV_DEV) {
            $url = Yii::$app->params['testBaseUrl'];
        } else {
            $url = Yii::$app->params['baseUrl'];
        }

        $url .= Url::to([
            '/user/autologin',
            'h' => Yii::$app->params['autologinHash'],
            'id' => $user->id
        ]);

        return $this->redirect($url);
    }

}
