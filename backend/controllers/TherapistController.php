<?php

namespace backend\controllers;

use backend\models\ReportErrorsForm;
use backend\models\TherapistSearch;
use common\components\MailHelper;
use common\models\Certificate;
use common\models\Country;
use common\models\Profile;
use common\models\RegionPrice;
use common\models\Therapist;
use common\models\TherapistAdditional;
use common\models\TherapistLang;
use common\models\Timezone;
use common\models\User;
use himiklab\sortablegrid\SortableGridAction;
use Yii;
use yii\base\Model;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Inflector;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * TherapistController implements the CRUD actions for Therapist model.
 */
class TherapistController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['editor'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'publish' => ['post'],
                    'unpublish' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @return array
     */
    public function actions()
    {
        return [
            'sort' => [
                'class' => SortableGridAction::className(),
                'modelName' => Therapist::className(),
            ],
        ];
    }

    /**
     * Lists all Therapist models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TherapistSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Therapist model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $therapist = $this->findModel($id);

        $certificates = Certificate::find()
            ->where(['therapist_id' => $therapist->user_id])
            ->joinWith('translation')
            ->all();

        $prices = RegionPrice::find()
            ->where(['therapist_id' => $therapist->user_id])
            ->joinWith('region')
            ->all();

        return $this->render('view', [
            'therapist' => $therapist,
            'certificates' => $certificates,
            'prices' => $prices,
        ]);
    }

    /**
     * Creates a new Therapist model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $user = new User(['role' => User::ROLE_THERAPIST, 'status' => User::STATUS_ACTIVE]);
        $user->scenario = 'create-admin';

        $country = Country::getCurrentByIP();
        $timezone = Timezone::getCurrentByIP();
        $user->country_id = ($country) ? $country->id : null;
        $user->timezone = ($timezone) ?: null;

        $therapist = new Therapist(['type' => Therapist::TYPE]);
        $translation = new TherapistLang(['language' => Yii::$app->sourceLanguage]);
        $additional = new TherapistAdditional();

        if (!$therapist->alias) {
            $therapist->alias = Inflector::slug($translation->name);
        }

        $loadedModels = $user->load(Yii::$app->request->post())
            && $therapist->load(Yii::$app->request->post())
            && $translation->load(Yii::$app->request->post());

        if ($loadedModels && Model::validateMultiple([$user, $therapist, $translation, $additional])) {
            $user->setPassword($user->user_password);
            $user->generateAuthKey();
            $user->save(false);
            $therapist->user_id = $user->id;
            $therapist->save(false);

            $translation->user_id = $user->id;
            $translation->save(false);

            $additional->user_id = $user->id;
            $additional->save(false);

            $emailMessage = Yii::$app->mailer->compose('therapistAccount', ['user' => $user])
                ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name])
                ->setTo($user->email)
                ->setSubject(Yii::t('backend', 'Your therapist account created'));

            //sign message
            MailHelper::signMessage($emailMessage);

            $emailMessage->send();

            $message = Yii::t('backend', 'Therapist added.') . '<br>' .
                Yii::t('backend', 'With login:') . ' ' . $user->username . ' ' .
                Yii::t('backend', 'and password:') . ' ' . $user->user_password . '<br>' .
                Yii::t('backend', 'This information sent also on email:') . ' ' . $user->email;

            Yii::$app->getSession()->setFlash('success', $message);

            return $this->redirect(['view', 'id' => $therapist->id]);
        }

        return $this->render('form', [
            'user' => $user,
            'model' => $therapist,
            'translation' => $translation,
            'additional' => $additional,
        ]);
    }

    /**
     * @param $id
     * @return string|\yii\web\Response
     */
    public function actionUpdate($id)
    {
        $therapist = $this->findModel($id);
        $translation = $therapist->translation;
        $user = $therapist->user;
        $additional = $therapist->additional;
        if (!$additional) {
            $additional = new TherapistAdditional([
                'user_id' => $therapist->user_id,
            ]);
            $additional->save();
        }

        $loadedModels = $user->load(Yii::$app->request->post())
            && $therapist->load(Yii::$app->request->post())
            && $translation->load(Yii::$app->request->post())
            && $additional->load(Yii::$app->request->post());

        if ($loadedModels && Model::validateMultiple([$user, $therapist, $translation])) {
            if ($user->user_password) {
                $user->setPassword($user->user_password);
                $user->generateAuthKey();
            }

            $user->save(false);
            $therapist->save(false);
            $translation->save(false);
            $additional->save(false);

            return $this->redirect(['view', 'id' => $therapist->id]);
        }

        return $this->render('form', [
            'model' => $therapist,
            'user' => $user,
            'translation' => $translation,
            'additional' => $additional,
        ]);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->user->delete();

        return $this->redirect(['index']);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     */
    public function actionPublish($id)
    {
        $this->findModel($id)->setStatus(Therapist::STATUS_PUBLISHED);

        Yii::$app->getSession()
            ->setFlash('success', Yii::t('backend', 'Therapist status set to published. Therapist is visible in catalog now'));

        return $this->redirect(['view', 'id' => $id]);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     */
    public function actionUnpublish($id)
    {
        $this->findModel($id)->setStatus(Therapist::STATUS_UNPUBLISHED);

        Yii::$app->getSession()
            ->setFlash('danger', Yii::t('backend', 'Therapist status set to unpublished. Therapist is not visible in catalog now'));

        return $this->redirect(['view', 'id' => $id]);
    }

    /**
     * @param $id
     * @return Therapist
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        $model = Therapist::findOne($id);

        if (!$model) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        return $model;
    }

    /**
     * @param $id
     * @return array
     * @throws NotFoundHttpException
     */
    public function actionCertificate($id = null)
    {
        Yii::$app->response->format = 'json';

        $certificate = Certificate::find()
            ->where(['`certificate`.`id`' => $id])
            ->joinWith('translation')
            ->one();

        if (!$certificate) {
            throw new NotFoundHttpException;
        }

        return [
            'result' => true,
            'html' => $this->renderPartial('_certificate', [
                'model' => $certificate,
            ])
        ];
    }

    /**
     * @param $id
     * @return array
     * @throws NotFoundHttpException
     */
    public function actionSend($id = null)
    {
        Yii::$app->response->format = 'json';

        $profile = Profile::findOne($id);
        if (!$profile) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        $message = false;
        $model = new ReportErrorsForm();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->sendEmail($profile->user);
            $profile->setStatus(Therapist::STATUS_REVIEWED);
            $message = Yii::t('main', 'Your message sent to therapist email');
        }

        return [
            'result' => true,
            'errors' => $model->hasErrors(),
            'message' => $message,
            'html' => $this->renderAjax('_sendForm', [
                'model' => $model,
                'id' => $id,
            ])
        ];
    }
}
