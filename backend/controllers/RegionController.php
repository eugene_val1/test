<?php

namespace backend\controllers;

use Yii;
use yii\base\Model;
use common\models\Region;
use backend\models\RegionSearch;
use common\models\RegionLang;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use himiklab\sortablegrid\SortableGridAction;

/**
 * RegionController implements the CRUD actions for Region model.
 */
class RegionController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }
    
    public function actions()
    {
        return [
            'sort' => [
                'class' => SortableGridAction::className(),
                'modelName' => Region::className(),
            ],
        ];
    }

    /**
     * Lists all Region models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new RegionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Region model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Region model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $region = new Region();
        $translation = new RegionLang(['language' => Yii::$app->sourceLanguage]);

        if ($translation->load(Yii::$app->request->post()) && Model::validateMultiple([$region, $translation])) {
            $region->save(false);
            
            $translation->region_id = $region->id;
            $translation->save(false);
            return $this->redirect(['view', 'id' => $region->id]);
        } else {
            return $this->render('form', [
                'model' => $region,
                'translation' => $translation,
            ]);
        }
    }

    /**
     * Updates an existing Region model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $region = $this->findModel($id);
        $translation = $region->translation;
            
        if ($translation->load(Yii::$app->request->post()) && Model::validateMultiple([$region, $translation])) {            
            $region->save(false);
            $translation->save(false);
            return $this->redirect(['view', 'id' => $region->id]);
        } else {
            return $this->render('form', [
                'model' => $region,
                'translation' => $translation,
            ]);
        }
    }

    /**
     * Deletes an existing Region model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $region = $this->findModel($id);
        
        if(count($region->countries) > 0){
            Yii::$app->getSession()->setFlash('danger', Yii::t('backend', 'Unable to delete. There are countries in this region'));
        } else {
            $region->delete();
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the Region model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Region the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Region::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
