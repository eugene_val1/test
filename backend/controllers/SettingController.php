<?php

namespace backend\controllers;

use Yii;
use common\models\Setting;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\base\Model;

/**
 * EventController.
 */
class SettingController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Setting.
     * @return mixed
     */
    public function actionIndex()
    {
        $settings = Setting::find()->with('translation')->indexBy('id')->all();
        $sortedSettings = [];
        
        foreach($settings as $key => $setting){
            if($setting->type == 'multilangual'){
                $setting->translation->title = $setting->title;
                $settings[$key] = $setting->translation;
            }
            $sortedSettings[$setting->category][$key] = $settings[$key];          
        }

        if (Model::loadMultiple($settings, Yii::$app->request->post(), 'Setting') &&
             Model::loadMultiple($settings, Yii::$app->request->post(), 'SettingLang') &&
             Model::validateMultiple($settings)) {
            foreach ($settings as $setting) {
                $setting->save(false);
            }
            
            Yii::$app->getSession()->setFlash('success', Yii::t('backend', 'Settings saved'));
            return $this->redirect('index');
        }
        
        return $this->render('index', [
            'settings' => $sortedSettings,
        ]);
    }
}
