<?php

namespace backend\controllers;

use Yii;
use yii\base\Model;
use common\models\FaqCategory;
use common\models\Faq;
use backend\models\FaqCategorySearch;
use common\models\FaqCategoryLang;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use himiklab\sortablegrid\SortableGridAction;
use yii\helpers\Inflector;

/**
 * FaqCategoryController implements the CRUD actions for FaqCategory model.
 */
class FaqCategoryController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['editor'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }
    
    public function actions()
    {
        return [
            'sort' => [
                'class' => SortableGridAction::className(),
                'modelName' => FaqCategory::className(),
            ],
        ];
    }

    /**
     * Lists all FaqCategory models.
     * @return mixed
     */
    public function actionIndex($id = 0)
    {
        $parent = FaqCategory::findOne($id);
        $searchModel = new FaqCategorySearch(['parent_id' => $id]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'parent' => $parent
        ]);
    }

    /**
     * Displays a single FaqCategory model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new FaqCategory model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($parent_id = 0)
    {
        $model = new FaqCategory(['parent_id' => $parent_id]);
        $translation = new FaqCategoryLang(['language' => Yii::$app->sourceLanguage]);

        if ($translation->load(Yii::$app->request->post()) && Model::validateMultiple([$model, $translation])) {
            $model->alias = Inflector::slug($translation->name);            
            $model->save(false);
            
            $translation->faq_category_id = $model->id;
            $translation->save(false);
            return $this->redirect(['index', 'id' => $model->parent_id]);
        } else {
            return $this->render('form', [
                'model' => $model,
                'translation' => $translation,
            ]);
        }
    }

    /**
     * Updates an existing FaqCategory model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $translation = $model->translation;
            
        if ($translation->load(Yii::$app->request->post()) && Model::validateMultiple([$model, $translation])) {            
            $model->save(false);
            $translation->save(false);
            return $this->redirect(['index', 'id' => $model->parent_id]);
        } else {
            return $this->render('form', [
                'model' => $model,
                'translation' => $translation,
            ]);
        }
    }

    /**
     * Deletes an existing FaqCategory model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        FaqCategory::deleteAll(['id' => $model->withChildren()]);
        Faq::deleteAll(['category_id' => $model->withChildren()]);
        return $this->redirect(['index']);
    }

    /**
     * Finds the FaqCategory model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return FaqCategory the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = FaqCategory::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
