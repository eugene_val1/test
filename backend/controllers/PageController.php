<?php

namespace backend\controllers;

use Yii;
use yii\base\Model;
use common\models\Page;
use common\models\PageLang;
use backend\models\PageSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Inflector;

/**
 * Class PageController
 *
 * @package backend\controllers
 */
class PageController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['editor'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Page models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PageSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Page model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Page model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $page = new Page();
        $translation = new PageLang(['language' => Yii::$app->sourceLanguage]);

        if (
            $page->load(Yii::$app->request->post())
            && $translation->load(Yii::$app->request->post())
            && Model::validateMultiple([$page, $translation])
        ) {
            if (!$page->alias) {
                $page->alias = Inflector::slug($translation->name);
            }
            $page->save(false);

            $translation->page_id = $page->id;
            $translation->save(false);

            return $this->redirect(['view', 'id' => $page->id]);
        }

        return $this->render('form', [
            'model' => $page,
            'translation' => $translation,
        ]);
    }

    /**
     * Updates an existing Page model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $page = $this->findModel($id);
        $translation = $page->translation;

        if (
            $page->load(Yii::$app->request->post())
            && $translation->load(Yii::$app->request->post())
            && Model::validateMultiple([$page, $translation])
        ) {
            $page->save(false);
            $translation->save(false);
            return $this->redirect(['view', 'id' => $page->id]);
        }

        return $this->render('form', [
            'model' => $page,
            'translation' => $translation,
        ]);
    }

    /**
     * Deletes an existing Page model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Page model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     * @return Page the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        $model = Page::findOne($id);

        if (!$model) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        return $model;
    }
}
