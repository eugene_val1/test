<?php

namespace backend\controllers;

use Yii;
use yii\base\Model;
use common\models\Link;
use backend\models\LinkSearch;
use common\models\LinkLang;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use himiklab\sortablegrid\SortableGridAction;

/**
 * LinkController implements the CRUD actions for Link model.
 */
class LinkController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }
    
    public function actions()
    {
        return [
            'sort' => [
                'class' => SortableGridAction::className(),
                'modelName' => Link::className(),
            ],
        ];
    }

    /**
     * Lists all Link models.
     * @return mixed
     */
    public function actionIndex($id = 0)
    {
        $id = (int)$id;
        $searchModel = new LinkSearch();
        $searchModel->menu_id = $id;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Link model.
     * If creation is successful, the browser will be redirected to the 'view' link.
     * @return mixed
     */
    public function actionCreate($id)
    {
        $link = new Link();
        $link->menu_id = (int)$id;
        $translation = new LinkLang(['language' => Yii::$app->sourceLanguage]);

        if ($link->load(Yii::$app->request->post()) && $translation->load(Yii::$app->request->post()) && Model::validateMultiple([$link, $translation])) {
            $link->save(false);
            $translation->link_id = $link->id;
            $translation->save(false);
            return $this->redirect(['index', 'id' => $link->menu_id]);
        } else {
            return $this->render('form', [
                'model' => $link,
                'translation' => $translation,
            ]);
        }
    }

    /**
     * Updates an existing Link model.
     * If update is successful, the browser will be redirected to the 'view' link.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $link = $this->findModel($id);
        $translation = $link->translation;
            
        if ($link->load(Yii::$app->request->post()) && $translation->load(Yii::$app->request->post()) && Model::validateMultiple([$link, $translation])) {
            $link->save(false);
            $translation->save(false);
            return $this->redirect(['index', 'id' => $link->menu_id]);
        } else {
            return $this->render('form', [
                'model' => $link,
                'translation' => $translation,
            ]);
        }
    }

    /**
     * Deletes an existing Link model.
     * If deletion is successful, the browser will be redirected to the 'index' link.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $link = $this->findModel($id);
        $link->delete();
        return $this->redirect(['index']);
    }

    /**
     * Finds the Link model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Link the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Link::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested link does not exist.');
        }
    }
}
