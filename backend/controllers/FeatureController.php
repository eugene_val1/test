<?php

namespace backend\controllers;

use Yii;
use yii\base\Model;
use common\models\Feature;
use backend\models\FeatureSearch;
use common\models\FeatureLang;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use himiklab\sortablegrid\SortableGridAction;

/**
 * FeatureController implements the CRUD actions for Feature model.
 */
class FeatureController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['editor'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }
    
    public function actions()
    {
        return [
            'sort' => [
                'class' => SortableGridAction::className(),
                'modelName' => Feature::className(),
            ],
        ];
    }

    /**
     * Lists all Feature models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new FeatureSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Feature model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Feature model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $feature = new Feature();
        $translation = new FeatureLang(['language' => Yii::$app->sourceLanguage]);
        if ($feature->load(Yii::$app->request->post()) && $translation->load(Yii::$app->request->post()) && Model::validateMultiple([$feature, $translation])) {
            $feature->save(false);            
            $translation->feature_id = $feature->id;
            $translation->save(false);
            return $this->redirect(['view', 'id' => $feature->id]);
        } else {
            return $this->render('form', [
                'model' => $feature,
                'translation' => $translation,
            ]);
        }
    }

    /**
     * Updates an existing Feature model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $feature = $this->findModel($id);
        $translation = $feature->translation;
            
        if ($feature->load(Yii::$app->request->post()) && $translation->load(Yii::$app->request->post()) && Model::validateMultiple([$feature, $translation])) {
            $feature->save(false);
            $translation->save(false);
            return $this->redirect(['view', 'id' => $feature->id]);
        } else {
            return $this->render('form', [
                'model' => $feature,
                'translation' => $translation,
            ]);
        }
    }

    /**
     * Deletes an existing Feature model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $feature = $this->findModel($id);
        $feature->delete();
        return $this->redirect(['index']);
    }

    /**
     * Finds the Feature model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Feature the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Feature::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
