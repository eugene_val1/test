<?php

namespace backend\controllers;

use backend\models\LanguageSearch;
use common\models\Language;
use common\models\SourceTranslationMessage;
use common\models\TranslationMessage;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * LanguageController implements the CRUD actions for Language model.
 */
class LanguageController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['editor'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Language models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new LanguageSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Course model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Updates an existing Language model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->save(false);
            return $this->redirect('index');
        } else {
            return $this->render('form', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Displays translations fields.
     * @param integer $id
     * @return mixed
     */
    public function actionTranslations($id)
    {
        $language = $this->findModel($id);
        $sourceMessages = SourceTranslationMessage::find()
            ->with('translations')
            ->indexBy('id')
            ->all();
        $messages = [];

        foreach ($sourceMessages as $sourceMessage) {
            $messages[$sourceMessage->category][$sourceMessage->id] = (isset($sourceMessage->translations[$language->local]))
                ? $sourceMessage->translations[$language->local]
                : new TranslationMessage(['id' => $sourceMessage->id, 'language' => $language->local]);
        }

        if (isset($_POST['TranslationMessage'])) {
            foreach (SourceTranslationMessage::getCategories() as $category => $categoryName) {
                foreach ($messages[$category] as $id => $message) {
                    if (isset($_POST['TranslationMessage'][$id])) {
                        $message->attributes = $_POST['TranslationMessage'][$id];

                        if ($message->validate()) {
                            $message->save(false);
                        }
                    }
                }
            }
        }

        return $this->render('translations', [
            'language' => $language,
            'sourceMessages' => $sourceMessages,
            'messages' => $messages,
        ]);
    }

    /**
     * Finds the Language model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Language the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Language::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
