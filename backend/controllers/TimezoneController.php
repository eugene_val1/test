<?php

namespace backend\controllers;

use Yii;
use yii\base\Model;
use common\models\Timezone;
use backend\models\TimezoneSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * TimezoneController implements the CRUD actions for Timezone model.
 */
class TimezoneController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Timezone models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TimezoneSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Timezone model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Timezone model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $timezone = new Timezone(['language' => Yii::$app->sourceLanguage]);

        if ($timezone->load(Yii::$app->request->post()) && $timezone->validate()) {
            $timezone->save(false);
            return $this->redirect(['view', 'id' => $timezone->id]);
        } else {
            return $this->render('form', [
                'model' => $timezone,
            ]);
        }
    }

    /**
     * Updates an existing Timezone model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $timezone = $this->findModel($id);
            
        if ($timezone->load(Yii::$app->request->post()) && $timezone->validate()) {            
            $timezone->save(false);
            return $this->redirect(['view', 'id' => $timezone->id]);
        } else {
            return $this->render('form', [
                'model' => $timezone,
            ]);
        }
    }

    /**
     * Deletes an existing Timezone model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        Timezone::deleteAll('timezone_id = :timezone', [':timezone' => $id]);
        return $this->redirect(['index']);
    }

    /**
     * Finds the Timezone model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Timezone the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Timezone::find()->where('timezone_id = :timezone AND language = :language', [':timezone' => $id, ':language' => Yii::$app->language])->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
