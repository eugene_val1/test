<?php

namespace backend\controllers;

use Yii;
use yii\base\Model;
use common\models\Property;
use backend\models\PropertySearch;
use common\models\PropertyLang;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use himiklab\sortablegrid\SortableGridAction;
use yii\helpers\Url;

/**
 * PropertyController implements the CRUD actions for Property model.
 */
class PropertyController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }
    
    public function actions()
    {
        return [
            'sort' => [
                'class' => SortableGridAction::className(),
                'modelName' => Property::className(),
            ],
        ];
    }

    /**
     * Lists all Property models.
     * @return mixed
     */
    public function actionIndex()
    {
        Url::remember('', $this->id);
        
        $searchModel = new PropertySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Property model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $property = new Property();
        $translation = new PropertyLang(['language' => Yii::$app->sourceLanguage]);

        if ($translation->load(Yii::$app->request->post()) && $property->load(Yii::$app->request->post()) && Model::validateMultiple([$property, $translation])) {
            $property->save(false);
            
            $translation->property_id = $property->id;
            $translation->save(false);
            return $this->redirect((Url::previous($this->id)) ?: ['index']);
        } else {
            return $this->render('form', [
                'model' => $property,
                'translation' => $translation,
            ]);
        }
    }

    /**
     * Updates an existing Property model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $property = $this->findModel($id);
        $translation = $property->translation;
            
        if ($translation->load(Yii::$app->request->post()) && $property->load(Yii::$app->request->post()) && Model::validateMultiple([$property, $translation])) {            
            $property->save(false);
            $translation->save(false);
            return $this->redirect((Url::previous($this->id)) ?: ['index']);
        } else {
            return $this->render('form', [
                'model' => $property,
                'translation' => $translation,
            ]);
        }
    }

    /**
     * Deletes an existing Property model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $property = $this->findModel($id);
        $property->delete();
       
        return $this->redirect((Url::previous($this->id)) ?: ['index']);
    }

    /**
     * Finds the Property model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Property the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Property::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
