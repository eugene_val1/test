<?php

namespace backend\controllers;

use Yii;
use yii\base\Model;
use common\models\Menu;
use backend\models\MenuSearch;
use common\models\MenuLang;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * MenuController implements the CRUD actions for Menu model.
 */
class MenuController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Menu models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MenuSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Menu model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Menu model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $menu = new Menu();
        $translation = new MenuLang(['language' => Yii::$app->sourceLanguage]);

        if ($menu->load(Yii::$app->request->post()) && $translation->load(Yii::$app->request->post()) && Model::validateMultiple([$menu, $translation])) {
            $menu->save(false);
            $translation->menu_id = $menu->id;
            $translation->save(false);
            return $this->redirect(['index']);
        } else {
            return $this->render('form', [
                'model' => $menu,
                'translation' => $translation,
            ]);
        }
    }

    /**
     * Updates an existing Menu model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $menu = $this->findModel($id);
        $translation = $menu->translation;
            
        if ($menu->load(Yii::$app->request->post()) && $translation->load(Yii::$app->request->post()) && Model::validateMultiple([$menu, $translation])) {
            $menu->save(false);
            $translation->save(false);
            return $this->redirect(['index']);
        } else {
            return $this->render('form', [
                'model' => $menu,
                'translation' => $translation,
            ]);
        }
    }

    /**
     * Deletes an existing Menu model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $menu = $this->findModel($id);
        $menu->delete();
        return $this->redirect(['index']);
    }

    /**
     * Finds the Menu model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Menu the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Menu::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
