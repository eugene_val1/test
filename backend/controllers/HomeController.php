<?php

namespace backend\controllers;

use Yii;
use yii\base\Model;
use common\models\HomeSlide;
use backend\models\HomeSlideSearch;
use common\models\HomeSlideLang;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use himiklab\sortablegrid\SortableGridAction;
use yii\widgets\ActiveForm;

/**
 * HomeController implements the CRUD actions for HomeSlide model.
 */
class HomeController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['editor'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }
    
    public function actions()
    {
        return [
            'sort' => [
                'class' => SortableGridAction::className(),
                'modelName' => HomeSlide::className(),
            ],
        ];
    }

    /**
     * Lists all HomeSlide models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new HomeSlideSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single HomeSlide model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new HomeSlide model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $homeSlide = new HomeSlide();
        $translation = new HomeSlideLang(['language' => Yii::$app->sourceLanguage]);
        $typeModel = '';
        
        if ($homeSlide->load(Yii::$app->request->post()) && $translation->load(Yii::$app->request->post())) {
            $valid = Model::validateMultiple([$homeSlide, $translation]);
            
            if($homeSlide->type){
                $typeModel = $this->getSlideClass($homeSlide->type);
                $typeModel->type = $homeSlide->type;
                $typeModel->load(Yii::$app->request->post());
                
                $valid = $typeModel->validate() && $valid;
            }
            
            if($valid){
                $homeSlide->data = ($typeModel !== '') ? Json::encode($typeModel->getData()) : '';
                $homeSlide->save(false);
                $translation->home_slide_id = $homeSlide->id;
                $translation->save(false);
                return $this->redirect(['index']);
            }
        }
        
        return $this->render('form', [
            'model' => $homeSlide,
            'translation' => $translation, 
            'typeModel' => $typeModel
        ]);
    }

    /**
     * Updates an existing HomeSlide model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $homeSlide = $typeModel = $this->findModel($id);
        $translation = $homeSlide->translation;
        
        if ($homeSlide->load(Yii::$app->request->post()) && $translation->load(Yii::$app->request->post()) && Model::validateMultiple([$homeSlide, $translation])) {

            $homeSlide->data = Json::encode($typeModel->getData());
            $homeSlide->save(false);
            $translation->save(false);
            return $this->redirect(['index']);
        }

        return $this->render('form', [
            'model' => $homeSlide,
            'translation' => $translation, 
            'typeModel' => $typeModel
        ]);
    }

    /**
     * Deletes an existing HomeSlide model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $homeSlide = $this->findModel($id);
        $homeSlide->delete();
        return $this->redirect(['index']);
    }

    /**
     * Finds the HomeSlide model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return HomeSlide the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = HomeSlide::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    protected function getSlideClass($type){
        $modelClass = '\common\models\Home' . ucfirst($type) . 'Slide';
        return new $modelClass();
    }
    
    public function actionType()
    {
        $type = Yii::$app->request->post('type');
        $homeSlide = $this->getSlideClass($type);
        $translation = new HomeSlideLang(['language' => Yii::$app->sourceLanguage]);

        return $this->renderPartial('_' . $type, [
            'model' => $homeSlide,
            'translation' => $translation,
            'form' => new ActiveForm()
        ]);
    }
}
