<?php

namespace backend\controllers;

use Yii;
use common\models\Article;
use backend\models\ArticleSearch;
use common\models\VideoArticle;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use backend\models\ReportErrorsForm;
use yii\helpers\Url;
use common\models\Tag;
use yii\db\Query;

/**
 * ArticleController implements the CRUD actions for Article model.
 */
class ArticleController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['editor'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Article models.
     * @return mixed
     */
    public function actionIndex()
    {
        Url::remember('', 'articles');

        $searchModel = new ArticleSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Article model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Article model.
     * If creation is successful, the browser will be redirected to the 'index' page.
     * @return mixed
     */
    public function actionCreate($type)
    {
        $article = $this->getArticleClass($type);
        $article->published = Article::STATUS_PUBLISHED;

        if ($article->load(Yii::$app->request->post()) && $article->validate()) {
            $article->save(false);
            return $this->redirect(['/article/view', 'id' => $article->id]);
        }

        return $this->render('form', [
            'model' => $article
        ]);
    }

    /**
     * Updates an existing Article model.
     * If update is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $article = $this->findModel($id);
        if ($article->load(Yii::$app->request->post()) && $article->validate()) {
            $article->save(false);
            return $this->redirect(['/article/view', 'id' => $id]);
        }

        return $this->render('form', [
            'model' => $article
        ]);
    }

    /**
     * Deletes an existing Article model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $article = $this->findModel($id);
        $article->delete();

        return $this->redirect(Url::previous('articles'));
    }

    /**
     * Finds the Article model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     * @return Article the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Article::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * @param $type
     * @return mixed
     */
    protected function getArticleClass($type)
    {
        $modelClass = '\common\models\\' . ucfirst($type) . 'Article';

        return new $modelClass(['type' => $type]);
    }

    /**
     * @return array
     */
    public function actionParseVideoUrl()
    {
        Yii::$app->response->format = 'json';

        $source = array();
        $url = Yii::$app->request->post('url');
        $type = Yii::$app->request->post('type');

        if ($url && $type) {
            $sourceId = VideoArticle::parseSourceUrl($url, $type);
            $source['video'] = VideoArticle::getVideoCode($type, $sourceId);
            $source['thumb'] = '<img src="' . VideoArticle::getThumbUrl($type, $sourceId) . '" alt="" >';
            $source['sourceId'] = $sourceId;
        }

        return $source;
    }

    /**
     * @param $id
     * @return \yii\web\Response
     */
    public function actionPublish($id)
    {
        $this->findModel($id)->setStatus(Article::STATUS_PUBLISHED);
        Yii::$app->getSession()
            ->setFlash('success', Yii::t('backend', 'Publication status set to published. It is visible in field now'));

        return $this->redirect(['view', 'id' => $id]);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     */
    public function actionUnpublish($id)
    {
        $this->findModel($id)->setStatus(Article::STATUS_PENDING);
        Yii::$app->getSession()
            ->setFlash('danger', Yii::t('backend', 'Publication status set to unpublished. It is not visible in field now'));

        return $this->redirect(['view', 'id' => $id]);
    }

    /**
     * @param $id
     * @return array
     * @throws NotFoundHttpException
     */
    public function actionSend($id)
    {
        Yii::$app->response->format = 'json';

        $article = Article::findOne($id);
        if (!$article) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        $message = false;
        $model = new ReportErrorsForm(['type' => 'article']);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->sendEmail($article->user);
            $article->setStatus(Article::STATUS_REVIEWED);
            $message = Yii::t('main', 'Your message sent to user email');
        }

        return [
            'result' => true,
            'errors' => $model->hasErrors(),
            'message' => $message,
            'html' => $this->renderAjax('_sendForm', ['model' => $model, 'id' => $id])
        ];
    }

    /**
     * @param $id
     * @return \yii\web\Response
     */
    public function actionFeature($id)
    {
        $model = $this->findModel($id);
        $model->setFeaturedStatus(Article::FEATURED);

        return $this->redirect(Url::previous('articles') ?: ['index']);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     */
    public function actionUnfeature($id)
    {
        $model = $this->findModel($id);
        $model->setFeaturedStatus(Article::NOT_FEATURED);

        return $this->redirect(Url::previous('articles') ?: ['index']);
    }

    /**
     * @param null $q
     * @param null $id
     * @return array
     */
    public function actionTags($q = null, $id = null)
    {
        Yii::$app->response->format = 'json';

        $out = ['results' => ['id' => '', 'text' => '']];

        if (!is_null($q)) {
            $query = new Query;
            $query->select('name as id, name as text')
                ->from('tag')
                ->where(['like', 'name', $q])
                ->limit(20);
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        } elseif ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => Tag::find($id)->name];
        }

        return $out;
    }
}