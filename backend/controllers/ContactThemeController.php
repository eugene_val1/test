<?php

namespace backend\controllers;

use Yii;
use yii\base\Model;
use common\models\ContactTheme;
use backend\models\ContactThemeSearch;
use common\models\ContactThemeLang;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use himiklab\sortablegrid\SortableGridAction;

/**
 * ContactThemeController implements the CRUD actions for ContactTheme model.
 */
class ContactThemeController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }
    
    public function actions()
    {
        return [
            'sort' => [
                'class' => SortableGridAction::className(),
                'modelName' => ContactTheme::className(),
            ],
        ];
    }

    /**
     * Lists all ContactTheme models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ContactThemeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ContactTheme model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ContactTheme model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $contactTheme = new ContactTheme();
        $translation = new ContactThemeLang(['language' => Yii::$app->sourceLanguage]);

        if ($contactTheme->load(Yii::$app->request->post()) && $translation->load(Yii::$app->request->post()) && Model::validateMultiple([$contactTheme, $translation])) {
            $contactTheme->save(false);
            
            $translation->contact_theme_id = $contactTheme->id;
            $translation->save(false);
            return $this->redirect(['view', 'id' => $contactTheme->id]);
        } else {
            return $this->render('form', [
                'model' => $contactTheme,
                'translation' => $translation,
            ]);
        }
    }

    /**
     * Updates an existing ContactTheme model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $contactTheme = $this->findModel($id);
        $translation = $contactTheme->translation;
            
        if ($contactTheme->load(Yii::$app->request->post()) && $translation->load(Yii::$app->request->post()) && Model::validateMultiple([$contactTheme, $translation])) {            
            $contactTheme->save(false);
            $translation->save(false);
            return $this->redirect(['view', 'id' => $contactTheme->id]);
        } else {
            return $this->render('form', [
                'model' => $contactTheme,
                'translation' => $translation,
            ]);
        }
    }

    /**
     * Deletes an existing ContactTheme model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $contactTheme = $this->findModel($id);
        $contactTheme->delete();
        return $this->redirect(['index']);
    }

    /**
     * Finds the ContactTheme model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ContactTheme the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ContactTheme::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
