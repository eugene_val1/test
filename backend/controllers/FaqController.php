<?php

namespace backend\controllers;

use Yii;
use yii\base\Model;
use common\models\Faq;
use backend\models\FaqSearch;
use common\models\FaqLang;
use common\models\FaqCategory;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use himiklab\sortablegrid\SortableGridAction;

/**
 * FaqController implements the CRUD actions for Faq model.
 */
class FaqController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['editor'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }
    
    public function actions()
    {
        return [
            'sort' => [
                'class' => SortableGridAction::className(),
                'modelName' => Faq::className(),
            ],
        ];
    }

    /**
     * Lists all Faq models.
     * @return mixed
     */
    public function actionIndex($id = 0)
    {
        $category = FaqCategory::findOne($id);
        
        if (!$category) {
            throw new NotFoundHttpException('The requested faq does not exist.');
        }
        
        $searchModel = new FaqSearch(['category_id' => $category->id]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        
        
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'category' => $category
        ]);
    }

    /**
     * Creates a new Faq model.
     * If creation is successful, the browser will be redirected to the 'view' faq.
     * @return mixed
     */
    public function actionCreate($id)
    {
        $faq = new Faq();
        $faq->category_id = (int)$id;
        $translation = new FaqLang(['language' => Yii::$app->sourceLanguage]);

        if ($faq->load(Yii::$app->request->post()) && $translation->load(Yii::$app->request->post()) && Model::validateMultiple([$faq, $translation])) {
            $faq->save(false);
            $translation->faq_id = $faq->id;
            $translation->save(false);
            return $this->redirect(['index', 'id' => $faq->category_id]);
        } else {
            return $this->render('form', [
                'model' => $faq,
                'translation' => $translation,
            ]);
        }
    }

    /**
     * Updates an existing Faq model.
     * If update is successful, the browser will be redirected to the 'view' faq.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $faq = $this->findModel($id);
        $translation = $faq->translation;
            
        if ($faq->load(Yii::$app->request->post()) && $translation->load(Yii::$app->request->post()) && Model::validateMultiple([$faq, $translation])) {
            $faq->save(false);
            $translation->save(false);
            return $this->redirect(['index', 'id' => $faq->category_id]);
        } else {
            return $this->render('form', [
                'model' => $faq,
                'translation' => $translation,
            ]);
        }
    }

    /**
     * Deletes an existing Faq model.
     * If deletion is successful, the browser will be redirected to the 'index' faq.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $faq = $this->findModel($id);
        $faq->delete();
        return $this->redirect(['index', 'id' => $faq->category_id]);
    }

    /**
     * Finds the Faq model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Faq the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Faq::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested faq does not exist.');
        }
    }
}
