<?php

namespace backend\controllers;

use common\components\ConsoleHelper;
use common\models\LoginForm;
use common\models\Page;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index', 'migrate', 'links'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * @return \yii\web\Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * @return bool
     */
    public function actionMigrate()
    {
        return ConsoleHelper::runConsoleActionFromWebApp('migrate', ['migrationPath' => '@console/migrations/', 'interactive' => false]);
    }

    /**
     * @return array
     */
    public function actionLinks()
    {
        Yii::$app->response->format = 'json';
        $links[] = [
            'name' => Yii::t('backend', 'Select page') . '...',
            'url' => ''
        ];

        $pages = Page::find()->with('translation')->all();
        foreach ($pages as $page) {
            $links[] = [
                'name' => $page->name . ' (' . $page->getTypeName() . ')',
                'url' => $page->getUrl()
            ];
        }

        return $links;
    }
}
