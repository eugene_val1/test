<?php

namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use backend\models\RobotsFileForm;

/**
 * SeoController.
 */
class SeoController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Editing robots.txt
     * @return mixed
     */
    public function actionRobots()
    {
        $model = new RobotsFileForm();
        $model->getFileContent();
        
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->saveFileContent();
        }
        
        return $this->render('robots', [
            'model' => $model,
        ]);
    }
}
