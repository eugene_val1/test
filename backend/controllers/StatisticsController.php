<?php

namespace backend\controllers;

use backend\models\Statistics\MainStatistics;
use backend\models\StatisticsSearch;
use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * Class StatisticsController
 * @package backend\controllers
 */
class StatisticsController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new StatisticsSearch();
        $searchModel->load(Yii::$app->request->queryParams);

        $mainStatisticsModel = new MainStatistics($searchModel->created_range);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'mainStatisticsModel' => $mainStatisticsModel,
        ]);
    }

    /**
     * @return string
     */
    public function actionClients()
    {
        $searchModel = new StatisticsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('clients', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
}
