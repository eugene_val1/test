<?php

namespace backend\models;

use common\models\CountryLang;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\User;
use yii\helpers\ArrayHelper;

/**
 * UserSearch represents the model behind the search form about `common\models\User`.
 */
class UserSearch extends User
{
    /**
     * @var string
     */
    public $created_range;

    /**
     * @var string
     */
    public $created_from;

    /**
     * @var string
     */
    public $created_to;

    /**
     * @var string
     */
    public $name;

    /**
     * @var int
     */
    public $country_id;

    /**
     * @var int
     */
    public $status;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'role', 'status', 'created', 'updated', 'status', 'country_id'], 'integer'],
            [['username', 'name', 'auth_key', 'password_hash', 'email'], 'safe'],
            [['created_range'], 'match', 'pattern' => '/^.+\s\-\s.+$/'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * @return array
     */
    public function getCountries()
    {
        return ArrayHelper::map(
            CountryLang::findAll(['language' => Yii::$app->language]),
            'country_id',
            'name'
        );
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = User::find()
            ->joinWith(['profile']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query
        ]);

        $dataProvider->setSort([
            'defaultOrder' => ['created' => SORT_DESC],
            'attributes' => [
                'email',
                'username',
                'role',
                'created',
            ]
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'user.id' => $this->id,
            'user.role' => $this->role,
            'user.status' => $this->status,
            'user.country_id' => $this->country_id,
        ]);

        if (!empty($this->created_range)) {
            $range = explode('-', $this->created_range);
            $this->created_from = trim($range[0]);
            $this->created_to = trim($range[1]);
            $query->andWhere([
                'between',
                'user.created',
                (int)Yii::$app->formatter->asTimestamp($this->created_from),
                (int)Yii::$app->formatter->asTimestamp($this->created_to) + 24 * 3600 - 1
            ]);
        }

        $query->andFilterWhere(['like', 'user.username', $this->username])
            ->andFilterWhere(['like', 'user.email', $this->email])
            ->andFilterWhere(['like', 'profile.name', $this->name]);

        return $dataProvider;
    }
}
