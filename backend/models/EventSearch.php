<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Event;
use DateTime;
use DateInterval;

/**
 * EventSearch represents the model behind the search form about `common\models\Event`.
 */
class EventSearch extends Event
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'type', 'importance'], 'integer'],
            [['text', 'data', 'created'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Event::find()->where(['target' => Event::RECEIVER_ADMIN]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['created'=>SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'type' => $this->type,
            'importance' => $this->importance,
        ]);

        if ($this->created) {
            $date = DateTime::createFromFormat('d.m.Y', $this->created);
            $date->setTime(0, 0, 0);

            $unixDateStart = $date->getTimeStamp();

            $date->add(new DateInterval('P1D'));
            $date->sub(new DateInterval('PT1S'));

            $unixDateEnd = $date->getTimeStamp();

            $query->andFilterWhere(
                    ['between', 'created', $unixDateStart, $unixDateEnd]);
        }

        return $dataProvider;
    }
}
