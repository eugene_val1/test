<?php

namespace backend\models\Statistics;

use common\models\Appointment;
use common\models\Messages\ConversationMessage;
use common\models\Transaction;
use common\models\User;
use Yii;
use yii\base\Model;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/**
 * Class MainStatistics
 * @package backend\models\Statistics
 */
class MainStatistics extends Model
{
    /**
     * @var string
     */
    public $created_range;

    /**
     * @var string
     */
    public $created_from;

    /**
     * @var string
     */
    public $created_to;

    /**
     * MainStatistics constructor.
     * @param string $created_range
     */
    public function __construct($created_range)
    {
        $this->created_range = $created_range;

        $range = explode('-', $this->created_range);
        $timezone = new \DateTimeZone('UTC');

        $this->created_from = empty($range[0])
            ? (new \DateTime('first day of this month 00:00:00', $timezone))->getTimestamp()
            : (new \DateTime(trim($range[0]) . ' 00:00:00', $timezone))->getTimestamp();

        $this->created_to = empty($range[1])
            ? (new \DateTime())->getTimestamp()
            : (new \DateTime(trim($range[1]) . ' 23:59:59', $timezone))->getTimestamp();

        parent::__construct();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_range'], 'match', 'pattern' => '/^.+\s\-\s.+$/'],
        ];
    }

    /**
     * Количество регистраций на платформе.
     * С возможностью выбрать даты и посмотреть за период
     * @return int
     */
    public function getRegistrationsCount()
    {
        $query = User::find();
        $this->applyDateRange($query, 'created');

        return $query
            ->andWhere([
                'role' => User::ROLE_USER,
                'status' => User::STATUS_ACTIVE,
            ])
            ->count();
    }

    /**
     * Количество регистраций на платформе.
     * С возможностью выбрать даты и посмотреть за период
     * @return int
     */
    public function getPaidUsersRegistrationsCount()
    {
        $query = User::find();
        $this->applyDateRange($query, 'user.created');

        return $query
            ->innerJoin('transaction', 'transaction.user_id=user.id')
            ->andWhere([
                'user.role' => User::ROLE_USER,
                'user.status' => User::STATUS_ACTIVE,
            ])
            ->count();
    }

    /**
     * Кол-во первых оплат
     * Человек впервые оплатил сессию
     * @return int
     */
    public function getFirstPaymentsCount()
    {
        $query = Transaction::find();

        return $query
            ->select([
                'user_id',
                new Expression('MIN(created) as first_created_at'),
            ])
            ->groupBy('user_id')
            ->andWhere(['status' => Transaction::STATUS_SUCCESS])
            ->andHaving(['between',
                'first_created_at',
                $this->created_from,
                $this->created_to,
            ])
            ->count();
    }

    /**
     * Кол-во первых запросов на сессию.
     * Человек впервые нажал кнопку Записаться или Написать сообщение (неважно, состоялась ли после этого оплата).
     * @return int
     */
    public function getRequestsCount()
    {
        $query = ConversationMessage::find();

        return $query
            ->select([
                'conversation_id',
                new Expression('MIN(created) as first_created_at'),
            ])
            ->groupBy('conversation_id')
            ->andHaving(['between',
                'first_created_at',
                $this->created_from,
                $this->created_to,
            ])
            ->count();
    }

    /**
     * SPL (Кол-во сессий за жизнь клиента )
     * Среднее количество купленных сессий (на множестве хотя бы раз заплативших клиентов)
     *
     * @return double
     */
    public function getSPL()
    {
        $query = Transaction::find();
        $this->applyDateRange($query, 'created');

        $paidPaymentsCount = $query
            ->select('user_id')
            ->andWhere(['status' => Transaction::STATUS_SUCCESS])
            ->count();

        if (!($firstPaymentsCount = $this->getFirstPaymentsCount())) {
            return 0;
        }

        return round($paidPaymentsCount/$firstPaymentsCount,1);
    }

    /**
     * Сумма доходов от всех платящих клиентов вместе / поделить на количество платящих клиентов
     *
     * @return double
     */
    public function getLTV()
    {
        $query = Transaction::find();
        $this->applyDateRange($query, 'created');

        $totalPaidAmount = $query
            ->select('amount')
            ->andWhere(['status' => Transaction::STATUS_SUCCESS])
            ->sum('amount');

        if (!($firstPaymentsCount = $this->getFirstPaymentsCount())) {
            return 0;
        }

        return round($totalPaidAmount/$firstPaymentsCount,2);
    }

    /**
     * @return float
     */
    public function getAverageAppointmentPrice()
    {
        $query = Transaction::find();
        $this->applyDateRange($query, 'created');

        $data = $query
            ->select([
                new Expression('SUM(amount) as totalPaidAmount'),
                new Expression('COUNT(id) as totalTransactionsCount')
            ])
            ->andWhere(['status' => Transaction::STATUS_SUCCESS])
            ->asArray()
            ->one();

        if (empty($data['totalTransactionsCount'])) {
            return 0;
        }

        return round((double)$data['totalPaidAmount']/(int)$data['totalTransactionsCount'],2);
    }

    /**
     * @return string
     */
    public function getCountriesChartData()
    {
        $query = User::find();
        $this->applyDateRange($query, 'user.created');

        $usersDataBuilder = $query
            ->select([
                new Expression('country_lang.name as countryName'),
                new Expression('COUNT(user.id) as usersCount'),
            ])
            ->innerJoin('country_lang', 'country_lang.country_id=user.country_id')
            ->andWhere([
                'user.status' => User::STATUS_ACTIVE,
                'user.role' => User::ROLE_USER,
                'country_lang.language' => Yii::$app->sourceLanguage,
            ])
            ->groupBy(['user.country_id'])
            ->orderBy('usersCount DESC');

        return $this->buildCountriesChartData($usersDataBuilder);
    }

    /**
     * @return string
     */
    public function getPaidUsersCountriesChartData()
    {
        $query = User::find();
        $this->applyDateRange($query, 'transaction.created');

        $usersDataBuilder = $query
            ->select([
                new Expression('country_lang.name as countryName'),
                new Expression('COUNT(user.id) as usersCount'),
            ])
            ->innerJoin('country_lang', 'country_lang.country_id=user.country_id')
            ->innerJoin('transaction', 'transaction.user_id=user.id')
            ->andWhere([
                'user.status' => User::STATUS_ACTIVE,
                'user.role' => User::ROLE_USER,
                'country_lang.language' => Yii::$app->sourceLanguage,
            ])
            ->groupBy(['user.country_id'])
            ->orderBy('usersCount DESC');

        return $this->buildCountriesChartData($usersDataBuilder);
    }

    /**
     * @return string
     */
    public function getAppointmentAmountRangeData()
    {
        $query = Appointment::find();
        $this->applyDateRange($query, 'created');

        $data = $query
            ->select([
                new Expression('COUNT(id) as appointmentCount'),
                new Expression('(CASE
                    WHEN amount < 40 THEN "<40"
                    WHEN amount >= 40 AND amount < 50 THEN "40-50"
                    WHEN amount >= 50 AND amount < 60 THEN "50-60"
                    WHEN amount >= 60 AND amount < 70 THEN "60-70"
                    WHEN amount >= 70 AND amount < 80 THEN "70-80"
                    WHEN amount >= 80 AND amount < 90 THEN "80-90"
                    WHEN amount >= 90 THEN ">=90" END)
                    AS rangeIndex'
                ),
            ])
            ->groupBy(['rangeIndex'])
            ->asArray()
            ->all();

        $data = ArrayHelper::index($data, 'rangeIndex');

        $chartData = [
            'labels' => ['<40', '40-50', '50-60', '60-70', '70-80', '80-90', '>=90'],
            'datasets' => [
                0 => [
                    'label' => 'Количество покупок в ценовой категории, USD',
                    'data' => [],
                    'backgroundColor' => [],
                    'borderColor' => [],
                ],
            ],
        ];
        foreach ($chartData['labels'] as $label) {
            $chartData['datasets'][0]['data'][] = isset($data[$label])
                ? (int)$data[$label]['appointmentCount']
                : 0;

            $chartData['datasets'][0]['backgroundColor'][] = 'rgba(0, 99, 132, 0.3)';
            $chartData['datasets'][0]['borderColor'][] = 'rgba(0, 99, 132, 0.5)';
        }

        return \json_encode($chartData);
    }

    /**
     * @return string
     */
    public function getAppointmentCountRangeData()
    {
        $query = Appointment::find();
        $this->applyDateRange($query, 'created');

        $groups = [
            '<5'    => 0,
            '5-10'  => 0,
            '10-15' => 0,
            '15-20' => 0,
            '20-25' => 0,
            '25-30' => 0,
            '>=30'  => 0,
        ];
        $data = $query
            ->select([
                new Expression('COUNT(id) as appointmentCount'),
            ])
            ->groupBy(['user_id'])
            ->asArray()
            ->all();

        foreach ($data as $datum) {
            $amount = (int)$datum['appointmentCount'];
            if ($amount < 5) {
                $groups['<5']++;
            } elseif ($amount >= 5 && $amount < 10) {
                $groups['5-10']++;
            } elseif ($amount >= 10 && $amount < 15) {
                $groups['10-15']++;
            } elseif ($amount >= 15 && $amount < 20) {
                $groups['15-20']++;
            } elseif ($amount >= 20 && $amount < 25) {
                $groups['20-25']++;
            } elseif ($amount >= 25 && $amount < 30) {
                $groups['25-30']++;
            } else {
                $groups['>=30']++;
            }
        }

        $chartData = [
            'labels' => array_keys($groups),
            'datasets' => [
                0 => [
                    'label' => 'Количество сесий в разрезе пользователей',
                    'data' => [],
                    'backgroundColor' => [],
                    'borderColor' => [],
                ],
            ],
        ];
        foreach ($chartData['labels'] as $label) {
            $chartData['datasets'][0]['data'][] = isset($groups[$label])
                ? (int)$groups[$label]
                : 0;

            $chartData['datasets'][0]['backgroundColor'][] = 'rgba(0, 50, 132, 0.3)';
            $chartData['datasets'][0]['borderColor'][] = 'rgba(0, 59, 132, 0.5)';
        }

        return \json_encode($chartData);
    }

    /**
     * @param Query $query
     * @param string $field
     */
    protected function applyDateRange($query, $field = 'start')
    {
        $query->andWhere([
            'between',
            $field,
            $this->created_from,
            $this->created_to,
        ]);
    }

    /**
     * @param $query Query
     *
     * @return string
     */
    protected function buildCountriesChartData($query)
    {
        $usersData = $query
            ->asArray()
            ->all();

        $total = 0;
        foreach ($usersData as $datum) {
            $total += $datum['usersCount'];
        }

        $chartData = [
            'labels' => [],
            'datasets' => [
                0 => [
                    'labels' => [],
                    'data' => [],
                    'backgroundColor' => [],
                    'borderColor' => [],
                ]
            ],
        ];

        foreach ($usersData as $i => $usersDatum) {
            $percent = round((double)$usersDatum['usersCount']/$total*100, 2);
            $chartData['labels'][] = $usersDatum['countryName'] . ', ' . $percent . '%';

            $chartData['datasets'][0]['data'][] = (int)$usersDatum['usersCount'];

            $colorR = \rand(0, 255);
            $colorG = \rand(0, 255);
            $colorB = \rand(0, 255);
            $chartData['datasets'][0]['backgroundColor'][] = "rgba({$colorR}, {$colorG}, {$colorB}, 0.7)";
        }

        return \json_encode($chartData);
    }
}
