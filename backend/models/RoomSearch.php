<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Room;
use DateTime;
use DateInterval;

/**
 * RoomSearch represents the model behind the search form about `common\models\Room`.
 */
class RoomSearch extends Room
{
    public $user_name;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id'], 'integer'],
            [['name', 'alias', 'user_name', 'status', 'created'], 'safe'],
        ];
    }
    
    public function formName() {
        return '';
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Room::find();
        $query->joinWith(['profile', 'therapistLang']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        
        $dataProvider->sort->attributes['user_name'] = [
            'asc' => ['therapist_lang.name' => SORT_ASC, 'profile.name' => SORT_ASC],
            'desc' => ['therapist_lang.name' => SORT_DESC, 'profile.name' => SORT_DESC],
        ];
        $dataProvider->sort->defaultOrder = ['created' => SORT_DESC];

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'alias' => $this->alias,
            'status' => $this->status,
        ]);
        
        if ($this->created) {
            $date = DateTime::createFromFormat('d.m.Y', $this->created);
            $date->setTime(0, 0, 0);

            $unixDateStart = $date->getTimeStamp();

            $date->add(new DateInterval('P1D'));
            $date->sub(new DateInterval('PT1S'));

            $unixDateEnd = $date->getTimeStamp();

            $query->andFilterWhere(
                    ['between', 'created', $unixDateStart, $unixDateEnd]);
        }

        $query->andFilterWhere(['like', 'room.name', $this->name]);
        $query->andFilterWhere([
            'or',
            ['like', 'therapist_lang.name', $this->user_name],
            ['like', 'profile.name', $this->user_name],
        ]);

        return $dataProvider;
    }
}
