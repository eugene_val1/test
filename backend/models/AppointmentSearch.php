<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Appointment;
use DateTime;
use DateInterval;
use DateTimeZone;

/**
 * AppointmentSearch represents the model behind the search form about `common\models\Appointment`.
 */
class AppointmentSearch extends Appointment
{
    public $user_name;
    public $therapist_name;

    public $created_range;
    public $created_from;
    public $created_to;

    public $appointment_created_range;
    public $appointment_created_from;
    public $appointment_created_to;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'therapist_id'], 'integer'],
            [['status', 'payment_status', 'user_name', 'therapist_name', 'start'], 'safe'],
            [['created_range', 'appointment_created_range'], 'match', 'pattern' => '/^.+\s\-\s.+$/'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Appointment::find()->distinct();
        $query->joinWith(['user p', 'therapistLang']);
        $query->with(['room', 'transaction']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['start' => SORT_DESC]]
        ]);

        $dataProvider->sort->attributes['therapist_name'] = [
            'asc' => ['therapist_lang.name' => SORT_ASC, 'user.name' => SORT_ASC],
            'desc' => ['therapist_lang.name' => SORT_DESC, 'user.name' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $dataProvider->sort->attributes['user_name'] = [
            'asc' => ['user.name' => SORT_ASC],
            'desc' => ['user.name' => SORT_DESC],
        ];

        if ($this->start) {
            $date = DateTime::createFromFormat('d.m.Y', $this->start);
            $date->setTimezone(new DateTimeZone(Yii::$app->user->identity->timezone));
            $date->setTime(0, 0, 0);

            $unixDateStart = $date->getTimeStamp();

            $date->add(new DateInterval('P1D'));
            $date->sub(new DateInterval('PT1S'));

            $unixDateEnd = $date->getTimeStamp();

            $query->andFilterWhere(['between', 'start', $unixDateStart, $unixDateEnd]);
        }

        /*if (!empty($this->created_range)) {
            $range = explode('-', $this->created_range);
            $this->created_from = trim($range[0]) ;
            $this->created_to = trim($range[1]);
            $query->andWhere(['between', 'p.created', Yii::$app->formatter->asTimestamp($this->created_from), (int)Yii::$app->formatter->asTimestamp($this->created_to) + 24*3600-1]);
        }*/

        if (!empty($this->appointment_created_range)) {
            $range = explode('-', $this->appointment_created_range);
            $this->appointment_created_from = trim($range[0]) ;
            $this->appointment_created_to = trim($range[1]);
            $query->andWhere(['between', 'created', Yii::$app->formatter->asTimestamp($this->appointment_created_from), (int)Yii::$app->formatter->asTimestamp($this->appointment_created_to) + 24*3600-1]);
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
            'payment_status' => $this->payment_status,
        ]);

        $query->andFilterWhere([
            'and',
            ['like', 'therapist_lang.name', $this->therapist_name],
            ['like', 'p.name', $this->user_name],
        ]);

        return $dataProvider;
    }
}
