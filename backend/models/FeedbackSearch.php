<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Feedback;
use DateTime;
use DateInterval;

/**
 * FeedbackSearch represents the model behind the search form about `common\models\Feedback`.
 */
class FeedbackSearch extends Feedback
{
    public $user_name;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'published'], 'integer'],
            [['text', 'user_name', 'created'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Feedback::find();        
        $query->joinWith(['profile', 'therapistLang']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['created'=>SORT_DESC]]
        ]);

        $dataProvider->sort->attributes['user_name'] = [
            'asc' => ['therapist_lang.name' => SORT_ASC, 'profile.name' => SORT_ASC],
            'desc' => ['therapist_lang.name' => SORT_DESC, 'profile.name' => SORT_DESC],
        ];
        
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'feedback.published' => $this->published,
        ]);
        
        if ($this->created) {
            $date = DateTime::createFromFormat('d.m.Y', $this->created);
            $date->setTime(0, 0, 0);

            $unixDateStart = $date->getTimeStamp();

            $date->add(new DateInterval('P1D'));
            $date->sub(new DateInterval('PT1S'));

            $unixDateEnd = $date->getTimeStamp();
            $query->andFilterWhere(['between', 'feedback.created', $unixDateStart, $unixDateEnd]);
        }
        
        $query->andFilterWhere(['like', 'text', $this->text]);
        $query->andFilterWhere([
            'or',
            ['like', 'therapist_lang.name', $this->user_name],
            ['like', 'profile.name', $this->user_name],
        ]);

        return $dataProvider;
    }
}
