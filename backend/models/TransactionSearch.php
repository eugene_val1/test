<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Transaction;
use DateTime;
use DateInterval;
use DateTimeZone;

/**
 * TransactionSearch represents the model behind the search form about `common\models\Transaction`.
 */
class TransactionSearch extends Transaction
{
    public $user_name;
    public $therapist_name;
    public $total;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'appointment_id'], 'integer'],
            [['reason', 'status', 'user_name', 'therapist_name', 'created', 'payment_id'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Transaction::find()->distinct();
        $query->joinWith(['profile p', 'therapist th', 'therapistLang']);
        $query->with('appointment');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['created' => SORT_DESC]]
        ]);

        $dataProvider->sort->attributes['therapist_name'] = [
            'asc' => ['therapist_lang.name' => SORT_ASC, 'profile.name' => SORT_ASC],
            'desc' => ['therapist_lang.name' => SORT_DESC, 'profile.name' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $dataProvider->sort->attributes['user_name'] = [
            'asc' => ['profile.name' => SORT_ASC],
            'desc' => ['profile.name' => SORT_DESC],
        ];


        if ($this->created) {
            $date = DateTime::createFromFormat('d.m.Y', $this->created);
            $date->setTimezone(new DateTimeZone(Yii::$app->user->identity->timezone));
            $date->setTime(0, 0, 0);

            $unixDateStart = $date->getTimeStamp();

            $date->add(new DateInterval('P1D'));
            $date->sub(new DateInterval('PT1S'));

            $unixDateEnd = $date->getTimeStamp();

            $query->andFilterWhere(
                ['between', 'created', $unixDateStart, $unixDateEnd]);
        }


        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'reason', $this->reason])
            ->andFilterWhere(['like', 'amount', $this->amount])
            ->andFilterWhere(['like', 'payment_id', $this->payment_id]);

        $query->andFilterWhere([
            'or',
            ['like', 'therapist_lang.name', $this->therapist_name],
            ['like', 'p.name', $this->user_name],
        ]);

        return $dataProvider;
    }
}
