<?php

namespace backend\models;

use common\components\MailHelper;
use Yii;
use yii\base\Model;

/**
 * Class ReportErrorsForm
 * @package backend\models
 */
class ReportErrorsForm extends Model
{
    public $text;
    public $type;

    public function rules()
    {
        return [
            ['text', 'required'],
            ['text', 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'text' => Yii::t('backend', 'Describe issues to fix'),
        ];
    }

    /**
     * @param $user
     * @return bool
     */
    public function sendEmail($user)
    {
        $email = Yii::$app->mailer->compose('reportErrors', ['user' => $user, 'text' => $this->text])
            ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name])
            ->setTo($user->email)
            ->setSubject($this->getSubject() . ' ' . Yii::$app->name);

        //sign message
        MailHelper::signMessage($email);

        return $email->send();
    }

    /**
     * @return string
     */
    public function getSubject()
    {
        switch ($this->type) {
            case 'article':
                return Yii::t('main', "Fix issues in you article on");
            default:
                return Yii::t('main', "Fix your account information on");
        }
    }
}
