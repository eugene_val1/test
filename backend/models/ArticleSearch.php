<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Article;
use DateTime;
use DateInterval;

/**
 * ArticleSearch represents the model behind the search form about `common\models\Article`.
 */
class ArticleSearch extends Article
{
    /**
     * @var string
     */
    public $user_name;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['content', 'title', 'type', 'created', 'user_name', 'published', 'featured'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * @return string
     */
    public function formName()
    {
        return '';
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Article::find()->distinct();
        $query->joinWith(['profile', 'therapistLang']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['created' => SORT_DESC]]
        ]);

        $dataProvider->sort->attributes['user_name'] = [
            'asc' => ['therapist_lang.name' => SORT_ASC, 'profile.name' => SORT_ASC],
            'desc' => ['therapist_lang.name' => SORT_DESC, 'profile.name' => SORT_DESC],
        ];

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }


        $query->andFilterWhere([
            'id' => $this->id,
            'article.type' => $this->type,
            'article.published' => $this->published,
            'article.featured' => $this->featured
        ]);

        if ($this->created) {
            $date = DateTime::createFromFormat('d.m.Y', $this->created);
            $date->setTime(0, 0, 0);

            $unixDateStart = $date->getTimeStamp();

            $date->add(new DateInterval('P1D'));
            $date->sub(new DateInterval('PT1S'));

            $unixDateEnd = $date->getTimeStamp();

            $query->andFilterWhere(
                ['between', 'created', $unixDateStart, $unixDateEnd]);
        }

        $query->andFilterWhere(['like', 'title', $this->title]);
        $query->andFilterWhere([
            'or',
            ['like', 'therapist_lang.name', $this->user_name],
            ['like', 'profile.name', $this->user_name],
        ]);

        return $dataProvider;
    }
}
