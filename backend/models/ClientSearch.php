<?php

namespace backend\models;

use common\models\Client;
use DateInterval;
use DateTime;
use DateTimeZone;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * ClientSearch represents the model behind the search form about `common\models\Client`.
 */
class ClientSearch extends Client
{
    public $user_name;
    public $therapist_name;
    public $email;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'therapist_id'], 'integer'],
            ['email', 'string'],
            [['status', 'user_name', 'therapist_name', 'created'], 'safe'],
        ];
    }

    /**
     * @return string
     */
    public function formName()
    {
        return '';
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     * @throws \Exception
     */
    public function search($params)
    {
        $query = Client::find()
            //->with(['innerUser'])
            ->distinct()
            ->joinWith(['user p', 'therapistLang', 'innerUser iu']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['created' => SORT_DESC]]
        ]);

        $dataProvider->sort->attributes['therapist_name'] = [
            'asc' => ['therapist_lang.name' => SORT_ASC, 'user.name' => SORT_ASC],
            'desc' => ['therapist_lang.name' => SORT_DESC, 'user.name' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $dataProvider->sort->attributes['user_name'] = [
            'asc' => ['user.name' => SORT_ASC],
            'desc' => ['user.name' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['email'] = [
            'asc' => ['iu.email' => SORT_ASC],
            'desc' => ['iu.email' => SORT_DESC],
        ];

        if ($this->created) {
            $date = DateTime::createFromFormat('d.m.Y', $this->created);
            $date->setTimezone(new DateTimeZone(Yii::$app->user->identity->timezone));
            $date->setTime(0, 0, 0);

            $unixDateStart = $date->getTimeStamp();

            $date->add(new DateInterval('P1D'));
            $date->sub(new DateInterval('PT1S'));

            $unixDateEnd = $date->getTimestamp();

            $query->andFilterWhere(['between', 'created', $unixDateStart, $unixDateEnd]);
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'client.status' => $this->status,
        ]);

        $query->andFilterWhere([
            'and',
            ['like', 'therapist_lang.name', $this->therapist_name],
            ['like', 'p.name', $this->user_name],
            ['like', 'iu.email', $this->email],
        ]);

        return $dataProvider;
    }
}
