<?php

namespace backend\models;

use common\models\User;
use Yii;
use yii\base\Model;
use yii\helpers\FileHelper;

/**
 * Class UserExportForm
 * @package backend\models
 */
class UserExportForm extends Model
{
    public $from;
    public $to;
    public $agree_mailing = true;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['from', 'to'], 'required'],
            [['agree_mailing'], 'boolean'],
            [['agree_mailing'], 'default', 'value' => true],
        ];
    }

    public function attributeLabels()
    {
        return [
            'from' => 'From Date',
            'to' => 'To Date',
            'agree_mailing' => 'Only users agreed to mailing'
        ];
    }

    /**
     * UserExportForm constructor.
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        parent::__construct($config);

        $this->from = $this->to = Yii::$app->formatter->asDate('now', 'php:d.m.Y');
    }

    /**
     * Export to CSV
     */
    public function exportCSV()
    {
        $response = Yii::$app->getResponse();

        $from = strtotime($this->from);
        $to = strtotime($this->to);

        if ($from > $to) {
            $this->addError('from', 'Wrong interval');
            return false;
        }

        if ($this->from === $this->to) {
            $to = $from + 24*3600;
        }

        $rows = User::find()
            ->joinWith('profile', false)
            ->select('email')
            ->andWhere(['between', 'created', $from, $to])
            ->andWhere('email IS NOT NULL AND email != ""');

        if ($this->agree_mailing) {
            $rows->andWhere(['profile.agree_mailing' => 1]);
        }

        $rows = $rows->all();

        if (empty($rows)) {
            $this->addError('from', 'No user signed up in this period');
            return false;
        }

        $list = [];
        foreach ($rows as $row) {
            $list[] = [
                $row->email
            ];
        }

        array_map('unlink', glob(\Yii::getAlias('@webroot') . '/export/*.*'));

        $filename = 'export-' . date('Y-m-d_H:i:s') . '.csv';
        $file = \Yii::getAlias('@webroot') . '/export/' . $filename;
        $fp = fopen($file, 'w');
        foreach ($list as $fields) {
            fputcsv($fp, $fields);
        }
        fclose($fp);

        $response->sendFile($file);
    }
}
