<?php

namespace backend\models;

use common\models\Appointment;
use common\models\Client;
use Yii;
use yii\data\ActiveDataProvider;
use common\models\Contact;
use yii\db\Query;

/**
 * Class StatSearch
 * @package backend\models
 */
class StatisticsSearch extends Contact
{
    /**
     * @var string
     */
    public $user_name;

    /**
     * @var string
     */
    public $therapist;

    /**
     * @var string
     */
    public $status;

    /**
     * @var string
     */
    public $created_range;

    /**
     * @var string
     */
    public $created_from;

    /**
     * @var string
     */
    public $created_to;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_name', 'therapist', 'email', 'status'], 'string'],
            [['created_range'], 'match', 'pattern' => '/^.+\s\-\s.+$/'],
        ];
    }

    /**
     * @param array $data
     * @param null $formName
     * @return bool
     */
    public function load($data, $formName = null)
    {
        parent::load($data, $formName);

        $range = explode('-', $this->created_range);

        $this->created_from = empty($range[0])
            ? (new \DateTime('first day of this month 00:00:00'))->getTimestamp()
            : (new \DateTime(trim($range[0]) . ' 00:00:00'))->getTimestamp();

        $this->created_to = empty($range[1])
            ? (new \DateTime())->getTimestamp()
            : (new \DateTime(trim($range[1]) . ' 23:59:59'))->getTimestamp();

        return true;
    }

    /**
     * Search
     * @param $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $this->load($params);

        $query = Client::find()
            ->alias('c')
            ->joinWith(['innerUser u', 'therapist t'])
            ->andFilterWhere([
                'c.status' => $this->status
            ])
            ->andFilterWhere([
                'and',
                ['like', 'u.email', $this->email],
                ['like', 'c.user_name', $this->user_name],
                ['like', 't.name', $this->therapist]
            ]);

        if (!empty($this->created_range)) {
            $this->applyDateRange($query, 'u.created');
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'created' => SORT_DESC
                ]
            ]
        ]);

        if (!$this->validate()) {
            return $dataProvider;
        }

        return $dataProvider;
    }

    /**
     * @return int|string
     */
    public function getNewAppointmentsCount()
    {
        $query = Appointment::find();

        $this->applyDateRange($query);

        return $query
            ->andWhere(['status' => Appointment::STATUS_NEW])
            ->count();
    }

    /**
     * @return int|string
     */
    public function getCompletedAppointmentsCount()
    {
        $query = Appointment::find();

        $this->applyDateRange($query);

        return $query
            ->andWhere(['status' => Appointment::STATUS_COMPLETED])
            ->count();
    }

    /**
     * @return int|string
     */
    public function getMissedAppointmentsCount()
    {
        $query = Appointment::find();

        $this->applyDateRange($query);

        return $query
            ->andWhere(['status' => Appointment::STATUS_MISSED])
            ->count();
    }

    /**
     * @return int|string
     */
    public function getCanceledAppointmentsCount()
    {
        $query = Appointment::find();

        $this->applyDateRange($query);

        return $query
            ->andWhere(['status' => Appointment::STATUS_CANCELED])
            ->count();
    }

    /**
     * @param Query $query
     * @param string $field
     */
    protected function applyDateRange($query, $field = 'start')
    {
        $query->andWhere([
            'between',
            $field,
            Yii::$app->formatter->asTimestamp($this->created_from),
            (int)Yii::$app->formatter->asTimestamp($this->created_to),
        ]);
    }
}
