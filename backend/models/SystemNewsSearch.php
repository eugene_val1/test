<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\SystemNews;

/**
 * SystemNewsSearch represents the model behind the search form about `common\models\SystemNews`.
 */
class SystemNewsSearch extends SystemNews
{
    public $created_range;
    public $created_from;
    public $created_to;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'target', 'status'], 'integer'],
            [['text', 'title', 'created', 'updated'], 'safe'],
            [['created_range'], 'match', 'pattern' => '/^.+\s\-\s.+$/'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SystemNews::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'target' => $this->target,
            'status' => $this->status,
            'created' => $this->created,
            'updated' => $this->updated,
        ]);

        if (!empty($this->created_range)) {
            $range = explode('-', $this->created_range);
            $this->created_from = trim($range[0]);
            $this->created_to = trim($range[1]);
            $query->andWhere([
                'between',
                'created',
                Yii::$app->formatter->asTimestamp($this->created_from),
                (int)Yii::$app->formatter->asTimestamp($this->created_to) + 24 * 3600 - 1
            ]);
        }

        $query->andFilterWhere(['like', 'text', $this->text])
            ->andFilterWhere(['like', 'title', $this->title]);

        return $dataProvider;
    }
}
