<?php

namespace backend\models;

use common\models\TherapistReview;
use DateInterval;
use DateTime;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * TherapistReviewSearch represents the model behind the search form about `common\models\TherapistReview`.
 */
class TherapistReviewSearch extends TherapistReview
{
    public $therapist_name;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'published'], 'integer'],
            [['text', 'name', 'therapist_name', 'created'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TherapistReview::find();
        $query->joinWith(['therapistLang']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['created' => SORT_DESC]]
        ]);

        $dataProvider->sort->attributes['therapist_name'] = [
            'asc' => ['therapist_lang.name' => SORT_ASC, 'profile.name' => SORT_ASC],
            'desc' => ['therapist_lang.name' => SORT_DESC, 'profile.name' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'therapist_review.published' => $this->published,
        ]);

        if ($this->created) {
            $date = DateTime::createFromFormat('d.m.Y', $this->created);
            $date->setTime(0, 0, 0);

            $unixDateStart = $date->getTimeStamp();

            $date->add(new DateInterval('P1D'));
            $date->sub(new DateInterval('PT1S'));

            $unixDateEnd = $date->getTimeStamp();
            $query->andFilterWhere(['between', 'therapist_review.created', $unixDateStart, $unixDateEnd]);
        }

        $query->andFilterWhere(['like', 'text', $this->text]);
        $query->andFilterWhere(['like', 'therapist_review.name', $this->name]);
        $query->andFilterWhere(['like', 'therapist_lang.name', $this->therapist_name]);

        return $dataProvider;
    }
}
