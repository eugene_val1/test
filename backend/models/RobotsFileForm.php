<?php
namespace backend\models;

use Yii;
use yii\base\Model;

class RobotsFileForm extends Model
{
    public $content;

    public function rules()
    {
        return [
            ['content', 'string', 'max' => 1000],
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'content' => Yii::t('main', 'Robots.txt content'),
        ];
    }
    
    public function getFileContent() {
       $this->content = file_get_contents($this->getFile());
    }
    
    public function saveFileContent() {
       file_put_contents($this->getFile(), $this->content);
    }
    
    public function getFile(){
        $file = Yii::getAlias('@frontend') . '/web/robots.txt';
        
        if (!is_file($file)) {
            touch($file);
        }
        
        return $file;
    }
}
