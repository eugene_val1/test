<?php
namespace backend\components;

use yii\web\Request;
use common\models\Language;

class LanguageRequest extends Request
{
    private $_langUrl;

    public function getUrl()
    {
        if ($this->_langUrl === null) {
            $this->_langUrl = parent::getUrl();
            
        	$url_list = explode('/', $this->_langUrl);

        	$lang_url = isset($url_list[1]) ? $url_list[1] : null;

        	Language::setCurrent($lang_url, false);

                if( $lang_url !== null && $lang_url === Language::getCurrent()->url && 
                strpos($this->_langUrl, Language::getCurrent()->url) === 1 )
                {
                     $this->_langUrl = substr($this->_langUrl, strlen(Language::getCurrent()->url)+1);
                }
        }

        return $this->_langUrl;
    }
}