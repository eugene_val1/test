<?php

namespace backend\components\translation\actions;

use Yii;
use yii\base\Action;
use yii\data\ActiveDataProvider;

class TranslateAction extends Action
{
    /**
     * @var string the view file to be rendered.
     */
    public $view = '@backend/components/translation/views/index';
    /**
     * @var string the name of the model
     */
    public $modelName;
    /**
     * @var string the name of the foreign field
     */
    public $foreignField;
    
    /**
     * Runs the action
     *
     * @return string result content
     */
    public function run($id)
    {
        $model = new $this->modelName;
        $query = $model::find()->where([$this->foreignField => $id]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        
        return $this->controller->render($this->view, [
            'dataProvider' => $dataProvider,
            'id' => $id
        ]);
    }
}
