<?php

namespace backend\components\translation\actions;

use Yii;
use yii\base\Action;
use yii\data\ActiveDataProvider;

class CreateTranslationAction extends Action
{
    /**
     * @var string the view file to be rendered.
     */
    public $view = '@backend/components/translation/views/form';
    /**
     * @var string the name of the model
     */
    public $modelName;
    /**
     * @var string the name of the foreign field
     */
    public $foreignField;
    /**
     * Runs the action
     *
     * @return string result content
     */
    public function run($id)
    {
        $model = new $this->modelName;
        $model->{$this->foreignField} = $id;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->controller->redirect(['translate', 'id' => $id]);
        } else {
            return $this->controller->render($this->view, [
                'id' => $id,
                'model' => $model,
                'attributes' => $model->relatedModel->translatedAttributes,
            ]);
        }
    }
}
