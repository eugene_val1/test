<?php

namespace backend\components\translation\actions;

use Yii;
use yii\base\Action;
use yii\data\ActiveDataProvider;

class UpdateTranslationAction extends Action
{
    /**
     * @var string the view file to be rendered.
     */
    public $view = '@backend/components/translation/views/form';
    /**
     * @var string the name of the model
     */
    public $modelName;
    /**
     * @var string the name of the foreign field
     */
    public $foreignField;
    /**
     * Runs the action
     *
     * @return string result content
     */
    public function run($id)
    {
        $model = new $this->modelName;
        $translation = $model::findOne($id);
        
        if ($translation === null) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        if ($translation->load(Yii::$app->request->post()) && $translation->save()) {
            return $this->controller->redirect(['translate', 'id' => $translation->{$this->foreignField}]);
        } else {
            return $this->controller->render($this->view, [
                'id' => $translation->{$this->foreignField},
                'model' => $translation,
                'attributes' => $translation->relatedModel->translatedAttributes,
            ]);
        }
    }
}
