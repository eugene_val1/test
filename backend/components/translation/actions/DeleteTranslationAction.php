<?php

namespace backend\components\translation\actions;

use Yii;
use yii\base\Action;
use yii\web\NotFoundHttpException;

class DeleteTranslationAction extends Action
{
    /**
     * @var string the view file to be rendered.
     */
    public $view = '@backend/components/translation/views/view';
    /**
     * @var string the name of the model
     */
    public $modelName;
    /**
     * @var string the name of the foreign field
     */
    public $foreignField;
    /**
     * Runs the action
     *
     * @return string result content
     */
    public function run($id)
    {
        $model = new $this->modelName;
        $translation = $model::findOne($id);
        
        if ($translation === null) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        
        if($translation->languageModel->local == Yii::$app->sourceLanguage){
            Yii::$app->getSession()->setFlash('danger', "You can't delete default translation");
        } else {
            $translation->delete();
        }

        return $this->controller->redirect(['translate', 'id' => $translation->{$this->foreignField}]);
    }
}
