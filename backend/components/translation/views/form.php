<?php
/* @var $this yii\web\View */
/* @var $model common\models\Course */
/* @var $form yii\widgets\ActiveForm */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use \vova07\imperavi\Widget;
use yii\helpers\ArrayHelper;
use common\models\Language;

$this->title = $model->isNewRecord ? Yii::t('backend', 'Create translation') : Yii::t('backend', 'Update translation') . ': ' . $model->languageName;
?>
<div class="row">
    <div class="col-lg-12">
        <?php $form = ActiveForm::begin(); ?>
        
        <?php
            if($model->isNewRecord){
                echo $form->field($model, 'language')->dropDownList(
                    ArrayHelper::map(Language::find()->all(), 'local', 'name'),
                    ['prompt'=>''] 
                );
            }
        ?>
        
        <?php 
            foreach ($attributes as $attribute => $type){
                echo ($type == 'string') ? $form->field($model, $attribute)->textInput() : $form->field($model, $attribute)->widget(Widget::className(), []);
            }
        ?>
        <div class="form-group">
        <?= Html::submitButton('<i class="fa fa-save"></i> ' . ($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Save')), ['class' => $model->isNewRecord ? 'btn btn-success btn-sm' : 'btn btn-primary btn-sm']) ?>
        <a href="<?= Url::toRoute(['translate', 'id' => $id]); ?>" class="btn btn-danger btn-sm"><i class="fa fa-backward"></i> <?= Yii::t('backend', 'Cancel'); ?></a>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>
