<?php
/* @var $this yii\web\View */
/* @var $searchModel backend\models\CourseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
$this->title = Yii::t('backend', 'Translations');
?>

<div class="row mt">
    <div class="col-lg-12">
     <div class="pull-right">
        <?= Html::a('<i class="fa fa-plus"></i> ' . Yii::t('backend', 'Create'), ['createTranslation', 'id' => $id], ['class' => 'btn btn-success btn-sm']) ?>
        <?= Html::a('<i class="fa fa-backward"></i> ' . Yii::t('backend', 'Back'), 'index', ['class' => 'btn btn-success btn-sm']) ?>
    </div>   
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'tableOptions' => ['class' => 'table table-striped table-advance table-hover'],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'languageName',
                'label' => Yii::t('backend', 'Language')
            ],
            [
                'attribute' => 'default',
                'format' => 'html',
                'value'=>function ($model) {
                     return ($model->languageModel->local == Yii::$app->sourceLanguage) ? '<span class="label label-success label-mini"><i class="fa fa-check"></i></span>' : '';
                 },
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update} {delete}',
                'buttons' => [
                    'view' => function ($url, $model, $key) {
                        return Html::a('<i class="fa fa-eye"></i> ' . Yii::t('backend', 'View'), ['viewTranslation', 'id' => $model->id], [
                            'class' => 'btn btn-success btn-sm',
                            'data-pjax' => 0,
                        ]);
                    },
                    'update' => function ($url, $model, $key) {
                        return Html::a('<i class="fa fa-pencil"></i> ' . Yii::t('backend', 'Edit'), ['updateTranslation', 'id' => $model->id], [
                            'class' => 'btn btn-primary btn-sm',
                            'data-pjax' => 0,
                        ]);
                    },
                    'delete' => function ($url, $model, $key) {
                        return Html::a('<i class="fa fa-trash-o "></i> ' . Yii::t('backend', 'Delete'), ['deleteTranslation', 'id' => $model->id], [
                            'class' => 'btn btn-danger btn-sm',
                            'data' => ['pjax' => 0, 'method' => 'post', 'confirm' => Yii::t('backend', 'Delete element?')],
                        ]);
                    }
                ],
            ],
        ],
    ]); ?>

    </div>
</div>