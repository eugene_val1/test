<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Course */

$this->title = Yii::t('backend', 'View translation');
?>
<div class="row">
    <div class="col-lg-12">
    <div style="margin-bottom: 15px" class="pull-right">
            <?= Html::a('<i class="fa fa-trash-o"></i> ' . Yii::t('backend', 'Delete'), ['deleteTranslation', 'id' => $model->id], [
            'class' => 'btn btn-danger btn-sm',
            'data' => [
                'confirm' => Yii::t('backend', 'Delete element?'),
                'method' => 'post',
            ],
    ]) ?>
    <?= Html::a('<i class="fa fa-pencil"></i> ' . Yii::t('backend', 'Update'), ['updateTranslation', 'id' => $model->id], ['class' => 'btn btn-primary btn-sm']) ?>
    <?= Html::a('<i class="fa fa-backward"></i> ' . Yii::t('backend', 'Back'), ['translate', 'id' => $id], ['class' => 'btn btn-success btn-sm']) ?>
    </div>    
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => call_user_func(function() use( &$attributes) {
            $attributesList = [];
            foreach ($attributes as $attribute => $type){
                $attributesList[] = ($type == 'html') ? $attribute . ":html" : $attribute;
            }
            return $attributesList;
        }),
    ]) ?>

    </div>
</div>
