APP_NAME = TreatField

SHELL ?= /bin/bash
ARGS = $(filter-out $@,$(MAKECMDGOALS))

IMAGE_TAG = latest
IMAGE_NAME = treatfield

BUILD_ID ?= $(shell /bin/date "+%Y%m%d-%H%M%S")

.SILENT: ;
.ONESHELL: ;
.NOTPARALLEL: ;
.EXPORT_ALL_VARIABLES: ;
Makefile: ;

# Run make help by default
.DEFAULT_GOAL = help

ifneq ("$(wildcard ./VERSION)","")
VERSION ?= $(shell cat ./VERSION | head -n 1)
else
VERSION ?= 0.0.1
endif

# Public targets

.PHONY: .title
.title:
	$(info $(APP_NAME) v$(VERSION))

.PHONY: build
build:
	docker build \
        --build-arg VERSION=$(VERSION) \
        --build-arg BUILD_ID=$(BUILD_ID) \
        -t $(IMAGE_NAME):$(IMAGE_TAG) \
        --no-cache \
        --force-rm .

.PHONY: up
up:
	docker-compose up -d

.PHONY: bash
bash:
	docker exec -it treatfield.local bash

.PHONY: down
down:
	docker-compose down

.PHONY: reset
reset: down up

.PHONY: start
start:
	docker-compose start

.PHONY: stop
stop:
	docker-compose stop

.PHONY: phpcs
phpcs:
	./vendor/bin/phpcs

.PHONY: migrate
migrate:
	docker-compose exec treatfield.local php yii migrate

.PHONY: db-init
db-init:
	docker exec -i treatfield_db mysql -u root -p treatfield < ./docker/mysql/dump/init.sql

.PHONY:
build-prod:
	ssh smart@46.101.212.28 "cd /var/www/treatfield.com; ./build master"

.PHONY: build-test
build-test:
	ssh smart@46.101.212.28 "cd /var/www/test.treatfield.com; ./git-force $(ARGS)"

.PHONY: help
help: .title
	@echo ''
	@echo 'Usage: make [target] [ENV_VARIABLE=ENV_VALUE ...]'
	@echo ''
	@echo 'Available targets:'
	@echo ''
	@echo '  help          Show this help and exit'
	@echo '  build         Build or rebuild services'
	@echo '  db-init       Init database'
	@echo '  up            Starts and attaches to containers for a service'
	@echo '  bash          Go to the application container (if any)'
	@echo '  down          Stop, kill and purge project containers.'
	@echo '  start         Start containers.'
	@echo '  stop          Stop containers.'
	@echo '  phpcs         Run code style check.'
	@echo '  migrate       Execute available migrations.'
	@echo ''

%:
	@:
