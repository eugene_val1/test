<?php

namespace api\modules\v1;

use yii\web\Response;

/**
 * Class Module
 * @package api\modules\v1
 */
class Module extends \yii\base\Module
{
    public $controllerNamespace = 'api\modules\v1\controllers';

    /**
     * Init
     */
    public function init()
    {
        parent::init();
    }

    /**
     * @param \yii\base\Action $action
     * @return bool
     */
    public function beforeAction($action)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        return parent::beforeAction($action);
    }
}