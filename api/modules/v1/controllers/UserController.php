<?php

namespace api\modules\v1\controllers;

use Yii;
use yii\filters\ContentNegotiator;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\filters\auth\HttpBearerAuth;
use \Firebase\JWT\JWT;
use common\models\User;
use yii\web\Response;

/**
 * Class UserController
 * @package api\modules\v1\controllers
 * @deprecated
 */
class UserController extends Controller
{
    public $enableCsrfValidation = false;

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'view'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['view'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'authenticator' => [
                'class' => HttpBearerAuth::className(),
                'except' => ['login']
            ],
            [
                'class' => ContentNegotiator::className(),
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }

    /**
     * @return array
     */
    public function actionLogin()
    {
        $login = Yii::$app->request->post('login');
        $password = Yii::$app->request->post('password');
        $social = Yii::$app->request->post('social');
        
        if (empty($login) || empty($password)) {
            return [
                'status' => 406,
                'message' => Yii::t('main', 'Incorrect username or password.')
            ];
        }
        
        $user = User::findByUsername($login);
        if (!$user) {
            $user = User::findByEmail($login);
        }

        //if social
        if (!empty($social) && md5($social . $login) !== $password) {
            return [
                'status' => 406,
                'message' => Yii::t('main', 'Incorrect username or password.')
            ];
        } elseif (!$user || !$user->validatePassword($password)) {
            return [
                'status' => 406,
                'message' => Yii::t('main', 'Incorrect username or password.')
            ];
        }

        $payload = [
            'data' => [
                'id' => $user->id
            ],
            'uid' => uniqid()
        ];
        $token = JWT::encode($payload, Yii::$app->params['apiSecret'], 'HS256');

        return [
            'status' => 200,
            'data' => [
                'token' => $token
            ]
        ];
    }

    /**
     * @return array
     */
    public function actionView()
    {
        /** @var $user User */
        $user = Yii::$app->user->identity;

        return [
            'status' => 200,
            'data' => [
                [
                    'id' => $user->getId(),
                    'username' => $user->username,
                    'email' => $user->email,
                    'name' => $user->getName(),
                    'role' => $user->getRoleName()
                ]
            ]
        ];
    }
}