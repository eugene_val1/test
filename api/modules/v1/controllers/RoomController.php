<?php

namespace api\modules\v1\controllers;

use common\models\Appointment;
use yii\web\Controller;
use yii\filters\ContentNegotiator;
use yii\web\Response;
use yii\filters\AccessControl;
use yii\filters\auth\HttpBearerAuth;

/**
 * Class RoomController
 * @package api\modules\v1\controllers
 */
class RoomController extends Controller
{
    public $enableCsrfValidation = false;

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['settings'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['list'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'authenticator' => [
                'class' => HttpBearerAuth::className(),
                'except' => ['settings']
            ],
        ];
    }

    /**
     * @return array
     */
    public function actionSettings()
    {
        return [
            'status' => '',
            'data' => [
                'username' => 'treatfield',
                'password' => 'tUi97Kmp',
                'uris' => [
                    'turn:treatfield.com:3478?transport=udp',
                    'turn:treatfield.com:3478?transport=tcp'
                ]
            ]
        ];
    }

    /**
     * @return string
     */
    public function actionList()
    {
        $appointments = Appointment::find()
            ->with(['room', 'therapist', 'user'])
            ->where([
                'or',
                ['user_id' => \Yii::$app->user->getId()],
                ['therapist_id' => \Yii::$app->user->getId()],
            ])
            ->andWhere(['>', 'room_id', 0])
            ->all();

        $list = [];

        foreach ($appointments as $appointment) {
            /** @var $appointment Appointment */
            $list[] = [
                'therapist' => $appointment->therapist->name,
                'user' => $appointment->user->name,
                'room_id' => $appointment->room_id,
                'room_alias' => $appointment->room->alias,
                'room_url' => \Yii::$app->params['baseUrl'] . '/appointment/video?id=' . $appointment->id ,
                'start' => $appointment->start,
                'end' => $appointment->end,
                'duration' => $appointment->duration,
                'complete_status' => $appointment->status,
                'payment_status' => $appointment->payment_status,
                'created' => $appointment->created
            ];
        }

        return [
            'status' => 200,
            'data' => $list
        ];
    }
}