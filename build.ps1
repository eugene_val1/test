#!/bin/bash

# start build

# git
echo "Get updates from current branch"
git checkout -f master
git pull origin $1 

# Composer install
echo "Running composer"
composer install

# Yii2		

# migrate up
echo "[Yii2] Running migrations"
./yii migrate/up --interactive=0

	
# combine and minify assets
echo "[Yii2] Combine assets"
./yii asset assets.php frontend/config/assets.php

